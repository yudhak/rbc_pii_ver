define(function (require, exports, module) {
    'use strict';

    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var Cookies = require('Cookies');
    var commonConfig = require('commonconfig');

    module.exports = LayoutManager.extend({
        el: false,
        initialize: function () {
            var self = this;
            this.isLoadingModule = false;
            if (!window.indexedDB)
                window.indexedDB = window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
            window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
            window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange

            if (!window.indexedDB) {
                window.alert("Your browser doesn't support a stable version of IndexedDB.")
            }
            var db;
            var request = window
                .indexedDB
                .open('newDatabase', 1);
            request.onsuccess = function (event) {
                db = request.result;
            }
            this.roles = JSON.parse(localStorage.getItem('roles'));
        },
        template: _.template(template),
        events: {
            'click [data-name="menuSidebar"] li a[href]:not([href="#"])': 'clickMenuSidebar',
            'click .xn-openable': 'setActive',
            //'click [single-menu]': 'setActiveSingle',
        },
        afterRender: function () {
            require(['commonfunction'], (commonFunction) => {
                var hash = commonFunction.getUrlHashSplit();
                var DOM = $(self.$('.x-navigation')[0]);

                if (DOM) {
                    var linkDOMs = $('a[href]:not([href="#"])', DOM);
                    var found = undefined;

                    for (var i = hash.length - 1; i >= 0; i--) {
                        var searchHashTag = commonFunction.getCurrentHashToLevel(i + 1);
                        found = _.find(linkDOMs, function (item) {
                            var hashTag = $(item).attr('href');
                            return (searchHashTag == hashTag);
                        });
                        if (found) {
                            linkDOMs.removeClass('active');
                            $(found)
                                .addClass('active')
                                .parents('.xn-openable')
                                .addClass('active');

                            break;
                        }
                    }
                }

            });
            this.setAccessRole();
        },
        setAccessRole: function () {
            var self = this;
            if (this.roles) {
                _
                    .each(this.roles, function (item) {
                        switch (item.Name) {
                            case "Dashboards":
                                self
                                    .$('[dashboard]')
                                    .removeClass('hide');
                                if (item.SubMenu) {
                                    _
                                        .each(item.SubMenu, function (obj) {
                                            switch (obj.Name.trim()) {
                                                case "Dashboard-Main":
                                                    self
                                                        .$('[main-dashboard]')
                                                        .removeClass('hide');
                                                    break;
                                                case "Dashboard-Kalkulasi":
                                                    self
                                                        .$('[dashboard-calculation]')
                                                        .removeClass('hide');
                                                    break;
                                                case "Dashboard-DetailResult":
                                                    self
                                                        .$('[dashboard-result]')
                                                        .removeClass('hide');
                                                    break;
                                            }
                                        });
                                }
                                break;
                            case "Kalkulasi":
                                self
                                    .$('[calculation]')
                                    .removeClass('hide');
                                break;
                            case "Kalkulasi Komparasi":
                                self
                                    .$('[calculation-compare]')
                                    .removeClass('hide');
                                break;
                            case "Master":
                                self
                                    .$('[master-data]')
                                    .removeClass('hide');
                                if (item.SubMenu) {
                                    _
                                        .each(item.SubMenu, function (val) {
                                            switch (val.Name) {
                                                case "Master-KategoriResiko":
                                                    self
                                                        .$('[risk-category]')
                                                        .removeClass('hide');
                                                    break;
                                                case "Master-Sektor":
                                                    self
                                                        .$('[sector]')
                                                        .removeClass('hide');
                                                    break;
                                                case "Master-Proyek":
                                                    self
                                                        .$('[project]')
                                                        .removeClass('hide');
                                                    break;
                                                case "Master-Parameter":
                                                    self
                                                        .$('[likelihood]')
                                                        .removeClass('hide');
                                                    break;
                                                case "Master-AssetData":
                                                    self
                                                        .$('[asset-data]')
                                                        .removeClass('hide');
                                                    break;
                                                case "Master-TahapanPenjaminan":
                                                    self
                                                        .$('[tahapan-project]')
                                                        .removeClass('hide');
                                                    break;
                                                case "Master-TahapanProject":
                                                    self
                                                        .$('[stage]')
                                                        .removeClass('hide');
                                                    break;
                                                case "Master-Komentar":
                                                    self
                                                        .$('[comment]')
                                                        .removeClass('hide');
                                                    break;
                                                case "Master-MatriksKolerasi":
                                                    self
                                                        .$('[correlation-matrix]')
                                                        .removeClass('hide');
                                                    break;
                                                case "Master-Ambang Batas Resiko":
                                                    self
                                                        .$('[functional-risk]')
                                                        .removeClass('hide');
                                                    break;
                                            }
                                        });
                                }
                                break;
                            case "Scenario":
                                self
                                    .$('[scenario]')
                                    .removeClass('hide');
                                break;
                            case "RisikoMatriks":
                                self
                                    .$('[risk-matrix]')
                                    .removeClass('hide');
                                break;
                            case "Korelasi":
                                self
                                    .$('[correlation]')
                                    .removeClass('hide');
                                if (item.SubMenu) {
                                    _
                                        .each(item.SubMenu, function (val) {
                                            switch (val.Name) {
                                                case "KorelasiResiko":
                                                    self
                                                        .$('[correlated-sector]')
                                                        .removeClass('hide');
                                                    break;
                                                case "KorelasiProyek":
                                                    self
                                                        .$('[correlated-project]')
                                                        .removeClass('hide');
                                                    break;
                                            }
                                        });
                                }
                                break;
                            case "KomentarKeseluruhan":
                                self
                                    .$('[overall-comment]')
                                    .removeClass('hide');
                                break;
                            case "Approval":
                                self
                                    .$('[approval]')
                                    .removeClass('hide');
                                break;
                            case "Setting Menu":
                                self.$('[user-management]').removeClass('hide');
                                self.$('[setting-menu]').removeClass('hide');
                                break;
                            case "Setting Approval":
                                self.$('[user-management]').removeClass('hide');
                                self.$('[master-approval]').removeClass('hide');
                                break;
                            case "AuditLog":
                                self
                                    .$('[audit-log]')
                                    .removeClass('hide');
                                break;
                        }
                    });
            }
            const roleName = Cookies.get(commonConfig.cookieFields.roleName);
            if (roleName === "Super Manager") {
                this.$('[user-management]').removeClass('hide');
            }
        },
        clickMenuSidebar: function (e) {
            e.preventDefault();
            var currentTarget = this.$(e.currentTarget);
            var href = currentTarget.attr('href');

            var ret = Backbone
                .history
                .navigate(href, true);
            if (ret === undefined) {
                Backbone
                    .history
                    .loadUrl(href);
            }
        },
        setActive: function (e) {
            e.preventDefault();
            e.stopPropagation();
            var currentTarget = $(e.currentTarget);
            var href = currentTarget.attr('href');

            var DOM = $($('.x-navigation')[0]);
            var linkDOMs = $('a[href]:not([href="#"])', DOM);
            linkDOMs.removeClass('active');

            var li = $(this).parent('li');
            var ul = li.parent("ul");
            if (ul.length == 0) {
                const elem = $('li').hasClass('active');
                if (elem) {
                    $('li').removeClass('active')
                }
            } else {
                ul.find(" > li").not(li).removeClass("active");
            }

            $(currentTarget).addClass('active');

            var li = $(this);
            if (li.children("ul").length > 0 || li.children(".panel").length > 0 || $(this).hasClass("xn-profile") > 0) {
                if (li.hasClass("active")) {
                    li.removeClass("active");
                    li.find("li.active").removeClass("active");
                } else
                    li.addClass("active");

                onresize();

                if ($(this).hasClass("xn-profile") > 0)
                    return true;
                else
                    return false;
            }
        },
        setActiveSingle: function (e) {
            e.preventDefault();
            var currentTarget = this.$(e.currentTarget);
            var DOM = $(this.$('.x-navigation')[0]);
            var linkDOMs = $('a[href]:not([href="#"])', DOM);
            linkDOMs.removeClass('active');
            $(currentTarget).addClass('active');
        }
    });
});