define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./row.html');
    var commonFunction = require('commonfunction');

    module.exports = LayoutManager.extend({
        tagName: 'tr',
        template: _.template(template),
        events: {
            'click [name="detail"]': 'Detail',
        },
        Detail: function(e) {
            commonFunction.showLoadingSpinner();
            var self = this;
            if (e.target.lang == "Scenario") {
                var scenarioId = e.target.value;
                require(['./../scenario/view'], function(View, scenarioId) {
                    commonFunction.setDefaultModalDialogFunction(self, View, self.model, scenarioId);
                });
            } else if (e.target.lang == "RiskMatrixProject") {
                var riskMatrixId = e.target.value;
                require(['./../risk_matrix/view'], function(View, riskMatrixId) {
                    commonFunction.setDefaultModalDialogFunction(self, View, self.model, riskMatrixId);
                });
            } else if (e.target.lang == "CorrelatedSektor") {
                var correlatedSectorId = e.target.value;
                require(['./../correlated_sector/view'], function(View, correlatedSectorId) {
                    commonFunction.setDefaultModalDialogFunction(self, View, self.model, correlatedSectorId);
                });
            } else if (e.target.lang == "CorrelatedProject") {
                var correlatedProjectId = e.target.value;
                require(['./../correlated_project/view'], function(View, correlatedProjectId) {
                    commonFunction.setDefaultModalDialogFunction(self, View, self.model, correlatedProjectId);
                });
            }
        }
    });
});
