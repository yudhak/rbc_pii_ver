define(function(require, exports, module) {
    'use strict';

    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'Id',
        urlRoot: 'api/Approval',
        defaults: function() {
            return {
              RequestId : '',
              StatusApproval : '',
              SourceApproval : '',
              StatusId : '',
              SourceApproval : '',
              CreateBy : '',
              CreateDate : '',
              UpdateBy : '',
              UpdateDate : '',
              IsDelete : '',
              RoleId : '',
              Keterangan : '',
              IsActive : ''
            }
        }
    });
});
