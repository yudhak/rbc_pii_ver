define(function (require, exports, module) {
    'use strict';
    var View = require('modaldialogdefault');
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');
    var moment = require('moment');
    var CollectionStage = require('./../../master/stage/collection');
    var TableLikelihood = require('./../../risk_matrix/detail/likelihood/detail/table/table');
    var CollectionLikelihood = require('./../../risk_matrix/detail/likelihood/detail/collection');
    var ModelStageTahunRiskMatrix = require('./../../risk_matrix/edit/model');
    var Model = require('./../model');

    module.exports = View.extend({
        template: _.template(template),
        initialize: function (options) {
            var self = this;
            this.IsSended = null;
            this.keyword = "";
            this.stages = null;
            this.project = options.model.attributes.riskMatrixProject;
            this.riskMatrixProject = options.model.attributes.riskMatrixCollectionParamForMasterAproval;
            this.stageTahunRiskMatrix = this.generateStageTahunData(this.riskMatrixProject);
            this.modelStageTahunRiskMatrix = new ModelStageTahunRiskMatrix();
            this.namaScenario = options.model.attributes.NamaScenario;
            this.keterangan = options.model.get('Keterangan');
            this.tableLikelihood = new TableLikelihood({
                collection: new CollectionLikelihood()
            });
            this.listenTo(this.model, 'sync error', function () { 
                debugger;
                eventAggregator.trigger('approval/risk_matrix/approval:fecth');
              });
            this.listenTo(this.model, 'sync', function (model) {
                commonFunction.responseSuccessUpdateAddDelete('Approval berhasil.');
                self.$el.modal('hide');
                eventAggregator.trigger('approval/risk_matrix/approval:fecth');
            });

            this.collectionStage = new CollectionStage();
            this.collectionStage.fetch({
                data: {
                    Search: this.keyword
                },
                success: function (req, res) {
                    self.stages = res;
                    self.formStage(res);
                }
            });
        },
        afterRender: function () {
            var self = this;
            if (!this.model.id) {
                return;
            }
            this.renderStage();
            this.renderLikelihood();
            this.renderTableRiskMatrix();
            this.fetchStageTahun();
            commonFunction.closeLoadingSpinner();
        },
        events: {
            'change [is-maximum-exposure]': 'setMaximumExposure',
            'change [likelihood-value]': 'likelihoodValidation',
            'change [for-validation]': 'currencyValidation',
            'change [validate-max-expose]': 'currencyValidationMaximumExposure',
            'keyup [exposure-value]': 'moveIntoNextInputField',
            'keyup [data-max-exposure-value]': 'moveIntoNextInputFieldMaxExpValue',
            'click [btnApprove]': 'sendApprove',
            'click [btnDraft]': 'sendDraft'
        },
        fetchStageTahun: function () {
            var self = this;
            this.modelStageTahunRiskMatrix.fetch({
                reset: true,
                data: {
                    id: this.project.Id
                },
                success: function (req, res) {
                    self.setStageTahunValue(res);
                }
            });
        },
        generateStageTahunData: function (data) {
            var self = this;
            var stageTahunRiskMatrix = [];
            if (data.StageTahunRiskMatrix) {
                _.each(data.StageTahunRiskMatrix, function (item) {
                    var stageYear = {};
                    stageYear.Id = item.Id;
                    stageYear.StageId = item.StageId;
                    stageYear.Tahun = item.Tahun;
                    stageTahunRiskMatrix.push(stageYear);
                });
            }
            return stageTahunRiskMatrix;
        },
        setStageTahunValue: function (data) {
            var self = this;
            var data = data.StageValue;
            if (data.length > 0) {
                $.each(data, function (index, item) {
                    var stageId = item.StageId;
                    self.$('[name="data-start-stage-' + stageId + '"]').text(item.Values[0]);
                    self.$('[name="data-end-stage-' + stageId + '"]').text(item.Values[1]);
                });
            }
        },
        renderTableRiskMatrix: function () {
            var self = this;
            var stageTahunRiskMatrix = this.riskMatrixProject;
            var html = '<div class="col-md-12 scrollable-matrix" obo-table-matrix>'
            html += '<div>'
            html += '<table style="width:100%" class="table-risk-matrix">'
            html += '<tr class="td-risk-matrix"></tr>'
            html += '<tr class="td-risk-matrix">'
            html += '<td colspan="2"></td>'
            // start looping for StageTahunRiskMatrix
            _.each(stageTahunRiskMatrix, function (model, i) {
                html += '<td data-year class="td-risk-matrix" data="' + model.Id + '" style="text-align: center">'
                html += '<label>' + model.Tahun + ' (' + model.Stage.NamaStage + ')</label>'
                html += '</td>'
                html += '<td data-year-id class="hidden">'
                html += '<label class="hidden">' + model.Id + '</label>'
                html += '</td>'
            });
            html += '</tr>'
            // end looping for StageTahunRiskMatrix

            // start looping for ProjectRiskStatus
            let attrId = 0;
            _.each(this.project.Project.ProjectRiskStatus, function (modelRiskRegistrasi) {
                html += '<tr id="" class="td-risk-matrix">'
                html += '<td id="' + modelRiskRegistrasi.RiskRegistrasiId + '" data-risk-registrasi class="td-risk-matrix" style="text-align: center">'
                html += '<label>' + modelRiskRegistrasi.KodeMRisk + '</label>'
                html += '<input data-risk-registrasi-id value="' + modelRiskRegistrasi.RiskRegistrasiId + '" class="hidden">'
                html += '</td>'
                html += '<td class="td-risk-matrix" style="height: 30px; text-align: center">Exposure</td>'
                _.each(stageTahunRiskMatrix, function (model) {

                    if (modelRiskRegistrasi.IsProjectUsed) {
                        attrId += 1;
                        var currentRiskId = modelRiskRegistrasi.RiskRegistrasiId;
                        var expValue = 0;
                        if (model.RiskMatrixValue) {
                            $.grep(model.RiskMatrixValue, function (item) {
                                if (item.RiskRegistrasiId == currentRiskId) {
                                    expValue = item.Values[0];
                                }
                            })
                        }
                        if (model.RiskMatrixValue) {
                            const riskFiltered = model.RiskMatrixValue.filter((value) => {
                                return value.RiskRegistrasiId === currentRiskId;
                            });
                            const isExposureChanged = riskFiltered[0].Colors[0];
                            if (isExposureChanged) {
                                html += '<td exposure-container-' + modelRiskRegistrasi.Id + ' class="td-risk-matrix text-right" style="height: 30px; background-color:yellow; color: black;">'
                                html += '<input type="text" tabIndex="' + attrId + '" class="text-right" exposure-value lang="' + model.Id + '" is-maximum-exposure maximum-exposure-for="' + model.Id + '" for-validation data-risk="' + modelRiskRegistrasi.RiskRegistrasiId + '-ForYear-' + model.Id + '" id="exp-' + model.Id + '" value="' + expValue + '" name="Exposure-' + modelRiskRegistrasi.RiskRegistrasiId + '-ForYear-' + model.Id + '" style="width: 80px; border: none; margin-right: 5px; margin-left: 5px; background-color: yellow;">'
                                html += '</td>'
                            } else {
                                html += '<td exposure-container-' + modelRiskRegistrasi.Id + ' class="td-risk-matrix text-right" style="height: 30px;">'
                                html += '<input type="text" tabIndex="' + attrId + '" class="text-right" exposure-value lang="' + model.Id + '" is-maximum-exposure maximum-exposure-for="' + model.Id + '" for-validation data-risk="' + modelRiskRegistrasi.RiskRegistrasiId + '-ForYear-' + model.Id + '" id="exp-' + model.Id + '" value="' + expValue + '" name="Exposure-' + modelRiskRegistrasi.RiskRegistrasiId + '-ForYear-' + model.Id + '" style="width: 80px; border: none; margin-right: 5px; margin-left: 5px;">'
                                html += '</td>'
                            }
                        }
                    } else {
                        html += '<td exposure-container-' + modelRiskRegistrasi.Id + ' class="td-risk-matrix text-right" style="height: 30px;background-color: #ebebe4;">'
                        html += '<input type="text" class="text-right" data-risk="' + modelRiskRegistrasi.RiskRegistrasiId + '-ForYear-' + model.Id + '" id="exp-' + model.Id + '" value="0" name="Exposure" style="width: 80px; border: none;  margin-right: 5px; margin-left: 5px;" disabled>'
                        html += '</td>'
                    }
                });
                html += '</tr>'

                html += '<tr id="" class="td-risk-matrix">'
                html += '<td class="td-risk-matrix" style="text-align: center">' + modelRiskRegistrasi.KodeMRisk + '</td>'
                html += '<td class="td-risk-matrix" style="height: 30px; text-align: center">Parameter</td>'
                _.each(stageTahunRiskMatrix, function (modelStage) {
                    if (modelRiskRegistrasi.IsProjectUsed) {
                        if (modelStage) {
                            const riskFiltered = modelStage.RiskMatrixValue.filter((value) => {
                                return value.RiskRegistrasiId === modelRiskRegistrasi.RiskRegistrasiId;
                            });
                            const isLikelihoodChanged = riskFiltered[0].Colors[1];
                            if (isLikelihoodChanged) {
                                html += '<td id="likehoodfor-' + modelStage.Id + '" class="td-risk-matrix" style="height: 30px;text-align: center; background-color:yellow; color: black;">'
                                html += '<select name="DefinisiLikehoodFor-' + modelStage.Id + '-RiskFor-' + modelRiskRegistrasi.RiskRegistrasiId + '" style="background-color:yellow;">'
                                _.each(self.project.Scenario.Likehood.LikehoodDetail, function (model) {
                                    if (modelStage.RiskMatrixValue) {
                                        $.grep(modelStage.RiskMatrixValue, function (item) {
                                            if (modelRiskRegistrasi.RiskRegistrasiId == item.RiskRegistrasiId) {
                                                if (model.Id == item.Values[1]) {
                                                    html += '<option value="' + model.Id + '" selected>' + model.DefinisiLikehood + '</option>'
                                                }
                                                else {
                                                    html += '<option value="' + model.Id + '">' + model.DefinisiLikehood + '</option>'
                                                }
                                            }
                                        });
                                    }
                                    // html += '<option value="' + model.Id + '">' + model.DefinisiLikehood + '</option>'
                                });
                                html += '</select>'
                                html += '</td>'
                            } else {
                                html += '<td id="likehoodfor-' + modelStage.Id + '" class="td-risk-matrix" style="height: 30px;text-align: center">'
                                html += '<select name="DefinisiLikehoodFor-' + modelStage.Id + '-RiskFor-' + modelRiskRegistrasi.RiskRegistrasiId + '">'
                                _.each(self.project.Scenario.Likehood.LikehoodDetail, function (model) {
                                    if (modelStage.RiskMatrixValue) {
                                        $.grep(modelStage.RiskMatrixValue, function (item) {
                                            if (modelRiskRegistrasi.RiskRegistrasiId == item.RiskRegistrasiId) {
                                                if (model.Id == item.Values[1]) {
                                                    html += '<option value="' + model.Id + '" selected>' + model.DefinisiLikehood + '</option>'
                                                }
                                                else {
                                                    html += '<option value="' + model.Id + '">' + model.DefinisiLikehood + '</option>'
                                                }
                                            }
                                        });
                                    }
                                    // html += '<option value="' + model.Id + '">' + model.DefinisiLikehood + '</option>'
                                });
                                html += '</select>'
                                html += '</td>'
                            }
                        }
                    } else {
                        html += '<td id="likehoodfor-' + modelStage.Id + '" class="td-risk-matrix" style="height: 30px;text-align: center">'
                        html += '<select name="DefinisiLikehoodFor-' + modelStage.Id + '-RiskFor-' + modelRiskRegistrasi.RiskRegistrasiId + '">'
                        html += '<option value="0">-</option>'
                        html += '</select>'
                        html += '</td>'
                    }
                });
                html += '</tr>'
            });
            // end looping for ProjectRiskStatus

            html += '<tr id="" class="td-risk-matrix">'
            html += '<td colspan="2" class="td-risk-matrix" style="height: 30px; text-align: center; background: #e8e8e8; font-weight: bold;">Maximum Exposure</td>'
            // start looping for Maximum Expose
            _.each(stageTahunRiskMatrix, function (model) {
                attrId += 1;
                if (model.IsUpdate) {
                    html += '<td id="year-' + model.Id + '" class="td-risk-matrix" style="height: 30px;text-align: center; background-color:yellow; color: black;">'
                    html += '<input type="text" tabIndex="' + attrId + '" class="text-right" validate-max-expose lang="' + model.Id + '" data-risk="" id="max-exposure-' + model.Id + '" name="max-exposure-' + model.Id + '" data-max-exposure-value value="' + model.MaximumNilaiExpose + '" style="width: 80px; border: none; text-align: center; font-weight: bold;  margin-right: 5px; margin-left: 5px; background-color:yellow; color: black;" placeholder="...">'
                    html += '</td>'
                } else {
                    html += '<td id="year-' + model.Id + '" class="td-risk-matrix" style="height: 30px;text-align: center">'
                    html += '<input type="text" tabIndex="' + attrId + '" class="text-right" validate-max-expose lang="' + model.Id + '" data-risk="" id="max-exposure-' + model.Id + '" name="max-exposure-' + model.Id + '" data-max-exposure-value value="' + model.MaximumNilaiExpose + '" style="width: 80px; border: none; text-align: center; font-weight: bold;  margin-right: 5px; margin-left: 5px;" placeholder="...">'
                    html += '</td>'
                }
            });
            // end looping for Maximum Expose

            html += '</tr>'

            html += '</table>'
            html += '</div>'
            html += '</div>'

            this.$('[obo-table-matrix]').append(html);
            this.$('[name="Keterangan"]').val(this.keterangan);
            // self.setTemplate();
        },
        renderStage: function () {
            var self = this;
            var project = this.project.Project.NamaProject;
            var startProject = moment(this.project.Project.TahunAwalProject).format('DD-MMM-YYYY');
            var endProject = moment(this.project.Project.TahunAkhirProject).format('DD-MMM-YYYY');
            var namaScenario = this.namaScenario;
            this.startProjectFormated = startProject;
            this.endProjectFormated = endProject;
            this.projectName = project;
            this.$('[name="NamaProject"]').text(project);
            this.$('[name="NamaScenario"]').text(namaScenario);
            this.$('[name="TahunAwalProject"]').text(startProject);
            this.$('[name="TahunAkhirProject"]').text(endProject);
        },
        renderLikelihood: function () {
            var likelihoodId = this.project.Scenario.LikehoodId;
            if (likelihoodId) {
                this.$('[obo-table-likelihood]').append(this.tableLikelihood.el);
                this.tableLikelihood.render();
                this.tableLikelihood.collection.fetch({
                    reset: true,
                    data: {
                        ParentId: likelihoodId,
                        PageSize: 100
                    }
                });
            }
        },
        renderRiskCategory: function () {
            this.collectionRisk.fetch({
                reset: true,
                data: {
                    IsPagination: false
                }
            });
        },
        getRiskRegistrasi: function () {
            var riskRegistrasi = [];
            this.$('[data-risk-registrasi-id]').each(function () {
                var val = this.value;
                riskRegistrasi.push(val);
            });
            return riskRegistrasi;
        },
        getStageTahunRiskMatrix: function () {
            var years = [];
            this.$('[data-year-id]').each(function () {
                var val = this.innerText;
                var id = val.trim();
                years.push(id);
            });
            return years;
        },
        setMaximumExposure: function (dom) {
            var self = this;
            var yearId = dom.currentTarget.lang;
            var tempValue = [];
            this.$("[maximum-exposure-for='" + yearId + "']").each(function (i, item) {
                tempValue.push(parseFloat(item.value.replace(/,/g, '')));
            });
            var maxValue = Math.max.apply(Math, tempValue);
            this.$('#max-exposure-' + yearId + '').val(maxValue);
        },
        formStage: function (data) {
            var self = this;
            if (data.results) {
                for (var i = 0; i < data.results.length; i++) {
                    var namaStage = data.results[i].NamaStage;
                    var idStage = data.results[i].Id;
                    var html = '<tr>'
                    html += '<td style="width: 10%;">'
                    html += '<label>' + namaStage + '</label>'
                    html += '</td>'
                    html += '<td style="width: 10%;">'
                    html += '<label>=</label>'
                    html += '</td>'
                    html += '<td style="width: 10%;">'
                    html += '<label name="data-start-stage-' + idStage + '"></label>'
                    html += '</td>'
                    html += '<td style="width: 10%;">'
                    html += '<label>s/d</label>'
                    html += '</td>'
                    html += '<td style="width: 10%;">'
                    html += '<label name="data-end-stage-' + idStage + '"></label>'
                    html += '</td>'
                    html += '</tr>'
                    self.$('[tab-content]').append(html);
                }
            }
        },
        moveIntoNextInputField: function (e) {
            const attrId = e.target.tabIndex;
            let nextAttrId = attrId + 1;
            if (e.keyCode == 9) {
                this.$('[tabindex="' + nextAttrId + '"]').focus();
                this.$('[tabindex="' + nextAttrId + '"]').addClass('enable-current-input-field');
                this.$('[tabindex="' + attrId + '"]').removeClass('enable-current-input-field');
            }
        },
        moveIntoNextInputFieldMaxExpValue: function (e) {
            const attrMaxExpId = e.target.tabIndex;
            let nextAttrId = attrMaxExpId + 1;
            if (e.keyCode == 9) {
                this.$('[tabindex="' + nextAttrId + '"]').focus();
            }
        },
        currencyValidation: function (e) {
            var self = this;
            var exposeFormat = /^\s*[0-9]+([\.][0-9][0-9]{0,12})?$|^$|^\s*$/;
            var attrName = e.target.name;
            var attrValue = this.$('[name="' + attrName + '"]').val();
            if (exposeFormat.test(attrValue)) {
                this.$('[name="' + attrName + '"]').removeClass('invalidCurrencyFormat');
            } else {
                commonFunction.responseWarningCannotExecute("Nilai Expose tidak sesuai format. Format yang dizinkan 1000.02 / 123.39847. Angka di belakang koma hanya diizinkan maksimal sebanyak 13 digit.");
                this.$('[name="' + attrName + '"]').addClass('invalidCurrencyFormat');
            }
        },
        currencyValidationMaximumExposure: function (e) {
            var exposeFormat = /^\s*[0-9]+([\.][0-9][0-9]{0,12})?$|^$|^\s*$/;
            var attrName = e.target.name;
            var attrValue = this.$('[name="' + attrName + '"]').val();
            if (exposeFormat.test(attrValue)) {
                this.$('[name="' + attrName + '"]').removeClass('invalidCurrencyFormat');
            } else {
                commonFunction.responseWarningCannotExecute("Nilai Maksimum Expose tidak sesuai format. Format yang dizinkan 1000.02 / 123.39847. Angka di belakang koma hanya diizinkan maksimal sebanyak 13 digit.");
                this.$('[name="' + attrName + '"]').addClass('invalidCurrencyFormat');
            }
        },
        generateTemplateNotification: function () {
            const url = commonFunction.getCurrentDomainAndPort();
            var projectName = this.$('[name="NamaProject"]').val();
            var startPc = "";
            var endPc = "";
            var startCn = "";
            var endCn = "";
            var startOp = "";
            var endOp = "";
            var statusApproval = this.$('[name="StatusId"]').val();
            var keterangan = this.$('[name="Keterangan"]').val();

            var html = '<div>'
            html += '<div style="font-size: 9pt;color:#373637;">'
            if (statusApproval == 2) {
                html += '<p style="margin-bottom:0px;">Dengan ini saya <b>meyetujui</b> permohonan atas Matriks Risiko dengan data berikut: </p>'
            } else {
                html += '<p style="margin-bottom:0px;">Dengan ini saya <b>tidak meyetujui</b> permohonan atas Matriks Risiko  dengan data berikut: </p>'
            }
            html += '<p style="font-style: italic;margin-top:0px;margin-bottom:0px;">Nama Proyek: ' + this.projectName + '</p>'
            html += '<p style="font-style: italic;margin-top:0px;margin-bottom:0px">Periode Mulai Proyek: ' + this.startProjectFormated + '</p>'
            html += '<p style="font-style: italic;margin-top:0px;">Periode Akhir Proyek: ' + this.endProjectFormated + '</p>'
            // html += '<p>PC= '+ startPc +' s/d '+ endPc +'</p>'
            // html += '<p>CN= '+ startCn +' s/d '+ endCn +'</p>'
            // html += '<p>OP= '+ startOp +' s/d '+ endOp +'</p>'
            html += '</div>'
            html += '<div>'
            html += '<p>Keterangan: ' + keterangan + '</p>'
            html += '</div>'
            html += '<div>'
            if (statusApproval == "2") {
                html += '<p>Mohon untuk dilakukan approval, data detail dapat dilihat melalui aplikasi dengan menekan <span><a href="' + url + '/#approval">link ini</a></span> dan masuk  berdasarkan login masing-masing.</p>'
            } else {
                html += '<p>Untuk menganti sesuai keterangan, silakan masuk melalui aplikasi dengan menekan <span><a href="' + url + '/#approval">link ini</a></span> berdasarkan login masing-masing.</p>'
            }
            html += '<p>Terimakasih.</p>'
            html += '</div>'
            html += '</div>'

            return html;
        },
        getConfirmation: function (dom) {
            var self = this;
            //validate Maximum Nilai Expose
            var isValidated = true;
            this.$('[exposure-value]').each(function (i, item) {
                if (item.value == "") {
                    isValidated = false;
                    commonFunction.responseWarningCannotExecute("Nilai Expose wajib diisi.");
                    self.$('[name="' + item.name + '"]').addClass('invalidCurrencyFormat');
                } else {
                    self.$('[name="' + item.name + '"]').removeClass('invalidCurrencyFormat');
                }
            });
            if (isValidated)
                this.confirmed();
        },
        confirmed: function () {
            var self = this;
            //validate all input type that fillable
            var isValidated = true;
            var statusApproval = this.$('[name="StatusId"]').val();
            var action = "";
            if (statusApproval == 2) {
                action = "menyetujui";
            } else {
                action = "tidak menyetujui";
            }
            if (isValidated) {
                var templateNotif = this.generateTemplateNotification();
                var retVal;
                if  (this.isSended == true){
                    retVal = confirm("Apakah anda yakin untuk " + action + " Matriks Risiko ini?" + "\n\n\Matriks Risiko otomatis akan dilanjutkan ke proses persetujuan / approval. Sehingga Skenario ini tidak bisa diubah lagi.");
                }
                if (this.isSended == false){
                    retVal = confirm("Apakah anda yakin untuk " + action + " Matriks Risiko ini?" + "\n\n\Matriks Risiko membutuhkan pengajuan persetujuan terpisah.");
                }
                if (retVal == true) {
                    this.saveRiskMatrix(templateNotif);
                } else {
                    this.$('[btn-save]').attr('disabled', false);
                }
            } else {
                commonFunction.exposureRiskMatrixValidation('SALAHHH');
            }
        },
        reCalculateMaximuExpose: function (year) {
            var self = this;
            var tempValue = [];
            this.$("[maximum-exposure-for='" + year + "']").each(function (i, item) {
                tempValue.push(parseFloat(item.value.replace(/,/g, '')));
            });
            var maxValue = Math.max.apply(Math, tempValue);
            return maxValue;
        },
        submitNotification: function () {
            var isSend = this.$('[name="IsSend"]').prop('checked');
            if (isSend) {
                commonFunction.responseWarningCannotExecute("Matriks Risiko otomatis akan dilanjutkan ke proses persetujuan / approval selanjutnya (jika ada) jika menekan tombol Simpan. Sehingga Matriks Risiko ini tidak bisa diubah lagi.");
            } else {
                commonFunction.responseWarningCannotExecute("Matriks Risiko membutuhkan pengajuan persetujuan selanjutnya (jika ada) secara terpisah setelah menekan tombol Simpan.");
            }
        },
        sendApprove: function () {
            this.isSended = true;
            this.getConfirmation();
        },
      
        sendDraft: function () {
            this.isSended = false;
            this.getConfirmation();
        },
        saveRiskMatrix: function (template) {
            commonFunction.showLoadingSpinner();
            var self = this;
            var stageTahunRiskMatrixIds = this.getStageTahunRiskMatrix();
            var riskRegistrasi = this.getRiskRegistrasi();
            var data = {};
            var riskMatrixCollection = [];
            for (var i = 0; i < stageTahunRiskMatrixIds.length; i++) {
                var stageTahunRiskMatrix = {};
                var year = stageTahunRiskMatrixIds[i];
                var riskMatrixValue = [];

                stageTahunRiskMatrix.StageTahunRiskMatrixId = year;
                for (var e = 0; e < riskRegistrasi.length; e++) {
                    var riskMatrixValueItem = {};
                    var riskId = riskRegistrasi[e];
                    riskMatrixValueItem.RiskRegistrasiId = riskId;

                    var value = [];
                    var exposureValue = self.$('[data-risk="' + riskId + '-ForYear-' + year + '"]').val();
                    value.push(exposureValue);
                    var likelihoodDetailId = self.$('[name="DefinisiLikehoodFor-' + year + '-RiskFor-' + riskId + '"]').val();
                    value.push(likelihoodDetailId);

                    riskMatrixValueItem.Values = value;

                    riskMatrixValue.push(riskMatrixValueItem);
                }
                stageTahunRiskMatrix.RiskMatrixValue = riskMatrixValue;
                var maximumExposePerYear = this.$('#max-exposure-' + year + '').val();
                if (maximumExposePerYear != "") {
                    var maxValue = this.$('#max-exposure-' + year + '').val();
                    var maxValueFormated = parseFloat(maxValue.replace(/,/g, ''));
                    stageTahunRiskMatrix.MaximumNilaiExpose = maxValueFormated;
                } else {
                    var maxValue = self.reCalculateMaximuExpose(year);
                    stageTahunRiskMatrix.MaximumNilaiExpose = maxValue;
                }
                riskMatrixCollection.push(stageTahunRiskMatrix);
            }
            data.RiskMatrixCollection = riskMatrixCollection;
            data.RiskMatrixProjectId = this.project.Id;
            data.Keterangan = this.$('[name="Keterangan"]').val();
            data.StatusId = this.$('[name="StatusId"]').val();
            data.IsSend = this.isSended;
            data.TemplateNotif = template;
            this.model.save(data);
            commonFunction.showLoadingSpinner();
            this.$('[btnDraft]').addClass('disabled');
            this.$('[btnApprove]').addClass('disabled');
            eventAggregator.trigger('approval/risk_matrix/:refecth');
        }
    });
});