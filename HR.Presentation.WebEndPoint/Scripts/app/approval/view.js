define(function (require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');
    var Table = require('./table/table');
    var Collection = require('./collection');
    var CollectionSent = require('./sent-item/collection');
    var TableSent = require('./sent-item/table/table');
    var Paging = require('paging');
    var PagingSent = require('paging');
    var TableRiskMatrix = require('./table_riskmatrix/table');
    var PagingRiskMatrix = require('paging');
    var TableCorrelatedSektor = require('./table_correlated_sektor/table');
    var PagingCorrelatedSektor = require('paging');
    var TableCorrelatedProject = require('./table_correlated_project/table');
    var PagingCorrelatedProject = require('paging');

    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content tbl bg-white',
        template: _.template(template),
        initialize: function () {
            var self = this;
            this.keyword = "";

            this.table = new Table({
                collection: new Collection()
            });
            this.paging = new Paging({
                collection: this.table.collection
            });

            this.tableSent = new TableSent({
                collection: new CollectionSent()
            });
            this.pagingSent = new PagingSent({
                collection: this.tableSent.collection
            });

            this.listenTo(eventAggregator, 'approval/correlated_sector/approval:fecth', function () {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'approval/correlated_project/approval:fecth', function () {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'approval/risk_matrix/approval:fecth', function () {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'approval/edit:fecth', function () {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'approval/scenario/approval:fecth', function () {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'approval/scenario/:refecth', function () {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'approval/risk_matrix/:refecth', function () {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'approval/correlated_sector/:refecth', function () {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'approval/correlated_project/:refecth', function () {
                self.fetchData();
            });
        },
        events : {
            'keyup #keyword': 'applyFilter',
            'click [name="btn-filter"]': 'applyFilter',
            'keyup #keywordInbox': 'applyFilterInbox',
            'click [name="btn-filter-inbox"]': 'applyFilterInbox'
        },
        afterRender: function () {
            this.$('[obo-table-approval]').append(this.table.el);
            this.table.render();

            this.insertView('[obo-paging]', this.paging);
            this.paging.render();

            this.$('[obo-table-approval-sent]').append(this.tableSent.el);
            this.tableSent.render();

            this.insertView('[obo-paging-sent]', this.pagingSent);
            this.pagingSent.render();

            this.fetchData();
            this.fetchDataSent();
        },
        fetchDataSent: function() {
            var param = [];
            param.Search = this.keyword;
            this.tableSent.collection.fetch({
                reset: true,
                data: param,
                success: function (req, res) { }
            })
        },
        fetchData: function () {
            var param = [];
            param.Search = this.keyword;
            this.table.collection.fetch({
                reset: true,
                data: param,
                success: function (req, res) { 
                    console.log(res);
                }
            })
        },
        applyFilter: function() {
            var self = this;
            var param  = []
            var keyword = this.$('#keyword').val();     
            this.keyword = keyword;
            param.Search = keyword;
            var keyIndex = this.$('#keyIndex option:selected').val();
            param.SearchBy = keyIndex;
            var keyIndex2 = this.$('#keyIndex2 option:selected').val();
            param.SearchBy2 = keyIndex2;
            this.tableSent.collection.fetch({
                reset: true,
                data: param
            })
        },
        applyFilterInbox: function() {
            var self = this;
            var param  = []
            var keyword = this.$('#keywordInbox').val();     
            this.keyword = keyword;
            param.Search = keyword;
            var keyIndex = this.$('#keyIndexInbox option:selected').val();
            param.SearchBy = keyIndex;
            var keyIndex2 = this.$('#keyIndexInbox2 option:selected').val();
            param.SearchBy2 = keyIndex2;
            this.table.collection.fetch({
                reset: true,
                data: param
            })
        }
    });
});