define(function (require, exports, module) {
    'use strict';
    var Table = require('table-2.table');
    var template = require('text!./table.html');
    var Tbody = require('./tbody');
    var eventAggregator = require('eventaggregator');

    module.exports = Table.extend({
        template: _.template(template),
        events: {
            'click #selectall': 'changeValue'
        },
        changeValue: function (e) {
            let isSelectedAll = false;
            if (this.$('[type="checkbox"]').prop('checked')) {
                this.$('input[type="checkbox"]').prop('checked', true);
                this.$('[data-item]').prop('disabled', true);
                isSelectedAll = true;
            } else {
                this.$('[type="checkbox"]').prop('checked', false);
                this.$('[data-item]').prop('disabled', false);
            }
            eventAggregator.trigger('approval/scenario/select_project:isSelectedAll', isSelectedAll);
        },
        Tbody: Tbody,
        regions: {
            body: {
                el: 'tbody',
                replaceElement: true
            }
        }
    });
});
