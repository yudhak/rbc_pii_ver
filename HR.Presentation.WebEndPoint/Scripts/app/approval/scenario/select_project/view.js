define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var commonFunction = require('commonfunction');
  var Table = require('./table/table');
  var Collection = require('./collection');
  var Paging = require('paging');
  var eventAggregator = require('eventaggregator');


  module.exports = View.extend({
    template: _.template(template),
    initialize: function (options) {
      var self = this;
      var isEdited = options.model.get('isAlreadyEdited');
      var ids = null;
      this.isSelectedAll = false;
      console.log(options);
      var projectIds = [];
      if (isEdited) {
        ids = options.view;
        _.each(ids, function (item) {
          projectIds.push(item.attributes.Id);
        });
      } else {
        ids = options.view;
        _.each(ids, function (item) {
          projectIds.push(item.Id);
        });
      }

      this.table = new Table({
        collection: new Collection()
      });
      var originalParse = this.table.collection.parse;
      this.table.collection.parse = function (response) {
        var result = originalParse.call(this, response);
        if (projectIds.length > 0) {
          _.each(result, function (item) {
            var found = _.find(projectIds, (id) => {
              return id == item.Id
            });
            item.isChecked = Boolean(found);
          });
          return result;
        } else {
          _.each(result, function (item) {
            var selectedId = self.model.get('ProjectSelected') || [];

            var found = _.find(selectedId, (id) => {
              return id == item.Id
            });
            item.isChecked = Boolean(found);
          });
          return result;
        }
      }

      this.paging = new Paging({
        collection: this.table.collection
      });
      this.on('cleanup', function () {
        this.table.destroy();
        this.modalDialog && this.modalDialog.remove && this.modalDialog.remove();
      }, this)
      this.listenTo(eventAggregator, 'approval/scenario/select_project:isSelectedAll', function (obj) {
        self.isSelectedAll = obj;
        self.model.set('IsSelectedAll', self.isSelectedAll);
        if (!self.isSelectedAll) {
          self.resetProjectSelected();
        } else {
          self.setSelectAllProject();
        }
      });
    },
    afterRender: function () {
      this.$('[project-table]').append(this.table.el);
      this.table.render();

      this.table.collection.fetch({
        reset: true,
        data: {
          IsPagination: false,
          PageSize: 100
        }
      });
    },
    events: {
      // 'click [name="allProject"]': 'selectRemoveAllRisk',
      'click [btn-save-chosen-risk]': 'getChosenRisk',
      //'click [check-all]': 'selectAllProject'
    },
    selectAllProject: function () {
      var status = this.$('input[type="checkbox"]').prop('checked');
      this.$('input[type="checkbox"]').prop('checked', true);
    },
    resetProjectSelected: function () {
      this.table.collection.map((model) => {
        return model.set('isChecked', false);
      });
    },
    setSelectAllProject: function () {
      this.table.collection.map((model) => {
        return model.set('isChecked', true);
      });
    },
    selectRemoveAllRisk: function () {
      var status = this.$('[name="allProject"]').prop('checked')
      if (status) {
        this.$('input[type="checkbox"]').prop('checked', true);
      } else {
        this.$('input[type="checkbox"]').prop('checked', false);
      }
    },
    getChosenRisk: function () {
      var self = this;
      var projectSelectedAfterEdit = [];
      var collection = this.table.collection.filter((model) => {
        var isSelected = model.get('isChecked');
        if (isSelected)
          projectSelectedAfterEdit.push(model);

        return isSelected;
      });
      // this.model.set('ProjectSelected', collection.map(
      //   (model) => {
      //     return model.get('Id')
      //   }))
      // this.model.set('ProjectNameSelected', collection.map(
      //   (model) => {
      //     return model.get('NamaProject')
      //   }))
      eventAggregator.trigger('approval/scenario/edit/select_project:project_selected', projectSelectedAfterEdit);
      this.$el.modal('hide');
    }
  });
});