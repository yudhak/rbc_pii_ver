define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var Model = require('./../model');
  var commonFunction = require('commonfunction');
  var commonConfig = require('commonconfig');
  var eventAggregator = require('eventaggregator');
  var ModelScenario = require('../../../app/scenario/model');
  var TableLikelihood = require('../../../app/scenario/select_likelihood/detail/table/table');
  var CollectionLikehoodDetail = require('../../../app/scenario/select_likelihood/detail/collection');
  var TableProject = require('./select_project/table/table');
  var CollectionProject = require('./select_project/collection');
  require('bootstrap-validator');

  module.exports = View.extend({
    template: _.template(template),
    events: {
      'click [name="SelectProject"]': 'selectProject',
      'click [name="SelectLikelihood"]': 'selectLikelihood',
      'click [btnApprove]': 'sendApprove',
      'click [btnDraft]': 'sendDraft'
    },
    initialize: function (options) {
      var self = this;
      this.IsSended = null;
      this.requestorName = options.model.get('NamaPembuat');
      this.projectSelected = [];
      this.isAlreadyEdited = false;
      this.projectSelectedAfterEdited = null;
      this.isDataAvailable = 0;
      this.scenario = null;
      this.scenarioId = options.model.get('RequestId');
      var scenarioId = options.model.get('RequestId');
      this.keterangan = options.model.get('Keterangan');
      this.newLikelihoodName = null;
      this.likelihoodDetail = null;
      this.likelihoodId = 0;
      if (options.model.get('Scenario.Likehood.LikehoodDetail'))
        this.likelihoodDetail = options.model.get('Scenario.Likehood.LikehoodDetail');
      this.model = new Model();
      this.model.set(this.model.idAttribute, options.model.get('Id'));
      this.modelScenario = new ModelScenario();
      this.tableLikelihood = new TableLikelihood({
        collection: new CollectionLikehoodDetail()
      });
      this.fetchScenario(scenarioId);
      this.tableProject = new TableProject({
        collection: new CollectionProject()
      });
      this.listenTo(eventAggregator, 'approval/scenario/select_likelihood:likelihood_selected', function (obj) {
        self.likelihoodId = obj.Id;
        self.renderLikelihoodDetailList();
      });
      this.listenTo(eventAggregator, 'approval/scenario/edit/select_project:project_selected', function (obj) {
        self.modelScenario.set('isAlreadyEdited', true);
        self.isAlreadyEdited = true;
        self.projectSelected = obj;
        if (obj) {
          _.each(obj, function (item) {
            self.projectIdEdited.push(item.attributes.Id);
          });
        }
        self.renderProjectListAfterEdited();
      });
      this.listenTo(this.model, 'sync error', function () { 
        debugger;
        eventAggregator.trigger('approval/scenario/approval:fecth');
      });
      this.listenTo(this.model, 'sync', function (model) {
        commonFunction.responseSuccessUpdateAddDelete('Approval berhasil.');
        self.$el.modal('hide');
        eventAggregator.trigger('approval/scenario/approval:fecth');
      });
      this.listenToOnce(this.modelScenario, 'sync', function (model) {
        this.render();
      });
      this.once('afterRender', function () {
        this.modelScenario.fetch();
      });
    },
    afterRender: function () {
      // this.setTemplate();
      commonFunction.closeLoadingSpinner();
    },
    fetchScenario: function (id) {
      var self = this;
      this.scenario = this.modelScenario.fetch({
        reset: true,
        data: {
          Id: id
        },
        success: function (req, res) {
          self.scenario = res;
          self.setTemplate();
        }
      });
    },
    setTemplate: function () {
      var self = this;
      this.isDataAvailable += 1;
      if (this.isDataAvailable >= 1) {
        var data = this.scenario;
        this.$('[name="NamaScenario"]').val(data.NamaScenario);
        this.$('[name="NamaLikehood"]').val(data.NamaLikehood);
        this.$('[name="NamaPembuat"]').val(this.requestorName);
        this.$('[name="Keterangan"]').val(this.keterangan);
        this.renderLikelihoodDetailList();
        this.renderProjectList();
      }
    },
    selectProject: function () {
      var self = this;
      this.projectIdEdited = [];
      require(['./select_project/view'], (View) => {
        commonFunction.setDefaultModalDialogFunction(this, View, this.modelScenario, self.projectSelected);
      });
    },
    selectLikelihood: function () {
      require(['./select_likelihood/view'], (View) => {
        commonFunction.setDefaultModalDialogFunction(this, View, this.model);
      });
    },
    renderProjectListAfterEdited: function () {
      var self = this;
      this.$('[project-table]').empty();
      var html = '<table>'
      html += '<tr class="header-kiw-table">'
      html += '<td class="td-kiw-table">Nama Proyek</td>'
      html += '<td class="td-kiw-table">Sektor</td>'
      html += '<td class="td-kiw-table">Kategori Risiko</td>'
      html += '</tr>'
      $.each(this.projectSelected, function (i, item) {
        var riskSelected = '';
        if (item.attributes.RiskRegistrasi) {
          $.each(item.attributes.RiskRegistrasi, function (e, val) {
            riskSelected += val.KodeMRisk;
            riskSelected += ', ';
          })
        }
        html += '<tr class="row-kiw-table" id="' + item.attributes.Id + '">'
        html += '<td class="td-kiw-table"> ' + item.attributes.NamaProject + ' </td>'
        html += '<td class="td-kiw-table"> ' + item.attributes.NamaSektor + ' </td>'
        html += '<td class="td-kiw-table"> ' + riskSelected + ' </td>'
        html += '<td>'
        html += '</tr>'
      });
      html += '</table>'

      this.$('[project-table]').append(html);

      var htmlNotif = '<table style="width:100%;border-collapse: collapse;font-family: Arial,sans-serif;font-size: 9pt;color:#373637;">'
      htmlNotif += '<tr style="border: 1px solid #c9c4c4;background-color: #eeebeb;">'
      htmlNotif += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">Nama Proyek</td>'
      htmlNotif += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">Sektor</td>'
      htmlNotif += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">Kategori Risiko</td>'
      htmlNotif += '</tr>'
      $.each(this.projectSelected, function (i, item) {
        var riskSelected = '';
        if (item.attributes.RiskRegistrasi) {
          $.each(item.attributes.RiskRegistrasi, function (e, val) {
            riskSelected += val.KodeMRisk;
            riskSelected += ', ';
          })
        }
        htmlNotif += '<tr style="border: 1px solid #c9c4c4;" id="' + item.attributes.Id + '">'
        htmlNotif += '<td style="border: 1px solid #c9c4c4;padding: 5px;"> ' + item.attributes.NamaProject + ' </td>'
        htmlNotif += '<td style="border: 1px solid #c9c4c4;padding: 5px;"> ' + item.attributes.NamaSektor + ' </td>'
        htmlNotif += '<td style="border: 1px solid #c9c4c4;padding: 5px;"> ' + riskSelected + ' </td>'
        htmlNotif += '</tr>'
      });
      htmlNotif += '</table>'
      this.projectHtml = htmlNotif;
    },
    generateTemplateNotification: function () {
      var self = this;
      var parameterName = this.$('[name="NamaLikehood"]').val();
      var scenarioName = this.$('[name="NamaScenario"]').val();
      var keterangan = this.$('[name="Keterangan"]').val();
      var likelihoodDetail = this.newLikelihoodName;
      var statusApproval = this.$('[name="StatusId"]').val();
      var obj = [];
      $.grep(this.scenario.ScenarioDetail, function (item) {
        if (!item.IsDelete) {
          obj.push(item.Project);
        }
      });

      var html = '<div>'
      html += '<div style="font-size: 9pt;color:#373637;">'
      if (statusApproval == "2") {
        html += '<p style="margin-bottom: 0px;">Dengan ini saya <b>meyetujui</b>  permohonan atas skenario dengan data berikut: </p>'
      } else {
        html += '<p style="margin-bottom: 0px;">Dengan ini saya <b>tidak meyetujui</b>  permohonan atas skenario dengan data berikut: </p>'
      }
      html += '<p style="margin-top: 0px;font-style: italic;">Nama Skenario: ' + scenarioName + '</p>'
      html += '</div>'
      html += '<div style="font-size: 9pt;color:#373637;">'
      html += '<p style="margin-bottom: 0;">Parameter: ' + parameterName + '</p>'
      html += '<table style="width:100%;border: 1px solid #c9c4c4;text-align: center;border-collapse: collapse;font-family: Arial,sans-serif;font-size: 9pt;color:#373637;">'
      html += '<tr style="border: 1px solid #c9c4c4;background-color: #eeebeb;">'
      html += '<td style="border: 1px solid #c9c4c4;padding: 5px;">Definisi Parameter</td>'
      html += '<td style="border: 1px solid #c9c4c4;padding: 5px;">Nilai Terendah</td>'
      html += '<td style="border: 1px solid #c9c4c4;padding: 5px;">Nilai Tertinggi</td>'
      html += '<td style="border: 1px solid #c9c4c4;padding: 5px;">Rata-Rata</td>'
      html += '</tr>'
      if (this.likelihoodDetail) {
        _.each(this.likelihoodDetail, function (item, i) {
          html += '<tr style="border: 1px solid #c9c4c4;">'
          html += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">' + item.DefinisiLikehood + '</td>'
          html += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">' + item.Lower + '</td>'
          html += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">' + item.Incres + '</td>'
          html += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">' + item.Average + '</td>'
          html += '</tr>'
        })
      }
      html += '</table>'
      html += '</div>'
      html += '<div style="margin-bottom: 20px;font-size: 9pt;color:#373637;"></div>'
      html += '<p style="margin-bottom: 0;font-size: 9pt;color:#373637;">Proyek</p>'
      if (this.isAlreadyEdited) {
        html += this.projectHtml;
      } else {
        html += '<table style="width:100%;border-collapse: collapse;font-family: Arial,sans-serif;font-size: 9pt;color:#373637;">'
        html += '<tr style="border: 1px solid #c9c4c4;background-color: #eeebeb;">'
        html += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">Nama Proyek</td>'
        html += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">Sektor</td>'
        html += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">Kategori Risiko</td>'
        html += '</tr>'

        $.each(obj, function (i, item) {
          var riskSelected = '';
          html += '<tr style="border: 1px solid #dfbebe;" id="' + item.Id + '">'
          html += '<td style="border: 1px solid #dfbebe;padding: 5px;"> ' + item.NamaProject + ' </td>'
          html += '<td style="border: 1px solid #dfbebe;padding: 5px;"> ' + item.NamaSektor + ' </td>'
          if (item.ProjectRiskRegistrasi) {
            _.each(item.ProjectRiskRegistrasi, function (modelRisk) {
              riskSelected += modelRisk.RiskRegistrasi.KodeMRisk;
              riskSelected += ', ';
            });
          }
          html += '<td style="border: 1px solid #dfbebe;padding: 5px;"> ' + riskSelected + ' </td>'
          html += '</tr>'
        });
        html += '</table>'
      }
      html += '<div style="margin-top: 25px">'
      html += '<p style="font-style: italic;">Keterangan: <b>' + keterangan + '</b>.</p>'
      html += '<p>Terimakasih.</p>'
      html += '</div>'

      return html;
    },
    renderProjectList: function () {
      var self = this;
      var obj = [];
      $.grep(this.scenario.ScenarioDetail, function (item) {
        if (!item.IsDelete) {
          obj.push(item.Project);
          self.projectSelected.push(item.Project);
        }
      });

      this.projectIds = [];
      var html = '<table>'
      html += '<tr class="header-kiw-table">'
      html += '<td class="td-kiw-table">Nama Proyek</td>'
      html += '<td class="td-kiw-table">Sektor</td>'
      html += '<td class="td-kiw-table">Kategori Risiko</td>'
      html += '</tr>'

      $.each(obj, function (i, item) {
        var riskSelected = '';
        self.projectIds.push(item.Id);
        html += '<tr class="row-kiw-table" id="' + item.Id + '">'
        html += '<td class="td-kiw-table"> ' + item.NamaProject + ' </td>'
        html += '<td class="td-kiw-table"> ' + item.NamaSektor + ' </td>'
        if (item.ProjectRiskRegistrasi) {
          _.each(item.ProjectRiskRegistrasi, function (modelRisk) {
            riskSelected += modelRisk.RiskRegistrasi.KodeMRisk;
            riskSelected += ', ';
          });
        }
        html += '<td class="td-kiw-table"> ' + riskSelected + ' </td>'
        html += '<td>'
        html += '</tr>'
      });
      html += '</table>'

      this.$('[project-table]').append(html);
    },
    renderLikelihoodDetailList: function () {
      var self = this;
      var likelihood = 0;
      this.$('[likelihood-table]').append(this.tableLikelihood.el);
      this.tableLikelihood.render();
      if (this.likelihoodId != 0) {
        likelihood = this.likelihoodId;
      } else {
        likelihood = this.scenario.LikehoodId;
        this.likelihoodId = likelihood;
      }
      this.tableLikelihood.collection.fetch({
        reset: true,
        data: {
          ParentId: likelihood,
          PageSize: 100
        },
        success: function (req, res) {
          self.newLikelihoodName = res.results[0].NamaLikehood;
          self.$('[name="NamaLikehood"]').val(self.newLikelihoodName);
        }
      });
    },
    submitNotification: function () {
      var isSend = this.$('[name="IsSend"]').prop('checked');
      if (isSend) {
        commonFunction.responseWarningCannotExecute("Skenario otomatis akan dilanjutkan ke proses persetujuan / approval berikutnya (jika ada) jika menekan tombol Simpan. Sehingga Skenario ini tidak bisa diubah lagi.");
      } else {
        commonFunction.responseWarningCannotExecute("Skenario membutuhkan pengajuan persetujuan berikutnya terpisah setelah menekan tombol Simpan.");
      }
    },
    getConfirmation: function () {
      var templateNotif = this.generateTemplateNotification();
      var data = this.options.model.attributes.NamaScenario;
      var action = "";
      var statusApproval = this.$('[name="StatusId"]').val();
      if (statusApproval == 2) {
        action = "menyetujui";
      } else {
        action = "tidak menyetujui";
      }
      var retVal;
      if (this.isSended == true) {
        retVal = confirm("Apakah anda yakin untuk " + action + " Skenario ini?" + "\n\n\Skenario otomatis akan dilanjutkan ke proses persetujuan / approval. Sehingga Skenario ini tidak bisa diubah lagi.");
      }
      if (this.isSended == false) {
        retVal = confirm("Apakah anda yakin untuk " + action + " Skenario ini?" + "\n\n\Skenario membutuhkan pengajuan persetujuan terpisah.");
      }
      if (retVal == true) {
        this.doSave(templateNotif);
      }
      else {
        this.$('[type="submit"]').attr('disabled', false);
      }
    },
    sendApprove: function () {
      this.isSended = true;
      this.getConfirmation();
    },
    sendDraft: function () {
      this.isSended = false;
      this.getConfirmation();
    },
    doSave: function (template) {
      var data = commonFunction.formDataToJson(this.$('form').serializeArray());
      if (this.modelScenario.get('isAlreadyEdited')) {
        data.ProjectId = this.projectIdEdited;
      } else {
        data.ProjectId = this.projectIds;
      }
      data.LikehoodId = this.likelihoodId;
      data.RequestId = this.scenarioId;
      data.Keterangan = this.$('[name="Keterangan"]').val();
      data.StatusId = this.$('[name="StatusId"]').val();
      data.IsSend = this.isSended;
      data.TemplateNotif = template;
      this.model.save(data);
      commonFunction.showLoadingSpinner();
      this.$('[btnDraft]').addClass('disabled');
      this.$('[btnApprove]').addClass('disabled');
      eventAggregator.trigger('approval/scenario/:refecth');
    }
  });
});
