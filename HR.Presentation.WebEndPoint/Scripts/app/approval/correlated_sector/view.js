define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var Model = require('./../model');
  var commonFunction = require('commonfunction');
  var commonConfig = require('commonconfig');
  var eventAggregator = require('eventaggregator');
  var CollectionRiskMatrix = require('./../../risk_matrix/collection');

  module.exports = View.extend({
    template: _.template(template),
    initialize: function (options) {
      var self = this;
      this.IsSended= null;
      this.currentCol = 0;
      this.currentRow = 0;
      this.modelId = options.model.attributes.Id;
      this.correlationMatrix = options.model.attributes.CorrelationMatrix;
      this.riskRegistrasi = options.model.attributes.RiskRegistrasi;
      this.correlatedSectorId = options.model.attributes.correlatedSektor.Id;
      this.correlatedSectorHeader = options.model.attributes.correlatedSektor;
      this.correlatedSectorDetail = options.model.attributes.correlatedSektorDetailCollection;
      this.namaScenario = options.model.attributes.NamaScenario;
      this.keterangan = options.model.get('Keterangan');
      this.model = new Model();
      this.listenTo(this.model, 'sync error', function () { 
        debugger;
        eventAggregator.trigger('approval/correlated_sector/approval:fecth');
      });
      this.listenTo(this.model, 'sync', function () {
        commonFunction.responseSuccessUpdateAddDelete('Approval Korelasi Kategori Risiko berhasil diproses.');
        self.$el.modal('hide');
        eventAggregator.trigger('approval/correlated_sector/approval:fecth');
      });
    },
    afterRender: function () {
      var self = this;
      this.renderTableCorrelatedSector();
      commonFunction.closeLoadingSpinner();
      this.$('[name="Keterangan"]').val(this.keterangan);
    },
    events: {
      'change [cormat]': 'setCorrelationMatrix',
      'click [btnApprove]': 'sendApprove',
      'click [btnDraft]': 'sendDraft'
    },
    setCorrelationMatrix: function (e) {
      var self = this;
      var attributeValue = e.target.getAttribute('data-cormat');
      var cormatValue = this.$('[data-cormat="' + attributeValue + '"]').val();

      var row = attributeValue.substring(0, attributeValue.indexOf('-'));
      var col = attributeValue.substring(attributeValue.indexOf('-') + 1);

      this.$('[data-cormat="' + col + '-' + row + '"]').val(cormatValue);
      if (this.currentCol == 0 && this.currentRow == 0) {
        this.$('[data-cormat="' + col + '-' + row + '"]').addClass('blink');
      } else {
        this.$('[data-cormat="' + this.currentCol + '-' + this.currentRow + '"]').removeClass('blink');
        this.$('[data-cormat="' + col + '-' + row + '"]').addClass('blink');
      }
      this.currentCol = col;
      this.currentRow = row;
    },
    renderTableCorrelatedSector: function () {
      var self = this;
      var html = '<div class="col-md-12 scrollable-matrix">'
      html += '<div class="table-mat">'
      html += '<div class="heading-mat">'
      html += '<div class="cell-mat"><p>Kode</p></div>'
      html += '<div class="cell-mat"><p>Nama</p></div>'
      _.each(this.riskRegistrasi, function (item) {
        html += '<div class="cell-mat" data-heading-risk="' + item.KodeMRisk + '">'
        html += '<p>' + item.KodeMRisk + '</p>'
        html += '</div>'
      });
      html += '</div>'

      _.each(this.riskRegistrasi, function (item) {
        html += '<div class="row-mat" data-risk-row="' + item.KodeMRisk + '">'

        html += '<div class="cell-mat text-center"> <p>' + item.KodeMRisk + '</p></div>'
        html += '<div class="cell-mat"> <p>' + item.NamaCategoryRisk + '</p></div>'
        _.each(self.riskRegistrasi, function (model) {
          html += '<div class="cell-mat text-center" data-cell-risk="' + item.Id + '-' + model.Id + '" data="' + item.KodeMRisk + '-' + model.KodeMRisk + '">'
          if (item.KodeMRisk == model.KodeMRisk) {
            status = true;
            html += '<select style="background-color: lightgrey;" cormat data-correlation-matrix="' + item.KodeMRisk + '-' + model.KodeMRisk + '" data-cormat="' + item.Id + '-' + model.Id + '" disabled>'
            html += '<option value="1">1</option>'
            html += '</select>'
          } else {
            html += '<select cormat data-correlation-matrix="' + item.KodeMRisk + '-' + model.KodeMRisk + '" data-cormat="' + item.Id + '-' + model.Id + '">'
            _.each(self.correlationMatrix, function (corMat) {
              html += '<option value="' + corMat.Id + '">' + corMat.Nilai + '</option>'
            });
            html += '</select>'
          }
          html += '</div>'
        });

        html += '</div>'
      })
      html += '</div>'
      html += '</div>'
      this.$('[kw-table-correlated-sector]').append(html);

      this.setCorrelatioinMatrixValue();
      this.setTemplateSektorInformation();
    },
    setCorrelatioinMatrixValue: function () {
      var self = this;
      if (this.correlatedSectorDetail.CorrelatedSektorDetailCollection) {
        for (let i = 0; i < this.correlatedSectorDetail.CorrelatedSektorDetailCollection.length; i++) {
          var dataDetail = this.correlatedSectorDetail.CorrelatedSektorDetailCollection[i];
          var riskRegistrasiId = dataDetail.RiskRegistrasiId;
          var riskRegistrasiValues = dataDetail.RiskRegistrasiValues;
          if (riskRegistrasiValues.length > 0) {
            for (let c = 0; c < riskRegistrasiValues.length; c++) {
              var cormatValue = riskRegistrasiValues[c].CorrelationMatrixId;
              var row = riskRegistrasiValues[c].RiskRegistrasiIdRow;
              var col = riskRegistrasiValues[c].RiskRegistrasiIdCol;

              this.$('[data-cormat="' + row + '-' + col + '"]').val(cormatValue);
            }
          }
        }
      }
    },
    setTemplateSektorInformation: function () {
      var self = this;
      this.$('[nama-scenario]').text('Skenario :  ' + this.namaScenario);
      this.$('[nama-sektor]').text('Sektor :  ' + this.correlatedSectorHeader.NamaSektor);
      var html = '<div class="col-md-6" style="margin-bottom: 15px;">'
      html += '<table class="table-risk-matrix">'
      html += '<tr class="td-risk-matrix" style="background-color: #f0f0f0;">'
      _.each(this.correlationMatrix, function (item) {
        html += '<td class="td-risk-matrix" style="padding: 5px">' + item.NamaCorrelationMatrix + '</td>'
      });
      html += '</tr>'
      html += '<tr class="td-risk-matrix">'
      _.each(this.correlationMatrix, function (item) {
        html += '<td class="td-risk-matrix text-center">' + item.Nilai + '</td>'
      });
      html += '</tr>'
      html += '</table>'
      html += '</div>'

      this.$('[kw-table-correlation-matrix]').append(html);
    },
    generateTemplateNotification: function () {
      const url = commonFunction.getCurrentDomainAndPort();
      var keterangan = this.$('[name="Keterangan"]').val();
      var scenarioName = this.$('[nama-scenario]').text();
      var sectorName = this.$('[nama-sektor]').text();
      var statusApproval = this.$('[name="StatusId"]').val();
      var html = '<div>'
      html += '<div style="font-size: 9pt;color:#373637;">'
      if (statusApproval == 2) {
        html += '<p style="margin-bottom:0px;">Dengan ini saya <b>meyetujui</b> permohonan atas Korelasi Kategori Risiko  dengan data berikut: </p>'
      } else {
        html += '<p style="margin-bottom:0px;">Dengan ini saya <b>tidak meyetujui</b>  permohonan atas Korelasi Kategori Risiko  dengan data berikut: </p>'
      }
      html += '<p style="font-style: italic;margin-top:0px;margin-bottom:0px;">' + scenarioName + '</p>'
      html += '<p style="font-style: italic;margin-top:0px;margin-bottom:0px;">' + sectorName + '</p>'
      html += '<p style="font-style: italic;margin-top:0px;margin-bottom:0px;">Keterangan: ' + keterangan + '</p>'
      if (statusApproval == 2) {
        html += '<p>Mohon untuk dilakukan Approval, data detail dapat dilihat  melalui aplikasi dengan menekan <span><a href="' + url + '/#approval">link ini</a></span> dan masuk  berdasarkan login masing-masing.</p>'
      } else {
        html += '<p>Untuk menganti sesuai keterangan, silakan masuk melalui aplikasi dengan menekan <span><a href="' + url + '/#approval">link ini</a></span> berdasarkan login masing-masing.</p>'
      }
      html += '<p>Terimakasih.</p>'
      html += '</div>'
      html += '</div>'

      return html;
    },
    getConfirmation: function () {
      var templateNotif = this.generateTemplateNotification();
      var statusApproval = this.$('[name="StatusId"]').val();
      var action = "";
      if (statusApproval == 2) {
        action = "menyetujui";
      } else {
        action = "tidak menyetujui";
      }
      var retVal;
      if  (this.isSended == true){
          retVal = confirm("Apakah anda yakin untuk " + action + " Korelasi Kategori Risiko ini?" + "\n\n\Korelasi Kategori Risiko otomatis akan dilanjutkan ke proses persetujuan / approval. Sehingga Skenario ini tidak bisa diubah lagi.");
      }
      if (this.isSended == false){
          retVal = confirm("Apakah anda yakin untuk " + action + " Korelasi Kategori Risiko ini?" + "\n\n\Korelasi Kategori Risiko membutuhkan pengajuan persetujuan terpisah.");
      }
      if (retVal == true) {
        this.doSave(templateNotif);
      }
      else {
        this.$('[type="submit"]').attr('disabled', false);
      }
    },
    sendApprove: function () {
        this.isSended = true;
        this.getConfirmation();
    },
  
    sendDraft: function () {
        this.isSended = false;
        this.getConfirmation();
    },
    doSave: function (template) {
      var param = {};
      var paramCollection = [];

      for (var i = 0; i < this.riskRegistrasi.length; i++) {
        var correlatedSektorDetailCollection = [];
        var risRegistrasiValues = [];
        var paramDetail = {};

        for (var r = 0; r < this.riskRegistrasi.length; r++) {
          var paramDetailValue = {};
          var row = this.riskRegistrasi[i].Id;
          var col = this.riskRegistrasi[r].Id;
          var cormatValue = this.$('[data-cormat="' + row + '-' + col + '"]').val();

          paramDetailValue.RiskRegistrasiIdRow = row;
          paramDetailValue.RiskRegistrasiIdCol = col;
          paramDetailValue.CorrelationMatrixId = parseInt(cormatValue);

          risRegistrasiValues.push(paramDetailValue);
        }

        correlatedSektorDetailCollection.push(risRegistrasiValues);
        paramDetail.RiskRegistrasiId = this.riskRegistrasi[i].Id;
        paramDetail.RiskRegistrasiValues = risRegistrasiValues;

        param.CorrelatedSektorId = this.correlatedSectorId;
        paramCollection.push(paramDetail);
        param.CorrelatedSektorDetailCollection = paramCollection;
        param.Keterangan = this.$('[name="Keterangan"]').val();
        param.StatusId = this.$('[name="StatusId"]').val();
        param.Id = this.modelId;
      }
      param.TemplateNotif = template;
      param.IsSend = this.isSended;
      this.model.save(param);
      commonFunction.showLoadingSpinner();
      this.$('[btnDraft]').addClass('disabled');
      this.$('[btnApprove]').addClass('disabled');
      eventAggregator.trigger('approval/correlated_sector/:refecth');
    }
  });
});
