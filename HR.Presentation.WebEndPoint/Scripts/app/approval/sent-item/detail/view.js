define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var Model = require('./../model');
  var commonFunction = require('commonfunction');
  var commonConfig = require('commonconfig');
  var moment = require('moment');

  module.exports = View.extend({
    template: _.template(template),
    initialize: function (options) {
      var self = this;
      this.sourceApproval = options.model.get('SourceApproval');
      this.requestId = options.model.get('RequestId');

      this.model = new Model();
    },
    afterRender: function () {
      var self = this;
      this.fetchData();

    },
    fetchData: function (id) {
      const self = this;
      this.model.fetch({
        reset: true,
        data: {
          id: self.requestId
        },
        success: function (req, res) {
          console.log(res);
          self.setTemplate(res);
        }
      })
    },
    setTemplate: function (data) {
      let html = "";
      html += '<table style="margin: 0 auto;">';
      html += '<th class="td-kiw-table">Nama Skenario</th>'
      html += '<th class="td-kiw-table">Entitas</th>'
      html += '<th class="td-kiw-table">Nama Entitas</th>'
      html += '<th class="td-kiw-table">Status</th>'
      html += '<th class="td-kiw-table">Request By</th>'
      html += '<th class="td-kiw-table">Request Date</th>'
      html += '<th class="td-kiw-table">Approve/Update/Reject By</th>'
      html += '<th class="td-kiw-table">Approve/Update/Reject Date</th>'
      html += '<th class="td-kiw-table">Keterangan</th>'
      html += '</tr>'
      switch (this.sourceApproval) {
        case "Scenario":
          if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
              html += '<tr class="row-kiw-table">'
              html += '<td class="td-kiw-table">' + data[i].NamaScenario + '</td>'
              html += '<td class="td-kiw-table">' + data[i].SourceApproval + '</td>'
              html += '<td class="td-kiw-table">' + data[i].Scenario.NamaScenario + '</td>'
              html += '<td class="td-kiw-table">' + data[i].Status.StatusDescription + '</td>'
              html += '<td class="td-kiw-table">' + data[i].User.UserName + '</td>'
              html += '<td class="td-kiw-table">' + moment(data[i].CreateDate).format('DD MMM YYYY, HH:mm') + '</td>'
              html += '<td class="td-kiw-table">' + data[i].UserApprove.UserName + '</td>'
              if (data[i].UpdateDate) {
                html += '<td class="td-kiw-table">' + moment(data[i].UpdateDate).format('DD MMM YYYY, HH:mm') + '</td>'
              } else {
                html += '<td class="td-kiw-table"></td>'
              }
              if (data[i].Keterangan) {
                html += '<td class="td-kiw-table">' + data[i].Keterangan + '</td>'
              } else {
                html += '<td class="td-kiw-table"></td>'
              }
              html += '</tr>'
            }
          }
          html += '</table>'
          this.$('[data-history]').append(html);
          break;
        case "RiskMatrixProject":
          if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
              html += '<tr class="row-kiw-table">'
              html += '<td class="td-kiw-table">' + data[i].NamaScenario + '</td>'
              html += '<td class="td-kiw-table">' + data[i].SourceApproval + '</td>'
              html += '<td class="td-kiw-table">' + data[i].NamaEntitas + '</td>'
              html += '<td class="td-kiw-table">' + data[i].Status.StatusDescription + '</td>'
              html += '<td class="td-kiw-table">' + data[i].User.UserName + '</td>'
              html += '<td class="td-kiw-table">' + moment(data[i].CreateDate).format('DD MMM YYYY, HH:mm') + '</td>'
              html += '<td class="td-kiw-table">' + data[i].UserApprove.UserName + '</td>'
              if (data[i].UpdateDate) {
                html += '<td class="td-kiw-table">' + moment(data[i].UpdateDate).format('DD MMM YYYY, HH:mm') + '</td>'
              } else {
                html += '<td class="td-kiw-table"></td>'
              }
              if (data[i].Keterangan) {
                html += '<td class="td-kiw-table">' + data[i].Keterangan + '</td>'
              } else {
                html += '<td class="td-kiw-table"></td>'
              }
              html += '</tr>'
            }
          }
          html += '</table>'
          this.$('[data-history]').append(html);
          break;
        case "CorrelatedSektor":
          if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
              html += '<tr class="row-kiw-table">'
              html += '<td class="td-kiw-table">' + data[i].NamaScenario + '</td>'
              html += '<td class="td-kiw-table">' + data[i].SourceApproval + '</td>'
              html += '<td class="td-kiw-table">' + data[i].NamaEntitas + '</td>'
              html += '<td class="td-kiw-table">' + data[i].Status.StatusDescription + '</td>'
              html += '<td class="td-kiw-table">' + data[i].User.UserName + '</td>'
              html += '<td class="td-kiw-table">' + moment(data[i].CreateDate).format('DD MMM YYYY, HH:mm') + '</td>'
              html += '<td class="td-kiw-table">' + data[i].UserApprove.UserName + '</td>'
              if (data[i].UpdateDate) {
                html += '<td class="td-kiw-table">' + moment(data[i].UpdateDate).format('DD MMM YYYY, HH:mm') + '</td>'
              } else {
                html += '<td class="td-kiw-table"></td>'
              }
              if (data[i].Keterangan) {
                html += '<td class="td-kiw-table">' + data[i].Keterangan + '</td>'
              } else {
                html += '<td class="td-kiw-table"></td>'
              }
              html += '</tr>'
            }
          }
          html += '</table>'
          this.$('[data-history]').append(html);
          break;
        case "CorrelatedProject":
          if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
              html += '<tr class="row-kiw-table">'
              html += '<td class="td-kiw-table">' + data[i].NamaScenario + '</td>'
              html += '<td class="td-kiw-table">' + data[i].SourceApproval + '</td>'
              html += '<td class="td-kiw-table">' + data[i].NamaEntitas + '</td>'
              html += '<td class="td-kiw-table">' + data[i].Status.StatusDescription + '</td>'
              html += '<td class="td-kiw-table">' + data[i].User.UserName + '</td>'
              html += '<td class="td-kiw-table">' + moment(data[i].CreateDate).format('DD MMM YYYY, HH:mm') + '</td>'
              html += '<td class="td-kiw-table">' + data[i].UserApprove.UserName + '</td>'
              if (data[i].UpdateDate) {
                html += '<td class="td-kiw-table">' + moment(data[i].UpdateDate).format('DD MMM YYYY, HH:mm') + '</td>'
              } else {
                html += '<td class="td-kiw-table"></td>'
              }
              if (data[i].Keterangan) {
                html += '<td class="td-kiw-table">' + data[i].Keterangan + '</td>'
              } else {
                html += '<td class="td-kiw-table"></td>'
              }
              html += '</tr>'
            }
          }
          html += '</table>'
          this.$('[data-history]').append(html);
          break;
      }
    }
  });
});