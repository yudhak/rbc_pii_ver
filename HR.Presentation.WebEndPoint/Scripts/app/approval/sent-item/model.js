﻿define(function(require, exports, module) {
    'use strict';

    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'Id',
        urlRoot: 'api/ApprovalSent',
        defaults: function() {
            return {
              CorrelatedProject : {
                ProjectName: '',
                ProjectId: '',
                SektorId: '',
                ScenarioId: '',
                StatusId: '',
                CreateBy : '',
                CreateDate : '',
                IsDelete : '',
                UpdateBy : '',
                UpdateDate : ''
              },
              CorrelatedSektor : {
                NamaSektor: '',
                SektorId: '',
                ScenarioId: '',
                StatusId: '',
                CreateBy : '',
                CreateDate : '',
                IsDelete : '',
                UpdateBy : '',
                UpdateDate : ''
              },
              CreateBy : '',
              CreateDate : '',
              DeleteDate : '',
              Id : '',
              IsActive : '',
              IsDelete : '',
              Keterangan : '',
              NomorUrutStatus : '',
              RequestId : '',
              RiskMatrixProject : {
                ProjectId: '',
                RiskMatrixId: '',
                ScenarioId: '',
                StatusId: '',
                CreateBy : '',
                CreateDate : '',
                IsDelete : '',
                UpdateBy : '',
                UpdateDate : ''
              },
              Scenario : '',
              SourceApproval : '',
              Status : '',
              StatusApproval : '',
              StatusId : '',
              UpdateBy : '',
              UpdateDate: '',
              NamaPembuat: '',
              Status: {
                  StatusDescription: ''
              },
              User: {
                  UserName: ''
              },
              NamaEntitas: ''
            }
        }
    });
});
