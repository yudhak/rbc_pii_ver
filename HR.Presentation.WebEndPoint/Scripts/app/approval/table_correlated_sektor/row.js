define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./row.html');
    var commonFunction = require('commonfunction');

    module.exports = LayoutManager.extend({
        tagName: 'tr',
        template: _.template(template),
        events: {
            'click [name="detail"]': 'Detail',
        },
        Detail: function() {
          var self = this;
          if (e.target.lang == "CorrelatedSektor") {
            var correlatedSektorId = e.target.value;
            require(['./../correlated_sector/view'], function(View, correlatedSektorId) {
                commonFunction.setDefaultModalDialogFunction(self, View, self.model, correlatedSektorId);
            });
          }
        },

    });
});
