define(function (require, exports, module) {
  'use strict';
  var LayoutManager = require('layoutmanager');
  var template = require('text!./template.html');
  var commonFunction = require('commonfunction');
  var eventAggregator = require('eventaggregator');
  var Table = require('./table/table');
  var Collection = require('./collection');
  var Paging = require('paging');
  var Model = require('./model');
  var ModelInterProject = require('./modelInterProject');
  var ModelComment = require('./modelComment');
  var ModelOverAllComment = require('./../../overall_comment/detail/model');
  const numeral = require('numeral');
  const ModelReport = require('./../../calculation/modelReport');
  const commonConfig = require('commonconfig');
  require('highcharts');
  require('jquerymask');

  module.exports = LayoutManager.extend({
    className: 'container-fluid main-content tbl bg-white',
    template: _.template(template),
    initialize: function () {
      var self = this;
      this.dataInterProject = null;
      this.comment = null;
      this.isDataAvailable = 0;
      this.model = new Model();
      this.modelInterProject = new ModelInterProject();
      this.modelComment = new ModelComment();
      this.modelOverAllComment = new ModelOverAllComment();
      this.modelReport = new ModelReport();
      this.table = new Table({
        collection: new Collection()
      });
      this.paging = new Paging({
        collection: this.table.collection
      });
      this.listenToOnce(this.modelInterProject, 'sync', function (model) {
        this.render();
      });
      this.listenTo(eventAggregator, 'master/functional_risk/add:fecth', function () {
        self.fetchData();
      });
      this.listenTo(eventAggregator, 'master/functional_risk/edit:fecth', function () {
        self.fetchData();
      });
      this.listenTo(eventAggregator, 'master/functional_risk/delete:fecth', function () {
        self.fetchData();
      });
      this.listenTo(eventAggregator, 'master/functional_risk/default:fecth', function () {
        self.fetchData();
      });
      this.roles = JSON.parse(localStorage.getItem('roles'));
    },
    events: {
      'click [name="ExportXLS"]': "downloadXLSFile"
    },
    afterRender: function () {
      this.fetchDataInterProject();
      this.fetchComment();
      this.setTemplate();
      this.setRoleAccess();
    },
    fetchDataInterProject: function () {
      commonFunction.showLoadingSpinner();
      var self = this;
      this.modelInterProject.fetch({
        reset: true,
        success: function (req, res) {
          self.dataInterProject = res;
        }
      });
    },
    fetchData: function () {
      this.table.collection.fetch({
        reset: true
      })
    },
    setTemplate: function () {
      var self = this;
      this.isDataAvailable += 1;
      if (this.isDataAvailable > 1) {
        var html = '<div class="col-md-12">'
        _.each(this.dataInterProject.Project, function (item) {
          html += '<div class="col-md-4">'
          html += '<div class="panel panel-default">'
          html += '<div class="panel-body" id="project-' + item.ProjectId + '"></div>'
          html += '</div>'
          html += '</div>'
        });
        html += '</div>'

        this.$('[chart-risk-capital-by-project]').append(html);

        this.renderChart(this.dataInterProject);
        this.renderFunctionRisk();
        this.renderAssetAllocationChart(this.dataInterProject.AssetClassDefault);
        this.renderComment();
        this.$('[name="ExportXLS"]').removeClass('hide');

        commonFunction.closeLoadingSpinner();
      }
    },
    renderChart: function (data) {
      var xAxisCategories = data.Year;

      _.each(data.Project, function (item) {
        var dom = "project-" + item.ProjectId;
        var dataTitle = item.NamaProject;
        var seriesCategories = [];
        _.each(item.Values, function (model) {
          seriesCategories.push(model.Value);
        });

        Highcharts.chart(dom, {
          title: {
            text: dataTitle
          },
          xAxis: {
            categories: xAxisCategories
          },
          tooltip: {
            headerFormat: 'Tahun: <b>{point.x}</b><br/>',
            valueDecimals: 2,
            pointFormat: '<b>{point.y}</b>'
          },
          plotOptions: {
            series: {
              allowPointSelect: true
            }
          },
          series: [{
            data: seriesCategories,
            name: 'Tahun'
          }]
        });
      });
    },
    renderFunctionRisk: function () {
      var self = this;
      //defines dynamic column
      var columnHeader = [];
      var columnHeaderResult = [];
      var functionalRisk = this.dataInterProject.FunctionalRiskMainDashboard;
      if (functionalRisk.length > 0) {
        _.each(functionalRisk[0].Colors, function (item) {
          columnHeader.push(item);
        });
      }
      var result = this.dataInterProject.FunctionalRiskResultTableResults;
      if (result.length > 0) {
        _.each(result[0].Scenarios, function (item) {
          columnHeaderResult.push(item.Scenario);
        });
      }
      var html = '<div class="row">'
      html += '<div class="col-md-12">'
      // html += '<div class="panel panel-default">'

      html += '<div class="margintop-5 text-bold-italic">Functional Risk Limits</div>'
      html += '<div class="col-md-12 scrollable-matrix" style="margin-bottom: 15px;" obo-table-matrix>'
      html += '<table style="width:100%" class="table-risk-matrix">'
      //header
      html += '<tr class="td-kiw-table header-kiw-table">'
      html += '<td class="td-kiw-table" rowspan="2">Metrices</td>'
      html += '<td class="td-kiw-table" rowspan="2">Formula</td>'
      if (columnHeader) {
        _.each(columnHeader, function (item) {
          var colorName = item.Color.replace(/\s/g, '').toLowerCase();
          html += '<td class="td-kiw-table header-kiw-table" lang="' + colorName + '" colspan="3">' + item.Color + '</td>'
        });
      }
      html += '</tr>'
      html += '<tr class="td-kiw-table">'
      if (columnHeader) {
        _.each(columnHeader, function (item) {
          if (item.Scenarios) {
            _.each(item.Scenarios, function (model) {
              html += '<td class="td-kiw-table header-kiw-table" lang="' + item.Color + '">' + model.Scenario + '</td>'
            });
          }
        });
      }
      html += '</tr>'
      //end of header
      //render row
      if (functionalRisk) {
        _.each(functionalRisk, function (item) {
          html += '<tr>'
          html += '<td class="td-kiw-table">' + item.NamaMatrix + '</td>'
          html += '<td class="td-kiw-table">' + item.NamaFormula + '</td>'
          if (item.Colors) {
            _.each(item.Colors, function (model) {
              if (model.Scenarios) {
                _.each(model.Scenarios, function (val) {
                  if (val.Definisi) {
                    html += '<td class="td-kiw-table">' + val.Definisi + '</td>'
                  } else {
                    html += '<td class="td-kiw-table">-</td>'
                  }
                });
              }
            });
          }
          html += '</tr>'
        });
      }
      //end of render row

      html += '</table>'
      html += '</div>'

      // html += '</div>'
      html += '</div>'
      html += '</div>'

      //result table
      html += '<div class="row">'
      html += '<div class="col-md-12">'

      html += '<div class="margintop-5 text-bold-italic">Results</div>'
      html += '<div class="col-md-12 scrollable-matrix" style="margin-bottom: 15px;" obo-table-matrix>'
      html += '<table style="width:100%" class="table-risk-matrix">'
      //header
      html += '<tr class="td-kiw-table header-kiw-table">'
      html += '<td class="td-kiw-table" rowspan="2">Metrices</td>'
      html += '<td class="td-kiw-table" rowspan="2">Formula</td>'
      if (columnHeaderResult) {
        _.each(columnHeaderResult, function (item) {
          html += '<td class="td-kiw-table header-kiw-table" colspan="2">' + item + '</td>'
        });
      }
      html += '</tr>'
      html += '<tr class="td-kiw-table">'
      if (columnHeaderResult) {
        _.each(columnHeaderResult, function (item) {
          html += '<td class="td-kiw-table header-kiw-table">Calculations</td>'
          html += '<td class="td-kiw-table header-kiw-table">Comments</td>'
        });
      }
      html += '</tr>'
      //end of header
      //render row
      if (result) {
        _.each(result, function (item) {
          html += '<tr>'
          html += '<td class="td-kiw-table">' + item.NamaMatrix + '</td>'
          html += '<td class="td-kiw-table">' + item.NamaFormula + '</td>'
          if (item.Scenarios) {
            _.each(item.Scenarios, function (model) {
              if (model.Calculations) {
                switch (model.CollorComment) {
                  case "Green":
                    html += '<td class="td-kiw-table text-right">'
                    html += '<span class="fa fa-check-circle" style="color: green; margin-right:5px;"></span>' + numeral(model.Calculations).format('0,0.00')
                    html += '</td>'
                    break;
                  case "Yellow":
                    html += '<td class="td-kiw-table text-right">'
                    html += '<span class="fa fa-exclamation-circle" style="color: yellow; margin-right:5px;"></span>' + numeral(model.Calculations).format('0,0.00')
                    html += '</td>'
                    break;
                  case "Red":
                    html += '<td class="td-kiw-table text-right">'
                    html += '<span class="fa fa-times-circle-o" style="color: red; margin-right:5px;"></span>' + numeral(model.Calculations).format('0,0.00')
                    html += '</td>'
                    break;
                }
              } else {
                html += '<td class="td-kiw-table">-</td>'
              }
              if (model.Comment) {
                html += '<td class="td-kiw-table">' + model.Comment + '</td>'
              } else {
                html += '<td class="td-kiw-table">-</td>'
              }
            });
          }
          html += '</tr>'
        });
      }
      //end of render row

      html += '</table>'
      html += '</div>'

      html += '</div>'
      html += '</div>'
      //end of result table

      this.$('[functional-risk]').append(html);
      this.setColor(columnHeader);
    },
    setColor: function (data) {
      var self = this;
      if (data) {
        _.each(data, function (item) {
          var colorName = item.Color.replace(/\s/g, '').toLowerCase();
          self.$('[lang="' + colorName + '"]').css('background-color', colorName);
          self.$('[lang="' + colorName + '"]').css('color', 'black');
        });
      }
    },
    renderAssetAllocationChart: function (obj) {
      var self = this;
      var html = '<div class="row">'
      html = '<div class="col-md-12">'
      html += '<div class="col-md-4">'
      html += '<div class="panel panel-default">'
      html += '<div class="panel-body" id="assetallocationchart"></div>'
      html += '</div>'
      html += '</div>'
      html += '</div>'
      html += '</div>'
      this.$('[chart-asset-allocation]').append(html);

      var data = [];
      if (obj.length > 0) {
        _.each(obj, function (item) {
          var dataDetail = {};
          dataDetail.name = item.AssetClass;
          dataDetail.y = item.Proportion;
          data.push(dataDetail);
        });
      }

      Highcharts.chart('assetallocationchart', {
        chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie'
        },
        title: {
          text: 'Asset Allocation (Strategic)'
        },
        tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
          pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
              enabled: false
            },
            showInLegend: true
          }
        },
        series: [{
          name: 'Brands',
          colorByPoint: true,
          data: data
        }]
      });
    },
    fetchComment: function () {
      var self = this;
      this.modelComment.fetch({
        reset: true,
        data: {
          idDef: 1,
          idDefault: 1
        },
        success: function (req, res) {
          if (res.length > 0) {
            self.comment = res;
            const commentId = res[0].ColorCommentId;
            self.modelOverAllComment.fetch({
              reset: true,
              data: {
                id: commentId
              },
              success: function (req, res) {
                self.overAllComment = res.OverAllComment;
              }
            });
          }
        }
      });
    },
    renderComment: function () {
      const self = this;
      const data = this.comment;
      if (data != null) {
        let html = '<div class="margintop-5 text-bold-italic">Overall Risk</div>'
        html += '<div class="row mb-5">'
        html += '<div class="col-md-1 mb-5" style="background-color:' + data[0].Warna + ';color:black;font-weight:bold;margin-bottom:5px;">' + data[0].Warna + '</div>'
        html += '<div class="col-md-11 mb-5"></div>'
        html += '</div>'
        html += '<div class="row mb-5">'
        html += '<div style="width:100%;font-weight:normal;margin-bottom: 10px;">'
        html += '<span style="float:left;font-weight:bold;margin-right:5px;">Komentar Keseluruhan: </span>' + this.overAllComment
        html += '</div>'
        html += '</div>'

        html += '<div class="col-md-12 scrollable-matrix" style="margin-bottom: 15px;padding-left: 0px;" obo-table-matrix>'
        html += '<div>'
        html += '<table style="width:100%" class="table-risk-matrix">'
        html += '<tr class="td-kiw-table header-kiw-table">'
        html += '<td class="td-kiw-table">Metrices</td>'
        html += '<td class="td-kiw-table">Comments</td>'
        html += '<td class="td-kiw-table">Action Points</td>'
        html += '</tr>'
        for (const i of data) {
          html += '<tr>'
          html += '<td class="td-kiw-table"><p>' + i.NamaMatrix + '</p></td>'
          if (i.Comment) {
            html += '<td class="td-kiw-table"><p>' + i.Comment + '</p></td>'
          } else {
            html += '<td class="td-kiw-table"><p>-</p></td>'
          }
          if (i.ActionPoint) {
            html += '<td class="td-kiw-table"><p>' + i.ActionPoint + '</p></td>'
          } else {
            html += '<td class="td-kiw-table"><p>-</p></td>'
          }
          html += '</tr>'
        }
        html += '</div>'
        html += '</table>'
        html += '</div>'
        this.$('[overall-comment]').append(html);
      } else {
        this.$('[overall-comment]').append('<p>Data tidak ditemukan.</p>');
      }
    },
    setRoleAccess: function () {

    },
    downloadXLSFile: function () {
      const host = commonConfig.requestServer;
      const uri = host + '/api/Report?isDownloadFile=2';
      const win = window.open(uri, '_blank');
      win.focus();
    }
  });
});
