define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var commonFunction = require('commonfunction');
  var commonConfig = require('commonconfig');
  var Model = require('./../model');
  var eventAggregator = require('eventaggregator');
  require('select2');

  module.exports = View.extend({
    template: _.template(template),
    initialize: function (options) {
      var self = this;
      this.projectIdSelected = options.view;
      this.modelProject = options.model;
      this.model = new Model();
      this.listenTo(this.model, 'request', function () {});
      this.listenTo(this.model, 'sync error', function () {});
    },
    afterRender: function () {
      var self = this;
      var html = '<select class="form-control select" name="NamaProject">';
      $.each(this.modelProject, function(index, item) {
        html += '<option value="'+ item.ProjectId +'">'+ item.NamaProject +'</option>';
      })
      html += '</select>';
      this.$('[data-project-list]').append(html);
      this.setTemplate();
    },
    events: {
        'change [name="NamaProject"]': 'renderCalculation'
    },
    renderCalculation: function(obj) {
      var projectId = this.$('[name="NamaProject"]').val();
      if (projectId != "") {
        eventAggregator.trigger('dashboard/result/project:fecth', projectId);
        this.$el.modal('hide');
      }
    },
    setTemplate: function() {
      if (this.projectIdSelected != "")
        this.$('[name="NamaProject"]').val(this.projectIdSelected);
    }
  });
});