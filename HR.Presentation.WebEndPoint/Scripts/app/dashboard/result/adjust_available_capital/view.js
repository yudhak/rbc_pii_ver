define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var commonFunction = require('commonfunction');
  var commonConfig = require('commonconfig');
  var Model = require('./model');
  var eventAggregator = require('eventaggregator');
  require('bootstrap-validator');
  require('datetimepicker');
  require('select2');
  var moment = require('moment');

  module.exports = View.extend({
    template: _.template(template),
    initialize: function (options) {
      var self = this;
      this.model = new Model();
      this.dataCapital = options.model;
    },
    afterRender: function () {
      this.setTemplate();
    },
    events: {
      'click [btn-save]': 'getValueChanged',
    },
    setTemplate: function() {
      let self = this;
      let html = '<div class="marginbottom-20 scrollable-matrix">'
      html += '<div class="table-mat">'
      html += '<div class="heading-mat">'
      html += '<div class="cell-mat header-column"><p>Name</p></div>'
      $.each(this.dataCapital, function(index, va){
        html += '<div class="cell-mat header-column">'
        html += '<p class="header-column-position">'+ va.Year +'</p>'
        html += '</div>'
      });
      html += '</div>'
      
      html += '<div class="row-mat">'
      html += '<div class="cell-mat"><p>Available Capital</p></div>'
      $.each(this.dataCapital, function(index, va){
        html += '<div class="cell-mat text-center" data-cell-risk="'+ va.Year +'">'
        html += '<div><input type="text" style="width:100px;padding:3px" value="'+ va.Value +'" class="form-control" name="CapitalYear-'+ va.Year +'" /></div>'
        html += '</div>'
      });
      html += '</div>'

      html += '</div>'
      html += '</div>'

      this.$('[data-available-capital]').append(html);
    },
    getValueChanged: function() {
      let self = this;
      let data = [];
      if (this.dataCapital) {
        for (let i = 0; i < this.dataCapital.length; i++) {
          const year = this.dataCapital[i].Year;
          const oldValue = this.dataCapital[i].Value;
          const newValue = this.$('[name="CapitalYear-'+ year +'"]').val();
          let dataChanged = {};
          if (oldValue != newValue) {
            dataChanged.Year = year;
            dataChanged.Value = newValue;
            data.push(dataChanged);
          }
        }
      } else {
        commonFunction.responseWarningCannotExecute("Jumlah data list tahun dan nilai Available Capital tidak ditemukan.");
      }
      this.doSave(data);
    },
    getConfirmation: function(){
      var statusDate = this.doValidateDate();
      var statusMaxMin = this.doValidateMaxMin();
      if(statusDate && statusMaxMin) {
        var data = this.$('[name="NamaProject"]').val();
        var action = "tambah";
        var retVal = confirm("Apakah anda yakin ingin " + action + " Project : "+ data +" ?");
        if( retVal == true ){
          this.doSave();
        }
        else{
          this.$('[type="submit"]').attr('disabled', false);
        }
      } else {
        this.$('[type="submit"]').attr('disabled', false);
      }
    },
    doSave: function (data) {
      let obj = {};
      obj.ListChanged = data;
      console.log(obj);
      this.model.save(obj);
    }
  });
});