define(function(require, exports, module) {
    'use strict';

    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');
    var Model = require('./model');
    var Collection = require('./collection');
    var ModelRiskBudget = require('./modelResultRiskBudget');
    var ModelDashboard = require('./../calculation/adjust_available_capital/model');
    const numeral = require('numeral');
    const commonConfig = require('commonconfig');
    require('highcharts');

    module.exports = LayoutManager.extend({
        el: false,
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.projectId = 0;
            this.sectorId = 0;
            this.riskId = 0;

            this.projects = null;
            this.risks = null;
            this.sektors = null;

            this.availableCapitalVSRiskcapitalData = null;
            this.leverageRatioChartYear = null;
            this.leverageRatioChartData = null;
            this.liquidityChartYear = null;
            this.liquidityChartData = null;

            this.diversifiedsektorchartyear = null;
            this.diversifiedsektorchartdata = null;
            this.diversifiedProjectChartYear = null;
            this.diversifiedProjectChartData = null;

            this.riskBudgetRiskCategoryChartYear = null;
            this.riskBudgetRiskCategoryChartData = null;
            this.riskBudgetProjectSectorChartYear = null;
            this.riskBudgetProjectSectorChartData = null;
            this.riskBudgetProjectRiskChartYear = null;
            this.riskBudgetProjectRiskChartData = null;

            this.sensitivityLiquidityChart = null;
            this.sensitivityScenarioChart = null;
            this.sensitivityYearChart = null;

            this.stressTestingYearChart = null;
            this.stressTestingSensitivityChart = null;
            this.stressTestingRiskCapitalChart = null;
            this.stressTestingAvailableCapital = null;
            this.projectMonitoring = null;
            this.sektorMonitoring = null;

            this.model = new Model();
            this.modelRiskBudget = new ModelRiskBudget();
            this.modelDashboard = new ModelDashboard();
            this.isDataAvailable = 0;
            this.listenToOnce(this.model, 'sync', function(model) {
                this.render();
            });
            this.listenTo(eventAggregator, 'dashboard/result/project:fecth', function(projectId) {
                self.renderProjectChanged(projectId);
            });
            this.listenTo(eventAggregator, 'dashboard/result/sektor:fecth', function(sektorId) {
                self.renderSektorChanged(sektorId);
            });
            this.listenTo(eventAggregator, 'dashboard/result/risk:fecth', function(riskId) {
                self.renderRiskChanged(riskId);
            });
            this.fetchData();
            this.roles = JSON.parse(localStorage.getItem('roles'));
        },
        events: {
            'click [change-project]': 'chooseProject',
            'click [change-sektor]': 'chooseSektor',
            'click [change-risk]': 'chooseRisk',
            'click [btn-riskbudgetprojectrisk-reload]': 'reloadAllRiskBudgetChart',
            'click [btn-riskbudgetprojectsector-reload]': 'reloadRiskBudgetProjectSectorChart',
            'click [btn-riskbudgetriskcategory-reload]': 'reloadRiskBudgetRiskCategoryChart',
            'click [btn-diversifiedsektorchart-reload]': 'reloadAllDiversifiedBenefitAchievedChart',
            'click [btn-availablecapitalvsriskcapital-reload]': 'reloadAvailableCapitalVSRiskCapitalChart',
            'click [btn-diversificatioratiochart-reload]': 'reloadAllLeverageRatioChart',
            'click [btn-liquiditychart-reload]': 'reloadLiquidityChart',
            'click [btn-sensitivitychart-reload]': 'reloadSensitivityChart',
            'click [btn-stresstestingchart-reload]': 'reloadStressTestingChart',
            'click [btn-ajdust-capital]': 'openAdjustAvailable',
            'click [name="ExportXLS"]': "downloadXLSFile"
        },
        afterRender: function() {
            var self = this;
            this.setTemplate(this.model);
            //this.renderCurrencyFormat();
            this.setRoleAccess();
        },
        fetchData: function() {
            this.model.set(this.model.idAttribute, 2038);
            this.model.set(this.model.idAttribute, 4);
            this.model.fetch({
                reset: true,
                data: {
                    projectId: 0,
                    riskRegistrasiId: 0,
                    sektorId: 0
                }
            });
            commonFunction.showLoadingSpinner();
        },
        setTemplate: function(data) {
            var self = this;
            this.isDataAvailable += 1;
            if (this.isDataAvailable > 1) {
                this.renderDiversifiedBenefitAchieved(data.attributes.DiversifiedBenefitAchieved, data.attributes.ProjectMonitoring, data.attributes.SektorMonitoring);
                this.renderLeverageRation(data.attributes.LeverageRatio);
                this.renderAvailableVSRiskCapital(data.attributes.AvailableVSRiskCapital);
                this.renderLiquidity(data.attributes.Liquidity);
                this.renderRiskBudget(data.attributes.RiskBudget);
                this.renderSensitivity(data.attributes.SensitivityStressTesting.SensitivityResult);
                this.renderStressTesting(data.attributes.SensitivityStressTesting.StressTestingResult);
                this.projects = data.attributes.RiskBudget.RiskBudgetByProject.ProjectCollection;
                this.sektors = data.attributes.RiskBudget.RiskBudgetBySektor.SektorCollection;
                this.risks = data.attributes.RiskBudget.RiskBudgetByRisk.RiskRegistrasiCollection;
                commonFunction.closeLoadingSpinner();
                // self.$('[name="ExportXLS"]').removeClass('hide');
            }
        },
        renderCurrencyFormat: function() {
            this.$('[currency-formating]').mask('000,000,000,000,000.00', {reverse: true});
        },
        renderDiversifiedBenefitAchieved: function(data, projectMonitor, sektorMonitor) {
            var self = this;
            var html = '<div class="row">'

                html += '<div class="col-md-12">'
                html += '<div class="panel panel-default">'
                html += '<div><button type="button" btn-diversifiedsektorchart-reload>Refresh</button></div>'
                html += '<div class="panel-body" id="diversifiedsektorchart"></div>'
                html += '</div>'
                html += '</div>'
                html += '<div class="col-md-12">'
                html += '<div class="panel panel-default">'
                html += '<div class="panel-body" id="diversifiedprojectchart"></div>'
                html += '</div>'
                html += '</div>'

                html += '<div class="col-md-12">'
                html += '<div class="margintop-5 text-bold-italic">Sektor</div>'
                html += '<div class="marginbottom-20 scrollable-matrix">'
                html += '<div class="table-mat">'
                html += '<div class="heading-mat">'
                html += '<div class="cell-mat header-column"><p>Sektor</p></div>'
                $.each(data.DiversifiedBenefitAchievedBySektor.YearSektorCollection, function(index, va){
                    html += '<div class="cell-mat header-column">'
                    html += '<p class="header-column-position">'+ va.Year +'</p>'
                    html += '</div>'
                });
                html += '</div>'

                $.each(sektorMonitor, function (i, lue) {
                    $.each(data.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved, function(i, value) {
                        if(lue == value.SektorId){
                            html += '<div class="row-mat">'
                            html += '<div class="cell-mat" data="'+ value.SektorId+'"><p>'+ value.NamaSektor +'</p></div>'
                            
                            $.each(data.DiversifiedBenefitAchievedBySektor.YearSektorCollection, function(index, val) {
                                html += '<div class="cell-mat text-center" data-cell-risk="'+ val.Year +'">'
                                $.grep(val.YearSektorValue, function(ind) {
                                    if (ind.SektorId == value.SektorId) {
                                        html += '<div class="text-right"><p currency-formating>'+ numeral(ind.Value).format('0,0.00') +'</p></div>'  
                                    }
                                });
                                html += '</div>'
                            });
                            html += '</div>'

                        }
                    });
                });
                //total
                html += '<div class="cell-mat text-center total-value-matrix"><p>TOTAL</p></div>'
                $.each(data.DiversifiedBenefitAchievedBySektor.YearSektorCollection, function(index, value) {
                    html += '<div class="cell-mat text-right total-value-matrix"><p currency-formating>'+ numeral(value.Total).format('0,0.00') +'</p></div>'
                });
                
                html += '</div>'
                html += '</div>'
                //end of sektor

                //project
                html += '<div class="margintop-5 text-bold-italic">Project</div>'
                html += '<div class="marginbottom-20 scrollable-matrix">'
                html += '<div class="table-mat">'
                html += '<div class="heading-mat">'
                html += '<div class="cell-mat header-column"><p>Project</p></div>'
                $.each(data.DiversifiedBenefitAchievedByProject.YearProjectCollection, function(index, va){
                    html += '<div class="cell-mat header-column">'
                    html += '<p class="header-column-position">'+ va.Year +'</p>'
                    html += '</div>'
                });
                html += '</div>'

                $.each(projectMonitor, function (i, lue) {
                    $.each(data.DiversifiedBenefitAchievedByProject.ProjectDiversifiedBenefitAchieved, function(i, value) {
                        if(lue == value.ProjectId){
                            html += '<div class="row-mat">'
                            html += '<div class="cell-mat" data="'+ value.ProjectId+'"><p>'+ value.NamaProject +'</p></div>'
                            
                            $.each(data.DiversifiedBenefitAchievedByProject.YearProjectCollection, function(index, val) {
                                html += '<div class="cell-mat text-center" data-cell-risk="'+ val.Year +'">'
                                $.grep(val.YearProjectValue, function(ind) {
                                    if (ind.ProjectId == value.ProjectId) {
                                        html += '<div class="text-right"><p currency-formating>'+ numeral(ind.Value).format('0,0.00') +'</p></div>'  
                                    }
                                });
                                html += '</div>'
                            });
                            html += '</div>'
                        }
                    });
                });
                
                //total
                html += '<div class="cell-mat text-center total-value-matrix"><p>TOTAL</p></div>'
                $.each(data.DiversifiedBenefitAchievedByProject.YearProjectCollection, function(index, value) {
                    html += '<div class="cell-mat text-right total-value-matrix"><p currency-formating>'+ numeral(value.Total).format('0,0.00') +'</p></div>'
                });
                
                html += '</div>'
                html += '</div>'
                //end of project

                html += '</div>'

                
                // html += '<div class="col-md-6">'
                // html += '<div class="panel panel-default">'
                // html += '<div class="panel-body" id="mitigationchart"></div>'
                // html += '</div>'
                html += '</div>'

                html += '</div>'

            self.$('[diversified-benefit-achieved-container]').append(html);
            self.diversifiedsektorchartyear = data.DiversifiedBenefitAchievedBySektor.YearSektorCollection;
            self.diversifiedsektorchartdata = data.DiversifiedBenefitAchievedBySektor;
            self.diversifiedProjectChartYear = data.DiversifiedBenefitAchievedByProject.YearProjectCollection;
            self.diversifiedProjectChartData = data.DiversifiedBenefitAchievedByProject;
            self.projectMonitoring = projectMonitor;
            self.sektorMonitoring = sektorMonitor;

            self.renderDiversifiedSektorChart(data.DiversifiedBenefitAchievedBySektor.YearSektorCollection, data.DiversifiedBenefitAchievedBySektor, sektorMonitor);
            self.renderDiversifiedProjectChart(data.DiversifiedBenefitAchievedByProject.YearProjectCollection, data.DiversifiedBenefitAchievedByProject, projectMonitor);
        },
        reloadAllDiversifiedBenefitAchievedChart: function() {
            var yearSector = this.diversifiedsektorchartyear;
            var dataSector = this.diversifiedsektorchartdata;
            var yearProject = this.diversifiedProjectChartYear;
            var dataProject = this.diversifiedProjectChartData;
            var dataProjectMonitor = this.projectMonitoring;
            var dataSektorMonitor = this.sektorMonitoring;

            this.reloadDiversifiedSektorChart(yearSector, dataSector, dataSektorMonitor);
            this.reloadDiversifiedProjectChart(yearProject, dataProject, dataProjectMonitor);
        },
        reloadDiversifiedSektorChart: function(year, values, dataSektorMonitor) {
            var xAxisCategories = [];
            var dataCollection = [];
            $.each(year, function(i, item) {
                xAxisCategories.push(item.Year);
            });
            $.each(dataSektorMonitor, function (i, lue) {
                for (var a = 0; a < values.SektorDiversifiedBenefitAchieved.length; a++) {
                    if(lue == values.SektorDiversifiedBenefitAchieved[a].SektorId){
                        var dataObject = {};
                        var dataPerSektor = [];
                        var name = values.SektorDiversifiedBenefitAchieved[a].NamaSektor;
                        $.each(year, function(i, item) {
                            $.each(item.YearSektorValue, function(e, val) {
                                if (val.SektorId == values.SektorDiversifiedBenefitAchieved[a].SektorId) {
                                    dataPerSektor.push(val.Value);
                                }
                            });
                        });
                        dataObject.data = dataPerSektor;
                        dataObject.name = name;
                        dataCollection.push(dataObject);
                    }
                };
            });
            var seriesCategories = dataCollection;
            Highcharts.setOptions({
                global: {
                    useUTC: false,
                },
                lang: {
                  decimalPoint: '.',
                  thousandsSep: ','
                }
            });
            Highcharts.chart('diversifiedsektorchart', {
                chart: {
                    type: 'column',
                    marginBottom: 80
                },
                title: {
                    text: 'Diversification Achieved: Sector'
                },
                xAxis: {
                    categories: xAxisCategories
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'IN IDR BIO'
                    }
                },
                legend: {
                    align: 'right',
                    x: -30,
                    verticalAlign: 'bottom',
                    y: 15,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                    valueDecimals: 2,
                    pointFormat: '{series.name}: <b>{point.y}</b><br/>Total: <b>{point.stackTotal:,.2f}</b>'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: false,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    }
                },
                series: seriesCategories
            });
        },
        reloadDiversifiedProjectChart: function(year, values, dataProjectMonitor) {
            var xAxisCategories = [];
            var dataCollection = [];
            $.each(year, function(i, item) {
                xAxisCategories.push(item.Year);
            });
            $.each(dataProjectMonitor, function (i, lue) {
                for (var a = 0; a < values.ProjectDiversifiedBenefitAchieved.length; a++) {
                    if(lue == values.ProjectDiversifiedBenefitAchieved[a].ProjectId){
                        var dataObject = {};
                        var dataPerProject = [];
                        var name = values.ProjectDiversifiedBenefitAchieved[a].NamaProject;
                        $.each(year, function(i, item) {
                            $.each(item.YearProjectValue, function(e, val) {
                                if (val.ProjectId == values.ProjectDiversifiedBenefitAchieved[a].ProjectId) {
                                    dataPerProject.push(val.Value);
                                }
                            });
                        });
                        dataObject.data = dataPerProject;
                        dataObject.name = name;
                        dataCollection.push(dataObject);
                    }
                };
            });
            var seriesCategories = dataCollection;
            Highcharts.setOptions({
                global: {
                    useUTC: false,
                },
                lang: {
                  decimalPoint: '.',
                  thousandsSep: ','
                }
            });
            Highcharts.chart('diversifiedprojectchart', {
                chart: {
                    height: 600,
                    type: 'column',
                    marginBottom: 150
                },
                title: {
                    text: 'Diversification Achieved: Project'
                },
                xAxis: {
                    categories: xAxisCategories
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'IN IDR BIO'
                    }
                },
                legend: {
                    align: 'right',
                    x: -30,
                    verticalAlign: 'bottom',
                    y: 15,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                    valueDecimals: 2,
                    pointFormat: '{series.name}: <b>{point.y}</b><br/>Total: <b>{point.stackTotal:,.2f}</b>'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: false,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    }
                },
                series: seriesCategories
            });
        },
        renderDiversifiedSektorChart: function(year, values, sektorMonitor) {
            var xAxisCategories = [];
            var dataCollection = [];
            $.each(year, function(i, item) {
                xAxisCategories.push(item.Year);
            });
            $.each(sektorMonitor, function (i, lue) {
                for (var a = 0; a < values.SektorDiversifiedBenefitAchieved.length; a++) {
                    if(lue == values.SektorDiversifiedBenefitAchieved[a].SektorId){
                        var dataObject = {};
                        var dataPerSektor = [];
                        var name = values.SektorDiversifiedBenefitAchieved[a].NamaSektor;
                        $.each(year, function(i, item) {
                            $.each(item.YearSektorValue, function(e, val) {
                                if (val.SektorId == values.SektorDiversifiedBenefitAchieved[a].SektorId) {
                                    dataPerSektor.push(val.Value);
                                }
                            });
                        });
                        dataObject.data = dataPerSektor;
                        dataObject.name = name;
                        dataCollection.push(dataObject);
                    }
                };
            });
                
            var seriesCategories = dataCollection;
            Highcharts.setOptions({
                global: {
                    useUTC: false,
                },
                lang: {
                  decimalPoint: '.',
                  thousandsSep: ','
                }
            });
            Highcharts.chart('diversifiedsektorchart', {
                chart: {
                    type: 'column',
                    marginBottom: 80
                },
                title: {
                    text: 'Diversification Achieved: Sector'
                },
                xAxis: {
                    categories: xAxisCategories
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'IN IDR BIO'
                    }
                },
                legend: {
                    align: 'right',
                    x: -30,
                    verticalAlign: 'bottom',
                    y: 15,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                    valueDecimals: 2,
                    pointFormat: '{series.name}: <b>{point.y}</b><br/>Total: <b>{point.stackTotal:,.2f}</b>',
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: false,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    }
                },
                series: seriesCategories
            });
        },
        renderDiversifiedProjectChart: function(year, values, projectMonitor) {
            var xAxisCategories = [];
            var dataCollection = [];
            $.each(year, function(i, item) {
                xAxisCategories.push(item.Year);
            });
            $.each(projectMonitor, function (i, lue) {
                for (var a = 0; a < values.ProjectDiversifiedBenefitAchieved.length; a++) {
                    if(lue == values.ProjectDiversifiedBenefitAchieved[a].ProjectId){
                        var dataObject = {};
                        var dataPerProject = [];
                        var name = values.ProjectDiversifiedBenefitAchieved[a].NamaProject;
                        $.each(year, function(i, item) {
                            $.each(item.YearProjectValue, function(e, val) {
                                if (val.ProjectId == values.ProjectDiversifiedBenefitAchieved[a].ProjectId) {
                                    dataPerProject.push(val.Value);
                                }
                            });
                        });
                        dataObject.data = dataPerProject;
                        dataObject.name = name;
                        dataCollection.push(dataObject);
                    }
                };
            });
            var seriesCategories = dataCollection;
            Highcharts.setOptions({
                global: {
                    useUTC: false,
                },
                lang: {
                  decimalPoint: '.',
                  thousandsSep: ','
                }
            });
            Highcharts.chart('diversifiedprojectchart', {
                chart: {
                    height: 600,
                    type: 'column',
                    marginBottom: 150
                },
                title: {
                    text: 'Diversification Achieved: Project'
                },
                xAxis: {
                    categories: xAxisCategories
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'IN IDR BIO'
                    }
                },
                legend: {
                    align: 'right',
                    x: -30,
                    verticalAlign: 'bottom',
                    y: 15,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                    valueDecimals: 2,
                    pointFormat: '{series.name}: <b>{point.y}</b><br/>Total: <b>{point.stackTotal:,.2f}</b>'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: false,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    }
                },
                series: seriesCategories
            });
        },
        renderLeverageRation: function(data) {
            var self = this;
            var html = '<div class="row">'
                html += '<div class="ml-10"><button type="button" btn-diversificatioratiochart-reload>Refresh</button></div>'
                html += '<div class="col-md-4">'
                html += '<div class="panel panel-default">'
                html += '<div class="panel-body" id="diversificatioratio"></div>'
                html += '</div>'
                html += '</div>'
                html += '<div class="col-md-4">'
                html += '<div class="panel panel-default">'
                html += '<div class="panel-body" id="leverageratiochart"></div>'
                html += '</div>'
                html += '</div>'
                html += '<div class="col-md-4">'
                html += '<div class="panel panel-default">'
                html += '<div class="panel-body" id="mitigationchart"></div>'
                html += '</div>'
                html += '</div>'

                html += '<div class="col-md-12">'
                html += '<div class="margintop-5 text-bold-italic">Leverage Ratios</div>'
                html += '<div class="marginbottom-20 scrollable-matrix">'
                html += '<div class="table-mat">'
                html += '<div class="heading-mat">'
                html += '<div class="cell-mat header-column"><p>Name</p></div>'
                $.each(data.YearCollectionLeverage, function(index, va){
                    html += '<div class="cell-mat header-column">'
                    html += '<p class="header-column-position">'+ va.Year +'</p>'
                    html += '</div>'
                });
                html += '</div>'

                $.each(data.LeverageItem, function(i, value) {
                    html += '<div class="row-mat">'
                    html += '<div class="cell-mat"><p>'+ value.Name +'</p></div>'
                    
                    $.each(data.YearCollectionLeverage, function(index, val) {
                        html += '<div class="cell-mat text-center" data-cell-risk="'+ val.Year +'">'
                        $.grep(value.Values, function(ind, lue) {
                            if (ind.Year == val.Year) {
                                html += '<div class="text-right"><p currency-formating>'+ numeral(ind.Value).format('0,0.00') +'</p></div>'
                            }
                        });
                        html += '</div>'
                    });
                    html += '</div>'
                });
                
                html += '</div>'
                html += '</div>'
                //end of leverage ratios

                html += '</div>'
                html += '</div>'

            self.$('[leverage-ratio-container]').append(html);

            self.renderDiversificationRatio(data.YearCollectionLeverage, data.LeverageItem);
            self.renderLeverageRatio(data.YearCollectionLeverage, data.LeverageItem);
            self.renderMitigation(data.YearCollectionLeverage, data.LeverageItem);
            self.leverageRatioChartYear = data.YearCollectionLeverage;
            self.leverageRatioChartData = data.LeverageItem;
        },
        renderDiversificationRatio: function(year, values) {
            var seriesCategories = [];
            var xAxisCategories = [];
            var dataTitle = "Diversification Ratio"
            $.grep(values, function(value, index) {
                if (value.Name == dataTitle) {
                    $.each(value.Values, function(i, val){
                        seriesCategories.push(val.Value);
                    });
                }
            });
            $.each(year, function(index, value) {
                xAxisCategories.push(value.Year);
            });
            Highcharts.setOptions({
                global: {
                    useUTC: false,
                },
                lang: {
                  decimalPoint: '.',
                  thousandsSep: ','
                }
            });
            Highcharts.chart('diversificatioratio', {
                title: {
                    text: dataTitle
                },
                xAxis: {
                    categories: xAxisCategories
                },
                tooltip: {
                    valueDecimals: 2,
                    pointFormat: '<b>{point.y}</b>'
                },
                plotOptions: {
                    series: {
                        allowPointSelect: true
                    }
                },
                series: [{
                    data: seriesCategories,
                    name: 'Tahun'
                }]
            });
        },
        renderLeverageRatio: function(year, values) {
            var seriesCategories = [];
            var xAxisCategories = [];
            var dataTitle = "Leveratio Ratio"
            $.grep(values, function(value, index) {
                if (value.Name == dataTitle) {
                    $.each(value.Values, function(i, val){
                        seriesCategories.push(val.Value);
                    });
                }
            });
            $.each(year, function(index, value) {
                xAxisCategories.push(value.Year);
            });
            Highcharts.setOptions({
                global: {
                    useUTC: false,
                },
                lang: {
                  decimalPoint: '.',
                  thousandsSep: ','
                }
            });
            Highcharts.chart('leverageratiochart', {
                title: {
                    text: dataTitle
                },
                xAxis: {
                    categories: xAxisCategories
                },
                tooltip: {
                    valueDecimals: 2,
                    pointFormat: '<b>{point.y}</b>'
                },
                plotOptions: {
                    series: {
                        allowPointSelect: true
                    }
                },
                series: [{
                    data: seriesCategories,
                    name: 'Tahun'
                }]
            });
        },
        renderMitigation: function(year, values) {
            var seriesCategories = [];
            var xAxisCategories = [];
            var dataTitle = "Mitigation Effectiveness"
            $.grep(values, function(value, index) {
                if (value.Name == dataTitle) {
                    $.each(value.Values, function(i, val){
                        seriesCategories.push(val.Value);
                    });
                }
            });
            $.each(year, function(index, value) {
                xAxisCategories.push(value.Year);
            });
            Highcharts.chart('mitigationchart', {
                title: {
                    text: dataTitle
                },
                xAxis: {
                    categories: xAxisCategories
                },
                tooltip: {
                    valueDecimals: 2,
                    pointFormat: '<b>{point.y}</b>'
                },
                plotOptions: {
                    series: {
                        allowPointSelect: true
                    }
                },
                series: [{
                    data: seriesCategories,
                    name: 'Tahun'
                }]
            });
        },
        reloadAllLeverageRatioChart: function() {
            var year = this.leverageRatioChartYear;
            var values = this.leverageRatioChartData;
            this.reloadDiversificationRatioChart(year, values);
            this.reloadLeverageRatioChart(year, values);
            this.reloadMitigationChart(year, values);
        },
        reloadDiversificationRatioChart: function(year, values) {
            var seriesCategories = [];
            var xAxisCategories = [];
            var dataTitle = "Diversification Ratio"
            $.grep(values, function(value, index) {
                if (value.Name == dataTitle) {
                    $.each(value.Values, function(i, val){
                        seriesCategories.push(val.Value);
                    });
                }
            });
            $.each(year, function(index, value) {
                xAxisCategories.push(value.Year);
            });
            Highcharts.chart('diversificatioratio', {
                title: {
                    text: dataTitle
                },
                xAxis: {
                    categories: xAxisCategories
                },
                tooltip: {
                    valueDecimals: 2,
                    pointFormat: '<b>{point.y}</b>'
                },
                plotOptions: {
                    series: {
                        allowPointSelect: true
                    }
                },
                series: [{
                    data: seriesCategories,
                    name: 'Tahun'
                }]
            });
        },
        reloadLeverageRatioChart: function(year, values) {
            var seriesCategories = [];
            var xAxisCategories = [];
            var dataTitle = "Leveratio Ratio"
            $.grep(values, function(value, index) {
                if (value.Name == dataTitle) {
                    $.each(value.Values, function(i, val){
                        seriesCategories.push(val.Value);
                    });
                }
            });
            $.each(year, function(index, value) {
                xAxisCategories.push(value.Year);
            });
            Highcharts.chart('leverageratiochart', {
                title: {
                    text: dataTitle
                },
                xAxis: {
                    categories: xAxisCategories
                },
                tooltip: {
                    valueDecimals: 2,
                    pointFormat: '<b>{point.y}</b>'
                },
                plotOptions: {
                    series: {
                        allowPointSelect: true
                    }
                },
                series: [{
                    data: seriesCategories,
                    name: 'Tahun'
                }]
            });
        },
        reloadMitigationChart: function(year, values) {
            var seriesCategories = [];
            var xAxisCategories = [];
            var dataTitle = "Mitigation Effectiveness"
            $.grep(values, function(value, index) {
                if (value.Name == dataTitle) {
                    $.each(value.Values, function(i, val){
                        seriesCategories.push(val.Value);
                    });
                }
            });
            $.each(year, function(index, value) {
                xAxisCategories.push(value.Year);
            });
            Highcharts.chart('mitigationchart', {
                title: {
                    text: dataTitle
                },
                xAxis: {
                    categories: xAxisCategories
                },
                tooltip: {
                    valueDecimals: 2,
                    pointFormat: '<b>{point.y}</b>'
                },
                plotOptions: {
                    series: {
                        allowPointSelect: true
                    }
                },
                series: [{
                    data: seriesCategories,
                    name: 'Tahun'
                }]
            });
        },
        renderAvailableVSRiskCapital: function(data) {
            var self = this;
            var html = '<div class="row">'

                html += '<div class="col-md-12">'
                html += '<div class="panel panel-default">'
                html += '<div><button type="button" btn-availablecapitalvsriskcapital-reload>Refresh</button></div>'
                html += '<div class="panel-body" id="availablecapitalvsriskcapital"></div>'
                html += '</div>'
                html += '</div>'
                html += '</div>'
                
                html += '<div class="col-md-12">'
                html += '<div class="margintop-5 text-bold-italic">Available Capital VS Risk Capital Projection</div>'
                html += '<div class="marginbottom-20 scrollable-matrix">'
                html += '<div class="table-mat">'
                html += '<div class="heading-mat">'
                html += '<div class="cell-mat header-column"><p>Name</p></div>'
                $.each(data.AvailableVSRiskCapitalYearCollection, function(index, va){
                    html += '<div class="cell-mat header-column">'
                    html += '<p class="header-column-position">'+ va.Year +'</p>'
                    html += '</div>'
                });
                html += '</div>'

                $.each(data.AvailableVSRiskCapitalCollection, function(i, value) {
                    html += '<div class="row-mat">'
                    html += '<div class="cell-mat"><p>'+ value.Name +'</p></div>'
                    
                    $.each(data.AvailableVSRiskCapitalYearCollection, function(index, val) {
                        html += '<div class="cell-mat text-center" data-cell-risk="'+ val.Year +'">'
                        $.grep(value.Values, function(ind, lue) {
                            if (ind.Year == val.Year) {
                                html += '<div class="text-right"><p currency-formating data-capital="'+ val.Year +'" value="'+ ind.Value +'">'+ numeral(ind.Value).format('0,0.00') +'</p></div>'
                            }
                        });
                        html += '</div>'
                    });
                    html += '</div>'
                });
                
                html += '</div>'
                html += '</div>'
                //end of availableVS

                html += '</div>'

                html += '</div>'

            self.$('[available-risk-container]').append(html);
            self.renderAvailableCapitalVSRiskcapitalChart(data);
            self.availableCapitalVSRiskcapitalData = data;
        },
        renderAvailableCapitalVSRiskcapitalChart: function(data) {
            var xAxisCategories = [];
            var diversifiedRiskCapitalColumnValues = [];
            var totalMaximumExposureColumnValues = [];
            var availableCapitalLineValues = [];
            var firstColumnName = "Diversified Risk Capital";
            var secondColumnName = "Total Maximum Exposure";
            var lineName = "Available Capital";
            $.each(data.AvailableVSRiskCapitalYearCollection, function(i, item) {
                xAxisCategories.push(item.Year);
            });
            $.each(data.AvailableVSRiskCapitalCollection, function(i, item) {
                switch(item.Name) {
                    case "Diversified Risk Capital":
                        $.each(item.Values, function(e, val) {
                            diversifiedRiskCapitalColumnValues.push(val.Value);
                        });
                        break;
                    case "Total Maximum Exposure":
                        $.each(item.Values, function(e, val) {
                            totalMaximumExposureColumnValues.push(val.Value);
                        });
                        break;
                    case "Available Capital":
                        $.each(item.Values, function(e, val) {
                            availableCapitalLineValues.push(val.Value);
                        });
                        break;
                }
            });
            Highcharts.chart('availablecapitalvsriskcapital', {
                title: {
                    text: 'Available Capital VS Risk Capital Projection'
                },
                xAxis: {
                    categories: xAxisCategories
                },
                labels: {
                    items: [{
                        html: '',
                        style: {
                            left: '50px',
                            top: '18px',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                        }
                    }]
                },
                series: [{
                    type: 'column',
                    name: firstColumnName,
                    data: diversifiedRiskCapitalColumnValues,
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Diversified Risk Capital: <b>{point.y}</b>'
                    }
                }, {
                    type: 'column',
                    name: secondColumnName,
                    data: totalMaximumExposureColumnValues,
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Total Maksimum Exposure: <b>{point.y}</b>'
                    }
                },{
                    type: 'spline',
                    name: 'Average',
                    data: availableCapitalLineValues,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white',
                    },
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Average: <b>{point.y}</b>'
                    }
                }]
            });
            this.availableCapitalValue = availableCapitalLineValues;
            this.availableCapitalYear = xAxisCategories;
        },
        reloadAvailableCapitalVSRiskCapitalChart: function() {
            var data = this.availableCapitalVSRiskcapitalData;
            var xAxisCategories = [];
            var diversifiedRiskCapitalColumnValues = [];
            var totalMaximumExposureColumnValues = [];
            var availableCapitalLineValues = [];
            var firstColumnName = "Diversified Risk Capital";
            var secondColumnName = "Total Maximum Exposure";
            var lineName = "Available Capital";
            $.each(data.AvailableVSRiskCapitalYearCollection, function(i, item) {
                xAxisCategories.push(item.Year);
            });
            $.each(data.AvailableVSRiskCapitalCollection, function(i, item) {
                switch(item.Name) {
                    case "Diversified Risk Capital":
                        $.each(item.Values, function(e, val) {
                            diversifiedRiskCapitalColumnValues.push(val.Value);
                        });
                        break;
                    case "Total Maximum Exposure":
                        $.each(item.Values, function(e, val) {
                            totalMaximumExposureColumnValues.push(val.Value);
                        });
                        break;
                    case "Available Capital":
                        $.each(item.Values, function(e, val) {
                            availableCapitalLineValues.push(val.Value);
                        });
                        break;
                }
            });
            Highcharts.chart('availablecapitalvsriskcapital', {
                title: {
                    text: 'Available Capital VS Risk Capital Projection'
                },
                xAxis: {
                    categories: xAxisCategories
                },
                labels: {
                    items: [{
                        html: '',
                        style: {
                            left: '50px',
                            top: '18px',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                        }
                    }]
                },
                series: [{
                    type: 'column',
                    name: firstColumnName,
                    data: diversifiedRiskCapitalColumnValues,
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Diversified Risk Capital: <b>{point.y}</b>'
                    }
                }, {
                    type: 'column',
                    name: secondColumnName,
                    data: totalMaximumExposureColumnValues,
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Total Maksimum Exposure: <b>{point.y}</b>'
                    }
                },{
                    type: 'spline',
                    name: 'Average',
                    data: availableCapitalLineValues,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    },
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Average: <b>{point.y}</b>'
                    }
                }]
            });
        },
        renderLiquidity: function(data) {
            var self = this;
            var html = '<div class="row">'

                html += '<div class="col-md-12">'
                html += '<div class="panel panel-default">'
                html += '<div><button type="button" btn-liquiditychart-reload>Refresh</button></div>'
                html += '<div class="panel-body" id="liquiditychart"></div>'
                html += '</div>'
                html += '</div>'
                
                html += '<div class="col-md-12">'
                html += '<div class="margintop-5 text-bold-italic">Liquidity</div>'
                html += '<div class="marginbottom-20 scrollable-matrix">'
                html += '<div class="table-mat">'
                html += '<div class="heading-mat">'
                html += '<div class="cell-mat header-column"><p>Name</p></div>'
                $.each(data.LiquidityYearCollection, function(index, va){
                    html += '<div class="cell-mat header-column">'
                    html += '<p class="header-column-position">'+ va.Year +'</p>'
                    html += '</div>'
                });
                html += '</div>'

                $.each(data.LiquidityCollection, function(i, value) {
                    html += '<div class="row-mat">'
                    html += '<div class="cell-mat"><p>'+ value.Name +'</p></div>'
                    
                    $.each(data.LiquidityYearCollection, function(index, val) {
                        html += '<div class="cell-mat text-center" data-cell-risk="'+ val.Year +'">'
                        $.grep(value.Values, function(ind, lue) {
                            if (ind.Year == val.Year) {
                                html += '<div class="text-right"><p currency-formating>'+ numeral(ind.Value).format('0,0.00') +'</p></div>'
                            }
                        });
                        html += '</div>'
                    });
                    html += '</div>'
                });
                
                html += '</div>'
                html += '</div>'
                //end of availableVS

                html += '</div>'

                html += '</div>'

            self.$('[liquidity-container]').append(html);
            self.renderLiquidityChart(data.LiquidityYearCollection, data.LiquidityCollection);
            self.liquidityChartYear = data.LiquidityYearCollection;
            self.liquidityChartData = data.LiquidityCollection;
        },
        renderLiquidityChart: function(year, data) {
            var xAxisCategories = [];
            var riskCapitalValues = [];
            var liqudityValues = [];
            $.each(year, function(i, item) {
                xAxisCategories.push(item.Year);
            });
            $.each(data, function(i, item) {
                switch(item.Name) {
                    case "Risk Capital":
                        $.each(item.Values, function(e, val) {
                            riskCapitalValues.push(val.Value);
                        });
                        break;
                    case "Liquidity":
                        $.each(item.Values, function(e, val) {
                            liqudityValues.push(val.Value);
                        });
                        break;
                }
            });
            Highcharts.chart('liquiditychart', {
                title: {
                    text: 'Risk Capital vs Liquidity'
                },
                xAxis: {
                    categories: xAxisCategories
                },
                labels: {
                    items: [{
                        html: '',
                        style: {
                            left: '50px',
                            top: '18px',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                        }
                    }]
                },
                series: [{
                    type: 'column',
                    name: 'Risk Capital',
                    data: riskCapitalValues,
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Risk Capital: <b>{point.y}</b>'
                    }
                }, {
                    type: 'spline',
                    name: 'Liquidity',
                    data: liqudityValues,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    },
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Liqudity: <b>{point.y}</b>'
                    }
                }]
            });
        },
        reloadLiquidityChart: function() {
            var year = this.liquidityChartYear;
            var data = this.liquidityChartData;
            var xAxisCategories = [];
            var riskCapitalValues = [];
            var liqudityValues = [];
            $.each(year, function(i, item) {
                xAxisCategories.push(item.Year);
            });
            $.each(data, function(i, item) {
                switch(item.Name) {
                    case "Risk Capital":
                        $.each(item.Values, function(e, val) {
                            riskCapitalValues.push(val.Value);
                        });
                        break;
                    case "Liquidity":
                        $.each(item.Values, function(e, val) {
                            liqudityValues.push(val.Value);
                        });
                        break;
                }
            });
            Highcharts.chart('liquiditychart', {
                title: {
                    text: 'Risk Capital vs Liquidity'
                },
                xAxis: {
                    categories: xAxisCategories
                },
                labels: {
                    items: [{
                        html: '',
                        style: {
                            left: '50px',
                            top: '18px',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                        }
                    }]
                },
                series: [{
                    type: 'column',
                    name: 'Risk Capital',
                    data: riskCapitalValues,
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Risk Capital: <b>{point.y}</b>'
                    }
                }, {
                    type: 'spline',
                    name: 'Liquidity',
                    data: liqudityValues,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    },
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Liqudity: <b>{point.y}</b>'
                    }
                }]
            });
        },
        renderRiskBudget(data) {
            var self = this;
            var isProject = 0;
            var isSektor = 0;
            var isRisk = 0;
            var html = '<div class="row">'
                
                html += '<div class="ml10"><button type="button" btn-riskbudgetprojectrisk-reload>Refresh</button></div>'
                html += '<div class="col-md-4">'
                html += '<div class="panel panel-default">'
                html += '<div class="panel-body" id="riskbudgetprojectrisk"></div>'
                html += '</div>'
                html += '</div>'
                html += '<div class="col-md-4">'
                html += '<div class="panel panel-default">'
                html += '<div class="panel-body" id="riskbudgetprojectsector"></div>'
                html += '</div>'
                html += '</div>'
                html += '<div class="col-md-4">'
                html += '<div class="panel panel-default">'
                html += '<div class="panel-body" id="riskbudgetriskcategory"></div>'
                html += '</div>'
                html += '</div>'
                html += '</div>'

                //risk
                html += '<div class="col-md-12">'
                html += '<div class="margintop-5 text-bold-italic mb-10"><button type="button" class="btn btn-info" change-risk>Ganti Risk</button></div>'
                html += '<div class="marginbottom-20 scrollable-matrix">'
                html += '<div class="table-mat">'
                html += '<div class="heading-mat">'
                html += '<div class="cell-mat header-column"><p>Name</p></div>'
                $.each(data.RiskBudgetByRisk.YearCollection, function(index, va){
                    html += '<div class="cell-mat header-column">'
                    html += '<p class="header-column-position">'+ va.Year +'</p>'
                    html += '</div>'
                });
                html += '</div>'

                $.each(data.RiskBudgetByRisk.RiskBudgetCollectionByRisk, function(i, value) {
                    html += '<div class="row-mat">'
                    isRisk += 1;
                    if (isRisk == 1) {
                        html += '<div class="cell-mat"  data-budget-name="'+ value.Name +'"><p data-risk-title>'+ value.Name +'</p></div>'
                    } else {
                        html += '<div class="cell-mat" data-budget-name="'+ value.Name +'"><p>'+ value.Name +'</p></div>'
                    }
                    
                    $.each(data.RiskBudgetByRisk.YearCollection, function(index, val) {
                        html += '<div class="cell-mat text-center" data-cell-risk="'+ val.Year +'-'+ value.Name +'">'
                        $.grep(value.Values, function(ind, lue) {
                            if (ind.Year == val.Year) {
                                if (isRisk == 1) {
                                    html += '<div class="text-right"><p data-risk-per-year="sektor-'+ val.Year +'">'+ numeral(ind.Value).format('0,0.00') +' %'+'</p></div>'
                                } else {
                                    html += '<div class="text-right"><p data-buget-per-year="'+ value.Name +'-risk-'+ val.Year +'">'+ numeral(ind.Value).format('0,0.00') +' %'+'</p></div>'
                                }
                            }
                        });
                        html += '</div>'
                    });
                    html += '</div>'
                });
                
                html += '</div>'
                html += '</div>'
                html += '</div>'
                //end of risk

                //sektor
                html += '<div class="col-md-12">'
                html += '<div class="margintop-5 text-bold-italic mb-10"><button type="button" class="btn btn-info" change-sektor>Ganti Sektor</button></div>'
                html += '<div class="marginbottom-20 scrollable-matrix">'
                html += '<div class="table-mat">'
                html += '<div class="heading-mat">'
                html += '<div class="cell-mat header-column"><p>Name</p></div>'
                $.each(data.RiskBudgetBySektor.YearCollection, function(index, va){
                    html += '<div class="cell-mat header-column">'
                    html += '<p class="header-column-position">'+ va.Year +'</p>'
                    html += '</div>'
                });
                html += '</div>'

                $.each(data.RiskBudgetBySektor.RiskBudgetCollectionBySektor, function(i, value) {
                    html += '<div class="row-mat">'
                    isSektor += 1;
                    if (isSektor == 1) {
                        html += '<div class="cell-mat"  data-budget-name="'+ value.Name +'"><p data-sektor-title>'+ value.Name +'</p></div>'
                    } else {
                        html += '<div class="cell-mat" data-budget-name="'+ value.Name +'"><p>'+ value.Name +'</p></div>'
                    }
                    
                    $.each(data.RiskBudgetBySektor.YearCollection, function(index, val) {
                        html += '<div class="cell-mat text-center" data-cell-risk="'+ val.Year +'-'+ value.Name +'">'
                        $.grep(value.Values, function(ind, lue) {
                            if (ind.Year == val.Year) {
                                if (isSektor == 1) {
                                    html += '<div class="text-right"><p data-sektor-per-year="sektor-'+ val.Year +'">'+ numeral(ind.Value).format('0,0.00') +' %'+'</p></div>'
                                } else {
                                    html += '<div class="text-right"><p data-buget-per-year="'+ value.Name +'-sektor-'+ val.Year +'">'+ numeral(ind.Value).format('0,0.00') +' %'+'</p></div>'
                                }
                            }
                        });
                        html += '</div>'
                    });
                    html += '</div>'
                });
                
                html += '</div>'
                html += '</div>'
                html += '</div>'
                html += '</div>'
                //end of sektor

                //project
                html += '<div class="col-md-12">'
                html += '<div class="margintop-5 text-bold-italic mb-10"><button type="button" class="btn btn-info" change-project>Ganti Proyek</button></div>'
                html += '<div class="marginbottom-20 scrollable-matrix">'
                html += '<div class="table-mat">'
                html += '<div class="heading-mat">'
                html += '<div class="cell-mat header-column"><p tester-aja>Name</p></div>'
                $.each(data.RiskBudgetByProject.RiskBudgetYearCollection, function(index, va){
                    html += '<div class="cell-mat header-column">'
                    html += '<p class="header-column-position">'+ va.Year +'</p>'
                    html += '</div>'
                });
                html += '</div>'

                $.each(data.RiskBudgetByProject.RiskBudgetCollection, function(i, value) {
                    html += '<div class="row-mat">'
                    isProject += 1;
                    if (isProject == 1) {
                        html += '<div class="cell-mat"  data-budget-name="'+ value.Name +'"><p data-project-title>'+ value.Name +'</p></div>'
                    } else {
                        html += '<div class="cell-mat" data-budget-name="'+ value.Name +'"><p>'+ value.Name +'</p></div>'
                    }
                    
                    $.each(data.RiskBudgetByProject.RiskBudgetYearCollection, function(index, val) {
                        html += '<div class="cell-mat text-center" data-cell-risk="'+ val.Year +'-'+ value.Name +'">'
                        $.grep(value.Values, function(ind, lue) {
                            if (ind.Year == val.Year) {
                                if (isProject == 1) {
                                    html += '<div class="text-right"><p data-project-per-year="project-'+ val.Year +'">'+ numeral(ind.Value).format('0,0.00') +' %'+'</p></div>'
                                } else {
                                    html += '<div class="text-right"><p data-buget-per-year="'+ value.Name +'-'+ val.Year +'">'+ numeral(ind.Value).format('0,0.00') +' %'+'</p></div>'
                                }
                            }
                        });
                        html += '</div>'
                    });
                    html += '</div>'
                });
                
                html += '</div>'
                html += '</div>'
                html += '</div>'
                html += '</div>'
                //end of project

                

                

            self.$('[risk-budget-container]').append(html);

            self.renderRiskBudgetRiskCategoryChart(data.RiskBudgetByProject.RiskBudgetYearCollection, data.RiskBudgetByProject.RiskBudgetCollection);
            self.renderRiskBudgetProjectSectorChart(data.RiskBudgetBySektor.YearCollection, data.RiskBudgetBySektor.RiskBudgetCollectionBySektor);
            self.renderRiskBudgetProjectRiskChart(data.RiskBudgetByRisk.YearCollection, data.RiskBudgetByRisk.RiskBudgetCollectionByRisk);

            self.riskBudgetRiskCategoryChartYear = data.RiskBudgetByProject.RiskBudgetYearCollection;
            self.riskBudgetRiskCategoryChartData = data.RiskBudgetByProject.RiskBudgetCollection;
            self.riskBudgetProjectSectorChartYear = data.RiskBudgetBySektor.YearCollection;
            self.riskBudgetProjectSectorChartData = data.RiskBudgetBySektor.RiskBudgetCollectionBySektor;
            self.riskBudgetProjectRiskChartYear = data.RiskBudgetByRisk.YearCollection;
            self.riskBudgetProjectRiskChartData = data.RiskBudgetByRisk.RiskBudgetCollectionByRisk;
        },
        renderProjectChanged: function(projectId) {
            this.projectId = projectId;
            commonFunction.showLoadingSpinner();
            var self = this;
            this.projectIdSelected = projectId;
            this.modelRiskBudget.fetch({
                reset: true,
                data: {
                    id: 4,
                    projectId: projectId,
                    riskRegistrasiId: 0,
                    sektorId: 0
                },
                success: function(res) {
                    self.setValueProjectChanged(res);
                },
                error: function() {
                    console.log('error');
                }
            });
        },
        chooseProject: function() {
            var self = this;
            require(['./project/view'], function(View) {
                commonFunction.setDefaultModalDialogFunction(self, View, self.projects, self.projectIdSelected);
            });
        },
        setValueProjectChanged: function(data) {
            var self = this;
            //change Proyek
            this.$('[data-project-title]').text(data.attributes.RiskBudget.RiskBudgetByProject.RiskBudgetCollection[0].Name);
            $.each(data.attributes.RiskBudget.RiskBudgetByProject.RiskBudgetCollection[0].Values, function(index, item) {
                self.$('[data-project-per-year="project-'+ item.Year +'"]').text(item.Value +' %');
            });
            //change min
            $.each(data.attributes.RiskBudget.RiskBudgetByProject.RiskBudgetCollection[1].Values, function(index, item) {
                self.$('[data-buget-per-year="Minimum Risk Percentage-'+ item.Year +'"]').text(item.Value +' %');
            });
            //change max
            $.each(data.attributes.RiskBudget.RiskBudgetByProject.RiskBudgetCollection[2].Values, function(index, item) {
                self.$('[data-buget-per-year="Maximum Risk Percentage-'+ item.Year +'"]').text(item.Value +' %');
            });
            this.renderRiskBudgetRiskCategoryChart(data.attributes.RiskBudget.RiskBudgetByProject.RiskBudgetYearCollection, data.attributes.RiskBudget.RiskBudgetByProject.RiskBudgetCollection);
            commonFunction.closeLoadingSpinner();
        },
        chooseSektor: function() {
            var self = this;
            require(['./sektor/view'], function(View) {
                commonFunction.setDefaultModalDialogFunction(self, View, self.sektors, self.sektorIdSelected);
            });
        },
        setValueSektorChanged: function(data) {
            var self = this;
            //change Sektor
            this.$('[data-sektor-title]').text(data.attributes.RiskBudget.RiskBudgetBySektor.RiskBudgetCollectionBySektor[0].Name);
            $.each(data.attributes.RiskBudget.RiskBudgetBySektor.RiskBudgetCollectionBySektor[0].Values, function(index, item) {
                self.$('[data-sektor-per-year="sektor-'+ item.Year +'"]').text(item.Value +' %');
            });
            //change min
            $.each(data.attributes.RiskBudget.RiskBudgetBySektor.RiskBudgetCollectionBySektor[1].Values, function(index, item) {
                self.$('[data-buget-per-year="Minimum Risk Percentage-sektor-'+ item.Year +'"]').text(item.Value +' %');
            });
            //change max
            $.each(data.attributes.RiskBudget.RiskBudgetBySektor.RiskBudgetCollectionBySektor[2].Values, function(index, item) {
                self.$('[data-buget-per-year="Maximum Risk Percentage-sektor-'+ item.Year +'"]').text(item.Value +' %');
            });
            this.renderRiskBudgetProjectSectorChart(data.attributes.RiskBudget.RiskBudgetBySektor.YearCollection, data.attributes.RiskBudget.RiskBudgetBySektor.RiskBudgetCollectionBySektor);
            commonFunction.closeLoadingSpinner();
        },
        renderSektorChanged: function(sektorId) {
            this.sectorId = sektorId;
            commonFunction.showLoadingSpinner();
            var self = this;
            this.sektorIdSelected = sektorId;
            this.modelRiskBudget.fetch({
                reset: true,
                data: {
                    id: 4,
                    projectId: 0,
                    riskRegistrasiId: 0,
                    sektorId: sektorId
                },
                success: function(res) {
                    self.setValueSektorChanged(res);
                },
                error: function() {
                    console.log('error');
                }
            });
        },
        renderRiskBudgetRiskCategoryChart: function(year, data) {
            var xAxisCategories = [];
            var projectValues = [];
            var minimumValues = [];
            var maximumValues = [];
            var columnName = data[0].Name;
            
            $.each(year, function(i, item) {
                xAxisCategories.push(item.Year);
            });
            $.each(data[0].Values, function(i, item) {
                projectValues.push(item.Value);
            });
            $.each(data, function(i, item) {
                switch(item.Name) {
                    case "Minimum Risk Percentage":
                        $.each(item.Values, function(e, val) {
                            minimumValues.push(val.Value);
                        });
                        break;
                    case "Maximum Risk Percentage":
                        $.each(item.Values, function(e, val) {
                            maximumValues.push(val.Value);
                        });
                        break;
                }
            });
            Highcharts.chart('riskbudgetriskcategory', {
                title: {
                    text: 'Risk Budget: Project'
                },
                xAxis: {
                    categories: xAxisCategories
                },
                labels: {
                    items: [{
                        html: '',
                        style: {
                            left: '50px',
                            top: '18px',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                        }
                    }]
                },
                series: [{
                    type: 'column',
                    name: columnName,
                    data: projectValues,
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Proyek: <b>{point.y}</b>'
                    }
                }, {
                    type: 'spline',
                    name: 'Minimum Risk Percentage',
                    data: minimumValues,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    },
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Minimum Risk Percentage: <b>{point.y}</b>'
                    }
                }, {
                    type: 'spline',
                    name: 'Maximum Risk Percentage',
                    data: maximumValues,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    },
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Maximum Risk Percentage: <b>{point.y}</b>'
                    }
                }]
            });
        },
        renderRiskBudgetProjectSectorChart: function(year, data) {
            var xAxisCategories = [];
            var sektorValues = [];
            var minimumValues = [];
            var maximumValues = [];
            var columnName = data[0].Name;
            
            $.each(year, function(i, item) {
                xAxisCategories.push(item.Year);
            });
            $.each(data[0].Values, function(i, item) {
                sektorValues.push(item.Value);
            });
            $.each(data, function(i, item) {
                switch(item.Name) {
                    case "Minimum Risk Percentage":
                        $.each(item.Values, function(e, val) {
                            minimumValues.push(val.Value);
                        });
                        break;
                    case "Maximum Risk Percentage":
                        $.each(item.Values, function(e, val) {
                            maximumValues.push(val.Value);
                        });
                        break;
                }
            });
            Highcharts.chart('riskbudgetprojectsector', {
                title: {
                    text: 'Risk Budget: Sektor'
                },
                xAxis: {
                    categories: xAxisCategories
                },
                labels: {
                    items: [{
                        html: '',
                        style: {
                            left: '50px',
                            top: '18px',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                        }
                    }]
                },
                series: [{
                    type: 'column',
                    name: columnName,
                    data: sektorValues,
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Sektor: <b>{point.y}</b>'
                    }
                }, {
                    type: 'spline',
                    name: 'Minimum Risk Percentage',
                    data: minimumValues,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    },
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Minimum Risk Percentage: <b>{point.y}</b>'
                    }
                }, {
                    type: 'spline',
                    name: 'Maximum Risk Percentage',
                    data: maximumValues,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    },
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Maximum Risk Percentage: <b>{point.y}</b>'
                    }
                }]
            });
        },
        renderRiskBudgetProjectRiskChart: function(year, data) {
            var xAxisCategories = [];
            var riskValues = [];
            var minimumValues = [];
            var maximumValues = [];
            var columnName = data[0].Name;
            
            $.each(year, function(i, item) {
                xAxisCategories.push(item.Year);
            });
            $.each(data[0].Values, function(i, item) {
                riskValues.push(item.Value);
            });
            $.each(data, function(i, item) {
                switch(item.Name) {
                    case "Minimum Risk Percentage":
                        $.each(item.Values, function(e, val) {
                            minimumValues.push(val.Value);
                        });
                        break;
                    case "Maximum Risk Percentage":
                        $.each(item.Values, function(e, val) {
                            maximumValues.push(val.Value);
                        });
                        break;
                }
            });
            Highcharts.chart('riskbudgetprojectrisk', {
                title: {
                    text: 'Risk Budget: Risk'
                },
                xAxis: {
                    categories: xAxisCategories
                },
                labels: {
                    items: [{
                        html: '',
                        style: {
                            left: '50px',
                            top: '18px',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                        }
                    }]
                },
                series: [{
                    type: 'column',
                    name: columnName,
                    data: riskValues,
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Risk: <b>{point.y}</b>'
                    }
                }, {
                    type: 'spline',
                    name: 'Minimum Risk Percentage',
                    data: minimumValues,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    },
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Minimum Risk Percentage: <b>{point.y}</b>'
                    }
                }, {
                    type: 'spline',
                    name: 'Maximum Risk Percentage',
                    data: maximumValues,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    },
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Maximum Risk Percentage: <b>{point.y}</b>'
                    }
                }],
                exporting: {
                    buttons: {
                        customButton: {
                            x: -62,
                            onclick: function () {
                                alert('Clicked');
                            },
                            symbol: 'circle'
                        }
                    }
                }
            });
        },
        reloadAllRiskBudgetChart: function() {
            var yearRiskBudgetRiskCategoryChart = this.riskBudgetRiskCategoryChartYear;
            var dataRiskBudgetRiskCategoryChart = this.riskBudgetRiskCategoryChartData;

            var yearRiskBudgetProjectSectorChart = this.riskBudgetProjectSectorChartYear;
            var dataRiskBudgetProjectSectorChart = this.riskBudgetProjectSectorChartData;

            var yearRiskBudgetProjectRiskChart = this.riskBudgetProjectRiskChartYear;
            var dataRiskBudgetProjectRiskChart = this.riskBudgetProjectRiskChartData;

            this.reloadRiskBudgetRiskCategoryChart(yearRiskBudgetRiskCategoryChart, dataRiskBudgetRiskCategoryChart);
            this.reloadRiskBudgetProjectSectorChart(yearRiskBudgetProjectSectorChart, dataRiskBudgetProjectSectorChart);
            this.reloadRiskBudgetProjectRiskChart(yearRiskBudgetProjectRiskChart, dataRiskBudgetProjectRiskChart);
        },
        reloadRiskBudgetRiskCategoryChart: function(year, data) {
            var xAxisCategories = [];
            var projectValues = [];
            var minimumValues = [];
            var maximumValues = [];
            var columnName = data[0].Name;
            
            $.each(year, function(i, item) {
                xAxisCategories.push(item.Year);
            });
            $.each(data[0].Values, function(i, item) {
                projectValues.push(item.Value);
            });
            $.each(data, function(i, item) {
                switch(item.Name) {
                    case "Minimum Risk Percentage":
                        $.each(item.Values, function(e, val) {
                            minimumValues.push(val.Value);
                        });
                        break;
                    case "Maximum Risk Percentage":
                        $.each(item.Values, function(e, val) {
                            maximumValues.push(val.Value);
                        });
                        break;
                }
            });
            Highcharts.chart('riskbudgetriskcategory', {
                title: {
                    text: 'Risk Budget: Project'
                },
                xAxis: {
                    categories: xAxisCategories
                },
                labels: {
                    items: [{
                        html: '',
                        style: {
                            left: '50px',
                            top: '18px',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                        }
                    }]
                },
                series: [{
                    type: 'column',
                    name: columnName,
                    data: projectValues,
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Proyek: <b>{point.y}</b>'
                    }
                }, {
                    type: 'spline',
                    name: 'Minimum Risk Percentage',
                    data: minimumValues,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    },
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Minimum Risk Percentage: <b>{point.y}</b>'
                    }
                }, {
                    type: 'spline',
                    name: 'Maximum Risk Percentage',
                    data: maximumValues,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    },
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Maximum Risk Percentage: <b>{point.y}</b>'
                    }
                }]
            });
        },
        reloadRiskBudgetProjectSectorChart: function(year, data) {
            var xAxisCategories = [];
            var sektorValues = [];
            var minimumValues = [];
            var maximumValues = [];
            var columnName = data[0].Name;
            
            $.each(year, function(i, item) {
                xAxisCategories.push(item.Year);
            });
            $.each(data[0].Values, function(i, item) {
                sektorValues.push(item.Value);
            });
            $.each(data, function(i, item) {
                switch(item.Name) {
                    case "Minimum Risk Percentage":
                        $.each(item.Values, function(e, val) {
                            minimumValues.push(val.Value);
                        });
                        break;
                    case "Maximum Risk Percentage":
                        $.each(item.Values, function(e, val) {
                            maximumValues.push(val.Value);
                        });
                        break;
                }
            });
            Highcharts.chart('riskbudgetprojectsector', {
                title: {
                    text: 'Risk Budget: Sektor'
                },
                xAxis: {
                    categories: xAxisCategories
                },
                labels: {
                    items: [{
                        html: '',
                        style: {
                            left: '50px',
                            top: '18px',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                        }
                    }]
                },
                series: [{
                    type: 'column',
                    name: columnName,
                    data: sektorValues,
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Sektor: <b>{point.y}</b>'
                    }
                }, {
                    type: 'spline',
                    name: 'Minimum Risk Percentage',
                    data: minimumValues,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    },
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Minumum Risk Percentage: <b>{point.y}</b>'
                    }
                }, {
                    type: 'spline',
                    name: 'Maximum Risk Percentage',
                    data: maximumValues,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    },
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Maximum Risk Percentage: <b>{point.y}</b>'
                    }
                }]
            });
        },
        reloadRiskBudgetProjectRiskChart: function(year, data) {
            var xAxisCategories = [];
            var riskValues = [];
            var minimumValues = [];
            var maximumValues = [];
            var columnName = data[0].Name;
            
            $.each(year, function(i, item) {
                xAxisCategories.push(item.Year);
            });
            $.each(data[0].Values, function(i, item) {
                riskValues.push(item.Value);
            });
            $.each(data, function(i, item) {
                switch(item.Name) {
                    case "Minimum Risk Percentage":
                        $.each(item.Values, function(e, val) {
                            minimumValues.push(val.Value);
                        });
                        break;
                    case "Maximum Risk Percentage":
                        $.each(item.Values, function(e, val) {
                            maximumValues.push(val.Value);
                        });
                        break;
                }
            });
            Highcharts.chart('riskbudgetprojectrisk', {
                title: {
                    text: 'Risk Budget: Risk'
                },
                xAxis: {
                    categories: xAxisCategories
                },
                labels: {
                    items: [{
                        html: '',
                        style: {
                            left: '50px',
                            top: '18px',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                        }
                    }]
                },
                series: [{
                    type: 'column',
                    name: columnName,
                    data: riskValues,
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Risk: <b>{point.y}</b>'
                    }
                }, {
                    type: 'spline',
                    name: 'Minimum Risk Percentage',
                    data: minimumValues,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    },
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Minimum Risk Percentage: <b>{point.y}</b>'
                    }
                }, {
                    type: 'spline',
                    name: 'Maximum Risk Percentage',
                    data: maximumValues,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    },
                    tooltip: {
                        headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                        valueDecimals: 2,
                        pointFormat: 'Maximum Risk Percentage: <b>{point.y}</b>'
                    }
                }],
                exporting: {
                    buttons: {
                        customButton: {
                            x: -62,
                            onclick: function () {
                                alert('Clicked');
                            },
                            symbol: 'circle'
                        }
                    }
                }
            });
        },
        chooseRisk: function() {
            var self = this;
            require(['./risk/view'], function(View) {
                commonFunction.setDefaultModalDialogFunction(self, View, self.risks, self.riskIdSelected);
            });
        },
        renderRiskChanged: function(riskId) {
            this.riskId = riskId;
            commonFunction.showLoadingSpinner();
            var self = this;
            this.riskIdSelected = riskId;
            this.modelRiskBudget.fetch({
                reset: true,
                data: {
                    id: 4,
                    projectId: 0,
                    riskRegistrasiId: riskId,
                    sektorId: 0
                },
                success: function(res) {
                    self.setValueRiskChanged(res);
                },
                error: function() {
                    console.log('error');
                }
            });
        },
        setValueRiskChanged: function(data) {
            var self = this;
            //change Risk
            this.$('[data-risk-title]').text(data.attributes.RiskBudget.RiskBudgetByRisk.RiskBudgetCollectionByRisk[0].Name);
            $.each(data.attributes.RiskBudget.RiskBudgetByRisk.RiskBudgetCollectionByRisk[0].Values, function(index, item) {
                self.$('[data-risk-per-year="sektor-'+ item.Year +'"]').text(numeral(item.Value).format('0,0.00') +' %');
            });
            //change min
            $.each(data.attributes.RiskBudget.RiskBudgetByRisk.RiskBudgetCollectionByRisk[1].Values, function(index, item) {
                self.$('[data-buget-per-year="Minimum Risk Percentage-risk-'+ item.Year +'"]').text(item.Value +' %');
            });
            //change max
            $.each(data.attributes.RiskBudget.RiskBudgetByRisk.RiskBudgetCollectionByRisk[2].Values, function(index, item) {
                self.$('[data-buget-per-year="Maximum Risk Percentage-risk-'+ item.Year +'"]').text(item.Value +' %');
            });
            this.renderRiskBudgetProjectRiskChart(data.attributes.RiskBudget.RiskBudgetByRisk.YearCollection, data.attributes.RiskBudget.RiskBudgetByRisk.RiskBudgetCollectionByRisk);
            commonFunction.closeLoadingSpinner();
        },
        renderSensitivity: function(data) {
            var self = this;
            var html = '<div class="row">'

                html += '<div class="col-md-12">'
                html += '<div class="ml-10"><button type="button" btn-sensitivitychart-reload>Refresh</button></div>'
                $.each(data.SensitivityStressScenarioCollection, function(index, value) {
                    html += '<div class="col-md-4">'
                    html += '<div class="panel panel-default">'
                    html += '<div class="panel-body" id="sensitivitychartskenario-'+ value.NamaScenario +'"></div>'
                    html += '</div>'
                    html += '</div>'
                });
                html += '</div>'
                html += '</div>'
                
                html += '<div class="col-md-12">'
                html += '<div class="margintop-5 text-bold-italic">Risk Capital</div>'
                html += '<div class="marginbottom-20 scrollable-matrix">'
                html += '<div class="table-mat">'
                html += '<div class="heading-mat">'
                html += '<div class="cell-mat header-column"><p>Name Skenario</p></div>'
                $.each(data.SensitivityStressYearCollection, function(index, va){
                    html += '<div class="cell-mat header-column">'
                    html += '<p class="header-column-position">'+ va.Year +'</p>'
                    html += '</div>'
                });
                html += '</div>'

                $.each(data.SensitivityStressScenarioCollection, function(i, value) {
                    html += '<div class="row-mat">'
                    html += '<div class="cell-mat"><p>'+ value.NamaScenario +'</p></div>'
                    
                    $.each(value.Values, function(index, val) {
                        html += '<div class="cell-mat text-center" data-cell-risk="'+ val.Year +'">'
                        html += '<div class="text-right"><p currency-formating>'+ numeral(val.Value).format('0,0.00') +'</p></div>'
                        html += '</div>'
                    });
                    html += '</div>'
                });
                
                html += '</div>'
                html += '</div>'
                //end of sensitivity table

                //available capital
                html += '<div class="col-md-12" style="padding-left:0">'
                html += '<div class="margintop-5 text-bold-italic"></div>'
                html += '<div class="marginbottom-20 scrollable-matrix">'
                html += '<div class="table-mat">'
                html += '<div class="heading-mat">'
                html += '<div class="cell-mat header-column"><p></p></div>'
                $.each(data.SensitivityStressYearCollection, function(index, va){
                    html += '<div class="cell-mat header-column">'
                    html += '<p class="header-column-position">'+ va.Year +'</p>'
                    html += '</div>'
                });
                html += '</div>'

                $.each(data.SensitivityStressCollection, function(i, value) {
                    html += '<div class="row-mat">'
                    html += '<div class="cell-mat"><p>'+ value.Name +'</p></div>'
                    
                    $.each(value.Values, function(index, val) {
                        html += '<div class="cell-mat text-center" data-cell-risk="'+ val.Year +'">'
                        html += '<div class="text-right"><p currency-formating>'+ numeral(val.Value).format('0,0.00') +'</p></div>'
                        html += '</div>'
                    });
                    html += '</div>'
                });
                
                html += '</div>'
                html += '</div>'
                //end of available capital

                html += '</div>'

            self.$('[sensitivity-container]').append(html);
            this.sensitivityLiquidityChart = data.SensitivityStressCollection;
            this.sensitivityScenarioChart = data.SensitivityStressScenarioCollection;
            this.sensitivityYearChart = data.SensitivityStressYearCollection;
            this.renderSensitivityChart(data.SensitivityStressCollection, data.SensitivityStressScenarioCollection, data.SensitivityStressYearCollection);
        },
        renderSensitivityChart: function(liquidity, scenario, year) {
            var self = this;
            var xAxisCategories = [];
            var liqudityValues = [];
            $.each(year, function(i, item) {
                xAxisCategories.push(item.Year);
            });
            $.each(liquidity, function(i, item) {
                switch(item.Name) {
                    case "Liquidity":
                        $.each(item.Values, function(e, val) {
                            liqudityValues.push(val.Value);
                        });
                        break;
                }
            });
            $.each(scenario, function(i, item) {
                var dom = "sensitivitychartskenario-"+ item.NamaScenario;
                var dataTitle = item.NamaScenario;
                var seriesCategories = [];
                $.each(item.Values, function(ind, lue) {
                    seriesCategories.push(lue.Value);
                });

                Highcharts.chart(dom, {
                    title: {
                        text: dataTitle
                    },
                    xAxis: {
                        categories: xAxisCategories
                    },
                    labels: {
                        items: [{
                            html: '',
                            style: {
                                left: '50px',
                                top: '18px',
                                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                            }
                        }]
                    },
                    series: [{
                        type: 'column',
                        name: 'Risk Capital',
                        data: seriesCategories,
                        tooltip: {
                            headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                            valueDecimals: 2,
                            pointFormat: 'Risk Capital: <b>{point.y}</b>'
                        }
                    }, {
                        type: 'spline',
                        name: 'Liquidity',
                        data: liqudityValues,
                        marker: {
                            lineWidth: 2,
                            lineColor: Highcharts.getOptions().colors[3],
                            fillColor: 'white'
                        },
                        tooltip: {
                            headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                            valueDecimals: 2,
                            pointFormat: 'Liqudity: <b>{point.y}</b>'
                        }
                    }]
                });
            });
        },
        reloadSensitivityChart: function() {
            var self = this;
            var year = this.sensitivityYearChart;
            var liquidity = this.sensitivityLiquidityChart;
            var scenario = this.sensitivityScenarioChart;
            var xAxisCategories = [];
            var liqudityValues = [];
            $.each(year, function(i, item) {
                xAxisCategories.push(item.Year);
            });
            $.each(liquidity, function(i, item) {
                switch(item.Name) {
                    case "Liquidity":
                        $.each(item.Values, function(e, val) {
                            liqudityValues.push(val.Value);
                        });
                        break;
                }
            });
            $.each(scenario, function(i, item) {
                var dom = "sensitivitychartskenario-"+ item.NamaScenario;
                var dataTitle = item.NamaScenario;
                var seriesCategories = [];
                $.each(item.Values, function(ind, lue) {
                    seriesCategories.push(lue.Value);
                });

                Highcharts.chart(dom, {
                    title: {
                        text: dataTitle
                    },
                    xAxis: {
                        categories: xAxisCategories
                    },
                    labels: {
                        items: [{
                            html: '',
                            style: {
                                left: '50px',
                                top: '18px',
                                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                            }
                        }]
                    },
                    series: [{
                        type: 'column',
                        name: 'Risk Capital',
                        data: seriesCategories,
                        tooltip: {
                            headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                            valueDecimals: 2,
                            pointFormat: 'Risk Capital: <b>{point.y}</b>'
                        }
                    }, {
                        type: 'spline',
                        name: 'Liquidity',
                        data: liqudityValues,
                        marker: {
                            lineWidth: 2,
                            lineColor: Highcharts.getOptions().colors[3],
                            fillColor: 'white'
                        },
                        tooltip: {
                            headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                            valueDecimals: 2,
                            pointFormat: 'Liquidity: <b>{point.y}</b>'
                        }
                    }]
                });
            });
        },
        renderStressTesting: function(data) {
            var self = this;
            var html = '<div class="row">'

                html += '<div class="col-md-12">'
                html += '<div class="ml10"><button type="button" btn-stresstestingchart-reload>Refresh</button></div>'
                $.each(data.RiskCapitalCollection, function(index, value) {
                    html += '<div class="col-md-4">'
                    html += '<div class="panel panel-default">'
                    html += '<div class="panel-body" id="stresstestingchartskenario-'+ value.ScenarioId +'"></div>'
                    html += '</div>'
                    html += '</div>'
                });
                html += '</div>'
                html += '</div>'
                
                html += '<div class="col-md-12">'
                html += '<div class="margintop-5 text-bold-italic">Risk Capital</div>'
                html += '<div class="marginbottom-20 scrollable-matrix">'
                html += '<div class="table-mat">'
                html += '<div class="heading-mat">'
                html += '<div class="cell-mat header-column"><p>Name Skenario</p></div>'
                $.each(data.SensitivityStressYearCollection, function(index, va){
                    html += '<div class="cell-mat header-column">'
                    html += '<p class="header-column-position">'+ va.Year +'</p>'
                    html += '</div>'
                });
                html += '</div>'

                $.each(data.RiskCapitalCollection, function(i, value) {
                    html += '<div class="row-mat">'
                    html += '<div class="cell-mat"><p>'+ value.NamaScenario +'</p></div>'
                    
                    $.each(value.Values, function(index, val) {
                        html += '<div class="cell-mat text-center" data-cell-risk="'+ val.Year +'">'
                        html += '<div class="text-right"><p currency-formating>'+ numeral(val.Value).format('0,0.00') +'</p></div>'
                        html += '</div>'
                    });
                    html += '</div>'
                });
                
                html += '</div>'
                html += '</div>'
                html += '</div>'
                //end of risk capital table

                //available capital
                html += '<div class="col-md-12">'
                html += '<div class="margintop-5 text-bold-italic">Available Capital</div>'
                html += '<div class="marginbottom-20 scrollable-matrix">'
                html += '<div class="table-mat">'
                html += '<div class="heading-mat">'
                html += '<div class="cell-mat header-column"><p></p></div>'
                $.each(data.SensitivityStressYearCollection, function(index, va){
                    html += '<div class="cell-mat header-column">'
                    html += '<p class="header-column-position">'+ va.Year +'</p>'
                    html += '</div>'
                });
                html += '</div>'

                $.each(data.AvailableCapitalCollection, function(i, value) {
                    html += '<div class="row-mat">'
                    html += '<div class="cell-mat"><p>'+ value.NamaScenario +'</p></div>'
                    
                    $.each(value.Values, function(index, val) {
                        html += '<div class="cell-mat text-center" data-cell-risk="'+ val.Year +'">'
                        html += '<div class="text-right"><p currency-formating>'+ numeral(val.Value).format('0,0.00') +'</p></div>'
                        html += '</div>'
                    });
                    html += '</div>'
                });
                
                html += '</div>'
                html += '</div>'
                html += '</div>'
                //end of available capital

                //liquidity
                html += '<div class="row">'
                html += '<div class="col-md-12 ml-5">'
                html += '<div class="margintop-5 text-bold-italic">Liquidity</div>'
                html += '<div class="marginbottom-20 scrollable-matrix">'
                html += '<div class="table-mat">'
                html += '<div class="heading-mat">'
                html += '<div class="cell-mat header-column"><p></p></div>'
                $.each(data.SensitivityStressYearCollection, function(index, va){
                    html += '<div class="cell-mat header-column">'
                    html += '<p class="header-column-position">'+ va.Year +'</p>'
                    html += '</div>'
                });
                html += '</div>'

                $.each(data.SensitivityStressCollection, function(i, value) {
                    html += '<div class="row-mat">'
                    html += '<div class="cell-mat"><p>'+ value.Name +'</p></div>'
                    
                    $.each(value.Values, function(index, val) {
                        html += '<div class="cell-mat text-center" data-cell-risk="'+ val.Year +'">'
                        html += '<div class="text-right"><p currency-formating>'+ numeral(val.Value).format('0,0.00') +'</p></div>'
                        html += '</div>'
                    });
                    html += '</div>'
                });
                
                html += '</div>'
                html += '</div>'
                html += '</div>'
                //end of liquidity


            self.$('[stress-testing-container]').append(html);
            this.renderStressTestingChart(data.AvailableCapitalCollection, data.RiskCapitalCollection, data.SensitivityStressCollection, data.SensitivityStressYearCollection);
            this.stressTestingAvailableCapital = data.AvailableCapitalCollection;
            this.stressTestingRiskCapitalChart = data.RiskCapitalCollection;
            this.stressTestingSensitivityChart = data.SensitivityStressCollection;
            this.stressTestingYearChart = data.SensitivityStressYearCollection;
        },
        renderStressTestingChart: function(availableCapital, riskCapital, liquidities, year) {
            var xAxisCategories = [];
            let liquidity = [];
            if (liquidities) {
                if (liquidities[0].Values) {
                    _.each(liquidities[0].Values, function(item) {
                        liquidity.push(item.Value);
                    });
                    this.stressTestingLiquidityChart = liquidity;
                }
            }
            if (year) {
                _.each(year, function(item) {
                    xAxisCategories.push(item.Year);
                });
                this.stressTestingYearChart = xAxisCategories;
            }
            if (availableCapital) {
                _.each(availableCapital, function(val) {
                    let seriesCategories = [];
                    if (riskCapital) {
                        _.each(riskCapital, function(riskVal) {
                            switch (riskVal.ScenarioId) {
                                case val.ScenarioId:
                                    if (riskVal.Values) {
                                        _.each(riskVal.Values, function(item) {
                                            seriesCategories.push(item.Value);
                                        });
                                    }
                                    break;
                            }
                        });
                    }
                    const dom = 'stresstestingchartskenario-'+ val.ScenarioId;
                    let availableCapitalValues = [];
                    if (val.Values) {
                        _.each(val.Values, function(lue) {
                            availableCapitalValues.push(lue.Value);
                        });
                    }
                    const dataTitle =  val.NamaScenario;
                    
                    Highcharts.chart(dom, {
                        title: {
                            text: dataTitle
                        },
                        xAxis: {
                            categories: xAxisCategories
                        },
                        labels: {
                            items: [{
                                html: '',
                                style: {
                                    left: '50px',
                                    top: '18px',
                                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                                }
                            }]
                        },
                        series: [{
                            type: 'column',
                            name: 'Risk Capital',
                            data: seriesCategories,
                            tooltip: {
                                headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                                valueDecimals: 2,
                                pointFormat: 'Risk Capital: <b>{point.y}</b>'
                            }
                        }, {
                            type: 'spline',
                            name: 'Liquidity',
                            data: liquidity,
                            marker: {
                                lineWidth: 2,
                                lineColor: Highcharts.getOptions().colors[3],
                                fillColor: 'white'
                            },
                            tooltip: {
                                headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                                valueDecimals: 2,
                                pointFormat: 'Liquidity: <b>{point.y}</b>'
                            }
                        }, {
                            type: 'spline',
                            name: 'Available Capital',
                            data: availableCapitalValues,
                            marker: {
                                lineWidth: 2,
                                lineColor: Highcharts.getOptions().colors[2],
                                fillColor: 'red'
                            },
                            tooltip: {
                                headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                                valueDecimals: 2,
                                pointFormat: 'Available Capital: <b>{point.y}</b>'
                            }
                        }]
                    });
                });
            }
        },
        reloadStressTestingChart: function() {
            const self = this;
            var xAxisCategories = [];
            let liquidity = [];
            if (this.stressTestingSensitivityChart) {
                if (this.stressTestingSensitivityChart[0].Values) {
                    _.each(this.stressTestingSensitivityChart[0].Values, function(item) {
                        liquidity.push(item.Value);
                    });
                }
            }
            if (this.stressTestingYearChart) {
                _.each(this.stressTestingYearChart, function(item) {
                    xAxisCategories.push(item.Year);
                });
            }
            if (this.stressTestingAvailableCapital) {
                _.each(this.stressTestingAvailableCapital, function(val) {
                    let seriesCategories = [];
                    if (self.stressTestingRiskCapitalChart) {
                        _.each(self.stressTestingRiskCapitalChart, function(riskVal) {
                            switch (riskVal.ScenarioId) {
                                case val.ScenarioId:
                                    if (riskVal.Values) {
                                        _.each(riskVal.Values, function(item) {
                                            seriesCategories.push(item.Value);
                                        });
                                    }
                                    break;
                            }
                        });
                    }
                    const dom = 'stresstestingchartskenario-'+ val.ScenarioId;
                    let availableCapitalValues = [];
                    if (val.Values) {
                        _.each(val.Values, function(lue) {
                            availableCapitalValues.push(lue.Value);
                        });
                    }
                    const dataTitle =  val.NamaScenario;
                    
                    Highcharts.chart(dom, {
                        title: {
                            text: dataTitle
                        },
                        xAxis: {
                            categories: xAxisCategories
                        },
                        labels: {
                            items: [{
                                html: '',
                                style: {
                                    left: '50px',
                                    top: '18px',
                                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                                }
                            }]
                        },
                        series: [{
                            type: 'column',
                            name: 'Risk Capital',
                            data: seriesCategories,
                            tooltip: {
                                headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                                valueDecimals: 2,
                                pointFormat: 'Risk Capital: <b>{point.y}</b>'
                            }
                        }, {
                            type: 'spline',
                            name: 'Liquidity',
                            data: liquidity,
                            marker: {
                                lineWidth: 2,
                                lineColor: Highcharts.getOptions().colors[3],
                                fillColor: 'white'
                            },
                            tooltip: {
                                headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                                valueDecimals: 2,
                                pointFormat: 'Liquidity: <b>{point.y}</b>'
                            }
                        }, {
                            type: 'spline',
                            name: 'Available Capital',
                            data: availableCapitalValues,
                            marker: {
                                lineWidth: 2,
                                lineColor: Highcharts.getOptions().colors[2],
                                fillColor: 'red'
                            },
                            tooltip: {
                                headerFormat: 'Tahun: <b>{point.x}</b><br/>',
                                valueDecimals: 2,
                                pointFormat: 'Available Capital: <b>{point.y}</b>'
                            }
                        }]
                    });
                });
            }
        },
        openAdjustAvailable: function() {
            let self = this;
            let data = [];
            if (this.availableCapitalYear.length != this.availableCapitalValue.length) {
                commonFunction.responseWarningCannotExecute("Jumlah data list tahun dan nilai Available Capital tidak sama.");
            } else {
                for (let i = 0; i < this.availableCapitalYear.length; i++) {
                    const year = this.availableCapitalYear[i];
                    const capitalValue = this.availableCapitalValue[i];
                    let dataDetail = {};
                    dataDetail.Year = year;
                    dataDetail.Value = capitalValue;
                    data.push(dataDetail);
                }
                require(['./adjust_available_capital/view'], function(View) {
                    commonFunction.setDefaultModalDialogFunction(self, View, data);
                });
            }
        },
        setRoleAccess: function() {
            let self = this;
            if (this.roles) {
                $.grep(this.roles, function(item) {
                    if (item.Name.trim() == "Dashboards") {
                        if (item.SubMenu) {
                            $.grep(item.SubMenu, function(val) {
                                if (val.Name.trim() == "Dashboard-DetailResult") {
                                    if (val.SubMenu) {
                                        _.each(val.SubMenu, function(item) {
                                            switch (item.Name) {
                                                case "Dashboard-Detail StressTesting":
                                                    // self.$('#stresstesting').addClass('active');
                                                    self.$('[dashboard-stresstesting]').removeClass('hide');
                                                    break;
                                                case "Dashboard-Detail AvailableVsRisk":
                                                    // self.$('#availablevsrisk').addClass('active');
                                                    self.$('[dashboard-availablevsrisk]').removeClass('hide');
                                                    break;

                                                case "Dashboard-Detail DiversifiedBenefit":
                                                    // self.$('#diversifiedbenefitachieved').addClass('active');
                                                    self.$('[dashboard-diversifiedbenefitachieved]').removeClass('hide');
                                                    break;
                                                case "Dashboard-Detail LaverageRatios":
                                                    // self.$('#leverageratio').addClass('active');
                                                    self.$('[dashboard-leverageratio]').removeClass('hide');
                                                    break;
                                                case "Dashboard-Detail LiquidityResult":
                                                    // self.$('#liquidity').addClass('active');
                                                    self.$('[dashboard-liquidity]').removeClass('hide');
                                                    break;
                                                case "Dashboard-Detail Sensitivity":
                                                    // self.$('#sensitivity').addClass('active');
                                                    self.$('[dashboard-sensitivity]').removeClass('hide');
                                                    break;
                                                case "Dashboard-Detail RiskBudget":
                                                    // self.$('#riskbudget').addClass('active');
                                                    self.$('[dashboard-riskbudget]').removeClass('hide');
                                                    break;
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
            }
        },
        downloadXLSFile: function() {
            const host = commonConfig.requestServer;
            const uri = host + '/api/Report?id=4&projectId='+ this.projectId +'&riskRegistrasiId='+ this.riskId +'&sektorId='+ this.sectorId +'';
            const win = window.open(uri, '_blank');
            win.focus();
        }
    });
});