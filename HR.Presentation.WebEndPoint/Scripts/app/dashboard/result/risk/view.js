define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var commonFunction = require('commonfunction');
  var commonConfig = require('commonconfig');
  var Model = require('./../model');
  var eventAggregator = require('eventaggregator');
  require('select2');

  module.exports = View.extend({
    template: _.template(template),
    initialize: function (options) {
      var self = this;
      this.riskIdSelected = options.view;
      this.modelRisk = options.model;
      this.model = new Model();
      this.listenTo(this.model, 'request', function () {});
      this.listenTo(this.model, 'sync error', function () {});
    },
    afterRender: function () {
      var self = this;
      var html = '<select class="form-control select" name="KodeMRisk">';
      // html += '<option value="0">-</option>';
      $.each(this.modelRisk, function(index, item) {
        html += '<option value="'+ item.RiskRegistrasiId +'">'+ item.KodeMRisk +'</option>';
      })
      html += '</select>';
      this.$('[data-risk-list]').append(html);
      this.setTemplate();
    },
    events: {
      'change [name="KodeMRisk"]': 'renderCalculation'
    },
    renderCalculation: function(obj) {
      var riskId = this.$('[name="KodeMRisk"]').val();
      eventAggregator.trigger('dashboard/result/risk:fecth', riskId);
      this.$el.modal('hide');
    },
    setTemplate: function() {
      if (this.riskIdSelected != "")
        this.$('[name="KodeMRisk"]').val(this.riskIdSelected);
    }
  });
});