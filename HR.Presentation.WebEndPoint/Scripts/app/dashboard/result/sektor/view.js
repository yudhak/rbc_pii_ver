define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var commonFunction = require('commonfunction');
  var commonConfig = require('commonconfig');
  var Model = require('./../model');
  var eventAggregator = require('eventaggregator');
  require('select2');

  module.exports = View.extend({
    template: _.template(template),
    initialize: function (options) {
      var self = this;
      this.sektorIdSelected = options.view;
      this.modelSektor = options.model;
      this.model = new Model();
      this.listenTo(this.model, 'request', function () {});
      this.listenTo(this.model, 'sync error', function () {});
    },
    afterRender: function () {
      var self = this;
      var html = '<select class="form-control select" name="NamaSektor">';
      $.each(this.modelSektor, function(index, item) {
        html += '<option value="'+ item.SektorId +'">'+ item.NamaSektor +'</option>';
      })
      html += '</select>';
      this.$('[data-sektor-list]').append(html);
      this.setTemplate();
    },
    events: {
        'change [name="NamaSektor"]': 'renderCalculation'
    },
    renderCalculation: function(obj) {
      var sektorId = this.$('[name="NamaSektor"]').val();
      if (sektorId != "") {
        eventAggregator.trigger('dashboard/result/sektor:fecth', sektorId);
        this.$el.modal('hide');
      }
    },
    setTemplate: function() {
      if (this.sektorIdSelected != "")
        this.$('[name="NamaSektor"]').val(this.sektorIdSelected);
    }
  });
});