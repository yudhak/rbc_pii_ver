define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var commonFunction = require('commonfunction');
  var commonConfig = require('commonconfig');
  var Model = require('./../model');
  var eventAggregator = require('eventaggregator');
  require('select2');

  module.exports = View.extend({
    template: _.template(template),
    initialize: function () {
      var self = this;
      this.model = new Model();
      this.listenTo(this.model, 'request', function () {});
      this.listenTo(this.model, 'sync error', function () {});
      commonFunction.setSelect2Scenario(this);
    },
    afterRender: function () {
    },
    events: {
        'change [name="NamaScenario"]': 'renderCalculation'
    },
    renderCalculation: function(obj) {
      var scenarioId = this.$('[name="NamaScenario"]').val();
      eventAggregator.trigger('dashboard/calculation/scenario:fecth', scenarioId);
      this.$el.modal('hide');
    }
  });
});