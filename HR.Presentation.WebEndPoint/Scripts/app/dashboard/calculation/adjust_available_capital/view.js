define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var commonFunction = require('commonfunction');
  var commonConfig = require('commonconfig');
  var Model = require('./model');
  var eventAggregator = require('eventaggregator');
  require('bootstrap-validator');
  require('datetimepicker');
  require('select2');
  var moment = require('moment');

  module.exports = View.extend({
    template: _.template(template),
    initialize: function (options) {
      var self = this;
      this.model = new Model();
      this.dataCapital = options.model;
      this.listenTo(this.model, 'request', function() {});

      this.listenTo(this.model, 'sync error', function() {});

      this.listenTo(this.model, 'sync', function(model) {
        commonFunction.responseSuccessUpdateAddDelete('Available Risk berhasil disimpan.');
        self.$el.modal('hide');
        eventAggregator.trigger('dashboard/calculation/adjust_available_capital:fecth');
      });
    },
    afterRender: function () {
      this.setTemplate();
    },
    events: {
      'click [btn-save]': 'getConfirmation',
      'change [currency-value]': 'currencyValidationOnChange'
    },
    setTemplate: function() {
      let self = this;
      let html = '<div class="marginbottom-20 scrollable-matrix">'
      html += '<div class="table-mat">'
      html += '<div class="heading-mat">'
      html += '<div class="cell-mat header-column"><p>Name</p></div>'
      $.each(this.dataCapital, function(index, va){
        html += '<div class="cell-mat header-column">'
        html += '<p class="header-column-position">'+ va.Year +'</p>'
        html += '</div>'
      });
      html += '</div>'
      
      html += '<div class="row-mat">'
      html += '<div class="cell-mat"><p>Available Capital</p></div>'
      $.each(this.dataCapital, function(index, va){
        html += '<div class="cell-mat text-center" data-cell-risk="'+ va.Year +'">'
        html += '<div><input type="text" currency-value style="width:100px;padding:3px" value="'+ va.Value +'" class="form-control" name="CapitalYear-'+ va.Year +'" /></div>'
        html += '</div>'
      });
      html += '</div>'

      html += '</div>'
      html += '</div>'

      this.$('[data-available-capital]').append(html);
    },
    getValueChanged: function() {
      let self = this;
      let data = [];
      if (this.dataCapital) {
        for (let i = 0; i < this.dataCapital.length; i++) {
          const year = this.dataCapital[i].Year;
          const oldValue = this.dataCapital[i].Value;
          const newValue = this.$('[name="CapitalYear-'+ year +'"]').val();
          let dataChanged = {};
          if (oldValue != newValue) {
            dataChanged.Year = year;
            dataChanged.Value = newValue;
            data.push(dataChanged);
          }
        }
      } else {
        commonFunction.responseWarningCannotExecute("Jumlah data list tahun dan nilai Available Capital tidak ditemukan.");
      }
      return data;
    },
    currencyValidationOnChange: function(e){
      var exposeFormat = /^\s*[0-9]+([\.][0-9][0-9]{0,12})?$|^$|^\s*$/;
      var attrName = e.target.name;
      var attrValue = this.$('[name="'+ attrName +'"]').val();
      if (exposeFormat.test(attrValue)) {
          this.$('[name="'+ attrName +'"]').removeClass('invalidCurrencyFormat');
      } else {
          commonFunction.responseWarningCannotExecute("Nilai Avaialable Capital tidak sesuai format. Format yang dizinkan 1000.02 / 123.39847. Angka di belakang koma hanya diizinkan maksimal sebanyak 13 digit.");
          this.$('[name="'+ attrName +'"]').addClass('invalidCurrencyFormat');
      }
  },
    currencyValidation: function() {
      let isValid = true;
      
      var valueFormat = /^\s*[0-9]+([\.][0-9][0-9]{0,12})?$|^$|^\s*$/;
      this.$('[currency-value]').each(function(i, item) {
        if (item.value == "") {
          commonFunction.responseWarningCannotExecute("Nilai Avaialable Capital wajib diisi.");
        } else {
          const objName = item.name;
          const currentObjectValue = $('[name="'+ objName +'"]').val();
          if (valueFormat.test(currentObjectValue)) {
            $('[name="'+ objName +'"]').removeClass('invalidCurrencyFormat');
          } else {
            isValid = false;
            commonFunction.responseWarningCannotExecute("Nilai Available Capital tidak sesuai format. Format yang dizinkan 1000.02 / 123.39847. Angka di belakang koma hanya diizinkan maksimal sebanyak 13 digit.");
            $('[name="'+ objName +'"]').addClass('invalidCurrencyFormat');
          }
        }
      });

      return isValid;
    },
    getConfirmation: function(){
      const isValid = this.currencyValidation();
      if(isValid) {
        var retVal = confirm("Apakah anda yakin ingin merubah data tersebut?");
        if( retVal == true ){
          this.doSave(this.getValueChanged());
        }
        else{
          this.$('[btn-save]').attr('disabled', false);
        }
      } else {
        this.$('[btn-save]').attr('disabled', false);
      }
    },
    doSave: function (data) {
      let obj = {};
      obj.ListChanged = data;
      this.model.save(obj);
    }
  });
});