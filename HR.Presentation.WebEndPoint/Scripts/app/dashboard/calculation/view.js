define(function (require, exports, module) {
    'use strict';

    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');
    var Model = require('./model');
    var Collection = require('./collection');
    const numeral = require('numeral');
    require('jquerymask');

    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content tbl bg-white',
        template: _.template(template),
        initialize: function () {
            var self = this;
            this.model = new Model();
            this.isDataAvailable = 0;
            this.correlatedSektor = null;
            this.isAjust = false;

            this.listenToOnce(this.model, 'sync', function (model) {
                this.render();
            });
            this.fetchData();

            this.listenTo(eventAggregator, 'dashboard/calculation/scenario:fecth', function (scenarioId) {
                self.fetchData(scenarioId);
            });
            this.listenTo(eventAggregator, 'dashboard/calculation/adjust_available_capital:fecth', function () {
                self.isAjust = true;
                self.fetchData();
            });
            this.roles = JSON.parse(localStorage.getItem('roles'));
        },
        events: {
            'click [change-scenario]': 'chooseScenario',
            'click [btn-ajdust-capital]': 'openAdjustAvailable'
        },
        afterRender: function () {
            var self = this;
            this.setTemplate(this.model);
            //this.renderCurrencyFormat();
            this.setRoleAccess();
        },
        fetchData: function () {
            let self = this;
            this.model.fetch({
                reset: true,
                success: function () {
                    if (self.isAjust) {
                        self.$('[asset-projection-container]').empty();
                        self.setTemplate(self.model);
                    }
                }
            });
            commonFunction.showLoadingSpinner();
        },
        setTemplate: function (data) {
            var self = this;
            this.isDataAvailable += 1;
            if (this.isDataAvailable > 1) {
                if (data.attributes.CalculationResult) {
                    this.renderProjectDetail(data.attributes.CalculationResult);
                    this.renderAggregationProject(data.attributes.CalculationResult.AggregationOfProject, data.attributes.CalculationResult.ProjectMonitoring);
                    this.renderAggregationOfSector(data.attributes.CalculationResult.AggregationSektor, data.attributes.CalculationResult.SektorMonitoring);
                    this.renderAggregationOfRisk(data.attributes.CalculationResult.AggregationRisk, data.attributes.CalculationResult.RiskRegistrasiMonitoring);
                    this.renderAssetProjecttion(
                        data.attributes.CalculationResult.AssetProjectionLiquidity.AssetClassDefault, data.attributes.CalculationResult.AssetProjectionLiquidity.AssetClassProjectionCollection,
                        data.attributes.CalculationResult.AssetProjectionLiquidity.AssetSummaryCollection, data.attributes.CalculationResult.AssetProjectionLiquidity.AssetSummaryDefault,
                        data.attributes.CalculationResult.AssetProjectionLiquidity.AssetProjectionIlustration
                    );
                    this.renderScenarioTesting(data.attributes.ScenarioTesting);
                    this.renderLiquidity(data.attributes.CalculationResult.AssetProjectionLiquidity.AssetClassDefault, data.attributes.CalculationResult.AssetProjectionLiquidity.AssetClassLiquidityCollection,
                        data.attributes.CalculationResult.AssetProjectionLiquidity.LiquidAsset
                    );
                    commonFunction.closeLoadingSpinner();
                } else {
                    commonFunction.responseWarningCannotExecute("Data kalkulasi tidak ditemukan.");
                }
            }
        },
        renderCurrencyFormat: function () {
            this.$('[currency-formating]').mask('000,000,000,000,000.00', {
                reverse: true
            });
        },
        renderProjectDetail: function (data) {
            var self = this;
            $.each(data.ProjectMonitoring, function (i, value) {
                $.each(data.ProjectDetail, function (index, lue) {
                    var riskRegistrasi = lue.RiskRegistrasi;
                    var dataProject = lue;
                    var projectName = lue.NamaProject;
                    var sektorName = lue.NamaSektor;
                    self.correlatedSektor = lue.CorrelatedSektorDetail;
                if(value == lue.ProjectId)
                {
                    var html = '<div class="col-md-12">'
                    html += '<button btn-colap class="btn btn-default-report" type="button" data-toggle="collapse" data-target="#collapse-' + lue.ProjectId + '" aria-expanded="false" aria-controls="collapse-' + lue.ProjectId + '">'
                    html += '' + projectName + ' - ' + ' Sektor ' + sektorName + ''
                    html += '</button>'
                    html += '<div class="collapse" id="collapse-' + lue.ProjectId + '">'
                    html += '<div class="margintop-5 text-bold-italic">Correlation Matrix between Risk Categories | ' + projectName + ' | Sektor ' + sektorName + '</div>'
                    html += '<div class="marginbottom-20 scrollable-matrix">'
                    html += '<div class="table-mat">'
                    html += '<div class="heading-mat">'
                    html += '<div class="cell-mat header-column"><p>Kode</p></div>'

                    $.each(riskRegistrasi, function (index, va) {
                        html += '<div class="cell-mat header-column" data-heading-risk="' + va.KodeMRisk + '">'
                        html += '<p class="header-column-position">' + va.KodeMRisk + '</p>'
                        html += '</div>'
                    });

                    html += '</div>'

                    $.each(riskRegistrasi, function (index, value) {
                        html += '<div class="row-mat" data-risk-row="' + value.KodeMRisk + '">'
                        html += '<div class="cell-mat text-center"><p>' + value.KodeMRisk + '</p></div>'

                        $.each(riskRegistrasi, function (dex, val) {
                            var corr = self.correlatedSektor;
                            html += '<div class="cell-mat text-center" data-cell-risk="' + value.Id + '-' + val.Id + '" data="' + value.KodeMRisk + '-' + val.KodeMRisk + '">'
                            $.grep(corr, function (co, i) {
                                if (co.RiskRegistrasiIdRow == value.Id && co.RiskRegistrasiIdCol == val.Id) {
                                    html += '<div class="text-right"><p currency-formating>' + numeral(co.CorrelationMatrixValue).format('0,0.00') + '</p></div>'
                                }
                            });
                            html += '</div>'
                        });

                        html += '</div>'
                    });
                    html += '</div>'
                    html += '</div>'

                    //UndifersifiedRiskCapital
                    html += '<div class="margintop-20 text-bold-italic">Undifersified Risk Capital | ' + projectName + ' | Sektor ' + sektorName + '</div>'
                    html += '<div class="marginbottom-20 scrollable-matrix">'
                    html += '<div class="table-mat">'
                    html += '<div class="heading-mat">'
                    html += '<div class="cell-mat header-column"><p>Kode</p></div>'
                    html += '<div class="cell-mat header-column"><p>Deskripsi</p></div>'
                    //render year columns
                    $.each(dataProject.UndiversifiedYearCollection, function (index, va) {
                        html += '<div class="cell-mat header-column" data-heading-risk="' + va.Year + '">'
                        html += '<p class="header-column-position">' + va.Year + '</p>'
                        html += '</div>'
                    });
                    html += '</div>'
                    //render row
                    $.each(riskRegistrasi, function (i, riskValue) {
                        html += '<div class="row-mat" data-risk-row="' + riskValue.KodeMRisk + '">'
                        html += '<div class="cell-mat text-center"><p>' + riskValue.KodeMRisk + '</p></div>'
                        html += '<div class="cell-mat"><p>' + riskValue.NamaCategoryRisk + '</p></div>'
                        $.each(dataProject.UndiversifiedYearCollection, function (ind, valu) {
                            $.grep(valu.YearValue, function (co, i) {
                                if (co.RiskRegistrasiId == riskValue.Id) {
                                    html += '<div class="cell-mat text-right"><p currency-formating>' + numeral(co.ValueUndiversified).format('0,0.00') + '</p></div>'
                                }
                            });
                        });
                        html += '</div>'
                    });
                    //total
                    html += '<div class="cell-mat text-center"><p></p></div>'
                    html += '<div class="cell-mat text-center total-value-matrix"><p>TOTAL</p></div>'
                    $.each(dataProject.UndiversifiedYearCollection, function (nd, val) {
                        html += '<div class="cell-mat text-right total-value-matrix"><p currency-formating>' + numeral(val.Total).format('0,0.00') + '</p></div>'
                    });
                    html += '</div>'
                    html += '</div>'

                    //DiversifiedRiskCapital
                    html += '<div class="margintop-20 text-bold-italic">Diversified Risk Capital | ' + projectName + ' | Sektor ' + sektorName + '</div>'
                    html += '<div class="marginbottom-20 scrollable-matrix">'
                    html += '<div class="table-mat">'
                    html += '<div class="heading-mat">'
                    html += '<div class="cell-mat header-column"><p>Kode</p></div>'
                    html += '<div class="cell-mat header-column"><p>Deskripsi</p></div>'
                    //render year columns
                    $.each(dataProject.DiversifiedRiskCapitalCollection, function (index, va) {
                        html += '<div class="cell-mat header-column" data-heading-risk="' + va.Year + '">'
                        html += '<p class="header-column-position">' + va.Year + '</p>'
                        html += '</div>'
                    });
                    html += '</div>'
                    //render row
                    $.each(riskRegistrasi, function (i, riskValue) {
                        html += '<div class="row-mat" data-risk-row="' + riskValue.KodeMRisk + '">'
                        html += '<div class="cell-mat text-center"><p>' + riskValue.KodeMRisk + '</p></div>'
                        html += '<div class="cell-mat"><p>' + riskValue.NamaCategoryRisk + '</p></div>'
                        $.each(dataProject.DiversifiedRiskCapitalCollection, function (ind, valu) {
                            $.grep(valu.RiskValueCollection, function (co, i) {
                                if (co.RiskRegistrasiId == riskValue.Id) {
                                    html += '<div class="cell-mat text-right"><p currency-formating>' + numeral(co.Value).format('0,0.00') + '</p></div>'
                                }
                            });
                        });
                        html += '</div>'
                    });
                    //total
                    html += '<div class="cell-mat text-center"><p></p></div>'
                    html += '<div class="cell-mat text-center total-value-matrix"><p>TOTAL</p></div>'
                    $.each(dataProject.DiversifiedRiskCapitalCollection, function (nd, val) {
                        html += '<div class="cell-mat text-right total-value-matrix"><p currency-formating>' + numeral(val.Total).format('0,0.00') + '</p></div>'
                    });
                    html += '</div>'
                    html += '</div>'

                    html += '</div>'
                    html += '</div>'

                    self.$('[correlated-sektor-container]').append(html);
                }
            });
            });
            this.$('[scenario-name]').text('Skenario : ' + data.NamaScenario + '');
        },
        renderAggregationProject: function (data, dataProject) {
            var self = this;

            var html = '<div class="col-md-12">'
            html += '<div class="margintop-5 text-bold-italic">Undiversified Project Capital</div>'
            html += '<div class="marginbottom-20 scrollable-matrix">'
            html += '<div class="table-mat">'
            html += '<div class="heading-mat">'
            html += '<div class="cell-mat header-column"><p>Nama Proyek</p></div>'
            html += '<div class="cell-mat header-column"><p>Sektor</p></div>'
            $.each(data.AggregationUndiversifiedProjectCapital, function (index, va) {
                html += '<div class="cell-mat header-column" data-heading-risk="' + va.aggregationYears + '">'
                html += '<p class="header-column-position">' + va.aggregationYears + '</p>'
                html += '</div>'
            });
            html += '</div>'
            $.each(dataProject, function (i, val) {
                $.each(data.AggregationOfProjectCollection, function (i, value) {
                    if(val == value.ProjectId)
                    {
                        html += '<div class="row-mat" data-risk-row="' + value.ProjectId + '">'
                        html += '<div class="cell-mat"><p>' + value.NamaProject + '</p></div>'
                        html += '<div class="cell-mat"><p>' + value.NamaSektor + '</p></div>'
                        $.each(data.AggregationUndiversifiedProjectCapital, function (index, val) {
                            html += '<div class="cell-mat text-right" data-cell-risk="' + val.aggregationYears + '-' + value.ProjectId + '" data="' + val.aggregationYears + '-' + value.ProjectId + '">'
                            $.grep(val.AggregationUndiversifiedProjectCollection, function (ind, lue) {
                                if (ind.ProjectId == value.ProjectId) {
                                    html += '<div><p currency-formating>' + numeral(ind.Total).format('0,0.00') + '</p></div>'
                                }
                            });
                            html += '</div>'
                        });
                        html += '</div>'
                    }
                });
            });

            //total
            html += '<div class="cell-mat text-right"><p></p></div>'
            html += '<div class="cell-mat text-right total-value-matrix"><p>TOTAL</p></div>'
            $.each(data.AggregationUndiversifiedProjectCapital, function (i, value) {
                html += '<div class="cell-mat text-right total-value-matrix"><p currency-formating>' + numeral(value.TotalPerYear).format('0,0.00') + '</p></div>'
            });

            html += '</div>'
            html += '</div>'
            //end of undiversified

            //intra diversified
            html += '<div class="margintop-5 text-bold-italic">Intra-Project Diversified Project Capital</div>'
            html += '<div class="marginbottom-20 scrollable-matrix">'
            html += '<div class="table-mat">'
            html += '<div class="heading-mat">'
            html += '<div class="cell-mat header-column"><p>Nama Proyek</p></div>'
            html += '<div class="cell-mat header-column"><p>Sektor</p></div>'
            $.each(data.AggregationIntraDiversifiedProjectCapital, function (index, va) {
                html += '<div class="cell-mat header-column" data-heading-risk="' + va.aggregationYears + '">'
                html += '<p class="header-column-position">' + va.aggregationYears + '</p>'
                html += '</div>'
            });
            html += '</div>'

            $.each(dataProject, function (i, val) {
                $.each(data.AggregationOfProjectCollection, function (i, value) {
                    if(val == value.ProjectId)
                    {
                        html += '<div class="row-mat" data-risk-row="' + value.ProjectId + '">'
                        html += '<div class="cell-mat"><p>' + value.NamaProject + '</p></div>'
                        html += '<div class="cell-mat"><p>' + value.NamaSektor + '</p></div>'
                        $.each(data.AggregationIntraDiversifiedProjectCapital, function (index, val) {
                            html += '<div class="cell-mat text-right" data-cell-risk="' + val.aggregationYears + '-' + value.ProjectId + '" data="' + val.aggregationYears + '-' + value.ProjectId + '">'
                            $.grep(val.AggregationIntraDiversifiedProjectCollection, function (ind, lue) {
                                if (ind.ProjectId == value.ProjectId) {
                                    html += '<div><p currency-formating>' + numeral(ind.Total).format('0,0.00') + '</p></div>'
                                }
                            });
                            html += '</div>'
                        });
                        html += '</div>'
                    }
                });
            });

            //total
            html += '<div class="cell-mat text-right"><p></p></div>'
            html += '<div class="cell-mat text-right total-value-matrix"><p>TOTAL</p></div>'
            $.each(data.AggregationIntraDiversifiedProjectCapital, function (i, value) {
                html += '<div class="cell-mat text-right total-value-matrix"><p currency-formating>' + numeral(value.TotalPerYear).format('0,0.00') + '</p></div>'
            });

            html += '</div>'
            html += '</div>'
            //end of intra diversified

            //inter diversified
            html += '<div class="margintop-5 text-bold-italic">Inter-Project Diversified Project Capital</div>'
            html += '<div class="marginbottom-20 scrollable-matrix">'
            html += '<div class="table-mat">'
            html += '<div class="heading-mat">'
            html += '<div class="cell-mat header-column"><p>Nama Proyek</p></div>'
            html += '<div class="cell-mat header-column"><p>Sektor</p></div>'
            $.each(data.AggregationInterDiversifiedProjectCapital, function (index, va) {
                html += '<div class="cell-mat header-column" data-heading-risk="' + va.aggregationYears + '">'
                html += '<p class="header-column-position">' + va.aggregationYears + '</p>'
                html += '</div>'
            });
            html += '</div>'

            $.each(dataProject, function (i, val) {
                $.each(data.AggregationOfProjectCollection, function (i, value) {
                    if(val == value.ProjectId)
                    {
                        html += '<div class="row-mat" data-risk-row="' + value.ProjectId + '">'
                        html += '<div class="cell-mat"><p>' + value.NamaProject + '</p></div>'
                        html += '<div class="cell-mat"><p>' + value.NamaSektor + '</p></div>'
                        $.each(data.AggregationInterDiversifiedProjectCapital, function (index, val) {
                            html += '<div class="cell-mat text-right" data-cell-risk="' + val.aggregationYears + '-' + value.ProjectId + '" data="' + val.aggregationYears + '-' + value.ProjectId + '">'
                            $.grep(val.AggregationInterDiversifiedProjectCollection, function (ind, lue) {
                                if (ind.ProjectId == value.ProjectId) {
                                    html += '<div><p currency-formating>' + numeral(ind.Total).format('0,0.00') + '</p></div>'
                                }
                            });
                            html += '</div>'
                        });
                        html += '</div>'
                    }
                });
            });

            //total
            html += '<div class="cell-mat text-right"><p></p></div>'
            html += '<div class="cell-mat text-right total-value-matrix"><p>TOTAL</p></div>'
            $.each(data.AggregationInterDiversifiedProjectCapital, function (i, value) {
                html += '<div class="cell-mat text-right total-value-matrix"><p currency-formating>' + numeral(value.TotalPerYear).format('0,0.00') + '</p></div>'
            });

            html += '</div>'
            html += '</div>'
            //end of inter diversified

            html += '</div>'

            self.$('[aggregation-project-container]').append(html);
        },
        renderAggregationOfSector: function (data, dataSektor) {
            var self = this;

            var html = '<div class="col-md-12">'
            html += '<div class="margintop-5 text-bold-italic">Undiversified Risk Capital</div>'
            html += '<div class="marginbottom-20 scrollable-matrix">'
            html += '<div class="table-mat">'
            html += '<div class="heading-mat">'
            html += '<div class="cell-mat header-column"><p>Nama Sektor</p></div>'
            $.each(data.UndiversifiedAggregationSektor.YearCollection, function (index, va) {
                html += '<div class="cell-mat header-column" data-heading-risk="' + va.Year + '">'
                html += '<p class="header-column-position">' + va.Year + '</p>'
                html += '</div>'
            });
            html += '</div>'

            $.each(dataSektor, function (i, val) {
                $.each(data.Sektor, function (i, value) {
                    if(val == value.SektorId)
                    {
                        html += '<div class="row-mat" data-risk-row="' + value.SektorId + '">'
                        html += '<div class="cell-mat"><p>' + value.NamaSektor + '</p></div>'
                        $.each(data.UndiversifiedAggregationSektor.YearCollection, function (index, val) {
                            html += '<div class="cell-mat text-right" data-cell-risk="' + val.Year + '-' + value.SektorId + '">'
                            $.grep(val.YearValues, function (ind, lue) {
                                if (ind.SektorId == value.SektorId) {
                                    html += '<div><p currency-formating>' + numeral(ind.Value).format('0,0.00') + '</p></div>'
                                }
                            });
                            html += '</div>'
                        });
                        html += '</div>'
                    }
                });
            });

            //total
            html += '<div class="cell-mat text-right total-value-matrix"><p>TOTAL</p></div>'
            $.each(data.UndiversifiedAggregationSektor.YearCollection, function (i, value) {
                html += '<div class="cell-mat text-right total-value-matrix"><p currency-formating>' + numeral(value.Total).format('0,0.00') + '</p></div>'
            });

            html += '</div>'
            html += '</div>'
            //end of undiversified

            //intra-project diversified
            html += '<div class="margintop-5 text-bold-italic">Intra-Project Diversified Risk Capital</div>'
            html += '<div class="marginbottom-20 scrollable-matrix">'
            html += '<div class="table-mat">'
            html += '<div class="heading-mat">'
            html += '<div class="cell-mat header-column"><p>Nama Sektor</p></div>'
            $.each(data.IntraProjectDiversifiedAggregationSektor.YearCollection, function (index, va) {
                html += '<div class="cell-mat header-column" data-heading-risk="' + va.Year + '">'
                html += '<p class="header-column-position">' + va.Year + '</p>'
                html += '</div>'
            });
            html += '</div>'

            $.each(dataSektor, function (i, val) {
                $.each(data.IntraProjectDiversifiedAggregationSektor.SektorIntraDiversified, function (i, value) {
                    if(val == value.SektorId)
                    {
                        html += '<div class="row-mat" data-risk-row="' + value.SektorId + '">'
                        html += '<div class="cell-mat"><p>' + value.NamaSektor + '</p></div>'
                        $.each(data.IntraProjectDiversifiedAggregationSektor.YearCollection, function (index, val) {
                            html += '<div class="cell-mat text-right" data-cell-risk="' + val.Year + '-' + value.SektorId + '">'
                            $.grep(val.YearValues, function (ind, lue) {
                                if (ind.SektorId == value.SektorId) {
                                    html += '<div><p currency-formating>' + numeral(ind.Value).format('0,0.00') + '</p></div>'
                                }
                            });
                            html += '</div>'
                        });
                        html += '</div>'
                    }
                });
            });

            //total
            html += '<div class="cell-mat text-right total-value-matrix"><p>TOTAL</p></div>'
            $.each(data.IntraProjectDiversifiedAggregationSektor.YearCollection, function (i, value) {
                html += '<div class="cell-mat text-right total-value-matrix"><p currency-formating>' + numeral(value.Total).format('0,0.00') + '</p></div>'
            });

            html += '</div>'
            html += '</div>'
            //end intra-project diversified

            //inter-project diversified
            html += '<div class="margintop-5 text-bold-italic">Inter-Project Diversified Risk Capital</div>'
            html += '<div class="marginbottom-20 scrollable-matrix">'
            html += '<div class="table-mat">'
            html += '<div class="heading-mat">'
            html += '<div class="cell-mat header-column"><p>Nama Sektor</p></div>'
            $.each(data.InterProjectDiversifiedAggregationSektor.YearCollection, function (index, va) {
                html += '<div class="cell-mat header-column" data-heading-risk="' + va.Year + '">'
                html += '<p class="header-column-position">' + va.Year + '</p>'
                html += '</div>'
            });
            html += '</div>'

            $.each(dataSektor, function (i, val) {
                $.each(data.InterProjectDiversifiedAggregationSektor.SektorInterDiversified, function (i, value) {
                    if(val == value.SektorId)
                    {
                        html += '<div class="row-mat" data-risk-row="' + value.SektorId + '">'
                        html += '<div class="cell-mat"><p>' + value.NamaSektor + '</p></div>'
                        $.each(data.InterProjectDiversifiedAggregationSektor.YearCollection, function (index, val) {
                            html += '<div class="cell-mat text-right" data-cell-risk="' + val.Year + '-' + value.SektorId + '">'
                            $.grep(val.YearValues, function (ind, lue) {
                                if (ind.SektorId == value.SektorId) {
                                    html += '<div><p currency-formating>' + numeral(ind.Value).format('0,0.00') + '</p></div>'
                                }
                            });
                            html += '</div>'
                        });
                        html += '</div>'
                    }
                });
            });

            //total
            html += '<div class="cell-mat text-right total-value-matrix"><p>TOTAL</p></div>'
            $.each(data.InterProjectDiversifiedAggregationSektor.YearCollection, function (i, value) {
                html += '<div class="cell-mat text-right total-value-matrix"><p currency-formating>' + numeral(value.Total).format('0,0.00') + '</p></div>'
            });

            html += '</div>'
            html += '</div>'
            //end inter-project diversified

            html += '</div>'

            self.$('[aggregation-sector-container]').append(html);
        },
        renderAggregationOfRisk: function (data, dataRiskRegistrasi) {
            var self = this;

            var html = '<div class="col-md-12">'
            html += '<div class="margintop-5 text-bold-italic">Undiversified Risk Capital</div>'
            html += '<div class="marginbottom-20 scrollable-matrix">'
            html += '<div class="table-mat">'
            html += '<div class="heading-mat">'
            html += '<div class="cell-mat header-column"><p>Kode</p></div>'
            html += '<div class="cell-mat header-column"><p>Deskripsi</p></div>'
            $.each(data.UndiversifiedAggregationRisk.RiskYearCollection, function (index, va) {
                html += '<div class="cell-mat header-column" data-heading-risk="' + va.Year + '">'
                html += '<p class="header-column-position">' + va.Year + '</p>'
                html += '</div>'
            });
            html += '</div>'

            $.each(dataRiskRegistrasi, function (i, val) {
                $.each(data.RiskRegistrasi, function (i, value) {
                    if(val == value.Id)
                    {
                        html += '<div class="row-mat" data-risk-row="' + value.Id + '">'
                        html += '<div class="cell-mat text-center"><p>' + value.KodeMRisk + '</p></div>'
                        html += '<div class="cell-mat"><p>' + value.NamaCategoryRisk + '</p></div>'
                        $.each(data.UndiversifiedAggregationRisk.RiskYearCollection, function (index, val) {
                            html += '<div class="cell-mat text-right" data-cell-risk="' + val.Year + '-' + value.Id + '">'
                            $.grep(val.RiskYearValue, function (ind, lue) {
                                if (ind.RiskRegistrasiId == value.Id) {
                                    html += '<div><p currency-formating>' + numeral(ind.Value).format('0,0.00') + '</p></div>'
                                }
                            });
                            html += '</div>'
                        });
                        html += '</div>'
                    }
                });
            });

            //total
            html += '<div class="cell-mat text-right total-value-matrix"></div>'
            html += '<div class="cell-mat text-right total-value-matrix"><p>TOTAL</p></div>'
            $.each(data.UndiversifiedAggregationRisk.RiskYearCollection, function (i, value) {
                html += '<div class="cell-mat text-right total-value-matrix"><p currency-formating>' + numeral(value.Total).format('0,0.00') + '</p></div>'
            });

            html += '</div>'
            html += '</div>'
            //end of undiversified

            //intra-project diversified
            html += '<div class="margintop-5 text-bold-italic">Intra-Project Diversified Risk Capital</div>'
            html += '<div class="marginbottom-20 scrollable-matrix">'
            html += '<div class="table-mat">'
            html += '<div class="heading-mat">'
            html += '<div class="cell-mat header-column"><p>Kode</p></div>'
            html += '<div class="cell-mat header-column"><p>Deskripsi</p></div>'
            $.each(data.IntraProjectDiversifiedAggregationRisk.RiskYearCollection, function (index, va) {
                html += '<div class="cell-mat header-column" data-heading-risk="' + va.Year + '">'
                html += '<p class="header-column-position">' + va.Year + '</p>'
                html += '</div>'
            });
            html += '</div>'

            $.each(dataRiskRegistrasi, function (i, val) {
                $.each(data.RiskRegistrasi, function (i, value) {
                    if(val == value.Id)
                    {
                        html += '<div class="row-mat" data-risk-row="' + value.Id + '">'
                        html += '<div class="cell-mat text-center"><p>' + value.KodeMRisk + '</p></div>'
                        html += '<div class="cell-mat"><p>' + value.NamaCategoryRisk + '</p></div>'
                        $.each(data.IntraProjectDiversifiedAggregationRisk.RiskYearCollection, function (index, val) {
                            html += '<div class="cell-mat text-right" data-cell-risk="' + val.Year + '-' + value.Id + '">'
                            $.grep(val.RiskYearValue, function (ind, lue) {
                                if (ind.RiskRegistrasiId == value.Id) {
                                    html += '<div><p currency-formating>' + numeral(ind.Value).format('0,0.00') + '</p></div>'
                                }
                            });
                            html += '</div>'
                        });
                        html += '</div>'
                    }
                });
            });

            //total
            html += '<div class="cell-mat text-right total-value-matrix"></div>'
            html += '<div class="cell-mat text-right total-value-matrix"><p>TOTAL</p></div>'
            $.each(data.IntraProjectDiversifiedAggregationRisk.RiskYearCollection, function (i, value) {
                html += '<div class="cell-mat text-right total-value-matrix"><p currency-formating>' + numeral(value.Total).format('0,0.00') + '</p></div>'
            });

            html += '</div>'
            html += '</div>'
            //end of intra-project diversified

            //inter-project diversified
            html += '<div class="margintop-5 text-bold-italic">Inter-Project Diversified Risk Capital</div>'
            html += '<div class="marginbottom-20 scrollable-matrix">'
            html += '<div class="table-mat">'
            html += '<div class="heading-mat">'
            html += '<div class="cell-mat header-column"><p>Kode</p></div>'
            html += '<div class="cell-mat header-column"><p>Deskripsi</p></div>'
            $.each(data.InterProjectDiversifiedAggregationRisk.RiskYearCollection, function (index, va) {
                html += '<div class="cell-mat header-column" data-heading-risk="' + va.Year + '">'
                html += '<p class="header-column-position">' + va.Year + '</p>'
                html += '</div>'
            });
            html += '</div>'

            $.each(dataRiskRegistrasi, function (i, val) {
                $.each(data.RiskRegistrasi, function (i, value) {
                    if(val == value.Id)
                    {
                        html += '<div class="row-mat" data-risk-row="' + value.Id + '">'
                        html += '<div class="cell-mat text-center"><p>' + value.KodeMRisk + '</p></div>'
                        html += '<div class="cell-mat"><p>' + value.NamaCategoryRisk + '</p></div>'
                        $.each(data.InterProjectDiversifiedAggregationRisk.RiskYearCollection, function (index, val) {
                            html += '<div class="cell-mat text-right" data-cell-risk="' + val.Year + '-' + value.Id + '">'
                            $.grep(val.RiskYearValue, function (ind, lue) {
                                if (ind.RiskRegistrasiId == value.Id) {
                                    html += '<div><p currency-formating>' + numeral(ind.Value).format('0,0.00') + '</p></div>'
                                }
                            });
                            html += '</div>'
                        });
                        html += '</div>'
                    }
                });
            });

            //total
            html += '<div class="cell-mat text-right total-value-matrix"></div>'
            html += '<div class="cell-mat text-right total-value-matrix"><p>TOTAL</p></div>'
            $.each(data.InterProjectDiversifiedAggregationRisk.RiskYearCollection, function (i, value) {
                html += '<div class="cell-mat text-right total-value-matrix"><p currency-formating>' + numeral(value.Total).format('0,0.00') + '</p></div>'
            });

            html += '</div>'
            html += '</div>'
            //end of inter-project diversified

            html += '</div>'

            self.$('[aggregation-risk-container]').append(html);
        },
        renderAssetProjecttion: function (assetClassDefault, assetClassProjectionCollection, summaryYear, summaryItem, illustration) {
            var self = this;
            let availableCapitalYear = [];
            let availableCapitalLineValues = [];

            var html = '<div class="col-md-12">'
            html += '<div class="margintop-5 text-bold-italic">Investment strategy - to to keep the balance of assets between categories</div>'
            html += '<div class="marginbottom-20 scrollable-matrix">'
            html += '<div class="table-mat">'
            html += '<div class="heading-mat">'
            html += '<div class="cell-mat header-column"><p>Asset Class</p></div>'
            html += '<div class="cell-mat header-column"><p>Proportion</p></div>'
            html += '<div class="cell-mat header-column"><p>Term</p></div>'
            html += '<div class="cell-mat header-column"><p>Assumed Return</p></div>'
            html += '<div class="cell-mat header-column"><p>Outstanding Years</p></div>'
            html += '<div class="cell-mat header-column"><p>Asset Value</p></div>'
            $.each(assetClassProjectionCollection, function (index, va) {
                html += '<div class="cell-mat header-column" data-heading-risk="' + va.Year + '">'
                html += '<p class="header-column-position">' + va.Year + '</p>'
                html += '</div>'
            });
            html += '</div>'

            $.each(assetClassDefault, function (i, value) {
                html += '<div class="row-mat" data-risk-row="' + value.AssetClass + '">'
                html += '<div class="cell-mat text-right"><p>' + value.AssetClass + '</p></div>'
                html += '<div class="cell-mat text-right"><p>' + numeral(value.Proportion).format('0,0.00') + '%</p></div>'
                html += '<div class="cell-mat text-right"><p>' + value.TermAwal + ' - ' + value.TermAkhir + '</p></div>'
                html += '<div class="cell-mat text-right"><p>' + value.AssumedReturn + ' %</p></div>'
                html += '<div class="cell-mat text-right"><p>' + value.OutstandingYearAwal + ' - ' + value.OutstandingYearAkhir + '</p></div>'
                html += '<div class="cell-mat text-right"><p currency-formating>' + numeral(value.AssetValue).format('0,0.00') + '</p></div>'

                $.each(assetClassProjectionCollection, function (index, val) {
                    html += '<div class="cell-mat text-right" data-cell-risk="' + val.Year + '">'
                    $.grep(val.AssetList, function (item) {
                        if (item.AssetClass == value.AssetClass) {
                            html += '<div><p currency-formating>' + numeral(item.Value).format('0,0.00') + '</p></div>'
                        }
                    });
                    html += '</div>'
                });
                html += '</div>'
            });

            html += '</div>'
            html += '</div>'
            //end of investment strategy

            //summary
            html += '<div class="margintop-5 text-bold-italic"></div>'
            html += '<div class="marginbottom-20 scrollable-matrix">'
            html += '<div class="table-mat">'
            html += '<div class="heading-mat">'
            html += '<div class="cell-mat header-column"><p style="width:490px;">Name</p></div>'
            html += '<div class="cell-mat header-column"><p>Asset Value</p></div>'
            $.each(summaryYear, function (index, va) {
                html += '<div class="cell-mat header-column">'
                html += '<p class="header-column-position">' + va.Year + '</p>'
                html += '</div>'
                availableCapitalYear.push(va.Year);
            });
            html += '</div>'

            $.each(summaryItem, function (i, value) {
                var propertyName = i;
                html += '<div class="row-mat">'
                html += '<div class="cell-mat"><p>' + i + '</p></div>'
                html += '<div class="cell-mat text-right"><p currency-formating>' + numeral(value).format('0,0.00') + '</p></div>'
                $.each(summaryYear, function (index, val) {
                    html += '<div class="cell-mat text-right" data-cell-risk="' + val.Year + '">'
                    if (val.isEditedAvailableCapital == true && i == "AvailableCapitalProjected") {
                        html += '<div><p currency-formating>' + numeral(val[propertyName]).format('0,0.00') + '</p></div>'
                    } else {
                        html += '<div><p currency-formating>' + numeral(val[propertyName]).format('0,0.00') + '</p></div>'
                    }
                    html += '</div>'
                    if (i == "AvailableCapitalProjected") {
                        availableCapitalLineValues.push(val["AvailableCapitalProjected"]);
                    }
                });
                html += '</div>'
            });

            html += '</div>'
            html += '</div>'
            this.availableCapitalValue = availableCapitalLineValues;
            this.availableCapitalYear = availableCapitalYear;
            //end of summary

            //illustration
            var illusNames = [];
            _.each(illustration[0], function (i, item) {
                if (item != "Year")
                    illusNames.push(item);
            });

            html += '<div class="margintop-5 text-bold-italic">Asset Projection allowing for Recourse mechanism (for illustration only)</div>'
            html += '<div class="marginbottom-20 scrollable-matrix">'
            html += '<div class="table-mat">'
            html += '<div class="heading-mat">'
            html += '<div class="cell-mat header-column"><p style="width: 570px;">Name</p></div>'
            $.each(illustration, function (index, va) {
                html += '<div class="cell-mat header-column">'
                html += '<p class="header-column-position">' + va.Year + '</p>'
                html += '</div>'
            });
            html += '</div>'

            $.each(illusNames, function (i, item) {
                html += '<div class="row-mat">'
                if (item != "isEditedAvailableCapital") {
                    html += '<div class="cell-mat"><p>' + item + '</p></div>'

                    $.each(illustration, function (index, val) {
                        html += '<div class="cell-mat text-right" data-cell-risk="' + val.Year + '">'
                        if (val.isEditedAvailableCapital == true && item == "AvailableCapitalRecourse") {
                            html += '<div><p currency-formating>' + numeral(val[item]).format('0,0.00') + '</p></div>'
                        } else {
                            html += '<div><p currency-formating>' + numeral(val[item]).format('0,0.00') + '</p></div>'
                        }
                        html += '</div>'
                    });
                    html += '</div>'
                }
            });

            html += '</div>'
            html += '</div>'
            //end of illustration

            html += '</div>'

            self.$('[asset-projection-container]').append(html);
        },
        renderScenarioTesting: function (data) {
            var self = this;
            const yearLength = data.Year.length;

            var html = '<div class="col-md-12">'

            //Scenario Testing Collection
            html += '<div class="margintop-5 text-bold-italic">Scenario Testing Collection</div>'
            html += '<div class="marginbottom-20 scrollable-matrix">'
            html += '<div class="table-mat">'
            html += '<div class="heading-mat">'
            html += '<div class="cell-mat header-column"><p>Scenario</p></div>'
            html += '<div class="cell-mat header-column"><p>Deskripsi</p></div>'
            $.each(data.Year, function (index, va) {
                html += '<div class="cell-mat header-column">'
                html += '<p class="header-column-position">' + va + '</p>'
                html += '</div>'
            });
            html += '</div>'

            $.each(data.ScenarioTestingCollection, function (i, value) {
                html += '<div class="row-mat">'
                html += '<div class="cell-mat"><p>' + value.NamaScenario + '</p></div>'
                html += '<div class="cell-mat"><p>Total Undiversified Capital</p></div>'

                $.each(value.ScenarioTestingYearCollection, function (index, val) {
                    html += '<div class="cell-mat text-right" data-cell-risk="' + val.Year + '">'
                    html += '<div><p currency-formating>' + numeral(val.TotalUndiversifiedCapital).format('0,0.00') + '</p></div>'
                    html += '</div>'
                });

                html += '</div>'

                html += '<div class="row-mat">'
                html += '<div class="cell-mat"><p></p></div>'
                html += '<div class="cell-mat"><p>Total Diversified Capital</p></div>'

                $.each(value.ScenarioTestingYearCollection, function (index, val) {
                    html += '<div class="cell-mat text-right" data-cell-risk="' + val.Year + '">'
                    html += '<div><p currency-formating>' + numeral(val.TotalDiversifiedCapital).format('0,0.00') + '</p></div>'
                    html += '</div>'
                });

                html += '</div>'
            });

            html += '</div>'
            html += '</div>'
            //end of Scenario Testing Collection

            //Sensitivity
            html += '<div class="margintop-5 text-bold-italic">Sensivity</div>'
            html += '<div class="col-md-12 scrollable-matrix" style="margin-bottom: 15px;" obo-table-matrix>'
            html += '<div>'
            html += '<table style="width:100%" class="table-risk-matrix">'
            html += '<tr class="td-kiw-table header-kiw-table">'
            html += '<td>Tipe</td>'
            html += '<td class="td-kiw-table">Skenario</td>'
            html += '<td class="td-kiw-table">Deskripsi Skenario</td>'
            $.each(data.Year, function (index, va) {
                html += '<td class="td-kiw-table">' + va + '</td>'
            });
            html += '</tr>'

            //loop type name
            $.each(data.Sensitivity, function (i, item) {
                var lenRowSpan = item.length * 3;
                html += '<tr class="row-mat">'
                html += '<td class="td-kiw-table" rowspan="' + lenRowSpan + '">' + i + '</td>'
                if (item.length > 0) {
                    for (var i = 0; i < item.length; i++) {
                        html += '<td class="td-kiw-table" rowspan="3">' + item[i].NamaScenario + '</td>'
                        html += '<td class="td-kiw-table">Upper</td>'

                        let lengItemSensitivityUpper = 0;
                        $.each(data.Year, function (index, va) {
                            $.grep(item[i].UpperValue, function (lue) {
                                if (lue.Year == va) {
                                    lengItemSensitivityUpper += 1;
                                    html += '<td class="td-kiw-table text-right" currency-formating>' + numeral(lue.Value).format('0,0.00') + '</td>'
                                }
                            });
                        });
                        //empty value by year handling
                        if (lengItemSensitivityUpper != yearLength) {
                            const differenceLength = yearLength - lengItemSensitivityUpper;
                            if (differenceLength != 0) {
                                for (let d = 0; d < differenceLength; d++) {
                                    html += '<td class="td-kiw-table text-right" currency-formating>-</td>'
                                }
                            }
                            lengItemSensitivityUpper = 0;
                        }
                        html += '</tr>'

                        html += '<tr class="row-mat">'
                        html += '<td class="td-kiw-table">Middle</td>'

                        let lengItemSensitivityMidle = 0;
                        $.each(data.Year, function (index, va) {
                            $.grep(item[i].MidValue, function (lue) {
                                if (lue.Year == va) {
                                    lengItemSensitivityMidle += 1;
                                    html += '<td class="td-kiw-table text-right" currency-formating>' + numeral(lue.Value).format('0,0.00') + '</td>'
                                }
                            });
                        });

                        //empty value by year handling
                        if (lengItemSensitivityMidle != yearLength) {
                            const differenceLength = yearLength - lengItemSensitivityMidle;
                            if (differenceLength != 0) {
                                for (let d = 0; d < differenceLength; d++) {
                                    html += '<td class="td-kiw-table text-right" currency-formating>-</td>'
                                }
                            }
                            lengItemSensitivityMidle = 0;
                        }
                        html += '</tr>'

                        html += '<tr class="row-mat">'
                        html += '<td class="td-kiw-table">Lower</td>'

                        let lengItemSensitivityLower = 0;
                        $.each(data.Year, function (index, va) {
                            $.grep(item[i].LowerValue, function (lue) {
                                if (lue.Year == va) {
                                    lengItemSensitivityLower += 1;
                                    html += '<td class="td-kiw-table text-right" currency-formating>' + numeral(lue.Value).format('0,0.00') + '</td>'
                                }
                            });
                        });
                        //empty value by year handling
                        if (lengItemSensitivityLower != yearLength) {
                            const differenceLength = yearLength - lengItemSensitivityLower;
                            if (differenceLength != 0) {
                                for (let d = 0; d < differenceLength; d++) {
                                    html += '<td class="td-kiw-table text-right" currency-formating>-</td>'
                                }
                            }
                            lengItemSensitivityLower = 0;
                        }
                        html += '</tr>'
                    }
                }
                html += '</tr>'
            });
            html += '</table>'
            html += '</div>'
            html += '</div>'
            //End of Sensitiviy

            //Stress Testing
            html += '<div class="margintop-5 text-bold-italic">Stress Testing</div>'

            //Risk Capital
            html += '<div class="margintop-5 text-italic">Risk Capital</div>'
            html += '<div class="marginbottom-20 scrollable-matrix">'
            html += '<div class="table-mat">'
            html += '<div class="heading-mat">'
            html += '<div class="cell-mat header-column"><p>Scenario</p></div>'
            $.each(data.Year, function (index, va) {
                html += '<div class="cell-mat header-column">'
                html += '<p class="header-column-position">' + va + '</p>'
                html += '</div>'
            });
            html += '</div>'

            $.each(data.StressTesting.RiskCapital, function (i, value) {
                html += '<div class="row-mat">'
                html += '<div class="cell-mat"><p>' + value.NamaScenario + '</p></div>'
                $.each(value.ValuePerYear, function (index, val) {
                    html += '<div class="cell-mat text-right" data-cell-risk="' + val.Year + '">'
                    html += '<div class="text-right"><p currency-formating>' + numeral(val.Value).format('0,0.00') + '</p></div>'
                    html += '</div>'
                });
                html += '</div>'
            });
            html += '</div>'
            html += '</div>'
            //End Of Risk Capital

            //Available Capital
            html += '<div class="margintop-5 text-italic">Available Capital</div>'
            html += '<div class="marginbottom-20 scrollable-matrix">'
            html += '<div class="table-mat">'
            html += '<div class="heading-mat">'
            html += '<div class="cell-mat header-column"><p>Scenario</p></div>'
            $.each(data.Year, function (index, va) {
                html += '<div class="cell-mat header-column">'
                html += '<p class="header-column-position">' + va + '</p>'
                html += '</div>'
            });
            html += '</div>'

            $.each(data.StressTesting.AvailableCapital, function (i, value) {
                html += '<div class="row-mat">'
                //html += '<div class="cell-mat"><p>Available Capital</p></div>'
                html += '<div class="cell-mat"><p>' + value.NamaScenario + '</p></div>'

                $.each(value.ValuePerYear, function (index, val) {
                    html += '<div class="cell-mat text-right" data-cell-risk="' + val.Year + '">'
                    html += '<div><p currency-formating>' + numeral(val.Value).format('0,0.00') + '</p></div>'
                    html += '</div>'
                });

                html += '</div>'
            });
            html += '</div>'
            html += '</div>'
            //End Of Available Capital

            //Total Liquidity
            html += '<div class="margintop-5 text-italic">Total Liquidity</div>'
            html += '<div class="marginbottom-20 scrollable-matrix">'
            html += '<div class="table-mat">'
            html += '<div class="heading-mat">'
            html += '<div class="cell-mat header-column"><p>Scenario</p></div>'
            $.each(data.Year, function (index, va) {
                html += '<div class="cell-mat header-column">'
                html += '<p class="header-column-position">' + va + '</p>'
                html += '</div>'
            });
            html += '</div>'

            $.each(data.StressTesting.TotalLiquidity, function (i, value) {
                html += '<div class="row-mat">'
                //html += '<div class="cell-mat"><p>Total Liquidity</p></div>'
                html += '<div class="cell-mat"><p>' + value.NamaScenario + '</p></div>'


                $.each(value.ValuePerYear, function (index, val) {
                    html += '<div class="cell-mat text-right" data-cell-risk="' + val.Year + '">'
                    html += '<div><p currency-formating>' + numeral(val.Value).format('0,0.00') + '</p></div>'
                    html += '</div>'
                });

                html += '</div>'
            });
            html += '</div>'
            html += '</div>'
            //End Of Total Liquidity

            //end of Stress Testing

            html += '</div>'

            self.$('[scenario-testing-container]').append(html);
        },
        renderLiquidity: function (assetClassDefault, data, total) {
            var self = this;
            var html = '<div class="col-md-12">'
            html += '<div class="margintop-5 text-bold-italic"></div>'
            html += '<div class="marginbottom-20 scrollable-matrix">'
            html += '<div class="table-mat">'
            html += '<div class="heading-mat">'
            html += '<div class="cell-mat header-column"><p>Asset Class</p></div>'
            html += '<div class="cell-mat header-column"><p>Proportion</p></div>'
            html += '<div class="cell-mat header-column"><p>Term</p></div>'
            html += '<div class="cell-mat header-column"><p>Assumed Return</p></div>'
            html += '<div class="cell-mat header-column"><p>Outstanding Years</p></div>'
            html += '<div class="cell-mat header-column"><p>Asset Value</p></div>'
            $.each(data, function (index, va) {
                html += '<div class="cell-mat header-column" data-heading-risk="' + va.Year + '">'
                html += '<p class="header-column-position">' + va.Year + '</p>'
                html += '</div>'
            });
            html += '</div>'

            $.each(assetClassDefault, function (i, value) {
                html += '<div class="row-mat" data-risk-row="' + value.AssetClass + '">'
                html += '<div class="cell-mat"><p>' + value.AssetClass + '</p></div>'
                html += '<div class="cell-mat text-right"><p>' + numeral(value.Proportion).format('0,0.00') + ' %</p></div>'
                html += '<div class="cell-mat text-right"><p>' + value.TermAwal + ' - ' + value.TermAkhir + '</p></div>'
                html += '<div class="cell-mat text-right"><p>' + value.AssumedReturn + ' %</p></div>'
                html += '<div class="cell-mat text-right"><p>' + value.OutstandingYearAwal + ' - ' + value.OutstandingYearAkhir + '</p></div>'
                html += '<div class="cell-mat text-right"><p>' + numeral(value.AssetValue).format('0,0.00') + '</p></div>'

                $.each(data, function (index, val) {
                    html += '<div class="cell-mat text-right" data-cell-risk="' + val.Year + '">'
                    $.grep(val.AssetList, function (lue) {
                        if (lue.AssetClass == value.AssetClass) {
                            html += '<div><p currency-formating>' + numeral(lue.Value).format('0,0.00') + '</p></div>'
                            html += '</div>'
                        }
                    });
                });
                html += '</div>'
            });

            //total
            html += '<div class="cell-mat text-right total-value-matrix"><p></p></div>'
            html += '<div class="cell-mat text-right total-value-matrix"><p></p></div>'
            html += '<div class="cell-mat text-right total-value-matrix"><p></p></div>'
            html += '<div class="cell-mat text-right total-value-matrix"><p></p></div>'
            html += '<div class="cell-mat text-right total-value-matrix"><p>Liquid Assets</p></div>'
            html += '<div class="cell-mat text-right total-value-matrix"><p></p></div>'
            $.each(total, function (i, value) {
                html += '<div class="cell-mat text-right total-value-matrix"><p currency-formating>' + numeral(value.Value).format('0,0.00') + '</p></div>'
            });

            html += '</div>'
            html += '</div>'
            //end
            html += '</div>'

            self.$('[liquidity-container]').append(html);
        },
        openAdjustAvailable: function () {
            let self = this;
            let data = [];
            if (this.availableCapitalYear.length != this.availableCapitalValue.length) {
                commonFunction.responseWarningCannotExecute("Jumlah data list tahun dan nilai Available Capital tidak sama.");
            } else {
                for (let i = 0; i < this.availableCapitalYear.length; i++) {
                    const year = this.availableCapitalYear[i];
                    const capitalValue = this.availableCapitalValue[i];
                    let dataDetail = {};
                    dataDetail.Year = year;
                    dataDetail.Value = capitalValue;
                    data.push(dataDetail);
                }
                require(['./adjust_available_capital/view'], function (View) {
                    commonFunction.setDefaultModalDialogFunction(self, View, data);
                });
            }
        },
        setRoleAccess: function () {
            let self = this;
            if (this.roles) {
                $.grep(this.roles, function (item) {
                    if (item.Name.trim() == "Dashboards") {
                        if (item.SubMenu) {
                            $.grep(item.SubMenu, function (val) {
                                if (val.Name.trim() == "Dashboard-Kalkulasi") {
                                    if (val.SubMenu) {
                                        _.each(val.SubMenu, function (item) {
                                            switch (item.Name) {
                                                case "Dashboard-Kalkulasi AggregationProject":
                                                    self.$('[dashboard-aggregation-project]').removeClass('hide');
                                                    break;
                                                case "Dashboard-Kalkulasi AggregationSector":
                                                    self.$('[dashboard-aggregation-sector]').removeClass('hide');
                                                    break;
                                                case "Dashboard-Kalkulasi AggregationRisk":
                                                    self.$('[dashboard-aggregation-risk]').removeClass('hide');
                                                    break;
                                                case "Dashboard-Kalkulasi AssetProjection":
                                                    self.$('[dashboard-asset-projection]').removeClass('hide');
                                                    break;
                                                case "Dashboard-Kalkulasi LiquidityKalkulasi":
                                                    self.$('[dashboard-liquidity]').removeClass('hide');
                                                    break;
                                                case "Dashboard-Kalkulasi ProjectDetail":
                                                    self.$('[dashboard-project-detail]').removeClass('hide');
                                                    break;
                                                case "Dashboard-Kalkulasi ScenarioTesting":
                                                    self.$('[dashboard-scenario-testing]').removeClass('hide');
                                                    break;
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });

            }
        }
    });
});