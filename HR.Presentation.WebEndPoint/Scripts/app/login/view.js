// login/app/login
define(function (require, exports, module) {
    'use strict';
    debugger;
    var LayoutManager = require('layoutmanager');
    var commonConfig = require('commonconfig');
    var commonFunction = require('commonfunction');
    var Model = require('./model');
    var ModelRole = require('../user_management/setting_menu/model');
    var template = require('text!./template.html');
    var templateTitle = require('text!./title.html');
    var templateContent = require('text!./content.html');
    var Cookies = require('Cookies');

    module.exports = LayoutManager.extend({
        className: 'page-container-login',
        template: _.template(template),
        initialize: function () {
            this.keyword = "";
            this.model = new Model();
            this.modelRole = new ModelRole();
            this.listenTo(this.model, 'request', function () {
                this.doLoadingDisabledButtonLogin();
                this.doHideErrorMessage();
            });
            this.listenTo(this.model, 'sync error', function () {
                this.doEnabledButtonLogin();
            });
            this.listenTo(this.model, 'sync', function () {
                localStorage.setItem('roles', this.model.get('menuList'));
                this.doHideErrorMessage();
                document.cookie = "Authorization=" + 'bearer ' + this.model.get('access_token') + "" + ";domain=" + commonConfig.requestDomain + ";path=/";
                document.cookie = "roleName=" + this.model.get('role') + "" + ";domain=" + commonConfig.requestDomain + ";path=/";
                document.cookie = "userName=" + this.model.get('username') + "" + ";domain=" + commonConfig.requestDomain + ";path=/";
                document.cookie = "roleId=" + this.model.get('id') + "" + "; domain = " + commonConfig.requestDomain + "; path = /";

                window.Router.navigate('dashboard/main_dashboard', {
                    trigger: true
                });
            });
            this.listenTo(this.model, 'error', function (xhr, text) {
                this.doShowErrorMessage(text);
            });

        },
        serialize: function () {
            return {
                title: templateTitle,
                content: templateContent
            }
        },
        fetchRoleAccess: function () {
            var self = this;
            var param = [];
            param.Search = this.keyword;
            this.modelRole.fetch({
                reset: true,
                data: param,
                success: function (req, res) {
                    console.log(res);
                }
            });
        },
        events: {
            'submit form': 'doSubmit',
            'click [obo-doReset]': 'doReset'
        },
        doSubmit: function () {
            this.model.save({
                username: this.$('[name="username"]').val(),
                password: this.$('[name="password"]').val()
            });
            return false;
        },
        doHideErrorMessage: function () {
            this.$('[obo-errormessage]').hide();
        },
        doShowErrorMessage: function (text) {
            this.$('[obo-name="errorMessage"]').html((text.responseJSON && text.responseJSON.error_description) || 'something went wrong on server');
            this.$('[obo-errormessage]').show();
            this.$('[obo-contain]').show(); 
        },
        doLoadingDisabledButtonLogin: function () {
            this.$('[obo-dologin]').attr('disabled', 'disabled').text('Loading...');
        },
        doEnabledButtonLogin: function () {
            this.$('[obo-dologin]').removeAttr('disabled').text('Login');
        }
    });
});