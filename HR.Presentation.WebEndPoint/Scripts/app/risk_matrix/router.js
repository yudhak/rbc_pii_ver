define(function (require, exports, module) {
  'use strict';
  var Subroute = require('backbone.subroute');
  var commonFunction = require('commonfunction');
  var eventAggregator = require('eventaggregator');

  var fnSetContentView = function (pathViewFile) {
    var hashtag = '#overall_comment';
    require([pathViewFile + '/view'], function (View) {
      if (View)
        commonFunction.setContentViewWithNewModuleView(new View(), hashtag);
    });
  };

  module.exports = Backbone.SubRoute.extend({
    initialize: function () {
      this.app = {};
    },
    routes: {
      '/': 'showList',
      ':id': 'showDetail',
      'edit_detail/:id': 'showEditDetail',
      'edit_ecek/ecek/:id': 'showEditEcek',
    },
    showList: function () {
      fnSetContentView('.');
    },
    showDetail: function () {
      fnSetContentView('./detail');
    },
    showEditDetail: function () {
      fnSetContentView('./edit_detail');
    },
    showEditEcek: function () {
      fnSetContentView('./edit_ecek');
    }
  });
});