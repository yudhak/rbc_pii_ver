define(function (require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./../detail/template.html');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');
    var Table = require('./table/table');
    var Paging = require('paging');
    var Model = require('./../model');
    var TableLikelihood = require('./likelihood/detail/table/table');
    var CollectionLikelihood = require('./likelihood/detail/collection');
    var CollectionStage = require('./../../master/stage/collection');
    var ModelStageTahunRiskMatrix = require('./../edit/model');
    var ModelRisk = require('./../../master/risk/model');
    var CollectionRisk = require('./../../master/risk/collection');
    var ModelStageTahunRiskMatrixDetail = require('./model');
    require('maskingcustom');
    require('jquerymask');

    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content tbl bg-white',
        template: _.template(template),
        initialize: function (options) {
            var self = this;
            this.keyword = "";
            var parentId = commonFunction.getUrlHashSplit(2);
            this.parentId = parentId;
            this.modelStageTahunRiskMatrixDetail = new ModelStageTahunRiskMatrixDetail();
            //this.modelStageTahunRiskMatrixDetail.set(this.modelStageTahunRiskMatrixDetail.idAttribute, this.parentId);
            this.model = new Model();
            this.modelStageTahunRiskMatrix = new ModelStageTahunRiskMatrix();
            this.modelStageTahunRiskMatrix.set(this.modelStageTahunRiskMatrix.idAttribute, this.parentId);
            this.collectionStage = new CollectionStage();

            this.tableLikelihood = new TableLikelihood({
                collection: new CollectionLikelihood()
            });
            this.collectionRisk = new CollectionRisk({
                collection: new CollectionRisk()
            });
            this.listenTo(this.model, 'sync', () => {
                this.render();
            });

            this.listenToOnce(this.modelStageTahunRiskMatrixDetail, 'sync', function (model) {
                // commonFunction.closeLoadingSpinner();
                this.listenTo(this.modelStageTahunRiskMatrixDetail, 'sync', function (model) {
                    commonFunction.responseSuccessUpdateAddDelete('Risk Matrix Berhasil Disimpan.');
                });
            }, this);

            this.modelStageTahunRiskMatrixDetail.fetch({
                reset: true,
                data: {
                    id: this.parentId
                },
                success: (req, res) => {
                    self.statusId = res.StatusId;
                }
            });

            this.modelStageTahunRiskMatrix.fetch({
                reset: true,
                data: {
                    id: this.parentId
                }
            });

            this.collectionStage.fetch({
                data: {
                    Search: this.keyword
                },
                success: function (collection, response) {
                    _.each(collection.models, function (model) { })
                }
            });

            this.once('afterRender', () => {
                this.model.set(this.model.idAttribute, this.parentId);
                this.model.fetch();

                this.table.collection.fetch({
                    reset: true,
                    data: {
                        ParentId: this.parentId,
                        Search: this.keyword
                    }
                });
            });

            this.table = new Table();
            commonFunction.showLoadingSpinner();
            this.roles = JSON.parse(localStorage.getItem('roles'));
        },
        afterRender: function () {
            if (!this.model.id) {
                return;
            }
            this.formStage();
            this.renderLikelihood();
            var likelihoodId = this.model && this.model.attributes && this.model.attributes.Scenario.LikehoodId;
            this.renderRiskCategory();
            this.renderStage();
            this.setTemplate();
            this.setTemplateStage();
            this.renderCurrencyFormat();
            commonFunction.closeLoadingSpinner();
            this.setRoleAccess();
        },
        events: {
            'click [btn-save]': 'getConfirmation',
            'change [is-maximum-exposure]': 'setMaximumExposure',
            'change [for-validation]': 'currencyValidation',
            'change [likelihood-value]': 'likelihoodValidation',
            'click [name="IsSend"]': 'submitNotification',
            'click [name="EditDetail"]': 'showEditDetail'
        },
        submitNotification: function () {
            var isSend = this.$('[name="IsSend"]').prop('checked');
            if (isSend) {
                commonFunction.responseWarningCannotExecute("Skenario otomatis akan dilanjutkan ke proses persetujuan / approval jika menekan tombol Simpan. Sehingga Skenario ini tidak bisa diubah lagi.");
            } else {
                commonFunction.responseWarningCannotExecute("Skenario membutuhkan pengajuan persetujuan terpisah setelah menekan tombol Simpan.");
            }
        },
        showDetail: function () {
            var riskMatrixProjectId = commonFunction.getUrlHashSplit(2);
            var url = "#risk_matrix/" + riskMatrixProjectId;
            window.location.href = url;
        },
        showEditDetail: function () {
            var riskMatrixProjectId = commonFunction.getUrlHashSplit(2);
            var url = "#risk_matrix/edit_detail/" + riskMatrixProjectId;
            window.location.href = url;
        },
        renderCurrencyFormat: function () {
            this.$('[currency-formating]').mask('000,000,000,000,000.00', {
                reverse: true
            });
        },
        renderStage: function () {
            var self = this;
            var project = this.model.attributes.Project.NamaProject;
            var startProject = this.model.attributes.Project.TahunAwalProject;
            var endProject = this.model.attributes.Project.TahunAkhirProject;
            this.$('[name="NamaProject"]').val(project);
            this.$('[name="TahunAwalProject"]').val(startProject);
            this.$('[name="TahunAkhirProject"]').val(endProject);
        },
        renderLikelihood: function () {
            var likelihoodId = this.model.attributes.Scenario.LikehoodId;
            if (likelihoodId) {
                this.$('[obo-table-likelihood]').append(this.tableLikelihood.el);
                this.tableLikelihood.render();
                this.tableLikelihood.collection.fetch({
                    reset: true,
                    data: {
                        ParentId: likelihoodId,
                        PageSize: 100
                    }
                });
            }
        },
        renderRiskCategory: function () {
            this.collectionRisk.fetch({
                reset: true,
                data: {
                    IsPagination: false
                }
            });
        },
        setTemplateStage: function () {
            var self = this;
            var data = this.modelStageTahunRiskMatrix.attributes.StageValue;
            if (data) {
                $.each(data, function (index, item) {
                    var stageId = item.StageId;
                    self.$('[name="data-start-stage-' + stageId + '"]').text(item.Values[0]);
                    self.$('[name="data-end-stage-' + stageId + '"]').text(item.Values[1]);
                });
            }
        },
        setTemplate: function () {
            var self = this;
            var data = this.modelStageTahunRiskMatrixDetail.attributes.RiskMatrixCollection;
            if (data != null) {
                if (data.length > 0) {
                    $.each(data, function (index, value) {
                        var yearId = value.StageTahunRiskMatrixId;
                        $.each(value.RiskMatrixValue, function (index, item) {
                            var riskId = item.RiskRegistrasiId;
                            var exp = parseFloat(item.Values[0]).toFixed(2);
                            var likehood = item.Values[1];
                            self.$('[data-risk="' + riskId + '-ForYear-' + yearId + '"]').val(exp);
                            self.$('[name="DefinisiLikehoodFor-' + yearId + '-RiskFor-' + riskId + '"]').val(likehood);

                            // set color when the value of exposure was changed
                            if (item.Colors[0]) {
                                self.$('[data-risk="' + riskId + '-ForYear-' + yearId + '"], [td-data-risk="' + riskId + '-ForYear-' + yearId + '"]').css('background-color', 'yellow');
                                self.$('[data-risk="' + riskId + '-ForYear-' + yearId + '"], [td-data-risk="' + riskId + '-ForYear-' + yearId + '"]').css('color', 'black');
                            }
                            // set color when the value of likelihood was changed
                            if (item.Colors[1]) {
                                self.$('[td-data-likelihood="likelihood-' + yearId + '-risk-' + riskId + '"], [name="DefinisiLikehoodFor-' + yearId + '-RiskFor-' + riskId + '"]').css('background-color', 'yellow');
                                self.$('[td-data-likelihood="likelihood-' + yearId + '-risk-' + riskId + '"], [name="DefinisiLikehoodFor-' + yearId + '-RiskFor-' + riskId + '"]').css('color', 'black');
                            }
                        });
                        self.$('#max-exposure-' + yearId + '').val(parseFloat(value.MaximumNilaiExpose).toFixed(2));
                        if (value.Warna) {
                            self.$('[lang="max-exposure-' + yearId + '"]').css('background-color', 'yellow');
                            self.$('#max-exposure-' + yearId).css('background-color', 'yellow');
                            self.$('#max-exposure-' + yearId).css('color', 'black');
                        }
                    });
                }
            }

            // set status
            let html = '';
            if (this.modelStageTahunRiskMatrixDetail.attributes.StatusId == 2) {
                html = '<span class="label label-success">Disetujui</span>';
                this.$('[kw-status]').append(html);
            } else if (this.modelStageTahunRiskMatrixDetail.attributes.StatusId == null) {
                html = '<span class="label label-default">Draft</span>';
                this.$('[kw-status]').append(html);
            } else if (this.modelStageTahunRiskMatrixDetail.attributes.StatusId == 3) {
                html = '<span class="label label-danger">Tidak Disetujui</span>';
                this.$('[kw-status]').append(html);
            } else {
                html = '<span class="label label-warning">Proses Approval</span>';
                this.$('[kw-status]').append(html);
            }
        },
        getRiskRegistrasi: function () {
            var riskRegistrasi = [];
            this.$('[data-risk-registrasi-id]').each(function () {
                var val = this.value;
                riskRegistrasi.push(val);
            });
            return riskRegistrasi;
        },
        getStageTahunRiskMatrix: function () {
            var years = [];
            this.$('[data-year-id]').each(function () {
                var val = this.innerText;
                var id = val.trim();
                years.push(id);
            });
            return years;
        },
        setMaximumExposure: function (dom) {
            var self = this;
            var yearId = dom.currentTarget.lang;
            var tempValue = [];
            this.$("[maximum-exposure-for='" + yearId + "']").each(function (i, item) {
                tempValue.push(parseFloat(item.value.replace(/,/g, '')));
            });
            var maxValue = Math.max.apply(Math, tempValue);
            this.$('#max-exposure-' + yearId + '').val(maxValue);
            this.renderCurrencyFormat();
        },
        formStage: function (data) {
            var self = this;
            if (this.collectionStage) {
                for (var i = 0; i < this.collectionStage.length; i++) {
                    var namaStage = this.collectionStage.models[i].attributes.NamaStage;
                    var idStage = this.collectionStage.models[i].attributes.Id;
                    var html = '<tr>'
                    html += '<td style="width: 10%;">'
                    html += '<label>' + namaStage + '</label>'
                    html += '</td>'
                    html += '<td style="width: 10%;">'
                    html += '<label>=</label>'
                    html += '</td>'
                    html += '<td style="width: 10%;">'
                    html += '<label name="data-start-stage-' + idStage + '"></label>'
                    html += '</td>'
                    html += '<td style="width: 10%;">'
                    html += '<label>s/d</label>'
                    html += '</td>'
                    html += '<td style="width: 10%;">'
                    html += '<label name="data-end-stage-' + idStage + '"></label>'
                    html += '</td>'
                    html += '</tr>'
                    self.$('[tab-content]').append(html);
                }
            }
        },
        likelihoodValidation: function (e) {
            var attrValue = self.$('[name="DefinisiLikehoodFor-' + e.target.lang + '-RiskFor-' + e.target.id + '"]').val();
            if (attrValue != "" || attrValue != null) {
                this.$('[name="DefinisiLikehoodFor-' + e.target.lang + '-RiskFor-' + e.target.id + '"]').removeClass('invalidCurrencyFormat');
            } else {
                commonFunction.responseWarningCannotExecute("Likelihood wajib diisi.");
                this.$('[name="DefinisiLikehoodFor-' + e.target.lang + '-RiskFor-' + e.target.id + '"]').addClass('invalidCurrencyFormat');
            }
        },
        currencyValidation: function (e) {
            var self = this;
            var attrName = e.target.name;
            var attrValue = this.$('[name="' + attrName + '"]').val();
            if (attrValue != "") {
                this.$('[name="' + attrName + '"]').removeClass('invalidCurrencyFormat');
            } else {
                commonFunction.responseWarningCannotExecute("Nilai Expose wajib diisi.");
                this.$('[name="' + attrName + '"]').addClass('invalidCurrencyFormat');
            }
        },
        getConfirmation: function (dom) {
            var self = this;
            //validate Maximum Nilai Expose
            var isValidated = true;
            this.$('[exposure-value]').each(function (i, item) {
                if (item.value == "") {
                    isValidated = false;
                    commonFunction.responseWarningCannotExecute("Nilai Expose wajib diisi.");
                    self.$('[name="' + item.name + '"]').addClass('invalidCurrencyFormat');
                } else {
                    self.$('[name="' + item.name + '"]').removeClass('invalidCurrencyFormat');
                }
            });

            //Validate Likelihood Detail
            this.$('[likelihood-value]').each(function (i, item) {
                var data = self.$('[name="DefinisiLikehoodFor-' + item.lang + '-RiskFor-' + item.id + '"]').val();
                if (data == null || data == "") {
                    isValidated = false;
                    commonFunction.responseWarningCannotExecute("Likelihood wajib diisi.");
                    self.$('[name="DefinisiLikehoodFor-' + item.lang + '-RiskFor-' + item.id + '"]').addClass('invalidCurrencyFormat');
                } else {
                    self.$('[name="DefinisiLikehoodFor-' + item.lang + '-RiskFor-' + item.id + '"]').removeClass('invalidCurrencyFormat');
                }
            });

            if (isValidated)
                this.confirmed();
        },
        confirmed: function () {
            var self = this;
            //validate all input type that fillable
            var isValidated = true;

            if (isValidated) {
                var action = "save";
                var retVal = confirm("Are you sure want to " + action + " this data ?");
                if (retVal == true) {
                    this.saveRiskMatrix();
                } else {
                    this.$('[btn-save]').attr('disabled', false);
                }
            } else {
                commonFunction.exposureRiskMatrixValidation('SALAHHH');
            }
        },
        reCalculateMaximuExpose: function (year) {
            var self = this;
            var tempValue = [];
            this.$("[maximum-exposure-for='" + year + "']").each(function (i, item) {
                tempValue.push(parseFloat(item.value.replace(/,/g, '')));
            });
            var maxValue = Math.max.apply(Math, tempValue);
            return maxValue;
        },
        saveRiskMatrix: function () {
            commonFunction.showLoadingSpinner();
            var self = this;
            var stageTahunRiskMatrixIds = this.getStageTahunRiskMatrix();
            var riskRegistrasi = this.getRiskRegistrasi();
            var data = {};
            var riskMatrixCollection = [];
            for (var i = 0; i < stageTahunRiskMatrixIds.length; i++) {
                var stageTahunRiskMatrix = {};
                var year = stageTahunRiskMatrixIds[i];
                var riskMatrixValue = [];

                stageTahunRiskMatrix.StageTahunRiskMatrixId = year;
                for (var e = 0; e < riskRegistrasi.length; e++) {
                    var riskMatrixValueItem = {};
                    var riskId = riskRegistrasi[e];
                    riskMatrixValueItem.RiskRegistrasiId = riskId;

                    var value = [];
                    var exposureValue = self.$('[data-risk="' + riskId + '-ForYear-' + year + '"]').val();
                    var exposureValueFormated = parseFloat(exposureValue.replace(/,/g, ''));
                    value.push(exposureValueFormated);
                    var likelihoodDetailId = self.$('[name="DefinisiLikehoodFor-' + year + '-RiskFor-' + riskId + '"]').val();
                    value.push(likelihoodDetailId);

                    riskMatrixValueItem.Values = value;

                    riskMatrixValue.push(riskMatrixValueItem);
                }
                stageTahunRiskMatrix.RiskMatrixValue = riskMatrixValue;
                var maximumExposePerYear = this.$('#max-exposure-' + year + '').val();
                if (maximumExposePerYear != "") {
                    var maxValue = this.$('#max-exposure-' + year + '').val();
                    var maxValueFormated = parseFloat(maxValue.replace(/,/g, ''));
                    stageTahunRiskMatrix.MaximumNilaiExpose = maxValueFormated;
                } else {
                    var maxValue = self.reCalculateMaximuExpose(year);
                    stageTahunRiskMatrix.MaximumNilaiExpose = maxValue;
                }
                riskMatrixCollection.push(stageTahunRiskMatrix);
            }
            data.RiskMatrixCollection = riskMatrixCollection;
            data.Id = this.parentId;
            data.IsSend = this.$('[name="IsSend"]').prop('checked');
            this.modelStageTahunRiskMatrixDetail.save(data);
        },
        setRoleAccess: function () {
            let self = this;
            if (this.roles) {
                $.grep(this.roles, function (item) {
                    if (item.Name.trim() == "RisikoMatriks") {
                        if (item.SubMenu) {
                            _.each(item.SubMenu, function (val) {
                                switch (val.Action) {
                                    case "detail":
                                        if (val.SubMenu) {
                                            _.each(val.SubMenu, function (item) {
                                                switch (item.Action) {
                                                    case "update":
                                                        if (self.statusId != 1)
                                                            self.$('[name="EditDetail"]').removeClass('hide');
                                                        break;
                                                }
                                            });
                                        }
                                        break;
                                }
                            });
                        }
                    }
                });
            }
        }
    });
});