define(function(require, exports, module) {
    'use strict';
    var View = require('modaldialogdefault');
    var template = require('text!./template.html');
    var Model = require('./../model');
    var ModelStageTahunRiskMatrix = require('./../edit/model');
    var CollectionStage = require('./../../master/stage/collection');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var eventAggregator = require('eventaggregator');
    var TableRisk = require('./../tabel_risk/table/table');
    var CollectionRisk = require('./../tabel_risk/collection');
    require('bootstrap-validator');

    module.exports = View.extend({
        template: _.template(template),
        initialize : function(){
            var self = this;
            this.keyword = "";
            var correlationId = this.model.get('Id');
            this.riskMatrixProjectId = this.model.get('Id');
            this.model = new Model();
            this.modelStageTahunRiskMatrix = new ModelStageTahunRiskMatrix();
            this.collectionStage = new CollectionStage();
            this.tableRisk = new TableRisk({
                    collection: new CollectionRisk()
                });
            this.model.set(this.model.idAttribute, correlationId);
            this.stageValue = {};
            this.dataStage = [];
            this.listenToOnce(this.model, 'sync', function(model) {
                this.render();
            }, this);
            this.listenToOnce(this.modelStageTahunRiskMatrix, 'sync', function(model) {
                self.$el.modal('hide');
                Router.navigate("/risk_matrix/edit_detail/" + this.riskMatrixProjectId, { trigger: true });
            }, this);
            this.once('afterRender', function() {
                this.model.fetch();
            });

            var param  = [];
            param.Search = this.keyword;
            this.collectionStage.fetch({
                reset:true,
                data: param
            });
        },
        afterRender : function(){
          var self = this;
          this.formStage();
          var stage = this.getIdStage();
            for (var e = 0; e < stage.length; e++) {
                var stageId = stage[e];
                this.$('[name="data-start-stage-'+ stageId +'"]').datetimepicker({defaultDate: null,format: commonConfig.datePickerYearFormat});
                this.$('[name="data-end-stage-'+ stageId +'"]').datetimepicker({defaultDate: null,format: commonConfig.datePickerYearFormat});
                this.$('[name="data-start-stage-'+ stageId +'"]').attr("idstart",[e]);
                this.$('[name="data-end-stage-'+ stageId +'"]').attr("idend",[e]);
            }
            this.renderRisk();
        },
        events: {
          'dp.change [data-start-stage]': 'validatorTahun',
          'click [btn-save]': 'getConfirmation',
        },
        validatorTahun: function() {
        },
        formStage: function(data) {
          var self = this;
            if(this.collectionStage){
              for (var i = 0; i < this.collectionStage.length; i++){
                var namaStage = this.collectionStage.models[i].attributes.NamaStage;
                var idStage = this.collectionStage.models[i].attributes.Id;
                var html = '<div class="form-group">'
                html += '<label class="col-md-4 control-label">'+ namaStage +'</label>'
                html += '<div class="col-md-4">'
                html += '<input type="text" class="form-control datepicker" data-start-stage value="" name="data-start-stage-'+ idStage +'">'
                html += '</div>'
                html += '<div class="col-md-4">'
                html += '<input type="text" class="form-control datepicker" data-end-stage value="" name="data-end-stage-'+ idStage +'">'
                html += '<label data-id-stage class="hidden">'+ idStage +'</label>'
                html += '</div>'
                html += '</div>'
                self.$('[tab-content]').append(html);
              }
            }
        },

        getIdStage: function(){
            var idStage = [];
            this.$('[data-id-stage]').each(function(){
                var val = this.innerText;
                idStage.push(val);
            });
            return idStage;
        },
        renderValidation: function() {
            var self = this;
            this.$('[ehs-form]').bootstrapValidator({
                fields: {
                    
                }
            })
            .on('success.form.bv', function(e) {
                e.preventDefault();
                self.getConfirmation();
            });
        },
        renderRisk: function(){
            this.$('[obo-table-risk]').append(this.tableRisk.el);
            this.tableRisk.render();
            this.tableRisk.collection.fetch({
                reset: true,
                data: {
                    PageSize: 100
                }
            });
        },
        stageValueValidation: function () {
            var isValid = false;
            var stage = this.getIdStage();
            
            var firstStageId = stage[0];
            let secondStageId = stage[1];
            var lastStageId = stage[stage.length - 1];

            let endOfFirstStage = new Date(this.$('[name="data-end-stage-'+ firstStageId +'"]').val()).getFullYear();
            let startOfSecondStage = new Date(this.$('[name="data-start-stage-'+ secondStageId +'"]').val()).getFullYear();
            let endOfSecondStage = new Date(this.$('[name="data-end-stage-'+ secondStageId +'"]').val()).getFullYear();
            let startOfLastStage = new Date(this.$('[name="data-start-stage-'+ lastStageId +'"]').val()).getFullYear();
            let endOfLastStage = new Date(this.$('[name="data-end-stage-'+ lastStageId +'"]').val()).getFullYear();

            var startYearString = this.$('[name="StartProject"]').val();
            var endYearString = this.$('[name="EndProject"]').val();
            var startYear = new Date(startYearString).getFullYear();
            var endYear = new Date(endYearString).getFullYear();
            
            var startYearStage = this.$('[name="data-start-stage-'+ firstStageId +'"]').val();
            var endYearStage = this.$('[name="data-end-stage-'+ lastStageId +'"]').val();
            if (startYear != startYearStage || endYear != endYearStage) {
                commonFunction.responseWarningCannotExecute("Tahun awal tahapan proyek PC dan akhir tahapan OP tidak sama dengan tahun Periode Awal Proyek dan Periode Akhir Proyek.");
            } else if (startOfSecondStage - endOfFirstStage != 1) {
                commonFunction.responseWarningCannotExecute("Tahun awal tahapan CN tidak valid.");
                this.$('[name="data-end-stage-'+ firstStageId +'"]').addClass('invalidCurrencyFormat');
                this.$('[name="data-start-stage-'+ secondStageId +'"]').addClass('invalidCurrencyFormat');
            } else if (startOfLastStage - endOfSecondStage != 1) {
                commonFunction.responseWarningCannotExecute("Tahun awal tahapan OP tidak valid.");
                this.$('[name="data-end-stage-'+ secondStageId +'"]').addClass('invalidCurrencyFormat');
                this.$('[name="data-start-stage-'+ lastStageId +'"]').addClass('invalidCurrencyFormat');
            } else if (startYear > endOfFirstStage) {
                commonFunction.responseWarningCannotExecute("Tahun awal tahapan OP tidak bisa lebih besar dari tahun akhir tahapan OP.");
                this.$('[name="data-start-stage-'+ firstStageId +'"]').addClass('invalidCurrencyFormat');
                this.$('[name="data-end-stage-'+ firstStageId +'"]').addClass('invalidCurrencyFormat');
            } else if (startOfSecondStage > endOfSecondStage) {
                commonFunction.responseWarningCannotExecute("Tahun awal tahapan CN tidak bisa lebih besar dari tahun akhir tahapan CN.");
                this.$('[name="data-start-stage-'+ secondStageId +'"]').addClass('invalidCurrencyFormat');
                this.$('[name="data-end-stage-'+ secondStageId +'"]').addClass('invalidCurrencyFormat');
            } else if (startOfLastStage > endOfLastStage) {
                commonFunction.responseWarningCannotExecute("Tahun awal tahapan PC tidak bisa lebih besar dari tahun akhir tahapan PC.");
                this.$('[name="data-start-stage-'+ secondStageId +'"]').addClass('invalidCurrencyFormat');
                this.$('[name="data-end-stage-'+ secondStageId +'"]').addClass('invalidCurrencyFormat');
            } else {
                isValid = true;
                this.$('[name="data-end-stage-'+ firstStageId +'"]').removeClass('invalidCurrencyFormat');
                this.$('[name="data-start-stage-'+ secondStageId +'"]').removeClass('invalidCurrencyFormat');
                this.$('[name="data-end-stage-'+ secondStageId +'"]').removeClass('invalidCurrencyFormat');
                this.$('[name="data-start-stage-'+ lastStageId +'"]').removeClass('invalidCurrencyFormat');

                this.$('[name="data-start-stage-'+ firstStageId +'"]').removeClass('invalidCurrencyFormat');
            }
  
            return isValid;
        },
        getConfirmation: function(){
            var isValid = this.stageValueValidation();
            if (isValid) {
                var action = "membuat";
                var retVal = confirm("Apakah anda yakin untuk " + action + " tahun tahapan proyek tersebut?");
                if( retVal == true ){
                    this.saveStage();
                }
                else{
                    this.$('[btn-save"]').attr('disabled', false);
                }
            }
        },
        saveStage: function() {
            var self = this;
            var stage = this.getIdStage();
            var data = {};
            var stageValue = [];
            for (var e = 0; e < stage.length; e++) {
                var stageValueItem = {};
                var stageId = stage[e];
                stageValueItem.StageId = stageId;
                
                var value = [];
                var startStageValue = self.$('[name="data-start-stage-'+ stageId +'"]').val();
                value.push(startStageValue);
                var endStageValue = self.$('[name="data-end-stage-'+ stageId +'"]').val();
                value.push(endStageValue);

                stageValueItem.Values = value;

                stageValue.push(stageValueItem);
            }
            
            data.StageValue = stageValue;

            var startProject = self.$('[name="StartProject"]').val();
            var endProject = self.$('[name="EndProject"]').val();
            data.StartProject = startProject;
            data.EndProject = endProject;
            var riskMatrixProjectId = this.riskMatrixProjectId;
            data.RiskMatrixProjectId = riskMatrixProjectId;

            this.modelStageTahunRiskMatrix.save(data);
        }
    });
});
