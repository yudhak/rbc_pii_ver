define(function (require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./../edit_detail/template.html');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');
    var Table = require('./table/table');
    var Paging = require('paging');
    var Model = require('./../model');
    var TableLikelihood = require('./likelihood/detail/table/table');
    var CollectionLikelihood = require('./likelihood/detail/collection');
    var CollectionStage = require('./../../master/stage/collection');
    var ModelStageTahunRiskMatrix = require('./../edit/model');
    var ModelRisk = require('./../../master/risk/model');
    var CollectionRisk = require('./../../master/risk/collection');
    var ModelStageTahunRiskMatrixDetail = require('./model');
    const moment = require('moment');


    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content tbl bg-white',
        template: _.template(template),
        initialize: function (options) {
            var self = this;
            this.isSended = null;
            this.keyword = "";
            var parentId = commonFunction.getUrlHashSplit(3);
            this.parentId = parentId;
            this.modelStageTahunRiskMatrixDetail = new ModelStageTahunRiskMatrixDetail();
            this.model = new Model();
            this.modelStageTahunRiskMatrix = new ModelStageTahunRiskMatrix();
            this.modelStageTahunRiskMatrix.set(this.modelStageTahunRiskMatrix.idAttribute, this.parentId);
            this.collectionStage = new CollectionStage();

            this.tableLikelihood = new TableLikelihood({
                collection: new CollectionLikelihood()
            });
            this.collectionRisk = new CollectionRisk({
                collection: new CollectionRisk()
            });
            this.listenTo(this.model, 'sync', () => {
                this.render();
            });

            this.listenToOnce(this.modelStageTahunRiskMatrixDetail, 'sync', function (model) {
                // commonFunction.closeLoadingSpinner();
                this.listenTo(this.modelStageTahunRiskMatrixDetail, 'sync', function (model) {
                    commonFunction.responseSuccessUpdateAddDelete('Risk Matrix berhasil disimpan.');
                    Router.navigate("/risk_matrix/" + self.parentId, { trigger: true });
                });
            }, this);

            this.modelStageTahunRiskMatrixDetail.fetch({
                reset: true,
                data: {
                    id: this.parentId
                }
            });

            this.modelStageTahunRiskMatrix.fetch({
                reset: true,
                data: {
                    id: this.parentId
                }
            });

            this.collectionStage.fetch({
                data: {
                    Search: this.keyword
                },
                success: function (collection, response) {
                    _.each(collection.models, function (model) {
                    })
                }
            });

            this.once('afterRender', () => {
                this.model.set(this.model.idAttribute, this.parentId);
                this.model.fetch();

                this.table.collection.fetch({
                    reset: true,
                    data: {
                        ParentId: this.parentId,
                        Search: this.keyword
                    }
                });
            });

            this.table = new Table();
            commonFunction.showLoadingSpinner();
        },
        afterRender: function () {
            if (!this.model.id) {
                return;
            }
            this.formStage();
            this.renderLikelihood();
            var likelihoodId = this.model && this.model.attributes && this.model.attributes.Scenario.LikehoodId;
            this.renderRiskCategory();
            this.renderStage();
            this.setTemplate();
            this.setTemplateStage();
            commonFunction.closeLoadingSpinner();
        },
        events: {
            'change [is-maximum-exposure]': 'setMaximumExposure',
            'change [for-validation]': 'currencyValidation',
            'change [likelihood-value]': 'likelihoodValidation',
            'click [name="BackToDetail"]': 'showDetail',
            'change [validate-max-expose]': 'currencyValidationMaximumExposure',
            'keyup [exposure-value]': 'moveIntoNextInputField',
            'keyup [data-max-exposure-value]': 'moveIntoNextInputFieldMaxExpValue',
            'click [btnApprove]': 'sendApprove',
            'click [btnDraft]': 'sendDraft'
        },
        moveIntoNextInputField: function (e) {
            const attrId = e.target.tabIndex;
            let nextAttrId = attrId + 1;
            if (e.keyCode == 9) {
                this.$('[tabindex="' + nextAttrId + '"]').focus();
                this.$('[tabindex="' + nextAttrId + '"]').addClass('enable-current-input-field');
                this.$('[tabindex="' + attrId + '"]').removeClass('enable-current-input-field');
            }
        },
        moveIntoNextInputFieldMaxExpValue: function (e) {
            const attrMaxExpId = e.target.tabIndex;
            let nextAttrId = attrMaxExpId + 1;
            if (e.keyCode == 9) {
                this.$('[tabindex="' + nextAttrId + '"]').focus();
            }
        },
        showDetail: function () {
            var riskMatrixProjectId = commonFunction.getUrlHashSplit(3);
            var url = "#risk_matrix/" + riskMatrixProjectId;
            window.location.href = url;
        },
        renderStage: function () {
            var self = this;
            var project = this.model.attributes.Project.NamaProject;
            var startProject = this.model.attributes.Project.TahunAwalProject;
            var endProject = this.model.attributes.Project.TahunAkhirProject;
            this.$('[name="NamaProject"]').val(project);
            this.$('[name="TahunAwalProject"]').val(startProject);
            this.$('[name="TahunAkhirProject"]').val(endProject);
        },
        renderLikelihood: function () {
            var likelihoodId = this.model.attributes.Scenario.LikehoodId;
            if (likelihoodId) {
                this.$('[obo-table-likelihood]').append(this.tableLikelihood.el);
                this.tableLikelihood.render();
                this.tableLikelihood.collection.fetch({
                    reset: true,
                    data: {
                        ParentId: likelihoodId,
                        PageSize: 100
                    }
                });
            }
        },
        renderRiskCategory: function () {
            this.collectionRisk.fetch({
                reset: true,
                data: {
                    IsPagination: false
                }
            });
        },
        setTemplateStage: function () {
            var self = this;
            var data = this.modelStageTahunRiskMatrix.attributes.StageValue;
            if (data) {
                $.each(data, function (index, item) {
                    var stageId = item.StageId;
                    self.$('[name="data-start-stage-' + stageId + '"]').text(item.Values[0]);
                    self.$('[name="data-end-stage-' + stageId + '"]').text(item.Values[1]);
                });
            }
        },
        setTemplate: function () {
            var self = this;
            var data = this.modelStageTahunRiskMatrixDetail.attributes.RiskMatrixCollection;
            if (data != null) {
                if (data.length > 0) {
                    $.each(data, function (index, value) {
                        var yearId = value.StageTahunRiskMatrixId;
                        $.each(value.RiskMatrixValue, function (index, item) {
                            var riskId = item.RiskRegistrasiId;
                            var exp = item.Values[0];
                            var likehood = item.Values[1];
                            self.$('[data-risk="' + riskId + '-ForYear-' + yearId + '"]').val(exp);
                            self.$('[name="DefinisiLikehoodFor-' + yearId + '-RiskFor-' + riskId + '"]').val(likehood);

                            // set color when the value of exposure was changed
                            if (item.Colors[0]) {
                                self.$('[data-risk="' + riskId + '-ForYear-' + yearId + '"], [td-data-risk="' + riskId + '-ForYear-' + yearId + '"]').css('background-color', 'yellow');
                                self.$('[data-risk="' + riskId + '-ForYear-' + yearId + '"], [td-data-risk="' + riskId + '-ForYear-' + yearId + '"]').css('color', 'black');
                            }
                            // set color when the value of likelihood was changed
                            if (item.Colors[1]) {
                                self.$('[td-data-likelihood="likelihood-' + yearId + '-risk-' + riskId + '"], [name="DefinisiLikehoodFor-' + yearId + '-RiskFor-' + riskId + '"]').css('background-color', 'yellow');
                                self.$('[td-data-likelihood="likelihood-' + yearId + '-risk-' + riskId + '"], [name="DefinisiLikehoodFor-' + yearId + '-RiskFor-' + riskId + '"]').css('color', 'black');
                            }
                        });
                        self.$('#max-exposure-' + yearId + '').val(value.MaximumNilaiExpose);
                        if (value.Warna) {
                            self.$('[lang="max-exposure-' + yearId + '"]').css('background-color', 'yellow');
                            self.$('#max-exposure-' + yearId).css('background-color', 'yellow');
                            self.$('#max-exposure-' + yearId).css('color', 'black');
                        }
                    });
                }
            }

            // set status
            let html = '';
            if (this.modelStageTahunRiskMatrixDetail.attributes.StatusId == 2) {
                html = '<span class="label label-success">Disetujui</span>';
                this.$('[kw-status]').append(html);
            } else if (this.modelStageTahunRiskMatrixDetail.attributes.StatusId == null) {
                html = '<span class="label label-default">Draft</span>';
                this.$('[kw-status]').append(html);
            } else if (this.modelStageTahunRiskMatrixDetail.attributes.StatusId == 3) {
                html = '<span class="label label-danger">Tidak Disetujui</span>';
                this.$('[kw-status]').append(html);
            } else {
                html = '<span class="label label-warning">Proses Approval</span>';
                this.$('[kw-status]').append(html);
            }
        },
        getRiskRegistrasi: function () {
            var riskRegistrasi = [];
            this.$('[data-risk-registrasi-id]').each(function () {
                var val = this.value;
                riskRegistrasi.push(val);
            });
            return riskRegistrasi;
        },
        getStageTahunRiskMatrix: function () {
            var years = [];
            this.$('[data-year-id]').each(function () {
                var val = this.innerText;
                var id = val.trim();
                years.push(id);
            });
            return years;
        },
        setMaximumExposure: function (dom) {
            var self = this;
            var yearId = dom.currentTarget.lang;
            var tempValue = [];
            this.$("[maximum-exposure-for='" + yearId + "']").each(function (i, item) {
                tempValue.push(parseFloat(item.value.replace(/,/g, '')));
            });
            var maxValue = Math.max.apply(Math, tempValue);
            this.$('#max-exposure-' + yearId + '').val(maxValue);
        },
        formStage: function (data) {
            var self = this;
            if (this.collectionStage) {
                for (var i = 0; i < this.collectionStage.length; i++) {
                    var namaStage = this.collectionStage.models[i].attributes.NamaStage;
                    var idStage = this.collectionStage.models[i].attributes.Id;
                    var html = '<tr>'
                    html += '<td style="width: 10%;">'
                    html += '<label>' + namaStage + '</label>'
                    html += '</td>'
                    html += '<td style="width: 10%;">'
                    html += '<label>=</label>'
                    html += '</td>'
                    html += '<td style="width: 10%;">'
                    html += '<label name="data-start-stage-' + idStage + '"></label>'
                    html += '</td>'
                    html += '<td style="width: 10%;">'
                    html += '<label>s/d</label>'
                    html += '</td>'
                    html += '<td style="width: 10%;">'
                    html += '<label name="data-end-stage-' + idStage + '"></label>'
                    html += '</td>'
                    html += '</tr>'
                    self.$('[tab-content]').append(html);
                }
            }
        },
        likelihoodValidation: function (e) {
            var attrValue = self.$('[name="DefinisiLikehoodFor-' + e.target.lang + '-RiskFor-' + e.target.id + '"]').val();
            if (attrValue != "" || attrValue != null) {
                this.$('[name="DefinisiLikehoodFor-' + e.target.lang + '-RiskFor-' + e.target.id + '"]').removeClass('invalidCurrencyFormat');
            } else {
                commonFunction.responseWarningCannotExecute("Likelihood wajib diisi.");
                this.$('[name="DefinisiLikehoodFor-' + e.target.lang + '-RiskFor-' + e.target.id + '"]').addClass('invalidCurrencyFormat');
            }
        },
        currencyValidation: function (e) {
            var self = this;
            var exposeFormat = /^\s*[0-9]+([\.][0-9][0-9]{0,12})?$|^$|^\s*$/;
            var attrName = e.target.name;
            var attrValue = this.$('[name="' + attrName + '"]').val();
            if (exposeFormat.test(attrValue)) {
                this.$('[name="' + attrName + '"]').removeClass('invalidCurrencyFormat');
            } else {
                commonFunction.responseWarningCannotExecute("Nilai Expose tidak sesuai format. Format yang dizinkan 1000.02 / 123.39847. Angka di belakang koma hanya diizinkan maksimal sebanyak 13 digit.");
                this.$('[name="' + attrName + '"]').addClass('invalidCurrencyFormat');
            }
        },
        currencyValidationMaximumExposure: function (e) {
            var exposeFormat = /^\s*[0-9]+([\.][0-9][0-9]{0,12})?$|^$|^\s*$/;
            var attrName = e.target.name;
            var attrValue = this.$('[name="' + attrName + '"]').val();
            if (exposeFormat.test(attrValue)) {
                this.$('[name="' + attrName + '"]').removeClass('invalidCurrencyFormat');
            } else {
                commonFunction.responseWarningCannotExecute("Nilai Maksimum Expose tidak sesuai format. Format yang dizinkan 1000.02 / 123.39847. Angka di belakang koma hanya diizinkan maksimal sebanyak 13 digit.");
                this.$('[name="' + attrName + '"]').addClass('invalidCurrencyFormat');
            }
        },
        generateTemplateNotification: function () {
            const url = commonFunction.getCurrentDomainAndPort();
            var projectName = this.$('[name="NamaProject"]').val();
            var startProject = this.$('[name="TahunAwalProject"]').val();
            var endProject = this.$('[name="TahunAkhirProject"]').val();

            const startProjectFormated = moment(startProject).format('DD-MMM-YYYY');
            const endProjectFormated = moment(endProject).format('DD-MMM-YYYY');

            var startPc = "";
            var endPc = "";
            var startCn = "";
            var endCn = "";
            var startOp = "";
            var endOp = "";

            var html = '<div>'
            html += '<div style="font-size: 9pt;color:#373637;">'
            html += '<p style="margin-bottom:0px;">Dengan ini saya mengajukan permohonan Approval untuk Matriks Risiko dengan data berikut:</p>'
            html += '<p style="font-style: italic;margin-top:0px;margin-bottom:0px;">Nama Proyek: ' + projectName + '</p>'
            html += '<p style="font-style: italic;margin-top:0px;margin-bottom:0px">Periode Mulai Proyek: ' + startProjectFormated + '</p>'
            html += '<p style="font-style: italic;margin-top:0px;">Periode Akhir Proyek: ' + endProjectFormated + '</p>'
            // html += '<p>PC= '+ startPc +' s/d '+ endPc +'</p>'
            // html += '<p>CN= '+ startCn +' s/d '+ endCn +'</p>'
            // html += '<p>OP= '+ startOp +' s/d '+ endOp +'</p>'
            html += '</div>'
            html += '<div>'
            html += '<p>Mohon dilakukan persetujuan melalui aplikasi dengan menekan <span><a href="' + url + '/#approval">link ini</a></span> dan memasukan username dan login masing-masing.</p>'
            html += '<p>Terimakasih.</p>'
            html += '</div>'
            html += '</div>'

            return html;
        },
        getConfirmation: function (dom) {
            var self = this;
            //validate Maximum Nilai Expose
            var isValidated = true;
            this.$('[exposure-value]').each(function (i, item) {
                if (item.value == "") {
                    isValidated = false;
                    commonFunction.responseWarningCannotExecute("Nilai Expose wajib diisi.");
                    self.$('[name="' + item.name + '"]').addClass('invalidCurrencyFormat');
                } else {
                    self.$('[name="' + item.name + '"]').removeClass('invalidCurrencyFormat');
                }
            });

            //Validate Likelihood Detail
            this.$('[likelihood-value]').each(function (i, item) {
                var data = self.$('[name="DefinisiLikehoodFor-' + item.lang + '-RiskFor-' + item.id + '"]').val();
                if (data == null || data == "") {
                    isValidated = false;
                    commonFunction.responseWarningCannotExecute("Likelihood wajib diisi.");
                    self.$('[name="DefinisiLikehoodFor-' + item.lang + '-RiskFor-' + item.id + '"]').addClass('invalidCurrencyFormat');
                } else {
                    self.$('[name="DefinisiLikehoodFor-' + item.lang + '-RiskFor-' + item.id + '"]').removeClass('invalidCurrencyFormat');
                }
            });

            if (isValidated)
                this.confirmed();
        },
        confirmed: function () {
            var self = this;
            //validate all input type that fillable
            var isValidated = true;

            if (isValidated) {
                var templateNotif = this.generateTemplateNotification();
                var action = "simpan";
                var retVal;
                if  (this.isSended == true){
                    retVal = confirm("Apakah anda yakin untuk " + action + " Matriks Risiko ini?" + "\n\n\Matriks Risiko otomatis akan dilanjutkan ke proses persetujuan / approval. Sehingga Skenario ini tidak bisa diubah lagi.");
                }
                if (this.isSended == false){
                    retVal = confirm("Apakah anda yakin untuk " + action + " Matriks Risiko ini?" + "\n\n\Matriks Risiko membutuhkan pengajuan persetujuan terpisah.");
                }
                if (retVal == true) {
                    this.saveRiskMatrix(templateNotif);
                }
                else {
                    this.$('[btnApprove]').attr('disabled', false);
                    this.$('[btnDraft]').attr('disabled', false);
                }
            } else {
                commonFunction.exposureRiskMatrixValidation('Salah');
            }
        },
        reCalculateMaximuExpose: function (year) {
            var self = this;
            var tempValue = [];
            this.$("[maximum-exposure-for='" + year + "']").each(function (i, item) {
                tempValue.push(parseFloat(item.value.replace(/,/g, '')));
            });
            var maxValue = Math.max.apply(Math, tempValue);
            return maxValue;
        },
        sendApprove: function () {
            this.isSended = true;
            this.getConfirmation();
        },
      
        sendDraft: function () {
            this.isSended = false;
            this.getConfirmation();
        },
        saveRiskMatrix: function (template) {
            commonFunction.showLoadingSpinner();
            var self = this;
            var stageTahunRiskMatrixIds = this.getStageTahunRiskMatrix();
            var riskRegistrasi = this.getRiskRegistrasi();
            var data = {};
            var riskMatrixCollection = [];
            for (var i = 0; i < stageTahunRiskMatrixIds.length; i++) {
                var stageTahunRiskMatrix = {};
                var year = stageTahunRiskMatrixIds[i];
                var riskMatrixValue = [];

                stageTahunRiskMatrix.StageTahunRiskMatrixId = year;
                for (var e = 0; e < riskRegistrasi.length; e++) {
                    var riskMatrixValueItem = {};
                    var riskId = riskRegistrasi[e];
                    riskMatrixValueItem.RiskRegistrasiId = riskId;

                    var value = [];
                    var exposureValue = self.$('[data-risk="' + riskId + '-ForYear-' + year + '"]').val();
                    var exposureValueFormated = parseFloat(exposureValue.replace(/,/g, ''));
                    value.push(exposureValueFormated);
                    var likelihoodDetailId = self.$('[name="DefinisiLikehoodFor-' + year + '-RiskFor-' + riskId + '"]').val();
                    value.push(likelihoodDetailId);

                    riskMatrixValueItem.Values = value;

                    riskMatrixValue.push(riskMatrixValueItem);
                }
                stageTahunRiskMatrix.RiskMatrixValue = riskMatrixValue;
                var maximumExposePerYear = this.$('#max-exposure-' + year + '').val();
                if (maximumExposePerYear != "") {
                    var maxValue = this.$('#max-exposure-' + year + '').val();
                    var maxValueFormated = parseFloat(maxValue.replace(/,/g, ''));
                    stageTahunRiskMatrix.MaximumNilaiExpose = maxValueFormated;
                } else {
                    var maxValue = self.reCalculateMaximuExpose(year);
                    stageTahunRiskMatrix.MaximumNilaiExpose = maxValue;
                }
                riskMatrixCollection.push(stageTahunRiskMatrix);
            }
            data.RiskMatrixCollection = riskMatrixCollection;
            data.Id = this.parentId;
            data.IsSend = this.isSended;
            data.TemplateNotif = template;
            this.modelStageTahunRiskMatrixDetail.save(data);
            this.$('[btnDraft]').addClass('disabled');
            this.$('[btnApprove]').addClass('disabled');
            eventAggregator.trigger('risk_matrix/edit_detail/:refecth');
        }
    });
});