define(function (require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');
    var Table = require('./table/table');
    var Collection = require('./collection');
    var Paging = require('paging');

    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content tbl bg-white',
        template: _.template(template),
        events: {
            'click [name="add"]': 'add',
            'keyup #keyword': 'applyFilter',
            'click [name="btn-filter"]': 'applyFilter'
        },
        initialize: function () {
            var self = this;
            this.keyword = "";
            this.table = new Table({
                collection: new Collection()
            });
            this.paging = new Paging({
                collection: this.table.collection
            });
            this.listenTo(eventAggregator, 'risk_matrix/add:fecth', function () {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'risk_matrix/delete:fecth', function () {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'risk_matrix/edit:fecth', function () {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'risk_matrix/update:fecth', function (data) {
                self.fetchData();
                Router.navigate("/risk_matrix/edit_detail/" + data, { trigger: true });
            });
            this.listenTo(eventAggregator, 'risk_matrix/default:fecth', function () {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'risk_matrix/edit_detail/:refecth', function () {
                self.fetchData();
            });
        },
        afterRender: function () {
            this.$('[obo-table-sublikelihood]').append(this.table.el);
            this.table.render();

            this.insertView('[obo-paging]', this.paging);
            this.paging.render();
            this.fetchData();
        },
        fetchData: function () {
            var param = [];
            param.Search = this.keyword;
            this.table.collection.fetch({
                reset: true,
                data: param,
                success: function (req, res) {
                }
            })
        },
        add: function () {
            var self = this;
            require(['./add/view'], function (View) {
                commonFunction.setDefaultModalDialogFunction(self, View);
            });
        },
        applyFilter: function () {
            var self = this;
            var param = []
            var keyword = this.$('#keyword').val();
            this.keyword = keyword;
            param.Search = keyword;
            var keyIndex = this.$('#keyIndex option:selected').val();
            param.SearchBy = keyIndex;
            var keyIndex2 = this.$('#keyIndex2 option:selected').val();
            param.SearchBy2 = keyIndex2;
            this.table.collection.fetch({
                reset: true,
                data: param
            })
        }
    });
});
