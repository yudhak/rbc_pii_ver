define(function (require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');
    var Model = require('./../model');
    var ModelDetail = require('./model');

    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content tbl bg-white',
        template: _.template(template),
        initialize: function (options) {
            var self = this;
            this.IsSended = null;
            this.correlatedProjectId = commonFunction.getUrlHashSplit(3);
            this.currentCol = 0;
            this.currentRow = 0;
            this.status = '';
            this.currentTrigger = null;
            this.model = new Model();
            this.modelDetail = new ModelDetail();
            this.countSync = 0;
            this.dataHeader = null;
            this.dataDetail = null;
            this.listenTo(this.model, 'sync', () => {
                this.listenToOnce(this.modelDetail, 'sync', function (model) {
                    if (self.countSync != 0) {
                        commonFunction.responseSuccessUpdateAddDelete('Korelasi - Proyek berhasil tersimpan.');
                    }
                }, this);
                this.render();
            });
            this.fetchModelHeader();
            this.fetchModelDetail();
            commonFunction.showLoadingSpinner();
        },
        events: {
            'change [cormat]': 'setCorrelationMatrix',
            'click [btnApprove]': 'sendApprove',
            'click [btnDraft]': 'sendDraft'
        },
        afterRender: function () {
            var self = this;
            this.project = this.model.get('Project');
            this.renderCorrelationMatrix();
            this.renderCorrelatedDetail();
            this.setValue();
        },
        submitNotification: function () {
            var isSend = this.$('[name="IsSend"]').prop('checked');
            if (isSend) {
                commonFunction.responseWarningCannotExecute("Korelasi Proyek otomatis akan dilanjutkan ke proses persetujuan / approval jika menekan tombol Simpan. Sehingga Skenario ini tidak bisa diubah lagi.");
            } else {
                commonFunction.responseWarningCannotExecute("Korelasi Proyek membutuhkan pengajuan persetujuan terpisah setelah menekan tombol Simpan.");
            }
        },
        fetchModelHeader: function () {
            const self = this;
            this.model.fetch({
                reset: true,
                data: {
                    id: this.correlatedProjectId
                },
                success: function (req, res) {
                    self.dataHeader = res;
                    self.status = res.StatusId;
                }
            });
        },
        fetchModelDetail: function () {
            var self = this;
            this.modelDetail.fetch({
                reset: true,
                data: {
                    id: this.correlatedProjectId
                },
                success: function (req, res) {
                    self.dataDetail = res;
                }
            });
        },
        renderCorrelationMatrix: function () {
            const self = this;
            if (this.dataHeader) {
                if (this.dataHeader.CorrelationMatrix) {
                    let html = '<table class="table-risk-matrix">'
                    html += '<tr class="td-risk-matrix" style="background-color: #f0f0f0;">'
                    _.each(this.dataHeader.CorrelationMatrix, function (item) {
                        html += '<td class="td-risk-matrix" style="padding: 5px">' + item.NamaCorrelationMatrix + '</td>'
                    });
                    html += '</tr>'
                    html += '<tr class="td-risk-matrix">'
                    _.each(this.dataHeader.CorrelationMatrix, function (item) {
                        html += '<td class="td-risk-matrix text-center">' + item.Nilai + '</td>'
                    });
                    html += '</tr>'
                    html += '</div>'
                    this.$('[data-correlation-matrix]').append(html);
                }
                //this.$('[proyek-sektor-title]').text('Skenario: ' + this.dataHeader.NamaScenario + ' - ' + 'Proyek: ' + this.dataHeader.NamaProject + ' - ' + 'Sektor: ' + this.dataHeader.NamaSektor);

                this.$('[nama-skenario]').text('Skenario: ' + this.dataHeader.NamaScenario + '');
                this.$('[nama-proyek]').text('Proyek: ' + this.dataHeader.NamaProject + '');
                this.$('[nama-sektor]').text('Sektor: ' + this.dataHeader.NamaSektor + '');

                let htmlStatus = '';
                if (this.status == 2) {
                    htmlStatus = 'Status: <span class="label label-success">Disetujui</span>';
                    this.$('[nama-status]').append(htmlStatus);
                } else if (this.status == null) {
                    htmlStatus = 'Status: <span class="label label-default">Draft</span>';
                    this.$('[nama-status]').append(htmlStatus);
                } else if (this.status == 3) {
                    htmlStatus = 'Status: <span class="label label-danger">Tidak Disetujui</span>';
                    this.$('[nama-status]').append(htmlStatus);
                } else {
                    htmlStatus = 'Status: <span class="label label-warning">Proses Approval</span>';
                    this.$('[nama-status]').append(htmlStatus);
                }
            }
        },
        renderCorrelatedDetail: function () {
            const self = this;
            if (this.dataHeader) {
                if (this.dataHeader.Project) {
                    let html = '<div class="table-mat">'
                    //header
                    html += '<div class="heading-mat">'
                    html += '<div class="cell-mat"><p>Nama Proyek</p></div>'
                    html += '<div class="cell-mat"><p>Sektor</p></div>'
                    _.each(this.dataHeader.Project, function (item) {
                        html += '<div class="cell-mat" data-heading-risk="' + item.Id + '">'
                        html += '<p>' + item.NamaProject + '</p>'
                        html += '</div>'
                    });
                    html += '</div>'
                    //end of header

                    //row
                    _.each(this.dataHeader.Project, function (item) {
                        html += '<div class="row-mat" data-risk-row="' + item.NamaProject + '">'
                        html += '<div class="cell-mat"><p>' + item.NamaProject + '</p></div>'
                        html += '<div class="cell-mat"><p>' + item.Sektor.NamaSektor + '</p></div>'
                        _.each(self.dataHeader.Project, function (model) {
                            html += '<div class="cell-mat text-center" data-cell-risk="' + item.Id + '-' + model.Id + '" data="' + item.Id + '-' + model.Id + '">'
                            if (item.Id == model.Id) {
                                html += '<select style="background-color: lightgrey;" cormat data-correlation-matrix="' + item.Id + '-' + model.Id + '" data-cormat="' + item.Id + '-' + model.Id + '" disabled>'
                                html += '<option value="1">1</option>'
                                html += '</select>'
                            } else {
                                html += '<select cormat data-correlation-matrix="' + item.Id + '-' + model.Id + '" data-cormat="' + item.Id + '-' + model.Id + '">'
                                _.each(self.dataHeader.CorrelationMatrix, function (corMat) {
                                    html += '<option value="' + corMat.Id + '">' + corMat.Nilai + '</option>'
                                });
                                html += '</select>'
                            }
                            html += '</div>'
                        });
                        html += '</div>'
                    });
                    //end of row
                    html += '</div>'
                    self.$('.scrollable-matrix').append(html);
                }
            }
        },
        setValue: function () {
            var self = this;
            var data = this.dataDetail;
            if (data) {
                if (data.CorrelatedProjectDetailCollection) {
                    for (var i = 0; i < data.CorrelatedProjectDetailCollection.length; i++) {
                        var dataDetail = data.CorrelatedProjectDetailCollection[i];
                        var projectiId = dataDetail.ProjectiId;
                        var projectValues = dataDetail.CorrelatedProjectMatrixValues;
                        if (projectValues.length > 0) {
                            for (var c = 0; c < projectValues.length; c++) {
                                var cormatValue = projectValues[c].CorrelationMatrixId;
                                var row = projectValues[c].ProjectIdRow;
                                var col = projectValues[c].ProjectIdCol;

                                this.$('[data-cormat="' + row + '-' + col + '"]').val(cormatValue);
                            }
                        }
                    }
                }
                commonFunction.closeLoadingSpinner();
            }
        },
        setCorrelationMatrix: function (e) {
            var self = this;
            var attributeValue = e.target.getAttribute('data-cormat');
            var cormatValue = this.$('[data-cormat="' + attributeValue + '"]').val();
            if (this.currentTrigger == null) {
                this.$('[data-cormat="' + attributeValue + '"]').addClass('blink');
            } else {
                this.$('[data-cormat="' + this.currentTrigger + '"]').removeClass('blink');
                this.$('[data-cormat="' + attributeValue + '"]').addClass('blink');
            }
            this.currentTrigger = attributeValue;

            var row = attributeValue.substring(0, attributeValue.indexOf('-'));
            var col = attributeValue.substring(attributeValue.indexOf('-') + 1);

            this.$('[data-cormat="' + col + '-' + row + '"]').val(cormatValue);
            if (this.currentCol == 0 && this.currentRow == 0) {
                this.$('[data-cormat="' + col + '-' + row + '"]').addClass('blink');
            } else {
                this.$('[data-cormat="' + this.currentCol + '-' + this.currentRow + '"]').removeClass('blink');
                this.$('[data-cormat="' + col + '-' + row + '"]').addClass('blink');
            }
            this.currentCol = col;
            this.currentRow = row;
        },
        generateTemplateNotification: function () {
            var projectName = this.$('[proyek-sektor-title]').text();
            const url = commonFunction.getCurrentDomainAndPort();

            var html = '<div>'
            html += '<div>'
            html += '<p style="margin-bottom:0px;">Dengan Ini saya mengajukan  permohonan atas Korelasi Proyek dengan data berikut: </p>'
            html += '<p style="font-style: italic;margin-top:0px;margin-bottom:0px;">' + projectName + '</p>'
            html += '<p>Untuk melihat lebih detail data Korelasi Proyek, silakan masuk melalui aplikasi dengan menekan <span><a href="' + url + '/#approval">link ini</a></span> berdasarkan login masing-masing.</p>'
            html += '<p>Terimakasih.</p>'
            html += '</div>'
            html += '</div>'

            return html;
        },
        getConfirmation: function () {
            var templateNotif = this.generateTemplateNotification();
            var action = "simpan";
            var retVal;
            if  (this.isSended == true){
                retVal = confirm("Apakah anda yakin untuk " + action + " Korelasi - Proyek ini?" + "\n\n\Korelasi Proyek otomatis akan dilanjutkan ke proses persetujuan / approval. Sehingga Skenario ini tidak bisa diubah lagi.");
            }
            if (this.isSended == false){
                retVal = confirm("Apakah anda yakin untuk " + action + " Korelasi - Proyek ini?" + "\n\n\Korelasi Proyek membutuhkan pengajuan persetujuan terpisah.");
            }
            if (retVal == true) {
                this.doSave(templateNotif);
            }
            else {
                //this.$('[type="submit"]').attr('disabled', false);
            }
        },
        sendApprove: function () {
            this.isSended = true;
            this.getConfirmation();
        },
        sendDraft: function () {
            this.isSended = false;
            this.getConfirmation();
        },
        doSave: function (template) {
            var param = {};
            var paramCollection = [];

            for (var i = 0; i < this.project.length; i++) {
                var correlatedSektorDetailCollection = [];
                var projectValues = [];
                var paramDetail = {};

                for (var r = 0; r < this.project.length; r++) {
                    var paramDetailValue = {};
                    var row = this.project[i].Id;
                    var col = this.project[r].Id;
                    var cormatValue = this.$('[data-cormat="' + row + '-' + col + '"]').val();

                    paramDetailValue.ProjectIdRow = row;
                    paramDetailValue.ProjectIdCol = col;
                    paramDetailValue.CorrelationMatrixId = parseInt(cormatValue);

                    projectValues.push(paramDetailValue);
                }

                correlatedSektorDetailCollection.push(projectValues);
                paramDetail.ProjectId = this.project[i].Id;
                paramDetail.CorrelatedProjectMatrixValues = projectValues;

                param.CorrelatedProjectId = parseInt(this.correlatedProjectId);
                paramCollection.push(paramDetail);
                param.CorrelatedProjectDetailCollection = paramCollection;
                param.IsSend = this.isSended;
            }
            param.TemplateNotif = template;
            this.modelDetail.save(param);
            this.countSync += 1;
            commonFunction.showLoadingSpinner();
            this.$('[btnDraft]').addClass('disabled');
            this.$('[btnApprove]').addClass('disabled');
            eventAggregator.trigger('correlation/project/edit/:refecth');
        }
    });
});