define(function (require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');
    var Model = require('./../model');
    var ModelDetail = require('./model');

    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content tbl bg-white',
        template: _.template(template),
        initialize: function (options) {
            var self = this;
            this.IsSended;
            this.correlatedSektorId = commonFunction.getUrlHashSplit(3);
            this.status = null;
            this.currentCol = 0;
            this.currentRow = 0;
            this.currentTrigger = null;
            this.model = new Model();
            this.modelDetail = new ModelDetail();
            this.countSync = 0;
            this.listenToOnce(this.model, 'sync', function (model) {
                this.listenTo(this.modelDetail, 'sync', function (model) {
                    if (self.countSync != 0) {
                        commonFunction.responseSuccessUpdateAddDelete('Korelasi - Kategori Risiko berhasil tersimpan.');
                    }
                },
                    this);
                this.render();
            });
            this.model.fetch({
                reset: true,
                data: {
                    id: this.correlatedSektorId
                },
                success: function(req, res) {
                    self.status = res.StatusId;
                }
            });
            this.modelDetail.fetch({
                reset: true,
                data: {
                    id: this.correlatedSektorId
                },
                success: function(req, res) {
                }
            });
        },
        events: {
            'change [cormat]': 'setCorrelationMatrix',
            'click [btnApprove]': 'sendApprove',
            'click [btnDraft]': 'sendDraft'
        },
        afterRender: function () {
            var self = this;
            this.riskRegistrasi = this.model.get('RiskRegistrasi');
            this.setValue();
        },
        setValue: function () {
            var self = this;
            var data = null;
            this.modelDetail.fetch({
                reset: true,
                data: {
                    id: this.correlatedSektorId
                },
                success: function (collection, response) {
                    data = collection;
                },
                error: function () {
                }
            });
            data = this.modelDetail.toJSON();
            if (data.CorrelatedSektorDetailCollection) {
                for (var i = 0; i < data.CorrelatedSektorDetailCollection.length; i++) {
                    var dataDetail = data.CorrelatedSektorDetailCollection[i];
                    var riskRegistrasiId = dataDetail.RiskRegistrasiId;
                    var riskRegistrasiValues = dataDetail.RiskRegistrasiValues;
                    if (riskRegistrasiValues.length > 0) {
                        for (var c = 0; c < riskRegistrasiValues.length; c++) {
                            var cormatValue = riskRegistrasiValues[c].CorrelationMatrixId;
                            var row = riskRegistrasiValues[c].RiskRegistrasiIdRow;
                            var col = riskRegistrasiValues[c].RiskRegistrasiIdCol;

                            this.$('[data-cormat="' + row + '-' + col + '"]').val(cormatValue);
                        }
                    }
                }
            }

            let htmlStatus = '';
            if (this.status == 2) {
                htmlStatus = 'Status: <span class="label label-success">Disetujui</span>';
                this.$('[nama-status]').append(htmlStatus);
            } else if (this.status == null) {
                htmlStatus = 'Status: <span class="label label-default">Draft</span>';
                this.$('[nama-status]').append(htmlStatus);
            } else if (this.status == 3) {
                htmlStatus = 'Status: <span class="label label-danger">Tidak Disetujui</span>';
                this.$('[nama-status]').append(htmlStatus);
            } else {
                htmlStatus = 'Status: <span class="label label-warning">Proses Approval</span>';
                this.$('[nama-status]').append(htmlStatus);
            }
        },
        setCorrelationMatrix: function (e) {
            var self = this;
            var attributeValue = e.target.getAttribute('data-cormat');
            var cormatValue = this.$('[data-cormat="' + attributeValue + '"]').val();
            if (this.currentTrigger == null) {
                this.$('[data-cormat="' + attributeValue + '"]').addClass('blink');
            } else {
                this.$('[data-cormat="' + this.currentTrigger + '"]').removeClass('blink');
                this.$('[data-cormat="' + attributeValue + '"]').addClass('blink');
            }
            this.currentTrigger = attributeValue;

            var row = attributeValue.substring(0, attributeValue.indexOf('-'));
            var col = attributeValue.substring(attributeValue.indexOf('-') + 1);

            this.$('[data-cormat="' + col + '-' + row + '"]').val(cormatValue);
            if (this.currentCol == 0 && this.currentRow == 0) {
                this.$('[data-cormat="' + col + '-' + row + '"]').addClass('blink');
            } else {
                this.$('[data-cormat="' + this.currentCol + '-' + this.currentRow + '"]').removeClass('blink');
                this.$('[data-cormat="' + col + '-' + row + '"]').addClass('blink');
            }

            this.currentCol = col;
            this.currentRow = row;
        },
        generateTemplateNotification: function () {
            var sectorName = this.$('[nama-sektor]').text();
            var scenarioName = this.$('[nama-scenario]').text();
            const url = commonFunction.getCurrentDomainAndPort();
            var html = '<div>'
            html += '<div style="font-size: 9pt;color:#373637;">'
            html += '<p style="margin-bottom:0px;">Dengan ini saya meyetujui  permohonan atas Korelasi Kategori Risiko dengan data berikut: </p>'
            html += '<p style="font-style: italic;margin-top:0px;margin-bottom:0px;">' + sectorName + '</p>'
            html += '<p style="font-style: italic;margin-top:0px;margin-bottom:0px;">' + scenarioName + '</p>'
            html += '<p>Untuk melihat lebih detail data Korelasi Kategori Risik, silakan masuk melalui aplikasi dengan menekan <span><a href="' + url + '/#approval">link ini</a></span> berdasarkan login masing-masing.</p>'
            html += '<p>Terimakasih.</p>'
            html += '</div>'
            html += '</div>'

            return html;
        },
        getConfirmation: function () {
            var templateNotif = this.generateTemplateNotification();
            var action = "simpan";
            var retVal;
            if  (this.isSended == true){
                retVal = confirm("Apakah anda yakin untuk " + action + " Korelasi - Kategori Risiko ini?" + "\n\n\Korelasi Kategori Risiko otomatis akan dilanjutkan ke proses persetujuan / approval. Sehingga Skenario ini tidak bisa diubah lagi.");
            }
            if (this.isSended == false){
                retVal = confirm("Apakah anda yakin untuk " + action + " Korelasi - Kategori Risiko ini?" + "\n\n\Korelasi Kategori Risiko membutuhkan pengajuan persetujuan terpisah.");
            }

            if (retVal == true) {
                this.doSave(templateNotif);
            }
            else {
                //this.$('[type="submit"]').attr('disabled', false);
            }
        },
        sendApprove: function () {
            this.isSended = true;
            this.getConfirmation();
        },
        sendDraft: function () {
            this.isSended = false;
            this.getConfirmation();
        },
        doSave: function (template) {
            var param = {};
            var paramCollection = [];
            for (var i = 0; i < this.riskRegistrasi.length; i++) {
                var correlatedSektorDetailCollection = [];
                var risRegistrasiValues = [];
                var paramDetail = {};

                for (var r = 0; r < this.riskRegistrasi.length; r++) {
                    var paramDetailValue = {};
                    var row = this.riskRegistrasi[i].Id;
                    var col = this.riskRegistrasi[r].Id;
                    var cormatValue = this.$('[data-cormat="' + row + '-' + col + '"]').val();

                    paramDetailValue.RiskRegistrasiIdRow = row;
                    paramDetailValue.RiskRegistrasiIdCol = col;
                    paramDetailValue.CorrelationMatrixId = parseInt(cormatValue);

                    risRegistrasiValues.push(paramDetailValue);
                }

                correlatedSektorDetailCollection.push(risRegistrasiValues);
                paramDetail.RiskRegistrasiId = this.riskRegistrasi[i].Id;
                paramDetail.RiskRegistrasiValues = risRegistrasiValues;
                //paramDetail.RiskRegistrasiValues = correlatedSektorDetailCollection;

                param.CorrelatedSektorId = parseInt(this.correlatedSektorId);
                paramCollection.push(paramDetail);
                param.CorrelatedSektorDetailCollection = paramCollection;
                param.IsSend = this.isSended;
            }
            param.TemplateNotif = template;
            this.modelDetail.save(param);
            this.countSync += 1;
            commonFunction.showLoadingSpinner();
            this.$('[btnDraft]').addClass('disabled');
            this.$('[btnApprove]').addClass('disabled');
            eventAggregator.trigger('correlation/risk_category/edit/:refecth');
        }
    });
});