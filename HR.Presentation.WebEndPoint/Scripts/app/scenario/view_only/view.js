define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var Model = require('./../model');
  var commonFunction = require('commonfunction');
  var commonConfig = require('commonconfig');
  var eventAggregator = require('eventaggregator');
  var Model = require('./../model');
  var ModelDetail = require('./../modelDetail');
  var CollectionLikehoodDetail = require('./../select_likelihood/detail/collection');
  var Table = require('./../select_likelihood/detail/table/table');
  require('bootstrap-validator');
  require('jquerymask');

  module.exports = View.extend({
    template: _.template(template),
    initialize: function (options) {
      var self = this;
      this.isAlreadyEdited = false;
      var scenarioId = this.model.get('Id');
      this.likelihoodId = this.model.get('LikehoodId');
      this.namaLikehood = this.model.get('NamaLikehood');
      this.scenario = this.model;
      this.projectIds = [];
      this.projectSelected = [];

      // if (this.model.get('ScenarioDetail').length) {
      //   _.each(this.model.get('ScenarioDetail'), function (item) {
      //     if (!item.IsDelete) {
      //       self.projectIds.push(item.Project.Id);
      //       self.projectSelected.push(item.Project);
      //     }
      //   });
      // }
      
      this.modelDetail = new ModelDetail();
      this.model = new Model();
      this.model.set(this.model.idAttribute, scenarioId);
      this.table = new Table({
        collection: new CollectionLikehoodDetail()
      });
      this.listenTo(this.model, 'request', function () {});
      this.listenTo(this.model, 'sync error', function () {});
      this.listenToOnce(this.model, 'sync', function (model) {
        this.render();
        var data = model.toJSON();
        this.listenTo(this.model, 'sync', function () {
          commonFunction.responseSuccessUpdateAddDelete('Scenario successfully updated.');
          self.$el.modal('hide');
          eventAggregator.trigger('scenario/edit:fecth');
        });
      }, this);
      this.listenTo(eventAggregator, 'scenario/add/select_likelihood:likelihood_selected', function (obj) {
        self.likelihoodId = obj.Id;
        self.namaLikehood = obj.NamaLikehood;
        self.renderLikelihoodDetail();
      });
      this.listenTo(eventAggregator, 'scenario/add/select_project:project_selected', function (projectNames, projectIds, projectCollection) {
        self.model.set('isAlreadyEdited', true);
        self.renderProjectAfterEdit(projectNames);
        self.projectIds = projectIds;
        self.projectSelected = projectIds;
        self.renderProjectList(projectCollection);
      });
      this.fetchDetail(scenarioId);
      this.once('afterRender', function () {
        this.model.fetch();
      });
    },
    afterRender: function () {
      this.$('[likelihood-table]').append(this.table.el);
      this.table.render();
      this.table.collection.fetch({
        reset: true,
        data: {
          ParentId: this.likelihoodId,
          PageSize: 100
        }
      });
      this.setTemplate(this.scenario);
      this.renderValidation();
      this.renderProjectList(this.projectSelected);
    },
    events: {
      'click [name="SelectProject"]': 'selectProject',
      'click [name="EditSelectProject"]': 'selectProject',
      'click [name="SelectLikelihood"]': 'selectLikelihood'
    },
    fetchDetail: function(scenarioId) {
      const self = this;
      this.modelDetail.fetch({
        reset: true,
        data: {
          isHeaderDetail: true,
          scenarioId: scenarioId
        },
        success: function(req, res) {
          if (res.length) {
            for (let i = 0; i < res.length; i++) {
              if (!res[i].IsDelete) {
                self.projectIds.push(res[i].Project.Id);
                self.projectSelected.push(res[i].Project);
              }
            }
          }
        }
      });
    },
    renderLikelihoodDetail: function () {
      var likehoodId = this.likelihoodId;
      if (likehoodId) {
        this.$('[likelihood-table]').append(this.table.el);
        this.table.render();
        this.table.collection.fetch({
          reset: true,
          data: {
            ParentId: likehoodId,
            PageSize: 100
          }
        });
      }
      var likehoodName = likehoodId;//this.modelLikehood.NamaLikehood;
      this.$('[name="NamaLikehood"]').val(this.namaLikehood);
    },
    renderProjectAfterEdit: function (projectNames) {
      var chosenName = projectNames.join(", ");
      var currentChosenProject = this.$('[name="ChosenProject"]').text(chosenName);
      if (this.$('[name="ChosenProject"]').val()) {
        this.$('[name="EditSelectProject"]').removeClass('hidden');
        this.$('[name="SelectProject"]').addClass('hidden');
      }
    },
    renderProjectList: function(collection) {
      var self = this;
      
      this.$('[project-table]').empty();
      var html = '<table>'
          html += '<tr class="header-kiw-table">'
          html += '<td class="td-kiw-table">Nama Proyek</td>'
          html += '<td class="td-kiw-table">Sektor</td>'
          html += '<td class="td-kiw-table">Kategori Risiko</td>'
          html += '</tr>'
          
          $.each(collection, function(i, item) {
            var riskSelected = ''; 
            html += '<tr class="row-kiw-table" id="'+ item.Id +'">'
            html += '<td class="td-kiw-table"> '+ item.NamaProject +' </td>'
            html += '<td class="td-kiw-table"> '+ item.NamaSektor +' </td>'
            if(item.ProjectRiskRegistrasi) {
              _.each(item.ProjectRiskRegistrasi, function(modelRisk) {
                riskSelected += modelRisk.RiskRegistrasi.KodeMRisk;
                riskSelected += ', ';
              });
            }
            html += '<td class="td-kiw-table"> '+ riskSelected +' </td>'
            html += '<td>'
            html += '</tr>'
          });
          html += '</table>'

        this.$('[project-table]').append(html);
    },
    setTemplate: function (obj) {
      var projectNames = '';
      var likelihoodName = obj.get('NamaLikehood');
      var project = this.projectSelected;
      if (project.length > 0) {
        for (var i = 0; i < project.length; i++) {
          projectNames += project[i].NamaProject;
          projectNames += ', ';
        }
      }
      this.$('[name="ChosenProject"]').text(projectNames);
      this.$('[name="NamaLikehood"]').val(this.namaLikehood);
    },
    selectProject: function () {
      require(['./../select_project/view'], (View) => {
        commonFunction.setDefaultModalDialogFunction(this, View, this.model, this.projectSelected);
      });
    },
    selectLikelihood: function () {
      require(['./../select_likelihood/view'], (View) => {
        commonFunction.setDefaultModalDialogFunction(this, View, this.model);
      });
    },
    renderValidation: function () {
      var self = this;
      this.$('[ehs-form]').bootstrapValidator({
          fields: {
            NamaScenario: {
              validators: {
                stringLength: {
                  message: 'Nama scenario must be less than 100 characters',
                  max: 100
                },
                notEmpty: {
                  message: 'Nama scenario is required'
                }
              }
            },
            ChosenProject: {
              validators: {
                notEmpty: {
                  message: 'Projects is required'
                }
              }
            },
            NamaLikehood: {
              validators: {
                notEmpty: {
                  message: 'Likelihood is required'
                }
              }
            }
          }
        })
        .on('success.form.bv', function (e) {
          e.preventDefault();
          self.getConfirmation();
        });
    },
    getConfirmation: function(){
      var self = this;
      var isValid = false;
      console.log(this.projectIds);
      if (this.likelihoodId && this.projectIds.length) {
        isValid = true;
      }
      if (isValid) {
        var data = this.$('[name="NamaScenario"]').val();
        var action = "edit";
        var retVal = confirm("Are you sure want to " + action + " Scenario : "+ data +" ?");
        if( retVal == true ){
          this.doSave();
        }
        else{
          this.$('[type="submit"]').attr('disabled', false);
        }
      } else {
        commonFunction.responseWarningCannotExecute("Parameter atau Proyek wajib diisi.");
        this.$('[type="submit"]').attr('disabled', false);
      }
    },
    doSave: function () {
      var data = commonFunction.formDataToJson(this.$('form').serializeArray());
      data.ProjectId = this.projectIds;
      data.IsSend = this.$('[name="IsSend"]').prop('checked');
      data.LikehoodId = this.likelihoodId;
      data.IsDefault = false;
      data.IsUpdate = true;
      this.model.save(data);
      commonFunction.showLoadingSpinner();
    }
  });
});