﻿define(function(require, exports, module) {
    'use strict';

    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'Id',
        urlRoot: 'api/ScenarioDetail',
        defaults: function() {
            return {
              ScenarioId : '',
              NamaScenario : '',
              ProjectId : '',
              NamaProject : '',
              IsDelete : '',
              Project : {
                Id : '',
                NamaProject : '',
              }
            }
        }
    });
});
