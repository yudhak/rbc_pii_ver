define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var commonFunction = require('commonfunction');
  var Table = require('./table/table');
  var Collection = require('./collection');
  var Paging = require('paging');
  var eventAggregator = require('eventaggregator');


  module.exports = View.extend({
    template: _.template(template),
    initialize: function (options) {
      var self = this;
      var ids = null;
      var isEdited = options.model.get('isAlreadyEdited');
      this.isSelectedAll = false;
      var projectIds = [];
      if (isEdited) {
        ids = options.view;
        _.each(ids, function (item) {
          projectIds.push(item);
        });
      } else {
        ids = options.view;
        _.each(ids, function (item) {
          projectIds.push(item.Id);
        });
      }

      this.table = new Table({
        collection: new Collection()
      });
      var originalParse = this.table.collection.parse;
      this.table.collection.parse = function (response) {
        var result = originalParse.call(this, response);
        if (projectIds.length > 0) {
          _.each(result, function (item) {
            var found = _.find(projectIds, (id) => {
              return id == item.Id
            });
            item.isChecked = Boolean(found);
          });
          return result;
        } else {
          _.each(result, function (item) {
            var selectedId = self.model.get('ProjectSelected') || [];

            var found = _.find(selectedId, (id) => {
              return id == item.Id
            });
            item.isChecked = Boolean(found);
          });
          return result;
        }
      }

      this.paging = new Paging({
        collection: this.table.collection
      });
      this.on('cleanup', function () {
        this.table.destroy();
        this.modalDialog && this.modalDialog.remove && this.modalDialog.remove();
      }, this)

      this.listenTo(eventAggregator, 'scenario/add/select_project:isSelectedAll', function (arg) {
        self.isSelectedAll = arg;
        self.model.set('IsSelectedAll', self.isSelectedAll);
        if (!self.isSelectedAll) {
          self.resetProjectSelected();
        }
      });
    },
    afterRender: function () {
      this.$('[project-table]').append(this.table.el);
      this.table.render();

      this.table.collection.fetch({
        reset: true,
        data: {
          IsPagination: false,
          PageSize: 100
        }
      });
    },
    events: {
      'click [btn-save-chosen-risk]': 'getChosenRisk'
    },
    resetProjectSelected: function () {
      this.table.collection.map((model) => {
        return model.set('isChecked', false);
      });
    },
    getChosenRisk: function () {
      var projectNameSelected = [];
      var projectSelectedAfterEdit = [];
      var projectSelectedCollection = [];

      if (this.isSelectedAll) {
        this.model.set('IsSelectedAll', true);
        this.table.collection.map((model) => {
          const setIsChecked = model.set('isChecked', true);
          projectNameSelected.push(model.attributes.NamaProject);
          projectSelectedAfterEdit.push(model.attributes.Id);
          projectSelectedCollection.push(model.attributes);
        });
      } else {
        var collection = this.table.collection.filter((model) => {
          var isSelected = model.get('isChecked');
          if (isSelected) {
            projectNameSelected.push(model.attributes.NamaProject);
            projectSelectedAfterEdit.push(model.attributes.Id);
            projectSelectedCollection.push(model.attributes);
          }
          return isSelected;
        });
      }
      eventAggregator.trigger(
        'scenario/add/select_project:project_selected',
        projectNameSelected,
        projectSelectedAfterEdit,
        projectSelectedCollection,
        this.isSelectedAll
      );
      this.$el.modal('hide');
    }
  });
});