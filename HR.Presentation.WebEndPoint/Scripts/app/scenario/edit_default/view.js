define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var Model = require('./../model');
  var commonFunction = require('commonfunction');
  var commonConfig = require('commonconfig');
  var eventAggregator = require('eventaggregator');
  var Model = require('./../model');
  var ModelDetail = require('./../modelDetail');
  var CollectionLikehoodDetail = require('./../select_likelihood/detail/collection');
  var Table = require('./../select_likelihood/detail/table/table');
  require('bootstrap-validator');
  require('jquerymask');

  module.exports = View.extend({
    template: _.template(template),
    initialize: function (options) {
      var self = this;
      this.isSended = null;
      this.isScenarioDefault = true;
      this.isAlreadyEdited = false;
      var scenarioId = this.model.get('Id');
      this.likelihoodId = this.model.get('LikehoodId');
      this.namaLikehood = this.model.get('NamaLikehood');
      this.scenario = this.model;
      this.projectIds = [];
      this.projectSelected = [];
      this.projectHtml = null;
      this.likelihoodDetailValue = null;
      // if (this.model.get('ScenarioDetail').length) {
      //   console.log(this.model.get('ScenarioDetail'));
      //   _.each(this.model.get('ScenarioDetail'), function (item) {
      //     if (!item.IsDelete) {
      //       self.projectIds.push(item.Project.Id);
      //       self.projectSelected.push(item.Project);
      //     }
      //   });
      // }

      this.model = new Model();
      this.modelDetail = new ModelDetail();
      this.model.set(this.model.idAttribute, scenarioId);
      this.table = new Table({
        collection: new CollectionLikehoodDetail()
      });

      this.fetchDetail(scenarioId);


      this.listenTo(this.model, 'request', function () {});
      this.listenTo(this.model, 'sync error', function () {
        debugger;
        eventAggregator.trigger('scenario/edit:fecth');
      });
      this.listenToOnce(this.model, 'sync', function (model) {
        debugger;
        this.render();
        var data = model.toJSON();
        this.listenTo(this.model, 'sync', function () {
          commonFunction.responseSuccessUpdateAddDelete('Skenario berhasil dibuat.');
          self.$el.modal('hide');
          eventAggregator.trigger('scenario/edit:fecth');
        });
      }, this);
      this.listenTo(eventAggregator, 'scenario/add/select_likelihood:likelihood_selected', function (obj) {
        self.likelihoodId = obj.Id;
        self.namaLikehood = obj.NamaLikehood;
        self.renderLikelihoodDetail();
      });
      this.listenTo(eventAggregator, 'scenario/add/select_project:project_selected', function (projectNames, projectIds, projectCollection) {
        self.model.set('isAlreadyEdited', true);
        self.renderProjectAfterEdit(projectNames);
        self.projectIds = projectIds;
        self.projectSelected = projectIds;
        self.renderProjectList(projectCollection);
      });
      this.once('afterRender', function () {
        this.model.fetch();
      });
    },
    afterRender: function () {
      var self = this;
      this.$('[likelihood-table]').append(this.table.el);
      this.table.render();
      this.table.collection.fetch({
        reset: true,
        data: {
          ParentId: this.likelihoodId,
          PageSize: 100
        },
        success: function(req, res) {
          self.likelihoodDetailValue = res;
        }
      });
      this.setTemplate(this.scenario);
      this.renderValidation();
      this.renderProjectList(this.projectSelected);
    },
    events: {
      'click [name="SelectProject"]': 'selectProject',
      'click [name="EditSelectProject"]': 'selectProject',
      'click [name="SelectLikelihood"]': 'selectLikelihood',
      'click [name="btnApprove"]': 'sendApprove',
      'click [name="btnDraft"]': 'sendDraft'
    },
    fetchDetail: function(scenarioId) {
      const self = this;
      this.modelDetail.fetch({
        reset: true,
        data: {
          isHeaderDetail: true,
          scenarioId: scenarioId
        },
        success: function(req, res) {
          if (res.length) {
            for (let i = 0; i < res.length; i++) {
              if (!res[i].IsDelete) {
                self.projectIds.push(res[i].Project.Id);
                self.projectSelected.push(res[i].Project);
              }
            }
          }
        }
      });
    },
    submitNotification: function() {
      var isSend = this.$('[name="IsSend"]').prop('checked');
      if (isSend) {
        commonFunction.responseWarningCannotExecute("Skenario otomatis akan dilanjutkan ke proses persetujuan / approval jika menekan tombol Simpan. Sehingga Skenario ini tidak bisa diubah lagi.");
      } else {
        commonFunction.responseWarningCannotExecute("Skenario membutuhkan pengajuan persetujuan terpisah setelah menekan tombol Simpan.");
      }
    },
    renderLikelihoodDetail: function () {
      var likehoodId = this.likelihoodId;
      if (likehoodId) {
        this.$('[likelihood-table]').append(this.table.el);
        this.table.render();
        this.table.collection.fetch({
          reset: true,
          data: {
            ParentId: likehoodId,
            PageSize: 100
          },
          success: function(req, res) {
            self.likelihoodDetailValue = res;
          }
        });
      }
      var likehoodName = likehoodId;//this.modelLikehood.NamaLikehood;
      this.$('[name="NamaLikehood"]').val(this.namaLikehood);
    },
    renderProjectAfterEdit: function (projectNames) {
      var chosenName = projectNames.join(", ");
      var currentChosenProject = this.$('[name="ChosenProject"]').text(chosenName);
      if (this.$('[name="ChosenProject"]').val()) {
        this.$('[name="EditSelectProject"]').removeClass('hidden');
        this.$('[name="SelectProject"]').addClass('hidden');
      }
    },
    renderProjectList: function(collection) {
      var self = this;
      
      this.$('[project-table]').empty();
      var html = '<table>'
          html += '<tr class="header-kiw-table">'
          html += '<td class="td-kiw-table">Nama Proyek</td>'
          html += '<td class="td-kiw-table">Sektor</td>'
          html += '<td class="td-kiw-table">Kategori Risiko</td>'
          html += '</tr>'
          
          $.each(collection, function(i, item) {
            var riskSelected = ''; 
            html += '<tr class="row-kiw-table" id="'+ item.Id +'">'
            html += '<td class="td-kiw-table"> '+ item.NamaProject +' </td>'
            html += '<td class="td-kiw-table"> '+ item.NamaSektor +' </td>'
            if(item.ProjectRiskRegistrasi) {
              _.each(item.ProjectRiskRegistrasi, function(modelRisk) {
                riskSelected += modelRisk.RiskRegistrasi.KodeMRisk;
                riskSelected += ', ';
              });
            }
            html += '<td class="td-kiw-table"> '+ riskSelected +' </td>'
            html += '<td>'
            html += '</tr>'
          });
          html += '</table>'

        this.$('[project-table]').append(html);

        var htmlNotif = '<table style="width:100%;border-collapse: collapse;font-family: Arial,sans-serif;font-size: 9pt;color:#373637;">'
          htmlNotif += '<tr style="border: 1px solid #c9c4c4;background-color: #eeebeb;">'
          htmlNotif += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">Nama Proyek</td>'
          htmlNotif += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">Sektor</td>'
          htmlNotif += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">Kategori Risiko</td>'
          htmlNotif += '</tr>'
          $.each(collection, function(i, item) {
            var riskSelected = ''; 
            htmlNotif += '<tr style="border: 1px solid #c9c4c4;" id="'+ item.Id +'">'
            htmlNotif += '<td style="border: 1px solid #c9c4c4;padding: 5px;"> '+ item.NamaProject +' </td>'
            htmlNotif += '<td style="border: 1px solid #c9c4c4;padding: 5px;"> '+ item.NamaSektor +' </td>'
            if(item.ProjectRiskRegistrasi) {
              _.each(item.ProjectRiskRegistrasi, function(modelRisk) {
                riskSelected += modelRisk.RiskRegistrasi.KodeMRisk;
                riskSelected += ', ';
              });
            }
            htmlNotif += '<td style="border: 1px solid #c9c4c4;padding: 5px;"> '+ riskSelected +' </td>'
            htmlNotif += '</tr>'
          });
          htmlNotif += '</table>'
          
        this.projectHtml = htmlNotif;
    },
    generateTemplateNotification: function() {
      const url = commonFunction.getCurrentDomainAndPort();
      var dataLikelihoodDetail = this.likelihoodDetailValue.results;
      var paramaterName = this.$('[name="NamaLikehood"]').val();
      var scenarioName = this.$('[name="NamaScenario"]').val();
      var keterangan = this.$('[name="NamaScenario"]').val();
      var html = '<div>'
      html += '<div style="font-size: 9pt;color:#373637;">'
      html += '<p style="margin-bottom: 0px;">Dengan ini saya mengajukan permohonan Approval untuk skenario dengan data berikut: </p>'
      html += '<p style="margin-top: 0px;font-style: italic;">Nama Skenario: '+ scenarioName +'</p>'
      html += '</div>'
      html += '<div style="font-size: 9pt;color:#373637;">'
      html += '<p style="margin-bottom: 0;">Parameter: '+ paramaterName +'</p>'
      html += '<table style="width:100%;border: 1px solid #c9c4c4;text-align: center;border-collapse: collapse;font-family: Arial,sans-serif;font-size: 9pt;color:#373637;">'
      html += '<tr style="border: 1px solid #c9c4c4;background-color: #eeebeb;">'
      html += '<td style="border: 1px solid #c9c4c4;padding: 5px;";">Definisi Parameter</td>'
      html += '<td style="border: 1px solid #c9c4c4;padding: 5px;";">Nilai Terendah</td>'
      html += '<td style="border: 1px solid #c9c4c4;padding: 5px;";">Nilai Tertinggi</td>'
      html += '<td style="border: 1px solid #c9c4c4;padding: 5px;";">Rata-Rata</td>'
      html += '</tr>'
      if (dataLikelihoodDetail) {
        _.each(dataLikelihoodDetail, function(item) {
          html += '<tr style="border: 1px solid #c9c4c4;">'
          html += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">'+ item.DefinisiLikehood +'</td>'
          html += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">'+ item.Lower +'</td>'
          html += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">'+ item.Incres +'</td>'
          html += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">'+ item.Average +'</td>'
          html += '</tr>'
        })
      }
      html += '</table>'
      html += '</div>'
      html += '<div style="margin-bottom: 20px;font-size: 9pt;color:#373637;"></div>'
      html += '<p style="margin-bottom: 0;font-size: 9pt;color:#373637;">Proyek</p>'
      html += this.projectHtml
      html += '<div style="margin-top: 25px;font-size: 9pt;color:#373637;">'
      html += '<p>Mohon dilakukan persetujuan melalui aplikasi dengan menekan <span><a href="' + url +'/#approval">link ini</a></span> dan memasukan username dan login masing-masing.</p>'
      html += '<p>Terimakasi.</p>'
      html += '</div>'

      return html;
    },
    setTemplate: function (obj) {
      var projectNames = '';
      var likelihoodName = obj.get('NamaLikehood');
      var project = this.projectSelected;
      if (project.length > 0) {
        for (var i = 0; i < project.length; i++) {
          projectNames += project[i].NamaProject;
          projectNames += ', ';
        }
      }
      this.$('[name="ChosenProject"]').text(projectNames);
      this.$('[name="NamaLikehood"]').val(this.namaLikehood);
    },
    selectProject: function () {
      debugger;
      require(['./../select_project/view'], (View) => {
        commonFunction.setDefaultModalDialogFunction(this, View, this.model, this.projectSelected);
      });
    },
    selectLikelihood: function () {
      require(['./../select_likelihood/view'], (View) => {
        commonFunction.setDefaultModalDialogFunction(this, View, this.model);
      });
    },
    renderValidation: function () {
      var self = this;
      this.$('[ehs-form]').bootstrapValidator({
          fields: {
            NamaScenario: {
              validators: {
                stringLength: {
                  message: 'Nama scenario must be less than 100 characters',
                  max: 100
                },
                notEmpty: {
                  message: 'Nama scenario is required'
                }
              }
            },
            ChosenProject: {
              validators: {
                notEmpty: {
                  message: 'Projects is required'
                }
              }
            },
            NamaLikehood: {
              validators: {
                notEmpty: {
                  message: 'Likelihood is required'
                }
              }
            }
          }
        })
        .on('success.form.bv', function (e) {
          e.preventDefault();
          self.getConfirmation();
        });
    },
    getConfirmation: function(){
      var self = this;
      var templateNotif = this.generateTemplateNotification();
      var isValid = false;
      
      if (this.likelihoodId && this.projectIds.length) {
        isValid = true;
      }
      if (isValid) {
        var data = this.$('[name="NamaScenario"]').val();
        var action = "ubah";
        var retVal;
        if  (this.isSended == true){
          retVal = confirm("Apakah anda yakin untuk " + action + " Skenario : " + data + " ?" + "\n\n\Skenario otomatis akan dilanjutkan ke proses persetujuan / approval. Sehingga Skenario ini tidak bisa diubah lagi.");
        }
        if (this.isSended == false){
          retVal = confirm("Apakah anda yakin untuk " + action + " Skenario : " + data + " ?" + "\n\n\Skenario membutuhkan pengajuan persetujuan terpisah.");
        }
        if( retVal == true ){
          this.doSave(templateNotif);
        }
        else{
          this.$('[type="submit"]').attr('disabled', false);
        }
      } else {
        commonFunction.responseWarningCannotExecute("Parameter atau Proyek wajib diisi.");
        this.$('[type="submit"]').attr('disabled', false);
      }
    },
    sendApprove: function () {
      this.isSended = true;
      },

    sendDraft: function () {
      this.isSended = false;
      },
    doSave: function (obj) {
      var data = commonFunction.formDataToJson(this.$('form').serializeArray());
      data.ProjectId = this.projectIds;
      data.IsSend = this.isSended;
      data.IsScenarioDefault = this.isScenarioDefault;
      data.LikehoodId = this.likelihoodId;
      data.IsDefault = false;
      data.IsUpdate = true;
      data.TemplateNotif = obj;
      this.model.save(data);
      this.$('[type="submit"]').addClass('disabled');
      commonFunction.showLoadingSpinner();
    }
  });
});