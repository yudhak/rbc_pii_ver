define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var commonFunction = require('commonfunction');
  var Model = require('./../model');
  var CollectionLikehoodDetail = require('./../select_likelihood/detail/collection');
  var Table = require('./../select_likelihood/detail/table/table');
  var eventAggregator = require('eventaggregator');
  require('bootstrap-validator');
  require('jquerymask');

  module.exports = View.extend({
    template: _.template(template),
    initialize: function () {
      debugger;
      var self = this;
      this.isSended = null;
      this.projectIds = null;
      this.likelihoodDetailValue = null;
      this.projectHtml = null;
      this.isProjectSelectedAll = false;
      this.table = new Table({
        collection: new CollectionLikehoodDetail()
      });

      this.model = new Model();

      this.listenTo(this.model, 'request', function () { });
      this.listenTo(this.model, 'sync error', function () { 
        debugger;
        eventAggregator.trigger('scenario/add:fecth');
      });
      this.listenTo(this.model, 'sync', function (model) {
        debugger;
        commonFunction.responseSuccessUpdateAddDelete('Skenario berhasil dibuat.');
        self.$el.modal('hide');
        eventAggregator.trigger('scenario/add:fecth');
      });

      this.listenTo(eventAggregator, 'scenario/add/select_likelihood:likelihood_selected', function (obj) {
        self.modelLikehood = obj;
        self.renderLikelihoodDetail();
      });
      this.listenTo(eventAggregator, 'scenario/add/select_project:project_selected', function (projectNames, projectIds, collectionProject, isSelectedAll) {
        self.setTemplate(projectNames);
        self.projectIds = projectIds;
        self.collectionProject = collectionProject;
        self.renderProjectList(self.collectionProject);
        self.model.set('isAlreadyEdited', true);
        self.isProjectSelectedAll = isSelectedAll;
      });
    },
    afterRender: function () {
      this.renderValidation();
    },
    events: {
      'click [name="SelectProject"]': 'selectProject',
      'click [name="EditSelectProject"]': 'selectProject',
      'click [name="SelectLikelihood"]': 'selectLikelihood',
      'click [data-dismiss]': 'refetch',
      'click [name="btnApprove"]': 'sendApprove',
      'click [name="btnDraft"]': 'sendDraft'
    },
    renderLikelihoodDetail: function () {
      var self = this;
      var likehoodId = this.modelLikehood.Id;
      if (likehoodId) {
        this.$('[likelihood-table]').append(this.table.el);
        this.table.render();
        this.table.collection.fetch({
          reset: true,
          data: {
            ParentId: likehoodId,
            PageSize: 100
          },
          success: function (req, res) {
            self.likelihoodDetailValue = res;
          }
        });
      }
      var likehoodName = this.modelLikehood.NamaLikehood;
      this.$('[name="NamaLikehood"]').val(likehoodName);
    },
    setTemplate: function (projectNames) {
      var chosenName = projectNames.join(", ");
      var currentChosenProject = this.$('[name="ChosenProject"]').text(chosenName);
      if (this.$('[name="ChosenProject"]').val()) {
        this.$('[name="EditSelectProject"]').removeClass('hidden');
        this.$('[name="SelectProject"]').addClass('hidden');
      }
    },
    selectProject: function () {
      this.model.set('IsProjectSelectedAll', this.isProjectSelectedAll);
      require(['./../select_project/view'], (View) => {
        commonFunction.setDefaultModalDialogFunction(this, View, this.model, this.projectIds);
      });
    },
    selectLikelihood: function () {
      require(['./../select_likelihood/view'], (View) => {
        commonFunction.setDefaultModalDialogFunction(this, View, this.model);
      });
    },
    renderProjectList: function (collection) {
      var self = this;
      var obj = [];
      this.$('[project-table]').empty();
      var html = '<table>'
      html += '<tr class="header-kiw-table">'
      html += '<td class="td-kiw-table">Nama Proyek</td>'
      html += '<td class="td-kiw-table">Sektor</td>'
      html += '<td class="td-kiw-table">Kategori Risiko</td>'
      html += '</tr>'

      $.each(collection, function (i, item) {
        var riskSelected = '';
        self.projectIds.push(item.Id);
        html += '<tr class="row-kiw-table" id="' + item.Id + '">'
        html += '<td class="td-kiw-table"> ' + item.NamaProject + ' </td>'
        html += '<td class="td-kiw-table"> ' + item.NamaSektor + ' </td>'
        if (item.ProjectRiskRegistrasi) {
          _.each(item.ProjectRiskRegistrasi, function (modelRisk) {
            riskSelected += modelRisk.RiskRegistrasi.KodeMRisk;
            riskSelected += ', ';
          });
        }
        html += '<td class="td-kiw-table"> ' + riskSelected + ' </td>'
        html += '</tr>'
      });
      html += '</table>'
      this.$('[project-table]').append(html);

      var htmlNotif = '<table style="width:100%;border-collapse: collapse;font-family: Arial,sans-serif;font-size: 9pt;color:#373637;">'
      htmlNotif += '<tr style="border: 1px solid #c9c4c4;background-color: #eeebeb;">'
      htmlNotif += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">Nama Proyek</td>'
      htmlNotif += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">Sektor</td>'
      htmlNotif += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">Kategori Risiko</td>'
      htmlNotif += '</tr>'
      $.each(collection, function (i, item) {
        var riskSelected = '';
        htmlNotif += '<tr style="border: 1px solid #c9c4c4;" id="' + item.Id + '">'
        htmlNotif += '<td style="border: 1px solid #c9c4c4;padding: 5px;"> ' + item.NamaProject + ' </td>'
        htmlNotif += '<td style="border: 1px solid #c9c4c4;padding: 5px;"> ' + item.NamaSektor + ' </td>'
        if (item.ProjectRiskRegistrasi) {
          _.each(item.ProjectRiskRegistrasi, function (modelRisk) {
            riskSelected += modelRisk.RiskRegistrasi.KodeMRisk;
            riskSelected += ', ';
          });
        }
        htmlNotif += '<td style="border: 1px solid #c9c4c4;padding: 5px;"> ' + riskSelected + ' </td>'
        htmlNotif += '</tr>'
      });
      htmlNotif += '</table>'
      this.projectHtml = htmlNotif;
    },
    generateTemplateNotification: function () {
      var self = this;
      const url = commonFunction.getCurrentDomainAndPort();
      var dataLikelihoodDetail = this.likelihoodDetailValue.results;
      var paramaterName = this.$('[name="NamaLikehood"]').val();
      var scenarioName = this.$('[name="NamaScenario"]').val();
      var keterangan = this.$('[name="NamaScenario"]').val();
      var html = '<div>'
      html += '<div style="font-size: 9pt;color:#373637;">'
      html += '<p style="margin-bottom: 0px;">Dengan ini saya mengajukan permohonan Approval untuk skenario dengan data berikut: </p>'
      html += '<p style="margin-top: 0px;font-style: italic;">Nama Skenario: ' + scenarioName + '</p>'
      html += '</div>'
      html += '<div style="font-size: 9pt;color:#373637;">'
      html += '<p style="margin-bottom: 0;">Parameter: ' + paramaterName + '</p>'
      html += '<table style="width:100%;border: 1px solid #c9c4c4;text-align: center;border-collapse: collapse;font-family: Arial,sans-serif;font-size: 9pt;color:#373637;">'
      html += '<tr style="border: 1px solid #c9c4c4;background-color: #eeebeb;">'
      html += '<td style="border: 1px solid #c9c4c4;padding: 5px;">Definisi Parameter</td>'
      html += '<td style="border: 1px solid #c9c4c4;padding: 5px;">Nilai Terendah</td>'
      html += '<td style="border: 1px solid #c9c4c4;padding: 5px;">Nilai Tertinggi</td>'
      html += '<td style="border: 1px solid #c9c4c4;padding: 5px;">Rata-Rata</td>'
      html += '</tr>'
      if (dataLikelihoodDetail) {
        _.each(dataLikelihoodDetail, function (item) {
          html += '<tr style="border: 1px solid #c9c4c4;">'
          html += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">' + item.DefinisiLikehood + '</td>'
          html += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">' + item.Lower + '</td>'
          html += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">' + item.Upper + '</td>'
          html += '<td style="border: 1px solid #c9c4c4;text-align: center;padding: 5px;">' + item.Average + '</td>'
          html += '</tr>'
        })
      }
      html += '</table>'
      html += '</div>'
      html += '<div style="margin-bottom: 20px;font-size: 9pt;color:#373637;"></div>'
      html += '<p style="margin-bottom: 0;font-size: 9pt;color:#373637;">Proyek</p>'
      html += this.projectHtml
      html += '<div style="margin-top: 25px;font-size: 9pt;color:#373637;">'
      html += '<p>Mohon dilakukan persetujuan melalui aplikasi dengan menekan <span><a href="' + url + '/#approval">link ini</a></span> dan memasukan username dan login masing-masing.</p>'
      html += '<p>Terimakasih.</p>'
      html += '</div>'

      return html;
    },
    renderValidation: function () {
      var self = this;
      this.$('[ehs-form]').bind("keypress", function (e) {
        if (e.keyCode == 13) {
          return false;
        }
      });
      this.$('[ehs-form]').bootstrapValidator({
        fields: {
          NamaScenario: {
            validators: {
              stringLength: {
                message: 'Nama Skenario harus kurang dari 100 karakter',
                max: 100
              },
              notEmpty: {
                message: 'Nama Skenario wajib diisi'
              }
            }
          },
          ChosenProject: {
            validators: {
              notEmpty: {
                message: 'Proyek wajib diisi'
              }
            }
          },
          NamaLikehood: {
            validators: {
              notEmpty: {
                message: 'Parameter wajib diisi'
              }
            }
          }
        }
      })
        .on('success.form.bv', function (e) {
          e.preventDefault();
          self.getConfirmation();
        });
    },
    sendApprove: function () {
      this.isSended = true;
      },

    sendDraft: function () {
      this.isSended = false;
      },
    getConfirmation: function () {
      var self = this;
      var templateNotif = this.generateTemplateNotification();
      var isValid = false;
      var likelihood = this.$('[name="NamaLikehood"]').val();
      if (likelihood && this.projectIds) {
        isValid = true;
      }
      if (isValid) {
        var data = this.$('[name="NamaScenario"]').val();
        var action = "add";
        var retVal;
        if  (this.isSended == true){
          retVal = confirm("Apakah anda yakin untuk " + action + " Skenario : " + data + " ?" + "\n\n\Skenario otomatis akan dilanjutkan ke proses persetujuan / approval. Sehingga Skenario ini tidak bisa diubah lagi.");
        }
        if (this.isSended == false){
          retVal = confirm("Apakah anda yakin untuk " + action + " Skenario : " + data + " ?" + "\n\n\Skenario membutuhkan pengajuan persetujuan terpisah.");
        }
        if (retVal == true) {
          this.doSave(templateNotif);
        }
        else {
          this.$('[type="submit"]').attr('disabled', false);
        }
      } else {
        commonFunction.responseWarningCannotExecute("Parameter atau Proyek wajib diisi.");
        this.$('[type="submit"]').attr('disabled', false);
      }
    },
    refetch: function () {
      eventAggregator.trigger('scenario/add/close:refecth');
    },
    doSave: function (obj) {
      debugger;
      var self = this;
      var data = commonFunction.formDataToJson(this.$('form').serializeArray());
      data.IsSend = this.isSended;
      data.ProjectId = this.projectIds;
      data.LikehoodId = this.modelLikehood.Id;
      data.TemplateNotif = obj;
      this.model.save(data);
      this.$('[type="submit"]').addClass('disabled');
      commonFunction.showLoadingSpinner();
    }
  });
});
