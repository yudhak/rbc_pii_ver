define(function(require, exports, module) {
    'use strict';

    var Model = require('backbone.model');

    module.exports = Model.extend({
        //idAttribute: 'Id',
        urlRoot: 'api/Scenario?id=1&id2=3',
        defaults: function() {
            return {
              NamaScenario : '',
              LikehoodId : '',
              NamaLikehood : '',
              IsDefault : '',
              //StatusId : '',
              CreateBy : '',
              CreateDate : '',
              UpdateBy : '',
              UpdateDate : '',
              IsDelete : '',
              DeleteDate : '',
              IsProjectSelectedAll: '',
              StatusUserApproval: ''
            }
        }
    });
});
