define(function (require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');
    var Table = require('./table/table');
    var modelDefault = require('./modelDefault');
    var Collection = require('./collection');
    var Paging = require('paging');
    debugger;
    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content tbl bg-white',
        template: _.template(template),
        initialize: function () {
            var self = this;
            this.keyword = "";
            this.table = new Table({
                collection: new Collection()
            });
            this.modelDefaultt = new modelDefault();
            this.paging = new Paging({
                collection: this.table.collection
            });
            this.listenTo(eventAggregator, 'scenario/add:fecth', function () {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'scenario/delete:fecth', function () {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'scenario/edit:fecth', function () {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'scenario/edit_default:fecth', function () {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'scenario/default:fecth', function () {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'scenario/add/close:refecth', function () {
                self.fetchData();
            });
            this.roles = JSON.parse(localStorage.getItem('roles'));
        }, 


        events: {
            'click [name="add"]': 'add',
            'click [name="edit_default"]': 'edit_default',
            'keyup #keyword': 'applyFilter',
            'click [name="btn-filter"]': 'applyFilter'
        },
        afterRender: function () {
            this.$('[obo-table-sublikelihood]').append(this.table.el);
            this.table.render();

            this.insertView('[obo-paging]', this.paging);
            this.paging.render();

            this.fetchData();
            this.setRoleAccess();
            this.setScenarioDefaultButton();
        },
        fetchData: function () {
            debugger;
            var param = [];
            param.Search = this.keyword;
            this.table.collection.fetch({
                reset: true,
                data: param,
                success: function (req, res) {
                }
            })
            this.modelDefaultt.fetch();
        },
        add: function () {
            var self = this;
            require(['./add/view'], function (View) {
                commonFunction.setDefaultModalDialogFunction(self, View);
            });
        },
        edit_default: function() {
            debugger;
            var self = this;
            var modelDe = new modelDefault();
            // var modelNew = self.table.collection.models.find(function(item){
            //     return item(item.get('IsDefault')) === true;
            //      });
            //self.modelDefault.fetch() self.modelDefaultt
            if  (self.modelDefaultt.attributes.LikehoodId == 0){
                require(['./add/view'], function (View) {
                    commonFunction.setDefaultModalDialogFunction(self, View);
                });
            }else{
                require(['./edit_default/view'], function(View) {
                    commonFunction.setDefaultModalDialogFunction(self, View, self.modelDefaultt);
                  });
            }
            
        },
        applyFilter: function () {
            var self = this;
            var param = []
            var keyword = this.$('#keyword').val();
            this.keyword = keyword;
            param.Search = keyword;
            // var keyIndex = this.$('#keyIndex option:selected').val();
            // param.SearchBy = keyIndex;
            var keyIndex2 = this.$('#keyIndex2 option:selected').val();
            param.SearchBy2 = keyIndex2;
            this.table.collection.fetch({
                reset: true,
                data: param
            })
        },
        setRoleAccess: function () {
            let self = this;
            if (this.roles) {
                $.grep(this.roles, function (item) {
                    if (item.Name.trim() == "Scenario") {
                        if (item.SubMenu) {
                            _.each(item.SubMenu, function (val) {
                                switch (val.Action) {
                                    case "add":
                                        self.$('[name="add"]').removeClass('hide');
                                        self.$('[name="edit_default"]').removeClass('hide');
                                        break;
                                }
                            });
                        }
                    }
                });
            }
        },
        setScenarioDefaultButton: function () {
            debugger;
            let self = this;
            if (this.modelDefaultt.attributes.NamaScenario == "") {
                //self.$('[name="edit_default"]').addClass('hide');
            }
        }
    });
});
