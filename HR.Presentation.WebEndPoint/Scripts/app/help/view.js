define(function(require, exports, module) {
    'use strict';

    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var eventAggregator = require('eventaggregator');
    var Model = require('./model');

    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content tbl bg-white',
        template: _.template(template),
        initialize: function() {
            var self = this;    
            this.model = new Model();
        },
        events: {
            'click [name="add"]': 'add'
        },
        afterRender: function() {
            this.fetchData();
        },

        fetchData: function() {
            var self = this

            this.model.fetch({
                reset: true,
                data: {
                },
                success: function(req, res) {
                    self.setTemplate(res);
                },
                error: function() {
                }
            });
        },

        setTemplate: function(data){
            const host = commonConfig.requestServer;
            var html = '<iframe src="'+host+'/Temp/Helps/Buku_Panduan_RBC_PII_Rev_3.pdf" width="100%" height="1300px" style="height:1300px;"></iframe>'
                html += '</iframe>'
            self.$('[viewPDF]').append(html);
        }
    });
});