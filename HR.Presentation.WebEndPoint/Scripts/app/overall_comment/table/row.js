define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./row.html');
    var commonFunction = require('commonfunction');

    module.exports = LayoutManager.extend({
        tagName: 'tr',
        template: _.template(template),
        events: {
            'click [name="Detail"]': 'Detail',
            'click [name="Default"]': 'Default'
        },
        Default: function() {
            var self = this;
            require(['./../default/view'], function(View) {
                commonFunction.setDefaultModalDialogFunction(self, View, self.model);
            });
        },
        Detail: function() {
            var self = this;
            require(['./../../detail/view'], function(View) {
                commonFunction.setDefaultModalDialogFunction(self, View, self.model);
            });
        }
    });
});
