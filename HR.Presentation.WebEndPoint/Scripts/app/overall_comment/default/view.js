define(function(require, exports, module) {
    'use strict';
    var View = require('modaldialogdefault');
    var template = require('text!./template.html');
    var Model = require('./../model');
    var ModelOverAllComment = require('./model');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var eventAggregator = require('eventaggregator');
    require('bootstrap-validator');

    module.exports = View.extend({
        template: _.template(template),
        events: {
          'click [name="setdefault"]':'getConfirmation'
        },
        initialize: function(options) {
          var self = this;
          this.colorName = options.model.get('Warna');
          var colorCommentId = this.model.get('Id');
          this.model = this.model;
          this.model = new Model();
          this.modelOverAllComment = new ModelOverAllComment();
          this.model.set(this.model.idAttribute, colorCommentId);
          this.modelOverAllComment.set(this.modelOverAllComment.idAttribute, colorCommentId);
          
          this.listenTo(this.model, 'sync', function() {
            this.listenToOnce(this.modelOverAllComment, 'sync', function(model) {
              commonFunction.responseSuccessUpdateAddDelete('Komentar Keseluruhan berhasil ditampilkan di dashboard.');
              self.$el.modal('hide');
              eventAggregator.trigger('overall_comment/default:fecth');
            }, this);
          });

          this.once('afterRender', function() {
              this.model.fetch();
          });
        },
        renderValidation: function() {
          var self = this;
          self.getConfirmation();
        },
        afterRender: function() {
          
          this.$('[data-color]').text(this.colorName);
          this.$('[data-color]').css('color', this.colorName);
        },
        getConfirmation: function(){
          var data = this.model.get('Warna');
          var action = "view to dashboard";
          var retVal = confirm("Are you sure want to " + action + " this  "+ data +" Collor ?");
          if( retVal == true ){
             this.doDefault();
          }
          else{
            this.$('[type="submit"]').attr('disabled', false);
          }
        },
        doDefault: function() {
          var data = commonFunction.formDataToJson(this.$('form').serializeArray());
          data.IsDefault = true;
          data.ColorCommentId = this.model.get('Id');
          this.modelOverAllComment.save(data);
        }
    });
});