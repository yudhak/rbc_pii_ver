define(function(require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var commonFunction = require('commonfunction');
  var Model = require('./../model');
  var eventAggregator = require('eventaggregator');
  require('bootstrap-validator');
  require('jquerymask');

  module.exports = View.extend({
      template: _.template(template),
      initialize: function(options) {
        var self = this;
        this.overAllComent = options.view;
        this.model = new Model();
        // this.model.urlRoot += `/${commonFunction.getLastSplitHash()}`;
        this.model.set('ColorCommentId', options.model.get('Id'));
        this.model.set(this.model.idAttribute, options.model.get('Id'));
        this.masterColor = options.model.get('Warna');

        this.listenTo(this.model, 'request', function() {});

        this.listenTo(this.model, 'sync error', function() {});
        
        commonFunction.setSelect2Matrix(this);

        this.listenTo(this.model, 'sync', function(model) {
          commonFunction.responseSuccessUpdateAddDelete('Komentar Keseluruhan berhasil diupdate');
          self.$el.modal('hide');
          var valueOverComentChanged = self.$('[name="OverAllComment"]').val();
          eventAggregator.trigger('overall_comment/detail/edit:fetch', valueOverComentChanged);
        });
      },
      afterRender: function() {
        this.setTemplate();
        this.renderValidation();
      },
      setTemplate: function() {
        // this.$('[name="MasterWarna"]').val(this.masterColor);
        var colorName = this.masterColor.toLowerCase();
        this.$('[name="MasterWarna"]').css("background-color", colorName);
        this.$('[name="MasterWarna"]').css("color", "black");
        this.$('[name="OverAllComment"]').val(this.overAllComent);
      },
      renderValidation: function() {
        var self = this;
        this.$('form').bind("keypress", function (e) {
              if (e.keyCode == 13) {
                  return false;
              }
          });
        this.$('form').bootstrapValidator({
            fields: {
              OverAllComment: {
                validators:{
                  notEmpty:{
                    message: 'Komentar Keseluruhan wajib diisi'
                  }
                }
              }
            }
          })
          .on('success.form.bv', function(e) {
            e.preventDefault();
            self.getConfirmation();
          });
      },
      getConfirmation: function(){
        var data = this.$('[name="OverAllComment"]').val();
        var action = "edit";
        var retVal = confirm("Apakah anda yakin ingin " + action + " Komentar Keseluruhan : "+ data +" ?");
        if( retVal == true ){
           this.doSave();
          }
        else{
          this.$('[type="submit"]').attr('disabled', false);
          }
        },
      doSave: function() {
        var data = commonFunction.formDataToJson(this.$('form').serializeArray());
        // data.ColorCommentId = this.model.get('ColorCommentId');
        data.OverAllComment = this.$('[name="OverAllComment"]').val();
        this.model.save(data);
      }
  });
});
