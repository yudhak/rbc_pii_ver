define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');
    var Table = require('./table/table');
    var Paging = require('paging');
    var Model = require('./../model');
    var ModelParent = require('./../model');
    var ModelOverAllComment = require('./model');

    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content tbl bg-white',
        template: _.template(template),
        initialize: function(options) {
            var self = this;
            this.model = new Model();
            this.overAllComment = null;
            this.modelDetail = null;
            this.modelParent = new ModelParent();
            this.modelOverAllComment = new ModelOverAllComment();
            this.parentId = commonFunction.getUrlHashSplit(2);
            this.listenTo(this.model, 'sync', () => {
                this.render();
            });
            this.modelParent.fetch({
                reset: true,
                data: {
                    id: this.parentId
                }
            });
            // this.modelOverAllComment.fetch({
            //     reset: true,
            //     data: {
            //         id: this.parentId
            //     },
            //     success: function(req, res) {
            //         self.overAllComment = res.OverAllComment;
            //     }
            // });
            this.model.set(this.model.idAttribute, commonFunction.getLastSplitHash());
            this.model.fetch();
            this.once('afterRender', () => {
                this.fetchData();
            });
            this.table = new Table();
            this.paging = new Paging({
                collection: this.table.collection
            });
            this.listenTo(eventAggregator, 'overall_comment/detail/add:fetch', function(data) {
                self.$('[name="OverAllComment"]').text(data);
                self.overAllComment = data;
                self.$('[name="edit"]').removeClass('hidden');
                self.$('[name="add"]').addClass('hidden');
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'overall_comment/detail/edit:fetch', function(data) {
                self.$('[name="OverAllComment"]').text(data);
                self.overAllComment = data;
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'overall_comment/detail/delete:fetch', function(model) {
              self.fetchData();
            });
            this.roles = JSON.parse(localStorage.getItem('roles'));
        },
        events: {
            'click [name="add"]': 'add',
            'click [name="edit"]': 'edit',
            'click [name="btn-filter"]': 'applyFilter'
        },
        afterRender: function() {
            this.fetchDataDetail();
            //this.setTemplateOverAllComment();
            this.$('[obo-table-comment]').append(this.table.el);
            this.table.render();
            this.insertView('[obo-paging]', this.paging);
            this.paging.render();
            this.setRoleAccess();
        },
        fetchDataDetail: function() {
            const self = this;
            this.modelOverAllComment.fetch({
                reset: true,
                data: {
                    id: this.parentId
                },
                success: function(req, res) {
                    self.overAllComment = res.OverAllComment;
                    if (self.overAllComment) {
                        self.$('[name="OverAllComment"]').text(self.overAllComment);
                        self.$('[name="edit"]').removeClass('hidden');
                        self.$('[name="add"]').addClass('hidden');
                    } else {
                        self.$('[name="add"]').removeClass('hidden');
                        self.$('[name="edit"]').addClass('hidden');
                    }
                }
            });
        },
        setTemplateOverAllComment: function() {
            if (this.overAllComment) {
                this.$('[name="OverAllComment"]').text(this.overAllComment);
                this.$('[name="edit"]').removeClass('hidden');
                this.$('[name="add"]').addClass('hidden');
            } else {
                this.$('[name="add"]').removeClass('hidden');
                this.$('[name="edit"]').addClass('hidden');
            }
        },
        fetchData: function() {
            this.table.collection.fetch({
                reset: true,
                data: {
                    ParentId: this.model.id
                }
            })
        },
        add: function() {
            var self = this;
            var modelParent = this.modelParent;
            require(['./add/view'], function(View) {
                commonFunction.setDefaultModalDialogFunction(self, View, modelParent);
            });
        },
        edit: function() {
            var self = this;
            var modelParent = this.modelParent;
            require(['./edit/view'], function(View) {
                commonFunction.setDefaultModalDialogFunction(self, View, modelParent, self.overAllComment);
            });
        },
        applyFilter: function() {
            var self = this;
            var param  = []
            var keyword = this.$('#keyword').val();
            param.Search = keyword;
            
            this.table.collection.fetch({
                reset: true,
                data: param
            })
        },
        setRoleAccess: function() {
            let self = this;
            if (this.roles) {
                $.grep(this.roles, function(item) {
                    if (item.Name.trim() == "KomentarKeseluruhan") {
                        if (item.SubMenu) {
                            _.each(item.SubMenu, function(val) {
                                switch (val.Action) {
                                    case "detail":
                                        if (val.SubMenu) {
                                            _.each(val.SubMenu, function(item) {
                                                switch (item.Action) {
                                                    case "add":
                                                        self.$('[name="add"]').removeClass('hide');
                                                        break;
                                                }
                                            });
                                        }
                                        break;
                                }
                            });
                        }
                    }
                });
            }
        }
    });
});