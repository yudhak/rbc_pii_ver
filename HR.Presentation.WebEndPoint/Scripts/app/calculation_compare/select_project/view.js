define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var commonFunction = require('commonfunction');
  var Table = require('./table/table');
  var Collection = require('./collection');
  var Paging = require('paging');
  var eventAggregator = require('eventaggregator');

  module.exports = View.extend({
    template: _.template(template),
    initialize: function (options) {
      var self = this;
      this.scenarioName = options.model[0].ScenarioName;
      this.projectIdChosenBefore = null;
      this.projectIds = [];
      this.projects = options.model;
      if (options.view !== "")
        this.projectIds = options.view;
      this.listenTo(this.model, 'sync', function (model) {
        self.$el.modal('hide');
      });
      this.listenTo(eventAggregator, 'calculation_compare/select_scneario', function (status) {
        self.isSelectedAll = status;
      });
    },
    afterRender: function () {
      this.setTemplate();
    },
    setTemplate: function () {
      let self = this;
      if (this.projects) {
        let html = '<table>'
        html += '<tr class="header-kiw-table">'
        html += '<th class="td-kiw-table"><input type="checkbox" id="selectall">All</th>'
        html += '<th class="td-kiw-table">Nama Proyek</th>'
        html += '<th class="td-kiw-table">Sektor</th>'
        html += '<th class="td-kiw-table">Tahapan Penjaminan</th>'
        html += '</tr>'
        for (const item of this.projects) {
          html += '<tr class="row-kiw-table" data-row id="' + item.Id + '">'
          if (this.isSelectedAll) {
            html += '<td class="td-kiw-table"><input type="checkbox" data-item data-project="' + item.Id + '" id="' + item.Id + '" checked></td>'
          } else {
            if (item.IsChosen) {
              html += '<td class="td-kiw-table"><input type="checkbox" data-item data-project="' + item.Id + '" id="' + item.Id + '" checked></td>'
            } else {
              html += '<td class="td-kiw-table"><input type="checkbox" data-item data-project="' + item.Id + '" id="' + item.Id + '"></td>'
            }
          }
          html += '<td class="td-kiw-table"> ' + item.ProjectName + ' </td>'
          html += '<td class="td-kiw-table"> ' + item.SectorName + ' </td>'
          html += '<td class="td-kiw-table"> ' + item.TahapanPenjaminanName + ' </td>'
          html += '</tr>'
        }
        html += '</table>'
        this.$('[project-table]').append(html);
        this.$('[data-title]').text('Pilih proyek untuk skenario: ' + this.scenarioName);
      }
    },
    events: {
      'click [data-project]': 'getChosenProject',
      'click [btn-save]': 'saveChosenProject',
      'click #selectall': 'selectAllProject'
    },
    selectAllProject: function () {
      if (this.$('[type="checkbox"]').prop('checked')) {
        this.$('input[type="checkbox"]').prop('checked', true);
        this.$('[data-item]').prop('disabled', true);
        this.isSelectedAll = true;

        if (this.projects) {
          this.projectIds = [];
          for (const item of this.projects) {
            item.IsChosen = true;
            this.projectIds.push(item.Id);
          }
        }
      } else {
        this.$('[type="checkbox"]').prop('checked', false);
        this.$('[data-item]').prop('disabled', false);
        this.isSelectedAll = false;
        if (this.projects) {
          for (const item of this.projects) {
            item.IsChosen = false;
            this.projectIds = [];
          }
        }
      }
    },
    getChosenProject: function (e) {
      let self = this;
      var projectChosen = new Set();
      const isChosen = $('[data-project="' + e.target.id + '"]').prop('checked');
      if (isChosen) {
        this.projectIds.push(e.target.id);
      } else {
        if (this.projectIds.length > 0) {
          const projectId = e.target.id;
          this.projectIds = this.projectIds.filter(e => e != projectId);
        }
      }
    },
    saveChosenProject: function () {
      eventAggregator.trigger('calculation_compare/select_project:get', this.projectIds, this.projects, this.isSelectedAll);
      this.$el.modal('hide');
    }
  });
});