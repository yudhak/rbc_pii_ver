define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var commonFunction = require('commonfunction');
  var commonConfig = require('commonconfig');
  var Model = require('./model');
  var ModelCompareCalculation = require('./../model');
  var eventAggregator = require('eventaggregator');
  require('select2');

  module.exports = View.extend({
    template: _.template(template),
    initialize: function () {
      var self = this;
      this.projectList = null;
      this.projectIds = null;
      this.projectChosen = null;
      this.isAllProjectSelected = false;
      this.model = new Model();
      this.modelCompareCalculation = new ModelCompareCalculation();
      this.param = [];
      this.listenTo(eventAggregator, 'calculation_compare/select_project:get', function (projectIds, projects, isAllProjectSelected) {
        self.projectIds = projectIds;
        self.projectChosen = projects;
        self.isAllProjectSelected = isAllProjectSelected;
        self.model.set('IsAllProjectSelected', isAllProjectSelected);
        self.setParam(self.projectChosen[0].ScenarioId, projectIds);
        self.setTemplate(projectIds, projects);
      });
    },
    afterRender: function () {
      this.fetchScenario();
    },
    events: {
      'change [name="FirstScenario"], [name="SecondScenario"], [name="ThirdScenario"]': 'getScenarioDetail',
      'click [btn-change-project-1], [btn-change-project-2], [btn-change-project-3]': 'reChooseProject',
      'click [btn-save]': 'doCalculate'
    },
    setParam: function (scenarioId, projectIds) {
      if (this.param.length > 0) {
        this.param = this.param.filter(e => e.ScenarioId != scenarioId);
        let obj = {};
        let ids = [];
        if (projectIds) {
          for (const i of projectIds) {
            ids.push(parseInt(i));
          }
        }
        obj.ScenarioId = scenarioId;
        obj.ProjectId = ids;
        this.param.push(obj);
      } else {
        let obj = {};
        let ids = [];
        if (projectIds) {
          for (const i of projectIds) {
            ids.push(parseInt(i));
          }
        }
        obj.ScenarioId = scenarioId;
        obj.ProjectId = ids;
        this.param.push(obj);
      }
    },
    fetchScenario: function () {
      let self = this;
      commonFunction.showLoadingSpinner();
      this.modelCompareCalculation.fetch({
        reset: true,
        data: {
          IsPagination: false
        },
        success: function (req, res) {
          self.renderScenarioList(res);
          self.renderSecondScenario(res);
          self.renderThirdScenario(res);
          commonFunction.closeLoadingSpinner();
        }
      });
    },
    renderScenarioList: function (data) {
      let html = '<select class="form-control select" validation name="FirstScenario">';
      html += '<option value="">--Pilih Skenario--</option>';
      if (data) {
        _.each(data, function (item) {
          html += '<option value="' + item.Id + '">' + item.NamaScenario + '</option>';
        });
      }
      html += '</select>';
      this.$('[data-scenario-list]').append(html);
    },
    renderSecondScenario: function (data) {
      let html = '<select class="form-control select" validation name="SecondScenario">';
      html += '<option value="">--Pilih Skenario--</option>';
      if (data) {
        _.each(data, function (item) {
          html += '<option value="' + item.Id + '">' + item.NamaScenario + '</option>';
        });
      }
      html += '</select>';
      this.$('[data-second-scenario-list]').append(html);
    },
    renderThirdScenario: function (data) {
      let html = '<select class="form-control select" validation name="ThirdScenario">';
      html += '<option value="">--Pilih Skenario--</option>';
      if (data) {
        _.each(data, function (item) {
          html += '<option value="' + item.Id + '">' + item.NamaScenario + '</option>';
        });
      }
      html += '</select>';
      this.$('[data-third-scenario-list]').append(html);
    },
    getScenarioDetail: function (e) {
      const self = this;
      const attrName = e.target.name;
      const scenarioId = this.$('[name="' + attrName + '"]').val();
      this.$('[validation]').each(function (i, item) {
      });
      if (scenarioId != "") {
        commonFunction.showLoadingSpinner();
        this.model.fetch({
          reset: true,
          data: {
            id: scenarioId
          },
          success: function (req, res) {
            self.projectList = res;
            self.chooseProject(res, attrName);
          }
        });
      }
    },
    chooseProject: function (data, attrName) {
      var self = this;
      let projects = [];
      const scenarioName = data.NamaScenario;
      if (data) {
        for (let i of data.ScenarioDetail) {
          let obj = {};
          if (!i.IsDelete) {
            obj.Id = i.Project.Id;
            obj.ProjectName = i.Project.NamaProject;
            obj.SectorName = i.Project.NamaSektor;
            obj.ScenarioId = data.Id;
            obj.ScenarioName = scenarioName;
            obj.TahapanPenjaminanId = i.Project.TahapanId;
            obj.TahapanPenjaminanName = i.Project.NamaTahapan;
            projects.push(obj);
          }
        }
      }
      require(['./../select_project/view'], function (View) {
        if (attrName == "FirstScenario") {
          commonFunction.setDefaultModalDialogFunction(self, View, projects, self.projectIdFirstScenario);
        } else if (attrName == "SecondScenario") {
          commonFunction.setDefaultModalDialogFunction(self, View, projects, self.projectIdSecondScenario);
        } else {
          commonFunction.setDefaultModalDialogFunction(self, View, projects, self.projectIdThirdScenario);
        }
      });
      commonFunction.closeLoadingSpinner();
    },
    reGetScenarioDetail: function (id, scenarioFor, isAllProjectSelected) {
      const self = this;
      let projects = [];
      const scenarioId = id;
      this.model.fetch({
        reset: true,
        data: {
          id: scenarioId
        },
        success: function (req, res) {
          self.projectList = res;
          if (self.projectList.ScenarioDetail) {
            for (const i of self.projectList.ScenarioDetail) {
              let obj = {};
              if (!i.IsDelete) {
                if (scenarioFor == 1) {
                  if (self.projectIdFirstScenario) {
                    const isFound = $.inArray(i.Project.Id.toString(), self.projectIdFirstScenario);
                    if (isFound != -1) {
                      obj.Id = i.Project.Id;
                      obj.IsChosen = true;
                      obj.ProjectName = i.Project.NamaProject;
                      obj.SectorName = i.Project.NamaSektor;
                      obj.ScenarioId = self.projectList.Id;
                      obj.ScenarioName = self.projectList.NamaScenario;
                      obj.TahapanPenjaminanId = i.Project.TahapanId;
                      obj.TahapanPenjaminanName = i.Project.NamaTahapan;
                      projects.push(obj);
                    } else {
                      if (self.isAllProjectSelected) {
                        obj.Id = i.Project.Id;
                        obj.IsChosen = true;
                        obj.ProjectName = i.Project.NamaProject;
                        obj.SectorName = i.Project.NamaSektor;
                        obj.ScenarioId = self.projectList.Id;
                        obj.ScenarioName = self.projectList.NamaScenario;
                        obj.TahapanPenjaminanId = i.Project.TahapanId;
                        obj.TahapanPenjaminanName = i.Project.NamaTahapan;
                        projects.push(obj);
                      } else {
                        obj.Id = i.Project.Id;
                        obj.IsChosen = false;
                        obj.ProjectName = i.Project.NamaProject;
                        obj.SectorName = i.Project.NamaSektor;
                        obj.ScenarioId = self.projectList.Id;
                        obj.ScenarioName = self.projectList.NamaScenario;
                        obj.TahapanPenjaminanId = i.Project.TahapanId;
                        obj.TahapanPenjaminanName = i.Project.NamaTahapan;
                        projects.push(obj);
                      }
                    }
                  }
                } else if (scenarioFor == 2) {
                  if (self.projectIdSecondScenario) {
                    const isFound = $.inArray(i.Project.Id.toString(), self.projectIdSecondScenario);
                    if (isFound != -1) {
                      obj.Id = i.Project.Id;
                      obj.IsChosen = true;
                      obj.ProjectName = i.Project.NamaProject;
                      obj.SectorName = i.Project.NamaSektor;
                      obj.ScenarioId = self.projectList.Id;
                      obj.ScenarioName = self.projectList.NamaScenario;
                      obj.TahapanPenjaminanId = i.Project.TahapanId;
                      obj.TahapanPenjaminanName = i.Project.NamaTahapan;
                      projects.push(obj);
                    } else {
                      obj.Id = i.Project.Id;
                      obj.IsChosen = false;
                      obj.ProjectName = i.Project.NamaProject;
                      obj.SectorName = i.Project.NamaSektor;
                      obj.ScenarioId = self.projectList.Id;
                      obj.ScenarioName = self.projectList.NamaScenario;
                      obj.TahapanPenjaminanId = i.Project.TahapanId;
                      obj.TahapanPenjaminanName = i.Project.NamaTahapan;
                      projects.push(obj);
                    }
                  }
                } else {
                  if (self.projectIdThirdScenario) {
                    const isFound = $.inArray(i.Project.Id.toString(), self.projectIdThirdScenario);
                    if (isFound != -1) {
                      obj.Id = i.Project.Id;
                      obj.IsChosen = true;
                      obj.ProjectName = i.Project.NamaProject;
                      obj.SectorName = i.Project.NamaSektor;
                      obj.ScenarioId = self.projectList.Id;
                      obj.ScenarioName = self.projectList.NamaScenario;
                      obj.TahapanPenjaminanId = i.Project.TahapanId;
                      obj.TahapanPenjaminanName = i.Project.NamaTahapan;
                      projects.push(obj);
                    } else {
                      obj.Id = i.Project.Id;
                      obj.IsChosen = false;
                      obj.ProjectName = i.Project.NamaProject;
                      obj.SectorName = i.Project.NamaSektor;
                      obj.ScenarioId = self.projectList.Id;
                      obj.ScenarioName = self.projectList.NamaScenario;
                      obj.TahapanPenjaminanId = i.Project.TahapanId;
                      obj.TahapanPenjaminanName = i.Project.NamaTahapan;
                      projects.push(obj);
                    }
                  }
                }
              }
            }
          }
          require(['./../select_project/view'], function (View) {
            if (scenarioFor == 1) {
              eventAggregator.trigger('calculation_compare/select_scneario', self.model.get('IsAllProjectSelected'));
              commonFunction.setDefaultModalDialogFunction(self, View, projects, self.projectIdFirstScenario, self.model.get('IsAllProjectSelected'));
            } else if (scenarioFor == 2) {
              commonFunction.setDefaultModalDialogFunction(self, View, projects, self.projectIdSecondScenario);
            } else {
              commonFunction.setDefaultModalDialogFunction(self, View, projects, self.projectIdThirdScenario);
            }
          });
        }
      });
    },
    reChooseProject: function (e) {
      let self = this;
      const scenarioId = e.target.lang;
      const scenarioFor = e.target.id;
      this.reGetScenarioDetail(scenarioId, scenarioFor, this.isAllProjectSelected);
    },
    setTemplate: function (projectIdChosen, objectProject) {
      if (objectProject) {
        if (this.param.length == 1) {
          this.projectIdFirstScenario = projectIdChosen;
        } else if (this.param.length == 2) {
          this.projectIdSecondScenario = projectIdChosen;
        } else {
          this.projectIdThirdScenario = projectIdChosen;
        }
        this.$('[data-project-list-' + this.param.length + ']').empty();
        let html = '<div class="col-lg-1"><button type="button" lang="' + objectProject[0].ScenarioId + '" btn-change-project-' + this.param.length + ' id="' + this.param.length + '">Ubah Proyek</button></div>'
        html += '<div class="col-lg-11" style="padding-left: 20px;">'
        html += '<table>'
        html += '<tr class="header-kiw-table">'
        html += '<td class="td-kiw-table">Nama Proyek</td>'
        html += '<td class="td-kiw-table">Sektor</td>'
        html += '<td class="td-kiw-table">Tahapan Penjaminan</td>'
        html += '</tr>'
        for (const i of projectIdChosen) {
          for (const item of objectProject) {
            if (item.Id == i) {
              html += '<tr class="row-kiw-table" id="' + item.Id + '">'
              html += '<td class="td-kiw-table"> ' + item.ProjectName + ' </td>'
              html += '<td class="td-kiw-table"> ' + item.SectorName + ' </td>'
              html += '<td class="td-kiw-table"> ' + item.TahapanPenjaminanName + ' </td>'
              html += '</tr>'
            }
          }
        }
        html += '</table>'
        html += '</div>'
        this.$('[data-project-list-' + this.param.length + ']').append(html);
      }
    },
    scenarioValidation: function () {
      const self = this;
      let isValid = false;
      let isValued = 0;
      this.$('[validation]').each(function (i, item) {
        const attrName = item.name;
        const attrValue = self.$('[name="' + attrName + '"]').val();
        if (attrValue != "") {
          isValid = true;
          isValued += 1;
        } else {
          isValid = false;
        }
      });
      return isValued;
    },
    doCalculate: function () {
      const isValid = this.scenarioValidation();
      if (isValid >= 1) {
        if (this.projectIds != null) {
          let data = {};
          data.ScenarioProject = this.param;
          eventAggregator.trigger('calculation_compare/scenario_selected:fecth', data);
          this.$el.modal('hide');
        } else {
          commonFunction.responseWarningCannotExecute("Proyek wajib dipilih.");
        }
      } else {
        commonFunction.responseWarningCannotExecute("Skenario wajib dipilih (minimal 1 skenario).");
      }
    }
  });
});