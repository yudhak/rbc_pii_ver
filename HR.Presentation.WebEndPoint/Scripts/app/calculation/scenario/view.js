define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var commonFunction = require('commonfunction');
  var Model = require('./../model');
  var ModelScenario = require('./../../calculation_compare/model');
  var ModelTahapan = require('./../../master/tahapan_project/model');
  var ModelProject = require('./../../master/project/model');
  var CollectionProject = require('./../../master/project/collection');
  var eventAggregator = require('eventaggregator');
  require('select2');

  module.exports = View.extend({
    template: _.template(template),
    initialize: function () {
      var self = this;
      this.objTahapan = {};
      this.isSelectedAll = false;
      this.isSecondTahapanSelectedAll = false;
      this.isThirdTahapanSelectedAll = false;
      this.tahapanIds = [];
      this.secondTahapanIds = [];
      this.thirdTahapanIds = [];
      this.firstProjectIds = [];
      this.secondProjectIds = [];
      this.thirdProjectIds = [];
      this.objProject = {};
      this.objSecondProject = {};
      this.objThirdProject = {};
      this.finalProjectFirstScenario = {};
      this.finalProjectSecondScenario = {};
      this.finalProjectThirdScenario = {};
      this.finalParam = [];
      this.firstScenarioId = 0;
      this.secondScenarioId = 0;
      this.thirdScenarioId = 0;
      this.scenarioIdDefault = 0;
      this.model = new Model();
      this.modelTahapan = new ModelTahapan();
      this.modelProject = new ModelProject();
      this.modelScenario = new ModelScenario();
      this.collectionProject = new CollectionProject();
      this.listenTo(this.model, 'request', function () { });
      this.listenTo(this.model, 'sync error', function () { });
    },
    afterRender: function () {
      this.fetchScenario();
      //this.fetchTahapan();
    },
    events: {
      // 'change [name="FirstScenario"], [name="SecondScenario"], [name="ThirdScenario"]': 'doValidation',
      'change [name="FirstScenario"], [name="SecondScenario"], [name="ThirdScenario"]': 'fetchTahapan',
      'click [btn-save]': 'doCalculate', //'renderCalculation',
      'change [data-item-tahapan], [data-item-tahapan2], [data-item-tahapan3]': 'getProjectByTahapan',
      'click #firstTahapanSelectall': 'selectFirstAllTahapan',
      'click #selectallProject': 'selectAllProject',
      'click #selectallSecondProject': 'selectAllSecondProject',
      'click #selectallThirdProject': 'selectAllThirdProject',
      'change [data-item-proyek], [data-item-proyek2], [data-item-proyek3]': 'setChosenProject',
      'click #secondTahapanSelectall': 'selectAllSecondTahapan',
      'click #thirdTahapanSelectall': 'selectAllThirdTahapan',
    },
    fetchScenario: function () {
      let self = this;
      commonFunction.showLoadingSpinner();
      this.modelScenario.fetch({
        reset: true,
        data: {
          IsPagination: false,
          IsApproved: true
        },
        success: function (req, res) {
          self.renderScenarioList(res);
          self.renderSecondScenario(res);
          self.renderThirdScenario(res);
          commonFunction.closeLoadingSpinner();
        }
      });
    },
    fetchTahapan: function (e) {
      const scenario = e.currentTarget.lang;
      const scenarioId = e.currentTarget.value;
      const self = this;

      switch (scenario) {
        case 'scenario1':
          this.$('[data-tahapan-penjaminan-for-first-scenario]').empty();
          this.$('[data-project-first-scenario]').empty();
          this.resetObjectParam(scenario);
          this.firstScenarioId = scenarioId;
          break;
        case 'scenario2':
          this.$('[data-tahapan-penjaminan-for-second-scenario]').empty();
          this.$('[data-project-second-scenario]').empty();
          this.resetObjectParam(scenario);
          this.secondScenarioId = scenarioId;
          break;
        case 'scenario3':
          this.$('[data-tahapan-penjaminan-for-third-scenario]').empty();
          this.$('[data-project-third-scenario]').empty();
          this.resetObjectParam(scenario);
          this.thirdScenarioId = scenarioId;
          break;
      }

      this.modelTahapan.fetch({
        reset: true,
        data: {
          IsPagination: true
        },
        success: function (req, res) {
          self.renderTahapan(res, scenario, scenarioId);
          self.objTahapan = res;
        }
      })
    },
    renderScenarioList: function (data) {
      const self = this;
      let html = '<select lang="scenario1" class="form-control select" validation id="scenario1" name="FirstScenario">';
      html += '<option value="">--Pilih Skenario--</option>';
      if (data) {
        _.each(data, function (item) {
          if (item.IsDefault) {
            self.scenarioIdDefault = item.Id;
            html += '<option value="' + item.Id + '" lang="' + item.IsDefault + '" style="background-color: yellow;">' + item.NamaScenario + '</option>';
          } else {
            html += '<option value="' + item.Id + '" lang="' + item.IsDefault + '">' + item.NamaScenario + '</option>';
          }
        });
      }
      html += '</select>';
      this.$('[data-scenario-list]').append(html);
    },
    renderSecondScenario: function (data) {
      let html = '<select lang="scenario2" class="form-control select" id="scenario2" validation name="SecondScenario">';
      html += '<option value="">--Pilih Skenario--</option>';
      if (data) {
        _.each(data, function (item) {
          if (item.IsDefault) {
            html += '<option value="' + item.Id + '" lang="' + item.IsDefault + '" style="background-color: yellow;">' + item.NamaScenario + '</option>';
          } else {
            html += '<option value="' + item.Id + '" lang="' + item.IsDefault + '">' + item.NamaScenario + '</option>';
          }
        });
      }
      html += '</select>';
      this.$('[data-second-scenario-list]').append(html);
    },
    renderThirdScenario: function (data) {
      let html = '<select lang="scenario3" class="form-control select" validation id="scenario3" name="ThirdScenario">';
      html += '<option value="">--Pilih Skenario--</option>';
      if (data) {
        _.each(data, function (item) {
          if (item.IsDefault) {
            html += '<option value="' + item.Id + '" lang="' + item.IsDefault + '" style="background-color: yellow;">' + item.NamaScenario + '</option>';
          } else {
            html += '<option value="' + item.Id + '" lang="' + item.IsDefault + '">' + item.NamaScenario + '</option>';
          }
        });
      }
      html += '</select>';
      this.$('[data-third-scenario-list]').append(html);
    },
    getProjectByTahapan: function (e) {
      const scenario = e.currentTarget.lang;
      const scenarioId = e.currentTarget.value;
      let isChosen = false;
      switch (scenario) {
        case 'scenario1':
          isChosen = this.$('[data-tahapan=' + e.currentTarget.id + ']').prop('checked');
          if (isChosen) {
            this.tahapanIds.push(parseInt(e.currentTarget.id));
          } else {
            if (this.tahapanIds.length > 0) {
              const index = this.tahapanIds.indexOf(parseInt(e.currentTarget.id));
              if (index > -1) {
                this.tahapanIds.splice(index, 1);
              }
            };
          }
          break;
        case 'scenario2':
          isChosen = this.$('[data-tahapan2=' + e.currentTarget.id + ']').prop('checked');
          if (isChosen) {
            this.secondTahapanIds.push(parseInt(e.currentTarget.id));
          } else {
            if (this.secondTahapanIds.length > 0) {
              const index = this.secondTahapanIds.indexOf(parseInt(e.currentTarget.id));
              if (index > -1) {
                this.secondTahapanIds.splice(index, 1);
              }
            };
          }
          break;
        case 'scenario3':
          isChosen = this.$('[data-tahapan3=' + e.currentTarget.id + ']').prop('checked');
          if (isChosen) {
            this.thirdTahapanIds.push(parseInt(e.currentTarget.id));
          } else {
            if (this.thirdTahapanIds.length > 0) {
              const index = this.thirdTahapanIds.indexOf(parseInt(e.currentTarget.id));
              if (index > -1) {
                this.thirdTahapanIds.splice(index, 1);
              }
            };
          }
          break;
      }
      commonFunction.showLoadingSpinner();
      this.getProject(scenario, scenarioId);
    },
    selectFirstAllTahapan: function (e) {
      const scenario = e.currentTarget.lang;
      const scenarioId = e.currentTarget.value;
      if (this.$('[type="checkbox"]').prop('checked')) {
        this.$('input[type="checkbox"][data-item-tahapan]').prop('checked', true);
        this.$('[data-item]').prop('disabled', true);
        this.isSelectedAll = true;
        this.tahapanIds = [];
        if (this.objTahapan.length > 0) {
          for (let i = 0; i < this.objTahapan.length; i++) {
            this.tahapanIds.push(parseInt(this.objTahapan[i].Id));
          }
        }
      } else {
        this.$('[type="checkbox"][data-item-tahapan]').prop('checked', false);
        this.isSelectedAll = false;
        this.tahapanIds = [];
      }
      this.resetObjectParam(scenario);
      commonFunction.showLoadingSpinner();
      this.getProject(scenario, scenarioId);
    },
    renderTahapan: function (data, whichScenario, scenarioId) {
      let html = '<table>'
      html += '<tr class="header-kiw-table">'
      switch (whichScenario) {
        case 'scenario1':
          html += '<th class="td-kiw-table"><input lang="scenario1" type="checkbox" id="firstTahapanSelectall" value="' + scenarioId + '">All</th>'
          break;
        case 'scenario2':
          html += '<th class="td-kiw-table"><input lang="scenario2" type="checkbox" id="secondTahapanSelectall" value="' + scenarioId + '">All</th>'
          break;
        case 'scenario3':
          html += '<th class="td-kiw-table"><input lang="scenario3" type="checkbox" id="thirdTahapanSelectall" value="' + scenarioId + '">All</th>'
          break;
      }
      html += '<th class="td-kiw-table">Nama Tahapan Penjaminan</th>'
      html += '</tr>'

      if (data) {
        for (let i = 0; i < data.length; i++) {
          html += '<tr class="row-kiw-table" data-row id="' + parseInt(data[i].Id) + '">'
          switch (whichScenario) {
            case 'scenario1':
              html += '<td class="td-kiw-table checkbox-center"><input type="checkbox" lang="scenario1" data-item-tahapan data-tahapan="' + data[i].Id + '" id="' + data[i].Id + '" value="' + scenarioId + '"></td>'
              break;
            case 'scenario2':
              html += '<td class="td-kiw-table checkbox-center"><input type="checkbox" lang="scenario2" data-item-tahapan2 data-tahapan2="' + data[i].Id + '" id="' + data[i].Id + '" value="' + scenarioId + '"></td>'
              break;
            case 'scenario3':
              html += '<td class="td-kiw-table checkbox-center"><input type="checkbox" lang="scenario3" data-item-tahapan3 data-tahapan3="' + data[i].Id + '" id="' + data[i].Id + '" value="' + scenarioId + '"></td>'
              break;
          }
          html += '<td class="td-kiw-table"> ' + data[i].NamaTahapan + ' </td>'
          html += '</tr>'
        }
      } else {
        html += '<tr class="row-kiw-table"><td col-span="2">Data tahapan penjaminan tidak ditemukan</td></tr>'
      }

      switch (whichScenario) {
        case 'scenario1':
          this.$('[data-tahapan-penjaminan-for-first-scenario]').empty();
          this.$('[data-tahapan-penjaminan-for-first-scenario]').append(html);
          break;
        case 'scenario2':
          this.$('[data-tahapan-penjaminan-for-second-scenario]').empty();
          this.$('[data-tahapan-penjaminan-for-second-scenario]').append(html);
          break;
        case 'scenario3':
          this.$('[data-tahapan-penjaminan-for-third-scenario]').empty();
          this.$('[data-tahapan-penjaminan-for-third-scenario]').append(html);
          break;
      }
    },
    fetchProject: function (whichScenario, tahapanId, scenarioId) {
      const self = this;
      let tahapanIdBasedOnScenario = null;
      switch (whichScenario) {
        case 'scenario1':
          tahapanIdBasedOnScenario = tahapanId.toString();
          break;
        case 'scenario2':
          tahapanIdBasedOnScenario = tahapanId.toString();
          break;
        case 'scenario3':
          tahapanIdBasedOnScenario = tahapanId.toString();
          break;
      }
      this.modelProject.fetch({
        reset: true,
        data: {
          isProjectByTahapan: true,
          scenarioId: scenarioId,
          tahapanIds: tahapanIdBasedOnScenario
        },
        success: function (req, res) {
          switch (whichScenario) {
            case 'scenario1':
              self.renderProject(res, whichScenario, scenarioId);
              self.objProject = res;
              break;
            case 'scenario2':
              self.renderProject(res, whichScenario, scenarioId);
              self.objSecondProject = res;
              break;
            case 'scenario3':
              self.renderProject(res, whichScenario, scenarioId);
              self.objThirdProject = res;
              break;
          }
        }
      });
    },
    getProject: function (whichScenario, scenarioId) {
      const self = this;
      let scenario = '';
      switch (whichScenario) {
        case 'scenario1':
          scenario = 'scenario1';
          if (this.tahapanIds.length < 1) {
            switch (whichScenario) {
              case 'scenario1':
                this.$('[data-project-first-scenario]').empty();
                break;
              case 'scenario2':
                this.$('[data-project-second-scenario]').empty();
                break;
              case 'scenario3':
                this.$('[data-project-third-scenario]').empty();
                break;
            }
            commonFunction.responseWarningCannotExecute("Tahapan Penjaminan wajib dipilih.");
          } else {
            this.fetchProject(scenario, this.tahapanIds, scenarioId);
          }
          break;
        case 'scenario2':
          scenario = 'scenario2';
          if (this.secondTahapanIds.length < 1) {
            switch (whichScenario) {
              case 'scenario1':
                this.$('[data-project-first-scenario]').empty();
                break;
              case 'scenario2':
                this.$('[data-project-second-scenario]').empty();
                break;
              case 'scenario3':
                this.$('[data-project-third-scenario]').empty();
                break;
            }
            commonFunction.responseWarningCannotExecute("Tahapan Penjaminan wajib dipilih.");
          } else {
            this.fetchProject(scenario, this.secondTahapanIds, scenarioId);
          }
          break;
        case 'scenario3':
          scenario = 'scenario3';
          if (this.thirdTahapanIds.length < 1) {
            switch (whichScenario) {
              case 'scenario1':
                this.$('[data-project-first-scenario]').empty();
                break;
              case 'scenario2':
                this.$('[data-project-second-scenario]').empty();
                break;
              case 'scenario3':
                this.$('[data-project-third-scenario]').empty();
                break;
            }
            commonFunction.responseWarningCannotExecute("Tahapan Penjaminan wajib dipilih.");
          } else {
            this.fetchProject(scenario, this.thirdTahapanIds, scenarioId);
          }
          break;
      }
    },
    renderProject: function (data, whichScenario, scenarioId) {
      switch (whichScenario) {
        case 'scenario1':
          this.$('[data-project-first-scenario]').empty();
          break;
        case 'scenario2':
          this.$('[data-project-second-scenario]').empty();
          break;
        case 'scenario3':
          this.$('[data-project-third-scenario]').empty();
          break;
      };
      let html = '';
      if (data.length != 0) {
        html += '<table>'
        html += '<tr class="header-kiw-table">'
        switch (whichScenario) {
          case 'scenario1':
            html += '<th class="td-kiw-table"><input type="checkbox" id="selectallProject" value="' + scenarioId + '">All</th>'
            break;
          case 'scenario2':
            html += '<th class="td-kiw-table"><input type="checkbox" id="selectallSecondProject" value="' + scenarioId + '">All</th>'
            break;
          case 'scenario3':
            html += '<th class="td-kiw-table"><input type="checkbox" id="selectallThirdProject" value="' + scenarioId + '">All</th>'
            break;
        };

        html += '<th class="td-kiw-table text-center">Proyek</th>'
        html += '<th class="td-kiw-table text-center">Sektor</th>'
        html += '<th class="td-kiw-table text-center">Tahapan Penjaminan</th>'
        html += '</tr>'

        for (let i = 0; i < data.length; i++) {
          html += '<tr class="row-kiw-table" data-row id="' + parseInt(data[i].Id) + '">'
          switch (whichScenario) {
            case 'scenario1':
              html += '<td class="td-kiw-table checkbox-center"><input lang="' + whichScenario + '" type="checkbox" data-item-proyek data-proyek="' + data[i].Id + '" id="' + data[i].Id + '" value="' + scenarioId + '"></td>'
              break;
            case 'scenario2':
              html += '<td class="td-kiw-table checkbox-center"><input lang="' + whichScenario + '" type="checkbox" data-item-proyek2 data-proyek2="' + data[i].Id + '" id="' + data[i].Id + '" value="' + scenarioId + '"></td>'
              break;
            case 'scenario3':
              html += '<td class="td-kiw-table checkbox-center"><input lang="' + whichScenario + '" type="checkbox" data-item-proyek3 data-proyek3="' + data[i].Id + '" id="' + data[i].Id + '" value="' + scenarioId + '"></td>'
              break;
          };

          html += '<td class="td-kiw-table">' + data[i].NamaProject + '</td>'
          html += '<td class="td-kiw-table">' + data[i].NamaSektor + '</td>'
          html += '<td class="td-kiw-table">' + data[i].NamaTahapan + '</td>'
          html += '</tr>'
        }
        html += '</table>'
      } else {
        html += '<p>Data Proyek tidak ditemukan</p>'
      }
      switch (whichScenario) {
        case 'scenario1':
          this.$('[data-project-first-scenario]').empty();
          this.$('[data-project-first-scenario]').append(html);
          break;
        case 'scenario2':
          this.$('[data-project-second-scenario]').empty();
          this.$('[data-project-second-scenario]').append(html);
          break;
        case 'scenario3':
          this.$('[data-project-third-scenario]').empty();
          this.$('[data-project-third-scenario]').append(html);
          break;
      };
      commonFunction.closeLoadingSpinner();
    },
    selectAllProject: function (e) {
      const scenarioId = parseInt(e.currentTarget.value);

      if (this.$('#selectallProject').prop('checked')) {
        this.$('input[type="checkbox"][data-item-proyek]').prop('checked', true);
        this.isProjectSelectedAll = true;
        this.firstProjectIds = [];
        if (this.objProject.length > 0) {
          for (let i = 0; i < this.objProject.length; i++) {
            this.firstProjectIds.push(parseInt(this.objProject[i].Id));
          }
        }
      } else {
        this.$('input[type="checkbox"][data-item-proyek]').prop('checked', false);
        this.$('#selectallProject').prop('checked', false);
        this.isProjectSelectedAll = false;
        this.firstProjectIds = [];
      }
    },
    selectAllSecondProject: function () {
      if (this.$('#selectallSecondProject').prop('checked')) {
        this.$('input[type="checkbox"][data-item-proyek2]').prop('checked', true);
        this.secondProjectIds = [];
        if (this.objSecondProject.length > 0) {
          for (let i = 0; i < this.objSecondProject.length; i++) {
            this.secondProjectIds.push(parseInt(this.objSecondProject[i].Id));
          }
        }
      } else {
        this.$('input[type="checkbox"][data-item-proyek2]').prop('checked', false);
        this.$('#selectallSecondProject').prop('checked', false);
        this.secondProjectIds = [];
      }
    },
    selectAllThirdProject: function () {
      if (this.$('#selectallThirdProject').prop('checked')) {
        this.$('input[type="checkbox"][data-item-proyek3]').prop('checked', true);
        this.thirdProjectIds = [];
        if (this.objThirdProject.length > 0) {
          for (let i = 0; i < this.objThirdProject.length; i++) {
            this.thirdProjectIds.push(parseInt(this.objThirdProject[i].Id));
          }
        }
      } else {
        this.$('input[type="checkbox"][data-item-proyek3]').prop('checked', false);
        this.$('#selectallThirdProject').prop('checked', false);
        this.thirdProjectIds = [];
      }
    },
    setChosenProject: function (e) {
      const scenario = e.currentTarget.lang;
      const scenarioId = e.currentTarget.value;
      this.finalProjectFirstScenario = {};
      let isChosen = false;
      switch (scenario) {
        case 'scenario1':
          this.firstScenarioId = scenarioId;
          isChosen = this.$('[data-proyek=' + e.currentTarget.id + ']').prop('checked');
          if (isChosen) {
            this.firstProjectIds.push(parseInt(e.currentTarget.id));
          } else {
            if (this.firstProjectIds.length > 0) {
              const index = this.firstProjectIds.indexOf(parseInt(e.currentTarget.id));
              if (index > -1) {
                this.firstProjectIds.splice(index, 1);
              }
            };
          }
          break;
        case 'scenario2':
          this.secondScenarioId = scenarioId;
          isChosen = this.$('[data-proyek2=' + e.currentTarget.id + ']').prop('checked');
          if (isChosen) {
            this.secondProjectIds.push(parseInt(e.currentTarget.id));
          } else {
            if (this.secondProjectIds.length > 0) {
              const index = this.secondProjectIds.indexOf(parseInt(e.currentTarget.id));
              if (index > -1) {
                this.secondProjectIds.splice(index, 1);
              }
            };
          }
          break;
        case 'scenario3':
          this.thirdScenarioId = scenarioId;
          isChosen = this.$('[data-proyek3=' + e.currentTarget.id + ']').prop('checked');
          if (isChosen) {
            this.thirdProjectIds.push(parseInt(e.currentTarget.id));
          } else {
            if (this.thirdProjectIds.length > 0) {
              const index = this.thirdProjectIds.indexOf(parseInt(e.currentTarget.id));
              if (index > -1) {
                this.thirdProjectIds.splice(index, 1);
              }
            };
          }
          break;
      };
    },
    selectAllSecondTahapan: function (e) {
      const scenario = e.currentTarget.lang;
      const scenarioId = e.currentTarget.value;
      if (this.$('#secondTahapanSelectall').prop('checked')) {
        this.$('input[type="checkbox"][data-item-tahapan2]').prop('checked', true);
        this.isSecondTahapanSelectedAll = true;
        this.secondTahapanIds = [];
        if (this.objTahapan.length > 0) {
          for (let i = 0; i < this.objTahapan.length; i++) {
            this.secondTahapanIds.push(parseInt(this.objTahapan[i].Id));
          }
        }
      } else {
        this.$('#secondTahapanSelectall').prop('checked', false);
        this.$('input[type="checkbox"][data-item-tahapan2]').prop('checked', false);
        this.isSecondTahapanSelectedAll = false;
        this.secondTahapanIds = [];
      }
      this.resetObjectParam(scenario);
      commonFunction.showLoadingSpinner();
      this.getProject(scenario, scenarioId);
    },
    selectAllThirdTahapan: function (e) {
      const scenario = e.currentTarget.lang;
      const scenarioId = e.currentTarget.value;
      if (this.$('#thirdTahapanSelectall').prop('checked')) {
        this.$('input[type="checkbox"][data-item-tahapan3]').prop('checked', true);
        this.thirdTahapanIds = [];
        if (this.objTahapan.length > 0) {
          for (let i = 0; i < this.objTahapan.length; i++) {
            this.thirdTahapanIds.push(parseInt(this.objTahapan[i].Id));
          }
        }
      } else {
        this.$('#thirdTahapanSelectall').prop('checked', false);
        this.$('input[type="checkbox"][data-item-tahapan3]').prop('checked', false);
        this.thirdTahapanIds = [];
      }
      this.resetObjectParam(scenario);
      commonFunction.showLoadingSpinner();
      this.getProject(scenario, scenarioId);
    },
    doValidation: function (e) {
      const self = this;
      let isValid = true;
      if (Object.keys(this.finalProjectFirstScenario).length != 0) {
        if (this.finalProjectFirstScenario.ScenarioId === 0) {
          commonFunction.responseWarningCannotExecute("Skenario 1 harus dipilih.");
          isValid = false;
        } else {
          if (this.tahapanIds.length === 0) {
            commonFunction.responseWarningCannotExecute("Pada Skenario 1, Tahapan Penjaminan harus dipilih.");
            isValid = false;
          } else {
            if (this.finalProjectFirstScenario.ProjectId.length === 0) {
              commonFunction.responseWarningCannotExecute("Pada Skenario 1, Proyek belum ada yang dipilih. Silahkan pilih proyek terlebih dahulu.");
              isValid = false;
            }
          }
        }
      } else {
        commonFunction.responseWarningCannotExecute("Skenario 1 harus dipilih");
        isValid = false;
      }
      if (isValid) {
        if (Object.keys(this.finalProjectSecondScenario).length != 0) {
          if (this.finalProjectSecondScenario.ScenarioId === 0) {
            commonFunction.responseWarningCannotExecute("Skenario 2 harus dipilih.");
            isValid = false;
          } else {
            if (this.secondTahapanIds.length === 0) {
              commonFunction.responseWarningCannotExecute("Pada Skenario 2, Tahapan Penjaminan harus dipilih.");
              isValid = false;
            } else {
              if (this.finalProjectSecondScenario.ProjectId.length === 0) {
                commonFunction.responseWarningCannotExecute("Pada Skenario 2, Proyek belum ada yang dipilih. Silahkan pilih proyek terlebih dahulu.");
                isValid = false;
              }
            }
          }
        } else {
          commonFunction.responseWarningCannotExecute("Skenario 2 harus dipilih");
          isValid = false;
        }
      }

      if (isValid) {
        if (Object.keys(this.finalProjectThirdScenario).length != 0) {
          if (this.finalProjectThirdScenario.ScenarioId === 0) {
            commonFunction.responseWarningCannotExecute("Skenario 3 harus dipilih.");
            isValid = false;
          } else {
            if (this.thirdTahapanIds.length === 0) {
              commonFunction.responseWarningCannotExecute("Pada Skenario 3, Tahapan Penjaminan harus dipilih.");
              isValid = false;
            } else {
              if (this.finalProjectThirdScenario.ProjectId.length === 0) {
                commonFunction.responseWarningCannotExecute("Pada Skenario 3, Proyek belum ada yang dipilih. Silahkan pilih proyek terlebih dahulu.");
                isValid = false;
              }
            }
          }
        } else {
          commonFunction.responseWarningCannotExecute("Skenario 3 harus dipilih");
          isValid = false;
        }
      }

      if (this.scenarioDefaultValidation() >= 2) {
        this.finalParam = [];
        isValid = false;
        commonFunction.responseWarningCannotExecute("Skenario default tidak bisa dipilih lebih dari 1 kali.");
      }

      if (isValid) {
        this.finalParam.push(this.finalProjectFirstScenario);
        this.finalParam.push(this.finalProjectSecondScenario);
        this.finalParam.push(this.finalProjectThirdScenario);
        eventAggregator.trigger('calculation/scenario:fecth', this.finalParam);
        console.log(this.finalParam);
        this.$el.modal('hide');
      }
    },
    // doTheLastValidation: function () {
    //   const self = this;
    //   let isValid = false;
    //   let isValued = 0;
    //   this.$('[validation]').each(function (i, item) {
    //     const attrName = item.name;
    //     const attrValue = self.$('[name="' + attrName + '"]').val();
    //     if (attrValue != "") {
    //       isValid = true;
    //       isValued += 1;
    //     } else {
    //       isValid = false;
    //     }
    //   });
    //   return isValued;
    // },
    // renderCalculation: function (obj) {
    //   const isValid = this.doTheLastValidation();
    //   if (isValid === 3) {
    //     var scenarios = [];
    //     for (var e = 1; e <= 3; e++) {
    //       var scenarioValue = this.$('#scenario' + [e] + '').val();
    //       scenarios.push(scenarioValue);
    //     }
    //     eventAggregator.trigger('calculation/scenario:fecth', scenarios);
    //     this.$el.modal('hide');
    //   } else {
    //     commonFunction.responseWarningCannotExecute("Skenario wajib dipilih.");
    //   }
    // },
    doCalculate: function () {
      let lastProjectValueFirstScenario = {};
      lastProjectValueFirstScenario.ScenarioId = parseInt(this.firstScenarioId);
      lastProjectValueFirstScenario.ProjectId = this.firstProjectIds;
      this.finalProjectFirstScenario = lastProjectValueFirstScenario;

      let lastProjectValueSecondScenario = {};
      lastProjectValueSecondScenario.ScenarioId = parseInt(this.secondScenarioId);
      lastProjectValueSecondScenario.ProjectId = this.secondProjectIds;
      this.finalProjectSecondScenario = lastProjectValueSecondScenario;

      let lastProjectValueThirdScenario = {};
      lastProjectValueThirdScenario.ScenarioId = parseInt(this.thirdScenarioId);
      lastProjectValueThirdScenario.ProjectId = this.thirdProjectIds;
      this.finalProjectThirdScenario = lastProjectValueThirdScenario;

      this.doValidation();
    },
    scenarioDefaultValidation: function () {
      let countScenario = 0;
      if (parseInt(this.firstScenarioId) === this.scenarioIdDefault) {
        countScenario += 1;
      };
      if (parseInt(this.secondScenarioId) === this.scenarioIdDefault) {
        countScenario += 1;
      };
      if (parseInt(this.thirdScenarioId) === this.scenarioIdDefault) {
        countScenario += 1;
      };
      return countScenario;
    },
    resetObjectParam: function (whichScenario) {
      switch (whichScenario) {
        case 'scenario1':
          if (Object.keys(this.finalProjectFirstScenario).length != 0) {
            this.finalProjectFirstScenario = {};
            this.firstProjectIds = [];
          }
          break;
        case 'scenario2':
          if (Object.keys(this.finalProjectSecondScenario).length != 0) {
            this.finalProjectSecondScenario = {};
            this.secondProjectIds = [];
          }
          break;
        case 'scenario3':
          if (Object.keys(this.finalProjectThirdScenario).length != 0) {
            this.finalProjectThirdScenario = {};
            this.thirdProjectIds = [];
          }
          break;
      }
    }
  });
});