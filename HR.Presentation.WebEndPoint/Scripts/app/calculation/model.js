define(function(require, exports, module) {
    'use strict';

    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'Id',
        urlRoot: 'api/Calculation',
        defaults: function() {
            return {
              // PageSize  : '',
              // ScenarioId : '',
              // NamaScenario : '',
              // ProjectId : '',
              // NamaProject : '',
              // SektorId : '',
              // NamaSektor : '',
              // UndiversifiedYearCollection : {
              //   Year: ''
              // },
              // DiversifiedRiskCapitalCollection : '',
              // CorrelatedSektorDetail : '',
              // RiskRegistrasi : ''
            }
        }
    });
});
