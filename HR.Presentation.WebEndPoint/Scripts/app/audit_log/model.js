define(function (require, exports, module) {
    'use strict';

    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'Id',
        urlRoot: 'api/AuditLog',
        defaults: () => {
            return {
                Id: '',
                MenuId: '',
                TableModified: '',
                DataObjekId: '',
                ColumnModified: '',
                DataAwal: '',
                DataAkhir: '',
                LogTimestamp: '',
                ModifiedBy: '',
                Aksi: '',
            }
        }
    });
});