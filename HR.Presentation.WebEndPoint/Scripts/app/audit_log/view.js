define(function (require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');    
    var commonConfig = require('commonconfig');
    var eventAggregator = require('eventaggregator');
    var Table = require('./table/table');
    var Collection = require('./collection');
    var Paging = require('paging');
    require('datetimepicker');    
    var moment = require('moment');

    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content tbl bg-white',
        template: _.template(template),
        initialize: function () {
            var self = this;
            this.keyword = "";
            this.table = new Table({
                collection: new Collection()
            });

            this.paging = new Paging({
                collection: this.table.collection
            });
            this.roles = JSON.parse(localStorage.getItem('roles'));
        },
        events: {
            'keyup #keyword': 'applyFilter',
            'click [name="btn-filter"]': 'applyFilter'
        },
        afterRender: function () {           
            this.$('[obo-table-sublikelihood]').append(this.table.el);
            this.table.render();

            this.insertView('[obo-paging]', this.paging);
            this.paging.render();

            this.fetchData();
            this.setRoleAccess();

            this.$('[name="TanggalAudit"], [name="TanggalAudit2"], .glyphicon glyphicon-calendar').datetimepicker({
                format: commonConfig.datePickerFormat,
            });
        },
        fetchData: function () {
            var param = [];
            param.Search = this.keyword;
            this.table.collection.fetch({
                reset: true,
                data: param,
                success: function (req, res) {
                    console.log(res);
                }
            })
        },
        add: function () {
            var self = this;
            require(['./add/view'], function (View) {
                commonFunction.setDefaultModalDialogFunction(self, View);
            });
        },
        doValidateDate: function() {
            var self = this;
            var status = true;
            var startDate = moment(this.$('[name="TanggalAudit"]').val()).format('YYYYMMDD');
            var endDate = moment(this.$('[name="TanggalAudit2"]').val()).format('YYYYMMDD');
            if(startDate == "Invalid date"){
                return status;
            }
            if(endDate != "" && startDate != "") {
              if(startDate > endDate) {
                commonFunction.responseWarningCannotExecute("Tanggal awal Audit tidak boleh lebih besar dari tanggal akhir Audit.");
                return false;
              } else if (endDate < startDate) {
                commonFunction.responseWarningCannotExecute("Tanggal akhir Audit tidak boleh lebih kecil dari tanggal awal Audit.");
                return false;
              } else {
                status = true;
              }
            }
            return status;
          },
        applyFilter: function () {            
            var statusDate = this.doValidateDate();
            if(statusDate){
                var self = this;
                var param = []
                var keyword = this.$('#keyword').val();
                this.keyword = keyword;
                param.Search = keyword;
                var keyIndex = this.$('#keyIndex option:selected').val();
                param.SearchBy = keyIndex;
                var sortBy = moment(this.$('[name="TanggalAudit"]').val()).format('YYYYMMDD');
                param.SortBy = sortBy;
                var sortDirection = moment(this.$('[name="TanggalAudit2"]').val()).format('YYYYMMDD');
                param.SortDirection = sortDirection;

                this.table.collection.fetch({
                    reset: true,
                    data: param
                })
            }            
        },
        setRoleAccess: function () {
            let self = this;
            if (this.roles) {
                $.grep(this.roles, function (item) {
                    if (item.Name.trim() == "Scenario") {
                        if (item.SubMenu) {
                            _.each(item.SubMenu, function (val) {
                                switch (val.Action) {
                                    case "add":
                                        self.$('[name="add"]').removeClass('hide');
                                        break;
                                }
                            });
                        }
                    }
                });
            }
        }
    });
});