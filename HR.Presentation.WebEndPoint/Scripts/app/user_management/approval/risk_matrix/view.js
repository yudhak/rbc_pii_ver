define(function(require, exports, module) {
  'use strict';
  var LayoutManager = require('layoutmanager');
  var template = require('text!./template.html');
  var commonFunction = require('commonfunction');
  var eventAggregator = require('eventaggregator');
  var Table = require('./table/table');
  // var Collection = require('./collection');
  var Paging = require('paging');
  var Model = require('./model');
  var ModelParent = require('./../model');

  module.exports = LayoutManager.extend({
      className: 'container-fluid main-content tbl bg-white',
      template: _.template(template),
      initialize: function(options) {
          var self = this;
          this.menuId = commonFunction.getLastSplitHash();
          this.keyword = "";
          this.model = new Model();
          this.listenTo(this.model, 'sync', () => {
              this.render();
          })
          this.once('afterRender', () => {
              this.model.set(this.model.idAttribute, commonFunction.getLastSplitHash());
              // this.model.fetch();

              // this.modelParent.set(this.modelParent.idAttribute, commonFunction.getLastSplitHash());
              // this.modelParent.fetch();
          });

          // this.parentId = commonFunction.getUrlHashSplit(3);

          this.table = new Table();

          this.paging = new Paging({
              collection: this.table.collection
          });
          this.listenTo(eventAggregator, 'master/risk/detail/add:fecth', function() {
            self.fetchData();
          });
          this.listenTo(eventAggregator, 'master/risk/detail/edit:fecth', function() {
           self.fetchData();
          });
          this.listenTo(eventAggregator, 'master/risk/detail/delete:fecth', function(model) {
            self.fetchData();
          });
      },
      events: {
          'click [name="add"]': 'add',
          'keyup #keyword': 'applyFilter',
          'click [name="btn-filter"]': 'applyFilter'
      },
      afterRender: function() {
        var param  = {};
        param.Search = "";
        param.IsDefaultOnly = true;
        console.log(param);
        this.$('[kw-table-risk-matrix]').append(this.table.el);
        this.table.render();

        this.table.collection.fetch({
            reset: true,
            data: {
                Search: this.keyword,
                IsDefaultOnly: true
            },
            success: function(req, res) {
            }
        });
        this.insertView('[kw-paging]', this.paging);
        this.paging.render();
        // this.setTemplate(this.modelParent);
      },
      fetchData: function() {
        var param  = {};
        param.Search = this.keyword;
        param.IsDefaultOnly = true;
        this.table.collection.fetch({
            reset: true,
            data: {
                Search: this.keyword,
                IsDefaultOnly: true
            },
            success: function(req, res) {
            }
        })
      },
      add: function() {
          var self = this;
          var modelParent = this.modelParent;
          require(['./add/view'], function(View) {
              commonFunction.setDefaultModalDialogFunction(self, View, modelParent);
          });
      },
      applyFilter: function () {
            var self = this;
            var param = {};
            var keyword = this.$('#keyword').val();
            var keyIndex = this.$('#keyIndex option:selected').val();
            param.Search = keyword;
            param.SearchBy = keyIndex;
            param.IsDefaultOnly = true;
            param.ParentId = this.parentId;
            this.table.collection.fetch({
                reset: true,
                data: param
            });
      }
  });
});