define(function(require, exports, module) {
    'use strict';

    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'Id',
        urlRoot: 'api/RiskMatrixProject',
        defaults: function() {
            return {
                Id : '',
                ProjectId : '',
                RiskMatrixId : '',
                ScenarioId : '',
                Project : {
                    Id : '',
                    NamaProject: '',
                    UserId: '',
                    NamaSektor: ''
                },
                CreateBy : '',
                CreateDate : '',
                UpdateBy : '',
                UpdateDate : '',
                IsDelete : '',
                DeleteDate : ''
            }
        }
    });
});
