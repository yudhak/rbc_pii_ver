define(function(require, exports, module) {
  'use strict';
  var LayoutManager = require('layoutmanager');
  var template = require('text!./template.html');
  var commonFunction = require('commonfunction');
  var eventAggregator = require('eventaggregator');
  var Table = require('./table/table');
  var Paging = require('paging');
  var Model = require('./model');
  var ModelParent = require('./../model');

  module.exports = LayoutManager.extend({
      className: 'container-fluid main-content tbl bg-white',
      template: _.template(template),
      initialize: function(options) {
          var self = this;
          this.menuId = commonFunction.getLastSplitHash();
          this.keyword = "";
          this.model = new Model();
          this.listenTo(this.model, 'sync', () => {
              this.render();
          })
          this.once('afterRender', () => {
              this.model.set(this.model.idAttribute, commonFunction.getLastSplitHash());
              // this.model.fetch();

              // this.modelParent.set(this.modelParent.idAttribute, commonFunction.getLastSplitHash());
              // this.modelParent.fetch();
          });

          // this.parentId = commonFunction.getUrlHashSplit(3);

          this.table = new Table();

          this.paging = new Paging({
              collection: this.table.collection
          });
      },
      events: {
          'keyup #keyword': 'applyFilter',
          'click [name="btn-filter"]': 'applyFilter'
      },
      afterRender: function() {
        var param  = [];
        param.Search = this.keyword;
        this.$('[kw-table-risk-matrix]').append(this.table.el);
        this.table.render();

        this.table.collection.fetch({
            reset: true,
            data: {
              data: param
            }
        });

        this.insertView('[kw-paging]', this.paging);
        this.paging.render();
        // this.setTemplate(this.modelParent);
      },
      fetchData: function() {
        var param  = [];
        param.Search = this.keyword;
        this.table.collection.fetch({
            reset: true,
            data: {
              data: param
            }
        })
      },
      applyFilter: function () {
          var self = this;
          var param = []
          var keyword = this.$('#keyword').val();
          var keyIndex = this.$('#keyIndex option:selected').val();
          param.Search = keyword;
          param.SearchBy = keyIndex;
          param.ParentId = this.parentId;
          this.table.collection.fetch({
              reset: true,
              data: param
          })
      }
  });
});