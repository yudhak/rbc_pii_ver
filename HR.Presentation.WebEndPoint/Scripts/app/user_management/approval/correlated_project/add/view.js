define(function(require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var commonFunction = require('commonfunction');
  var Model = require('./../model');
  var ModelUser = require('./../../modelUser');
  var ModelMasterApprovalCorrelatedProject = require('./../modelApprovalCorrelatedProject');
  var eventAggregator = require('eventaggregator');
  require('bootstrap-validator');
  require('jquerymask');

  module.exports = View.extend({
      template: _.template(template),
      initialize: function(options) {
        var self = this;
        this.entity = options.model.attributes;
        this.namaProject = options.model.attributes.NamaProject;
        this.menuId = commonFunction.getLastSplitHash();
        this.projectId = options.model.attributes.Id;
        this.userList = null;
        this.model = new Model();
        this.modelUser = new ModelUser();
        this.modelMasterApprovalCorrelatedProject = new ModelMasterApprovalCorrelatedProject();
        this.listenTo(this.model, 'request', function() {});

        this.listenTo(this.model, 'sync error', function() {});

        this.listenTo(this.model, 'sync', function(model) {
          commonFunction.responseSuccessUpdateAddDelete('Master persetujuan Skenario berhasil dibuat.');
          self.$el.modal('hide');
          eventAggregator.trigger('master/approval/scenario/add:fecth');
        });
      },
      events: {
        'change [name="Level1"], [name="Level2"], [name="Level3"]': 'levelValidationPerItem'
      },
      afterRender: function() {
        this.fetchUser();
        this.renderValidation();
      },
      fetchUser: function() {
        var self = this;
        this.modelUser.fetch({
            reset: true,
            success: function(req, res) {
              self.userList = res;
              if (res) {
                var htmlLevel1 = '<select data-approval class="form-control select" name="Level1">';
                    htmlLevel1 += '<option value="0">Pilih User</option>';
                    _.each(res, function(item) {
                      htmlLevel1 += '<option value="'+ item.Id +'">'+ item.UserName +'</option>';
                    });
                    htmlLevel1 += '</select>';

                var htmlLevel2 = '<select data-approval class="form-control select" name="Level2">';
                    htmlLevel2 += '<option value="0">Pilih User</option>';
                    _.each(res, function(item) {
                      htmlLevel2 += '<option value="'+ item.Id +'">'+ item.UserName +'</option>';
                    });
                    htmlLevel2 += '</select>';

                var htmlLevel3 = '<select data-approval class="form-control select" name="Level3">';
                    htmlLevel3 += '<option value="0">Pilih User</option>';
                    _.each(res, function(item) {
                      htmlLevel3 += '<option value="'+ item.Id +'">'+ item.UserName +'</option>';
                    });
                    htmlLevel3 += '</select>';

                self.$('[level-1-list]').append(htmlLevel1);
                self.$('[level-2-list]').append(htmlLevel2);
                self.$('[level-3-list]').append(htmlLevel3);

                self.fetchCurrentValue();
                self.setTemplate();
              }
            }
        })
      },
      fetchCurrentValue: function() {
        var self = this;
        this.modelMasterApprovalCorrelatedProject.fetch({
          reset: true,
          data: {
            id: this.projectId
          },
          success: function(req, res) {
            self.setTemplate();
          }
        })
      },
      setTemplate: function() {
        this.$('[name="Name"]').val("Korelasi - Proyek");
        this.$('[name="NamaProject"]').val(this.namaProject);

        var currentData = this.modelMasterApprovalCorrelatedProject;
          if (!currentData.isEmpty({})) {
            this.modelMasterApprovalCorrelatedProject.set(this.model.idAttribute, this.projectId);
            _.each(currentData.attributes, function(item) {
              var seqeunceVal = item.NomorUrutStatus;
              self.$('[name="Level'+ seqeunceVal +'"]').val(item.UserId);
            });
          }
      },
      levelValidationPerItem: function(e) {
        var self = this;
        var currentAttr = e.currentTarget.name;
        var dom = this.$('[name="'+ currentAttr +'"]').val();
        var domAmmount = 0;
        this.$('[data-approval]').each(function(i, item) {
          var val = self.$('[name="'+ item.name +'"]').val();
          if (dom == val) {
            domAmmount += 1;
          }
          if (domAmmount > 1) {
            commonFunction.responseWarningCannotExecute(currentAttr + " tidak boleh sama dengan level lainnya.");
            self.$('[type="submit"]').attr('disabled', true);
          } else {
            self.$('[type="submit"]').attr('disabled', false);
          }
        });
      },
      renderValidation: function() {
        var self = this;
        this.$('[ehs-form]').bind("keypress", function (e) {
              if (e.keyCode == 13) {
                  return false;
              }
          });
        this.$('[ehs-form]').bootstrapValidator({
            fields: {
              Level1: {
                validators:{
                  notEmpty:{
                    message: 'Level 1 wajib diisi'
                  }
                }
              },
              Level1: {
                validators: {
                  notEmpty: {
                    message: 'Level 2 wajib diisi'
                  }
                }
              },
              Level3: {
                validators:{
                  notEmpty:{
                    message: 'Level 3 wajib diisi'
                  }
                }
              }
            }
          })
          .on('success.form.bv', function(e) {
            e.preventDefault();
            self.getConfirmation();
          });
      },
      getConfirmation: function(){
        var data = this.$('[name="KodeMRisk"]').val();
          var action = "tambah";
          var retVal = confirm("Apakah anda yakin ingin " + action + " data ini ?");
          if( retVal == true ){
            this.doSave();
          }
          else{
            this.$('[type="submit"]').attr('disabled', false);
          }
      },
      doSave: function() {
        var self = this;
        var data = commonFunction.formDataToJson(this.$('form').serializeArray());
        var ids = [];
        this.$('[data-approval]').each(function(i, item) {
          var attrName = item.name;
          var attrValue = self.$('[name="'+ attrName +'"]').val();
          ids.push(attrValue);
        });

        data.ProjectId = this.projectId;
        data.MenuId = this.menuId;
        data.UserIdList = ids;
        this.modelMasterApprovalCorrelatedProject.save(data);

        commonFunction.responseSuccessUpdateAddDelete('Master persetujuan Korelasi - Proyek berhasil dibuat.');
        self.$el.modal('hide');
      }
  });
});
