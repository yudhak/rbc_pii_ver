define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./row.html');
    var commonFunction = require('commonfunction');

    module.exports = LayoutManager.extend({
        tagName: 'tr',
        template: _.template(template),
        events: {
            'click [name="Setting"]': 'Detail',
        },
        Detail: function(e) {
            var self = this;
            var entity = e.currentTarget.id;
            switch(entity) {
                case "Scenario":
                    require(['./../scenario/view'], function(View) {
                        commonFunction.setDefaultModalDialogFunction(self, View, self.model);
                    });
                    break;
                case "Risiko Matriks":
                    require(['./../risk_matrix/view'], function(View) {
                        commonFunction.setDefaultModalDialogFunction(self, View, self.model);
                    });
                    break;
                case "Correlated Sektor":
                    require(['./../correlated_sector/view'], function(View) {
                        commonFunction.setDefaultModalDialogFunction(self, View, self.model);
                    });
                    break;
                case "Correlated Project":
                    require(['./../correlated_project/view'], function(View) {
                        commonFunction.setDefaultModalDialogFunction(self, View, self.model);
                    });
                    break;
            }
        }
    });
});
