define(function(require, exports, module) {
    'use strict';

    var Subroute = require('backbone.subroute');
    var commonFunction = require('commonfunction');

    var fnSetContentView = function(pathViewFile) {
        var hashtag = '#mainapproval';
        require([pathViewFile + '/view'], function(View) {
            if (View)
                commonFunction.setContentViewWithNewModuleView(new View(), hashtag);
        });
    };

    module.exports = Subroute.extend({
        initialize: function() {
            this.app = {};
        },
        routes: {
            '':'showList',
            ':correlated_project/cp/:id': 'showCorrelatedProject',
            ':risk_matrix/:id': 'showRiskMatrix',
        },
        showList:function(){
            fnSetContentView('.');
        },
        showCorrelatedProject: function(){
            fnSetContentView('./correlated_project');
        },
        showRiskMatrix: function(){
            fnSetContentView('./risk_matrix');
        }
    });
});
