﻿define(function(require, exports, module) {
    'use strict';

    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'Id',
        urlRoot: 'api/User',
        defaults: function() {
            return {
                Id : '',
                UserName : '',
                Email: ''
            }
        }
    });
});
