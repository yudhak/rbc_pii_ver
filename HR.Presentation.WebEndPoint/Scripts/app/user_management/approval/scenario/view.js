define(function(require, exports, module) {
    'use strict';
    var View = require('modaldialogdefault');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var ModelMasterApprovalScenario = require('./model');
    var Model = require('./../model');
    var ModelUser = require('./../modelUser');
    var eventAggregator = require('eventaggregator');
    require('select2');
    require('bootstrap-validator');
    require('jquerymask');

    module.exports = View.extend({
        template: _.template(template),
        initialize: function(options) {
          this.entity = options.model.attributes;
          var self = this;
          this.userList = null;
          this.currentModel = null;
          this.model = new Model();
          this.modelUser = new ModelUser();
          this.modelMasterApprovalScenario = new ModelMasterApprovalScenario();
          // this.modelMasterApprovalScenario.set(this.model.idAttribute, this.entity.MenuId);

          this.listenToOnce(this.modelMasterApprovalScenario, 'sync', function(model) {
            this.render();
          });
          this.listenTo(this.model, 'request', function() {});
          this.listenTo(this.model, 'sync', function(model) {
            this.listenToOnce(this.modelMasterApprovalScenario, 'sync', function(model) {
              commonFunction.responseSuccessUpdateAddDelete('Master persetujuan Skenario berhasil dibuat.');
              self.$el.modal('hide');
              eventAggregator.trigger('master/approval/scenario/add:fecth');
            });
          });
        },
        events: {
          'change [name="Level1"], [name="Level2"], [name="Level3"]': 'levelValidationPerItem'
        },
        afterRender: function() {
          this.fetchUser();
          this.renderValidation();
        },
        fetchUser: function() {
          var self = this;
          this.modelUser.fetch({
              reset: true,
              success: function(req, res) {
                self.userList = res;
                if (res) {
                  var htmlLevel1 = '<select data-approval class="form-control select" id="Level1" name="Level1">';
                      htmlLevel1 += '<option value="0">Pilih User</option>';
                      _.each(res, function(item) {
                        htmlLevel1 += '<option value="'+ item.Id +'">'+ item.UserName +'</option>';
                      });
                      htmlLevel1 += '</select>';

                  var htmlLevel2 = '<select data-approval class="form-control select" id="Level2" name="Level2">';
                      htmlLevel2 += '<option value="0">Pilih User</option>';
                      _.each(res, function(item) {
                        htmlLevel2 += '<option value="'+ item.Id +'">'+ item.UserName +'</option>';
                      });
                      htmlLevel2 += '</select>';

                  var htmlLevel3 = '<select data-approval class="form-control select" id="Level3" name="Level3">';
                      htmlLevel3 += '<option value="0">Pilih User</option>';
                      _.each(res, function(item) {
                        htmlLevel3 += '<option value="'+ item.Id +'">'+ item.UserName +'</option>';
                      });
                      htmlLevel3 += '</select>';
                  self.$('[level-1-list]').append(htmlLevel1);
                  self.$('[level-2-list]').append(htmlLevel2);
                  self.$('[level-3-list]').append(htmlLevel3);

                  self.fetchCurrentValue();
                  self.setTemplate();
                }
              }
          })
        },
        fetchCurrentValue: function() {
          this.modelMasterApprovalScenario.fetch({
            reset: true,
            data: {
              id: this.entity.MenuId
            }
          })
        },
        setTemplate: function() {
          var self = this;
          this.$('[name="Name"]').val(this.entity.Description);
          var currentData = this.modelMasterApprovalScenario;
          if (!currentData.isEmpty({})) {
            this.modelMasterApprovalScenario.set(this.model.idAttribute, this.entity.MenuId);
            _.each(currentData.attributes, function(item) {
              var seqeunceVal = item.NomorUrutStatus;
              self.$('#Level'+ seqeunceVal).val(item.UserId);
            });
          }
        },
        renderValidation: function () {
          var self = this;
          this.$('[ehs-form]').bind("keypress", function (e) {
            if (e.keyCode == 13) {
              return false;
            }
          });
          this.$('[ehs-form]').bootstrapValidator({
              fields: {
                NamaProject: {
                  validators: {
                    stringLength: {
                      message: 'Nama Project must be less than 50 characters',
                      max: 50
                    },
                    notEmpty: {
                      message: 'Nama Project is required'
                    }
                  }
                }
              }
            })
            .on('success.form.bv', function (e) {
              e.preventDefault();
              self.getConfirmation();
            });
        },
        levelValidationPerItem: function(e) {
          var self = this;
          var currentAttr = e.currentTarget.name;
          var dom = this.$('[name="'+ currentAttr +'"]').val();
          var domAmmount = 0;
          this.$('[data-approval]').each(function(i, item) {
            var val = self.$('[name="'+ item.name +'"]').val();
            if (dom == val) {
              domAmmount += 1;
            }
            if (domAmmount > 1) {
              commonFunction.responseWarningCannotExecute(currentAttr + " tidak boleh sama dengan level lainnya.");
              self.$('[type="submit"]').attr('disabled', true);
            } else {
              self.$('[type="submit"]').attr('disabled', false);
            }
          });
        },
        getConfirmation: function(){
          var data = this.$('[name="KodeMRisk"]').val();
          var action = "tambah";
          var retVal = confirm("Apakah anda yakin ingin " + action + " data ini ?");
          if( retVal == true ){
            this.doSave();
          }
          else{
            this.$('[type="submit"]').attr('disabled', false);
          }
        },
        doSave: function() {
          var self = this;
          var data = commonFunction.formDataToJson(this.$('form').serializeArray());
          var ids = [];
          this.$('[data-approval]').each(function(i, item) {
            var attrName = item.name;
            var attrValue = self.$('[name="'+ attrName +'"]').val();
            ids.push(attrValue);
          });

          data.MenuId = this.entity.MenuId;
          data.UserIdList = ids;
          this.modelMasterApprovalScenario.save(data);

          commonFunction.responseSuccessUpdateAddDelete('Master persetujuan Skenario berhasil dibuat.');
          self.$el.modal('hide');
        }
    });
});
