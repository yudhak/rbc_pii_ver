define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var commonFunction = require('commonfunction');
  var Model = require('./../model');
  var ModelMenu = require('./../modelMenu');
  var eventAggregator = require('eventaggregator');
  require('bootstrap-validator');
  require('jquerymask');
  require('select2');

  module.exports = View.extend({
    template: _.template(template),
    initialize: function () {
      var self = this;
      this.keyword = "";
      this.model = new Model();
      this.modelMenu = new ModelMenu();

      this.listenTo(this.model, 'request', function () {});
      this.listenTo(this.model, 'sync error', function () {});
      this.listenTo(this.model, 'sync', function (model) {
        commonFunction.responseSuccessUpdateAddDelete('Setting menu berhasil dibuat.');
        self.$el.modal('hide');
        eventAggregator.trigger('master/setting_menu/add:fecth');
      });
      this.fetchMenu();
    },
    afterRender: function () {
      this.renderValidation();
    },
    fetchMenu: function() {
      var self = this;
      var param = {};
      param.Search = this.keyword;
      param.PageSize = 300;
      this.modelMenu.fetch({
        reset: true,
        data: {
          data: param
        },
        success: function(req, res) {
          self.setTemplate(res);
        }
      });
    },
    events: {
      
    },
    setTemplate: function (data) {
      var self = this;
      if (data) {
        var html = '<select class="form-control select" name="NamaMenu">'
          html += '<option value="0">Pilih Menu</option>'
        _.each(data, function(item) {
          html += '<option value="'+ item.Id +'">'+ item.Name +'</option>'
        });
        html += '</select>'
        this.$('[data-menu]').append(html);
      }
      this.$('[name="Lihat"]').prop('checked', true);
    },
    renderValidation: function () {
      var self = this;
      this.$('[ehs-form]').bind("keypress", function (e) {
        if (e.keyCode == 13) {
          return false;
        }
      });
      this.$('[ehs-form]').bootstrapValidator({
          fields: {
            NamaScenario: {
              validators: {
                stringLength: {
                  message: 'Nama scenario must be less than 100 characters',
                  max: 100
                },
                notEmpty: {
                  message: 'Nama scenario is required'
                }
              }
            },
          }
        })
        .on('success.form.bv', function (e) {
          e.preventDefault();
          self.getConfirmation();
        });
    },
    getConfirmation: function () {
      var self = this;
      var isValid = false;
      var menu = this.$('[name="NamaMenu"]').val();
      if (menu != 0) {
        isValid = true;
      }
      if (isValid) {
        var action = "add";
        var retVal = confirm("Are you sure want to " + action + " Setting Menu ?");
        if (retVal == true) {
          this.doSave();
        } else {
          this.$('[type="submit"]').attr('disabled', false);
        }
      } else {
        commonFunction.responseWarningCannotExecute("Menu wajib diisi.");
        this.$('[type="submit"]').attr('disabled', false);
      }
    },
    doSave: function () {
      var data = commonFunction.formDataToJson(this.$('form').serializeArray());
      data.MenuId = this.$('[name="NamaMenu"]').val();
      data.RoleId = this.$('[name="NamaRole"]').val();
      data.Tambah = this.$('[name="Tambah"]').prop('checked');
      data.Ubah = this.$('[name="Ubah"]').prop('checked');
      data.Hapus = this.$('[name="Hapus"]').prop('checked');
      data.Lihat = true;
      data.Detail = this.$('[name="Detail"]').prop('checked');
      data.IsActive = this.$('[name="IsActive"]').val();
      this.model.save(data);
    }
  });
});