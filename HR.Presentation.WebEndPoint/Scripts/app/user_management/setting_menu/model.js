define(function(require, exports, module) {
    'use strict';

    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'Id',
        urlRoot: 'api/RoleAccessFront',
        defaults: function() {
            return {
              Role : '',
              Menu : {
                Name: '',
                ControllerName: '',
                ParentId: '',
                ActionName: '',
              },
              Tambah : '',
              Ubah : '',
              Hapus : '',
              Lihat : '',
              Detail : '',
              IsActive : ''
            }
        }
    });
});
