define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');
    var Table = require('./table/table');
    var Collection = require('./collection');
    var Paging = require('paging');

    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content tbl bg-white',
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.keyword = "";
            this.table = new Table({
                collection: new Collection()
            });
            this.paging = new Paging({
                collection: this.table.collection
            });
            this.listenTo(eventAggregator, 'master/setting_menu/add:fecth', function() {
              self.fetchData();
            });
            this.listenTo(eventAggregator, 'master/setting_menu/delete:fecth', function() {
             self.fetchData();
            });
            this.listenTo(eventAggregator, 'master/setting_menu/edit:fecth', function() {
              self.fetchData();
            });           
        },
        events : {
            'click [name="add"]':'add',
            'keyup #keyword': 'applyFilter',
            'click [name="btn-filter"]': 'applyFilter'
        },
        afterRender: function() {
            this.$('[obo-table-menu]').append(this.table.el);
            this.table.render();

            this.insertView('[obo-paging]', this.paging);
            this.paging.render();

            this.fetchData();
        },
        fetchData: function() {
            var self = this;
            var param  = [];
            param.Search = this.keyword;
            this.table.collection.fetch({
                reset:true,
                data: param,
                success: function(req, res) {
                    self.listMainMenu = [];
                    if (res.results) {
                        _.each(res.results, function(item) {
                            self.listMainMenu.push(item);
                        });
                    }
                }
            });
        },
        add : function(){
          var self = this;
          require(['./add/view'], function(View) {
            commonFunction.setDefaultModalDialogFunction(self, View);
          });
        },
        applyFilter: function() {
            var self = this;
            var param  = []
            var keyword = this.$('#keyword').val();
            param.Search = keyword;
            var keyIndex = this.$('#keyIndex option:selected').val();
            param.SearchBy = keyIndex;
            
            this.table.collection.fetch({
                reset: true,
                data: param
            })
        }
    });
});
