define(function(require, exports, module) {
    'use strict';
    var View = require('modaldialogdefault');
    var template = require('text!./template.html');
    var Model = require('./../model');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var eventAggregator = require('eventaggregator');
    require('bootstrap-validator');

    module.exports = View.extend({
        template: _.template(template),
        initialize : function(){
          var self = this;
          var userId = this.model.get('Id');
          this.model = new Model();
          this.model.set(this.model.idAttribute, userId);
          this.listenToOnce(this.model, 'sync', function(model) {
              this.render();
              var data = model.toJSON();
              this.listenTo(this.model, 'sync', function() {
                commonFunction.responseSuccessUpdateAddDelete('Pengguna berhasil diubah.');
                self.$el.modal('hide');
                eventAggregator.trigger('master/user_role/edit:fecth');
              });
          }, this);

          this.once('afterRender', function() {
              this.model.fetch();
          });
        },
        afterRender : function(){
          this.renderValidation();
          this.setValueToTemplate();
        },
        
    setValueToTemplate: function() {
      this.$('[name="RoleId"]').val(this.model.attributes.RoleId);
      this.$('[name="Status"]').val(this.model.attributes.Status.toString());
    },
        renderValidation: function() {
          var self = this;
          this.$('[ehs-form]').bootstrapValidator({
              fields: {
                
              }
            })
            .on('success.form.bv', function(e) {
              e.preventDefault();
              self.getConfirmation();
            });
        },
        getConfirmation: function(){
            var data = this.$('[name="UserName"]').val();
            var action = "Ubah";
            var retVal = confirm("Apakah anda yakin ingin " + action + " Pengguna : "+ data +" ?");
            if( retVal == true ){
               this.doSave();
            }
            else{
              this.$('[type="submit"]').attr('disabled', false);
            }
        },
        doSave: function() {
          var data = commonFunction.formDataToJson(this.$('form').serializeArray());
          this.model.save(data);
        }
    });
});