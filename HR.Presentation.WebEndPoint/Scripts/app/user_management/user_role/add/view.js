define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var commonFunction = require('commonfunction');
  var Model = require('./../model');
  var eventAggregator = require('eventaggregator');
  require('bootstrap-validator');
  require('jquerymask');
  require('select2');

  module.exports = View.extend({
    template: _.template(template),
    initialize: function () {
      var self = this;
      this.keyword = "";
      this.model = new Model();

      this.listenTo(this.model, 'request', function () {});
      this.listenTo(this.model, 'sync error', function () {});
      this.listenTo(this.model, 'sync', function (model) {
        commonFunction.responseSuccessUpdateAddDelete('Pengguna berhasil dibuat.');
        self.$el.modal('hide');
        eventAggregator.trigger('master/user_role/add:fecth');
      });
      // this.fetchMenu();
    },
    afterRender: function () {
      this.renderValidation();
    },
    renderValidation: function () {
      var self = this;
      this.$('[ehs-form]').bind("keypress", function (e) {
        if (e.keyCode == 13) {
          return false;
        }
      });
      this.$('[ehs-form]').bootstrapValidator({
          fields: {

          }
        })
        .on('success.form.bv', function (e) {
          e.preventDefault();
          self.getConfirmation();
        });
    },
    getConfirmation: function () {
      var self = this;
      var isValid = false;
      var menu = this.$('[name="NamaMenu"]').val();
      if (menu != 0) {
        isValid = true;
      }
      if (isValid) {
        var action = "Tambah";
        var retVal = confirm("Apakah anda yakin ingin " + action + " Pengguna ?");
        if (retVal == true) {
          this.doSave();
        } else {
          this.$('[type="submit"]').attr('disabled', false);
        }
      } else {
        commonFunction.responseWarningCannotExecute("Kolom wajib diisi.");
        this.$('[type="submit"]').attr('disabled', false);
      }
    },
    doSave: function () {
      var data = commonFunction.formDataToJson(this.$('form').serializeArray());
      data.UserName = this.$('[name="UserName"]').val();
      data.Email = this.$('[name="Email"]').val();
      data.RoleId = this.$('[name="RoleId"]').val();
      data.Status = this.$('[name="Status"]').val();
      debugger;
      this.model.save(data);
    }
  });
});