//scripts/app
define(function (require, exports, module) {
    'use strict';

    require('backbone');
    var commonFunction = require('commonfunction');
    var fnSetContentView = function (pathViewFile, hashtag, options) {
        require(['./' + pathViewFile + '/view'], function (View) {
            if (View)
                commonFunction.setContentViewWithNewModuleView(new View(), hashtag, options);
        });
    };

    module.exports = Backbone.Router.extend({
        initialize: function () {
            this.app = {};
        },
        routes: {
            // '': 'showMainMenu',
            'login': 'showLogin',
            'forgot_password': 'showForgotPassword',
            'reset_password': 'showResetPassword',
            'employee(/*subrouter)': 'redirectToEmployeeModule',
            'master(/*subrouter)': 'callMasterSubRouter',
            'scenario(/*subrouter)': 'callScenarioModule',
            'risk_matrix(/*subrouter)': 'callRiskMatrixModule',
            'correlation(/*subrouter)': 'callCorrelationSubRouter',
            'overall_comment(/*subrouter)': 'callOverallCommentModule',
            'dashboard(/*subrouter)': 'callDashboardSubRouter',
            'calculation(/*subrouter)': 'callCalculationModule',
            'approval(/*subrouter)': 'callApprovalCommentModule',
            'calculation_compare(/*subrouter)': 'callCalculationCompareModule',
            'audit_log(/*subrouter)': 'callAuditLogModule',
            'user_management(/*subrouter)': 'callUserManagementSubRouter',
            'help(/*subrouter)': 'callHelpModule',
            '*actions': 'notFound'
        },
        start: function () {
            Backbone.history.start();
        },
        showMainMenu: function () {
            fnSetContentView('./dashboard/main_dashboard');
        },
        showLogin: function () {
            fnSetContentView('./login');
        },
        showForgotPassword: function () {
            fnSetContentView('./forgot_password');
        },
        showResetPassword: function () {
            fnSetContentView('./reset_password');
        },
        redirectToEmployeeModule: function () {
            var self = this;
            if (!this.app.employeeRouter) {
                require(['./employee/router'], function (Router) {
                    self.app.employeeRouter = new Router('employee', {
                        createTrailingSlashRoutes: true
                    });
                });
            }
        },
        callMasterSubRouter: function () {
            if (!this.app.masterSubRouter) {
                require(['./master/router'], Router => {
                    this.app.masterRouter = new Router('master');
                });
            }
        },
        callScenarioModule: function () {
            if (!this.app.scenarioRouter) {
                require(['./scenario/router'], Router => {
                    this.app.scenarioRouter = new Router('scenario');
                });
            }
        },
        callRiskMatrixModule: function () {
            if (!this.app.risk_matrixRouter) {
                require(['./risk_matrix/router'], Router => {
                    this.app.risk_matrixRouter = new Router('risk_matrix');
                });
            }
        },
        callOverallCommentModule: function () {
            if (!this.app.overall_commentRouter) {
                require(['./overall_comment/router'], Router => {
                    this.app.overall_commentRouter = new Router('overall_comment');
                });
            }
        },
        callCorrelationSubRouter: function () {
            if (!this.app.correlaltionSubRouter) {
                require(['./correlation/router'], Router => {
                    this.app.masterRouter = new Router('correlation');
                });
            }
        },
        callDashboardSubRouter: function () {
            if (!this.app.dashboardSubRouter) {
                require(['./dashboard/router'], Router => {
                    this.app.masterRouter = new Router('dashboard');
                });
            }
        },
        callCalculationModule: function () {
            if (!this.app.calculationSubRouter) {
                require(['./calculation/router'], Router => {
                    this.app.masterRouter = new Router('calculation');
                });
            }
        },
        callApprovalCommentModule: function () {
            if (!this.app.dashboardSubRouter) {
                require(['./approval/router'], Router => {
                    this.app.masterRouter = new Router('approval');
                });
            }
        },
        callCalculationCompareModule: function () {
            if (!this.app.calculationCompareSubRouter) {
                require(['./calculation_compare/router'], Router => {
                    this.app.masterRouter = new Router('calculation_compare');
                });
            }
        },
        callAuditLogModule: function () {
            if (!this.app.auditLogSubRouter) {
                require(['./audit_log/router'], Router => {
                    this.app.masterRouter = new Router('audit_log');
                });
            }
        },
        callUserManagementSubRouter: function () {
            if (!this.app.userManagementSubRouter) {
                require(['./user_management/router'], Router => {
                    this.app.userManagementRouter = new Router('user_management');
                });
            }
        },
        callHelpModule: function () {
            if (!this.app.help) {
                require(['./help/router'], Router => {
                    this.app.help = new Router('help');
                });
            }
        },
        notFound: function () {
            require(['./errorpages/notfound/view'], function (View) {
                commonFunction.setContentViewWithNewModuleView(new View(), '#');
            });
        },
    });
});