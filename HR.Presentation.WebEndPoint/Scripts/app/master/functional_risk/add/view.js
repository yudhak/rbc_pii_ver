define(function(require, exports, module) {
    'use strict';
    var View = require('modaldialogdefault');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var Model = require('./../model');
    var ModelScenario = require('./../../../calculation_compare/model');
    var eventAggregator = require('eventaggregator');
    require('bootstrap-validator');
    require('jquerymask');

    module.exports = View.extend({
        template: _.template(template),
        initialize: function() {
          var self = this;
          this.model = new Model();
          this.modelScenario = new ModelScenario();
          this.listenTo(this.model, 'request', function() {});
          this.listenTo(this.model, 'sync error', function() {});
          this.listenTo(this.model, 'sync', function(model) {
            commonFunction.responseSuccessUpdateAddDelete('Ambang Batas Risiko berhasil dibuat.');
            self.$el.modal('hide');
            eventAggregator.trigger('master/functional_risk/add:fecth');
          });
          this.fecthScenario();
        },
        afterRender: function() {
          this.renderValidation();
        },
        fecthScenario: function() {
          let self = this;
          this.modelScenario.fetch({
            reset: true,
            data: {
              IsPagination : false,
              IsApproved: true
            },
            success: function(req, res) {
              self.renderScenarioList(res);
              self.renderSecondScenarioList(res);
            }
          });
        },
        renderScenarioList: function(data) {
          let html = '<select class="form-control select" name="NamaScenario" data="scenario0">';
          html += '<option value="">--Pilih Skenario--</option>';
          if (data) {
            _.each(data, function(item) {
              html += '<option value="'+ item.Id +'">'+ item.NamaScenario +'</option>';
            });
          }
          html += '</select>';
          this.$('[first-scenario]').append(html);
        },
        renderSecondScenarioList: function(data) {
          let htmls = '<select class="form-control select" name="NamaScenario" data="scenario1">';
          htmls += '<option value="">--Pilih Skenario--</option>';
          if (data) {
            _.each(data, function(item) {
              htmls += '<option value="'+ item.Id +'">'+ item.NamaScenario +'</option>';
            });
          }
          htmls += '</select>';
          this.$('[second-scenario]').append(htmls);
        },
        renderValidation: function() {
          var self = this;
          this.$('[ehs-form]').bind("keypress", function (e) {
                if (e.keyCode == 13) {
                    return false;
                }
            });
          this.$('[ehs-form]').bootstrapValidator({
              fields: {
              }
            })
            .on('success.form.bv', function(e) {
              e.preventDefault();
              self.getConfirmation();
            });
        },
        getConfirmation: function(){
          var retVal = confirm("Are you sure want to add this scenarios ?");
          if( retVal == true ){
             this.doSave();
          }
          else{
            this.$('[type="submit"]').attr('disabled', false);
          }
        },
        doSave: function() {
          var self = this;
          var data = {};
          var scenarios = [];
          debugger;
          for (var e = 0; e < 2; e++) {
              var scenarioValue = self.$('[data="scenario'+[e]+'"]').val();
              scenarios.push(scenarioValue);
          }
          data.Scenarios = scenarios;
          this.model.save(data);
        }
    });
});
