define(function(require, exports, module) {
    'use strict';
    var View = require('modaldialogdefault');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var Model = require('./../model');
    var eventAggregator = require('eventaggregator');
    require('bootstrap-validator');
    require('jquerymask');

    module.exports = View.extend({
        template: _.template(template),
        initialize: function() {
          var self = this;
          this.model = new Model();
          this.listenTo(this.model, 'request', function() {});

          this.listenTo(this.model, 'sync error', function() {});

          this.listenTo(this.model, 'sync', function(model) {
            commonFunction.responseSuccessUpdateAddDelete('Name korelasi Tipe berhasil dibuat.');
            self.$el.modal('hide');
            eventAggregator.trigger('master/correlationmatrix/add:fecth');
          });
        },
        afterRender: function() {
          this.renderValidation();
        },
        renderValidation: function() {
          var self = this;
          this.$('[ehs-form]').bind("keypress", function (e) {
                if (e.keyCode == 13) {
                    return false;
                }
            });
          this.$('[ehs-form]').bootstrapValidator({
              fields: {
                NamaCorrelationMatrix: {
                  validators:{
                    stringLength: {
                        message: 'Name Korelasi Tipe harus kurang dari 50 karakater',
                        max: 50
                    },
                    notEmpty:{
                      message: 'Name Korelasi Tipe wajib diisi'
                    },
                    regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'Nama Korelasi Tipe hanya alfabet dan spasi'
                    }
                  }
                },
                Nilai: {
                  validators: {
                    notEmpty: {
                      message: 'Besaran Korelasi wajib diisi'
                    },
                    numeric: {
                      message: 'Besaran Korelasi format tidak valid. Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                },
              }
            })
            .on('success.form.bv', function(e) {
              e.preventDefault();
              self.getConfirmation();
            });
        },
        getConfirmation: function(){
          var data = this.$('[name="NamaCorrelationMatrix"]').val();
          var action = "add";
          var retVal = confirm("Are you sure want to " + action + " Correlation Type : "+ data +" ?");
          if( retVal == true ){
             this.doSave();
          }
          else{
            this.$('[type="submit"]').attr('disabled', false);
          }
        },
        doSave: function() {
          var data = commonFunction.formDataToJson(this.$('form').serializeArray());
          this.model.save(data);
        }
    });
});
