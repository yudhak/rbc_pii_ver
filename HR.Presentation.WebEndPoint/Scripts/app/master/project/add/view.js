define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var commonFunction = require('commonfunction');
  var commonConfig = require('commonconfig');
  var Model = require('./../model');
  var eventAggregator = require('eventaggregator');
  require('bootstrap-validator');
  require('datetimepicker');
  require('select2');
  var moment = require('moment');

  module.exports = View.extend({
    template: _.template(template),
    initialize: function () {
      var self = this;
      this.sektorList = null;
      this.model = new Model();
      this.listenTo(this.model, 'request', function () {});

      this.listenTo(this.model, 'sync error', function () {});

      commonFunction.setSelect2Tahapan(this);
      commonFunction.setSelect2Sektor(this);
      commonFunction.setSelect2Risk(this);
      commonFunction.setSelect2User(this);
      this.listenTo(this.model, 'sync', function (model) {
        commonFunction.responseSuccessUpdateAddDelete('Proyek berhasil dibuat.');
        self.$el.modal('hide');
        eventAggregator.trigger('master/project/add:fecth');
      });
      this.listenTo(eventAggregator, 'master/project/add/select_risk:risk_selected', function(riskNames, riskIds) {
        self.setTemplate(riskNames);
        self.riskIds = riskIds;
      });
    },
    afterRender: function () {
      this.$('[name="TahunAwalProject"], [name="TahunAkhirProject"], .glyphicon glyphicon-calendar').datetimepicker({
        // defaultDate: new(Date),
        format: commonConfig.datePickerFormat,
      });
      // this.$('[name="TahunAkhirProject"]').datetimepicker({
      //   // defaultDate: new(Date),
      //   format: commonConfig.datePickerFormat,
      // });
      this.renderValidation();

      this.$('[name="NamaSektor"], [name="UserName"], [name="NamaTahapan"]').select2({ width: '100%' });           
    },
    events: {
      'click [name="SelectRisk"]': 'selectRisk',
      'click [name="EditSelectRisk"]': 'selectRisk',
      'dp.change [name="TahunAwalProject"]': 'revalidationDate',
      'dp.change [name="TahunAkhirProject"]': 'revalidationDate',
      'change [name="Minimum"]': 'revalidationMin',
      'change [name="Maximum"]': 'revalidationMax'
    },
    revalidateStartProyek: function(){
      this.$('[ehs-form]').bootstrapValidator('revalidateField', 'TahunAwalProject');
      this.$('[ehs-form]').bootstrapValidator('revalidateField', 'TahunAkhirProject');
    },
    revalidationMin: function() {
      this.$('[name="Maximum"]').prop("disabled", false);
      this.$('[ehs-form]').bootstrapValidator('revalidateField', 'Minimum');
    },
    revalidationMax: function() {
      this.$('[ehs-form]').bootstrapValidator('revalidateField', 'Maximum');
    },
    setTemplate: function(obj) {
      //this.$('.select2 select2-container select2-container--bootstrap').addClass('width-dropdown');
      var chosenName = obj.join(", ");
      var currentChosenRisk = this.$('[name="ChosenRisk"]').text(chosenName);
      if(this.$('[name="ChosenRisk"]').val()){
        this.$('[name="EditSelectRisk"]').removeClass('hidden');
        this.$('[name="SelectRisk"]').addClass('hidden');
      }
    },
    selectRisk: function () {
      var self = this;
      require(['./../select_risk/view'], (View) => {
        commonFunction.setDefaultModalDialogFunction(self, View, this.model);
      });
    },
    revalidationDate: function() {
      this.$('[name="TahunAkhirProject"]').prop("disabled", false);
      this.revalidateStartProyek();  
    },
    doValidateDate: function() {
      var self = this;
      var status = false;
      var startDate = moment(this.$('[name="TahunAwalProject"]').val()).format('YYYYMMDD');
      var endDate = moment(this.$('[name="TahunAkhirProject"]').val()).format('YYYYMMDD');
      
      if(endDate != "" && startDate != "") {
        if(startDate > endDate) {
          commonFunction.responseWarningCannotExecute("Tanggal awal proyek tidak boleh lebih besar dari tanggal akhir proyek.");
        } else if (endDate < startDate) {
          commonFunction.responseWarningCannotExecute("Tanggal akhir proyek tidak boleh lebih kecil dari tanggal awal proyek.");
        } else {
          status = true;
        }
      }
      return status;
    },
    doValidateMaxMin: function() {
      var status = false;
      var minVal = parseInt(this.$('[name="Minimum"]').val());
      var maxVal = parseInt(this.$('[name="Maximum"]').val());
      if((minVal != "" && maxVal != "") || (minVal == 0 || maxVal == 0)) {
        if(minVal > maxVal) {
          commonFunction.responseWarningCannotExecute("Minimum tidak boleh lebih besar dari Maximum.");
        } else if (maxVal < minVal) {
          commonFunction.responseWarningCannotExecute("Maxmium tidak boleh lebih kecil dari Minimum.");
        } else {
          status = true;
        }
      }
      return status;
    },
    renderValidation: function () {
      var self = this;
      this.$('[ehs-form]').bind("keypress", function (e) {
        if (e.keyCode == 13) {
          return false;
        }
      });
      this.$('[ehs-form]').bootstrapValidator({
          fields: {
            NamaProject: {
              validators: {
                stringLength: {
                  message: 'Nama Proyek harus kurang dari 50 karakter',
                  max: 50
                },
                notEmpty: {
                  message: 'Nama Proyek harus diisi'
                }
              }
            },
            Keterangan: {
              validators: {
                stringLength: {
                  message: 'Keterangan harus kurang dari 500 karakter',
                  max: 500
                },
                notEmpty: {
                  message: 'Keterangan harus diisi'
                }
              }
            },
            TahunAwalProject: {
              validators: {
                notEmpty: {
                  message: 'Awal Proyek harus diisi'
                }
              }
            },
            TahunAkhirProject: {
              validators: {
                notEmpty: {
                  message: 'Akhir Proyek harus diisi'
                }
              }
            },
            Minimum: {
              validators: {
                notEmpty: {
                  message: 'Minimum harus diisi'
                },
                numeric: {
                  message: 'Minimum tidak valid. Format yang diizinkan 123.45 atau 12.34 atau 12',
                  thousandsSeparator: '',
                  decimalSeparator: '.'
                },
                // between: {
                //   max: 'Maximum',
                //   message: 'Nilai Minimum tidak boleh lebih besar dari Maximum'
                // }
              }
            },
            Maximum: {
              validators: {
                notEmpty: {
                  message: 'Maksimum harus diisi'
                },
                numeric: {
                  message: 'Maksimum tidak valid. Format yang diizinkan 123.45 atau 12.34 atau 12',
                  thousandsSeparator: '',
                  decimalSeparator: '.'
                },
                // between: {
                //   min: 'Minimum',
                //   message: 'Nilai Maximum tidak boleh lebih kecil dari Minimum'
                // }
              }
            },
            NamaSektor: {
              validators: {
                notEmpty: {
                  message: 'Sektor harus diisi'
                }
              }
            },
            NamaTahapan: {
              validators: {
                notEmpty: {
                  message: 'Tahapan Penjaminan harus diisi'
                }
              }
            },
            UserName: {
              validators:{
                notEmpty:{
                  message: 'User Proyek harus diisi'
                }
              }
            },
            NamaCategoryRisk: {
              validators: {
                notEmpty: {
                  message: 'Risk harus diisi'
                }
              }
            }
          }
        })
        .on('success.form.bv', function (e) {
          e.preventDefault();
          if(self.doValidateRiskRegister()){
            self.getConfirmation();
          } 
        });
    },
    getConfirmation: function(){
      var statusDate = this.doValidateDate();
      var statusMaxMin = this.doValidateMaxMin();
      if(statusDate && statusMaxMin) {
        var data = this.$('[name="NamaProject"]').val();
        var action = "tambah";
        var retVal = confirm("Apakah anda yakin ingin " + action + " Project : "+ data +" ?");
        if( retVal == true ){
          this.doSave();
        }
        else{
          this.$('[type="submit"]').attr('disabled', false);
        }
      } else {
        this.$('[type="submit"]').attr('disabled', false);
      }
    },
    doValidateRiskRegister: function() {
      const risk = this.riskIds;
      if (!risk) {
        commonFunction.responseWarningCannotExecute("Kategori Risiko wajib diisi.");
        this.$('[type="submit"]').attr('disabled', false);
        return false;
      } else {
        return true;
      }
    },
    doSave: function () {
      var data = commonFunction.formDataToJson(this.$('form').serializeArray());
      data.TahapanId = this.$('[name="NamaTahapan"]').val();
      data.SektorId = this.$('[name="NamaSektor"]').val();
      data.UserId = this.$('[name="UserName"]').val();
      data.RiskRegistrasiId = this.riskIds;
      this.model.save(data);
    }
  });
});