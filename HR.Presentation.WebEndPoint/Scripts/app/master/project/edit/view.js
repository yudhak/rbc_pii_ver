define(function(require, exports, module) {
    'use strict';
    var View = require('modaldialogdefault');
    var template = require('text!./template.html');
    var Model = require('./../model');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var eventAggregator = require('eventaggregator');
    var moment = require('moment');
    require('bootstrap-validator');
    require('datetimepicker');
    require('select2');

    module.exports = View.extend({
        template: _.template(template),
        initialize : function(){
          var self = this;
          var tahapanId = this.model.get('TahapanId');
          var sektorId = this.model.get('SektorId');
          var projectId = this.model.get('Id');
          var userId = this.model.get('UserId');
          this.isActive = this.model.get('IsActive');
          this.project = this.model;
          this.riskIds = [];
          if(this.model.get('ProjectRiskStatus').length){
            _.each(this.model.get('ProjectRiskStatus'), function(item) {
              if (item.IsProjectUsed) {
                self.riskIds.push(item.RiskRegistrasiId);
              }
            });
          }
          this.model = new Model();
          this.model.set(this.model.idAttribute, projectId);
          this.listenToOnce(this.model, 'sync', function(model) {
              this.render();
              var data = model.toJSON();
              this.listenTo(this.model, 'sync', function() {
                commonFunction.responseSuccessUpdateAddDelete('Proyek berhasil diubah.');
                self.$el.modal('hide');
                eventAggregator.trigger('master/project/edit:fecth');
              });
              commonFunction.setSelect2Tahapan(this).then(function(object){
                self.$(object.options.selector).val(tahapanId).trigger('change');
              });
              commonFunction.setSelect2Sektor(this).then(function(object){
                self.$(object.options.selector).val(sektorId).trigger('change');
              });
              commonFunction.setSelect2User(this).then(function(object){
                self.$(object.options.selector).val(userId).trigger('change');
              });
          }, this);
          var selectorTahapan = this.$('[name="NamaTahapan"]');
          this.on('beforeRender beforeRemove cleanup', function() {
            if (selectorTahapan.length && selectorTahapan.hasClass('select2-hidden-accessible')) {
                selectorTahapan.select2('destroy');
            }
          });
          var selectorSektor = this.$('[name="NamaSektor"]');
          this.on('beforeRender beforeRemove cleanup', function() {
            if (selectorSektor.length && selectorSektor.hasClass('select2-hidden-accessible')) {
                selectorSektor.select2('destroy');
            }
          });
          var selectorUser = this.$('[name="UserName"]');
          this.on('beforeRender beforeRemove cleanup', function() {
            if (selectorUser.length && selectorUser.hasClass('select2-hidden-accessible')) {
                selectorUser.select2('destroy');
            }
          });
          this.listenTo(eventAggregator, 'master/project/select_risk:risk_selected', function(obj) {
            self.riskRegistrasiId = obj;
          });
          this.listenTo(eventAggregator, 'master/project/add/select_risk:risk_selected', function(riskNames, riskIds) {
            self.renderRiskAfterEdit(riskNames);
            self.riskIds = riskIds;
          });
          this.once('afterRender', function() {
              this.model.fetch();
          });
        },
        afterRender : function(){
          this.$('[name="TahunAwalProject"], [name="TahunAkhirProject"]').datetimepicker({
            format : commonConfig.datePickerFormat
          });
          this.setTemplate(this.project);
          this.renderValidation();
          this.$('[name="NamaSektor"], [name="UserName"], [name="NamaTahapan"]').select2({ width: '100%' });           
        },
        events: {
          'click [name="SelectRisk"]': 'selectRisk',
          'dp.change [name="TahunAwalProject"]': 'revalidationDate',
          'dp.change [name="TahunAkhirProject"]': 'revalidationDate',
          'change [name="Minimum"]': 'revalidationMin',
          'change [name="Maximum"]': 'revalidationMax'
        },
        setTemplate: function(obj) {
          var riskNames = '';
          var risk = obj.get('ProjectRiskStatus');
          if(risk.length > 0) {
            for(var i = 0; i < risk.length; i++) {
              if (risk[i].IsProjectUsed) {
                riskNames += risk[i].NamaCategoryRisk;
                riskNames += ', ';
              }
            }
          }
          this.$('[name="ChosenRisk"]').text(riskNames);
          if (this.isActive != null) {
            var status = this.isActive.toString();
            this.$('[name="IsActive"]').val(status);
          }
        },
        renderRiskAfterEdit: function(obj) {
          var chosenName = obj.join(", ");
          this.$('[name="ChosenRisk"]').text(chosenName);
        },
        selectRisk: function() {
          var self = this;
          require(['./../select_risk/view'], (View) => {
            commonFunction.setDefaultModalDialogFunction(self, View, this.model, this.riskIds);
          });
        },
        revalidateStartProyek: function(){
          // this.$('[ehs-form]').bootstrapValidator('revalidateField', 'TahunAwalProject');
          // this.$('[ehs-form]').bootstrapValidator('revalidateField', 'TahunAkhirProject');
        },
        revalidationMin: function() {
          this.$('[name="Maximum"]').prop("disabled", false);
          this.$('[ehs-form]').bootstrapValidator('revalidateField', 'Minimum');
        },
        revalidationMax: function() {
          this.$('[ehs-form]').bootstrapValidator('revalidateField', 'Maximum');
        },
        revalidationDate: function() {
          this.$('[name="TahunAkhirProject"]').prop("disabled", false);
          this.revalidateStartProyek();  
        },
        doValidateDate: function() {
          var self = this;
          var status = false;
          var startDate = moment(this.$('[name="TahunAwalProject"]').val()).format('YYYYMMDD');
          var endDate = moment(this.$('[name="TahunAkhirProject"]').val()).format('YYYYMMDD');
          
          if(endDate != "" && startDate != "") {
            if(startDate > endDate) {
              commonFunction.responseWarningCannotExecute("Tanggal awal proyek tidak boleh lebih besar dari tanggal akhir proyek.");
            } else if (endDate < startDate) {
              commonFunction.responseWarningCannotExecute("Tanggal akhir proyek tidak boleh lebih kecil dari tanggal awal proyek.");
            } else {
              status = true;
            }
          }
          return status;
        },
        doValidateMaxMin: function() {
          var status = false;
          var minVal = parseInt(this.$('[name="Minimum"]').val());
          var maxVal = parseInt(this.$('[name="Maximum"]').val());
          if((minVal != "" && maxVal != "") || (minVal == 0 || maxVal == 0)) {
            if(minVal > maxVal) {
              commonFunction.responseWarningCannotExecute("Minimum tidak boleh lebih besar dari Maximum.");
            } else if (maxVal < minVal) {
              commonFunction.responseWarningCannotExecute("Maxmium tidak boleh lebih kecil dari Minimum.");
            } else {
              status = true;
            }
          }
          return status;
        },
        renderValidation: function() {
          var self = this;
          this.$('[ehs-form]').bootstrapValidator({
              fields: {
                NamaProject: {
                  validators: {
                    stringLength: {
                      message: 'Nama Proyek harus kurang dari 50 karakter',
                      max: 50
                    },
                    notEmpty: {
                      message: 'Nama Proyek harus diisi'
                    }
                  }
                },
                Keterangan: {
                  validators: {
                    stringLength: {
                      message: 'Keterangan harus kurang dari 500 karakter',
                      max: 500
                    },
                    notEmpty: {
                      message: 'Keterangan harus diisi'
                    }
                  }
                },
                TahunAwalProject: {
                  validators: {
                    notEmpty: {
                      message: 'Awal Proyek harus diisi'
                    }
                  }
                },
                TahunAkhirProject: {
                  validators: {
                    notEmpty: {
                      message: 'Akhir Proyek harus diisi'
                    }
                  }
                },
                Minimum: {
                  validators: {
                    notEmpty: {
                      message: 'Minimum harus diisi'
                    },
                    numeric: {
                      message: 'Minimum tidak valid. Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    },
                    // between: {
                    //   max: 'Maximum',
                    //   message: 'Nilai Minimum tidak boleh lebih besar dari Maximum'
                    // }
                  }
                },
                Maximum: {
                  validators: {
                    notEmpty: {
                      message: 'Maksimum harus diisi'
                    },
                    numeric: {
                      message: 'Maksimum tidak valid. Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    },
                    // between: {
                    //   min: 'Minimum',
                    //   message: 'Nilai Maximum tidak boleh lebih kecil dari Minimum'
                    // }
                  }
                },
                NamaSektor: {
                  validators: {
                    notEmpty: {
                      message: 'Sektor harus diisi'
                    }
                  }
                },
                NamaTahapan: {
                  validators: {
                    notEmpty: {
                      message: 'Tahapan Penjaminan harus diisi'
                    }
                  }
                },
                UserName: {
                  validators:{
                    notEmpty:{
                      message: 'User Proyek harus diisi'
                    }
                  }
                },
                NamaCategoryRisk: {
                  validators: {
                    notEmpty: {
                      message: 'Risk harus diisi'
                    }
                  }
                }
              }
            })
            .on('success.form.bv', function(e) {
              e.preventDefault();
              self.getConfirmation();
            });
        },
        getConfirmation: function(){
          var statusDate = this.doValidateDate();
          var statusMaxMin = this.doValidateMaxMin();
          if(statusDate && statusMaxMin) {
            var data = this.$('[name="NamaProject"]').val();
            var action = "ubah";
            var retVal = confirm("Apakah anda yakin ingin " + action + " Project : "+ data +" ?");
            if( retVal == true ){
              this.doSave();
            }
            else{
              this.$('[type="submit"]').attr('disabled', false);
            }
          } else {
            this.$('[type="submit"]').attr('disabled', false);
          }
        },
        doSave: function() {
          var data = commonFunction.formDataToJson(this.$('form').serializeArray());
          data.TahapanId = this.$('[name="NamaTahapan"]').val();
          data.SektorId = this.$('[name="NamaSektor"]').val();
          data.UserId = this.$('[name="UserName"]').val();
          data.RiskRegistrasiId = this.riskIds;
          this.model.save(data);
        }
    });
});
