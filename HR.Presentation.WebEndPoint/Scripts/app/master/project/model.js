define(function(require, exports, module) {
    'use strict';

    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'Id',
        urlRoot: 'api/Project',
        defaults: function() {
            return {
              NamaProject : '',
              TahunAwalProject : '',
              TahunAkhirProject : '',
              StatusProject : '',
              IsActive: '',
              Minimum : '',
              Maximum : '',
              Keterangan : '',
              TahapanId : '',
              SektorId : '',
              RiskRegistrasiId : '',
              UserId: '',
              UserName: '',
              CreateBy : '',
              CreateDate : '',
              UpdateBy : '',
              UpdateDate : '',
              IsDelete : '',
              DeleteDate : '',
              IsSelectedAll: ''
            }
        }
    });
});
