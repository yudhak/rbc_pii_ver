define(function(require, exports, module) {
    'use strict';
    var View = require('modaldialogdefault');
    var template = require('text!./template.html');
    var Model = require('./../model');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var eventAggregator = require('eventaggregator');
    require('bootstrap-validator');

    module.exports = View.extend({
        template: _.template(template),
        initialize : function(){
          var self = this;
          var correlationId = this.model.get('Id');
          this.model = new Model();
          this.model.set(this.model.idAttribute, correlationId);
          this.listenToOnce(this.model, 'sync', function(model) {
              this.render();
              var data = model.toJSON();
              this.listenTo(this.model, 'sync', function() {
                commonFunction.responseSuccessUpdateAddDelete('Asset Data berhasil diubah.');
                self.$el.modal('hide');
                eventAggregator.trigger('master/asset_data/edit:fetch');
              });
          }, this);

          this.once('afterRender', function() {
              this.model.fetch();
          });
        },
        events: {
          'change [data-currency-validation]' : 'liveCurrencyValidation',
        },
        afterRender : function(){
          this.renderValidation();
        },
        liveCurrencyValidation: function(e) {
          var exposeFormat = /^\s*[0-9]+([\.][0-9][0-9]{0,12})?$|^$|^\s*$/;
          var attrName = e.target.name;
          var attrValue = this.$('[name="'+ attrName +'"]').val();
          if (exposeFormat.test(attrValue)) {
            this.$('[name="'+ attrName +'"]').removeClass('invalidCurrencyFormat');
          } else {
            commonFunction.responseWarningCannotExecute("Nilai Aset/Proporsi/Asumsi Persentasi/Diasumsikan Kembali tidak sesuai format. Format yang dizinkan 1000.02 / 123.39847. Angka di belakang koma hanya diizinkan maksimal sebanyak 13 digit.");
            this.$('[name="'+ attrName +'"]').addClass('invalidCurrencyFormat');
          }
        },
        currencyValidation: function() {
          var self = this;
          var isCurrencyValid = true;
          var valueFormat = /^\s*[0-9]+([\.][0-9][0-9]{0,12})?$|^$|^\s*$/;
          this.$('[data-currency-validation]').each(function(i, item) {
            var currentValue = self.$('[name="'+ item.name +'"]').val();
            if (!valueFormat.test(currentValue)) {
              commonFunction.responseWarningCannotExecute("Nilai Aset/Proporsi/Asumsi Persentasi/Diasumsikan Kembali tidak sesuai format. Format yang dizinkan 1000.02 / 123.39847. Angka di belakang koma hanya diizinkan maksimal sebanyak 13 digit.");
              self.$('[name="'+ item.name +'"]').addClass('invalidCurrencyFormat');
              isCurrencyValid = false;
            } else {
              self.$('[name="'+ item.name +'"]').removeClass('invalidCurrencyFormat');
            }
          });
          return isCurrencyValid;
        },
        renderValidation: function() {
          var self = this;
          this.$('[ehs-form]').bootstrapValidator({
              fields: {
                AssetClass: {
                  validators: {
                    notEmpty: {
                      message: 'Asset Class wajib diisi'
                    }
                  }
                },
                TermAwal: {
                  validators: {
                    integer: {
                      message: 'Term Awal format tidak valid. Format yang diizinkan hanya angka.'
                    }
                  }
                },
                TermAkhir: {
                  validators: {
                    integer: {
                      message: 'Term Akhir format tidak valid. Format yang diizinkan hanya angka.'
                    }
                  }
                },
                OutstandingStartYears: {
                  validators: {
                    integer: {
                      message: 'Diluar tahun awal format tidak valid. Format yang diizinkan hanya angka.'
                    }
                  }
                },
                OutstandingEndYears: {
                  validators: {
                    integer: {
                      message: 'Diluar tahun akhir format tidak valid. Format yang diizinkan hanya angka.'
                    }
                  }
                },
                AssetValue: {
                  validators: {
                    numeric: {
                      message: 'Nilai Asset format tidak valid. Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                },
                Porpotion: {
                  validators: {
                    numeric: {
                      message: 'Proporsi format tidak valid. Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                },
                AssumedReturnPercentage: {
                  validators: {
                    numeric: {
                      message: 'Asumsi Persentase format tidak valid. Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                },
                AssumedReturn: {
                  validators: {
                    numeric: {
                      message: 'Diasumsikan kembali format tidak valid. Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                }
              }
            })
            .on('success.form.bv', function(e) {
              e.preventDefault();
              self.getConfirmation();
            });
        },
        getConfirmation: function(){
          var isCurrencyValid = this.currencyValidation();
          if (isCurrencyValid) {
            var data = this.$('[name="AssetClass"]').val();
            var action = "Ubah";
            var retVal = confirm("Apakah anda yakin ingin " + action + " Asset Data : "+ data +" ?");
            if( retVal == true ){
               this.doSave();
            }
            else{
              this.$('[type="submit"]').attr('disabled', false);
            }
          }
        },
        doSave: function() {
          var data = commonFunction.formDataToJson(this.$('form').serializeArray());
          this.model.save(data);
        }
    });
});