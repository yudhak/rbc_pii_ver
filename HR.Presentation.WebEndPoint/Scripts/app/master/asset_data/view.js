define(function(require, exports, module) {
    'use strict';

    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');
    var Table = require('./table/table');
    var Collection = require('./collection');
    var Paging = require('paging');
    var TablePMN = require('./pmn/table/table');
    var CollectionPMN = require('./pmn/collection');
    var PagingPMN = require('paging');

    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content tbl bg-white',
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.keyword = "";
            this.table = new Table({
                collection: new Collection()
            });
            this.paging = new Paging({
                collection: this.table.collection
            });
            this.tablePMN = new TablePMN({
                collection: new CollectionPMN()
            });
            this.pagingPMN = new PagingPMN({
                collection: this.tablePMN.collection
            });
            this.listenTo(eventAggregator, 'master/asset_data/add:fetch', function() {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'master/asset_data/edit:fetch', function() {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'master/asset_data/delete:fetch', function(model) {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'master/asset_data/pmn/add:fetch', function() {
              self.fetchDataPMN();
            });
            this.listenTo(eventAggregator, 'master/asset_data/pmn/edit:fetch', function() {
              self.fetchDataPMN();
            });
            this.listenTo(eventAggregator, 'master/asset_data/pmn/delete:fetch', function(model) {
              self.fetchDataPMN();
            });
            this.roles = JSON.parse(localStorage.getItem('roles'));
        },
        events: {
            'click [name="add"]': 'add',
            'click [name="addPMN"]':'addPMN',
            'click [name="btn-filter"]': 'applyFilter',
            'keyup #keyword': 'applyFilter',
            'click [name="btn-filterPMN"]': 'applyFilterPMN',
            'keyup #keywordPMN': 'applyFilterPMN',
        },
        afterRender: function() {
            this.$('[obo-table-asset_data]').append(this.table.el);
            this.table.render();
            this.insertView('[obo-paging-asset_data]', this.paging);
            this.paging.render();
            this.fetchData();

            this.$('[obo-table-pmn]').append(this.tablePMN.el);
            this.tablePMN.render();
            this.insertView('[obo-paging-pmn]', this.pagingPMN);
            this.pagingPMN.render();
            this.fetchDataPMN();
            this.setRoleAccess();
        },
        fetchData: function() {
            var param  = [];
            param.Search = this.keyword;
            this.table.collection.fetch({
                reset: true,
                data: param
            })
        },
        fetchDataPMN: function() {
            var param  = [];
            param.Search = this.keyword;
            this.tablePMN.collection.fetch({
                reset:true,
                data: param
            })
        },
        add: function() {
            var self = this;
            require(['./add/view'], function(View) {
                commonFunction.setDefaultModalDialogFunction(self, View);
            });
        },
        addPMN : function(){
          var self = this;
          require(['./pmn/add/view'], function(View) {
            commonFunction.setDefaultModalDialogFunction(self, View);
          });
        },
        applyFilter: function() {
            var self = this;
            var param  = []
            var keyword = this.$('#keyword').val();
            this.keyword = keyword;
            param.Search = keyword;
            var keyIndex = this.$('#keyIndex option:selected').val();
            param.SearchBy = keyIndex;

            this.table.collection.fetch({
                reset: true,
                data: param
            })
        },
        applyFilterPMN: function() {
            var self = this;
            var param  = []
            var keyword = this.$('#keywordPMN').val();
            this.keyword = keyword;
            param.Search = keyword;
            var keyIndex = this.$('#keyIndex2 option:selected').val();
            param.SearchBy = keyIndex;

            this.tablePMN.collection.fetch({
                reset: true,
                data: param
            })
        },
        setRoleAccess: function() {
            let self = this;
            if (this.roles) {
                $.grep(this.roles, function(item) {
                    if (item.Name.trim() == "Master") {
                        if (item.SubMenu) {
                            _.each(item.SubMenu, function(val) {
                                switch (val.Name) {
                                    case "Master-AssetData":
                                        if (val.SubMenu) {
                                            _.each(val.SubMenu, function(item) {
                                                switch(item.Action) {
                                                    case "add":
                                                        self.$('[name="add"]').removeClass('hide');
                                                        break;
                                                }
                                            });
                                        }
                                        break;
                                    case "Master-PMN":
                                        if (val.SubMenu) {
                                            _.each(val.SubMenu, function(item) {
                                                switch(item.Action) {
                                                    case "add":
                                                        self.$('[name="addPMN"]').removeClass('hide');
                                                        break;
                                                }
                                            });
                                        }
                                        break;
                                }
                            });
                        }
                    }
                });
            }
        }
    });
});