define(function(require, exports, module) {
    'use strict';
    var View = require('modaldialogdefault');
    var template = require('text!./template.html');
    var Model = require('./../model');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var eventAggregator = require('eventaggregator');
    require('bootstrap-validator');

    module.exports = View.extend({
        template: _.template(template),
        initialize : function(){
          var self = this;
          var correlationId = this.model.get('Id');
          this.model = new Model();
          this.model.set(this.model.idAttribute, correlationId);
          this.listenToOnce(this.model, 'sync', function(model) {
              this.render();
              var data = model.toJSON();
              this.listenTo(this.model, 'sync', function() {
                commonFunction.responseSuccessUpdateAddDelete('PMN berhasil diubah.');
                self.$el.modal('hide');
                eventAggregator.trigger('master/asset_data/pmn/edit:fetch');
              });
          }, this);

          this.once('afterRender', function() {
              this.model.fetch();
          });
        },
        afterRender : function(){
          this.renderValidation();
        },
        renderValidation: function() {
          var self = this;
          this.$('[ehs-form]').bootstrapValidator({
              fields: {
                PMNToModalDasarCap: {
                  validators: {
                    notEmpty: {
                      message: 'PMN untuk Modal Dasar Cap harus diisi'
                    },
                    numeric: {
                      message: 'PMN untuk Modal Dasar Cap format tidak valid.  Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                },
                RecourseDelay: {
                  validators: {
                    notEmpty: {
                      message: 'Penundaan Cap harus diisi'
                    },
                    numeric: {
                      message: 'Penundaan format is not valid.  Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                },
                DelayYears: {
                  validators: {
                    notEmpty: {
                      message: 'Tahun Penundaan harus diisi'
                    },
                    numeric: {
                      message: 'Tahun Penundaan format tidak valid.  Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                },
                OpexGrowth: {
                  validators: {
                    notEmpty: {
                      message: 'Kenaikan Opex harus diisi'
                    },
                    numeric: {
                      message: 'Kenaikan Opex format tidak valid.  Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                },
                Opex: {
                  validators: {
                    notEmpty: {
                      message: 'Opex harus diisi'
                    },
                    numeric: {
                      message: 'Opex format tidak valid.  Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                },
                ValuePMNToModalDasarCap: {
                  validators: {
                    notEmpty: {
                      message: 'ValuePMNToModalDasarCap harus diisi'
                    },
                    numeric: {
                      message: 'ValuePMNToModalDasarCap format tidak valid.  Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                }
              }
            })
            .on('success.form.bv', function(e) {
              e.preventDefault();
              self.getConfirmation();
            });
        },
        getConfirmation: function(){
          var data = this.$('[name="PMNToModalDasarCap"]').val();
          var action = "Ubah";
          var retVal = confirm("Apakah anda yakin ingin " + action + " Asset Data : "+ data +" ?");
          if( retVal == true ){
             this.doSave();
          }
          else{
            this.$('[type="submit"]').attr('disabled', false);
          }
        },
        doSave: function() {
          var data = commonFunction.formDataToJson(this.$('form').serializeArray());
          this.model.save(data);
        }
    });
});