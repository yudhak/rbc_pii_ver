define(function(require, exports, module) {
    'use strict';
    var View = require('modaldialogdefault');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var Model = require('./../model');
    var eventAggregator = require('eventaggregator');
    require('bootstrap-validator');
    require('jquerymask');

    module.exports = View.extend({
        template: _.template(template),
        initialize: function() {
          var self = this;
          this.model = new Model();
          this.listenTo(this.model, 'request', function() {});

          this.listenTo(this.model, 'sync error', function() {});

          this.listenTo(this.model, 'sync', function(model) {
            commonFunction.responseSuccessUpdateAddDelete('PMN berhasil dibuat.');
            self.$el.modal('hide');
            eventAggregator.trigger('master/asset_data/pmn/add:fetch');
          });
        },
        afterRender: function() {
          this.renderValidation();
        },
        renderValidation: function() {
          var self = this;
          this.$('[ehs-form]').bind("keypress", function (e) {
                if (e.keyCode == 13) {
                    return false;
                }
            });
          this.$('form').bootstrapValidator({
              fields: {
                PMNToModalDasarCap: {
                  validators: {
                    notEmpty: {
                      message: 'PMN untuk Modal Dasar Cap harus diisi'
                    },
                    numeric: {
                      message: 'PMN untuk Modal Dasar Cap format tidak valid.  Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                },
                RecourseDelay: {
                  validators: {
                    notEmpty: {
                      message: 'Penundaan Cap harus diisi'
                    },
                    numeric: {
                      message: 'Penundaan format is not valid.  Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                },
                DelayYears: {
                  validators: {
                    notEmpty: {
                      message: 'Tahun Penundaan harus diisi'
                    },
                    numeric: {
                      message: 'Tahun Penundaan format tidak valid.  Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                },
                OpexGrowth: {
                  validators: {
                    notEmpty: {
                      message: 'Kenaikan Opex harus diisi'
                    },
                    numeric: {
                      message: 'Kenaikan Opex format tidak valid.  Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                },
                Opex: {
                  validators: {
                    notEmpty: {
                      message: 'Opex harus diisi'
                    },
                    numeric: {
                      message: 'Opex format tidak valid.  Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                },
                ValuePMNToModalDasarCap: {
                  validators: {
                    notEmpty: {
                      message: 'ValuePMNToModalDasarCap harus diisi'
                    },
                    numeric: {
                      message: 'ValuePMNToModalDasarCap format tidak valid.  Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                }
              }
            })
            .on('success.form.bv', function(e) {
              e.preventDefault();
              self.getConfirmation();
            });
        },
        getConfirmation: function(){
        var data = this.$('[name="PMNToModalDasarCap"]').val();
        var action = "Tambah";
        var retVal = confirm("Apakah anda yakin ingin " + action + " PMN : "+ data +" ?");
        if( retVal == true ){
           this.doSave();
          }
        else{
          this.$('[type="submit"]').attr('disabled', false);
          }
        },
        doSave: function() {
          var data = commonFunction.formDataToJson(this.$('form').serializeArray());
          this.model.save(data);
        }
    });
});
