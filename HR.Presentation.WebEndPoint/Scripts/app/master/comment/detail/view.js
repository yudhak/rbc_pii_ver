define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');
    var Table = require('./table/table');
    var Collection = require('./collection');
    var Paging = require('paging');
    var Model = require('./../model');
    var ModelParent = require('./../model');

    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content tbl bg-white',
        template: _.template(template),
        initialize: function(options) {
            var self = this;
            this.matrixSelected = null;
            this.model = new Model();
            this.modelParent = new ModelParent();
            this.table = new Table({
                collection: new Collection()
            });
            this.paging = new Paging({
                collection: this.table.collection
            });
            this.parentId = commonFunction.getUrlHashSplit(3);
            this.listenTo(this.model, 'sync', () => {
                this.render();
            });
            this.modelParent.fetch({
                reset: true,
                data: {
                    id: this.parentId
                }
            });
            // this.model.set(this.model.idAttribute, commonFunction.getLastSplitHash());
            this.model.fetch({
                reset: true,
                data: {
                    id: this.parentId
                }
            });
            this.once('afterRender', () => {
                this.fetchData();
            });
            this.listenTo(eventAggregator, 'master/comment/detail/add:fetch', function() {
              self.fetchData();
            });
            this.listenTo(eventAggregator, 'master/comment/detail/edit:fetch', function() {
             self.fetchData();
            });
            this.listenTo(eventAggregator, 'master/comment/detail/delete:fetch', function(model) {
              self.fetchData();
            });
            this.roles = JSON.parse(localStorage.getItem('roles'));
        },
        events: {
            'click [name="add"]': 'add',
            'click [name="btn-filter"]': 'applyFilter'
        },
        afterRender: function() {
            this.$('[obo-table-comment]').append(this.table.el);
            this.table.render();
            this.insertView('[obo-paging-comment]', this.paging);
            this.paging.render();
            this.fetchData();
            this.setRoleAccess();
        },
        fetchData: function() {
            var self = this;
            this.table.collection.fetch({
                reset: true,
                data: {
                    ParentId: this.parentId
                },
                success: function(req, res) {
                    self.matrixSelected = res;
                }
            })
        },
        add: function() {
            var self = this;
            var modelParent = this.modelParent;
            require(['./add/view'], function(View) {
                commonFunction.setDefaultModalDialogFunction(self, View, modelParent, self.matrixSelected);
            });
        },
        applyFilter: function() {
            var self = this;
            var param  = []
            var keyword = this.$('#keyword').val();
            param.Search = keyword;
            
            this.table.collection.fetch({
                reset: true,
                data: {param,
                ParentId: this.parentId}
            })
        },
        setRoleAccess: function() {
            let self = this;
            if (this.roles) {
                $.grep(this.roles, function(item) {
                    if (item.Name.trim() == "Master") {
                        if (item.SubMenu) {
                            _.each(item.SubMenu, function(val) {
                                switch (val.Name) {
                                    case "Master-Komentar":
                                        if (val.SubMenu) {
                                            _.each(val.SubMenu, function(item) {
                                                switch(item.Action) {
                                                    case "detail":
                                                        if (item.SubMenu) {
                                                            _.each(item.SubMenu, function(val) {
                                                                switch (val.Action) {
                                                                    case "add":
                                                                        self.$('[name="add"]').removeClass('hide');
                                                                        break;
                                                                }
                                                            });
                                                        }
                                                }
                                            });
                                        }
                                        break;
                                }
                            });
                        }
                    }
                });
            }
        }
    });
});