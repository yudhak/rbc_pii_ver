define(function(require, exports, module) {
    'use strict';
    var View = require('modaldialogdefault');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var Model = require('./../table/model');
    var eventAggregator = require('eventaggregator');
    require('bootstrap-validator');
    require('jquerymask');

    module.exports = View.extend({
        template: _.template(template),
        initialize: function(options) {
          var self = this;
          this.existingMatrix = [];
          if (options.view.results) {
            _.each(options.view.results, function(item) {
              self.existingMatrix.push(item.Matrix.Id);
            });
          }
          this.model = new Model();
          this.model.urlRoot += `/${commonFunction.getLastSplitHash()}`;
          this.model.set('ColorId', options.model.get('Id'));
          this.masterColor = options.model.get('Warna');

          this.listenTo(this.model, 'request', function() {});

          this.listenTo(this.model, 'sync error', function() {});
          
          commonFunction.setSelect2Matrix(this);

          this.listenTo(this.model, 'sync', function(model) {
            commonFunction.responseSuccessUpdateAddDelete('Komentar berhasil dibuat.');
            self.$el.modal('hide');
            eventAggregator.trigger('master/comment/detail/add:fetch');
          });
        },
        events: {
          'change [name="NamaMatrix"]': 'existingDataValidation',
        },
        afterRender: function() {
          this.setTemplate();
          this.renderValidation();
        },
        setTemplate: function() {
          this.$('[name="MasterWarna"]').html('<span class="col-md-2" style="background-color:'+ this.masterColor + '; color: ' + this.masterColor + ';">123456</span>');
          this.$('[name="NamaMatrix"]').select2({ width: '100%' });           
        },
        existingDataValidation: function() {
          var self = this;
          var selectedMatrix = this.$('[name="NamaMatrix"]').val();
          var found = false;
          if (this.existingMatrix) {
            $.grep(this.existingMatrix, function(item) {
              if (item == selectedMatrix) {
                found = true;
              }
            });
          }
          if (found) {
            commonFunction.responseWarningCannotExecute("Matrix yang sudah dipilih tidak bisa dipilih lagi. Silakan pilih Matrix lain.");
            this.$('[type="submit"]').attr('disabled', true);
          } else {
            this.$('[type="submit"]').attr('disabled', false);
          }
          return found;
        },
        renderValidation: function() {
          var self = this;
          this.$('form').bind("keypress", function (e) {
                if (e.keyCode == 13) {
                    return false;
                }
            });
          this.$('form').bootstrapValidator({
              fields: {
                Comment: {
                  validators:{
                    notEmpty:{
                      message: 'Komentar wajib diisi'
                    }
                  }
                },
                NamaMatrix: {
                  validators:{
                    notEmpty:{
                      message: 'Nama Matriks wajib diisi'
                    }
                  }
                }
              }
            })
            .on('success.form.bv', function(e) {
              e.preventDefault();
              self.getConfirmation();
            });
        },
        getConfirmation: function(){
          var self = this;
          var isMatrixValid = this.existingDataValidation();
          if (!isMatrixValid) {
            var data = this.$('[name="Comment"]').val();
            var action = "tambah";
            var retVal = confirm("Apakah anda yakin ingin " + action + " Komentar : "+ data +" ?");
            if( retVal == true ){
              this.doSave();
              }
            else{
              this.$('[type="submit"]').attr('disabled', false);
            }
          } 
        },
        doSave: function() {
          var data = commonFunction.formDataToJson(this.$('form').serializeArray());
          data.ColorCommentId = this.model.get('ColorId');
          data.MatrixId = this.$('[name="NamaMatrix"]').val();
          this.model.save(data);
        }
    });
});
