define(function(require, exports, module) {
    'use strict';
    var View = require('modaldialogdefault');
    var template = require('text!./template.html');
    var Model = require('./../model');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var eventAggregator = require('eventaggregator');
    require('bootstrap-validator');

    module.exports = View.extend({
        template: _.template(template),
        initialize : function(options){
          var self = this;
          this.currentId = this.model.get('MatrixId');
          this.existingMatrix = [];
          if (options.model.collection.models.length > 0) {
            _.each(options.model.collection.models, function(item) {
              self.existingMatrix.push(item.attributes.Matrix.Id);
            });
          }
          var commentId = this.model.get('Id');
          var matrixId = this.model.get('MatrixId');
          var colorCommentId = this.model.get('ColorCommentId');
          this.masterColor = options.model.get('Warna');

          this.model = new Model();
          this.model.set(this.model.idAttribute, commentId);
          this.listenToOnce(this.model, 'sync', function(model) {
              this.render();
              var data = model.toJSON();
              this.listenTo(this.model, 'sync', function() {
                commonFunction.responseSuccessUpdateAddDelete('Komentar berhasi diubah.');
                self.$el.modal('hide');
                eventAggregator.trigger('master/comment/detail/edit:fetch');
              });
              commonFunction.setSelect2Matrix(this).then(function(object){
                self.$(object.options.selector).val(matrixId).trigger('change');
              });
          }, this);
          var selectorMatrix = this.$('[name="NamaMatrix"]');
          this.on('beforeRender beforeRemove cleanup', function() {
            if (selectorMatrix.length && selectorMatrix.hasClass('select2-hidden-accessible')) {
                selectorMatrix.select2('destroy');
            }
          });
          this.once('afterRender', function() {
              this.model.fetch();
          });
        },
        afterRender : function(){
          this.setTemplate();
          this.renderValidation();
        },
        events: {
          'change [name="NamaMatrix"]': 'existingDataValidation',
        },
        setTemplate: function() {
          this.$('[name="MasterWarna"]').html('<span class="col-md-2" style="background-color:'+ this.masterColor + '; color: ' + this.masterColor + ';">123456</span>');
          this.$('[name="NamaMatrix"]').select2({ width: '100%' });           
        },
        renderValidation: function() {
          var self = this;
          this.$('[ehs-form]').bootstrapValidator({
              fields: {
                Comment: {
                  validators:{
                    notEmpty:{
                      message: 'Komentar wajib diisi'
                    }
                  }
                },
                NamaMatrix: {
                  validators:{
                    notEmpty:{
                      message: 'Nama Matriks wajib diisi'
                    }
                  }
                }
              }
            })
            .on('success.form.bv', function(e) {
              e.preventDefault();
              self.getConfirmation();
            });
        },
        existingDataValidation: function() {
          var self = this;
          var selectedMatrix = this.$('[name="NamaMatrix"]').val();
          var found = false;
          if (this.existingMatrix) {
            $.grep(this.existingMatrix, function(item) {
              if (item == selectedMatrix) {
                if (item != self.currentId) {
                  found = true;
                } 
              }
            });
          }
          if (found) {
            commonFunction.responseWarningCannotExecute("Matrix yang sudah dipilih tidak bisa dipilih lagi. Silakan pilih Matrix lain.");
            this.$('[type="submit"]').attr('disabled', true);
          } else {
            this.$('[type="submit"]').attr('disabled', false);
          }
          return found;
        },
        getConfirmation: function(){
          var data = this.$('[name="Comment"]').val();
          var isMatrixValid = this.existingDataValidation();
          if (!isMatrixValid) {
            var action = "ubah";
            var retVal = confirm("Apakah anda yakin ingin " + action + " Komentar : "+ data +" ?");
            if( retVal == true ){
               this.doSave();
            }
            else{
              this.$('[type="submit"]').attr('disabled', false);
            }
          }          
        },
        doSave: function() {
          var data = commonFunction.formDataToJson(this.$('form').serializeArray());
          data.MatrixId = this.$('[name="NamaMatrix"]').val();
          this.model.save(data);
        }
    });
});
