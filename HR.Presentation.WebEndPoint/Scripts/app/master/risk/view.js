define(function(require, exports, module) {
    'use strict';

    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');
    var Table = require('./table/table');
    var Collection = require('./collection');
    var Paging = require('paging');

    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content tbl bg-white',
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.keyword = "";
            this.table = new Table({
                collection: new Collection()
            });

            this.paging = new Paging({
                collection: this.table.collection
            });
            this.listenTo(eventAggregator, 'master/risk/mainrisk/add:fecth', function() {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'master/risk/mainrisk/edit:fecth', function() {
                self.fetchData();
            });
            this.listenTo(eventAggregator, 'master/risk/mainrisk/delete:fecth', function(model) {
                self.fetchData();
            });
            this.roles = JSON.parse(localStorage.getItem('roles'));
        },
        events: {
            'click [name="add"]': 'add',
            'keyup #keyword': 'applyFilter',
            'click [name="btn-filter"]': 'applyFilter'
        },
        afterRender: function() {
            this.$('[obo-table-mainrisk]').append(this.table.el);
            this.table.render();

            this.insertView('[obo-paging]', this.paging);
            this.paging.render();

            this.fetchData();
            this.setRoleAccess();
        },
        fetchData: function() {
            var param  = [];
            param.Search = this.keyword;
            this.table.collection.fetch({
                reset: true,
                data: param
            })
        },
        add: function() {
            var self = this;
            require(['./add/view'], function(View) {
                commonFunction.setDefaultModalDialogFunction(self, View);
            });
        },
        applyFilter: function() {
            var self = this;
            var param  = []
            var keyword = this.$('#keyword').val();
            this.keyword = keyword;
            var keyIndex = this.$('#keyIndex option:selected').val();
            param.Search = keyword;
            param.SearchBy = keyIndex;
            
            this.table.collection.fetch({
                reset: true,
                data: param
            })
        },
        setRoleAccess: function() {
            let self = this;
            if (this.roles) {
                $.grep(this.roles, function(item) {
                    if (item.Name.trim() == "Master") {
                        if (item.SubMenu) {
                            _.each(item.SubMenu, function(val) {
                                switch (val.Name) {
                                    case "Master-KategoriResiko":
                                        if (val.SubMenu) {
                                            _.each(val.SubMenu, function(item) {
                                                switch(item.Action) {
                                                    case "add":
                                                        self.$('[name="add"]').removeClass('hide');
                                                        break;
                                                }
                                            });
                                        }
                                        break;
                                }
                            });
                        }
                    }
                });
            }
        }
    });
});