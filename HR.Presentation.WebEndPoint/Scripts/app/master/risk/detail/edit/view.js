define(function(require, exports, module) {
    'use strict';
    var View = require('modaldialogdefault');
    var template = require('text!./template.html');
    var Model = require('./../model');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var eventAggregator = require('eventaggregator');
    require('bootstrap-validator');

    module.exports = View.extend({
        template: _.template(template),
        initialize : function(options){
          var self = this;
          var subRiskId = this.model.get('Id');
          this.mRiskId = options.model.get('MRiskId');
          this.masterCodeRisk = options.model.get('KodeRisk').charAt(0);
          this.subCodeRisk = options.model.get('KodeRisk').substring(2);

          this.model = new Model();
          this.model.set(this.model.idAttribute, subRiskId);
          this.listenToOnce(this.model, 'sync', function(model) {
              this.render();
              var data = model.toJSON();
              this.listenTo(this.model, 'sync', function() {
                commonFunction.responseSuccessUpdateAddDelete('Risiko Kejadian berhasi diubah.');
                self.$el.modal('hide');
                eventAggregator.trigger('master/risk/detail/edit:fecth');
              });
          }, this);

          this.once('afterRender', function() {
              this.model.fetch();
          });
        },
        afterRender : function(){
          this.setTemplate();
          this.renderValidation();
        },
        setTemplate: function() {
          this.$('[name="MasterRiskCode"]').val(this.masterCodeRisk);
          this.$('[name="SubRiskCode"]').val(this.subCodeRisk);
        },
        renderValidation: function() {
          var self = this;
          this.$('[ehs-form]').bootstrapValidator({
              fields: {
                RiskEvenClaim: {
                  validators:{
                    stringLength: {
                        message: 'RPeristiwa Risiko Menuju Klaim harus kurang dari 500 karakter',
                        max: 500
                    },
                    notEmpty:{
                      message: 'Peristiwa Risiko Menuju Klaim harus diisi'
                    },
                    regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'Peristiwa Risiko Menuju Klaim hanya karakter dan spasi'
                    }
                  }
                },
                DescriptionRiskEvenClaim: {
                  validators:{
                    stringLength: {
                        message: 'Keterangan harus kurang dari 500 karakter',
                        max: 500
                    },
                    notEmpty:{
                      message: 'Keterangan harus diisi'
                    },
                    regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'Keterangan hanya karakter dan spasi'
                    }
                  }
                },
                SugestionMigration: {
                  validators:{
                    stringLength: {
                        message: 'Peringatan yang disarankan harus kurang dari 500 karakter',
                        max: 500
                    },
                    notEmpty:{
                      message: 'Peringatan yang disarankan harus diisi'
                    },
                    regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'Peringatan yang disarankan hanya karakter dan spasi'
                    }
                  }
                },
                MRiskId: {
                  validators: {
                    notEmpty: {
                      message: 'Risiko ID harus diisi'
                    },
                    numeric: {
                      message: 'Risiko ID tidak valid. Format yang diizinkan 123.45 atau 12.34 atau 12'
                    }
                  }
                },
                KodeRisk: {
                  validators: {
                    notEmpty: {
                      message: 'Kode Risk harus diisi'
                    }
                  }
                }
              }
            })
            .on('success.form.bv', function(e) {
              e.preventDefault();
              self.getConfirmation();
            });
        },
        getConfirmation: function(){
          var data = this.$('[name="SubRiskCode"]').val();
          var action = "ubah";
          var retVal = confirm("Apakah anda yakin ingin " + action + " Risk Event : "+ data +" ?");
          if( retVal == true ){
             this.doSave();
          }
          else{
            this.$('[type="submit"]').attr('disabled', false);
          }
        },
        doSave: function() {
          var data = commonFunction.formDataToJson(this.$('form').serializeArray());
          var subRiskCode = this.$('[name="SubRiskCode"]').val();
          data.KodeRisk = this.masterCodeRisk + '-' +  subRiskCode;
          data.MRiskId = this.mRiskId;
          this.model.save(data);
        }
    });
});
