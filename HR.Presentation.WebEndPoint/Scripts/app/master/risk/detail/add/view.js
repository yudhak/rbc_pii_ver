define(function(require, exports, module) {
    'use strict';
    var View = require('modaldialogdefault');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var Model = require('./../table/model');
    var eventAggregator = require('eventaggregator');
    require('bootstrap-validator');
    require('jquerymask');

    module.exports = View.extend({
        template: _.template(template),
        initialize: function(options) {
          var self = this;
          this.model = new Model();
          this.model.urlRoot += `/${commonFunction.getLastSplitHash()}`;
          this.model.set('MRiskId', options.model.get('Id'));
          this.masterCodeRisk = options.model.get('KodeMRisk').charAt(0);

          this.listenTo(this.model, 'request', function() {});

          this.listenTo(this.model, 'sync error', function() {});

          this.listenTo(this.model, 'sync', function(model) {
            commonFunction.responseSuccessUpdateAddDelete('Risiko Kejadian berhasil dibuat.');
            self.$el.modal('hide');
            eventAggregator.trigger('master/risk/detail/add:fecth');
          });
        },
        afterRender: function() {
          this.setTemplate();
          this.renderValidation();
        },
        setTemplate: function() {
          this.$('[name="MasterRiskCode"]').val(this.masterCodeRisk);
        },
        renderValidation: function() {
          var self = this;
          this.$('form').bind("keypress", function (e) {
                if (e.keyCode == 13) {
                    return false;
                }
            });
          this.$('form').bootstrapValidator({
              fields: {
                RiskEvenClaim: {
                  validators:{
                    stringLength: {
                        message: 'Peristiwa Risiko Menuju Klaim harus kurang dari 500 karakter',
                        max: 500
                    },
                    notEmpty:{
                      message: 'Peristiwa Risiko Menuju Klaim harus diisi'
                    }
                  }
                },
                DescriptionRiskEvenClaim: {
                  validators:{
                    stringLength: {
                        message: 'Keterangan harus kurang dari 500 karakter',
                        max: 500
                    },
                    notEmpty:{
                      message: 'Keterangan harus diisi'
                    }
                  }
                },
                SugestionMigration: {
                  validators:{
                    stringLength: {
                        message: 'SPeringatan yang disarankan harus kurang dari 500 karakter',
                        max: 500
                    },
                    notEmpty:{
                      message: 'Peringatan yang disarankan harus diisi'
                    }
                  }
                },
                MRiskId: {
                  validators: {
                    notEmpty: {
                      message: 'Risiko ID harus diisi'
                    },
                    numeric: {
                      message: 'Risiko ID tidak valid. Format yang diizinkan 123.45 atau 12.34 atau 12'
                    }
                  }
                },
                KodeRisk: {
                  validators: {
                    notEmpty: {
                      message: 'Kode Risk harus diisi'
                    }
                  }
                }
              }
            })
            .on('success.form.bv', function(e) {
              e.preventDefault();
              self.getConfirmation();
            });
        },
        getConfirmation: function(){
          var data = this.$('[name="SubRiskCode"]').val();
          var action = "tambah";
          var retVal = confirm("Apakah anda yakin ingin " + action + " Risk Event : "+ data +" ?");
          if( retVal == true ){
             this.doSave();
          }
          else{
            this.$('[type="submit"]').attr('disabled', false);
          }
        },
        doSave: function() {
          var data = commonFunction.formDataToJson(this.$('form').serializeArray());
          var subRiskCode = this.$('[name="SubRiskCode"]').val();
          data.KodeRisk = this.masterCodeRisk + '-' +  subRiskCode;
          data.RiskRegistrasiId = this.model.get('MRiskId');
          this.model.save(data);
        }
    });
});
