define(function(require, exports, module) {
    'use strict';

    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'Id',
        urlRoot: 'api/RiskRegistrasi',
        defaults: function() {
            return {
              KodeMRisk : '',
              NamaCategoryRisk : '',
              Definisi : '',
              Maximum : '',
              Minimum : '',
              CreateBy : '',
              CreateDate : '',
              UpdateBy : '',
              UpdateDate : '',
              IsDelete : '',
              DeleteDate : ''
            }
        }
    });
});
