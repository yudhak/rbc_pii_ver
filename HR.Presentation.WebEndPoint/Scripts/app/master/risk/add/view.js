define(function(require, exports, module) {
    'use strict';
    var View = require('modaldialogdefault');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var Model = require('./../model');
    var eventAggregator = require('eventaggregator');
    require('bootstrap-validator');
    require('jquerymask');

    module.exports = View.extend({
        template: _.template(template),
        initialize: function() {
          var self = this;
          this.model = new Model();
          this.listenTo(this.model, 'request', function() {});

          this.listenTo(this.model, 'sync error', function() {});

          this.listenTo(this.model, 'sync', function(model) {
            commonFunction.responseSuccessUpdateAddDelete('Kategori Resiko berhasil dibuat.');
            self.$el.modal('hide');
            eventAggregator.trigger('master/risk/mainrisk/add:fecth');
          });
        },
        afterRender: function() {
          this.renderValidation();
        },
        renderValidation: function() {
          var self = this;
          this.$('[ehs-form]').bind("keypress", function (e) {
                if (e.keyCode == 13) {
                    return false;
                }
            });
          this.$('[ehs-form]').bootstrapValidator({
              fields: {
                KodeMRisk: {
                  validators:{
                    stringLength: {
                        message: 'Kode Kategori Risiko harus lebih dari 1 karakter',
                        max: 1
                    },
                    notEmpty:{
                      message: 'Kode Kategori Risiko harus diisi'
                    },
                    regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'Kode Kategori Risiko hanya karakter dan spasi'
                    }
                  }
                },
                NamaCategoryRisk: {
                  validators: {
                    notEmpty: {
                      message: 'Nama Kategori Risiko harus diisi'
                    },
                    stringLength: {
                        message: 'Nama Kategori Risiko harus kurang dari 50 karakter',
                        max: 50
                    }
                  }
                },
                Definisi: {
                  validators:{
                    stringLength: {
                        message: 'Definisi harus kurang dari 500 karkater',
                        max: 500
                    },
                    notEmpty:{
                      message: 'Definisi harus diisi'
                    }
                  }
                },
                Minimum: {
                  validators: {
                    notEmpty: {
                      message: 'Minimum harus diisi'
                    },
                    numeric: {
                      message: 'Minimum tidak valid. Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                },
                Maximum: {
                  validators: {
                    notEmpty: {
                      message: 'Maximum harus diisi'
                    },
                    numeric: {
                      message: 'Maximum tidak valid. Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                }
              }
            })
            .on('success.form.bv', function(e) {
              e.preventDefault();
              self.getConfirmation();
            });
        },
        doValidateMaxMin: function() {
          var status = false;
          var minVal = parseInt(this.$('[name="Minimum"]').val());
          var maxVal = parseInt(this.$('[name="Maximum"]').val());
          if((minVal != "" && maxVal != "") || (minVal == 0 || maxVal == 0)) {
            if(minVal > maxVal) {
              commonFunction.responseWarningCannotExecute("Minimum tidak boleh lebih besar dari Maximum.");
            } else if (maxVal < minVal) {
              commonFunction.responseWarningCannotExecute("Maxmium tidak boleh lebih kecil dari Minimum.");
            } else {
              status = true;
            }
          }
          return status;
        },
        getConfirmation: function(){
          var statusMaxMin = this.doValidateMaxMin();
          if(statusMaxMin) {
            var data = this.$('[name="KodeMRisk"]').val();
            var action = "tambah";
            var retVal = confirm("Apakah anda yakin ingin " + action + " Kode Risk : "+ data +" ?");
            if( retVal == true ){
              this.doSave();
            }
            else{
              this.$('[type="submit"]').attr('disabled', false);
            }
          } else {
            this.$('[type="submit"]').attr('disabled', false);
          }         
        },
        doSave: function() {
          var data = commonFunction.formDataToJson(this.$('form').serializeArray());
          this.model.save(data);
        }
    });
});
