define(function (require, exports, module) {
  'use strict';
  var View = require('modaldialogdefault');
  var template = require('text!./template.html');
  var Model = require('./../model');
  var commonFunction = require('commonfunction');
  var commonConfig = require('commonconfig');
  var eventAggregator = require('eventaggregator');
  require('bootstrap-validator');

  module.exports = View.extend({
    template: _.template(template),
    initialize: function (options) {
      var self = this;
      var subRiskId = this.model.get('Id');
      this.model = new Model();
      this.model.set(this.model.idAttribute, subRiskId);
      this.listenToOnce(this.model, 'sync', function (model) {
        this.render();
        var data = model.toJSON();
        this.listenTo(this.model, 'sync', function () {
          commonFunction.responseSuccessUpdateAddDelete('Definisi Paremeter berhasil diubah.');
          self.$el.modal('hide');
          eventAggregator.trigger('master/likelihood/detail/edit:fecth');
        });
      }, this);

      this.once('afterRender', function () {
        this.model.fetch();
      });
    },
    events: {
      'keyup [name="Upper"], [name="Lower"]': 'countingAverage'
    },
    afterRender: function () {
      //this.setTemplate();
      this.renderValidation();
    },
    setTemplate: function () {
      const self = this;
      let html = '<div>'
      html += '<table style="width:100%" class="table-risk-matrix">'
      html += '<tr class="td-risk-matrix">'
      html += '<th>Definisi Parameter</th>'
      html += '<th>Nilai Terendah</th>'
      html += '<th>Nilai Tertinggi</th>'
      html += '<th>Rata-rata</th>'
      html += '</tr>'
      if (this.likelihoodDetail.length > 0 || this.likelihoodDetail !== 'undefined') {
        for (let i = 0; i < this.likelihoodDetail.length; i++) {
          html += '<tr class="td-risk-matrix">'
          html += '<td>'
          html += '<input type="text" value="' + this.likelihoodDetail[i].DefinisiLikehood + '" class="form-control" name="DefinisiLikehood">'
          html += '</td>'
          html += '<td>'
          html += '<input type="text" value="' + this.likelihoodDetail[i].Lower + '" class="form-control" name="Lower">'
          html += '</td>'
          html += '<td>'
          html += '<input type="text" value="' + this.likelihoodDetail[i].Upper + '" class="form-control" name="Upper">'
          html += '</td>'
          html += '<td>'
          html += '<input type="text" value="' + this.likelihoodDetail[i].Average + '" class="form-control" name="Average">'
          html += '</td>'
          html += '</tr>'
        }
      }
      html += '</table>'
      html += '</div>'

      this.$('[kw-data-detail]').append(html);
    },
    countingAverage: function () {
      var self = this;
      var sum = 0;
      var lower = $('[name="Lower"]').val();
      var upper = $('[name="Upper"]').val();
      sum = +lower + +upper;
      $('[name="Average"]').val(sum / 2);
    },
    renderValidation: function () {
      var self = this;
      this.$('[ehs-form]').bootstrapValidator({
        fields: {
          DefinisiLikehood: {
            validators: {
              stringLength: {
                message: 'Definisi harus kurang dari  10 karakter',
                max: 10
              },
              notEmpty: {
                message: 'Definisi harus diisi'
              }
            }
          },
          Lower: {
            validators: {
              notEmpty: {
                message: 'Nilai Terendah harus diisi'
              },
              numeric: {
                message: 'Nilai Terendah format tidak valid. Format yang diizinkan 123.45 atau 12.34 atau 12',
                thousandsSeparator: '',
                decimalSeparator: '.'
              }
            }
          },
          Upper: {
            validators: {
              notEmpty: {
                message: 'Nilai Terbesar harus diisi'
              },
              numeric: {
                message: 'Nilai Terbesar format tidak valid. Format yang diizinkan 123.45 atau 12.34 atau 12',
                thousandsSeparator: '',
                decimalSeparator: '.'
              }
            }
          }
        }
      })
        .on('success.form.bv', function (e) {
          e.preventDefault();
          self.getConfirmation();
        });
    },
    doValidateMaxMin: function () {
      var status = false;
      var minVal = parseInt(this.$('[name="Lower"]').val());
      var maxVal = parseInt(this.$('[name="Upper"]').val());
      if ((minVal != "" && maxVal != "") || (minVal == 0 || maxVal == 0)) {
        if (minVal > maxVal) {
          commonFunction.responseWarningCannotExecute("Minimum tidak boleh lebih besar dari Maximum.");
        } else if (maxVal < minVal) {
          commonFunction.responseWarningCannotExecute("Maxmium tidak boleh lebih kecil dari Minimum.");
        } else {
          status = true;
        }
      }
      return status;
    },
    getConfirmation: function () {
      var statusMaxMin = this.doValidateMaxMin();
      if (statusMaxMin) {
        var data = this.$('[name="DefinisiLikehood"]').val();
        var action = "edit";
        var retVal = confirm("Are you sure want to " + action + " Definisi Likelihood : " + data + " ?");
        if (retVal == true) {
          this.doSave();
        }
        else {
          this.$('[type="submit"]').attr('disabled', false);
        }
      } else {
        this.$('[type="submit"]').attr('disabled', false);
      }
    },
    doSave: function () {
      var data = commonFunction.formDataToJson(this.$('form').serializeArray());
      data.Average = $('[name="Average"]').val();
      this.model.save(data);
    }
  });
});
