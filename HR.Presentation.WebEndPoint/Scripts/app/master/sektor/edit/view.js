define(function(require, exports, module) {
    'use strict';
    var View = require('modaldialogdefault');
    var template = require('text!./template.html');
    var Model = require('./../model');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var eventAggregator = require('eventaggregator');
    require('bootstrap-validator');
    require('jquerymask');

    module.exports = View.extend({
        template: _.template(template),
        initialize : function(){
          var self = this;
          var stageId = this.model.get('Id');
          this.model = new Model();
          this.model.set(this.model.idAttribute, stageId);
          this.listenToOnce(this.model, 'sync', function(model) {
              this.render();
              var data = model.toJSON();
              this.listenTo(this.model, 'sync', function() {
                commonFunction.responseSuccessUpdateAddDelete('Sektor berhasil diubah.');
                self.$el.modal('hide');
                eventAggregator.trigger('master/sektor/edit:fecth');
              });
          }, this);

          this.once('afterRender', function() {
              this.model.fetch();
          });
        },
        afterRender : function(){
          this.setTemplate();
          this.renderValidation();
        },
        setTemplate : function(){
          // this.$('[name="Relationship"]').val(this.model.get('Relationship'));
        },
        renderValidation: function() {
          var self = this;
          this.$('[ehs-form]').bootstrapValidator({
              fields: {
                NamaSektor: {
                  validators:{
                    stringLength: {
                        message: 'Nama Sektor harus kurang dari 50 karakter',
                        max: 50
                    },
                    notEmpty:{
                      message: 'Nama Sektor wajib diisi'
                    }
                  }
                },
                Minimum: {
                  validators: {
                    notEmpty: {
                      message: 'Minimum wajib diisi'
                    },
                    numeric: {
                      message: 'Minimum format tidak valid. Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                },
                Maximum: {
                  validators: {
                    notEmpty: {
                      message: 'Maksimum wajib diisi'
                    },
                    numeric: {
                      message: 'Maksimum format tidak valid. Format yang diizinkan 123.45 atau 12.34 atau 12',
                      thousandsSeparator: '',
                      decimalSeparator: '.'
                    }
                  }
                },
                Definisi: {
                  validators:{
                    stringLength: {
                        message: 'Deskripsi harus kurang dari 50 karakter',
                        max: 50
                    },
                    notEmpty:{
                      message: 'Deskripsi wajib diisi'
                    }
                  }
                }
              }
            })
            .on('success.form.bv', function(e) {
              e.preventDefault();
              self.getConfirmation();
            });
        },
        doValidateMaxMin: function() {
          var status = false;
          var minVal = parseInt(this.$('[name="Minimum"]').val());
          var maxVal = parseInt(this.$('[name="Maximum"]').val());
          if((minVal != "" && maxVal != "") || (minVal == 0 || maxVal == 0)) {
            if(minVal > maxVal) {
              commonFunction.responseWarningCannotExecute("Minimum tidak boleh lebih besar dari Maximum.");
            } else if (maxVal < minVal) {
              commonFunction.responseWarningCannotExecute("Maxmium tidak boleh lebih kecil dari Minimum.");
            } else {
              status = true;
            }
          }
          return status;
        },
        getConfirmation: function(){
          var statusMaxMin = this.doValidateMaxMin();
          if(statusMaxMin) {
            var data = this.$('[name="NamaSektor"]').val();
            var action = "ubah";
            var retVal = confirm("Apakah anda yakin ingin " + action + " Sektor : "+ data +" ?");
            if( retVal == true ){
              this.doSave();
            }
            else{
              this.$('[type="submit"]').attr('disabled', false);
            }
          } else {
            this.$('[type="submit"]').attr('disabled', false);
          }
        },
        doSave: function() {
          var data = commonFunction.formDataToJson(this.$('form').serializeArray());
          this.model.save(data);
        }
    });
});
