﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HR.Presentation.WebEndPoint.Startup))]
namespace HR.Presentation.WebEndPoint
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
