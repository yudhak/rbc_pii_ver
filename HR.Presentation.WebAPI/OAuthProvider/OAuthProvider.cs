﻿using HR.Application;
using HR.Application.DTO;
using HR.Application.Params;
using HR.Common;
using HR.Domain;
using HR.Infrastructure;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.DirectoryServices.AccountManagement;


namespace HR.Presentation.WebAPI
{
    public class OAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;
        private string _clientid { get; set; }
        private string _username { get; set; }
        private string _ip { get; set; }
        private readonly IMenuService _menuService;

        public OAuthProvider(string publicClientId, IMenuService menuService)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientId = publicClientId;
            _menuService = menuService;
        }

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated(); //
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            DatabaseContext dbContext = new DatabaseContext();

            string userName = context.UserName;
            string password = context.Password;

            if (string.IsNullOrWhiteSpace(userName) || string.IsNullOrWhiteSpace(password))
            {
                context.SetError("invalid_grant", "Invalid credentials");
                return;
            }

            User user = dbContext.Users.Where(x => x.UserName == context.UserName && x.Status == true).SingleOrDefault();

            var winUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);

            if (user != null)
            {
                UserDTO userDTO = UserDTO.From(user);
                string existingPassword = DataSecurity.Decrypt(user.Password);
                if(password != existingPassword)
                {
                    context.SetError("invalid_grant", "Provided username and password is incorrect");
                    return;
                }

                //UserRole userRole = dbContext.UserRoles.Where(x => x.UserId == user.Id).SingleOrDefault();

                var getRole = dbContext.Roles.Where(x => x.Id == user.RoleId).SingleOrDefault();

                if(getRole != null)
                {
                    identity.AddClaim(new Claim(ClaimTypes.Role, getRole.Name));
                    identity.AddClaim(new Claim("username", user.UserName));
                    identity.AddClaim(new Claim(ClaimTypes.Email, user.UserName));
                    identity.AddClaim(new Claim(ClaimTypes.UserData, user.Id.ToString()));
                    identity.AddClaim(new Claim("userId", user.Id.ToString()));
                    identity.AddClaim(new Claim("roleaccess", getRole.Id.ToString()));
                    var props = new AuthenticationProperties(new Dictionary<string, string>
                    {
                        {
                            "username", userName
                        },
                        {
                            "role", getRole.Name
                        },
                        {
                            "id", userDTO.Id.ToString()
                        },
                        {
                            "permission", getRole.Id.ToString()
                        }
                    });

                    if (userDTO.Role != null && userDTO.Role.Count > 0)
                    {
                        foreach (RoleDTO role in userDTO.Role)
                        {
                            if (role.Accesses != null && role.Accesses.Count > 0)
                            {
                                List<MenuDTO> menuList = role.Accesses.ToList();

                                foreach (MenuDTO menu in menuList)
                                {
                                    identity.AddClaim(new Claim(ClaimTypes.Webpage, menu.ControllerName + "$%" + menu.ActionName));
                                }
                            }
                        }
                    }

                    if (userDTO.RoleSingle.Accesses != null)
                    {
                        foreach (var item in userDTO.RoleSingle.Accesses)
                        {
                            identity.AddClaim(new Claim(ClaimTypes.Webpage, item.Name + ',' + item.ActionName));
                        }
                    }

                    var ticket = new AuthenticationTicket(identity, props);

                    #region LDAP Auth
                    //bool flag = false;
                    //try
                    //{
                    //    using (PrincipalContext principalContext = new PrincipalContext(ContextType.Domain))
                    //    {
                    //        flag = principalContext.ValidateCredentials(userName, password);
                    //    }
                    //}
                    //catch (PrincipalServerDownException)
                    //{
                    //    context.SetError("invalid_grant", "Provided username and password is incorrect");
                    //    return;
                    //}
                    #endregion LDAP Auth

                    context.Validated(ticket);
                    context.Request.Context.Authentication.SignIn(identity);
                }

                else
                {
                    context.SetError("invalid_grant", "Existing user not set any Role(s)");
                    return;
                }
            }
            else
            {
                context.SetError("invalid_grant", "Provided username and password is incorrect");
                return;
            }
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var originalClient = context.Ticket.Properties.Dictionary["as:client_id"];
            var currentClient = context.ClientId;

            if (originalClient != currentClient)
            {
                context.SetError("invalid_clientId", "Refresh token is issued to a different clientId.");
                return Task.FromResult<object>(null);
            }

            // Change auth ticket for refresh token requests
            var newIdentity = new ClaimsIdentity(context.Ticket.Identity);

            var newClaim = newIdentity.Claims.Where(c => c.Type == "newClaim").FirstOrDefault();
            if (newClaim != null)
            {
                newIdentity.RemoveClaim(newClaim);
            }
            newIdentity.AddClaim(new Claim("newClaim", "newValue"));

            var newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);
            context.Validated(newTicket);

            return Task.FromResult<object>(null);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            IList<RoleLite> roleList = new List<RoleLite>();
            var menuList = context.Identity.Claims.Where(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/webpage").ToList();
            if (menuList.Count > 0)
            {
                foreach (var item in menuList)
                {
                    RoleLite role = new RoleLite();
                    role.Name = item.Value.Split(',').First();
                    role.Action = item.Value.Split(',').Last();
                    roleList.Add(role);
                }
            }

            var getMenuAccess = GetMenu(Convert.ToInt32(context.Identity.Claims.Where(x => x.Type == "userId").Single().Value));
            var jsonMenu = new JavaScriptSerializer().Serialize(getMenuAccess);

            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
            context.AdditionalResponseParameters.Add("menuList", jsonMenu);

            return Task.FromResult<object>(null);
        }

        public Menu GetMenuById(int id)
        {
            DatabaseContext dbContext = new DatabaseContext();
            Menu menuDashboard = dbContext.Menus.Where(x=>x.Id == id).SingleOrDefault();
            var child1 = dbContext.Menus.Where(x => x.Parent.Id == menuDashboard.Sequence).ToList();
            foreach (var item in child1.ToList())
            {

                if (item.Sequence == item.ParentId)
                {
                    foreach (var item2 in item.Child.ToList())
                    {
                        item.Child.Remove(item2);
                    }
                }
                else
                {
                    var child2 = dbContext.Menus.Where(x => x.Parent.Id == item.Sequence).ToList();
                    foreach (var item2 in child2.ToList())
                    {
                        if (item2.Sequence == item2.ParentId)
                        {
                            foreach (var item3 in item2.Child.ToList())
                            {
                                item2.Child.Remove(item3);
                            }
                        }
                        else
                        {
                            var child3 = dbContext.Menus.Where(x => x.Parent.Id == item2.Sequence).ToList();
                            foreach (var item3 in child3.ToList())
                            {
                                foreach (var item4 in item3.Child.ToList())
                                {
                                    item3.Child.Remove(item4);
                                }
                            }
                            foreach (var item3 in item2.Child.ToList())
                            {
                                item2.Child.Remove(item3);
                            }
                            foreach (var item3 in child3.ToList())
                            {
                                item2.Child.Add(item3);
                            }
                        }
                    }
                    foreach (var item2 in item.Child.ToList())
                    {
                        item.Child.Remove(item2);
                    }
                    foreach (var item2 in child2.ToList())
                    {
                        item.Child.Add(item2);
                    }
                }
            }

            //Replace with true child
            foreach (var item in menuDashboard.Child.ToList())
            {
                menuDashboard.Child.Remove(item);
            }

            foreach (var item in child1.ToList())
            {
                menuDashboard.Child.Add(item);
            }
            return menuDashboard;
        }

        public List<MenuForSettingDTO> GetMenu(int roleID)
        {
            DatabaseContext dbContext = new DatabaseContext();
            var UserModel = dbContext.Users.Where(x => x.Id == roleID).SingleOrDefault();
            #region haris
            var AllRoleAccessFront = dbContext.RoleAccessFronts.Where(x => x.RoleId == UserModel.RoleId && x.IsDelete == false && x.IsActive == true).OrderBy(x=>x.MenuId);
            List<MenuForSettingDTO> AllMenu = new List<MenuForSettingDTO>();

            //Dashboard
            List<MenuForSettingDTO> menuDashboardList = new List<MenuForSettingDTO>();
            MenuForSettingDTO MenuDashBoardMerge = new MenuForSettingDTO();
            List<string> Dashb = new List<string>();
            foreach (var item in AllRoleAccessFront)
            {
                var AllRoleAccess = dbContext.RoleAccesses.Where(x => x.RoleId == roleID && x.FlagFromFront == item.Id);
                if (item.MenuId < 21)
                {
                    MenuForSettingDTO menuDashBoard = new MenuForSettingDTO();
                    var Dashboards = GetMenuById(1);
                    foreach (var name in Dashboards.Child.ToList())
                    {
                        Dashb.Add(name.Name);
                    }
                    menuDashBoard.Name = Dashboards.Name;
                    menuDashBoard.Action = Dashboards.ActionName;
                    if (item.MenuId == 1)
                    {
                        List<MenuForSettingDTO> MenuChild = new List<MenuForSettingDTO>();
                        foreach (var child in Dashboards.Child)
                        {
                            List<MenuForSettingDTO> MenuGrandChild = new List<MenuForSettingDTO>();
                            MenuForSettingDTO menuChildDashboard = new MenuForSettingDTO();
                            menuChildDashboard.Name = child.Name;
                            menuChildDashboard.Action = child.ActionName;
                            foreach (var grandChild in child.Child)
                            {
                                MenuForSettingDTO menuGrandChildDashboard = new MenuForSettingDTO();
                                menuGrandChildDashboard.Name = grandChild.Name;
                                menuGrandChildDashboard.Action = grandChild.ActionName;
                                menuGrandChildDashboard.SubMenu = null;
                                MenuGrandChild.Add(menuGrandChildDashboard);
                            }
                            menuChildDashboard.SubMenu = MenuGrandChild;
                            MenuChild.Add(menuChildDashboard);
                        }
                        menuDashBoard.SubMenu = MenuChild;
                        menuDashboardList.Add(menuDashBoard);
                    }
                    if (item.MenuId == 2 || item.MenuId == 5 || item.MenuId == 13)
                    {
                        Menu childish = new Menu();
                        if (item.MenuId == 2)
                        {
                            childish = Dashboards.Child[0];
                        }
                        if (item.MenuId == 5)
                        {
                            childish = Dashboards.Child[1];
                        }
                        if (item.MenuId == 13)
                        {
                            childish = Dashboards.Child[2];
                        }

                        List<MenuForSettingDTO> MenuChild = new List<MenuForSettingDTO>();

                        List<MenuForSettingDTO> MenuGrandChild = new List<MenuForSettingDTO>();
                        MenuForSettingDTO menuChildDashboard = new MenuForSettingDTO();
                        menuChildDashboard.Name = childish.Name;
                        menuChildDashboard.Action = childish.ActionName;
                        foreach (var grandChild in childish.Child)
                        {
                            MenuForSettingDTO menuGrandChildDashboard = new MenuForSettingDTO();
                            menuGrandChildDashboard.Name = grandChild.Name;
                            menuGrandChildDashboard.Action = grandChild.ActionName;
                            menuGrandChildDashboard.SubMenu = null;
                            MenuGrandChild.Add(menuGrandChildDashboard);
                        }
                        menuChildDashboard.SubMenu = MenuGrandChild;

                        MenuChild.Add(menuChildDashboard);
                        menuDashBoard.SubMenu = MenuChild;
                        menuDashboardList.Add(menuDashBoard);
                    }
                    if (item.MenuId > 2 && item.MenuId < 5 || item.MenuId > 5 && item.MenuId < 13 || item.MenuId > 13)
                    {
                        Menu childish = new Menu();
                        if (item.MenuId > 2 && item.MenuId < 5)
                        {
                            childish = Dashboards.Child[0];
                        }
                        if (item.MenuId > 5 && item.MenuId < 13)
                        {
                            childish = Dashboards.Child[1];
                        }
                        if (item.MenuId > 13)
                        {
                            childish = Dashboards.Child[2];
                        }

                        List<MenuForSettingDTO> MenuChild = new List<MenuForSettingDTO>();

                        List<MenuForSettingDTO> MenuGrandChild = new List<MenuForSettingDTO>();
                        MenuForSettingDTO menuChildDashboard = new MenuForSettingDTO();
                        menuChildDashboard.Name = childish.Name;
                        menuChildDashboard.Action = childish.ActionName;
                        foreach (var grandChild in childish.Child)
                        {
                            if (grandChild.Id == item.MenuId)
                            {
                                MenuForSettingDTO menuGrandChildDashboard = new MenuForSettingDTO();
                                menuGrandChildDashboard.Name = grandChild.Name;
                                menuGrandChildDashboard.Action = grandChild.ActionName;
                                menuGrandChildDashboard.SubMenu = null;
                                MenuGrandChild.Add(menuGrandChildDashboard);
                            }
                        }
                        menuChildDashboard.SubMenu = MenuGrandChild;

                        MenuChild.Add(menuChildDashboard);
                        menuDashBoard.SubMenu = MenuChild;
                        menuDashboardList.Add(menuDashBoard);
                    }
                }
            }
            if (menuDashboardList.Count() != 0)
            {
                if (menuDashboardList.Count == 1)
                {
                    MenuDashBoardMerge = menuDashboardList[0];
                }
                else
                {
                    MenuDashBoardMerge = menuDashboardList[0];
                    var a = 1;
                    foreach (var item in menuDashboardList.ToList())
                    {
                        if (a > 1)
                        {
                            foreach (var childB in item.SubMenu.ToList())
                            {
                                var Penanda = 0;
                                foreach (var child in MenuDashBoardMerge.SubMenu.ToList())
                                {
                                    if (childB.Name == child.Name)
                                    {
                                        Penanda = 1;
                                        List<MenuForSettingDTO> GrandchildList = new List<MenuForSettingDTO>();
                                        foreach (var grandchildB in childB.SubMenu.ToList())
                                        {
                                            GrandchildList.Add(grandchildB);
                                        }
                                        foreach (var grandchild in child.SubMenu.ToList())
                                        {
                                            GrandchildList.Add(grandchild);
                                        }
                                        child.SubMenu = GrandchildList;
                                    }
                                }
                                if (Penanda != 1)
                                {
                                    List<MenuForSettingDTO> childList = new List<MenuForSettingDTO>();
                                    childList.Add(childB);
                                    foreach (var chil in MenuDashBoardMerge.SubMenu.ToList())
                                    {
                                        childList.Add(chil);
                                    }
                                    MenuDashBoardMerge.SubMenu = childList;
                                }
                            }
                        }
                        a++;
                    }

                    List<MenuForSettingDTO> menuDashboardUrutan = new List<MenuForSettingDTO>();

                    foreach (var item in MenuDashBoardMerge.SubMenu.ToList())
                    {
                        if (item.Name == Dashb[0])
                        {
                            menuDashboardUrutan.Add(item);
                        }
                    }

                    foreach (var item in MenuDashBoardMerge.SubMenu.ToList())
                    {
                        if (item.Name == Dashb[1])
                        {
                            menuDashboardUrutan.Add(item);
                        }
                    }

                    foreach (var item in MenuDashBoardMerge.SubMenu.ToList())
                    {
                        if (item.Name == Dashb[2])
                        {
                            menuDashboardUrutan.Add(item);
                        }
                    }

                    MenuDashBoardMerge.SubMenu = menuDashboardUrutan;
                }

                AllMenu.Add(MenuDashBoardMerge);
            }

            //Master
            List<MenuForSettingDTO> listMenuMaster = new List<MenuForSettingDTO>();
            var Master = GetMenuById(21);
            foreach (var item in AllRoleAccessFront)
            {
                if (item.MenuId == 21)
                {
                    var Korelasis = GetMenuById(22);

                    if (item.Lihat == true)
                    {
                        MenuForSettingDTO childKorelasi = new MenuForSettingDTO();
                        childKorelasi.Name = Korelasis.Name;
                        childKorelasi.Action = Korelasis.ActionName;

                        List<MenuForSettingDTO> grandChildList = new List<MenuForSettingDTO>();
                        if (item.Lihat == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[0].Name;
                            grandChildResiko.Action = Korelasis.Child[0].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Tambah == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[1].Name;
                            grandChildResiko.Action = Korelasis.Child[1].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Ubah == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[2].Name;
                            grandChildResiko.Action = Korelasis.Child[2].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Hapus == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[3].Name;
                            grandChildResiko.Action = Korelasis.Child[3].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Detail == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[4].Name;
                            grandChildResiko.Action = Korelasis.Child[4].ActionName;
                            List<MenuForSettingDTO> grandChildList2 = new List<MenuForSettingDTO>();
                            if (item.Lihat == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis.Child[4].Child[0].Name;
                                grandChildKomentar.Action = Korelasis.Child[4].Child[0].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }
                            if (item.Tambah == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis.Child[4].Child[1].Name;
                                grandChildKomentar.Action = Korelasis.Child[4].Child[1].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }
                            if (item.Ubah == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis.Child[4].Child[2].Name;
                                grandChildKomentar.Action = Korelasis.Child[4].Child[2].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }
                            if (item.Hapus == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis.Child[4].Child[3].Name;
                                grandChildKomentar.Action = Korelasis.Child[4].Child[3].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }

                            grandChildResiko.SubMenu = grandChildList2;
                            grandChildList.Add(grandChildResiko);
                        }

                        childKorelasi.SubMenu = grandChildList;
                        listMenuMaster.Add(childKorelasi);
                    }

                    for (int i = 0; i < 2; i++)
                    {
                        var Korelasis1 = GetMenuById(32);
                        if (i == 1)
                        {
                            Korelasis1 = GetMenuById(37);
                        }
                        if (item.Lihat == true)
                        {
                            MenuForSettingDTO childKorelasi = new MenuForSettingDTO();
                            childKorelasi.Name = Korelasis1.Name;
                            childKorelasi.Action = Korelasis1.ActionName;

                            List<MenuForSettingDTO> grandChildList = new List<MenuForSettingDTO>();
                            if (item.Lihat == true)
                            {
                                MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                                grandChildResiko.Name = Korelasis1.Child[0].Name;
                                grandChildResiko.Action = Korelasis1.Child[0].ActionName;
                                grandChildResiko.SubMenu = null;
                                grandChildList.Add(grandChildResiko);
                            }
                            if (item.Tambah == true)
                            {
                                MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                                grandChildResiko.Name = Korelasis1.Child[1].Name;
                                grandChildResiko.Action = Korelasis1.Child[1].ActionName;
                                grandChildResiko.SubMenu = null;
                                grandChildList.Add(grandChildResiko);
                            }
                            if (item.Ubah == true)
                            {
                                MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                                grandChildResiko.Name = Korelasis1.Child[2].Name;
                                grandChildResiko.Action = Korelasis1.Child[2].ActionName;
                                grandChildResiko.SubMenu = null;
                                grandChildList.Add(grandChildResiko);
                            }
                            if (item.Hapus == true)
                            {
                                MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                                grandChildResiko.Name = Korelasis1.Child[3].Name;
                                grandChildResiko.Action = Korelasis1.Child[3].ActionName;
                                grandChildResiko.SubMenu = null;
                                grandChildList.Add(grandChildResiko);
                            }

                            childKorelasi.SubMenu = grandChildList;
                            listMenuMaster.Add(childKorelasi);
                        }
                    }

                    var Korelasis2 = GetMenuById(42);

                    if (item.Lihat == true)
                    {
                        MenuForSettingDTO childKorelasi = new MenuForSettingDTO();
                        childKorelasi.Name = Korelasis2.Name;
                        childKorelasi.Action = Korelasis2.ActionName;

                        List<MenuForSettingDTO> grandChildList = new List<MenuForSettingDTO>();
                        if (item.Lihat == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis2.Child[0].Name;
                            grandChildResiko.Action = Korelasis2.Child[0].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Tambah == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis2.Child[1].Name;
                            grandChildResiko.Action = Korelasis2.Child[1].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Ubah == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis2.Child[2].Name;
                            grandChildResiko.Action = Korelasis2.Child[2].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Hapus == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis2.Child[3].Name;
                            grandChildResiko.Action = Korelasis2.Child[3].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Detail == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis2.Child[5].Name;
                            grandChildResiko.Action = Korelasis2.Child[5].ActionName;
                            List<MenuForSettingDTO> grandChildList2 = new List<MenuForSettingDTO>();
                            if (item.Lihat == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis2.Child[6].Name;
                                grandChildKomentar.Action = Korelasis2.ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }
                            if (item.Ubah == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis2.Child[6].Name;
                                grandChildKomentar.Action = Korelasis2.Child[6].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }

                            grandChildResiko.SubMenu = grandChildList2;
                            grandChildList.Add(grandChildResiko);
                        }

                        childKorelasi.SubMenu = grandChildList;
                        listMenuMaster.Add(childKorelasi);
                    }

                    for (int i = 0; i < 4; i++)
                    {
                        var Korelasis1 = GetMenuById(50);
                        if (i == 1)
                        {
                            Korelasis1 = GetMenuById(55);
                        }
                        if (i == 2)
                        {
                            Korelasis1 = GetMenuById(60);
                        }
                        if (i == 3)
                        {
                            Korelasis1 = GetMenuById(65);
                        }
                        if (item.Lihat == true)
                        {
                            MenuForSettingDTO childKorelasi = new MenuForSettingDTO();
                            childKorelasi.Name = Korelasis1.Name;
                            childKorelasi.Action = Korelasis1.ActionName;

                            List<MenuForSettingDTO> grandChildList = new List<MenuForSettingDTO>();
                            if (item.Lihat == true)
                            {
                                MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                                grandChildResiko.Name = Korelasis1.Child[0].Name;
                                grandChildResiko.Action = Korelasis1.Child[0].ActionName;
                                grandChildResiko.SubMenu = null;
                                grandChildList.Add(grandChildResiko);
                            }
                            if (item.Tambah == true)
                            {
                                MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                                grandChildResiko.Name = Korelasis1.Child[1].Name;
                                grandChildResiko.Action = Korelasis1.Child[1].ActionName;
                                grandChildResiko.SubMenu = null;
                                grandChildList.Add(grandChildResiko);
                            }
                            if (item.Ubah == true)
                            {
                                MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                                grandChildResiko.Name = Korelasis1.Child[2].Name;
                                grandChildResiko.Action = Korelasis1.Child[2].ActionName;
                                grandChildResiko.SubMenu = null;
                                grandChildList.Add(grandChildResiko);
                            }
                            if (item.Hapus == true)
                            {
                                MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                                grandChildResiko.Name = Korelasis1.Child[3].Name;
                                grandChildResiko.Action = Korelasis1.Child[3].ActionName;
                                grandChildResiko.SubMenu = null;
                                grandChildList.Add(grandChildResiko);
                            }

                            childKorelasi.SubMenu = grandChildList;
                            listMenuMaster.Add(childKorelasi);
                        }
                    }

                    var Korelasis3 = GetMenuById(70);

                    if (item.Lihat == true)
                    {
                        MenuForSettingDTO childKorelasi = new MenuForSettingDTO();
                        childKorelasi.Name = Korelasis3.Name;
                        childKorelasi.Action = Korelasis3.ActionName;

                        List<MenuForSettingDTO> grandChildList = new List<MenuForSettingDTO>();
                        if (item.Lihat == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis3.Child[0].Name;
                            grandChildResiko.Action = Korelasis3.Child[0].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Detail == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis3.Child[1].Name;
                            grandChildResiko.Action = Korelasis3.Child[1].ActionName;
                            List<MenuForSettingDTO> grandChildList2 = new List<MenuForSettingDTO>();
                            if (item.Lihat == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis3.Child[1].Child[0].Name;
                                grandChildKomentar.Action = Korelasis3.Child[1].Child[0].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }
                            if (item.Tambah == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis3.Child[1].Child[1].Name;
                                grandChildKomentar.Action = Korelasis3.Child[1].Child[1].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }
                            if (item.Ubah == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis3.Child[1].Child[2].Name;
                                grandChildKomentar.Action = Korelasis3.Child[1].Child[2].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }
                            if (item.Hapus == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis3.Child[1].Child[3].Name;
                                grandChildKomentar.Action = Korelasis3.Child[1].Child[3].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }

                            grandChildResiko.SubMenu = grandChildList2;
                            grandChildList.Add(grandChildResiko);
                        }

                        childKorelasi.SubMenu = grandChildList;
                        listMenuMaster.Add(childKorelasi);
                    }

                    var Korelasis4 = GetMenuById(77);

                    if (item.Lihat == true)
                    {
                        MenuForSettingDTO childKorelasi = new MenuForSettingDTO();
                        childKorelasi.Name = Korelasis4.Name;
                        childKorelasi.Action = Korelasis4.ActionName;

                        List<MenuForSettingDTO> grandChildList = new List<MenuForSettingDTO>();
                        if (item.Lihat == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis4.Child[0].Name;
                            grandChildResiko.Action = Korelasis4.Child[0].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Tambah == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis4.Child[1].Name;
                            grandChildResiko.Action = Korelasis4.Child[1].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Ubah == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis4.Child[2].Name;
                            grandChildResiko.Action = Korelasis4.Child[2].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Hapus == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis4.Child[3].Name;
                            grandChildResiko.Action = Korelasis4.Child[3].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }

                        childKorelasi.SubMenu = grandChildList;
                        listMenuMaster.Add(childKorelasi);
                    }

                    var Korelasis5 = GetMenuById(82);

                    if (item.Lihat == true)
                    {
                        MenuForSettingDTO childKorelasi = new MenuForSettingDTO();
                        childKorelasi.Name = Korelasis5.Name;
                        childKorelasi.Action = Korelasis5.ActionName;

                        List<MenuForSettingDTO> grandChildList = new List<MenuForSettingDTO>();
                        if (item.Lihat == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis5.Child[0].Name;
                            grandChildResiko.Action = Korelasis5.Child[0].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Tambah == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis5.Child[1].Name;
                            grandChildResiko.Action = Korelasis5.Child[1].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Ubah == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis5.Child[2].Name;
                            grandChildResiko.Action = Korelasis5.Child[2].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }

                        childKorelasi.SubMenu = grandChildList;
                        listMenuMaster.Add(childKorelasi);
                    }
                }

                if (item.MenuId == 22)
                {
                    var Korelasis = GetMenuById(item.MenuId);

                    if (item.Lihat == true)
                    {
                        MenuForSettingDTO childKorelasi = new MenuForSettingDTO();
                        childKorelasi.Name = Korelasis.Name;
                        childKorelasi.Action = Korelasis.ActionName;

                        List<MenuForSettingDTO> grandChildList = new List<MenuForSettingDTO>();
                        if (item.Lihat == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[0].Name;
                            grandChildResiko.Action = Korelasis.Child[0].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Tambah == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[1].Name;
                            grandChildResiko.Action = Korelasis.Child[1].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Ubah == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[2].Name;
                            grandChildResiko.Action = Korelasis.Child[2].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Hapus == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[3].Name;
                            grandChildResiko.Action = Korelasis.Child[3].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Detail == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[4].Name;
                            grandChildResiko.Action = Korelasis.Child[4].ActionName;
                            List<MenuForSettingDTO> grandChildList2 = new List<MenuForSettingDTO>();
                            if (item.Lihat == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis.Child[4].Child[0].Name;
                                grandChildKomentar.Action = Korelasis.Child[4].Child[0].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }
                            if (item.Tambah == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis.Child[4].Child[1].Name;
                                grandChildKomentar.Action = Korelasis.Child[4].Child[1].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }
                            if (item.Ubah == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis.Child[4].Child[2].Name;
                                grandChildKomentar.Action = Korelasis.Child[4].Child[2].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }
                            if (item.Hapus == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis.Child[4].Child[3].Name;
                                grandChildKomentar.Action = Korelasis.Child[4].Child[3].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }

                            grandChildResiko.SubMenu = grandChildList2;
                            grandChildList.Add(grandChildResiko);
                        }

                        childKorelasi.SubMenu = grandChildList;
                        listMenuMaster.Add(childKorelasi);
                    }
                }

                if (item.MenuId == 32 || item.MenuId == 37 || item.MenuId == 50 || item.MenuId == 55 || item.MenuId == 60 || item.MenuId == 65 || item.MenuId == 77)
                {
                    var Korelasis = GetMenuById(item.MenuId);

                    if (item.Lihat == true)
                    {
                        MenuForSettingDTO childKorelasi = new MenuForSettingDTO();
                        childKorelasi.Name = Korelasis.Name;
                        childKorelasi.Action = Korelasis.ActionName;

                        List<MenuForSettingDTO> grandChildList = new List<MenuForSettingDTO>();
                        if (item.Lihat == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[0].Name;
                            grandChildResiko.Action = Korelasis.Child[0].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Tambah == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[1].Name;
                            grandChildResiko.Action = Korelasis.Child[1].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Ubah == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[2].Name;
                            grandChildResiko.Action = Korelasis.Child[2].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Hapus == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[3].Name;
                            grandChildResiko.Action = Korelasis.Child[3].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }

                        childKorelasi.SubMenu = grandChildList;
                        listMenuMaster.Add(childKorelasi);
                    }
                }

                if (item.MenuId == 42)
                {
                    var Korelasis = GetMenuById(item.MenuId);

                    if (item.Lihat == true)
                    {
                        MenuForSettingDTO childKorelasi = new MenuForSettingDTO();
                        childKorelasi.Name = Korelasis.Name;
                        childKorelasi.Action = Korelasis.ActionName;

                        List<MenuForSettingDTO> grandChildList = new List<MenuForSettingDTO>();
                        if (item.Lihat == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[0].Name;
                            grandChildResiko.Action = Korelasis.Child[0].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Tambah == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[1].Name;
                            grandChildResiko.Action = Korelasis.Child[1].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Ubah == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[2].Name;
                            grandChildResiko.Action = Korelasis.Child[2].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Hapus == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[3].Name;
                            grandChildResiko.Action = Korelasis.Child[3].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Detail == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[5].Name;
                            grandChildResiko.Action = Korelasis.Child[5].ActionName;
                            List<MenuForSettingDTO> grandChildList2 = new List<MenuForSettingDTO>();
                            if (item.Lihat == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis.Child[6].Name;
                                grandChildKomentar.Action = Korelasis.ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }
                            if (item.Ubah == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis.Child[6].Name;
                                grandChildKomentar.Action = Korelasis.Child[6].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }

                            grandChildResiko.SubMenu = grandChildList2;
                            grandChildList.Add(grandChildResiko);
                        }

                        childKorelasi.SubMenu = grandChildList;
                        listMenuMaster.Add(childKorelasi);
                    }
                }

                if (item.MenuId == 70)
                {
                    var Korelasis = GetMenuById(item.MenuId);

                    if (item.Lihat == true)
                    {
                        MenuForSettingDTO childKorelasi = new MenuForSettingDTO();
                        childKorelasi.Name = Korelasis.Name;
                        childKorelasi.Action = Korelasis.ActionName;

                        List<MenuForSettingDTO> grandChildList = new List<MenuForSettingDTO>();
                        if (item.Lihat == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[0].Name;
                            grandChildResiko.Action = Korelasis.Child[0].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Detail == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[1].Name;
                            grandChildResiko.Action = Korelasis.Child[1].ActionName;
                            List<MenuForSettingDTO> grandChildList2 = new List<MenuForSettingDTO>();
                            if (item.Lihat == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis.Child[1].Child[0].Name;
                                grandChildKomentar.Action = Korelasis.Child[1].Child[0].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }
                            if (item.Tambah == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis.Child[1].Child[1].Name;
                                grandChildKomentar.Action = Korelasis.Child[1].Child[1].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }
                            if (item.Ubah == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis.Child[1].Child[2].Name;
                                grandChildKomentar.Action = Korelasis.Child[1].Child[2].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }
                            if (item.Hapus == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis.Child[1].Child[3].Name;
                                grandChildKomentar.Action = Korelasis.Child[1].Child[3].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }

                            grandChildResiko.SubMenu = grandChildList2;
                            grandChildList.Add(grandChildResiko);
                        }

                        childKorelasi.SubMenu = grandChildList;
                        listMenuMaster.Add(childKorelasi);
                    }
                }

                if (item.MenuId == 82 )
                {
                    var Korelasis = GetMenuById(item.MenuId);

                    if (item.Lihat == true)
                    {
                        MenuForSettingDTO childKorelasi = new MenuForSettingDTO();
                        childKorelasi.Name = Korelasis.Name;
                        childKorelasi.Action = Korelasis.ActionName;

                        List<MenuForSettingDTO> grandChildList = new List<MenuForSettingDTO>();
                        if (item.Lihat == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[0].Name;
                            grandChildResiko.Action = Korelasis.Child[0].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Tambah == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[1].Name;
                            grandChildResiko.Action = Korelasis.Child[1].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Ubah == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[2].Name;
                            grandChildResiko.Action = Korelasis.Child[2].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }

                        childKorelasi.SubMenu = grandChildList;
                        listMenuMaster.Add(childKorelasi);
                    }
                }

            }
            if (listMenuMaster.Count() != 0)
            {
                MenuForSettingDTO MenuMasterMerge = new MenuForSettingDTO();
                MenuMasterMerge.Name = Master.Name;
                MenuMasterMerge.Action = Master.ActionName;
                MenuMasterMerge.SubMenu = listMenuMaster;
                AllMenu.Add(MenuMasterMerge);
            }

            //Scenario
            foreach (var item in AllRoleAccessFront)
            {
                if (item.MenuId == 86)
                {
                    MenuForSettingDTO menuScenario = new MenuForSettingDTO();
                    var Scenarios = GetMenuById(86);
                    menuScenario.Name = Scenarios.Name;
                    menuScenario.Action = Scenarios.ActionName;

                    List<MenuForSettingDTO> listScenarioChild = new List<MenuForSettingDTO>();

                    if (item.Lihat == true)
                    {
                        MenuForSettingDTO childScenario = new MenuForSettingDTO();
                        childScenario.Name = Scenarios.Child[0].Name;
                        childScenario.Action = Scenarios.Child[0].ActionName;
                        childScenario.SubMenu = null;
                        listScenarioChild.Add(childScenario);
                    }

                    if (item.Tambah == true)
                    {
                        MenuForSettingDTO childScenario = new MenuForSettingDTO();
                        childScenario.Name = Scenarios.Child[1].Name;
                        childScenario.Action = Scenarios.Child[1].ActionName;
                        childScenario.SubMenu = null;
                        listScenarioChild.Add(childScenario);
                    }

                    if (item.Ubah == true)
                    {
                        MenuForSettingDTO childScenario = new MenuForSettingDTO();
                        childScenario.Name = Scenarios.Child[2].Name;
                        childScenario.Action = Scenarios.Child[2].ActionName;
                        childScenario.SubMenu = null;
                        listScenarioChild.Add(childScenario);
                    }

                    if (item.Hapus == true)
                    {
                        MenuForSettingDTO childScenario = new MenuForSettingDTO();
                        childScenario.Name = Scenarios.Child[3].Name;
                        childScenario.Action = Scenarios.Child[3].ActionName;
                        childScenario.SubMenu = null;
                        listScenarioChild.Add(childScenario);
                    }

                    menuScenario.SubMenu = listScenarioChild;
                    AllMenu.Add(menuScenario);
                }
            }

            //Risiko Matrix
            foreach (var item in AllRoleAccessFront)
            {
                if (item.MenuId == 92)
                {
                    MenuForSettingDTO menuResikoMatrix = new MenuForSettingDTO();
                    var ResikoMatrix = GetMenuById(92);
                    menuResikoMatrix.Name = ResikoMatrix.Name;
                    menuResikoMatrix.Action = ResikoMatrix.ActionName;

                    List<MenuForSettingDTO> listResikoMatrixChild = new List<MenuForSettingDTO>();

                    if (item.Lihat == true)
                    {
                        MenuForSettingDTO childResiko = new MenuForSettingDTO();
                        childResiko.Name = ResikoMatrix.Child[0].Name;
                        childResiko.Action = ResikoMatrix.Child[0].ActionName;
                        childResiko.SubMenu = null;
                        listResikoMatrixChild.Add(childResiko);
                    }

                    if (item.Tambah == true)
                    {
                        MenuForSettingDTO childResiko = new MenuForSettingDTO();
                        childResiko.Name = ResikoMatrix.Child[1].Name;
                        childResiko.Action = ResikoMatrix.Child[1].ActionName;
                        childResiko.SubMenu = null;
                        listResikoMatrixChild.Add(childResiko);
                    }

                    if (item.Ubah == true)
                    {
                        MenuForSettingDTO childResiko = new MenuForSettingDTO();
                        childResiko.Name = ResikoMatrix.Child[2].Name;
                        childResiko.Action = ResikoMatrix.Child[2].ActionName;
                        childResiko.SubMenu = null;
                        listResikoMatrixChild.Add(childResiko);
                    }

                    if (item.Detail == true)
                    {
                        MenuForSettingDTO childResiko = new MenuForSettingDTO();
                        childResiko.Name = ResikoMatrix.Child[4].Name;
                        childResiko.Action = ResikoMatrix.Child[4].ActionName;

                        List<MenuForSettingDTO> grandChildList = new List<MenuForSettingDTO>();
                        if (item.Lihat == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = ResikoMatrix.Child[4].Child[0].Name;
                            grandChildResiko.Action = ResikoMatrix.Child[4].Child[0].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Ubah == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = ResikoMatrix.Child[4].Child[1].Name;
                            grandChildResiko.Action = ResikoMatrix.Child[4].Child[1].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }

                        childResiko.SubMenu = grandChildList;
                        listResikoMatrixChild.Add(childResiko);
                    }

                    menuResikoMatrix.SubMenu = listResikoMatrixChild;
                    AllMenu.Add(menuResikoMatrix);
                }
            }

            //Korelasi
            List<MenuForSettingDTO> listMenuKorelasi = new List<MenuForSettingDTO>();
            List<string> Korel = new List<string>();
            foreach (var item in AllRoleAccessFront)
            {
                if (item.MenuId == 100)
                {
                    MenuForSettingDTO menuKorelasi = new MenuForSettingDTO();
                    var Korelasis = GetMenuById(100);
                    menuKorelasi.Name = Korelasis.Name;
                    menuKorelasi.Action = Korelasis.ActionName;

                    List<MenuForSettingDTO> listKorelasiChild = new List<MenuForSettingDTO>();
                    for (int i = 0; i < 2; i++)
                    {
                        if (item.Lihat == true)
                        {
                            MenuForSettingDTO childKorelasi = new MenuForSettingDTO();
                            childKorelasi.Name = Korelasis.Child[i].Name;
                            childKorelasi.Action = Korelasis.Child[i].ActionName;

                            List<MenuForSettingDTO> grandChildList = new List<MenuForSettingDTO>();
                            if (item.Lihat == true)
                            {
                                MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                                grandChildResiko.Name = Korelasis.Child[i].Child[0].Name;
                                grandChildResiko.Action = Korelasis.Child[i].Child[0].ActionName;
                                grandChildResiko.SubMenu = null;
                                grandChildList.Add(grandChildResiko);
                            }
                            if (item.Ubah == true || item.Detail == true)
                            {
                                MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                                grandChildResiko.Name = Korelasis.Child[i].Child[1].Name;
                                grandChildResiko.Action = Korelasis.Child[i].Child[1].ActionName;
                                grandChildResiko.SubMenu = null;
                                grandChildList.Add(grandChildResiko);
                            }
                            if (item.Detail == true || item.Ubah == true)
                            {
                                MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                                grandChildResiko.Name = Korelasis.Child[i].Child[4].Name;
                                grandChildResiko.Action = Korelasis.Child[i].Child[4].ActionName;
                                List<MenuForSettingDTO> grandChildList2 = new List<MenuForSettingDTO>();
                                if (item.Lihat == true)
                                {
                                    MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                    grandChildKomentar.Name = Korelasis.Child[i].Child[4].Child[0].Name;
                                    grandChildKomentar.Action = Korelasis.Child[i].Child[4].Child[0].ActionName;
                                    grandChildKomentar.SubMenu = null;
                                    grandChildList2.Add(grandChildKomentar);
                                }
                                if (item.Ubah == true)
                                {
                                    MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                    grandChildKomentar.Name = Korelasis.Child[i].Child[4].Child[1].Name;
                                    grandChildKomentar.Action = Korelasis.Child[i].Child[4].Child[1].ActionName;
                                    grandChildKomentar.SubMenu = null;
                                    grandChildList2.Add(grandChildKomentar);
                                }

                                grandChildResiko.SubMenu = grandChildList2;
                                grandChildList.Add(grandChildResiko);
                            }

                            childKorelasi.SubMenu = grandChildList;
                            listKorelasiChild.Add(childKorelasi);
                        }
                    }

                    menuKorelasi.SubMenu = listKorelasiChild;
                    AllMenu.Add(menuKorelasi);
                }

                if (item.MenuId == 101)
                {
                    MenuForSettingDTO menuKorelasi = new MenuForSettingDTO();
                    var Korelasis = GetMenuById(100);
                    menuKorelasi.Name = Korelasis.Name;
                    menuKorelasi.Action = Korelasis.ActionName;
                    foreach (var name in Korelasis.Child.ToList())
                    {
                        Korel.Add(name.Name);
                    }

                    List<MenuForSettingDTO> listKorelasiChild = new List<MenuForSettingDTO>();
                    if (item.Lihat == true)
                    {
                        MenuForSettingDTO childKorelasi = new MenuForSettingDTO();
                        childKorelasi.Name = Korelasis.Child[0].Name;
                        childKorelasi.Action = Korelasis.Child[0].ActionName;

                        List<MenuForSettingDTO> grandChildList = new List<MenuForSettingDTO>();
                        if (item.Lihat == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[0].Child[0].Name;
                            grandChildResiko.Action = Korelasis.Child[0].Child[0].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Ubah == true || item.Detail == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[0].Child[1].Name;
                            grandChildResiko.Action = Korelasis.Child[0].Child[1].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Detail == true || item.Ubah == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[0].Child[4].Name;
                            grandChildResiko.Action = Korelasis.Child[0].Child[4].ActionName;
                            List<MenuForSettingDTO> grandChildList2 = new List<MenuForSettingDTO>();
                            if (item.Lihat == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis.Child[0].Child[4].Child[0].Name;
                                grandChildKomentar.Action = Korelasis.Child[0].Child[4].Child[0].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }
                            if (item.Ubah == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis.Child[0].Child[4].Child[1].Name;
                                grandChildKomentar.Action = Korelasis.Child[0].Child[4].Child[1].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }

                            grandChildResiko.SubMenu = grandChildList2;
                            grandChildList.Add(grandChildResiko);
                        }

                        childKorelasi.SubMenu = grandChildList;
                        listKorelasiChild.Add(childKorelasi);
                    }

                    menuKorelasi.SubMenu = listKorelasiChild;
                    listMenuKorelasi.Add(menuKorelasi);
                }

                if (item.MenuId == 109)
                {
                    MenuForSettingDTO menuKorelasi = new MenuForSettingDTO();
                    var Korelasis = GetMenuById(100);
                    menuKorelasi.Name = Korelasis.Name;
                    menuKorelasi.Action = Korelasis.ActionName;

                    List<MenuForSettingDTO> listKorelasiChild = new List<MenuForSettingDTO>();
                    if (item.Lihat == true)
                    {
                        MenuForSettingDTO childKorelasi = new MenuForSettingDTO();
                        childKorelasi.Name = Korelasis.Child[1].Name;
                        childKorelasi.Action = Korelasis.Child[1].ActionName;

                        List<MenuForSettingDTO> grandChildList = new List<MenuForSettingDTO>();
                        if (item.Lihat == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[1].Child[0].Name;
                            grandChildResiko.Action = Korelasis.Child[1].Child[0].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Ubah == true || item.Detail == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[1].Child[1].Name;
                            grandChildResiko.Action = Korelasis.Child[1].Child[1].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Detail == true || item.Ubah == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Korelasis.Child[1].Child[4].Name;
                            grandChildResiko.Action = Korelasis.Child[1].Child[4].ActionName;
                            List<MenuForSettingDTO> grandChildList2 = new List<MenuForSettingDTO>();
                            if (item.Lihat == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis.Child[1].Child[4].Child[0].Name;
                                grandChildKomentar.Action = Korelasis.Child[1].Child[4].Child[0].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }
                            if (item.Ubah == true)
                            {
                                MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                                grandChildKomentar.Name = Korelasis.Child[1].Child[4].Child[1].Name;
                                grandChildKomentar.Action = Korelasis.Child[1].Child[4].Child[1].ActionName;
                                grandChildKomentar.SubMenu = null;
                                grandChildList2.Add(grandChildKomentar);
                            }

                            grandChildResiko.SubMenu = grandChildList2;
                            grandChildList.Add(grandChildResiko);
                        }

                        childKorelasi.SubMenu = grandChildList;
                        listKorelasiChild.Add(childKorelasi);
                    }

                    menuKorelasi.SubMenu = listKorelasiChild;
                    listMenuKorelasi.Add(menuKorelasi);
                }
            }
            MenuForSettingDTO MenuKorelasiMerge = new MenuForSettingDTO();
            if (listMenuKorelasi.Count() != 0)
            {
                if (listMenuKorelasi.Count() == 1)
                {
                    MenuKorelasiMerge = listMenuKorelasi[0];
                }
                else
                {
                    MenuKorelasiMerge = listMenuKorelasi[0];
                    List<MenuForSettingDTO> subMerge = new List<MenuForSettingDTO>();
                    for (int i = 0; i < 2; i++)
                    {
                        foreach (var childMerge in listMenuKorelasi)
                        {
                            if (childMerge.SubMenu[0].Name == Korel[i])
                            {
                                subMerge.Add(childMerge.SubMenu[0]);
                            }
                        }
                    }
                    MenuKorelasiMerge.SubMenu = subMerge;
                }
                AllMenu.Add(MenuKorelasiMerge);
            }

            //Komentar
            foreach (var item in AllRoleAccessFront)
            {
                if (item.MenuId == 117)
                {
                    MenuForSettingDTO menuKomentar = new MenuForSettingDTO();
                    var Komentars = GetMenuById(117);
                    menuKomentar.Name = Komentars.Name;
                    menuKomentar.Action = Komentars.ActionName;

                    List<MenuForSettingDTO> listKomentarChild = new List<MenuForSettingDTO>();

                    if (item.Lihat == true)
                    {
                        MenuForSettingDTO childKomentar = new MenuForSettingDTO();
                        childKomentar.Name = Komentars.Child[0].Name;
                        childKomentar.Action = Komentars.Child[0].ActionName;
                        childKomentar.SubMenu = null;
                        listKomentarChild.Add(childKomentar);
                    }

                    if (item.Detail == true)
                    {
                        MenuForSettingDTO childKomentar = new MenuForSettingDTO();
                        childKomentar.Name = Komentars.Child[1].Name;
                        childKomentar.Action = Komentars.Child[1].ActionName;

                        List<MenuForSettingDTO> grandChildList = new List<MenuForSettingDTO>();
                        if (item.Lihat == true)
                        {
                            MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                            grandChildKomentar.Name = Komentars.Child[1].Child[0].Name;
                            grandChildKomentar.Action = Komentars.Child[1].Child[0].ActionName;
                            grandChildKomentar.SubMenu = null;
                            grandChildList.Add(grandChildKomentar);
                        }
                        if (item.Tambah == true)
                        {
                            MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                            grandChildKomentar.Name = Komentars.Child[1].Child[1].Name;
                            grandChildKomentar.Action = Komentars.Child[1].Child[1].ActionName;
                            grandChildKomentar.SubMenu = null;
                            grandChildList.Add(grandChildKomentar);
                        }
                        if (item.Ubah == true)
                        {
                            MenuForSettingDTO grandChildKomentar = new MenuForSettingDTO();
                            grandChildKomentar.Name = Komentars.Child[1].Child[2].Name;
                            grandChildKomentar.Action = Komentars.Child[1].Child[2].ActionName;
                            grandChildKomentar.SubMenu = null;
                            grandChildList.Add(grandChildKomentar);
                        }

                        childKomentar.SubMenu = grandChildList;
                        listKomentarChild.Add(childKomentar);
                    }

                    menuKomentar.SubMenu = listKomentarChild;
                    AllMenu.Add(menuKomentar);
                }
            }

            //Kalkulasi
            foreach (var item in AllRoleAccessFront)
            {
                if (item.MenuId == 123)
                {
                    MenuForSettingDTO menuKalkulasi = new MenuForSettingDTO();
                    var Kalkulasis = GetMenuById(123);
                    menuKalkulasi.Name = Kalkulasis.Name;
                    menuKalkulasi.Action = Kalkulasis.ActionName;

                    List<MenuForSettingDTO> listKalkulasiChild = new List<MenuForSettingDTO>();

                    if (item.Lihat == true)
                    {
                        MenuForSettingDTO childScenario = new MenuForSettingDTO();
                        childScenario.Name = Kalkulasis.Child[0].Name;
                        childScenario.Action = Kalkulasis.Child[0].ActionName;
                        childScenario.SubMenu = null;
                        listKalkulasiChild.Add(childScenario);
                    }

                    if (item.Ubah == true)
                    {
                        MenuForSettingDTO childScenario = new MenuForSettingDTO();
                        childScenario.Name = Kalkulasis.Child[1].Name;
                        childScenario.Action = Kalkulasis.Child[1].ActionName;
                        childScenario.SubMenu = null;
                        listKalkulasiChild.Add(childScenario);
                    }

                    menuKalkulasi.SubMenu = listKalkulasiChild;
                    AllMenu.Add(menuKalkulasi);
                }
            }

            //Kalkulasi Komparasi
            foreach (var item in AllRoleAccessFront)
            {
                if (item.MenuId == 149)
                {
                    MenuForSettingDTO menuScenario = new MenuForSettingDTO();
                    var Scenarios = GetMenuById(149);
                    menuScenario.Name = Scenarios.Name;
                    menuScenario.Action = Scenarios.ActionName;

                    List<MenuForSettingDTO> listScenarioChild = new List<MenuForSettingDTO>();

                    if (item.Lihat == true)
                    {
                        MenuForSettingDTO childScenario = new MenuForSettingDTO();
                        childScenario.Name = Scenarios.Child[0].Name;
                        childScenario.Action = Scenarios.Child[0].ActionName;
                        childScenario.SubMenu = null;
                        listScenarioChild.Add(childScenario);
                    }

                    if (item.Tambah == true)
                    {
                        MenuForSettingDTO childScenario = new MenuForSettingDTO();
                        childScenario.Name = Scenarios.Child[1].Name;
                        childScenario.Action = Scenarios.Child[1].ActionName;
                        childScenario.SubMenu = null;
                        listScenarioChild.Add(childScenario);
                    }

                    if (item.Ubah == true)
                    {
                        MenuForSettingDTO childScenario = new MenuForSettingDTO();
                        childScenario.Name = Scenarios.Child[2].Name;
                        childScenario.Action = Scenarios.Child[2].ActionName;
                        childScenario.SubMenu = null;
                        listScenarioChild.Add(childScenario);
                    }

                    if (item.Hapus == true)
                    {
                        MenuForSettingDTO childScenario = new MenuForSettingDTO();
                        childScenario.Name = Scenarios.Child[3].Name;
                        childScenario.Action = Scenarios.Child[3].ActionName;
                        childScenario.SubMenu = null;
                        listScenarioChild.Add(childScenario);
                    }

                    menuScenario.SubMenu = listScenarioChild;
                    AllMenu.Add(menuScenario);
                }
            }

            //Approval
            foreach (var item in AllRoleAccessFront)
            {
                if (item.MenuId == 126)
                {
                    MenuForSettingDTO menuApproval = new MenuForSettingDTO();
                    var Approvals = GetMenuById(126);
                    menuApproval.Name = Approvals.Name;
                    menuApproval.Action = Approvals.ActionName;

                    List<MenuForSettingDTO> listResikoMatrixChild = new List<MenuForSettingDTO>();

                    if (item.Lihat == true)
                    {
                        MenuForSettingDTO childResiko = new MenuForSettingDTO();
                        childResiko.Name = Approvals.Child[0].Name;
                        childResiko.Action = Approvals.Child[0].ActionName;
                        childResiko.SubMenu = null;
                        listResikoMatrixChild.Add(childResiko);
                    }

                    if (item.Tambah == true)
                    {
                        MenuForSettingDTO childResiko = new MenuForSettingDTO();
                        childResiko.Name = Approvals.Child[1].Name;
                        childResiko.Action = Approvals.Child[1].ActionName;
                        childResiko.SubMenu = null;
                        listResikoMatrixChild.Add(childResiko);
                    }

                    if (item.Ubah == true)
                    {
                        MenuForSettingDTO childResiko = new MenuForSettingDTO();
                        childResiko.Name = Approvals.Child[2].Name;
                        childResiko.Action = Approvals.Child[2].ActionName;
                        childResiko.SubMenu = null;
                        listResikoMatrixChild.Add(childResiko);
                    }

                    if (item.Hapus == true)
                    {
                        MenuForSettingDTO childResiko = new MenuForSettingDTO();
                        childResiko.Name = Approvals.Child[3].Name;
                        childResiko.Action = Approvals.Child[3].ActionName;
                        childResiko.SubMenu = null;
                        listResikoMatrixChild.Add(childResiko);
                    }

                    if (item.Detail == true)
                    {
                        MenuForSettingDTO childResiko = new MenuForSettingDTO();
                        childResiko.Name = Approvals.Child[4].Name;
                        childResiko.Action = Approvals.Child[4].ActionName;

                        List<MenuForSettingDTO> grandChildList = new List<MenuForSettingDTO>();
                        if (item.Lihat == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Approvals.Child[4].Child[0].Name;
                            grandChildResiko.Action = Approvals.Child[4].Child[0].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }
                        if (item.Ubah == true)
                        {
                            MenuForSettingDTO grandChildResiko = new MenuForSettingDTO();
                            grandChildResiko.Name = Approvals.Child[4].Child[1].Name;
                            grandChildResiko.Action = Approvals.Child[4].Child[1].ActionName;
                            grandChildResiko.SubMenu = null;
                            grandChildList.Add(grandChildResiko);
                        }

                        childResiko.SubMenu = grandChildList;
                        listResikoMatrixChild.Add(childResiko);
                    }

                    menuApproval.SubMenu = listResikoMatrixChild;
                    AllMenu.Add(menuApproval);
                }
            }

            //AuditLog
            foreach (var item in AllRoleAccessFront)
            {
                if (item.MenuId == 144)
                {
                    MenuForSettingDTO menuApproval = new MenuForSettingDTO();
                    var Approvals = GetMenuById(144);
                    menuApproval.Name = Approvals.Name;
                    menuApproval.Action = Approvals.ActionName;

                    List<MenuForSettingDTO> listResikoMatrixChild = new List<MenuForSettingDTO>();

                    if (item.Lihat == true)
                    {
                        MenuForSettingDTO childResiko = new MenuForSettingDTO();
                        childResiko.Name = Approvals.Child[0].Name;
                        childResiko.Action = Approvals.Child[0].ActionName;
                        childResiko.SubMenu = null;
                        listResikoMatrixChild.Add(childResiko);
                    }

                    if (item.Tambah == true)
                    {
                        MenuForSettingDTO childResiko = new MenuForSettingDTO();
                        childResiko.Name = Approvals.Child[1].Name;
                        childResiko.Action = Approvals.Child[1].ActionName;
                        childResiko.SubMenu = null;
                        listResikoMatrixChild.Add(childResiko);
                    }

                    if (item.Ubah == true)
                    {
                        MenuForSettingDTO childResiko = new MenuForSettingDTO();
                        childResiko.Name = Approvals.Child[2].Name;
                        childResiko.Action = Approvals.Child[2].ActionName;
                        childResiko.SubMenu = null;
                        listResikoMatrixChild.Add(childResiko);
                    }

                    if (item.Hapus == true)
                    {
                        MenuForSettingDTO childResiko = new MenuForSettingDTO();
                        childResiko.Name = Approvals.Child[3].Name;
                        childResiko.Action = Approvals.Child[3].ActionName;
                        childResiko.SubMenu = null;
                        listResikoMatrixChild.Add(childResiko);
                    }                    

                    menuApproval.SubMenu = listResikoMatrixChild;
                    AllMenu.Add(menuApproval);
                }
            }

            if (UserModel.RoleId == 1)
            {
                //Setting Menu
                MenuForSettingDTO menuScenario = new MenuForSettingDTO();
                    var Scenarios = GetMenuById(134);
                    menuScenario.Name = Scenarios.Name;
                    menuScenario.Action = Scenarios.ActionName;

                List<MenuForSettingDTO> listScenarioChild = new List<MenuForSettingDTO>();

                if (UserModel.RoleId == 1)
                {
                    MenuForSettingDTO childScenario = new MenuForSettingDTO();
                    childScenario.Name = Scenarios.Child[0].Name;
                    childScenario.Action = Scenarios.Child[0].ActionName;
                    childScenario.SubMenu = null;
                    listScenarioChild.Add(childScenario);
                }

                if (UserModel.RoleId == 1)
                {
                    MenuForSettingDTO childScenario = new MenuForSettingDTO();
                    childScenario.Name = Scenarios.Child[1].Name;
                    childScenario.Action = Scenarios.Child[1].ActionName;
                    childScenario.SubMenu = null;
                    listScenarioChild.Add(childScenario);
                }

                if (UserModel.RoleId == 1)
                {
                    MenuForSettingDTO childScenario = new MenuForSettingDTO();
                    childScenario.Name = Scenarios.Child[2].Name;
                    childScenario.Action = Scenarios.Child[2].ActionName;
                    childScenario.SubMenu = null;
                    listScenarioChild.Add(childScenario);
                }

                if (UserModel.RoleId == 1)
                {
                    MenuForSettingDTO childScenario = new MenuForSettingDTO();
                    childScenario.Name = Scenarios.Child[3].Name;
                    childScenario.Action = Scenarios.Child[3].ActionName;
                    childScenario.SubMenu = null;
                    listScenarioChild.Add(childScenario);
                }

                    menuScenario.SubMenu = listScenarioChild;
                    AllMenu.Add(menuScenario);
            }

            if (UserModel.RoleId == 1)
            {
                //Setting Menu
                MenuForSettingDTO menuScenario = new MenuForSettingDTO();
                    var Scenarios = GetMenuById(139);
                    menuScenario.Name = Scenarios.Name;
                    menuScenario.Action = Scenarios.ActionName;

                List<MenuForSettingDTO> listScenarioChild = new List<MenuForSettingDTO>();

                if (UserModel.RoleId == 1)
                {
                    MenuForSettingDTO childScenario = new MenuForSettingDTO();
                    childScenario.Name = Scenarios.Child[0].Name;
                    childScenario.Action = Scenarios.Child[0].ActionName;
                    childScenario.SubMenu = null;
                    listScenarioChild.Add(childScenario);
                }

                if (UserModel.RoleId == 1)
                {
                    MenuForSettingDTO childScenario = new MenuForSettingDTO();
                    childScenario.Name = Scenarios.Child[1].Name;
                    childScenario.Action = Scenarios.Child[1].ActionName;
                    childScenario.SubMenu = null;
                    listScenarioChild.Add(childScenario);
                }

                if (UserModel.RoleId == 1)
                {
                    MenuForSettingDTO childScenario = new MenuForSettingDTO();
                    childScenario.Name = Scenarios.Child[2].Name;
                    childScenario.Action = Scenarios.Child[2].ActionName;
                    childScenario.SubMenu = null;
                    listScenarioChild.Add(childScenario);
                }

                if (UserModel.RoleId == 1)
                {
                    MenuForSettingDTO childScenario = new MenuForSettingDTO();
                    childScenario.Name = Scenarios.Child[3].Name;
                    childScenario.Action = Scenarios.Child[3].ActionName;
                    childScenario.SubMenu = null;
                    listScenarioChild.Add(childScenario);
                }

                    menuScenario.SubMenu = listScenarioChild;
                    AllMenu.Add(menuScenario);
            }

            return AllMenu;
            #endregion haris
        }
    }
}