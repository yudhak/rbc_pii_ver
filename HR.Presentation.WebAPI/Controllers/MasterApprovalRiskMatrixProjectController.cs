﻿using HR.Application;
using HR.Application.DTO;
using HR.Application.Params;
using HR.Common;
using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace HR.Presentation.WebAPI.Controllers
{
    public class MasterApprovalRiskMatrixProjectController : BaseAPIController
    {
        private readonly IMasterApprovalRiskMatrixProjectService _masterApprovalRiskMatrixProjectService;
        private readonly IRiskMatrixProjectService _riskMatrixProjectService;
        private readonly IProjectService _projectService;
        private readonly IRiskRegistrasiService _riskRegistrasiService;
        private readonly IRiskMatrixProjectService _riskMatrixStageService;
        private readonly IStageTahunRiskMatrixService _stageTahunRiskMatrixService;


        public MasterApprovalRiskMatrixProjectController(IMasterApprovalRiskMatrixProjectService masterApprovalRiskMatrixProjectService, 
            IRiskMatrixProjectService riskMatrixProjectService, IProjectService projectService, IRiskRegistrasiService riskRegistrasiService,
            IRiskMatrixProjectService riskMatrixStageService,  IStageTahunRiskMatrixService stageTahunRiskMatrixService)
        {

            _masterApprovalRiskMatrixProjectService = masterApprovalRiskMatrixProjectService;
            _riskMatrixProjectService = riskMatrixProjectService;
            _projectService = projectService;
            _riskRegistrasiService = riskRegistrasiService;
            _riskMatrixStageService = riskMatrixStageService;
            _stageTahunRiskMatrixService = stageTahunRiskMatrixService;
        }

        //GET api/MasterApprovalRiskMatrixProject
        [HttpGet]
        public IHttpActionResult Get([FromUri] RiskMatrixProjectListParameter param)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    string keyword = string.Empty;
                    //string field = string.Empty;

                    param.Validate();
                    keyword = param.Search;
                    //field = param.SearchBy;

                    int field = 0;

                    if (param.SearchBy != "")
                    {
                        field = Convert.ToInt32(param.SearchBy);
                    }

                    int skip = 0;
                    if (param.PageNo > 0)
                    {
                        skip = (param.PageNo - 1) * param.PageSize;
                    }

                    int totalRows = _projectService.GetAll(keyword, field).Where(x => x.IsActive == true).Count();
                    var totalPage = totalRows / param.PageSize;
                    var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
                    if (totalPages < 0)
                        totalPages = 0;

                    var result = _projectService.GetAll(keyword, field).Where(x => x.IsActive == true)
                        .Skip(skip)
                        .Take(param.PageSize)
                        .ToList();

                    IList<ProjectDTO> colls = ProjectDTO.From(result);

                    PaginationDTO page = new PaginationDTO();
                    page.PageCount = totalPages;
                    page.PageNo = param.PageNo;
                    page.PageSize = param.PageSize;
                    page.results = colls;

                    return Ok(page);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }

        }

        //GET api/MasterApprovalRiskMatrixProject/menuId
        //[HttpGet]
        //public IHttpActionResult Get(int menuId,[FromUri] MasterApprovalRiskMatrixProjectListParam param)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            IList<RiskMatrixProject> riskMatrixProject = _riskMatrixProjectService.GetAllProjectActive().ToList();
        //            var riskRegistrasi = _riskRegistrasiService.GetAll().ToList();
        //            //IList<MasterApprovalRiskMatrixProject> masterApprovalRiskMatrixProject = _masterApprovalRiskMatrixProjectService.GetAll().ToList();
        //            if (param.IsPagination())
        //            {
        //                string keyword = string.Empty;
        //                string field = string.Empty;

        //                param.Validate();
        //                keyword = param.Search;
        //                field = param.SearchBy;

        //                int skip = 0;
        //                if (param.PageNo > 0)
        //                {
        //                    skip = (param.PageNo - 1) * param.PageSize;
        //                }

        //                int totalRows = _riskMatrixProjectService.GetAllProjectActive().Count();
        //                var totalPage = totalRows / param.PageSize;
        //                var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
        //                if (totalPages < 0)
        //                    totalPages = 0;

        //                var result = _riskMatrixProjectService.GetAllProjectActive()
        //                    .Skip(skip)
        //                    .Take(param.PageSize)
        //                    .ToList();

        //                IList<RiskMatrixProjectDTO> colls = RiskMatrixProjectDTO.From(result, riskRegistrasi);

        //                PaginationDTO page = new PaginationDTO();
        //                page.PageCount = totalPages;
        //                page.PageNo = param.PageNo;
        //                page.PageSize = param.PageSize;
        //                page.results = colls;

        //                return Ok(page);
        //            }
        //            else
        //            {
        //                IList<RiskMatrixProjectDTO> dto = RiskMatrixProjectDTO.From(riskMatrixProject, riskRegistrasi);
        //                return Ok(dto);
        //            }
        //        }
        //        else
        //        {
        //            string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
        //            return Content(HttpStatusCode.BadRequest, errorResult);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.InnerException == null)
        //            return Content(HttpStatusCode.InternalServerError, ex.Message);
        //        else
        //            return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
        //    }
        //}

        //GET api/MasterApprovalRiskMatrixProject/id
        //Get by Project Id
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
        
            try
            {

                IList<MasterApprovalRiskMatrixProject> result = _masterApprovalRiskMatrixProjectService.GetAllByProjectId(id).ToList();
                IList<MasterApprovalRiskMatrixProjectDTO> masterApprovalScenarioDTO = MasterApprovalRiskMatrixProjectDTO.From(result);
                return Ok(masterApprovalScenarioDTO);
                
               
                //return Ok(stageDTO);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //POST api/MasterApprovalRiskMatrixProject
        [HttpPost]
        public IHttpActionResult Add(MasterApprovalRiskMatrixProjectParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.CreateDate = DateHelper.GetDateTime();
                    param.CreateBy = Int32.Parse(base.UserId);
                    //param.CreateBy = 5;
                    int id = _masterApprovalRiskMatrixProjectService.Add(param);
                    return Ok(id);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //PUT api/MasterApprovalRiskMatrixProject/id
        [HttpPut]
        public IHttpActionResult Update(int id, [FromBody]MasterApprovalRiskMatrixProjectParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.UpdateDate = DateHelper.GetDateTime();
                     param.UpdateBy = Int32.Parse(base.UserId);
                    //param.UpdateBy = 5;
                    int result = _masterApprovalRiskMatrixProjectService.Update(id, param);
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        // DELETE api/MasterApprovalRiskMatrixProject/id
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId = Int32.Parse(base.UserId);
                    DateTime getDate = DateHelper.GetDateTime();
                    int result = _masterApprovalRiskMatrixProjectService.DeleteByProjectId(id, userId, getDate);
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }
    }
}
