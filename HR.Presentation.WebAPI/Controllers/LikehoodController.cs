﻿using HR.Application;
using HR.Application.DTO;
using HR.Application.Params;
using HR.Common;
using HR.Domain;
using HR.Presentation.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Routing;

namespace HR.Presentation.WebAPI.Controllers
{
    public class LikehoodController : BaseAPIController
    {
        private readonly ILikehoodService _likehoodService;

        public LikehoodController(ILikehoodService likehoodService)
        {
            _likehoodService = likehoodService;
        }

        //GET api/likehood
        [HttpGet]
        public IHttpActionResult Get([FromUri] LikehoodListParameter param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string keyword = string.Empty;
                    string field = string.Empty;

                    param.Validate();
                    keyword = param.Search;
                    field = param.SearchBy;

                    int skip = 0;
                    if (param.PageNo > 0)
                    {
                        skip = (param.PageNo - 1) * param.PageSize;
                    }

                    int totalRows = _likehoodService.GetAll(keyword).Count();
                    var totalPage = totalRows / param.PageSize;
                    var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
                    if (totalPages < 0)
                        totalPages = 0;

                    var result = _likehoodService.GetAll(keyword)
                        .Skip(skip)
                        .Take(param.PageSize)
                        .ToList();

                    IList<LikehoodDTO> colls = LikehoodDTO.From(result);

                    PaginationDTO page = new PaginationDTO();
                    page.PageCount = totalPages;
                    page.PageNo = param.PageNo;
                    page.PageSize = param.PageSize;
                    page.results = colls;

                    return Ok(page);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //GET api/likehood/1
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = _likehoodService.Get(id);
                LikehoodDTO likehoodDTO = LikehoodDTO.From(result);
                return Ok(likehoodDTO);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //POST api/likehood
        [HttpPost]
        public IHttpActionResult Add(LikehoodParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.CreateDate = DateHelper.GetDateTime();
                    param.CreateBy = Int32.Parse(base.UserId);

                    int id = _likehoodService.Add(param);
                    return Ok(id);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //PUT api/likehood/id
        [HttpPut]
        public IHttpActionResult Update(int id, [FromBody]LikehoodParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.UpdateDate = DateHelper.GetDateTime();
                    param.UpdateBy = Int32.Parse(base.UserId);

                    int result;
                    if(param.Status == true)
                    {
                        result = _likehoodService.Default(id, param);
                    }
                    else
                    {
                        result = _likehoodService.Update(id, param);
                    }
                    
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //DELETE api/likehood/id
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId = Int32.Parse(base.UserId);
                    DateTime getDate = DateHelper.GetDateTime();
                    int result = _likehoodService.Delete(id, userId, getDate);
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }
    }
}
