﻿using HR.Application;
using HR.Application.DTO;
using HR.Application.Params;
using HR.Common;
using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace HR.Presentation.WebAPI.Controllers
{
    public class MasterApprovalCorrelatedProjectController : BaseAPIController
    {
        private readonly IMasterApprovalCorrelatedProjectService _masterApprovalCorrelatedProjectService;
        private readonly ICorrelatedProjectService _correlatedProjectService;
        private readonly ICorrelationRiskAntarSektorService _correlationRiskAntarSektorService;
        private readonly ICorrelationMatrixService _correlationMatrixService;
        private readonly IScenarioService _scenarioService;
        private readonly IProjectService _projectService;

        public MasterApprovalCorrelatedProjectController(IMasterApprovalCorrelatedProjectService masterApprovalCorrelatedProjectService
            , ICorrelatedProjectService correlatedProjectService, ICorrelationRiskAntarSektorService correlationRiskAntarSektorService
            , ICorrelationMatrixService correlationMatrixService, IScenarioService scenarioService, IProjectService projectService)
        {

            _masterApprovalCorrelatedProjectService = masterApprovalCorrelatedProjectService;
            _correlatedProjectService = correlatedProjectService;
            _correlationRiskAntarSektorService = correlationRiskAntarSektorService;
            _correlationMatrixService = correlationMatrixService;
            _scenarioService = scenarioService;
            _projectService = projectService;
        }

        //GET api/MasterApprovalCorrelatedProject
        [HttpGet]
        public IHttpActionResult Get([FromUri] CorrelatedProjectListParameter param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string keyword = string.Empty;
                    int field = 0;

                    param.Validate();
                    keyword = param.Search;
                    if (param.SearchBy != "")
                    {
                        field = Convert.ToInt32(param.SearchBy);
                    }

                    int skip = 0;
                    if (param.PageNo > 0)
                    {
                        skip = (param.PageNo - 1) * param.PageSize;
                    }

                    int totalRows = _projectService.GetAll(keyword, field).Where(x => x.IsActive == true).Count();
                    var totalPage = totalRows / param.PageSize;
                    var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
                    if (totalPages < 0)
                        totalPages = 0;

                    var result = _projectService.GetAll(keyword, field).Where(x => x.IsActive == true)
                        .Skip(skip)
                        .Take(param.PageSize)
                        .ToList();

                    IList<ProjectDTO> colls = ProjectDTO.From(result);

                    PaginationDTO page = new PaginationDTO();
                    page.PageCount = totalPages;
                    page.PageNo = param.PageNo;
                    page.PageSize = param.PageSize;
                    page.results = colls;

                    return Ok(page);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }

        }

        //GET api/MasterApprovalCorrelatedProject/id
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                IList<MasterApprovalCorrelatedProject> result = _masterApprovalCorrelatedProjectService.GetAllByProjectId(id).ToList();
                IList<MasterApprovalCorrelatedProjectDTO> masterApprovalScenarioDTO = MasterApprovalCorrelatedProjectDTO.From(result);
                return Ok(masterApprovalScenarioDTO);

                //return Ok(stageDTO);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //POST api/MasterApprovalCorrelatedProject
        [HttpPost]
        public IHttpActionResult Add(MasterApprovalCorrelatedProjectParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.CreateDate = DateHelper.GetDateTime();
                    param.CreateBy = Int32.Parse(base.UserId);
                    //param.CreateBy = 5;
                    int id = _masterApprovalCorrelatedProjectService.Add(param);
                    return Ok(id);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //PUT api/MasterApprovalCorrelatedProject/id
        [HttpPut]
        public IHttpActionResult Update(int id, [FromBody]MasterApprovalCorrelatedProjectParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.UpdateDate = DateHelper.GetDateTime();
                    param.UpdateBy = Int32.Parse(base.UserId);
                    //param.UpdateBy = 5;
                    int result = _masterApprovalCorrelatedProjectService.Update(id, param);
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        // DELETE api/MasterApprovalCorrelatedProject/id
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId = Int32.Parse(base.UserId);
                    DateTime getDate = DateHelper.GetDateTime();
                    int result = _masterApprovalCorrelatedProjectService.DeleteByProjectId(id, userId, getDate);
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }
    }
}
