﻿using HR.Application;
using HR.Application.DTO;
using HR.Application.Params;
using HR.Common;
using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace HR.Presentation.WebAPI.Controllers
{
    public class UserController : BaseAPIController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        //GET api/user
        [HttpGet]
        public IHttpActionResult Get([FromUri] UserListParameter param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string keyword = string.Empty;
                    int field = 0;

                    param.Validate();
                    keyword = param.Search;
                    if (param.SearchBy != "")
                    {
                        field = Convert.ToInt32(param.SearchBy);
                    }
                    IList<User> users = _userService.GetAll(keyword, field).ToList();
                    if (param.IsPagination())
                    {
                        int skip = 0;
                        if (param.PageNo > 0)
                        {
                            skip = (param.PageNo - 1) * param.PageSize;
                        }

                        int totalRows = _userService.GetAll(keyword, field).Count();
                        var totalPage = totalRows / param.PageSize;
                        var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
                        if (totalPages < 0)
                            totalPages = 0;

                        var result = users.Skip(skip).Take(param.PageSize).ToList();

                        IList<UserDTO> colls = UserDTO.From(result);

                        PaginationDTO page = new PaginationDTO();
                        page.PageCount = totalPages;
                        page.PageNo = param.PageNo;
                        page.PageSize = param.PageSize;
                        page.results = colls;

                        return Ok(page);
                    }
                    else if (param.userFilter == true)
                    {
                        IList<User> usersFilter = _userService.GetAll(keyword).Where(x => x.RoleId == 5).ToList();
                        IList<UserDTO> dto = UserDTO.From(usersFilter);
                        return Ok(dto);
                    }
                    else if (param.userFilterRole > 0)
                    {
                        IList<User> usersFilter = _userService.GetAll(keyword).Where(x => x.RoleId == param.userFilterRole).ToList();
                        IList<UserDTO> dto = UserDTO.From(usersFilter);
                        return Ok(dto);
                    }
                    else
                    {
                        IList<UserDTO> dto = UserDTO.From(users);
                        return Ok(dto);
                    }

                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //GET api/user/1
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = _userService.Get(id);
                UserDTO userDTO = UserDTO.From(result);
                return Ok(userDTO);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //POST api/user
        [HttpPost]
        public IHttpActionResult Add(UserParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.CreateDate = DateHelper.GetDateTime();
                    //param.CreateBy = Int32.Parse(base.UserId);

                    int id = _userService.Add(param);
                    return Ok(id);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //PUT api/user/id
        [HttpPut]
        public IHttpActionResult Update(int id, [FromBody]UserParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.UpdateDate = DateHelper.GetDateTime();
                    //param.UpdateBy = Int32.Parse(base.UserId);

                    int result = _userService.Update(id, param);
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }


        //DELETE api/user/id
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId = Int32.Parse(base.UserId);
                    int deleteBy = Int32.Parse(base.UserId);
                    DateTime deleteDate = DateHelper.GetDateTime();
                    _userService.Delete(id, deleteBy, deleteDate);
                    return Ok(0);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //[HttpGet]
        //public IHttpActionResult GetAllFilter(int id, string sort)
        //{
        //    try
        //    {
        //        IList<User> users = _userService.GetAllFilter().ToList();

        //        IList<UserDTO> dto = UserDTO.From(users);
        //        return Ok(dto);

        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.InnerException == null)
        //            return Content(HttpStatusCode.InternalServerError, ex.Message);
        //        else
        //            return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
        //    }
        //}
    }
}
