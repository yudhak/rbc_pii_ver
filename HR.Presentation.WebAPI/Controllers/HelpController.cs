﻿using System.IO;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.Threading;
using System.Web;
using System.Net.Http.Headers;

namespace HR.Presentation.WebAPI.Controllers
{
    public class HelpController : BaseAPIController
    {
        public HelpController() { }

        public IHttpActionResult GetHelpDocument()
        {
            var path = "~\\Temp\\Helps\\";
            var name = "Buku_Panduan_RBC_PII_Rev_3.pdf";

            try
            {
                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(path + name);
                if (urlFile != null)
                {
                    var dataByte = File.ReadAllBytes(urlFile);
                    var dataStream = new MemoryStream(dataByte);
                    return Ok(urlFile);
                }
                else
                {
                    throw new ApplicationException(string.Format("File tidak ditemukan."));
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        public class FileResult : IHttpActionResult
        {
            private readonly string filePath;
            private readonly string contentType;

            public FileResult(string filePath, string contentType = null)
            {
                this.filePath = filePath;
                this.contentType = contentType;
            }


            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                return Task.Run(() =>
                {
                    var contentType = this.contentType ?? MimeMapping.GetMimeMapping(Path.GetExtension(filePath));
                    var fileName = Path.GetFileName(filePath);
                    HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                    var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                    response.Content = new StreamContent(stream);
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = fileName
                    };
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                    return response;
                }, cancellationToken);
            }
        }
    }
}


