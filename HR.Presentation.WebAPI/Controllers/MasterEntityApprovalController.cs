﻿using HR.Application;
using HR.Application.DTO;
using HR.Application.Params;
using HR.Common;
using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace HR.Presentation.WebAPI.Controllers
{
    public class MasterEntityApprovalController : BaseAPIController
    {
        //private readonly IMasterApprovalScenarioService _masterApprovalScenarioService;
        //private readonly IMasterApprovalScenarioRepository _masterApprovalScenarioRepository;

        //private readonly IMasterApprovalRiskMatrixProjectRepository _masterApprovalRiskMatrixProjectRepository;

        //private readonly IMasterApprovalCorrelatedProjectRepository _masterApprovalCorrelatedProjectRepository;
        //private readonly IMasterApprovalCorrelatedSektorRepository _masterApprovalCorrelatedSektorRepository;

        private readonly IMasterMenuApprovalService _masterMenuApprovalService;

        private readonly IStatusService _statusService;

        public MasterEntityApprovalController(IStatusService statusService, IMasterMenuApprovalService masterMenuApprovalService)
        {

            //_masterApprovalScenarioService = masterApprovalScenarioService;
            //_masterApprovalScenarioRepository = masterApprovalScenarioRepository;
            //_masterApprovalRiskMatrixProjectRepository = masterApprovalRiskMatrixProjectRepository;
            //_masterApprovalCorrelatedSektorRepository = masterApprovalCorrelatedSektorRepository;
            //_masterApprovalCorrelatedProjectRepository = masterApprovalCorrelatedProjectRepository;
            //_masterMenuApprovalRepository = masterMenuApprovalRepository;
            _statusService = statusService;
            _masterMenuApprovalService = masterMenuApprovalService;
        }

        //GET api/MasterEntityApproval
        [HttpGet]
        public IHttpActionResult Get([FromUri] StatusListParameter param)
        {

            //try
            //{
            //    if (ModelState.IsValid)
            //    {
            //        IList<Status> status = _statusService.GetAll().ToList();
            //        if (param.IsPagination())
            //        {
            //            string keyword = string.Empty;
            //            string field = string.Empty;

            //            param.Validate();
            //            keyword = param.Search;
            //            field = param.SearchBy;

            //            int skip = 0;
            //            if (param.PageNo > 0)
            //            {
            //                skip = (param.PageNo - 1) * param.PageSize;
            //            }

            //            int totalRows = _statusService.GetAll().Count();
            //            var totalPage = totalRows / param.PageSize;
            //            var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
            //            if (totalPages < 0)
            //                totalPages = 0;

            //            var result = _statusService.GetAll(keyword)
            //                .Skip(skip)
            //                .Take(param.PageSize)
            //                .ToList();

            //            IList<StatusDTO> colls = StatusDTO.From(result);

            //            PaginationDTO page = new PaginationDTO();
            //            page.PageCount = totalPages;
            //            page.PageNo = param.PageNo;
            //            page.PageSize = param.PageSize;
            //            page.results = colls;

            //            return Ok(page);
            //        }
            //        else
            //        {
            //            IList<StatusDTO> dto = StatusDTO.From(status);
            //            return Ok(dto);
            //        }
            //    }
            //    else
            //    {
            //        string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
            //        return Content(HttpStatusCode.BadRequest, errorResult);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    if (ex.InnerException == null)
            //        return Content(HttpStatusCode.InternalServerError, ex.Message);
            //    else
            //        return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            //}

            try
            {
                if (ModelState.IsValid)
                {
                    IList<MasterMenuApproval> status = _masterMenuApprovalService.GetAll().ToList();
                    if (param.IsPagination())
                    {
                        string keyword = string.Empty;
                        string field = string.Empty;

                        param.Validate();
                        keyword = param.Search;
                        field = param.SearchBy;

                        int skip = 0;
                        if (param.PageNo > 0)
                        {
                            skip = (param.PageNo - 1) * param.PageSize;
                        }

                        int totalRows = _masterMenuApprovalService.GetAll().Count();
                        var totalPage = totalRows / param.PageSize;
                        var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
                        if (totalPages < 0)
                            totalPages = 0;

                        var result = _masterMenuApprovalService.GetAll(keyword)
                            .Skip(skip)
                            .Take(param.PageSize)
                            .ToList();

                        IList<MasterMenuApprovalDTO> colls = MasterMenuApprovalDTO.From(result);

                        PaginationDTO page = new PaginationDTO();
                        page.PageCount = totalPages;
                        page.PageNo = param.PageNo;
                        page.PageSize = param.PageSize;
                        page.results = colls;

                        return Ok(page);
                    }
                    else
                    {
                        IList<MasterMenuApprovalDTO> dto = MasterMenuApprovalDTO.From(status);
                        return Ok(dto);
                    }
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

    }
}
