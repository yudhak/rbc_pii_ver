﻿using HR.Application;
using HR.Application.DTO;
using HR.Application.Params;
using HR.Common;
using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace HR.Presentation.WebAPI.Controllers
{
    public class StatusController : BaseAPIController
    {
        private readonly IStatusService _statusService;

        public StatusController(IStatusService statusService)
        {

            _statusService = statusService;
        }

        //GET api/status
        [HttpGet]
        public IHttpActionResult Get([FromUri] StatusListParameter param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IList<Status> status = _statusService.GetAll().ToList();
                    if (param.IsPagination())
                    {
                        string keyword = string.Empty;
                        string field = string.Empty;

                        param.Validate();
                        keyword = param.Search;
                        field = param.SearchBy;

                        int skip = 0;
                        if (param.PageNo > 0)
                        {
                            skip = (param.PageNo - 1) * param.PageSize;
                        }

                        int totalRows = _statusService.GetAll().Count();
                        var totalPage = totalRows / param.PageSize;
                        var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
                        if (totalPages < 0)
                            totalPages = 0;

                        var result = _statusService.GetAll(keyword)
                            .Skip(skip)
                            .Take(param.PageSize)
                            .ToList();

                        IList<StatusDTO> colls = StatusDTO.From(result);

                        PaginationDTO page = new PaginationDTO();
                        page.PageCount = totalPages;
                        page.PageNo = param.PageNo;
                        page.PageSize = param.PageSize;
                        page.results = colls;

                        return Ok(page);
                    }
                    else
                    {
                        IList<StatusDTO> dto = StatusDTO.From(status);
                        return Ok(dto);
                    }
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //GET api/status/id
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = _statusService.Get(id);
                StatusDTO statusDTO = StatusDTO.From(result);
                return Ok(statusDTO);
                
                //return Ok(stageDTO);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //POST api/status
        //[HttpPost]
        //public IHttpActionResult Add(StatusParam param)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        
        //            int id = _statusService.Add(param);
        //            return Ok(1);
        //        }
        //        else
        //        {
        //            string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
        //            return Content(HttpStatusCode.BadRequest, errorResult);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.InnerException == null)
        //            return Content(HttpStatusCode.InternalServerError, ex.Message);
        //        else
        //            return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
        //    }
        //}

        //PUT api/status/id
        //[HttpPut]
        //public IHttpActionResult Update(int id, [FromBody]StatusParam param)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {

        //            param.UpdateDate = DateHelper.GetDateTime();
        //            param.UpdateBy = Int32.Parse(base.UserId);

        //            int result = _statusService.Update(id, param);
        //            return Ok(result);
        //        }
        //        else
        //        {
        //            string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
        //            return Content(HttpStatusCode.BadRequest, errorResult);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.InnerException == null)
        //            return Content(HttpStatusCode.InternalServerError, ex.Message);
        //        else
        //            return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
        //    }
        //}

       // DELETE api/approval/id
        //[HttpDelete]
        //public IHttpActionResult Delete(int id)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            int userId = Int32.Parse(base.UserId);
        //            DateTime getDate = DateHelper.GetDateTime();
        //            int result = _statusService.Delete(id, userId, getDate);
        //            return Ok(result);
        //        }
        //        else
        //        {
        //            string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
        //            return Content(HttpStatusCode.BadRequest, errorResult);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.InnerException == null)
        //            return Content(HttpStatusCode.InternalServerError, ex.Message);
        //        else
        //            return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
        //    }
        //}
    }
}
