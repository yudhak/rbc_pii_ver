﻿using HR.Application;
using HR.Application.DTO;
using HR.Application.Params;
using HR.Common;
using HR.Domain;
using HR.Presentation.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Routing;

namespace HR.Presentation.WebAPI.Controllers
{
    public class AuditLogController : BaseAPIController
    {
        private readonly IAuditLogService _auditLogService;        

        public AuditLogController(IAuditLogService auditLogService)
        {
            _auditLogService = auditLogService;            
        }

        //GET api/auditlog
        [HttpGet]
        public IHttpActionResult Get([FromUri] AuditLogListParameter param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string keyword = string.Empty;
                    int field = 0;
                    string tanggalAwal = string.Empty;
                    string tanggalAkhir = string.Empty;

                    if(param.SortBy != "Invalid date")
                    {
                        tanggalAwal = param.SortBy;
                    }

                    if (param.SortDirection != "Invalid date")
                    {
                        tanggalAkhir = param.SortDirection;
                    }

                    param.Validate();
                    keyword = param.Search;
                    if(param.SearchBy != "")
                    {
                        field = Convert.ToInt32(param.SearchBy);
                    }                    

                    int skip = 0;
                    if (param.PageNo > 0)
                    {
                        skip = (param.PageNo - 1) * param.PageSize;
                    }

                    int totalRows = _auditLogService.GetAll(field, tanggalAwal, tanggalAkhir).Count();
                    var totalPage = totalRows / param.PageSize;
                    var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
                    if (totalPages < 0)
                        totalPages = 0;

                    var result = _auditLogService.GetAll(field, tanggalAwal, tanggalAkhir)
                        .Skip(skip)
                        .Take(param.PageSize)
                        .ToList();

                    var colls = _auditLogService.GetAllConverter(result);
                    //IList<AuditLogDTO> colls = AuditLogDTO.From(result);

                    PaginationDTO page = new PaginationDTO();
                    page.PageCount = totalPages;
                    page.PageNo = param.PageNo;
                    page.PageSize = param.PageSize;
                    page.results = colls;

                    return Ok(page);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //GET api/auditLog/1
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = _auditLogService.Get(id);
                AuditLogDTO auditLogDTO = AuditLogDTO.From(result);
                return Ok(auditLogDTO);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //POST api/auditLog
        [HttpPost]
        public IHttpActionResult Add(AuditLogParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.LogTimestamp = DateHelper.GetDateTime();                    

                    int id = _auditLogService.Add(param);                    
                    return Ok(id);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }        
    }
}
