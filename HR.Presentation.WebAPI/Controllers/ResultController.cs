﻿using HR.Application;
using HR.Application.Params;
using System;
using System.Net;
using System.Web.Http;

namespace HR.Presentation.WebAPI.Controllers
{
    public class ResultController : BaseAPIController
    {
        private readonly IResultService _resultService;
        private readonly IReportService _reportService;

        public ResultController(IResultService resultService, IReportService reportService)
        {
            _resultService = resultService;
            _reportService = reportService;
        }

        [HttpGet]
        public IHttpActionResult GetResult(int id, int projectId, int riskRegistrasiId, int sektorId) //id ==> scenarioId
        {
            try
            {
                var data = _resultService.GetAllDataResult(id, projectId, riskRegistrasiId, sektorId);
                return Ok(data);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //[HttpGet]
        //public IHttpActionResult GetDashboardTreshold()
        //{
        //    try
        //    {
        //        //var scenarioDefault = _scenarioService.GetDefault();
        //        var data = _resultService.GetAllDataCalculationByScenarioId();
        //        return Ok(data);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.InnerException == null)
        //            return Content(HttpStatusCode.InternalServerError, ex.Message);
        //        else
        //            return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
        //    }
        //}
        
    }
}
