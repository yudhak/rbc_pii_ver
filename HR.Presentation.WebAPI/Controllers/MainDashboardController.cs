﻿using HR.Application;
using System;
using System.Net;
using System.Web.Http;

namespace HR.Presentation.WebAPI.Controllers
{
    public class MainDashboardController : BaseAPIController
    {
        private readonly IMainDashboardService _mainDashboardService;

        public MainDashboardController(IMainDashboardService mainDashboardService)
        {
            _mainDashboardService = mainDashboardService;
        }

        [HttpGet]
        public IHttpActionResult GetMainDashboard()
        {
            try
            {
                var data = _mainDashboardService.GetAggregationOfProject();
                return Ok(data);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }
    }
}
