﻿using System.IO;
using HR.Application;
using HR.Application.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.Threading;
using System.Web;
using System.Net.Http.Headers;

namespace HR.Presentation.WebAPI.Controllers
{
    public class CalculationController : BaseAPIController
    {
        private readonly ICalculationService _calculationService;
        private readonly IScenarioService _scenarioService;
        private readonly IResultService _resultService;
        private readonly IReportService _reportService;

        public CalculationController(ICalculationService calculationService, IScenarioService scenarioService, IResultService resultService, IReportService reportService)
        {
            _calculationService = calculationService;
            _scenarioService = scenarioService;
            _resultService = resultService;
            _reportService = reportService;
        }

        [HttpPost]
        public IHttpActionResult GetDashboardDiversifiedRiskCapitalProject(ScenarioCollection param) //id ==> scenarioId
        {
            try
            {
                _calculationService.GenerteScenarioCalculationRisk(param);
                var data = _calculationService.GetAllDataCalculationByScenarioId();
                return Ok(data);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult GetDashboardCalculation()
        {
            try
            {
                var data = _calculationService.GetAllDataCalculationByScenarioId();
                return Ok(data);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        
        [HttpGet]
        public IHttpActionResult GetExcelFiles(bool isDownloadFile)
        {
            string path = "~\\Temp\\Reports\\";
            string name = "RBC_PII.xlsx";

            try
            {
                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(path + name);
                if (urlFile != null)
                {
                    var dataByte = File.ReadAllBytes(urlFile);
                    var dataStream = new MemoryStream(dataByte);
                    return new FileResult(urlFile);
                }
                else
                {
                    throw new ApplicationException(string.Format("File RBC_PII.xlsx tidak ditemukan."));
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        public class FileResult : IHttpActionResult
        {
            private readonly string filePath;
            private readonly string contentType;

            public FileResult(string filePath, string contentType = null)
            {
                this.filePath = filePath;
                this.contentType = contentType;
            }

            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                return Task.Run(() =>
                {
                    var contentType = this.contentType ?? MimeMapping.GetMimeMapping(Path.GetExtension(filePath));
                    var fileName = Path.GetFileName(filePath);
                    HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                    var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                    response.Content = new StreamContent(stream);
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = fileName
                    };
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                    return response;
                }, cancellationToken);
            }
        }
    }
}
