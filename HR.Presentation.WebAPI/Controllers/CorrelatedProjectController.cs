﻿using HR.Application;
using HR.Application.DTO;
using HR.Application.Params;
using HR.Common;
using HR.Domain;
using HR.Presentation.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Routing;

namespace HR.Presentation.WebAPI.Controllers
{
    public class CorrelatedProjectController : BaseAPIController
    {
        private readonly ICorrelatedProjectService _correlatedProjectService;
        private readonly ICorrelationRiskAntarSektorService _correlationRiskAntarSektorService;
        private readonly ICorrelationMatrixService _correlationMatrixService;
        private readonly IScenarioService _scenarioService;
        private readonly IProjectService _projectService;
        private readonly IApprovalService _approvalService;

        public CorrelatedProjectController(ICorrelatedProjectService correlatedProjectService, ICorrelationRiskAntarSektorService correlationRiskAntarSektorService, ICorrelationMatrixService correlationMatrixService,
            IScenarioService scenarioService, IProjectService projectService, IApprovalService approvalService)
        {
            _correlatedProjectService = correlatedProjectService;
            _correlationRiskAntarSektorService = correlationRiskAntarSektorService;
            _correlationMatrixService = correlationMatrixService;
            _scenarioService = scenarioService;
            _projectService = projectService;
            _approvalService = approvalService;
        }

        //GET api/correlatedSector
        [HttpGet]
        public IHttpActionResult Get([FromUri] CorrelatedProjectListParameter param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    
                    string keyword = string.Empty;
                    int field = 0;
                    int user = Int32.Parse(base.UserId);
                    int field2 = 0;
                    param.Validate();
                    keyword = param.Search;
                    if (param.SearchBy != "")
                    {
                        field = Convert.ToInt32(param.SearchBy);
                    }
                    if (param.SearchBy2 != "")
                    {
                        field2 = Convert.ToInt32(param.SearchBy2);
                    }
                    int skip = 0;
                    if (param.PageNo > 0)
                    {
                        skip = (param.PageNo - 1) * param.PageSize;
                    }

                   

                    //int totalRows = _correlatedProjectService.GetByScenarioDefaultId(keyword, field).Where(x => x.StatusId != 4).Count();
                    int totalRows = _correlatedProjectService.GetByScenarioDefaultId(keyword, field, field2, user).Count();
                    var totalPage = totalRows / param.PageSize;
                    var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
                    if (totalPages < 0)
                        totalPages = 0;

                    //var result = _correlatedProjectService.GetByScenarioDefaultId(keyword, field).Where(x => x.StatusId != 4)
                    //    .Skip(skip)
                    //    .Take(param.PageSize)
                    //    .ToList();
                    var result = _correlatedProjectService.GetByScenarioDefaultId(keyword, field, field2, user)
                       .Skip(skip)
                       .Take(param.PageSize)
                       .ToList();

                    //var scenarioDefault = _scenarioService.GetDefault();
                    //var projects = _correlatedProjectService.GetProjectByScenarioDefault(scenarioDefault.Id, user).ToList();
                    var correlationMatrix = _correlationMatrixService.GetAll().ToList();
                    var scenarioAll = _scenarioService.GetAll().ToList();
                    IList<CorrelatedProjectDTO> colls = CorrelatedProjectDTO.From(result, correlationMatrix, scenarioAll, 0);

                    foreach (var item in colls)
                    {
                        if (item.StatusId == 1)
                        {
                            item.StatusUserApproval = _approvalService.GetByActive(item.Id, item.CreateBy.GetValueOrDefault(), "CorrelatedProject");
                        }
                    }

                    PaginationDTO page = new PaginationDTO();
                    page.PageCount = totalPages;
                    page.PageNo = param.PageNo;
                    page.PageSize = param.PageSize;
                    page.results = colls;

                    return Ok(page);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //GET api/correlatedProject/1
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var correlatedProject = _correlatedProjectService.Get(id);
                var scenarioDefault = _scenarioService.GetDefault();
                var projects = _correlationRiskAntarSektorService.GetProjectByScenarioDefault(correlatedProject.ScenarioId).ToList();
                //var projects = _correlatedProjectService.GetProjectByScenarioDefault(scenarioDefault.Id).ToList();
                var correlationMatrix = _correlationMatrixService.GetAllAscNilai().ToList();
                var result = _correlatedProjectService.Get(id);
                //var project = _projectService.Get(result.ProjectId);
                //IList<Project> projects = new List<Project>();
                //projects.Add(project);
                CorrelatedProjectDTO dto = CorrelatedProjectDTO.From(result, correlationMatrix, projects);
                var scenario = _scenarioService.Get(correlatedProject.ScenarioId);
                dto.NamaScenario = scenario.NamaScenario;
                return Ok(dto);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }
    }
}
