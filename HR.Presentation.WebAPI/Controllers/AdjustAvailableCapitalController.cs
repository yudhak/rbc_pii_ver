﻿using System.IO;
using HR.Application;
using HR.Application.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.Threading;
using System.Web;
using System.Net.Http.Headers;

namespace HR.Presentation.WebAPI.Controllers
{
    public class AdjustAvailableCapitalController : BaseAPIController
    {
        private readonly ICalculationService _calculationService;
        private readonly IScenarioService _scenarioService;
        private readonly IResultService _resultService;
        private readonly IReportService _reportService;

        public AdjustAvailableCapitalController(ICalculationService calculationService, IScenarioService scenarioService, IResultService resultService, IReportService reportService)
        {
            _calculationService = calculationService;
            _scenarioService = scenarioService;
            _resultService = resultService;
            _reportService = reportService;
        }

        [HttpPost]
        public IHttpActionResult Add(AvailableCapitalProjectedParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var data = 0;
                    if (param != null)
                    {
                        data = _calculationService.AddAvailableCapitalProjected(param);
                    }
                    return Ok(data);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }
        
    }
}
