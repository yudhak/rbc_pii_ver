﻿using HR.Application;
using HR.Application.DTO;
using HR.Application.Params;
using HR.Common;
using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace HR.Presentation.WebAPI.Controllers
{
    public class ApprovalController : BaseAPIController
    {
        private readonly IApprovalService _approvalService;
        private readonly IUserService _userService;
        private readonly IScenarioDetailService _scenarioDetailService;
        private readonly IProjectRepository _projectRepository;

        private readonly IProjectRiskRegistrasiService _projectRiskRegistrasiService;
        private readonly IRiskRegistrasiService _riskRegistrasiService;
        private readonly IRiskMatrixProjectService _riskMatrixStageService;

        private readonly IScenarioService _scenarioService;
        private readonly IRiskMatrixProjectRepository _riskMatrixProjectRepository;
        private readonly ICorrelatedProjectRepository _correlatedProjectRepository;
        private readonly ICorrelatedSektorRepository _correlatedSektorRepository;
        private readonly ICorrelatedSektorDetailService _correlatedSektorDetailRepository;
        private readonly ICorrelatedProjectDetailService _correlatedProjectDetailService;
        private readonly ICorrelatedProjectService _correlatedProjectService;

        private readonly ISektorRepository _sektorRepository;

        private readonly IProjectService _projectService;

        private readonly IMasterApprovalRiskMatrixProjectRepository _masterApprovalRiskMatrixProjectRepository;
        private readonly IMasterApprovalScenarioRepository _masterApprovalScenarioRepository;
        private readonly IMasterApprovalCorrelatedSektorRepository _masterApprovalCorrelatedSektorRepository;
        private readonly IMasterApprovalCorrelatedProjectRepository _masterApprovalCorrelatedProjectRepository;
        private readonly IStageTahunRiskMatrixService _stageTahunRiskMatrixService;
        private readonly IStageTahunRiskMatrixDetailService _stageTahunRiskMatrixDetailService;

        private readonly ICorrelationRiskAntarSektorService _correlationRiskAntarSektorService;
        private readonly ICorrelationMatrixService _correlationMatrixService;

        private readonly IMasterApprovalScenarioService _masterApprovalScenarioService;
        private readonly IEmailService _emailService;


        private readonly IApprovalRepository _approvalRepository;
        public ApprovalController(IApprovalService approvalService, IUserService userService,
            IScenarioService scenarioService, IScenarioDetailService scenarioDetailService, IProjectRepository projectRepository,
            ISektorRepository sektorRepository, IProjectRiskRegistrasiService projectRiskRegistrasiService,
             IRiskRegistrasiService riskRegistrasiService, IRiskMatrixProjectRepository riskMatrixProjectRepository
            , ICorrelatedProjectRepository correlatedProjectRepository, ICorrelatedSektorRepository correlatedSektorRepository,
             IMasterApprovalScenarioRepository masterApprovalScenarioRepository, IMasterApprovalRiskMatrixProjectRepository masterApprovalRiskMatrixProjectRepository
            , IProjectService projectService, IMasterApprovalCorrelatedSektorRepository masterApprovalCorrelatedSektorRepository
            , IMasterApprovalCorrelatedProjectRepository masterApprovalCorrelatedProjectRepository, ICorrelationRiskAntarSektorService correlationRiskAntarSektorService,
             ICorrelationMatrixService correlationMatrixService, IRiskMatrixProjectService riskMatrixStageService, IStageTahunRiskMatrixService stageTahunRiskMatrixService,
             IStageTahunRiskMatrixDetailService stageTahunRiskMatrixDetailService, ICorrelatedSektorDetailService correlatedSektorDetailRepository, ICorrelatedProjectDetailService correlatedProjectDetailService
            , ICorrelatedProjectService correlatedProjectService, IMasterApprovalScenarioService masterApprovalScenarioService, IEmailService emailService, IApprovalRepository approvalRepository)
        {
            _approvalService = approvalService;
            _userService = userService;
            _scenarioDetailService = scenarioDetailService;
            _projectRepository = projectRepository;
            _sektorRepository = sektorRepository;
            _projectRiskRegistrasiService = projectRiskRegistrasiService;
            _riskRegistrasiService = riskRegistrasiService;
            _scenarioService = scenarioService;
            _riskMatrixProjectRepository = riskMatrixProjectRepository;
            _correlatedProjectRepository = correlatedProjectRepository;
            _correlatedSektorRepository = correlatedSektorRepository;
            _masterApprovalScenarioRepository = masterApprovalScenarioRepository;
            _masterApprovalRiskMatrixProjectRepository = masterApprovalRiskMatrixProjectRepository;
            _projectService = projectService;
            _masterApprovalCorrelatedSektorRepository = masterApprovalCorrelatedSektorRepository;
            _masterApprovalCorrelatedProjectRepository = masterApprovalCorrelatedProjectRepository;
            _correlationRiskAntarSektorService = correlationRiskAntarSektorService;
            _correlationMatrixService = correlationMatrixService;
            _correlatedSektorDetailRepository = correlatedSektorDetailRepository;
            _riskMatrixStageService = riskMatrixStageService;
            _stageTahunRiskMatrixService = stageTahunRiskMatrixService;
            _stageTahunRiskMatrixDetailService = stageTahunRiskMatrixDetailService;
            _correlatedProjectDetailService = correlatedProjectDetailService;
            _correlatedProjectService = correlatedProjectService;
            _masterApprovalScenarioService = masterApprovalScenarioService;
            _emailService = emailService;
            _approvalRepository = approvalRepository;
        }

        //GET api/approval
        [HttpGet]
        public IHttpActionResult Get([FromUri] ApprovalListParameter param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                   int userId = Int32.Parse(base.UserId);
                    bool isAdminNewScenario = false;
                    bool isAdminNewRiskMatrix = false;
                    bool isAdminNewCorrelatedSektor = false;
                    bool isAdminNewCorrelatedProject = false;

                    string keyword = string.Empty;
                    int field = 0;
                    int field2 = 0;
                    param.Validate();
                    keyword = param.Search;
                    if (param.SearchBy != "")
                    {
                        field = Convert.ToInt32(param.SearchBy);
                    }
                    if (param.SearchBy2 != "")
                    {
                        field2 = Convert.ToInt32(param.SearchBy2);
                    }
                    var userUpdate = _userService.Get(userId);
                    var riskRegistrasi = _riskRegistrasiService.GetAll().OrderBy(x => x.KodeMRisk).ToList();

                    #region getFromSource
                    var result = _approvalRepository.GetItemActive(keyword, field, field2).ToList();

                    IList<ApprovalDTO> DTOGetItem = new List<ApprovalDTO>();

                    var masterApprovalScenario = _masterApprovalScenarioRepository.GetByUserId(userId);
                    if (masterApprovalScenario == null && (userUpdate.RoleId == 5 || userUpdate.RoleId == 3 || userUpdate.RoleId == 4 ))
                    {
                        masterApprovalScenario =_masterApprovalScenarioRepository.GetByRoleId(userUpdate.RoleId);
                        isAdminNewScenario = true;
                    }
                    //get role user from master scenario
                    var masterApprovalCorrelatedSektor = _masterApprovalCorrelatedSektorRepository.GetByUserId(userId);
                    if (masterApprovalCorrelatedSektor == null && (userUpdate.RoleId == 5 || userUpdate.RoleId == 3 || userUpdate.RoleId == 4))
                    {
                        masterApprovalCorrelatedSektor = _masterApprovalCorrelatedSektorRepository.GetByRoleId(userUpdate.RoleId);
                        isAdminNewCorrelatedSektor = true;
                    }

                    for (int i = 0; i < result.Count(); i++)
                    {
                        switch (result[i].SourceApproval)
                        {
                            #region scenario
                            case "Scenario":
                                if (masterApprovalScenario.NomorUrutStatus - 1 == result[i].NomorUrutStatus)
                                {
                                    var scenario = _scenarioService.Get(result[i].RequestId);

                                    var user = _userService.Get(result[i].CreateBy.GetValueOrDefault());
                                    ApprovalDTO approvalDTOs = new ApprovalDTO(result[i].Status, scenario, user);
                                    { 
                                    approvalDTOs.CreateBy = result[i].CreateBy.GetValueOrDefault();
                                    approvalDTOs.CreateDate = result[i].CreateDate.GetValueOrDefault();
                                    approvalDTOs.DeleteDate = result[i].DeleteDate;
                                    approvalDTOs.Id = result[i].Id;
                                    approvalDTOs.IsActive = result[i].IsActive;
                                    approvalDTOs.IsDelete = result[i].IsDelete;
                                    approvalDTOs.Keterangan = result[i].Keterangan;
                                    approvalDTOs.RequestId = result[i].RequestId;
                                    approvalDTOs.SourceApproval = result[i].SourceApproval;
                                    approvalDTOs.UpdateBy = result[i].UpdateBy;
                                    approvalDTOs.UpdateDate = result[i].UpdateDate;
                                    approvalDTOs.StatusApproval = result[i].StatusApproval;
                                    approvalDTOs.NamaPembuat = user.UserName;
                                    approvalDTOs.NamaScenario = scenario.NamaScenario;
                                    approvalDTOs.NamaEntitas = scenario.NamaScenario;
                                        approvalDTOs.Keterangan = result[i].Keterangan;
                                    };
                                    if (isAdminNewScenario == true)
                                    {
                                        approvalDTOs.SourceApproval = approvalDTOs.SourceApproval + " ";
                                    }
                                    DTOGetItem.Add(approvalDTOs);
                                }
                                break;
                            #endregion scenario

                            #region risk matrix project
                            case "RiskMatrixProject":
                                var riskMatrixProject = _riskMatrixProjectRepository.Get(result[i].RequestId);
                                var masterApprovalRiskMatrixProject = _masterApprovalRiskMatrixProjectRepository.GetByProjectIdUserId(riskMatrixProject.ProjectId,userId);
                                if (masterApprovalRiskMatrixProject == null && (userUpdate.RoleId == 5 || userUpdate.RoleId == 3 || userUpdate.RoleId == 4))
                                {
                                    masterApprovalRiskMatrixProject = _masterApprovalRiskMatrixProjectRepository.GetByProjectIdRoleId(riskMatrixProject.ProjectId,userUpdate.RoleId);
                                    isAdminNewRiskMatrix = true;
                                }
                                if (masterApprovalRiskMatrixProject.NomorUrutStatus - 1 == result[i].NomorUrutStatus)
                                {
                                    var user = _userService.Get(result[i].CreateBy.GetValueOrDefault());

                                    var namaScenario = _scenarioService.Get(riskMatrixProject.ScenarioId).NamaScenario;

                                    var namaProject = _projectRepository.Get(riskMatrixProject.ProjectId).NamaProject;

                                    IList<RiskMatrixCollectionParam> riskmatrixcollectionListParam = new List<RiskMatrixCollectionParam>();

                                    //var resultStageTahunDetail = _stageTahunRiskMatrixDetailService.GetByRiskMatrixProjectId(result[i].RequestId);

                                    var resultStageTahun = _stageTahunRiskMatrixService.GetByRiskMatrixProjectIdForHeaderTable(result[i].RequestId).ToList();

                                    var paramStageTahun = _stageTahunRiskMatrixDetailService.GetByRiskMatrixProjectId(result[i].RequestId);

                                    foreach (var itemStageTahun in resultStageTahun)
                                    {
                                        RiskMatrixCollectionParam riskMatrixCollectionParam = new RiskMatrixCollectionParam()
                                        {
                                            CreateBy = itemStageTahun.CreateBy,
                                            CreateDate = itemStageTahun.CreateDate,
                                            DeleteDate = itemStageTahun.DeleteDate,
                                            Id = itemStageTahun.Id,
                                            IsDelete = itemStageTahun.IsDelete,
                                            IsUpdate = itemStageTahun.IsUpdate,
                                            Stage = itemStageTahun.Stage,
                                            StageId = itemStageTahun.StageId,
                                            Tahun = itemStageTahun.Tahun,
                                            UpdateBy = itemStageTahun.UpdateBy,
                                            UpdateDate = itemStageTahun.UpdateDate

                                        };
                                        riskmatrixcollectionListParam.Add(riskMatrixCollectionParam);
                                    }

                                    int jumlahParam = riskmatrixcollectionListParam.Count();
                                    int jumlahParamAwal = 0;

                                    foreach (var item2 in paramStageTahun.RiskMatrixCollection)
                                    {
                                        riskmatrixcollectionListParam[jumlahParamAwal].MaximumNilaiExpose = item2.MaximumNilaiExpose;
                                        riskmatrixcollectionListParam[jumlahParamAwal].RiskMatrixValue = item2.RiskMatrixValue;
                                        jumlahParamAwal++;
                                    }

                                    ApprovalDTO approvalDTOrm = new ApprovalDTO(result[i].Status, riskMatrixProject, riskRegistrasi, user, resultStageTahun, riskmatrixcollectionListParam)
                                    {
                                        CreateBy = result[i].CreateBy,
                                        CreateDate = result[i].CreateDate,
                                        DeleteDate = result[i].DeleteDate,
                                        Id = result[i].Id,
                                        IsActive = result[i].IsActive,
                                        IsDelete = result[i].IsDelete,
                                        Keterangan = result[i].Keterangan,
                                        RequestId = result[i].RequestId,
                                        SourceApproval = result[i].SourceApproval,
                                        UpdateBy = result[i].UpdateBy,
                                        UpdateDate = result[i].UpdateDate,
                                        StatusApproval = result[i].StatusApproval,
                                        NamaPembuat = user.UserName,
                                        NamaScenario = namaScenario,
                                        NamaEntitas = namaProject
                                    };
                                    if (isAdminNewRiskMatrix == true)
                                    {
                                        approvalDTOrm.SourceApproval = approvalDTOrm.SourceApproval + " ";
                                    }
                                    DTOGetItem.Add(approvalDTOrm);
                                }
                                break;
                            #endregion risk matrix project

                            #region correlated project
                            case "CorrelatedProject":
                                var correlatedProject = _correlatedProjectService.Get(result[i].RequestId);
                                var masterApprovalCorrelatedProject = _masterApprovalCorrelatedProjectRepository.GetByProjectIdUserId(correlatedProject.ProjectId, userId);
                                if (masterApprovalCorrelatedProject == null && (userUpdate.RoleId == 5 || userUpdate.RoleId == 3 || userUpdate.RoleId == 4))
                                {
                                    masterApprovalCorrelatedProject = _masterApprovalCorrelatedProjectRepository.GetByProjectIdRoleId(correlatedProject.ProjectId, userUpdate.RoleId);
                                    isAdminNewCorrelatedProject = true;
                                }
                                if (masterApprovalCorrelatedProject.NomorUrutStatus - 1 == result[i].NomorUrutStatus)
                                {
                                    var scenario = _scenarioService.Get(correlatedProject.ScenarioId);

                                    var project = _projectRepository.Get(correlatedProject.ProjectId);

                                    var user = _userService.Get(result[i].CreateBy.GetValueOrDefault());
                                    ApprovalDTO approvalDTOcp = new ApprovalDTO(result[i].Status, user)
                                    {
                                        CreateBy = result[i].CreateBy,
                                        CreateDate = result[i].CreateDate,
                                        DeleteDate = result[i].DeleteDate,
                                        Id = result[i].Id,
                                        IsActive = result[i].IsActive,
                                        IsDelete = result[i].IsDelete,
                                        Keterangan = result[i].Keterangan,
                                        RequestId = result[i].RequestId,
                                        SourceApproval = result[i].SourceApproval,
                                        UpdateBy = result[i].UpdateBy,
                                        UpdateDate = result[i].UpdateDate,
                                        StatusApproval = result[i].StatusApproval,
                                        NamaPembuat = user.UserName,
                                        NamaScenario = scenario.NamaScenario,
                                        NamaEntitas = project.NamaProject
                                    };


                                    var collection = _correlatedProjectDetailService.GetByCorrelatedProjectId(correlatedProject.Id);

                                    approvalDTOcp.correlatedProjectDetailCollection = collection;
                                    IList<CorrelationMatrix> correlationMatrix = _correlationMatrixService.GetAll(null, 0).ToList();
                                    approvalDTOcp.CorrelationMatrix = correlationMatrix;

                                    List<CorrelatedProjectApprovalParam> correlatedProjectApprovalParamList = new List<CorrelatedProjectApprovalParam>();
                                    var projects = _correlationRiskAntarSektorService.GetProjectByScenarioDefault(correlatedProject.ScenarioId).ToList();
                                    foreach (var item2 in projects)
                                    {
                                        if (correlatedProject != null)
                                        {
                                            CorrelatedProjectApprovalParam item3 = new CorrelatedProjectApprovalParam();
                                            item3.ProjectId = item2.Id;
                                            item3.ProjectName = item2.NamaProject;
                                            item3.SektorId = item2.Sektor.Id;
                                            item3.SektorName = item2.Sektor.NamaSektor;

                                            correlatedProjectApprovalParamList.Add(item3);
                                        }
                                    }

                                    approvalDTOcp.correlatedProjectList = correlatedProjectApprovalParamList;
                                    if (isAdminNewCorrelatedProject == true)
                                    {
                                        approvalDTOcp.SourceApproval = approvalDTOcp.SourceApproval = " ";
                                    }
                                    DTOGetItem.Add(approvalDTOcp);
                                }
                                break;

                            #endregion correlated project
    
                            #region correlated sektor
                            case "CorrelatedSektor":
                                if (masterApprovalCorrelatedSektor.NomorUrutStatus - 1 == result[i].NomorUrutStatus)
                                {
                                    var user = _userService.Get(result[i].CreateBy.GetValueOrDefault());

                                    var correlatedSektor = _correlatedSektorRepository.Get(result[i].RequestId);

                                    var namaSektor = _sektorRepository.Get(correlatedSektor.SektorId).NamaSektor;

                                    var namaScenario = _scenarioService.Get(correlatedSektor.ScenarioId).NamaScenario;

                                    ApprovalDTO approvalDTOcs = new ApprovalDTO(result[i].Status, correlatedSektor, user, masterApprovalCorrelatedSektor)
                                    {
                                        CreateBy = result[i].CreateBy,
                                        CreateDate = result[i].CreateDate,
                                        DeleteDate = result[i].DeleteDate,
                                        Id = result[i].Id,
                                        IsActive = result[i].IsActive,
                                        IsDelete = result[i].IsDelete,
                                        Keterangan = result[i].Keterangan,
                                        RequestId = result[i].RequestId,
                                        SourceApproval = result[i].SourceApproval,
                                        UpdateBy = result[i].UpdateBy,
                                        UpdateDate = result[i].UpdateDate,
                                        StatusApproval = result[i].StatusApproval,
                                        NamaPembuat = user.UserName,
                                        NamaScenario = namaScenario,
                                        NamaEntitas = namaSektor

                                    };

                                    var collection = _correlatedSektorDetailRepository.GetByCorrelatedSektorId(correlatedSektor.Id);
                                    approvalDTOcs.correlatedSektorDetailCollection = collection;
                                    //IList<RiskRegistrasi> risks = _riskRegistrasiService.GetAll(null, 0).ToList();
                                    approvalDTOcs.RiskRegistrasi = riskRegistrasi;
                                    IList<CorrelationMatrix> correlationMatrix = _correlationMatrixService.GetAll(null, 0).ToList();
                                    approvalDTOcs.CorrelationMatrix = correlationMatrix;
                                    if (isAdminNewCorrelatedSektor == true)
                                    {
                                        approvalDTOcs.SourceApproval = approvalDTOcs.SourceApproval + " ";
                                    }
                                    DTOGetItem.Add(approvalDTOcs);

                                }
                                break;
                            #endregion correlated sektor
                            default:
                                break;
                        }
                    }
                    #endregion getFromSource

                    IList<Approval> approval = _approvalService.GetAll().ToList();
                    if (param.IsPagination())
                    {
                        int skip = 0;
                        if (param.PageNo > 0)
                        {
                            skip = (param.PageNo - 1) * param.PageSize;
                        }

                       // app.IsActive == true && app.IsDelete == false && app.RoleId == roleId.Id - 1
                        int totalRows = DTOGetItem.Count();
                        var totalPage = totalRows / param.PageSize;
                        var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
                        if (totalPages < 0)
                            totalPages = 0;

                        var result1 = _approvalService.GetAll()
                            .Skip(skip)
                            .Take(param.PageSize)
                            .ToList();

                        var result2 = DTOGetItem
                            .Skip(skip)
                            .Take(param.PageSize)
                            .ToList();

                        IList<ApprovalDTO> appDto2 = new List<ApprovalDTO>();
                        //foreach (var item in result2)
                        //{
                        //    //approval master risk matrix project
                        //    //ApprovalDTO approvalDTO = new ApprovalDTO(item.Status, item.riskMatrixProject, riskRegistrasi, item.user)
                        //    //{
                        //    //    CreateBy = item.CreateBy,
                        //    //    CreateDate = item.CreateDate,
                        //    //    DeleteDate = item.DeleteDate,
                        //    //    Id = item.Id,
                        //    //    IsActive = item.IsActive,
                        //    //    IsDelete = item.IsDelete,
                        //    //    Keterangan = item.Keterangan,
                        //    //    RequestId = item.RequestId,
                        //    //    SourceApproval = item.SourceApproval,
                        //    //    UpdateBy = item.UpdateBy,
                        //    //    UpdateDate = item.UpdateDate,
                        //    //    StatusApproval = item.StatusApproval
                        //    //};
                        //    //approval master scenario
                        //    //ApprovalDTO approvalDTO = new ApprovalDTO(item.Status, item.scenario, item.user)
                        //    //{
                        //    //    CreateBy = item.CreateBy,
                        //    //    CreateDate = item.CreateDate,
                        //    //    DeleteDate = item.DeleteDate,
                        //    //    Id = item.Id,
                        //    //    IsActive = item.IsActive,
                        //    //    IsDelete = item.IsDelete,
                        //    //    Keterangan = item.Keterangan,
                        //    //    RequestId = item.RequestId,
                        //    //    SourceApproval = item.SourceApproval,
                        //    //    UpdateBy = item.UpdateBy,
                        //    //    UpdateDate = item.UpdateDate,
                        //    //    StatusApproval = item.StatusApproval
                        //    //};
                        //    //approval correlated project
                        //    //ApprovalDTO approvalDTO = new ApprovalDTO(item.Status, item.user)
                        //    //{
                        //    //    CreateBy = item.CreateBy,
                        //    //    CreateDate = item.CreateDate,
                        //    //    DeleteDate = item.DeleteDate,
                        //    //    Id = item.Id,
                        //    //    IsActive = item.IsActive,
                        //    //    IsDelete = item.IsDelete,
                        //    //    Keterangan = item.Keterangan,
                        //    //    RequestId = item.RequestId,
                        //    //    SourceApproval = item.SourceApproval,
                        //    //    UpdateBy = item.UpdateBy,
                        //    //    UpdateDate = item.UpdateDate,
                        //    //    StatusApproval = item.StatusApproval
                        //    //};
                        //    //appDto2.Add(approvalDTO);
                        //}

                        PaginationDTO page = new PaginationDTO();
                        page.PageCount = totalPages;
                        page.PageNo = param.PageNo;
                        page.PageSize = param.PageSize;
                        page.results = result2;

                        return Ok(page);
                    }
                    else
                    {
                        IList<ApprovalDTO> dto = ApprovalDTO.From(approval);
                        return Ok(DTOGetItem);
                    }
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //GET api/approval/id
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = _approvalService.Get(id);
                ApprovalDTO approvalDTO = ApprovalDTO.From(result);
                return Ok(approvalDTO);

                //return Ok(stageDTO);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //POST api/approval
        [HttpPost]
        public IHttpActionResult Add(ApprovalParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    int id = _approvalService.Add(param);
                    return Ok(1);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                if (ex.Message == "Failure sending mail.")
                {
                    //throw new ApplicationException(string.Format("Gagal mengirim email"));
                    return Content(HttpStatusCode.InternalServerError, "Gagal mengirim email");
                }
                return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //PUT api/approval/id
        [HttpPut]
        public IHttpActionResult Update(int id, [FromBody]ApprovalParam param)
        {
            try
            {
                int result;
                param.UpdateDate = DateHelper.GetDateTime();
                param.UpdateBy = Int32.Parse(base.UserId);
                result = _approvalService.UpdateStatus(id, param);

                return Ok(id);

                //if (ModelState.IsValid)
                //{
                //    int result;
                //    param.UpdateDate = DateHelper.GetDateTime();
                //    param.UpdateBy = Int32.Parse(base.UserId);
                //    result = _approvalService.UpdateStatus(id, param);
                    
                //    return Ok(id);
                //}
                //else
                //{
                //    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                //    return Content(HttpStatusCode.BadRequest, errorResult);
                //}
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                if (ex.Message == "Failure sending mail.")
                {
                    //throw new ApplicationException(string.Format("Gagal mengirim email"));
                    return Content(HttpStatusCode.InternalServerError, "Gagal mengirim email");
                }
                return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //DELETE api/approval/id
        //[HttpDelete]
        //public IHttpActionResult Delete(int id)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            int userId = Int32.Parse(base.UserId);
        //            DateTime getDate = DateHelper.GetDateTime();
        //            int result = _approvalService.Delete(id, userId, getDate);
        //            return Ok(result);
        //        }
        //        else
        //        {
        //            string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
        //            return Content(HttpStatusCode.BadRequest, errorResult);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.InnerException == null)
        //            return Content(HttpStatusCode.InternalServerError, ex.Message);
        //        else
        //            return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
        //    }
        //}
    }
}
