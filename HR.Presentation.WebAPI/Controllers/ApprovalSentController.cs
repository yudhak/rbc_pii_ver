﻿using HR.Application;
using HR.Application.DTO;
using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace HR.Presentation.WebAPI.Controllers
{
    public class ApprovalSentController : BaseAPIController
    {
        private readonly IApprovalService _approvalService;
        private readonly IUserService _userService;
        private readonly IScenarioService _scenarioService;
        private readonly IStatusService _statusService;

        public ApprovalSentController(IApprovalService approvalService, IUserService userService, IScenarioService scenarioService, IStatusService statusService)
        {
            _approvalService = approvalService;
            _userService = userService;
            _scenarioService = scenarioService;
            _statusService = statusService;
        }

        [HttpGet]
        public IHttpActionResult Get([FromUri] ApprovalListParameter param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userId = Int32.Parse(base.UserId);
                    //var userId = 3;

                    string keyword = string.Empty;
                    int field = 0;
                    int field2 = 0;
                    param.Validate();
                    keyword = param.Search;
                    if (param.SearchBy != "")
                    {
                        field = Convert.ToInt32(param.SearchBy);
                    }
                    if (param.SearchBy2 != "")
                    {
                        field2 = Convert.ToInt32(param.SearchBy2);
                    }
                    IList<ApprovalExtend> approvals = _approvalService.GetSentItem(keyword, field, field2, true, userId).ToList();
                   
                    int skip = 0;
                    if (param.PageNo > 0)
                    {
                        skip = (param.PageNo - 1) * param.PageSize;
                    }

                    int totalRows = _approvalService.GetSentItem(keyword, field, field2, true, userId).Count();
                    var totalPage = totalRows / param.PageSize;
                    var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
                    if (totalPages < 0)
                        totalPages = 0;

                    var result = approvals.Skip(skip).Take(param.PageSize).ToList();
                       
                    PaginationDTO page = new PaginationDTO();
                    page.PageCount = totalPages;
                    page.PageNo = param.PageNo;
                    page.PageSize = param.PageSize;
                    page.results = result;

                    return Ok(page);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //GET api/approvalsent/id
        [HttpGet]
        public IHttpActionResult Get(int id) // id = requestId
        {
            try
            {
                var result = _approvalService.GetByRequestId(id).ToList();
                return Ok(result);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }
    }
}
