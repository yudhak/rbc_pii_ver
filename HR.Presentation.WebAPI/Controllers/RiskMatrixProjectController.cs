﻿using HR.Application;
using HR.Application.DTO;
using HR.Application.Params;
using HR.Common;
using HR.Domain;
using HR.Presentation.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Routing;

namespace HR.Presentation.WebAPI.Controllers
{
    public class RiskMatrixProjectController : BaseAPIController
    {
        private readonly IRiskMatrixProjectService _riskMatrixStageService;
        private readonly IRiskRegistrasiService _riskRegistrasiService;
        private readonly IStageTahunRiskMatrixService _stageTahunRiskMatrixService;
        private readonly IApprovalService _approvalService;

        public RiskMatrixProjectController(IRiskMatrixProjectService riskMatrixStageService, IRiskRegistrasiService riskRegistrasiService, IStageTahunRiskMatrixService stageTahunRiskMatrixService, IApprovalService approvalService)
        {
            _riskMatrixStageService = riskMatrixStageService;
            _riskRegistrasiService = riskRegistrasiService;
            _stageTahunRiskMatrixService = stageTahunRiskMatrixService;
            _approvalService = approvalService;
        }

        //GET api/riskMatrixStage
        [HttpGet]
        public IHttpActionResult Get([FromUri] RiskMatrixProjectListParameter param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string keyword = string.Empty;
                    int field = 0;
                    int field2 = 0;
                    int userId = 0;
                    if (base.UserId == "")
                    {
                        userId = 1;
                    }
                    else
                    {
                        userId = Int32.Parse(base.UserId);
                    }

                    param.Validate();
                    keyword = param.Search;
                    if (param.SearchBy != "")
                    {
                        field = Convert.ToInt32(param.SearchBy);
                    }
                    if (param.SearchBy2 != "")
                    {
                        field2 = Convert.ToInt32(param.SearchBy2);
                    }
                    var riskRegistrasi = _riskRegistrasiService.GetAll().ToList();
                    if (!param.IsAllData)
                    {
                        int skip = 0;
                        if (param.PageNo > 0)
                        {
                            skip = (param.PageNo - 1) * param.PageSize;
                        }

                        int totalRows = _riskMatrixStageService.GetAll(keyword, field, field2, userId).Where(x => x.StatusId != 4).Count();
                        //int totalRows = _riskMatrixStageService.GetAllBaseId(keyword, Int32.Parse(base.UserId)).Count();
                        var totalPage = totalRows / param.PageSize;
                        var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
                        if (totalPages < 0)
                            totalPages = 0;

                        var result = _riskMatrixStageService.GetAll(keyword, field, field2, userId).Where(x => x.StatusId != 4)
                            .Skip(skip)
                            .Take(param.PageSize)
                            .ToList();

                        //var result = _riskMatrixStageService.GetAllBaseId(keyword, Int32.Parse(base.UserId))
                        //    .Skip(skip)
                        //    .Take(param.PageSize)
                        //    .ToList();

                        IList<RiskMatrixProjectLightDTO> colls = RiskMatrixProjectLightDTO.From(result, riskRegistrasi);

                        foreach (var item in colls)
                        {
                            if (item.StatusId == 1)
                            {
                                item.StatusUserApproval = _approvalService.GetByActive(item.Id, item.CreateBy.GetValueOrDefault(), "RiskMatrixProject");
                            }
                        }

                        PaginationDTO page = new PaginationDTO();
                        page.PageCount = totalPages;
                        page.PageNo = param.PageNo;
                        page.PageSize = param.PageSize;
                        page.results = colls;

                        return Ok(page);
                    }
                    else if (param.userFilter == true)
                    {
                        int skip = 0;
                        if (param.PageNo > 0)
                        {
                            skip = (param.PageNo - 1) * param.PageSize;
                        }

                        int totalRows = _riskMatrixStageService.GetAll2(keyword, field, userId).Count();
                        //int totalRows = _riskMatrixStageService.GetAllBaseId(keyword, Int32.Parse(base.UserId)).Count();
                        var totalPage = totalRows / param.PageSize;
                        var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
                        if (totalPages < 0)
                            totalPages = 0;

                        var result = _riskMatrixStageService.GetAll2(keyword, field, userId)
                            .Skip(skip)
                            .Take(param.PageSize)
                            .ToList();

                        IList<RiskMatrixProjectDTO> colls = RiskMatrixProjectDTO.From(result, riskRegistrasi);

                        PaginationDTO page = new PaginationDTO();
                        page.PageCount = totalPages;
                        page.PageNo = param.PageNo;
                        page.PageSize = param.PageSize;
                        page.results = colls;

                        return Ok(page);
                    }
                    else
                    {
                        var result = _riskMatrixStageService.GetAllData(keyword).ToList();
                        IList<RiskMatrixProjectDTO> dto = RiskMatrixProjectDTO.From(result, riskRegistrasi);
                        return Ok(dto);
                    }
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }



        //GET api/riskMatrixStage/1
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = _riskMatrixStageService.Get(id);
                var riskRegistrasi = _riskRegistrasiService.GetAll().ToList();
                var stageTahun = _stageTahunRiskMatrixService.GetByRiskMatrixProjectIdForHeaderTable(result.Id).ToList();

                RiskMatrixProjectExtendDTO riskMatrixStageDTO = RiskMatrixProjectExtendDTO.From(result, riskRegistrasi, stageTahun);
                return Ok(riskMatrixStageDTO);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //POST api/riskMatrixStage
        [HttpPost]
        public IHttpActionResult Add(RiskMatrixProjectParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.CreateDate = DateHelper.GetDateTime();
                    param.CreateBy = Int32.Parse(base.UserId);

                    int id = _riskMatrixStageService.Add(param);
                    return Ok(id);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //PUT api/riskMatrixStage/id
        [HttpPut]
        public IHttpActionResult Update(int id, [FromBody]RiskMatrixProjectParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.UpdateDate = DateHelper.GetDateTime();
                    param.UpdateBy = Int32.Parse(base.UserId);

                    int result = _riskMatrixStageService.Update(id, param);
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //DELETE api/riskMatrixStage/id
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId = Int32.Parse(base.UserId);
                    DateTime getDate = DateHelper.GetDateTime();
                    int result = _riskMatrixStageService.Delete(id, userId, getDate);
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }
    }
}
