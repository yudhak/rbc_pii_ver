﻿using HR.Application;
using HR.Application.DTO;
using HR.Application.Params;
using HR.Common;
using HR.Domain;
using HR.Presentation.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Routing;

namespace HR.Presentation.WebAPI.Controllers
{
    public class FunctionalRiskController : BaseAPIController
    {
        private readonly IFunctionalRiskService _functionalRiskService;

        public FunctionalRiskController(IFunctionalRiskService functionalRiskService)
        {
            _functionalRiskService = functionalRiskService;
        }

        //GET api/functionalRisk
        [HttpGet]
        public IHttpActionResult Get([FromUri] FunctionalRiskListParameter param)
        {
            try
            {
                if (ModelState.IsValid)
                {                   
                    if (param.IsPagination())
                    {
                        string keyword = string.Empty;
                        int field = 0;

                        param.Validate();
                        keyword = param.Search;
                        if (param.SearchBy != "")
                        {
                            field = Convert.ToInt32(param.SearchBy);
                        }

                        int skip = 0;
                        if (param.PageNo > 0)
                        {
                            skip = (param.PageNo - 1) * param.PageSize;
                        }

                        IList<FunctionalRisk> functionalRisks = _functionalRiskService.GetAll(keyword, field).ToList();
                        int totalRows = _functionalRiskService.GetAll(keyword, field).Count();
                        var totalPage = totalRows / param.PageSize;
                        var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
                        if (totalPages < 0)
                            totalPages = 0;

                        var result = functionalRisks.Skip(skip).Take(param.PageSize).ToList();
                        
                        IList<FunctionalRiskDTO> colls = FunctionalRiskDTO.From(result);

                        PaginationDTO page = new PaginationDTO();
                        page.PageCount = totalPages;
                        page.PageNo = param.PageNo;
                        page.PageSize = param.PageSize;
                        page.results = colls;

                        return Ok(page);
                    }
                    else
                    {
                        IList<FunctionalRisk> functionalRisks = _functionalRiskService.GetAll(null, 0).ToList();
                        IList<FunctionalRiskDTO> dto = FunctionalRiskDTO.From(functionalRisks);
                        return Ok(dto);
                    }
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //GET api/functionalRisk/1
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                //IList<FunctionalRisk> functionalRisks = _functionalRiskService.GetByScenarioId(id).ToList();
                //IList<FunctionalRiskDTO> functionalRiskDTO = FunctionalRiskDTO.From(functionalRisks);

                var result = _functionalRiskService.Get(id);
                FunctionalRiskDTO functionalRiskDTO = FunctionalRiskDTO.From(result);
                return Ok(functionalRiskDTO);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //POST api/functionalRisk
        [HttpPost]
        public IHttpActionResult Add(FunctionalRiskAddParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.CreateDate = DateHelper.GetDateTime();
                    param.CreateBy = Int32.Parse(base.UserId);

                    int id = _functionalRiskService.Add(param);
                    return Ok(id);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //PUT api/functionalRisk/id
        [HttpPut]
        public IHttpActionResult Update(int id, [FromBody]FunctionalRiskParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.UpdateDate = DateHelper.GetDateTime();
                    param.UpdateBy = Int32.Parse(base.UserId);

                    int result = _functionalRiskService.Update(id, param);
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //DELETE api/functionalRisk/id
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId = Int32.Parse(base.UserId);
                    DateTime getDate = DateHelper.GetDateTime();
                    int result = _functionalRiskService.Delete(id, userId, getDate);
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }
    }
}
