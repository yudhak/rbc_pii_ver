﻿using HR.Application;
using System;
using System.Net;
using System.Web.Http;

namespace HR.Presentation.WebAPI.Controllers
{
    public class ResultRiskBudgetController : BaseAPIController
    {
        private readonly IResultService _resultService;

        public ResultRiskBudgetController(IResultService resultService)
        {
            _resultService = resultService;
        }

        [HttpGet]
        public IHttpActionResult GetResult(int id, int projectId, int riskRegistrasiId, int sektorId) //id ==> scenarioId
        {
            try
            {
                var data = _resultService.GetAllDataResult(id, projectId, riskRegistrasiId, sektorId);
                return Ok(data);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }
    }
}
