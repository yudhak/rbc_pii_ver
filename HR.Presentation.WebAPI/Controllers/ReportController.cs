﻿using System.IO;
using HR.Application;
using HR.Application.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.Threading;
using System.Web;
using System.Net.Http.Headers;

namespace HR.Presentation.WebAPI.Controllers
{
    public class ReportController : BaseAPIController
    {
        private readonly IReportService _reportService;
        private readonly IScenarioService _scenarioService;
        private readonly IResultService _resultService;
        private readonly IMainDashboardService _mainDashboardService;
        private readonly ICalculationService _calculationService;

        public ReportController(IReportService reportService, IScenarioService scenarioService, IResultService resultService, IMainDashboardService mainDashboardService, ICalculationService calculationService)
        {
            _reportService = reportService;
            _scenarioService = scenarioService;
            _resultService = resultService;
            _mainDashboardService = mainDashboardService;
            _calculationService = calculationService;
        }
        

        [HttpGet]
        public IHttpActionResult GetCalculationResultReport(int isDownloadFile)
        {
            string path = "";
            string name = "";

            if (isDownloadFile == 1)
            {
                var data = _calculationService.GetAllDataCalculationByScenarioId();
                _reportService.CalculationReport(data);
                path = "~\\Temp\\Reports\\Calculation\\";
                name = "RBC_PII_Calculation.xlsx";
            }

            if (isDownloadFile == 2)
            {
                var data = _mainDashboardService.GetReportMainDashboard();
                _reportService.DashboardReport(data);
                path = "~\\Temp\\Reports\\Main_Dashboard\\";
                name = "RBC_PII_Main_Dashboard.xlsx";
            }

            try
            {
                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(path + name);
                if (urlFile != null)
                {
                    var dataByte = File.ReadAllBytes(urlFile);
                    var dataStream = new MemoryStream(dataByte);
                    return new FileResult(urlFile);
                }
                else
                {
                    throw new ApplicationException(string.Format("File tidak ditemukan."));
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        public IHttpActionResult GetMainDashboardReport(int id, int projectId, int riskRegistrasiId, int sektorId) //id ==> scenarioId
        {
            string path = "";
            string name = "";

            var data = _resultService.GetAllDataResult(id, projectId, riskRegistrasiId, sektorId);
            _reportService.ResultReport(data);
            path = "~\\Temp\\Reports\\Result\\";
            name = "RBC_PII_Result.xlsx";

            try
            {
                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(path + name);
                if (urlFile != null)
                {
                    var dataByte = File.ReadAllBytes(urlFile);
                    var dataStream = new MemoryStream(dataByte);
                    return new FileResult(urlFile);
                }
                else
                {
                    throw new ApplicationException(string.Format("File tidak ditemukan."));
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        public class FileResult : IHttpActionResult
        {
            private readonly string filePath;
            private readonly string contentType;

            public FileResult(string filePath, string contentType = null)
            {
                this.filePath = filePath;
                this.contentType = contentType;
            }

            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                return Task.Run(() =>
                {
                    var contentType = this.contentType ?? MimeMapping.GetMimeMapping(Path.GetExtension(filePath));
                    var fileName = Path.GetFileName(filePath);
                    HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                    var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                    response.Content = new StreamContent(stream);
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = fileName
                    };
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                    return response;
                }, cancellationToken);
            }
        }
    }
}
