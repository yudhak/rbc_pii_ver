﻿using HR.Application;
using HR.Application.DTO;
using HR.Application.Params;
using HR.Common;
using HR.Domain;
using HR.Presentation.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Routing;

namespace HR.Presentation.WebAPI.Controllers
{
    public class RoleAccessFrontController : BaseAPIController
    {
        private readonly IRoleAccessFrontServices _roleAccessFrontServices;

        public RoleAccessFrontController(IRoleAccessFrontServices roleAccessFrontServices)
        {
            _roleAccessFrontServices = roleAccessFrontServices;
        }

        //GET api/roleAccessFront
        [HttpGet]
        public IHttpActionResult Get([FromUri] RoleAccessFrontListParameter param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string keyword = string.Empty;
                    int field = 0;

                    param.Validate();
                    keyword = param.Search;
                    if (param.SearchBy != "")
                    {
                        field = Convert.ToInt32(param.SearchBy);
                    }

                    int skip = 0;
                    if (param.PageNo > 0)
                    {
                        skip = (param.PageNo - 1) * param.PageSize;
                    }

                    int totalRows = _roleAccessFrontServices.GetAll(keyword, field).Count();
                    var totalPage = totalRows / param.PageSize;
                    var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
                    if (totalPages < 0)
                        totalPages = 0;

                    var result = _roleAccessFrontServices.GetAll(keyword, field)
                        .Skip(skip)
                        .Take(param.PageSize)
                        .ToList();

                    IList<RoleAccessFrontDTO > colls = RoleAccessFrontDTO.From(result);

                    PaginationDTO page = new PaginationDTO();
                    page.PageCount = totalPages;
                    page.PageNo = param.PageNo;
                    page.PageSize = param.PageSize;
                    page.results = colls;

                    return Ok(page);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //GET api/roleAccessFront/1
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = _roleAccessFrontServices.Get(id);
                RoleAccessFrontDTO roleAccessDTO = RoleAccessFrontDTO.From(result);
                return Ok(roleAccessDTO);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //POST api/roleAccessFront
        [HttpPost]
        public IHttpActionResult Add(RoleAccessFrontParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.CreateDate = DateHelper.GetDateTime();                    
                    param.CreateBy = Int32.Parse(base.UserId);
                    int id = _roleAccessFrontServices.Add(param);                    
                    return Ok(id);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //PUT api/roleAccessFront/id
        [HttpPut]
        public IHttpActionResult Update(int id, [FromBody]RoleAccessFrontParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.UpdateDate = DateHelper.GetDateTime();
                    param.UpdateBy = Int32.Parse(base.UserId);
                    int result = _roleAccessFrontServices.Update(id, param);
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //DELETE api/roleAccessFront/id
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId = 3;
                    DateTime getDate = DateHelper.GetDateTime();
                    int result = _roleAccessFrontServices.Delete(id, userId, getDate);
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }
    }
}
