﻿using HR.Application;
using HR.Application.DTO;
using HR.Application.Params;
using HR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace HR.Presentation.WebAPI.Controllers
{
    public class CorrelatedSektorDetailController : BaseAPIController
    {
        private readonly ICorrelatedSektorDetailService _correlatedSektorDetailService;
        private readonly IRiskRegistrasiService _riskRegistrasiService;
        public CorrelatedSektorDetailController(ICorrelatedSektorDetailService correlatedSektorDetailService, IRiskRegistrasiService riskRegistrasiService)
        {
            _correlatedSektorDetailService = correlatedSektorDetailService;
            _riskRegistrasiService = riskRegistrasiService;
        }

        //GET api/CorrelatedSektorDetail/1
        [HttpGet]
        public IHttpActionResult Get(int id) //id => correlatedRiskAntarSektorId
        {
            try
            {
                var result = _correlatedSektorDetailService.GetByCorrelatedSektorId(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //POST api/correlatedSektorDetail
        [HttpPost]
        public IHttpActionResult Add(CorrelatedSektorDetailCollectionParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.CreateDate = DateHelper.GetDateTime();
                    param.CreateBy = Int32.Parse(base.UserId);

                    int id = _correlatedSektorDetailService.Add(param);
                    return Ok(id);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null) { 
                  
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                }
                else { 
                    if (ex.Message == "Failure sending mail.")
                    {
                        //throw new ApplicationException(string.Format("Gagal mengirim email"));
                        return Content(HttpStatusCode.InternalServerError, "Gagal mengirim email");
                    }
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
                }
            }
        }
    }
}
