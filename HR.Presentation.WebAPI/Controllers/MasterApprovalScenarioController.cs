﻿using HR.Application;
using HR.Application.DTO;
using HR.Application.Params;
using HR.Common;
using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace HR.Presentation.WebAPI.Controllers
{
    public class MasterApprovalScenarioController : BaseAPIController
    {
        private readonly IMasterApprovalScenarioService _masterApprovalScenarioService;

        public MasterApprovalScenarioController(IMasterApprovalScenarioService masterApprovalScenarioService)
        {

            _masterApprovalScenarioService = masterApprovalScenarioService;
        }

        //GET api/MasterApprovalScenario
        [HttpGet]
        public IHttpActionResult Get([FromUri] MasterApprovalScenarioListParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {


                    IList<MasterApprovalScenario> masterApprovalScenario = _masterApprovalScenarioService.GetAll().ToList();
                    if (param.IsPagination())
                    {
                        string keyword = string.Empty;
                        string field = string.Empty;

                        param.Validate();
                        keyword = param.Search;
                        field = param.SearchBy;

                        int skip = 0;
                        if (param.PageNo > 0)
                        {
                            skip = (param.PageNo - 1) * param.PageSize;
                        }

                        int totalRows = _masterApprovalScenarioService.GetAll().Count();
                        var totalPage = totalRows / param.PageSize;
                        var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
                        if (totalPages < 0)
                            totalPages = 0;

                        var result = _masterApprovalScenarioService.GetAll()
                            .Skip(skip)
                            .Take(param.PageSize)
                            .ToList();

                        IList<MasterApprovalScenarioDTO> colls = MasterApprovalScenarioDTO.From(result);

                        PaginationDTO page = new PaginationDTO();
                        page.PageCount = totalPages;
                        page.PageNo = param.PageNo;
                        page.PageSize = param.PageSize;
                        page.results = colls;

                        return Ok(page);
                    }
                    else
                    {
                        IList<MasterApprovalScenarioDTO> dto = MasterApprovalScenarioDTO.From(masterApprovalScenario);
                        return Ok(dto);
                    }
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //GET api/MasterApprovalScenario/id
        //Get by menuId
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
        
            try
            {
                IList<MasterApprovalScenario> result = _masterApprovalScenarioService.GetAllByMenuId(id).ToList();
                IList<MasterApprovalScenarioDTO> masterApprovalScenarioDTO = MasterApprovalScenarioDTO.From(result);
                return Ok(masterApprovalScenarioDTO);
                //var result = _masterApprovalScenarioService.Get(id);
                //MasterApprovalScenarioDTO masterApprovalScenarioDTO = MasterApprovalScenarioDTO.From(result);
                //return Ok(masterApprovalScenarioDTO);


                //return Ok(stageDTO);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //POST api/MasterApprovalScenario
        [HttpPost]
        public IHttpActionResult Add(MasterApprovalScenarioParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.CreateDate = DateHelper.GetDateTime();
                    param.CreateBy = Int32.Parse(base.UserId);
                    //param.CreateBy = 1;

                    int id = _masterApprovalScenarioService.Add(param);
                    return Ok(id);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //PUT api/MasterApprovalScenario/id
        [HttpPut]
        public IHttpActionResult Update(int id, [FromBody]MasterApprovalScenarioParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.UpdateDate = DateHelper.GetDateTime();
                    param.UpdateBy = Int32.Parse(base.UserId);
                   // param.UpdateBy = 5;
                    int result = _masterApprovalScenarioService.Update(id, param);
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        // DELETE api/MasterApprovalScenario/id
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId = Int32.Parse(base.UserId);
                    DateTime getDate = DateHelper.GetDateTime();
                    int result = _masterApprovalScenarioService.DeleteByMenuId(id, userId, getDate);
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }
    }
}
