﻿using HR.Domain;
using HR.Application;
using HR.Application.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using HR.Application.DTO;
using System.Web.Http;

namespace HR.Presentation.WebAPI.Controllers
{
    public class CompareCalculationController : BaseAPIController
    {
        private readonly ICompareCalculationService _CompareCalculationService;
        private readonly IScenarioService _scenarioService;

        public CompareCalculationController(ICompareCalculationService CompareCalculationService, IScenarioService scenarioService)
        {
            _CompareCalculationService = CompareCalculationService;
            _scenarioService = scenarioService;
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            IList<Scenario> scenarios = _scenarioService.GetAll().Where(x => x.StatusId != 4).ToList();
            IList<ScenarioLightDTO> dto = ScenarioLightDTO.From(scenarios);
            return Ok(dto);
        }

        [HttpGet]
        public IHttpActionResult Get(bool IsPagination, bool IsApproved)
        {
            if (IsApproved)
            {
                IList<Scenario> scenarios = _scenarioService.GetAll().Where(x => x.StatusId == 2).ToList();
                IList<ScenarioLightDTO> dto = ScenarioLightDTO.From(scenarios);
                return Ok(dto);
            }
            else
            {
                IList<Scenario> scenarios = _scenarioService.GetAll().Where(x => x.StatusId != 4).ToList();
                IList<ScenarioLightDTO> dto = ScenarioLightDTO.From(scenarios);
                return Ok(dto);
            }
        }

        [HttpPost]
        public IHttpActionResult GetCompareCalculationPost(ScenarioCollection param) //id ==> scenarioId
        {
            try
            {
                var data = _CompareCalculationService.GetCompareCalculation(param);
                return Ok(data);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }
    }
}
