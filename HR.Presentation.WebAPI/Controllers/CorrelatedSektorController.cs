﻿using HR.Application;
using HR.Application.DTO;
using HR.Application.Params;
using HR.Common;
using HR.Domain;
using HR.Presentation.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Routing;

namespace HR.Presentation.WebAPI.Controllers
{
    public class CorrelatedSektorController : BaseAPIController
    {
        private readonly ICorrelatedSektorService _correlatedSektorService;
        private readonly ICorrelationMatrixService _correlationMatrixService;
        private readonly IRiskRegistrasiService _riskRegistrasiService;
        private readonly IScenarioService _scenarioService;
        private readonly IApprovalService _approvalService;

        public CorrelatedSektorController(ICorrelatedSektorService correlatedSektorService, ICorrelationMatrixService correlationMatrixService, 
            IRiskRegistrasiService riskRegistrasiService, IScenarioService scenarioService, IApprovalService approvalService)
        {
            _correlatedSektorService = correlatedSektorService;
            _correlationMatrixService = correlationMatrixService;
            _riskRegistrasiService = riskRegistrasiService;
            _scenarioService = scenarioService;
            _approvalService = approvalService;
        }

        //GET api/correlatedSector
        [HttpGet]
        public IHttpActionResult Get([FromUri] CorrelatedSektorListParameter param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string keyword = string.Empty;
                    int field = 0;
                    int user = Int32.Parse(base.UserId);
                    int field2 = 0;
                    param.Validate();
                    keyword = param.Search;
                    //field = param.SearchBy;
                    if (param.SearchBy != "")
                    {
                        field = Convert.ToInt32(param.SearchBy);
                    }
                    if (param.SearchBy2 != "")
                    {
                        field2 = Convert.ToInt32(param.SearchBy2);
                    }
                    int skip = 0;
                    if (param.PageNo > 0)
                    {
                        skip = (param.PageNo - 1) * param.PageSize;
                    }

                    var scneario = _scenarioService.GetDefault();
                    int totalRows = _correlatedSektorService.GetByScenarioDefaultId(keyword, field, field2, user).Count();
                    var totalPage = totalRows / param.PageSize;
                    var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
                    if (totalPages < 0)
                        totalPages = 0;

                    var result = _correlatedSektorService.GetByScenarioDefaultId(keyword, field, field2, user)
                        .Skip(skip)
                        .Take(param.PageSize)
                        .ToList();

                    var riskRegistrasi = _riskRegistrasiService.GetAll().ToList();
                    var correlationMatrix = _correlationMatrixService.GetAll().ToList();
                    var scenarioAll = _scenarioService.GetAll().ToList();
                    IList<CorrelatedSektorDTO> colls = CorrelatedSektorDTO.From(result, riskRegistrasi, correlationMatrix, scenarioAll);

                    foreach (var item in colls)
                    {
                        if (item.StatusId == 1)
                        {
                            item.StatusUserApproval = _approvalService.GetByActive(item.Id, item.CreateBy.GetValueOrDefault(), "CorrelatedSektor");
                        }
                    }

                    PaginationDTO page = new PaginationDTO();
                    page.PageCount = totalPages;
                    page.PageNo = param.PageNo;
                    page.PageSize = param.PageSize;
                    page.results = colls;

                    return Ok(page);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //GET api/correlatedSector/1
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var riskRegistrasi = _riskRegistrasiService.GetAll().ToList();
                var correlationMatrix = _correlationMatrixService.GetAllAscNilai().ToList();
                var result = _correlatedSektorService.Get(id);
                CorrelatedSektorDTO correlatedSectorDTO = CorrelatedSektorDTO.From(result, riskRegistrasi, correlationMatrix);
                var scenario = _scenarioService.Get(correlatedSectorDTO.ScenarioId);
                correlatedSectorDTO.NamaScenario = scenario.NamaScenario;
                return Ok(correlatedSectorDTO);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //POST api/correlatedSector
        [HttpPost]
        public IHttpActionResult Add(CorrelatedSektorParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.CreateDate = DateHelper.GetDateTime();
                    param.CreateBy = Int32.Parse(base.UserId);

                    int id = _correlatedSektorService.Add(param);
                    return Ok(id);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //PUT api/correlatedSector/id
        [HttpPut]
        public IHttpActionResult Update(int id, [FromBody]CorrelatedSektorParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.UpdateDate = DateHelper.GetDateTime();
                    param.UpdateBy = Int32.Parse(base.UserId);

                    int result = _correlatedSektorService.Update(id, param);
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //DELETE api/correlatedSector/id
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId = Int32.Parse(base.UserId);
                    DateTime getDate = DateHelper.GetDateTime();
                    int result = _correlatedSektorService.Delete(id, userId, getDate);
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }
    }
}
