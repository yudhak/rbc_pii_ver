﻿using HR.Application;
using HR.Application.DTO;
using HR.Application.Params;
using HR.Common;
using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace HR.Presentation.WebAPI.Controllers
{
    public class ScenarioController : BaseAPIController
    {
        private readonly IScenarioService _scenarioService;
        private readonly IApprovalService _approvalService;

        public ScenarioController(IScenarioService scenarioService, IApprovalService approvalService)
        {
            _scenarioService = scenarioService;
            _approvalService = approvalService;
        }

        //GET api/scenario
        [HttpGet]
        public IHttpActionResult Get([FromUri] ScenarioListParameter param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IList<Scenario> scenarios = _scenarioService.GetAll().Where(x => x.StatusId != 4).ToList();
                    if (param.IsPagination())
                    {
                        string keyword = string.Empty;
                        int field = 0;
                        int field2 = 0;
                        param.Validate();
                        keyword = param.Search;
                        if (param.SearchBy != "")
                        {
                            field = Convert.ToInt32(param.SearchBy);
                        }
                        if (param.SearchBy2 != "")
                        {
                            field2 = Convert.ToInt32(param.SearchBy2);
                        }
                        int skip = 0;
                        if (param.PageNo > 0)
                        {
                            skip = (param.PageNo - 1) * param.PageSize;
                        }

                        int totalRows = _scenarioService.GetAll(keyword, field, field2, Int32.Parse(base.UserId)).Where(x => x.StatusId != 4).Count();
                        var totalPage = totalRows / param.PageSize;
                        var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
                        if (totalPages < 0)
                            totalPages = 0;

                        var result = _scenarioService.GetAll(keyword, field, field2, Int32.Parse(base.UserId)).Where(x => x.StatusId != 4)
                            .Skip(skip)
                            .Take(param.PageSize)
                            .ToList();

                        IList<ScenarioLightDTO> colls = ScenarioLightDTO.From(result);

                        foreach (var item in colls)
                        {
                            if (item.StatusId == 1)
                            {
                                if (item.StatusUserApproval == null)
                                {
                                    item.StatusUserApproval = _approvalService.GetByActive(item.Id, item.CreateBy.GetValueOrDefault(), "Scenario");
                                    if (item.StatusUserApproval == null)
                                    {
                                        item.StatusId = null;
                                    }
                                }
                            }
                        }

                        PaginationDTO page = new PaginationDTO();
                        page.PageCount = totalPages;
                        page.PageNo = param.PageNo;
                        page.PageSize = param.PageSize;
                        page.results = colls;

                        return Ok(page);
                    }
                    else
                    {
                        IList<ScenarioLightDTO> dto = ScenarioLightDTO.From(scenarios);
                        return Ok(dto);
                    }
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //GET api/scenario/id
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = _scenarioService.Get(id);
                ScenarioDTO scenarioDTO = ScenarioDTO.From(result);
                return Ok(scenarioDTO);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult Get(int id, int id2)
        {
            try
            {
                var result = _scenarioService.GetDefault();
                ScenarioDTO scenarioDTO = ScenarioDTO.From(result);
                return Ok(scenarioDTO);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }


        [HttpGet]
        public IHttpActionResult Get(int id, bool used)
        {
            try
            {
                var result = _scenarioService.GetWithProject(id);
                ScenarioDTO scenarioDTO = ScenarioDTO.From(result);
                List<ScenarioDetailDTO> list = new List<ScenarioDetailDTO>();
               
                return Ok(scenarioDTO);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //POST api/scenario
        [HttpPost]
        public IHttpActionResult Add(ScenarioParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.CreateDate = DateHelper.GetDateTime();
                    param.CreateBy = Int32.Parse(base.UserId);

                    int id = _scenarioService.Add(param);
                    return Ok(id);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                if (ex.Message == "Failure sending mail.")
                {
                    //throw new ApplicationException(string.Format("Gagal mengirim email"));
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
                }
                return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //PUT api/scenario/id
        [HttpPut]
        public IHttpActionResult Update(int id, [FromBody]ScenarioParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.UpdateDate = DateHelper.GetDateTime();
                    param.UpdateBy = Int32.Parse(base.UserId);

                    int result;
                    if (param.IsDefault == true)
                    {
                        result = _scenarioService.SetDefault(id, param.UpdateBy, param.UpdateDate);
                    }
                    else if (param.IsUpdate == true)
                    {
                        result = _scenarioService.Update(id, param);
                    }
                    else
                    {
                        result = _scenarioService.Update(id, param);
                    }
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                if (ex.Message == "Failure sending mail.")
                {
                    //throw new ApplicationException(string.Format("Gagal mengirim email"));
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
                }
                return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //DELETE api/scenario/id
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId = Int32.Parse(base.UserId);
                    DateTime getDate = DateHelper.GetDateTime();
                    int result = _scenarioService.Delete(id, userId, getDate);
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }
    }
}
