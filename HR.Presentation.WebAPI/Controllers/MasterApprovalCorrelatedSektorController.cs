﻿using HR.Application;
using HR.Application.DTO;
using HR.Application.Params;
using HR.Common;
using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace HR.Presentation.WebAPI.Controllers
{
    public class MasterApprovalCorrelatedSektorController : BaseAPIController
    {
        private readonly IMasterApprovalCorrelatedSektorService _masterApprovalCorrelatedSektorService;
        private readonly ICorrelatedSektorService _correlatedSektorService;
        private readonly ICorrelationMatrixService _correlationMatrixService;
        private readonly IRiskRegistrasiService _riskRegistrasiService;
        private readonly IScenarioService _scenarioService;

        public MasterApprovalCorrelatedSektorController(IMasterApprovalCorrelatedSektorService masterApprovalCorrelatedSektorService, ICorrelatedSektorService correlatedSektorService
            , ICorrelationMatrixService correlationMatrixService, IRiskRegistrasiService riskRegistrasiService, IScenarioService scenarioService)
        {
            _masterApprovalCorrelatedSektorService = masterApprovalCorrelatedSektorService;
            _correlatedSektorService = correlatedSektorService;
            _correlationMatrixService = correlationMatrixService;
            _riskRegistrasiService = riskRegistrasiService;
            _scenarioService = scenarioService;
        }

        //GET api/MasterApprovalCorrelatedSektor
        [HttpGet]
        public IHttpActionResult Get([FromUri] CorrelatedSektorListParameter param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string keyword = string.Empty;
                    string field = string.Empty;

                    param.Validate();
                    keyword = param.Search;
                    field = param.SearchBy;

                    int skip = 0;
                    if (param.PageNo > 0)
                    {
                        skip = (param.PageNo - 1) * param.PageSize;
                    }

                    var scneario = _scenarioService.GetDefault();
                    int totalRows = _correlatedSektorService.GetByScenarioDefaultId(null).Count();
                    var totalPage = totalRows / param.PageSize;
                    var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
                    if (totalPages < 0)
                        totalPages = 0;

                    var result = _correlatedSektorService.GetByScenarioDefaultId(null)
                        .Skip(skip)
                        .Take(param.PageSize)
                        .ToList();

                    var riskRegistrasi = _riskRegistrasiService.GetAll().ToList();
                    var correlationMatrix = _correlationMatrixService.GetAll().ToList();
                    IList<CorrelatedSektorDTO> colls = CorrelatedSektorDTO.From(result, riskRegistrasi, correlationMatrix);

                    PaginationDTO page = new PaginationDTO();
                    page.PageCount = totalPages;
                    page.PageNo = param.PageNo;
                    page.PageSize = param.PageSize;
                    page.results = colls;

                    return Ok(page);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //GET api/MasterApprovalCorrelatedSektor
        //[HttpGet]
        //public IHttpActionResult Get(int menuId,[FromUri] MasterApprovalCorrelatedSektorListParam param)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            IList<CorrelatedSektor> correlatedSektor = _correlatedSektorService.GetByScenarioDefaultId(null).ToList();
        //            var riskRegistrasi = _riskRegistrasiService.GetAll().ToList();
        //            var correlationMatrix = _correlationMatrixService.GetAll().ToList();
        //            IList<MasterApprovalCorrelatedSektor> masterApprovalCorrelatedSektor = _masterApprovalCorrelatedSektorService.GetAll().ToList();
        //            if (param.IsPagination())
        //            {
        //                string keyword = string.Empty;
        //                string field = string.Empty;

        //                param.Validate();
        //                keyword = param.Search;
        //                field = param.SearchBy;

        //                int skip = 0;
        //                if (param.PageNo > 0)
        //                {
        //                    skip = (param.PageNo - 1) * param.PageSize;
        //                }

        //                int totalRows = _correlatedSektorService.GetByScenarioDefaultId(null).Count();
        //                var totalPage = totalRows / param.PageSize;
        //                var totalPages = (int)Math.Ceiling((double)totalRows / param.PageSize);
        //                if (totalPages < 0)
        //                    totalPages = 0;

        //                var result = _correlatedSektorService.GetByScenarioDefaultId(null)
        //                    .Skip(skip)
        //                    .Take(param.PageSize)
        //                    .ToList();

                        
        //                IList<CorrelatedSektorDTO> colls = CorrelatedSektorDTO.From(result, riskRegistrasi, correlationMatrix);

        //                PaginationDTO page = new PaginationDTO();
        //                page.PageCount = totalPages;
        //                page.PageNo = param.PageNo;
        //                page.PageSize = param.PageSize;
        //                page.results = colls;

        //                return Ok(page);
        //            }
        //            else
        //            {
        //                IList<CorrelatedSektorDTO> dto = CorrelatedSektorDTO.From(correlatedSektor, riskRegistrasi, correlationMatrix);
        //                return Ok(dto);
        //            }
        //        }
        //        else
        //        {
        //            string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
        //            return Content(HttpStatusCode.BadRequest, errorResult);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.InnerException == null)
        //            return Content(HttpStatusCode.InternalServerError, ex.Message);
        //        else
        //            return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
        //    }
        //}


        //GET api/MasterApprovalCorrelatedSektor/id
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
        
            try
            {
                IList<MasterApprovalCorrelatedSektor> result = _masterApprovalCorrelatedSektorService.GetAllByMenuId(id).ToList();
                IList<MasterApprovalCorrelatedSektorDTO> masterApprovalScenarioDTO = MasterApprovalCorrelatedSektorDTO.From(result);
                return Ok(masterApprovalScenarioDTO);
                
               
                //return Ok(stageDTO);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //POST api/MasterApprovalCorrelatedSektor
        [HttpPost]
        public IHttpActionResult Add(MasterApprovalCorrelatedSektorParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.CreateDate = DateHelper.GetDateTime();
                    param.CreateBy = Int32.Parse(base.UserId);

                    int id = _masterApprovalCorrelatedSektorService.Add(param);
                    return Ok(id);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        //PUT api/MasterApprovalCorrelatedSektor/id
        [HttpPut]
        public IHttpActionResult Update(int id, [FromBody]MasterApprovalCorrelatedSektorParam param)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    param.UpdateDate = DateHelper.GetDateTime();
                    param.UpdateBy = Int32.Parse(base.UserId);

                    int result = _masterApprovalCorrelatedSektorService.Update(id, param);
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }

        // DELETE api/MasterApprovalCorrelatedSektor/id
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId = Int32.Parse(base.UserId);
                    DateTime getDate = DateHelper.GetDateTime();
                    int result = _masterApprovalCorrelatedSektorService.DeleteByMenuId(id, userId, getDate);
                    return Ok(result);
                }
                else
                {
                    string errorResult = string.Join(" ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                    return Content(HttpStatusCode.BadRequest, errorResult);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, ex.InnerException.Message);
            }
        }
    }
}
