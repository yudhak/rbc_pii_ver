﻿using HR.Application;
using HR.Infrastructure;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Web.Http;

[assembly: OwinStartup(typeof(HR.Presentation.WebAPI.Startup))]

namespace HR.Presentation.WebAPI
{
    public class Startup
    {
        public static string PublicClientId { get; private set; }
        public static IMenuService MenuService { get; private set; }
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.CreatePerOwinContext(DatabaseContext.Create);

            PublicClientId = "self";
            OAuthAuthorizationServerOptions options = new OAuthAuthorizationServerOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new OAuthProvider(PublicClientId, MenuService)
            };
            app.UseOAuthAuthorizationServer(options);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

            app.UseCookieAuthentication(new CookieAuthenticationOptions());

            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);
        }
    }
}