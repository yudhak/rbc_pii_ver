﻿using System;

namespace HR.Common
{
    public class CurrencyHelper
    {
        public static double toDouble(decimal? data)
        {
            try
            {
                var toDouble = (double)data;
                var getTwoDigit = System.Math.Round(toDouble, 2);

                return getTwoDigit;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw new Exception(error);
            }
        }

        public static decimal toDecimal(double data)
        {
            try
            {
                var toDecimal = System.Convert.ToDecimal(data);
                return toDecimal;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw new Exception(error);
            }
        }
    }
}
