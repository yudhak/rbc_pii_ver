﻿using HR.Core;
using System;
using System.Collections.Generic;

namespace HR.Domain
{
    public class RiskRegistrasi : Entity
    {
        public string KodeMRisk { get; private set; }
        public string NamaCategoryRisk { get; private set; }
        public string Definisi { get; private set; }
        public decimal? Minimum { get; private set; }
        public decimal? Maximum { get; private set; }
        public int? CreateBy { get; private set; }
        public DateTime? CreateDate { get; private set; }
        public int? UpdateBy { get; private set; }
        public DateTime? UpdateDate { get; private set; }
        public bool? IsDelete { get; private set; }
        public DateTime? DeleteDate { get; private set; }

        //Navigation properties
        public virtual IList<SubRiskRegistrasi> SubRiskRegistrasi { get; set; }

        public RiskRegistrasi()
        {
            this.SubRiskRegistrasi = new List<SubRiskRegistrasi>();
        }

        public RiskRegistrasi(string kodeMRisk, string namaCategoryRisk, string definisi, decimal? maximum, decimal? minimum, int? createBy, DateTime? createDate)
        {
            this.KodeMRisk = kodeMRisk;
            this.NamaCategoryRisk = namaCategoryRisk;
            this.Definisi = definisi;
            this.Minimum = minimum;
            this.Maximum = maximum;
            this.CreateBy = createBy;
            this.CreateDate = createDate;
            this.IsDelete = false;
        }

        public virtual void Update(string kodeMRisk, string namaCategoryRisk, string definisi,decimal? maximum, decimal? minimum, int? updateBy, DateTime? updateDate)
        {
            this.KodeMRisk = kodeMRisk;
            this.NamaCategoryRisk = namaCategoryRisk;
            this.Definisi = definisi;
            this.Minimum = minimum;
            this.Maximum = maximum;
            this.UpdateBy = updateBy;
            this.UpdateDate = updateDate;
        }

        public virtual void Delete(int? deleteBy, DateTime? deleteDate)
        {
            this.IsDelete = true;
            this.DeleteDate = deleteDate;
        }

        public virtual void AddRiskRegistrasiDetail(SubRiskRegistrasi riskRegistrasiDetail)
        {
            this.SubRiskRegistrasi.Add(riskRegistrasiDetail);
        }

        public virtual void RemoveRiskRegistrasiDetail(SubRiskRegistrasi riskRegistrasiDetail)
        {
            this.SubRiskRegistrasi.Remove(riskRegistrasiDetail);
        }
    }

    public class RiskRegistrasiLite
    {
        public int RiskRegistrasiId { get; set; }
        public string KodeMRisk { get; set; }
        public string NamaCategoryRisk { get; set; }
        public string Definisi { get; set; }
        public decimal? Minimum { get; set; }
        public decimal? Maximum { get; set; }
        public int? CreateBy { get; private set; }
        public DateTime? CreateDate { get; private set; }
        public int? UpdateBy { get; private set; }
        public DateTime? UpdateDate { get; private set; }
        public bool? IsDelete { get; private set; }
        public DateTime? DeleteDate { get; private set; }

        public RiskRegistrasiLite() { }
    }
}
