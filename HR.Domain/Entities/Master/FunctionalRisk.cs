﻿using HR.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public class FunctionalRisk : Entity
    {
        public string Definisi { get; private set; }
        public int? CreateBy { get; private set; }
        public DateTime? CreateDate { get; private set; }
        public int? UpdateBy { get; private set; }
        public DateTime? UpdateDate { get; private set; }
        public bool? IsDelete { get; private set; }
        public DateTime? DeleteDate { get; private set; }
        public decimal NilaiMaksimum { get; private set; }
        public decimal NilaiMinimum { get; private set; }
        public string Komentar { get; private set; }

        //Foreign Key
        public int? MatrixId { get; private set; }
        public int? ScenarioId { get; private set; }
        public int? ColorCommentId { get; private set; }

        //Navigation
        public virtual Matrix Matrix { get; set; }
        public virtual ColorComment ColorComment { get; set; }
        public virtual Scenario Scenario { get; set; }


        public FunctionalRisk()
        {

        }

        public FunctionalRisk(Matrix Matrix, ColorComment ColorComment, Scenario Scenario, string definisi, int? createBy, DateTime? createDate, decimal nilaiMaksimum, decimal nilaiMinimum, string komentar)
        {
            this.MatrixId = Matrix.Id;
            this.ColorCommentId = ColorComment.Id;
            this.ScenarioId = Scenario.Id;
            this.Definisi = definisi;
            this.CreateBy = createBy;
            this.CreateDate = createDate;
            this.IsDelete = false;
            this.NilaiMaksimum = nilaiMaksimum;
            this.NilaiMinimum = NilaiMinimum;
            this.Komentar = komentar;
        }

        public virtual void Update(Matrix Matrix, ColorComment ColorComment, Scenario Scenario, string definisi, int? updateBy, DateTime? updateDate, decimal nilaiMaksimum, decimal nilaiMinimum, string komentar)
        {
            this.MatrixId = Matrix.Id;
            this.ColorCommentId = ColorComment.Id;
            this.ScenarioId = Scenario.Id;
            this.Definisi = definisi;
            this.UpdateBy = updateBy;
            this.UpdateDate = updateDate;
            this.NilaiMaksimum = nilaiMaksimum;
            this.NilaiMinimum = nilaiMinimum;
            this.Komentar = komentar;
        }

        public virtual void Delete(int? deleteBy, DateTime? deleteDate)
        {
            this.IsDelete = true;
            this.DeleteDate = deleteDate;
        }

        public virtual void UnDelete()
        {
            this.IsDelete = false;
        }
    }
}
