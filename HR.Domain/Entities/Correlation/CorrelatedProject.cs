﻿using HR.Core;
using System;

namespace HR.Domain
{
    public class CorrelatedProject : Entity
    {
        public int? CreateBy { get; private set; }
        public DateTime? CreateDate { get; private set; }
        public int? UpdateBy { get; private set; }
        public DateTime? UpdateDate { get; private set; }
        public bool? IsDelete { get; private set; }
        public DateTime? DeleteDate { get; private set; }
        public bool? IsApproved { get; private set; }

        //Foreign Key
        public int ScenarioId { get; set; }
        public int ProjectId { get; private set; }
        public int SektorId { get; private set; }
        public int? StatusId { get; private set; }

        public virtual Status Status { get; set; }
        public virtual Project Project { get; set; }
        public virtual Sektor Sektor { get; set; }
        //public virtual Scenario Scenario { get; set; }
        public CorrelatedProject()
        { }

        public CorrelatedProject(Scenario scenario, int projectId, int sektorId, int? createBy, DateTime? createDate, Status status)
        {
            this.ScenarioId = scenario.Id;
            this.ProjectId = projectId;
            this.SektorId = sektorId;
            this.CreateBy = createBy;
            this.CreateDate = createDate;
            this.IsDelete = false;
            this.IsApproved = false;
            if (status != null)
            {
                this.StatusId = status.Id;
            }
        }

        public CorrelatedProject(Scenario scenario, int projectId, int sektorId, int? createBy, DateTime? createDate)
        {
            this.ScenarioId = scenario.Id;
            this.ProjectId = projectId;
            this.SektorId = sektorId;
            this.CreateBy = createBy;
            this.CreateDate = createDate;
            this.IsDelete = false;
            this.IsApproved = false;
        }


        public virtual void Update(Scenario scenario, int? updateBy, DateTime? updateDate)
        {
            this.ScenarioId = scenario.Id;
            this.UpdateBy = updateBy;
            this.UpdateDate = updateDate;
        }

        public virtual void UpdateStatus(Status status, int? updateBy, DateTime? updateDate)
        {
            if (status != null)
            {
                this.StatusId = status.Id;
            }
            else
            {
                this.StatusId = null;
            }

            this.UpdateBy = updateBy;
            this.UpdateDate = updateDate;
        }


        public virtual void Delete(int? updateBy, DateTime? updateDate)
        {
            this.IsDelete = true;
            this.UpdateBy = updateBy;
            this.DeleteDate = updateDate;
        }

        public virtual void RemoveDelete(int? updateBy, DateTime? updateDate)
        {
            this.IsDelete = false;
            this.UpdateBy = updateBy;
            this.DeleteDate = updateDate;
        }


        public virtual void UpdateStatusReject()
        {
            this.StatusId = 3;
        }

        public virtual void SetIsApproved(int? updateBy, DateTime? updateDate)
        {
            this.IsApproved = true;
            this.UpdateBy = updateBy;
            this.DeleteDate = updateDate;
        }

        public virtual void RemoveIsApproved(int? updateBy, DateTime? updateDate)
        {
            this.IsApproved = false;
            this.UpdateBy = updateBy;
            this.DeleteDate = updateDate;
        }

        public virtual void SetNullStatus(int? updateBy, DateTime? updateDate)
        {
            this.StatusId = null;
            this.UpdateBy = updateBy;
            this.UpdateDate = updateDate;
        }
    }
}
