﻿using HR.Core;
using System;

namespace HR.Domain
{
    public class CorrelatedSektor : Entity
    {
        public string NamaSektor { get; private set; }
        public int? CreateBy { get; private set; }
        public DateTime? CreateDate { get; private set; }
        public int? UpdateBy { get; private set; }
        public DateTime? UpdateDate { get; private set; }
        public bool? IsDelete { get; private set; }
        public DateTime? DeleteDate { get; private set; }

        //Foreign Key
        public int ScenarioId { get; set; }
        public int SektorId { get; private set; }
        public int? StatusId { get; private set; }

        public virtual Status Status { get; set; }
       // public virtual Scenario Scenario { get; set; }
        public CorrelatedSektor()
        { }

        public CorrelatedSektor(int sektorId, string namaSektor, Scenario scenario, int? createBy, DateTime? createDate, Status status)
        {
            this.SektorId = sektorId;
            this.NamaSektor = namaSektor;
            this.ScenarioId = scenario.Id;
            this.CreateBy = createBy;
            this.CreateDate = createDate;
            this.IsDelete = false;
            if (status != null)
            {
                this.StatusId = status.Id;
            }
        }

        public CorrelatedSektor(int sektorId, string namaSektor, Scenario scenario, int? createBy, DateTime? createDate)
        {
            this.SektorId = sektorId;
            this.NamaSektor = namaSektor;
            this.ScenarioId = scenario.Id;
            this.CreateBy = createBy;
            this.CreateDate = createDate;
            this.IsDelete = false;
        }

        public virtual void Update(Scenario scenario, int? updateBy, DateTime? updateDate)
        {
            this.ScenarioId = scenario.Id;
            this.UpdateBy = updateBy;
            this.UpdateDate = updateDate;
        }

        public virtual void Delete(int deleteBy, DateTime deleteDate)
        {
            this.IsDelete = true;
            this.UpdateBy = deleteBy;
            this.DeleteDate = deleteDate;
        }

        public virtual void RemoveDelete(int deleteBy, DateTime deleteDate)
        {
            this.IsDelete = false;
            this.UpdateBy = deleteBy;
            this.DeleteDate = deleteDate;
        }

        public virtual void UpdateStatus(Status status, int? updateBy, DateTime? updateDate)
        {
            if (status != null)
            {
                this.StatusId = status.Id;
            }
            else
            {
                this.StatusId = null;
            }
            this.UpdateBy = updateBy;
            this.UpdateDate = updateDate;
        }

        public virtual void UpdateStatusTanpaUpdateBy(Status status)
        {
            if (status != null)
            {
                this.StatusId = status.Id;
            }
            else
            {
                this.StatusId = null;
            }
        }

        public virtual void UpdateStatusReject()
        {
            this.StatusId = 3;
        }

        public virtual void SetNullStatus(int? updateBy, DateTime? updateDate)
        {
            this.StatusId = null;
            this.UpdateBy = updateBy;
            this.UpdateDate = updateDate;
        }
    }
}
