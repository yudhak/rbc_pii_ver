﻿using HR.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HR.Domain
{
    public class AuditLog : Entity
    {
        public int? MenuId { get; private set; }
        public string TableModified { get; private set; }
        public int? DataObjekId { get; private set; }
        public string ColumnModified  { get; private set; }
        public string DataAwal { get; private set; }
        public string DataAkhir { get; private set; }
        public DateTime LogTimestamp { get; private set; }
        public int? ModifiedBy { get; private set; }
        
        public AuditLog()
        { }

        public AuditLog(int? menuId, string tableModified, int? dataObjekId, string columnModified, string dataAwal, string dataAkhir, DateTime logTimestamp, int? modifiedBy)
        {
            this.MenuId = menuId;
            this.TableModified = tableModified;
            this.DataObjekId = dataObjekId;
            this.ColumnModified = columnModified;
            this.DataAwal = dataAwal;
            this.DataAkhir = dataAkhir;
            this.LogTimestamp = logTimestamp;
            this.ModifiedBy = modifiedBy;
        }

        public virtual void Update()
        { }

        public virtual void Delete()
        { }
    }
}
