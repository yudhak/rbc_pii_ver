﻿using HR.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HR.Domain
{
    public class Approval : Entity
    {
        public string StatusApproval { get;  set; }
        public string SourceApproval { get;  set; }      
        public int? CreateBy { get;  set; }
        public DateTime? CreateDate { get;  set; }
        public int? UpdateBy { get;  set; }
        public DateTime? UpdateDate { get;  set; }
        public bool? IsDelete { get;  set; }
        public DateTime? DeleteDate { get;  set; }
        public string Keterangan { get;  set; }
        public bool IsActive { get;  set; }

        //Foreign Key
        public int StatusId { get;  set; }
        public int RequestId { get;  set; }
        public int NomorUrutStatus { get;  set; }

        //public virtual Scenario Scenario { get; set; }
        //public virtual RiskMatrixProject RiskMatrixProject { get; set; }


        //Navigation Properties
        public virtual Status Status { get; set; }
       // public virtual Role Role { get; set; }


        public Approval()
        {
            
        }

        public Approval(int requestId, string statusApproval, string sourceApproval, Status status, int? createBy,
            DateTime? createDate,  string keterangan, int nomorUrutStatus)
        {
            this.StatusApproval = statusApproval;
            this.SourceApproval = sourceApproval;
            this.Keterangan = keterangan;
            this.CreateDate = createDate;
            this.CreateBy = createBy;
            this.IsDelete = false;
            this.StatusId = status.Id;
            this.RequestId = requestId;
            this.IsActive = true;
            this.NomorUrutStatus = nomorUrutStatus;
        }

        public virtual void Update(int requestId, string statusApproval, string sourceApproval, 
            int userId, Status status, int? updateBy, DateTime? updateDate, string keterangan, int nomorUrutStatus)
        {
            this.RequestId = requestId;
            this.StatusApproval = statusApproval;
            this.SourceApproval = sourceApproval;
            this.StatusId = status.Id;
            this.UpdateBy = updateBy;
            this.UpdateDate = updateDate;
            this.Keterangan = keterangan;
            this.NomorUrutStatus = nomorUrutStatus;
        }

        public virtual void Delete(int deleteBy, DateTime deleteDate)
        {
            this.IsDelete = true;
            this.UpdateBy = deleteBy;
            this.DeleteDate = deleteDate;
            this.IsActive = false;
        }

        public virtual void RemoveActive(int? updateBy, DateTime? updateDate)
        {
            this.IsActive = false;
            this.UpdateBy = updateBy;
            this.UpdateDate = updateDate;
        }


        public virtual void UpdateRequestId(int requesId, int? updateBy, DateTime? updateDate)
        {
            this.RequestId = requesId;
            this.UpdateBy = updateBy;
            this.UpdateDate = updateDate;
        }

        public virtual void SetIsActive()
        {
            this.IsActive = true;
        }

        public virtual void UpdateKeterangan(string keterangan)
        {
            this.Keterangan = keterangan;
        }
    }
}
