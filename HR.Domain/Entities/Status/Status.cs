﻿using HR.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HR.Domain
{
    public class Status : Entity
    {
       
        public string StatusDescription { get; private set; }

        public Status()
        { }

        public Status( string statusDescription)
        {
           
            this.StatusDescription = statusDescription;
        }


    }

}
