﻿using HR.Core;
using System;

namespace HR.Domain
{
    public class ApprovalExtend
    {
        public int Id { get; set; }
        public string NamaScenario { get; set; }
        public string StatusApproval { get;  set; }
        public string SourceApproval { get;  set; }
        public int? CreateBy { get;  set; }
        public DateTime? CreateDate { get;  set; }
        public int? UpdateBy { get;  set; }
        public DateTime? UpdateDate { get;  set; }
        public bool? IsDelete { get;  set; }
        public DateTime? DeleteDate { get;  set; }
        public string Keterangan { get;  set; }
        public bool IsActive { get;  set; }
        public int StatusId { get;  set; }
        public int RequestId { get;  set; }
        public int NomorUrutStatus { get;  set; }
        public string NamaEntitas { get; set; }
        public StatusExtend Status { get; set; }
        public ScenarioExtend Scenario { get; set; }
        public RiskMatrixProjectExtend RiskMatrixProject { get; set; }
        public CorrelatedSektorExtend CorrelatedSektor { get; set; }
        public CorrelatedProjectExtend CorrelatedProject { get; set; }
        public UserExtend User { get; set; }
        public UserExtend UserApprove { get; set; }
        public ApprovalExtend()
        {

        }
    }

    public class StatusExtend
    {
        public string StatusDescription { get; set; }
        public StatusExtend() { }
    }
    public class ScenarioExtend
    {
        public string NamaScenario { get; set; }
        public bool IsDefault { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DeleteDate { get; set; }

        public int LikehoodId { get; set; }
        public int? StatusId { get; set; }
        public ScenarioExtend() { }
    }
    public class RiskMatrixProjectExtend
    {
        public int? CreateBy { get;  set; }
        public DateTime? CreateDate { get;  set; }
        public int? UpdateBy { get;  set; }
        public DateTime? UpdateDate { get;  set; }
        public bool? IsDelete { get;  set; }
        public DateTime? DeleteDate { get;  set; }
        public int ProjectId { get;  set; }
        public int RiskMatrixId { get; set; }
        public int ScenarioId { get; set; }
        public int? StatusId { get;  set; }
        public RiskMatrixProjectExtend() { }
    }
    public class CorrelatedSektorExtend
    {
        public string NamaSektor { get;  set; }
        public int? CreateBy { get;  set; }
        public DateTime? CreateDate { get;  set; }
        public int? UpdateBy { get;  set; }
        public DateTime? UpdateDate { get;  set; }
        public bool? IsDelete { get;  set; }
        public DateTime? DeleteDate { get;  set; }
        public int ScenarioId { get; set; }
        public int SektorId { get;  set; }
        public int? StatusId { get;  set; }
        public CorrelatedSektorExtend() { }
    }
    public class CorrelatedProjectExtend
    {
        public int ScenarioId { get; set; }
        public int ProjectId { get;  set; }
        public string ProjectName { get; set; }
        public int SektorId { get;  set; }
        public int? StatusId { get;  set; }
        public int? CreateBy { get;  set; }
        public DateTime? CreateDate { get;  set; }
        public int? UpdateBy { get;  set; }
        public DateTime? UpdateDate { get;  set; }
        public bool? IsDelete { get;  set; }
        public DateTime? DeleteDate { get;  set; }
        public bool? IsApproved { get;  set; }
        
        public CorrelatedProjectExtend() { }
    }

    public class UserExtend
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }

        public UserExtend() { }
    }

    public class ProjectExtend
    {
        public int Id { get; set; }
        public string NamaProject { get; set; }

        public ProjectExtend() { }
    }

    public class SektorExtend
    {
        public int Id { get; set; }
        public string NamaSektor { get; set; }

        public SektorExtend() { }
    }
}
