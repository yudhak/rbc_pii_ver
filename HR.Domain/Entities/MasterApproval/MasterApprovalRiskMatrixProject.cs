﻿using HR.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HR.Domain
{
    public class MasterApprovalRiskMatrixProject : Entity
    {
        public int? MenuId { get; private set; }
        public int? ProjectId { get; private set; }
        public int? UserId { get; private set; }
        public int? NomorUrutStatus { get; private set; }
        public int? CreateBy { get; private set; }
        public DateTime? CreateDate { get; private set; }
        public int? UpdateBy { get; private set; }
        public DateTime? UpdateDate { get; private set; }
        public bool? IsDelete { get; private set; }
        public DateTime? DeleteDate { get; private set; }

        //Navigation Properties
        public virtual User User{ get; set; }
        public virtual Menu Menu { get; set; }
        public virtual Project Project { get; set; }

        public MasterApprovalRiskMatrixProject()
        {

        }

        public MasterApprovalRiskMatrixProject(Menu menu, Project project, User user, int? nomorUrutStatus, int? createBy,
            DateTime? createDate)
        {
            this.MenuId = menu.Id;
            this.ProjectId = project.Id;
            this.UserId = user.Id;
            this.NomorUrutStatus = nomorUrutStatus;
            this.CreateBy = createBy;
            this.CreateDate = createDate;
            this.IsDelete = false;
        }

        public virtual void Update(Menu menu, Project project, User user, int? nomorUrutStatus, int? updateBy,
            DateTime? updateDate)
        {
            this.MenuId = menu.Id;
            this.ProjectId = project.Id;
            this.UserId = user.Id;
            this.NomorUrutStatus = nomorUrutStatus;
            this.UpdateBy = updateBy;
            this.UpdateDate = updateDate;
        }

        public virtual void Update(User user, int? updateBy,
           DateTime? updateDate)
        {
            this.UserId = user.Id;
            this.UpdateBy = updateBy;
            this.UpdateDate = updateDate;
        }

        public virtual void Delete(int deleteBy, DateTime deleteDate)
        {
            this.IsDelete = true;
            this.UpdateBy = deleteBy;
            this.DeleteDate = deleteDate;
        }

    }

}
