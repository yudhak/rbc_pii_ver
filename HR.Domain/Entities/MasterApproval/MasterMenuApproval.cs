﻿using HR.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HR.Domain
{
    public class MasterMenuApproval : Entity
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public int MenuId { get; private set; }

        public MasterMenuApproval()
        {

        }

        public MasterMenuApproval(string name, string description, int menuId)
        {
            this.Name = name;
            this.Description = description;
            this.MenuId = menuId;
        }

    }

}
