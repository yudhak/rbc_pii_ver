﻿using HR.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public class RoleAccessFront : Entity
    {
        public int RoleId { get; private set; }
        public virtual Role Role { get; private set; }
        public int MenuId { get; private set; }
        public virtual Menu Menu { get; private set; }
        public bool? Tambah { get; private set; }
        public bool? Ubah { get; private set; }
        public bool? Hapus { get; private set; }
        public bool? Lihat { get; private set; }
        public bool? Detail { get; private set; }
        public int? CreateBy { get; private set; }
        public DateTime? CreateDate { get; private set; }
        public int? UpdateBy { get; private set; }
        public DateTime? UpdateDate { get; private set; }
        public bool? IsDelete { get; private set; }
        public bool? IsActive { get; private set; }

        public RoleAccessFront()
        { }

        public RoleAccessFront(int roleId, int menuId, bool? tambah, bool? ubah, bool? hapus, bool? lihat, bool? detail, bool? isActive, int? createBy, DateTime? createDate)
        {
            this.RoleId = roleId;
            this.MenuId = menuId;
            this.Tambah = tambah;
            this.Ubah = ubah;
            this.Hapus = hapus;
            this.Lihat = lihat;
            this.Detail = detail;
            this.IsActive = isActive;
            this.CreateBy = createBy;
            this.CreateDate = createDate;
            this.IsDelete = false;
        }

        public virtual void Update(int roleId, int menuId, bool? tambah, bool? ubah, bool? hapus, bool? lihat, bool? detail, bool? isActive, int? updateBy, DateTime? updateDate)
        {
            this.RoleId = roleId;
            this.MenuId = menuId;
            this.Tambah = tambah;
            this.Ubah = ubah;
            this.Hapus = hapus;
            this.Lihat = lihat;
            this.Detail = detail;
            this.IsActive = isActive;
            this.UpdateBy = updateBy;
            this.UpdateDate = updateDate;
        }

        public virtual void Delete(int deleteBy, DateTime deleteDate)
        {
            this.IsDelete = true;
            this.UpdateDate = deleteDate;
        }
    }
}
