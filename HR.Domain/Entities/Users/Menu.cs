﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public class Menu
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public bool Active { get; set; }
        public bool IsGeneralAccess { get; private set; }
        public bool IsOnMenu { get; set; }
        public string StyleClass { get; set; }
        public string Icon { get; private set; }
        public int Sequence { get; set; }
        public bool IsSuperAdminOnly { get; private set; }
        public int? ParentId { get; set; }
        public virtual Menu Parent { get; private set; }
        public ICollection<APIMenu> APIMenuList { get; set; }
        public ICollection<RoleAccess> RoleAccessList { get; set; }
        public virtual IList<Menu> Child { get; set; }

        public Menu()
        {
            this.APIMenuList = new List<APIMenu>();
            this.RoleAccessList = new List<RoleAccess>();
            this.Child = new List<Menu>();
        }
        public void AddRoleAccess(RoleAccess mod)
        {
            this.RoleAccessList.Add(mod);
        }
        public void RemoveRoleAccess(RoleAccess mod)
        {
            this.RoleAccessList.Remove(mod);
        }
    }
}
