﻿using HR.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public class User
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool Status { get; set; }
        public string Language { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateTime { get; set; }
        public string ExpiryDate { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public int? DeleteBy { get; set; }
        public DateTime? DeleteDate { get; set; }

        //Foreign Key
        public int RoleId { get; set; }

        //Navigation Properties
        public virtual Role Role { get; set; }

        public int? EmployeeId { get; set; }

        public virtual IList<UserRole> UserRoles { get; set; }

        public User()
        {
            this.Language = "en";
            this.UserRoles = new List<UserRole>();
        }
        public User(string userName, string password, bool status, string expiryDate, DateTime? date, string time, Role role) : this()
        {
            this.UserName = userName;
            this.Password = DataSecurity.Encrypt(password);
            this.CreateDate = date;
            this.CreateTime = time;
            this.Status = status;
            this.ExpiryDate = expiryDate;
            this.RoleId = role.Id;
        }

        public User(string userName, string password, bool status, IList<UserRole> UserRole, DateTime? date, string time, Role role)
            : this(userName, password, status, "", date, time, role)
        {
            this.UserRoles = UserRole;
            //this.Accesses = userAccess;
        }

        public User(string userName, string password, bool status, bool isSenior, DateTime? date, string time, Role role)
            : this(userName, password, status, "", date, time, role)
        {

        }

        //    public User(string userName, string password, bool status, IList<UserRole> UserRole, string date, string time)
        //: this(userName, password, status, "", date, time)
        //    {
        //        this.UserRoles = UserRole;
        //        //this.Accesses = userAccess;
        //    }

        //    public User(string userName, string password, bool status, bool isSenior, string date, string time)
        //        : this(userName, password, status, "", date, time)
        //    {

        //    }



        public void UpdateUserRole(UserRole UserRole)
        {
            this.UserRoles[0] = UserRole;
        }

        public User(string userName, string password, bool status, bool isSenior, IList<UserRole> UserRole, DateTime? date, string time, Role role)
            : this(userName, password, status, UserRole, date, time, role)
        {
        }

        //    public User(string userName, string password, bool status, bool isSenior, IList<UserRole> UserRole, string date, string time)
        //: this(userName, password, status, UserRole, date, time)
        //    {
        //    }

        public void Update(string userName, string password, bool status, string expiryDate = "")
        {
            this.UserName = userName;
            this.Password = DataSecurity.Encrypt(password);
            this.Status = status;
            this.ExpiryDate = expiryDate;
           
        }


        public void ChangePassword(string oldPassword, string newPassword)
        {
            //Validate.IsOldPasswordValid(Security.Decrypt(this.password), oldPassword);
            this.Password = DataSecurity.Encrypt(newPassword);
        }

        public void ChangeLanguage(string lang)
        {
            this.Language = lang;
        }

        public bool ValidatePassword(string pass)
        {
            string confirmPass = DataSecurity.Decrypt(this.Password);
            return pass == confirmPass;
        }

        public bool IsActive()
        {
            return this.Status;
        }

        public void Activate()
        {
            this.Status = true;
        }
        public void Deactive()
        {
            this.Status = false;
        }
        public string EncodePassword(string password)
        {
            return DataSecurity.Encrypt(password).ToString();
        }
        public void AddUserRole(UserRole UserRole)
        {
            this.UserRoles.Add(UserRole);
        }
        public void RemoveUserRole(UserRole UserRole)
        {
            this.UserRoles.Remove(UserRole);
        }

        public User(string userName, string email, int roleId, bool status, DateTime? createDate, string password)
        {
            this.UserName = userName;
            this.Password = DataSecurity.Encrypt(password);
            this.Status = status;
            this.CreateDate = createDate;
            this.Email = email;
            this.RoleId = roleId;
        }

        public virtual void Update(string userName, string password, string email, int roleId, bool status, DateTime? lastUpdateDate)
        {
            this.UserName = userName;
            this.Password = DataSecurity.Encrypt(password);
            this.Status = status;
            this.LastUpdateDate = lastUpdateDate;
            this.Email = email;
            this.RoleId = roleId;
        }

        public void RemoveUser(int deleteBy, DateTime deleteDate)
        {
            this.IsDelete = true;
            this.Status = false;
            this.DeleteBy = deleteBy;
            this.DeleteDate = deleteDate;
        }
    }
}
