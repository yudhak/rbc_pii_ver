﻿using HR.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public class RoleAccess : Entity
    {
        public int RoleId { get; set; }
        public virtual Role Role { get; set; }
        public int MenuId { get; set; }
        public virtual Menu Menu { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool? IsActive { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public int? FlagFromFront { get; set; }

        public RoleAccess()
        {

        }
        public RoleAccess(int roleId, int menuId ,int? createBy, DateTime? createDate, bool? isActive, int? flagFromFront)
        {
            this.RoleId = roleId;
            this.MenuId = menuId;
            this.CreateBy = createBy;
            this.CreateDate = createDate;
            this.IsActive = isActive;
            this.FlagFromFront = flagFromFront;
        }
        public void Active(int roleId, int menuId, int? updateBy, DateTime? updateDate)
        {
            this.RoleId = roleId;
            this.MenuId = menuId;
            this.UpdateBy = updateBy;
            this.UpdateDate = updateDate;
            this.IsActive = true;
        }
        public void DisActive(int roleId, int menuId, int? updateBy, DateTime? updateDate)
        {
            this.RoleId = roleId;
            this.MenuId = menuId;
            this.UpdateBy = updateBy;
            this.UpdateDate = updateDate;
            this.IsActive = false;
        }
    }
}
