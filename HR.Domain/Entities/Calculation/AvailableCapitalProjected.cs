﻿using HR.Core;
using System;

namespace HR.Domain
{
    public class AvailableCapitalProjected : Entity
    {
        //Foreign Key
        public int ScenarioId { get; private set; }
        public int Year { get; set; }
        public decimal? Value { get; set; }
        public string Source { get; set; }

        public AvailableCapitalProjected()
        { }

        public AvailableCapitalProjected(Scenario scenario, int year, decimal? value, string source)
        {
            this.ScenarioId = scenario.Id;
            this.Year = year;
            this.Value = value;
            this.Source = source;
        }
        public virtual void Update(decimal? value)
        {
            this.Value = value;
        }
    }
}
