﻿using HR.Core;
using System;

namespace HR.Domain
{
    public class ScenarioCalculation : Entity
    {
        //Foreign Key
        public int ScenarioId { get; private set; }

        public ScenarioCalculation()
        { }

        public ScenarioCalculation(Scenario scenario)
        {
            this.ScenarioId = scenario.Id;
        }
    }
}
