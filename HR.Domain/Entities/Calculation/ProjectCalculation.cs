﻿using HR.Core;
using System;

namespace HR.Domain
{
    public class ProjectCalculation : Entity
    {
        //Foreign Key
        public int ProjectId { get; private set; }
        public int ScenarioCalculationId { get; private set; }

        public ProjectCalculation()
        { }

        public ProjectCalculation(Project project, ScenarioCalculation scenarioCalculation)
        {
            this.ProjectId = project.Id;
            this.ScenarioCalculationId = scenarioCalculation.Id;
        }

        public virtual void UpdateScenarioCalculationId(int? calculationId)
        {
            this.ScenarioCalculationId = calculationId.GetValueOrDefault();
        }
    }
}
