﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public interface IAssetDataRepository
    {
        AssetData Get(int id);
        IEnumerable<AssetData> GetAll();
        IEnumerable<AssetData> GetAll(string keyword, int id);
        void Insert(AssetData model);
        void Update(AssetData model);
        void Delete(int id, int deleteBy, DateTime deleteDate);
        bool IsExist(int id, string assetClass);
        bool IsExist(string assetClass);
    }
}
