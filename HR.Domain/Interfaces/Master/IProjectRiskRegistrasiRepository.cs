﻿using System;
using System.Collections.Generic;

namespace HR.Domain
{
    public interface IProjectRiskRegistrasiRepository
    {
        IEnumerable<ProjectRiskRegistrasi> GetAll();
        IEnumerable<ProjectRiskRegistrasi> GetByProjectId(int projectId);
        IEnumerable<int> GetByProjectId(int[] projectId);
        ProjectRiskRegistrasi Get(int id);
        void Insert(ProjectRiskRegistrasi model);
        void Insert(IList<ProjectRiskRegistrasi> collections);
        void Update(ProjectRiskRegistrasi model);
        void Update(IList<ProjectRiskRegistrasi> collections);
        void DeleteByProjectId(int id);
        bool IsExist(int projectId, int riskRegistrasiId);
    }
}
