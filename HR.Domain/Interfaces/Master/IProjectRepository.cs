﻿using System;
using System.Collections.Generic;

namespace HR.Domain
{
    public interface IProjectRepository
    {
        Project Get(int id);
        IEnumerable<Project> GetAll();
        IEnumerable<Project> GetByTahapan(int scenarioId, string tahapanIds);
        IEnumerable<Project> GetByTahapan(List<ScenarioDetail> project);
        IEnumerable<Project> GetAll(string keyword, int id);
        IEnumerable<Project> GetAllActive();
        void Insert(Project model);
        void Update(Project model);
        void Delete(int id, int? deleteBy, DateTime? deleteDate);
        bool IsExist(int id, string namaProject);
        bool IsExist(string namaProject);
        Project GetProjectActive(int id);
    }
}
