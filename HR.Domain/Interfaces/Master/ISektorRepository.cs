﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public interface ISektorRepository
    {
        Sektor Get(int id);
        IEnumerable<Sektor> GetAll();
        IEnumerable<Sektor> GetAll(string keyword, int id);
        void Insert(Sektor model);
        void Update(Sektor model);
        void Delete(int id, int deleteBy, DateTime deleteDate);
        bool IsExist(int id, string namaSektor);
        bool IsExist(string namaSektor);
        Sektor GetActive(int id);
    }
}
