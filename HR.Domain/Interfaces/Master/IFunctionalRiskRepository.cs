﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public interface IFunctionalRiskRepository
    {
        FunctionalRisk Get(int id);
        FunctionalRisk GetYellowByMatrix(int matrixId, int scenarioId);
        FunctionalRisk GetCommentByColor(int matrixId, int scenarioId, int colorId);
        IEnumerable<FunctionalRisk> GetAll(string keyword, int id);
        IEnumerable<FunctionalRisk> GetAllFalse();
        IEnumerable<FunctionalRisk> GetByScenarioId(int scenarioId);
        void Insert(FunctionalRisk model);
        void Update(FunctionalRisk model);
        void Delete(int id, int deleteBy, DateTime deleteDate);
        bool IsExist(int id, string definisi);
        bool IsExist(string definisi);
    }
}
