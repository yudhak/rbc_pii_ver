﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public interface IRiskMatriksTemporerRepository
    {
        RiskMatriksTemporer Get(int id);
        IEnumerable<RiskMatriksTemporer> GetAll();
        void Insert(RiskMatriksTemporer model);
        void Delete(int id, int deleteBy, DateTime deleteDate);
    }
}
