﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public interface IAuditLogRepository
    {
        AuditLog Get(int id);
        IEnumerable<AuditLog> GetAll(int id);
        void Insert(AuditLog model);
    }
}
