﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public interface ICorrelatedProjectTemporerRepository
    {
        CorrelatedProjectTemporer Get(int id);
        IEnumerable<CorrelatedProjectTemporer> GetAll();
        void Insert(CorrelatedProjectTemporer model);
        void Delete(int id, int deleteBy, DateTime deleteDate);
    }
}
