﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public interface ICorrelatedSektorTemporerRepository
    {
        CorrelatedSektorTemporer Get(int id);
        IEnumerable<CorrelatedSektorTemporer> GetAll();
        void Insert(CorrelatedSektorTemporer model);
        void Delete(int id, int deleteBy, DateTime deleteDate);
    }
}
