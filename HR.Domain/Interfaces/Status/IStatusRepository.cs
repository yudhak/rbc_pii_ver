﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public interface IStatusRepository
    {
        Status Get(int id);
        Status GetStatus(int? id);
        IEnumerable<Status> GetAll(string keyword);
        IEnumerable<Status> GetAll();

    }
}
