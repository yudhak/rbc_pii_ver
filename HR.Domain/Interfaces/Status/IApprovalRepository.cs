﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public interface IApprovalRepository
    {
        Approval Get(int id);
        string GetByActive(int id, int idUser, string source);
        IEnumerable<Approval> GetAll();
        IEnumerable<Approval> GetSentItem(bool sentItem, int userId, string keyword, int fieldId);
        IEnumerable<Approval> GetSentItem(bool sentItem, int userId, string keyword, int fieldId, int fieldId2);
        IEnumerable<Approval> GetByRequestId(int requestId);
		IEnumerable<Approval> GetItemActive(string keyword, int fieldId);
        IEnumerable<Approval> GetItemActive(string keyword, int fieldId, int fieldId2);
        IEnumerable<Approval> GetAllBaseOnSource(int id, string sourceApproval);
        Approval GetIdCreateByNoUrut(int id, int createBy, int noUrut);
        void Insert(Approval model);
        void Update(Approval model);
        void Delete(int id, int deleteBy, DateTime deleteDate);
        void Delete(int id, string source, int deleteBy, DateTime deleteDate);
    }
}
