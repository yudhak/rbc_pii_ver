﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public interface IRoleAccessFrontRepository
    {
        RoleAccessFront Get(int id);
        IEnumerable<RoleAccessFront> GetAll(string keyword, int id);
        void Insert(RoleAccessFront model);
        void Update(RoleAccessFront model);
        void Delete(int id);
        bool IsExist(int menuId, int roleId);
    }
}
