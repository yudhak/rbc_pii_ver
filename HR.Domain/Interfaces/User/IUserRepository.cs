﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public interface IUserRepository
    {
        IEnumerable<User> GetAll();
        IEnumerable<User> GetAll(string keyword);
        IEnumerable<User> GetAll(string keyword, int field);
        User Get(int id);
        User Find(string username);
        bool EmailExist(string username);
        bool isExist(string username);
        Role GetRoleMaster(int id);
        Role GetRoleUser(int id);
        //Role GetRole(int id);
        IEnumerable<User> GetAllFilter();

        void Insert(User model);
        void Update(User model);
        void Delete(int id);
        void Delete(int id, int deleteBy, DateTime deleteDate);
        bool IsExist(int id, string userName);
        bool IsExist(string userName);
    }
}
