﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public interface IRoleAccessRepository
    {
        RoleAccess Get(int id);
        IEnumerable<RoleAccess> GetAll();
        IEnumerable<RoleAccess> GetAllOldAccess(int id);
        void Insert(RoleAccess model);
        void Update(RoleAccess model);
        void Delete(int id);
    }
}
