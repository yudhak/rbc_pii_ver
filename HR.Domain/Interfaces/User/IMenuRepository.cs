﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public interface IMenuRepository
    {
        IQueryable<Menu> GetMenu();
        IEnumerable<Menu> GetAll();
        Menu Get(int Id);
        Menu GetParent(int? Id, Menu menu);
        IList<Menu> GetChild(int parentId);
        IEnumerable<Menu> GetSimilarName(int id);
    }
}
