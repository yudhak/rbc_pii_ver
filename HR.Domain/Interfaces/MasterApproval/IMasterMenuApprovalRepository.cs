﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public interface IMasterMenuApprovalRepository
    {
        MasterMenuApproval Get(int id);
        IEnumerable<MasterMenuApproval> GetAll(string keyword);
        IEnumerable<MasterMenuApproval> GetAll();

    }
}
