﻿using System;
using System.Collections.Generic;

namespace HR.Domain
{
    public interface IMasterApprovalCorrelatedSektorRepository
    {
        MasterApprovalCorrelatedSektor Get(int? id);
        MasterApprovalCorrelatedSektor GetByNomorUrut(int nomorUrutStatus);
        IEnumerable<MasterApprovalCorrelatedSektor> GetAll();
        IEnumerable<MasterApprovalCorrelatedSektor> GetAllByMenuId(int menuId);
        MasterApprovalCorrelatedSektor GetByUserId(int id);
        MasterApprovalCorrelatedSektor GetByRoleId(int roleId);
        MasterApprovalCorrelatedSektor GetBySektorIdUserid(int sektorId, int userId);
        //IEnumerable<MasterApprovalCorrelatedSektor> GetAll(string keyword);
        IEnumerable<MasterApprovalCorrelatedSektor> GetAllBySektorId(int sektorid);
        void Insert(MasterApprovalCorrelatedSektor model);
        void Insert(IList<MasterApprovalCorrelatedSektor> model);
        void Update(MasterApprovalCorrelatedSektor model);
        void Delete(int id, int deleteBy, DateTime deleteDate);
        bool IsExist(int id, int menuId, int sektorId, int userId, int nomorUrutStatus);
        //bool IsExist(string namaScenario);

    }
}
