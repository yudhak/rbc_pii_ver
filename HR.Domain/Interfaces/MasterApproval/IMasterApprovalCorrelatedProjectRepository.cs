﻿using System;
using System.Collections.Generic;

namespace HR.Domain
{
    public interface IMasterApprovalCorrelatedProjectRepository
    {
        MasterApprovalCorrelatedProject Get(int? id);
        MasterApprovalCorrelatedProject GetByUserId(int id);
        MasterApprovalCorrelatedProject GetByProjectIdUserId(int projectId,int userId);
        MasterApprovalCorrelatedProject GetByProjectIdRoleId(int projectId, int roleId);
        MasterApprovalCorrelatedProject GetByNomorUrutProjectId(int noUrutId, int projectId);
        IEnumerable<MasterApprovalCorrelatedProject> GetAll();
        IEnumerable<MasterApprovalCorrelatedProject> GetAllByMenuId(int menuId);
        //IEnumerable<MasterApprovalCorrelatedProject> GetAll(string keyword);
        IEnumerable<MasterApprovalCorrelatedProject> GetAllByProjectId(int projectId);
        void Insert(MasterApprovalCorrelatedProject model);
        void Insert(IList<MasterApprovalCorrelatedProject> model);
        void Update(MasterApprovalCorrelatedProject model);
        void Delete(int id, int deleteBy, DateTime deleteDate);
        bool IsExist(int id, int menuId, int projectId, int userId, int nomorUrutStatus);
        //bool IsExist(string namaScenario);

    }
}
