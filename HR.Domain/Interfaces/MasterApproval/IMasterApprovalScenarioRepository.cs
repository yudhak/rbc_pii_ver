﻿using System;
using System.Collections.Generic;

namespace HR.Domain
{
    
    public interface IMasterApprovalScenarioRepository
    {
        MasterApprovalScenario Get(int? id);
        IEnumerable<MasterApprovalScenario> GetAll();
        IEnumerable<MasterApprovalScenario> GetAllByMenuId(int menuId);
        IEnumerable<MasterApprovalScenario> GetAllByMenuIdIsDeleteTrue(int menuId);
        MasterApprovalScenario GetByMenuIdNomorUrutId(int menuId,int nomorUrut);
        MasterApprovalScenario GetByUserId(int id);
        MasterApprovalScenario GetByRoleId(int roleId);
        MasterApprovalScenario GetByNomorUrut(int nomorUrut);
        //IEnumerable<MasterApprovalScenario> GetAll(string keyword);
        void Insert(MasterApprovalScenario model);
        void Insert(IList<MasterApprovalScenario> model);
        void Update(MasterApprovalScenario model);
        void UpdateList(IList<MasterApprovalScenario> model);
        void Delete(int id, int deleteBy, DateTime deleteDate);
        bool IsExist(int id, int menuId, int userId, int nomorUrutStatus);
        //bool IsExist(string namaScenario);
        IEnumerable<MasterApprovalScenario> GetByCreateBy(int createBy);
    }
}
