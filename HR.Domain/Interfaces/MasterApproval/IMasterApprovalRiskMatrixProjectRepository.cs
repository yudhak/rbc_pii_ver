﻿using System;
using System.Collections.Generic;

namespace HR.Domain
{
    public interface IMasterApprovalRiskMatrixProjectRepository
    {
        MasterApprovalRiskMatrixProject Get(int? id);
        IEnumerable<MasterApprovalRiskMatrixProject> GetAll();
        IEnumerable<MasterApprovalRiskMatrixProject> GetAllByMenuId(int menuId);
        MasterApprovalRiskMatrixProject GetByUserId(int id);
        MasterApprovalRiskMatrixProject GetByProjectIdUserId(int id, int userId);
        MasterApprovalRiskMatrixProject GetByProjectIdRoleId(int projectId, int roleId);
        //IEnumerable<MasterApprovalRiskMatrixProject> GetAll(string keyword);
        IEnumerable<MasterApprovalRiskMatrixProject> GetAllByProjectId(int projectId);
        void Insert(MasterApprovalRiskMatrixProject model);
        void Insert(IList<MasterApprovalRiskMatrixProject> model);
        void Update(MasterApprovalRiskMatrixProject model);
        void Delete(int id, int deleteBy, DateTime deleteDate);
        bool IsExist(int id,  int menuId, int projectId, int userId, int nomorUrutStatus);
        //bool IsExist(string namaScenario);

    }
}
