﻿using System;
using System.Collections.Generic;

namespace HR.Domain
{
    public interface ICorrelatedProjectRepository
    {
        
        CorrelatedProject Get(int id);
        CorrelatedProject GetByScenarioIdProjectId(int scenarioId);
        IEnumerable<CorrelatedProject> GetByScenarioIdCompareCalculation(int scenarioId);
        CorrelatedProject GetByScenarioIdProjectIdAll(int scenarioId, int projectId);
        IEnumerable<CorrelatedProject> GetByScenarioIdProjectIdAllList(int scenarioId, int projectId);
        IEnumerable<CorrelatedProject> GetByScenarioIdProjectIdList(int scenarioId, int projectId);
        CorrelatedProject GetIsApproved(int id);
        IEnumerable<CorrelatedProject> GetIsApprovedList(int id);
        IEnumerable<CorrelatedProject> GetAll();
        IEnumerable<CorrelatedProject> GetByScenarioId(int scenarioId, string keyword, int id);
        IEnumerable<CorrelatedProject> GetByScenarioIdAll(int scenarioId);
        IEnumerable<CorrelatedProject> GetByScenarioId(int scenarioId, string keyword, int id, int user);
        IEnumerable<CorrelatedProject> GetByScenarioId(int scenarioId, string keyword, int id,int id2, int user);
        IEnumerable<CorrelatedProject> GetByScenarioProjectSektorId(int scenarioId, int projectId, int sektorId);
        IEnumerable<CorrelatedProject> GetByScenarioProjectSektorId2(int scenarioId, int projectId, int sektorId, int createBy);
        IEnumerable<CorrelatedProject> GetByScenarioIdIsDeleteFalse(int scenarioId);
        CorrelatedProject GetByScenarioIdProjectIdAllFalse(int scenarioId, int projectId);
        void Insert(CorrelatedProject model);
        void Update(CorrelatedProject model);
        void Delete(int id);
        
    }
}
