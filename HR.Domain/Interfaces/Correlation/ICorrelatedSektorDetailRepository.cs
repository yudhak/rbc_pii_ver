﻿using System;
using System.Collections.Generic;


namespace HR.Domain
{
    public interface ICorrelatedSektorDetailRepository
    {
        CorrelatedSektorDetail Get(int id);
        IEnumerable<CorrelatedSektorDetail> GetByCorrelatedSektorId(int correlatedSektorId);
        IEnumerable<CorrelatedSektorDetailLite> GetByCorrelatedSektorIdLite(int correlatedSektorId);
        IEnumerable<CorrelatedSektorDetail> GetOldData(int correlatedSektorId);
        IEnumerable<CorrelatedSektorDetail> GetByCorrelatedSectorRiskRegistrasiRow(int correlatedSektorId, int riskRegistrasiRow);
        IEnumerable<CorrelatedSektorDetail> GetByCorrelatedSectorRiskRegistrasiCol(int correlatedSektorId, int riskRegistrasiCol);
        void Insert(CorrelatedSektorDetail model);
        void Insert(IList<CorrelatedSektorDetail> collection);
        CorrelatedSektorDetail IsExisitOnAdding(int correlataedSektor, int riskRegistrasiIdRow, int riskRegistrasiIdCol);
        void Update(CorrelatedSektorDetail model);

    }
}
