﻿using System;
using System.Collections.Generic;

namespace HR.Domain
{
    public interface ICorrelatedSektorRepository
    {
        CorrelatedSektor Get(int id);
        CorrelatedSektor GetByScenarioIdSektorId(int scenarioId, int sektorId);
        int GetByScenarioIdSektorIdCst(int scenarioId, int sektorId);
        IEnumerable<CorrelatedSektor> GetAll();
        IEnumerable<CorrelatedSektor> GetByScenarioDefaultId(int scenarioId, string keyword);
        IEnumerable<CorrelatedSektor> GetByScenarioDefaultId(int scenarioId, string keyword, int user);
        IEnumerable<CorrelatedSektor> GetByScenarioDefaultId(int scenarioId, string keyword,int id, int user);
        IEnumerable<CorrelatedSektor> GetByScenarioDefaultId(int scenarioId, string keyword, int id,int id2, int user);
        IEnumerable<CorrelatedSektor> GetByScenarioIdSektorIdList(int scenarioId, int sektorId);
        IEnumerable<CorrelatedSektor> GetByScenarioIdSektorIdList2(int scenarioId, int sektorId, int createBy);
        void Insert(CorrelatedSektor model);
        void Insert(IList<CorrelatedSektor> collection);
        void Update(CorrelatedSektor model);
        void Delete(int id, int deleteBy, DateTime deleteDate);
        void Delete2(int id);
        IEnumerable<CorrelatedSektor> GetByScenarioIdAll(int scenarioId);
        IEnumerable<CorrelatedSektor> GetByScenarioIdIsDeleteFalse(int scenarioId);
        
    }
}
