﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public interface IRiskMatrixProjectRepository
    {
        RiskMatrixProject Get(int id);
        IEnumerable<RiskMatrixProject> GetAllData();
        IEnumerable<RiskMatrixProject> GetAllData(string keyword);
        IEnumerable<RiskMatrixProject> GetAll();
        IEnumerable<RiskMatrixProject> GetAll(string keyword);
        IEnumerable<RiskMatrixProject> GetAll(string keyword, int id);
        IEnumerable<RiskMatrixProject> GetAll(string keyword, int id, int id2);
        IEnumerable<RiskMatrixProject> GetByScenarioId(int scenarioId);
        IEnumerable<RiskMatrixProject> GetByScenarioIdAll(int scenarioId);
        IEnumerable<RiskMatrixProject> GetByProjectRiskMatrixScenarioId(int projectId, int riskMatrixId,int scenarioId);
        IEnumerable<RiskMatrixProject> GetByProjectRiskMatrixScenarioId2(int projectId, int riskMatrixId, int scenarioId, int createBy);
        IEnumerable<RiskMatrixProject> GetAllByScenarioId(int scenarioId);
        RiskMatrixProject GetByScenarioIdProjectId(int scenarioId, int projectId);

        RiskMatrixProject GetByScenarioIdProjectId2(int scenarioId, int projectId);
        RiskMatrixProject GetByScenarioIdProjectIdAll(int scenarioId, int projectId);
        IEnumerable<RiskMatrixProject> GetByScenarioIdProjectIdAllList(int scenarioId, int projectId);
        IEnumerable<RiskMatrixProject> GetByScenarioIdProjectIdList(int scenarioId, int projectId);
        RiskMatrixProject GetByProjectId(int projectId);
        void Insert(RiskMatrixProject model);
        void Insert(IList<RiskMatrixProject> collelction);
        void Update(RiskMatrixProject model);
        void Delete(int id);
        void DeleteByScenarioIdProjectId(int scenarioId, int projectId);
        bool IsExist(int id, int ProjectId);
        bool IsExist(int ProjectId);
        IEnumerable<RiskMatrixProject> GetAllByProjectIdNeedDelete(int projectId);
    }
}
