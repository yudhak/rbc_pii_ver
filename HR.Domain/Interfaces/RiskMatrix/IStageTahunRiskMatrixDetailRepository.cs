﻿using System;
using System.Collections.Generic;

namespace HR.Domain
{
    public interface IStageTahunRiskMatrixDetailRepository
    {
        IEnumerable<StageTahunRiskMatrixDetail> GetByRiskMatrixProjectId(int riskMatrixProjectId);
        IEnumerable<StageTahunRiskMatrixDetail> GetByRiskMatrixProjectIdAll(int riskMatrixProjectId);
        IEnumerable<StageTahunRiskMatrixDetail> GetByStageTahunRiskMatrixId(int stageTahunRiskMatrixId);
        IEnumerable<StageTahunRiskMatrixDetail> GetByStageTahunRiskMatrixIdHasRemoved(int stageTahunRiskMatrixId);
        void Insert(StageTahunRiskMatrixDetail model);
        void Update(StageTahunRiskMatrixDetail model);
        void Delete(int riskMatrixProjectId);
        bool IsExist(int id, int stageTahunRiskMatrixId);
        bool IsExist(int riskMatrixProjectId);
    }
}
