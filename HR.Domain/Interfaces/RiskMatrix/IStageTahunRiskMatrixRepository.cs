﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public interface IStageTahunRiskMatrixRepository
    {
        StageTahunRiskMatrix Get(int id);
        IEnumerable<StageTahunRiskMatrix> GetByRiskMatrixProjectId(int riskMatrixProjectId);
        IEnumerable<StageTahunRiskMatrix> GetByRiskMatrixProjectIdDelete(int riskMatrixProjectId);
        IEnumerable<StageTahunRiskMatrix> GetByRiskMatrixProjectIdAll(int riskMatrixProjectId);
        StageTahunRiskMatrix GetByRiskMatrixProjectIdYear(int riskMatrixProjectId, int year);
        StageTahunRiskMatrix GetByIdRiskMatrixProjectId(int id, int riskMatrixProjectId);
        IEnumerable<StageTahunRiskMatrix> GetByRiskMatrixProjectIdYearStage(int riskMatrixProjectId, int year, int stageId);
        IEnumerable<StageTahunRiskMatrix> GetAll();
        void Insert(StageTahunRiskMatrix model);
        void Update(StageTahunRiskMatrix model);
        void Delete(int riskMatrixProjectId);
        //void Delete(int id, int deleteBy, DateTime deleteDate);
        bool IsExist(int id, int riskMatrixProjectId);
        bool IsExist(int riskMatrixProjectId);
    }
}
