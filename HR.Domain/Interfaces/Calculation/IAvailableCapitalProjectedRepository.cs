﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public interface IAvailableCapitalProjectedRepository
    {
        AvailableCapitalProjected Get(int id);
        AvailableCapitalProjected GetByScenarioIdYear(int scenarioId, int year, string source);
        IEnumerable<AvailableCapitalProjected> GetAll();
        void Insert(AvailableCapitalProjected model);
        void Update(AvailableCapitalProjected model);
        void Delete();
    }
}
