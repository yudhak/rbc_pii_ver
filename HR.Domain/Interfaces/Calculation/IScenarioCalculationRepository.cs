﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public interface IScenarioCalculationRepository
    {
        ScenarioCalculation Get(int id);
        ScenarioCalculation GetByScenarioId(int id);
        IEnumerable<ScenarioCalculation> GetAll();
        void Insert(ScenarioCalculation model);
        void Delete();
        void Delete(ScenarioCalculation model);
    }
}
