﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Domain
{
    public interface IProjectCalculationRepository
    {
        ProjectCalculation Get(int id);
        ProjectCalculation GetByProjectId(int id);
        IEnumerable<ProjectCalculation> GetByScenarioCalculationId(int id);
        IEnumerable<ProjectCalculation> GetAll();
        void Insert(ProjectCalculation model);
        void Delete();
        void Delete(ProjectCalculation model);
        void Update(ProjectCalculation model);
    }
}
