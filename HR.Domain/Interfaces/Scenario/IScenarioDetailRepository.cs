﻿using System;
using System.Collections.Generic;

namespace HR.Domain
{
    public interface IScenarioDetailRepository
    {
        ScenarioDetail Get(int id);
        IEnumerable<ScenarioDetail> GetAll();
        IEnumerable<ScenarioDetail> GetByScenarioId(int scenarioId);
        void Insert(ScenarioDetail model);
        void Insert(IList<ScenarioDetail> collections);
        void Update(ScenarioDetail model);
        void Delete(int id);
        IEnumerable<ScenarioDetail> GetAllByScenarioId(int scenarioId);
        IEnumerable<ScenarioDetail> GetAllProjectInScenario(int projectId);
        IEnumerable<ScenarioDetail> GetAllProjectInScenarioNeedDelete(int projectId);
    }
}
