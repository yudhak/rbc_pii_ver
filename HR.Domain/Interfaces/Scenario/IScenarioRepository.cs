﻿using System;
using System.Collections.Generic;

namespace HR.Domain
{
    public interface IScenarioRepository
    {
        Scenario Get(int? id);
        IEnumerable<Scenario> GetAll();
        IEnumerable<Scenario> GetAll(string keyword, int userId);
        IEnumerable<Scenario> GetAll(string keyword,int id, int userId);
        IEnumerable<Scenario> GetAll(string keyword, int id, int id2, int userId);
        IEnumerable<Scenario> GetAllByName(int id);
        IEnumerable<Scenario> GetAllByName2(int id, int idUser);
        int GetAllByName2Collate(int idUser, string namaScer);
        void Insert(Scenario model);
        void Update(Scenario model);
        void Delete(int id, int deleteBy, DateTime deleteDate);
        bool IsExist(int id, string namaScenario);
        bool IsExist(string namaScenario);
        Scenario GetDefault();
        //void SetDefault(Scenario model);
        IEnumerable<Scenario> GetAllForProductValidate();

    }
}
