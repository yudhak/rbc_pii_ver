﻿using HR.Domain;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace HR.Infrastructure.EntityConfig
{
    public class ProjectCalculationConfig : EntityTypeConfiguration<ProjectCalculation>
    {
        public ProjectCalculationConfig()
        {
            //table
            ToTable("tblProjectCalculations");

            //key
            HasKey(x => x.Id);

            //property
            Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ProjectId).HasColumnName("projectId");
            Property(x => x.ScenarioCalculationId).HasColumnName("scenarioCalculationId");
        }
    }
}
