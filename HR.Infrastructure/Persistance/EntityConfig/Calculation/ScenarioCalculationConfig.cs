﻿using HR.Domain;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace HR.Infrastructure.EntityConfig
{
    public class ScenarioCalculationConfig : EntityTypeConfiguration<ScenarioCalculation>
    {
        public ScenarioCalculationConfig()
        {
            //table
            ToTable("tblScenarioCalculations");

            //key
            HasKey(x => x.Id);

            //property
            Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ScenarioId).HasColumnName("scenarioId");
        }
    }
}
