﻿using HR.Domain;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace HR.Infrastructure.EntityConfig
{
    public class AvailableCapitalProjectedConfig : EntityTypeConfiguration<AvailableCapitalProjected>
    {
        public AvailableCapitalProjectedConfig()
        {
            //table
            ToTable("tblAvailableCapitalProjecteds");

            //key
            HasKey(x => x.Id);

            //property
            Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ScenarioId).HasColumnName("scenarioId");
            Property(x => x.Year).HasColumnName("year");
            Property(x => x.Value).HasColumnName("value");
            Property(x => x.Source).HasColumnName("source");
        }
    }
}
