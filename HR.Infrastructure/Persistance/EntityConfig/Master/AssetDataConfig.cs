﻿using HR.Domain;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace HR.Infrastructure.EntityConfig
{
    public class AssetDataConfig : EntityTypeConfiguration<AssetData>
    {
        public AssetDataConfig()
        {
            //table
            ToTable("tblAssetDatas");

            //key
            HasKey(x => x.Id);

            //property
            Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.AssetClass).HasColumnName("assetClass");
            Property(x => x.TermAwal).HasColumnName("termAwal");
            Property(x => x.TermAkhir).HasColumnName("termAkhir");
            Property(x => x.OutstandingStartYears).HasColumnName("outstandingStartYears");
            Property(x => x.OutstandingEndYears).HasColumnName("outstandingEndYears");
            Property(x => x.AssetValue).HasColumnName("assetValue").HasPrecision(18, 13);
            Property(x => x.Porpotion).HasColumnName("porpotion").HasPrecision(18, 13);
            Property(x => x.AssumedReturnPercentage).HasColumnName("assumedReturnPercentage").HasPrecision(18, 13);
            Property(x => x.AssumedReturn).HasColumnName("assumedReturn").HasPrecision(18, 13);
            Property(x => x.CreateBy).HasColumnName("createBy");
            Property(x => x.CreateDate).HasColumnName("createDate");
            Property(x => x.UpdateBy).HasColumnName("updateBy");
            Property(x => x.UpdateDate).HasColumnName("updateDate");
            Property(x => x.IsDelete).HasColumnName("isDelete");
            Property(x => x.DeleteDate).HasColumnName("deleteDate");
            Property(x => x.Status).HasColumnName("status");
        }
    }
}
