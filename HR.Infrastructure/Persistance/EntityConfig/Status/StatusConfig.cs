﻿using HR.Domain;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace HR.Infrastructure.EntityConfig
{
    public class StatusConfig : EntityTypeConfiguration<Status>
    {
        public StatusConfig()
        {
            //table
            ToTable("tblStatus");

            //key
            HasKey(x => x.Id);

            //property
            Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.StatusDescription).HasColumnName("statusDescription");  
            
        }
    }
}
