﻿using HR.Domain;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace HR.Infrastructure.EntityConfig
{
    public class ApprovalConfig : EntityTypeConfiguration<Approval>
    {
        public ApprovalConfig()
        {
            //table
            ToTable("tblApproval");

            //key
            HasKey(x => x.Id);

            //property
            Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);           
            Property(x => x.StatusApproval).HasColumnName("statusApproval");
            Property(x => x.SourceApproval).HasColumnName("sourceApproval");
            Property(x => x.Keterangan).HasColumnName("keterangan");
            Property(x => x.CreateBy).HasColumnName("createBy");
            Property(x => x.CreateDate).HasColumnName("createDate");
            Property(x => x.UpdateBy).HasColumnName("updateBy");
            Property(x => x.UpdateDate).HasColumnName("updateDate");
            Property(x => x.IsDelete).HasColumnName("isDelete");
            Property(x => x.DeleteDate).HasColumnName("deleteDate");
            Property(x => x.IsActive).HasColumnName("isActive");
            Property(x => x.NomorUrutStatus).HasColumnName("nomorUrutStatus");

            //foreign key
            Property(x => x.StatusId).HasColumnName("statusId");
            Property(x => x.RequestId).HasColumnName("requestId");
        }
    }
}
