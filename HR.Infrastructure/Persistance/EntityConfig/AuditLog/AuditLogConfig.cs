﻿using HR.Domain;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace HR.Infrastructure.EntityConfig
{
    public class AuditLogConfig : EntityTypeConfiguration<AuditLog>
    {
        public AuditLogConfig()
        {
            //table
            ToTable("tblAuditLogs");

            //key
            HasKey(x => x.Id);

            //property
            Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.MenuId).HasColumnName("menuId");
            Property(x => x.TableModified).HasColumnName("tableModified");
            Property(x => x.DataObjekId).HasColumnName("dataObjekId");
            Property(x => x.ColumnModified).HasColumnName("columnModified");
            Property(x => x.DataAwal).HasColumnName("dataAwal");
            Property(x => x.DataAkhir).HasColumnName("dataAkhir");
            Property(x => x.LogTimestamp).HasColumnName("logTimestamp");
            Property(x => x.ModifiedBy).HasColumnName("modifiedBy");    
        }
    }
}
