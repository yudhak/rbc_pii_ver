﻿using HR.Domain;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace HR.Infrastructure.EntityConfig
{
    public class RiskMatriksTemporerConfig : EntityTypeConfiguration<RiskMatriksTemporer>
    {
        public RiskMatriksTemporerConfig()
        {
            //table
            ToTable("tblRiskMatriksTemporers");

            //key
            HasKey(x => x.Id);

            //property
            Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.MenuId).HasColumnName("menuId");
            Property(x => x.TableModified).HasColumnName("tableModified");
            Property(x => x.DataObjekId).HasColumnName("dataObjekId");
            Property(x => x.ColumnModified).HasColumnName("columnModified");
            Property(x => x.DataAwal).HasColumnName("dataAwal");
            Property(x => x.DataAkhir).HasColumnName("dataAkhir");
            Property(x => x.LogTimestamp).HasColumnName("logTimestamp");
            Property(x => x.ModifiedBy).HasColumnName("modifiedBy");
            Property(x => x.DeleteDate).HasColumnName("deleteDate");
            Property(x => x.DeleteBy).HasColumnName("deleteBy");
        }
    }
}
