﻿using HR.Domain;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace HR.Infrastructure.EntityConfig
{
    public class MasterMenuApprovalConfig : EntityTypeConfiguration<MasterMenuApproval>
    {
        public MasterMenuApprovalConfig()
        {
            //table
            ToTable("tblMasterMenuApprovals");

            //key
            HasKey(x => x.Id);

            //property
            Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Name).HasColumnName("name");
            Property(x => x.Description).HasColumnName("description");
            Property(x => x.MenuId).HasColumnName("menuId");
        }
    }
}
