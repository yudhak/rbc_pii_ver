﻿using HR.Domain;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace HR.Infrastructure.EntityConfig
{
    public class MasterApprovalCorrelatedSektorConfig : EntityTypeConfiguration<MasterApprovalCorrelatedSektor>
    {
        public MasterApprovalCorrelatedSektorConfig()
        {
            //table
            ToTable("tblMasterApprovalCorrelatedSektors");

            //key
            HasKey(x => x.Id);

            //property
            Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.NomorUrutStatus).HasColumnName("nomorUrutStatus");
            Property(x => x.CreateBy).HasColumnName("createBy");
            Property(x => x.CreateDate).HasColumnName("createDate");
            Property(x => x.UpdateBy).HasColumnName("updateBy");
            Property(x => x.UpdateDate).HasColumnName("updateDate");
            Property(x => x.IsDelete).HasColumnName("isDelete");
            Property(x => x.DeleteDate).HasColumnName("deleteDate");

            //foreign key
            Property(x => x.MenuId).HasColumnName("menuId");
            Property(x => x.SektorId).HasColumnName("sektorId");
            Property(x => x.UserId).HasColumnName("userId");
        }
    }
}
