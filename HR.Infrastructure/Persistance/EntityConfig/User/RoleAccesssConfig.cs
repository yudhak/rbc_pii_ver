﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HR.Infrastructure.EntityConfig
{
    public class RoleAccesssConfig : EntityTypeConfiguration<RoleAccess>
    {
        public RoleAccesssConfig()
        {
            //table
            ToTable("tblRoleAccess");

            //key
            HasKey(x => x.Id);

            //property Foreign Key
            Property(x => x.RoleId).HasColumnName("RoleId");
            Property(x => x.MenuId).HasColumnName("MenuId");
            Property(x => x.CreateBy).HasColumnName("CreateBy");
            Property(x => x.CreateDate).HasColumnName("CreateDate");
            Property(x => x.IsActive).HasColumnName("IsActive");
            Property(x => x.UpdateBy).HasColumnName("UpdateBy");
            Property(x => x.UpdateDate).HasColumnName("UpdateDate");
            Property(x => x.IsDelete).HasColumnName("IsDelete");
            Property(x => x.FlagFromFront).HasColumnName("FlagFromFront");

            ////relationship1
            HasRequired(x => x.Role).WithMany(x => x.RoleAccesses).HasForeignKey(x => x.RoleId);
            HasRequired(x => x.Menu).WithMany(x => x.RoleAccessList).HasForeignKey(x => x.MenuId);
        }
    }
}
