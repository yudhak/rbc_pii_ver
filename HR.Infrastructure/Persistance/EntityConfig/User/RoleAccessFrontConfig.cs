﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HR.Infrastructure.EntityConfig
{
    public class RoleAccessFrontConfig : EntityTypeConfiguration<RoleAccessFront>
    {
        public RoleAccessFrontConfig()
        {
            //table
            ToTable("tblRoleAccessFronts");

            //key
            HasKey(x => x.Id);

            //property Foreign Key
            Property(x => x.MenuId).HasColumnName("menuId");
            Property(x => x.RoleId).HasColumnName("roleId");
            Property(x => x.Tambah).HasColumnName("tambah");
            Property(x => x.Ubah).HasColumnName("ubah");
            Property(x => x.Hapus).HasColumnName("hapus");
            Property(x => x.Lihat).HasColumnName("lihat");
            Property(x => x.Detail).HasColumnName("detail");           
            Property(x => x.CreateBy).HasColumnName("createBy");
            Property(x => x.CreateDate).HasColumnName("createDate");           
            Property(x => x.UpdateBy).HasColumnName("updateBy");
            Property(x => x.UpdateDate).HasColumnName("updateDate");
            Property(x => x.IsDelete).HasColumnName("isDelete");
            Property(x => x.IsActive).HasColumnName("isActive");
        }
    }
}
