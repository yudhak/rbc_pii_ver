﻿ALTER TABLE tblStageTahunRiskMatrixDetails
ADD warnaExpose bit;
ALTER TABLE tblStageTahunRiskMatrixDetails
ADD warnaLikelihood bit;

insert into tblMenu (name,controllername,actionname,active,isonmenu,sequence) values ('AuditLog', 'AuditLog', 'index',1,1,34);
insert into tblMenu (name,controllername,actionname,active,isonmenu,sequence, ParentId) values ('AuditLog', 'AuditLog', 'index2',1,0,34,34);
insert into tblMenu (name,controllername,actionname,active,isonmenu,sequence, ParentId) values ('AuditLog', 'AuditLog', 'add',1,0,34,34);
insert into tblMenu (name,controllername,actionname,active,isonmenu,sequence, ParentId) values ('AuditLog', 'AuditLog', 'update',1,0,34,34);
insert into tblMenu (name,controllername,actionname,active,isonmenu,sequence, ParentId) values ('AuditLog', 'AuditLog', 'delete',1,0,34,34);