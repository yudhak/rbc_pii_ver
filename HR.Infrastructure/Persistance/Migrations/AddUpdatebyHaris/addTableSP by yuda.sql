﻿
/****** Object:  Table [dbo].[tblProjectScenarioLeftOuterVirtual]    Script Date: 7/6/2018 3:05:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblProjectScenarioLeftOuterVirtual](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[projectId] [int] NULL,
	[scenarioId] [int] NULL,
 CONSTRAINT [PK_tblProjectScenarioLeftOuter] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblProjectScenarioLeftVirtual]    Script Date: 7/6/2018 3:05:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblProjectScenarioLeftVirtual](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[projectId] [int] NULL,
	[scenarioId] [int] NULL,
 CONSTRAINT [PK_tblLeftJoinScenarioDetails] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblProjectScenarioVirtual]    Script Date: 7/6/2018 3:05:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblProjectScenarioVirtual](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[projectId] [int] NULL,
	[scenarioId] [int] NULL,
 CONSTRAINT [PK_tblScenarioTemporary] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblSektorScenarioLeftOuterVirtual]    Script Date: 7/6/2018 3:05:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSektorScenarioLeftOuterVirtual](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[sektorId] [int] NULL,
	[scenarioId] [int] NULL,
 CONSTRAINT [PK_tblSektorVirtual] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblSektorScenarioLeftVirtual]    Script Date: 7/6/2018 3:05:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSektorScenarioLeftVirtual](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[sektorId] [int] NULL,
	[scenarioId] [int] NULL,
 CONSTRAINT [PK_tblSektorLeftVirtual] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblSektorScenarioRightVirtual]    Script Date: 7/6/2018 3:05:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSektorScenarioRightVirtual](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[sektorId] [int] NULL,
	[scenarioId] [int] NULL,
 CONSTRAINT [PK_tblSektorScenarioRightVirtual] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblStageTahunRiskMatrixScenarioVirtual]    Script Date: 7/6/2018 3:05:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblStageTahunRiskMatrixScenarioVirtual](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tahun] [int] NULL,
	[scenarioId] [int] NULL,
 CONSTRAINT [PK_tblStageTahunRiskMatrixTemporary] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
