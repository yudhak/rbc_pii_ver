﻿
GO
/****** Object:  Table [dbo].[tblProjectScenarioRightVirtual]    Script Date: 7/12/2018 3:55:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblProjectScenarioRightVirtual](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[projectId] [int] NULL,
	[scenarioId] [int] NULL,
 CONSTRAINT [PK_tblProjectScenarioRightVirtual] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
