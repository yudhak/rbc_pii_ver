﻿/****** Object:  StoredProcedure [dbo].[DuplicateRiskMatrixBasedScenario]    Script Date: 6/28/2018 4:32:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DuplicateRiskMatrixBasedScenario3]
	-- Add the parameters for the stored procedure here
	 @scenario1 int,
	 @scenario2 int,
	 @createBy int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	INSERT INTO tblRiskMatrixs (scenarioId, createBy, createDate, isDelete) values (@scenario2, @createBy, CURRENT_TIMESTAMP, 0)
	DECLARE @RiskMatrixId  int
	SELECT @RiskMatrixId = id FROM tblRiskMatrixs WHERE scenarioId = @scenario2 and isDelete = 0

	DECLARE @table1 TABLE (
    idx int identity(1,1),
    col1 int )
	DECLARE @RowsToProcess  int
	DECLARE @CurrentRow int		  

	INSERT into @table1 (col1) 
	SELECT A.projectId AS project
	FROM tblScenarioDetails A, tblScenarioDetails B
	WHERE A.scenarioId = @scenario2 and B.scenarioId = @scenario1 and A.isDelete =0 and b.isDelete = 0
	AND A.projectId = B.projectId
	SET @RowsToProcess=@@ROWCOUNT
	SET @CurrentRow = 0

	WHILE(@CurrentRow<@RowsToProcess)
	BEGIN
		SET @CurrentRow=@CurrentRow+1
		DECLARE @colVar int
		SELECT @colVar = col1 FROM @table1 WHERE idx = @CurrentRow

		-- Generate Correlated Project
		INSERT INTO [dbo].[tblCorrelatedProjects] ([projectId],  [scenarioId], [sektorId], [createBy],[createDate], [isDelete],[statusId])
		SELECT [projectId],  @scenario2, [sektorId], [createBy],[createDate], [isDelete],[statusId]
		FROM tblCorrelatedProjects
		WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0

		DECLARE @colVar12 int
		SELECT @colVar12 = id FROM tblCorrelatedProjects WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0

		DECLARE @colVar13 int
		SELECT @colVar13 = id FROM tblCorrelatedProjects WHERE scenarioId = @scenario2 and projectId = @colVar and isDelete = 0

		INSERT INTO [dbo].tblCorrelatedProjectDetails([correlatedProjectId],[projectIdRow],[projectIdCol],[correlationMatrixId],[createBy],[createDate],[updateBy],[updateDate] ,[deleteDate],[isDelete])
		SELECT @colVar13,[projectIdRow],[projectIdCol],[correlationMatrixId] ,[createBy],[createDate],[updateBy],[updateDate] ,[deleteDate],[isDelete]
		FROM tblCorrelatedProjectDetails
		WHERE correlatedProjectId = @colVar12 and isDelete = 0;

		-- Do your work here
		INSERT INTO [dbo].[tblRiskMatrixProjects] ([projectId], [riskMatrixId], [scenarioId], [createBy],[createDate], [isDelete],[statusId])
		SELECT [projectId], @RiskMatrixId, @scenario2, [createBy],[createDate], [isDelete],[statusId]
		FROM tblRiskMatrixProjects
		WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0

		DECLARE @colVar3 int
		SELECT @colVar3 = id FROM tblRiskMatrixProjects WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0

		DECLARE @colVar4 int
		SELECT @colVar4 = id FROM tblRiskMatrixProjects WHERE scenarioId = @scenario2 and projectId = @colVar and isDelete = 0

		INSERT INTO [dbo].tblStageTahunRiskMatrixs([riskMatrixProjectId],[tahun],[stageId] ,[createBy],[createDate],[updateBy],[updateDate],[isUpdate] ,[deleteDate],[isDelete],[maximumNilaiExpose])
		SELECT @colVar4,[tahun],[stageId] ,[createBy],[createDate],[updateBy],[updateDate],[isUpdate] ,[deleteDate],[isDelete],[maximumNilaiExpose]
		FROM tblStageTahunRiskMatrixs
		WHERE riskMatrixProjectId = @colVar3 and isDelete = 0;

		INSERT INTO [dbo].tblStageTahunRiskMatrixDetails([riskMatrixProjectId],[nilaiExpose],[stageTahunRiskMatrixId],[riskRegistrasiId],[likehoodDetailId],[warnaExpose],[warnaLikelihood] ,[createBy],[createDate],[updateBy],[updateDate],[isUpdate] ,[deleteDate],[isDelete])
		SELECT @colVar4,[nilaiExpose],[stageTahunRiskMatrixId],[riskRegistrasiId],[likehoodDetailId],[warnaExpose],[warnaLikelihood] ,[createBy],[createDate],[updateBy],[updateDate],[isUpdate] ,[deleteDate],[isDelete]
		FROM tblStageTahunRiskMatrixDetails
		WHERE riskMatrixProjectId = @colVar3 and isDelete = 0;

		DECLARE @RowsToProcess2  int
		DECLARE @CurrentRow2 int	
		DECLARE @table5 TABLE (
		idx int identity(1,1),
		col1 int )
		INSERT into @table5 (col1) SELECT tahun FROM tblStageTahunRiskMatrixs where riskMatrixProjectId = @colVar3 and isDelete = 0
		SET @RowsToProcess2=@@ROWCOUNT
		SET @CurrentRow2 = 0

		WHILE(@CurrentRow2<@RowsToProcess2)
		BEGIN
			SET @CurrentRow2=@CurrentRow2+1
			DECLARE @colVar5 int
			DECLARE @colVar6 int
			DECLARE @colVar7 int

			SELECT @colVar5 = col1 FROM @table5 WHERE idx = @CurrentRow2
			SELECT @colVar6 = id FROM tblStageTahunRiskMatrixs WHERE riskMatrixProjectId = @colVar3 and tahun = @colVar5 and isDelete = 0
			SELECT @colVar7 = id FROM tblStageTahunRiskMatrixs WHERE riskMatrixProjectId = @colVar4 and tahun = @colVar5 and isDelete = 0

			UPDATE tblStageTahunRiskMatrixDetails
			SET stageTahunRiskMatrixId = @colVar7
			WHERE riskMatrixProjectId = @colVar4 and stageTahunRiskMatrixId = @colVar6;
			
		END
	END

	DECLARE @table2 TABLE (
    idx int identity(1,1),
    col1 int )
	INSERT into @table2 (col1) SELECT projectId FROM tblScenarioDetails where scenarioId = @scenario2 and isDelete = 0

	DECLARE @table3 TABLE (
    idx int identity(1,1),
    col1 int )
	INSERT into @table3 (col1) SELECT projectId FROM tblScenarioDetails where scenarioId = @scenario1 and isDelete = 0

	DECLARE @table4 TABLE (
    idx int identity(1,1),
    col1 int )

	INSERT into @table4 (col1)
	SELECT A.col1
	FROM @table2 A
	LEFT JOIN @table3 B
	ON A.col1 = B .col1
	WHERE B.idx is NULL
	SET @RowsToProcess=@@ROWCOUNT
	SET @CurrentRow = 0

	WHILE(@CurrentRow<@RowsToProcess)
	BEGIN
		SET @CurrentRow=@CurrentRow+1
		DECLARE @colVar2 int
		SELECT @colVar2 = col1 FROM @table4 WHERE idx = @CurrentRow
		-- Do your work here
		INSERT INTO [dbo].[tblRiskMatrixProjects] ([projectId], [riskMatrixId], [scenarioId], [createBy],[createDate], [isDelete])
		VALUES (@colVar2, @RiskMatrixId, @scenario2, @createBy,CURRENT_TIMESTAMP, 0)
		-- Generate Correlated Project
		DECLARE @colVar11 int
		SELECT @colVar11 = sektorId FROM tblProjects WHERE id = @colVar2
		INSERT INTO [dbo].tblCorrelatedProjects([projectId],  [scenarioId], [sektorId], [createBy],[createDate], [isDelete])
		VALUES (@colVar2, @scenario2, @colVar11, @createBy,CURRENT_TIMESTAMP, 0)
	END

	-- Correlated Sektor Generate
	DECLARE @RowsToProcess3  int
	DECLARE @CurrentRow3 int	
	DECLARE @table6 TABLE (
	idx int identity(1,1),
	col1 int )
	INSERT into @table6 (col1)
	SELECT B.sektorId FROM tblScenarioDetails A
	LEFT OUTER JOIN tblProjects B
	ON A.projectId = B.id
	where A.scenarioId = @scenario2 and A.isDelete = 0
	group by B.sektorId
	SET @RowsToProcess3=@@ROWCOUNT
	SET @CurrentRow3 = 0

	WHILE(@CurrentRow3<@RowsToProcess3)
	BEGIN
		SET @CurrentRow3=@CurrentRow3+1
		DECLARE @colVar8 int
		SELECT @colVar8 = col1 FROM @table6 WHERE idx = @CurrentRow3

		INSERT INTO [dbo].[tblCorrelatedSektors] ([namaSektor], [createBy], [createDate], [updateBy],[updateDate], [isDelete],[deleteDate],[scenarioId],[sektorId],[statusId])
		SELECT [namaSektor], [createBy], [createDate], [updateBy],[updateDate], [isDelete],[deleteDate],@scenario2,[sektorId],[statusId]
		FROM [tblCorrelatedSektors]
		WHERE scenarioId = @scenario1 and sektorId = @colVar8 and isDelete = 0

		DECLARE @colVar9 int
		SELECT @colVar9 = id FROM tblCorrelatedSektors WHERE scenarioId = @scenario1 and sektorId = @colVar8 and isDelete = 0

		DECLARE @colVar10 int
		SELECT @colVar10 = id FROM tblCorrelatedSektors WHERE scenarioId = @scenario2 and sektorId = @colVar8 and isDelete = 0

		INSERT INTO [dbo].tblCorrelatedSektorDetails([correlatedSektorId],[riskRegistrasiIdRow],[correlationMatrixId],[riskRegistrasiIdCol],[createBy],[createDate],[updateBy],[updateDate],[deleteDate],[isDelete])
		SELECT @colVar10,[riskRegistrasiIdRow],[correlationMatrixId],[riskRegistrasiIdCol],[createBy],[createDate],[updateBy],[updateDate],[deleteDate],[isDelete]
		FROM tblCorrelatedSektorDetails
		WHERE correlatedSektorId = @colVar9 and isDelete = 0;
	END	
	
	DECLARE @table7 TABLE (
    idx int identity(1,1),
    col1 int )
	INSERT into @table7 (col1)
	SELECT B.sektorId FROM tblScenarioDetails A
	LEFT OUTER JOIN tblProjects B
	ON A.projectId = B.id
	where A.scenarioId = @scenario1 and A.isDelete = 0
	group by B.sektorId				
	
	DECLARE @table8 TABLE (
    idx int identity(1,1),
    col1 int )

	DECLARE @RowsToProcess4  int
	DECLARE @CurrentRow4 int
	INSERT into @table8 (col1)
	SELECT A.col1
	FROM @table6 A
	LEFT JOIN @table7 B
	ON A.col1 = B .col1
	WHERE B.idx is NULL
	SET @RowsToProcess4=@@ROWCOUNT
	SET @CurrentRow4 = 0	

	WHILE(@CurrentRow4<@RowsToProcess4)
	BEGIN
		SET @CurrentRow4=@CurrentRow4+1
		DECLARE @colVar14 int
		SELECT @colVar14 = col1 FROM @table8 WHERE idx = @CurrentRow4
		-- Do your work here
		DECLARE @colVar15 varchar(500)
		SELECT @colVar15 = namaSektor FROM tblSektors WHERE id = @colVar14
		INSERT INTO [dbo].tblCorrelatedSektors([namaSektor], [createBy], [createDate], [isDelete],[scenarioId],[sektorId])
		VALUES (@colVar15, @createBy,CURRENT_TIMESTAMP,0,@scenario2,@colVar14)
	END

END
