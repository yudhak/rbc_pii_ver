﻿
ALTER PROCEDURE [dbo].[DuplicateRiskMatrixBasedScenario3]
	-- Add the parameters for the stored procedure here
	 @scenario1 int,
	 @scenario2 int,
	 @createBy int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert table risk matrix
	INSERT INTO tblRiskMatrixs (scenarioId, createBy, createDate, isDelete) values (@scenario2, @createBy, CURRENT_TIMESTAMP, 0)
	DECLARE @RiskMatrixId  int
	SELECT @RiskMatrixId = id FROM tblRiskMatrixs WHERE scenarioId = @scenario2 and isDelete = 0

	INSERT into tblProjectScenarioVirtual(projectId, scenarioId)  
	SELECT A.projectId AS project , @scenario2
	FROM tblScenarioDetails A, tblScenarioDetails B
	WHERE A.scenarioId = @scenario2 and B.scenarioId = @scenario1 and A.isDelete =0 and b.isDelete = 0
	AND A.projectId = B.projectId

	Declare @maksProjectId int
	Declare @minProjectId int

	Select @maksProjectId = MAX(b.id), @minProjectId = MIN(b.id)
	From tblProjectScenarioVirtual as b
	Where scenarioId = @scenario2
	print @minProjectId
	print @maksProjectId
	WHILE(@minProjectId<=@maksProjectId)
	BEGIN
		DECLARE @colVar int
		SET @colVar = 0
		SELECT @colVar = projectId FROM tblProjectScenarioVirtual WHERE id = @minProjectId and scenarioId = @scenario2

		-- Generate Correlated Project
		DECLARE @Statusid2 int
		DECLARE @Statusid1 int
		DECLARE @Statusidnull int
		DECLARE @Statusfinal int
		SET @Statusid2 = NULL
		SET @Statusid1 = NULL
		SET @Statusidnull = NULL
		SET @Statusfinal = NULL
		DECLARE @colVar12 int
		SET @colVar12 = 0
		SELECT @Statusid2  = statusId
		FROM tblCorrelatedProjects
		WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0  and statusId = 2

		SELECT  @Statusid1 = statusId
		FROM tblCorrelatedProjects
		WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0  and statusId = 1

	
		IF (@Statusid2 = 2)
		 BEGIN  
			SET @Statusfinal = @Statusid2
			SET @Statusidnull = @Statusid2

		INSERT INTO [dbo].[tblCorrelatedProjects] ([projectId],  [scenarioId], [sektorId], [createBy],[createDate], [isDelete])
		SELECT [projectId],  @scenario2, [sektorId], [createBy],[createDate], [isDelete]
		FROM tblCorrelatedProjects
		WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0 AND statusId = @Statusfinal
		SELECT @colVar12 = id FROM tblCorrelatedProjects WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0 AND statusId = @Statusfinal


			END  
		 ELSE IF (@Statusid1 = 1)
		 BEGIN
			SET @Statusfinal = @Statusid1

		INSERT INTO [dbo].[tblCorrelatedProjects] ([projectId],  [scenarioId], [sektorId], [createBy],[createDate], [isDelete])
		SELECT [projectId],  @scenario2, [sektorId], [createBy],[createDate], [isDelete]
		FROM tblCorrelatedProjects
		WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0 AND statusId = @Statusfinal
		SELECT @colVar12 = id FROM tblCorrelatedProjects WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0 AND statusId = @Statusfinal

			END
		ELSE 
		 BEGIN
		INSERT INTO [dbo].[tblCorrelatedProjects] ([projectId],  [scenarioId], [sektorId], [createBy],[createDate], [isDelete])
		SELECT [projectId],  @scenario2, [sektorId], [createBy],[createDate], [isDelete]
		FROM tblCorrelatedProjects
		WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0 AND statusId IS NULL
		SELECT @colVar12 = id FROM tblCorrelatedProjects WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0 AND statusId IS NULL

		END

		DECLARE @colVar13 int
		SELECT @colVar13 = id FROM tblCorrelatedProjects WHERE scenarioId = @scenario2 and projectId = @colVar and isDelete = 0

		INSERT INTO [dbo].tblCorrelatedProjectDetails([correlatedProjectId],[projectIdRow],[projectIdCol],[correlationMatrixId],[createBy],[createDate],[updateBy],[updateDate] ,[deleteDate],[isDelete])
		SELECT @colVar13,[projectIdRow],[projectIdCol],[correlationMatrixId] ,[createBy],[createDate],[updateBy],[updateDate] ,[deleteDate],[isDelete]
		FROM tblCorrelatedProjectDetails
		WHERE correlatedProjectId = @colVar12 and isDelete = 0;

		-- Filter status Id
		SET @Statusid2 = NULL
		SET @Statusid1 = NULL
		SET @Statusidnull = NULL
		SET @Statusfinal = NULL
		DECLARE @colVar3 int
		SELECT @Statusid2  = statusId
		FROM tblRiskMatrixProjects
		WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0  and statusId = 2

		SELECT  @Statusid1 = statusId
		FROM tblRiskMatrixProjects
		WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0  and statusId = 1

		SELECT @Statusidnull  = statusId
		FROM tblRiskMatrixProjects
		WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0  and statusId is null

		IF (@Statusid2 = 2)
		 BEGIN  
			SET @Statusfinal = @Statusid2
			SET @Statusidnull = @Statusid2

			INSERT INTO [dbo].[tblRiskMatrixProjects] ([projectId], [riskMatrixId], [scenarioId], [createBy],[createDate], [isDelete],[statusId])
		SELECT [projectId], @RiskMatrixId, @scenario2, [createBy],[createDate], [isDelete],@Statusidnull
		FROM tblRiskMatrixProjects
		WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0 and statusId = @Statusfinal

		
		SELECT @colVar3 = id FROM tblRiskMatrixProjects WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0 and statusId = @Statusfinal

			END  
		 ELSE IF (@Statusid1 = 1)
		 BEGIN
			SET @Statusfinal = @Statusid1

			INSERT INTO [dbo].[tblRiskMatrixProjects] ([projectId], [riskMatrixId], [scenarioId], [createBy],[createDate], [isDelete],[statusId])
		SELECT [projectId], @RiskMatrixId, @scenario2, [createBy],[createDate], [isDelete],@Statusidnull
		FROM tblRiskMatrixProjects
		WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0 and statusId = @Statusfinal

		
		SELECT @colVar3 = id FROM tblRiskMatrixProjects WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0 and statusId = @Statusfinal

			END
		ELSE 
		 BEGIN
			SET @Statusfinal = @Statusidnull

			INSERT INTO [dbo].[tblRiskMatrixProjects] ([projectId], [riskMatrixId], [scenarioId], [createBy],[createDate], [isDelete],[statusId])
		SELECT [projectId], @RiskMatrixId, @scenario2, [createBy],[createDate], [isDelete],@Statusidnull
		FROM tblRiskMatrixProjects
		WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0 and statusId is null
		SELECT @colVar3 = id FROM tblRiskMatrixProjects WHERE scenarioId = @scenario1 and projectId = @colVar and isDelete = 0 and statusId is null

			END

		-- Generate risk matrix project
		
		DECLARE @colVar4 int
		SELECT @colVar4 = id FROM tblRiskMatrixProjects WHERE scenarioId = @scenario2 and projectId = @colVar and isDelete = 0

		INSERT INTO [dbo].tblStageTahunRiskMatrixs([riskMatrixProjectId],[tahun],[stageId] ,[createBy],[createDate],[updateBy],[updateDate],[isUpdate] ,[deleteDate],[isDelete],[maximumNilaiExpose])
		SELECT @colVar4,[tahun],[stageId] ,[createBy],[createDate],[updateBy],[updateDate],[isUpdate] ,[deleteDate],[isDelete],[maximumNilaiExpose]
		FROM tblStageTahunRiskMatrixs
		WHERE riskMatrixProjectId = @colVar3 and isDelete = 0;

		INSERT INTO [dbo].tblStageTahunRiskMatrixDetails([riskMatrixProjectId],[nilaiExpose],[stageTahunRiskMatrixId],[riskRegistrasiId],[likehoodDetailId],[warnaExpose],[warnaLikelihood] ,[createBy],[createDate],[updateBy],[updateDate],[isUpdate] ,[deleteDate],[isDelete])
		SELECT @colVar4,[nilaiExpose],[stageTahunRiskMatrixId],[riskRegistrasiId],[likehoodDetailId],[warnaExpose],[warnaLikelihood] ,[createBy],[createDate],[updateBy],[updateDate],[isUpdate] ,[deleteDate],[isDelete]
		FROM tblStageTahunRiskMatrixDetails
		WHERE riskMatrixProjectId = @colVar3 and isDelete = 0;

		DECLARE @maksIdTahun  int
		DECLARE @minIdTahun  int	
	
		INSERT into tblStageTahunRiskMatrixScenarioVirtual(tahun, scenarioId)
		SELECT tahun, @scenario2 FROM tblStageTahunRiskMatrixs where riskMatrixProjectId = @colVar3 and isDelete = 0

		select @maksIdTahun = Max(id), @minIdTahun = Min(id)  from tblStageTahunRiskMatrixScenarioVirtual where scenarioId = @scenario2

		WHILE(@minIdTahun<=@maksIdTahun)
		BEGIN

			DECLARE @colVar5 int
			DECLARE @colVar6 int
			DECLARE @colVar7 int

			SELECT @colVar5 = tahun FROM tblStageTahunRiskMatrixScenarioVirtual WHERE id = @minIdTahun and scenarioId = @scenario2
			SELECT @colVar6 = id FROM tblStageTahunRiskMatrixs WHERE riskMatrixProjectId = @colVar3 and tahun = @colVar5 and isDelete = 0
			SELECT @colVar7 = id FROM tblStageTahunRiskMatrixs WHERE riskMatrixProjectId = @colVar4 and tahun = @colVar5 and isDelete = 0

			UPDATE tblStageTahunRiskMatrixDetails
			SET stageTahunRiskMatrixId = @colVar7
			WHERE riskMatrixProjectId = @colVar4 and stageTahunRiskMatrixId = @colVar6;

			---increment while loop stage tahun risk matrix
			SET @minIdTahun=@minIdTahun+1
			
		END
		-- delete last stage tahun risk matrix
		delete tblStageTahunRiskMatrixScenarioVirtual where scenarioId = @scenario2
		---increment while loop risk matrix project
			SET @minProjectId=@minProjectId+1
	END
	
	delete tblProjectScenarioVirtual where scenarioId = @scenario2
	
	
	INSERT into tblProjectScenarioLeftVirtual (projectId, scenarioId)
	SELECT projectId, @scenario2 FROM tblScenarioDetails where scenarioId = @scenario2 and isDelete = 0

	INSERT into tblProjectScenarioRightVirtual (projectId, scenarioId)
	SELECT projectId, @scenario1 FROM tblScenarioDetails where scenarioId = @scenario1 and isDelete = 0

	DECLARE @minScenarioDetailsTemporary int
	DECLARE @maxScenarioDetailsTemporary int

	INSERT into tblProjectScenarioLeftOuterVirtual (projectId,scenarioId)
	SELECT a.projectId, @scenario2
	FROM tblProjectScenarioLeftVirtual A
	LEFT JOIN tblProjectScenarioRightVirtual B
	ON A.projectId = B .projectId
	WHERE B.ProjectId is NULL

	Select @minScenarioDetailsTemporary = min(id),  @maxScenarioDetailsTemporary = max(id) 
	From tblProjectScenarioLeftOuterVirtual
	Where scenarioId = @scenario2


	WHILE(@minScenarioDetailsTemporary<=@maxScenarioDetailsTemporary)
	BEGIN
		
		DECLARE @colVar2 int
		SELECT @colVar2 = projectId FROM tblProjectScenarioLeftOuterVirtual WHERE id = @minScenarioDetailsTemporary and scenarioId = @scenario2
		-- Generate Risk Matrix Project
		INSERT INTO [dbo].[tblRiskMatrixProjects] ([projectId], [riskMatrixId], [scenarioId], [createBy],[createDate], [isDelete])
		VALUES (@colVar2, @RiskMatrixId, @scenario2, @createBy,CURRENT_TIMESTAMP, 0)
		-- Generate Correlated Project
		DECLARE @colVar11 int
		SELECT @colVar11 = sektorId FROM tblProjects WHERE id = @colVar2
		INSERT INTO [dbo].tblCorrelatedProjects([projectId],  [scenarioId], [sektorId], [createBy],[createDate], [isDelete])
		VALUES (@colVar2, @scenario2, @colVar11, @createBy,CURRENT_TIMESTAMP, 0)
			---increment while loop 
		SET @minScenarioDetailsTemporary=@minScenarioDetailsTemporary+1
	END
	delete tblProjectScenarioLeftVirtual where scenarioId = @scenario2
	delete tblProjectScenarioLeftOuterVirtual where scenarioId = @scenario2
	delete tblProjectScenarioRightVirtual where scenarioId = @scenario1
	-- Correlated Sektor Generate

	Declare @maksSektor int
	Declare @minSektor int

	INSERT into tblSektorScenarioLeftVirtual(sektorId, scenarioId)
	SELECT B.sektorId, @scenario2 FROM tblScenarioDetails A
	LEFT OUTER JOIN tblProjects B
	ON A.projectId = B.id
	where A.scenarioId = @scenario2 and A.isDelete = 0
	group by B.sektorId

	Select @maksSektor = MAX(b.id), @minSektor = MIN(b.id)
	From tblSektorScenarioLeftVirtual as b
	Where scenarioId = @scenario2

	WHILE(@minSektor<=@maksSektor)
	BEGIN
		
		DECLARE @colVar8 int
		SELECT @colVar8 = sektorId FROM tblSektorScenarioLeftVirtual WHERE id = @minSektor and scenarioId = @scenario2

		SET @Statusid2 = NULL
		SET @Statusid1 = NULL
		SET @Statusidnull = NULL
		SET @Statusfinal = NULL
		DECLARE @colVar9 int
		SET @colVar9 = 0
		SELECT @Statusid2  = statusId
		FROM tblCorrelatedSektors
		WHERE scenarioId = @scenario1 and sektorId = @colVar8 and isDelete = 0  and statusId = 2

		SELECT  @Statusid1 = statusId
		FROM tblCorrelatedSektors
		WHERE scenarioId = @scenario1 and sektorId = @colVar8 and isDelete = 0  and statusId = 1

		SELECT @Statusidnull  = statusId
		FROM tblCorrelatedSektors
		WHERE scenarioId = @scenario1 and sektorId = @colVar8 and isDelete = 0  and statusId is null

		IF (@Statusid2 = 2)
		 BEGIN  
			SET @Statusfinal = @Statusid2
			SET @Statusidnull = @Statusid2

			INSERT INTO [dbo].[tblCorrelatedSektors] ([namaSektor], [createBy], [createDate], [updateBy],[updateDate], [isDelete],[deleteDate],[scenarioId],[sektorId],[statusId])
		SELECT [namaSektor], [createBy], [createDate], [updateBy],[updateDate], [isDelete],[deleteDate],@scenario2,[sektorId],@Statusidnull
		FROM [tblCorrelatedSektors]
		WHERE scenarioId = @scenario1 and sektorId = @colVar8 and isDelete = 0 and statusId = @Statusfinal

		
		SELECT @colVar9 = id FROM tblCorrelatedSektors WHERE scenarioId = @scenario1 and sektorId = @colVar8 and isDelete = 0 and statusId = @Statusfinal

			END  
		 ELSE IF (@Statusid1 = 1)
		 BEGIN
			SET @Statusfinal = @Statusid1

			INSERT INTO [dbo].[tblCorrelatedSektors] ([namaSektor], [createBy], [createDate], [updateBy],[updateDate], [isDelete],[deleteDate],[scenarioId],[sektorId],[statusId])
		SELECT [namaSektor], [createBy], [createDate], [updateBy],[updateDate], [isDelete],[deleteDate],@scenario2,[sektorId],@Statusidnull
		FROM [tblCorrelatedSektors]
		WHERE scenarioId = @scenario1 and sektorId = @colVar8 and isDelete = 0 and statusId = @Statusfinal

		
		SELECT @colVar9 = id FROM tblCorrelatedSektors WHERE scenarioId = @scenario1 and sektorId = @colVar8 and isDelete = 0 and statusId = @Statusfinal

			END
		ELSE 
		 BEGIN
			SET @Statusfinal = @Statusidnull

			INSERT INTO [dbo].[tblCorrelatedSektors] ([namaSektor], [createBy], [createDate], [updateBy],[updateDate], [isDelete],[deleteDate],[scenarioId],[sektorId],[statusId])
		SELECT [namaSektor], [createBy], [createDate], [updateBy],[updateDate], [isDelete],[deleteDate],@scenario2,[sektorId],@Statusidnull
		FROM [tblCorrelatedSektors]
		WHERE scenarioId = @scenario1 and sektorId = @colVar8 and isDelete = 0 and statusId IS NULL

		
		SELECT @colVar9 = id FROM tblCorrelatedSektors WHERE scenarioId = @scenario1 and sektorId = @colVar8 and isDelete = 0 and statusId IS NULL

			END


		
		DECLARE @colVar10 int
		SELECT @colVar10 = id FROM tblCorrelatedSektors WHERE scenarioId = @scenario2 and sektorId = @colVar8 and isDelete = 0

		INSERT INTO [dbo].tblCorrelatedSektorDetails([correlatedSektorId],[riskRegistrasiIdRow],[correlationMatrixId],[riskRegistrasiIdCol],[createBy],[createDate],[updateBy],[updateDate],[deleteDate],[isDelete])
		SELECT @colVar10,[riskRegistrasiIdRow],[correlationMatrixId],[riskRegistrasiIdCol],[createBy],[createDate],[updateBy],[updateDate],[deleteDate],[isDelete]
		FROM tblCorrelatedSektorDetails
		WHERE correlatedSektorId = @colVar9 and isDelete = 0;

		SET @minSektor=@minSektor+1
	END	
	
	Declare @maksSektorOuter int
	Declare @minSektorOuter int

	INSERT into tblSektorScenarioRightVirtual(sektorId, scenarioId)
	SELECT B.sektorId, @scenario1 FROM tblScenarioDetails A
	LEFT OUTER JOIN tblProjects B
	ON A.projectId = B.id
	where A.scenarioId = @scenario1 and A.isDelete = 0
	group by B.sektorId				
	

	DECLARE @RowsToProcess4  int
	DECLARE @CurrentRow4 int
	INSERT into tblSektorScenarioLeftOuterVirtual (sektorId,scenarioId)
	SELECT A.sektorId, @scenario2
	FROM tblSektorScenarioLeftVirtual A
	LEFT JOIN tblSektorScenarioRightVirtual B
	ON A.sektorId = B .sektorId
	WHERE B.id is NULL

	Select @maksSektorOuter = MAX(b.id), @minSektorOuter = MIN(b.id)
	From tblSektorScenarioLeftOuterVirtual as b
	Where scenarioId = @scenario2


	WHILE(@minSektorOuter<=@maksSektorOuter)
	BEGIN
		
		DECLARE @colVar14 int
		SELECT @colVar14 = sektorId FROM tblSektorScenarioLeftOuterVirtual WHERE id = @minSektorOuter and scenarioId = @scenario2
		-- Do your work here
		DECLARE @colVar15 varchar(500)
		SELECT @colVar15 = namaSektor FROM tblSektors WHERE id = @colVar14
		INSERT INTO [dbo].tblCorrelatedSektors([namaSektor], [createBy], [createDate], [isDelete],[scenarioId],[sektorId])
		VALUES (@colVar15, @createBy,CURRENT_TIMESTAMP,0,@scenario2,@colVar14)

		SET @minSektorOuter=@minSektorOuter+1
	END

	delete tblSektorScenarioLeftVirtual where scenarioId = @scenario2
	delete tblSektorScenarioLeftOuterVirtual where scenarioId = @scenario2
	delete tblSektorScenarioRightVirtual where scenarioId = @scenario1

	--truncate table tblSektorScenarioLeftVirtual
	--truncate table tblSektorScenarioLeftOuterVirtual
	--truncate table tblSektorScenarioRightVirtual
END

