USE [master]
GO
/****** Object:  Database [RBC_PII_USER]    Script Date: 21/05/2018 10.28.52 ******/
CREATE DATABASE [RBC_PII_USER]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'RBC_PII_USER', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\RBC_PII_USER.mdf' , SIZE = 10240KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'RBC_PII_USER_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\RBC_PII_USER_log.ldf' , SIZE = 16576KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [RBC_PII_USER] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RBC_PII_USER].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RBC_PII_USER] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [RBC_PII_USER] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [RBC_PII_USER] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [RBC_PII_USER] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [RBC_PII_USER] SET ARITHABORT OFF 
GO
ALTER DATABASE [RBC_PII_USER] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [RBC_PII_USER] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [RBC_PII_USER] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [RBC_PII_USER] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [RBC_PII_USER] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [RBC_PII_USER] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [RBC_PII_USER] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [RBC_PII_USER] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [RBC_PII_USER] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [RBC_PII_USER] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [RBC_PII_USER] SET  DISABLE_BROKER 
GO
ALTER DATABASE [RBC_PII_USER] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [RBC_PII_USER] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [RBC_PII_USER] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [RBC_PII_USER] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [RBC_PII_USER] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [RBC_PII_USER] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [RBC_PII_USER] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [RBC_PII_USER] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [RBC_PII_USER] SET  MULTI_USER 
GO
ALTER DATABASE [RBC_PII_USER] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [RBC_PII_USER] SET DB_CHAINING OFF 
GO
ALTER DATABASE [RBC_PII_USER] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [RBC_PII_USER] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [RBC_PII_USER]
GO
/****** Object:  Table [dbo].[tblUsers]    Script Date: 5/21/2018 11:22:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUsers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userName] [varchar](150) NOT NULL,
	[password] [varchar](250) NOT NULL,
	[status] [bit] NULL,
	[language] [nvarchar](255) NULL,
	[createDate] [varchar](8) NOT NULL,
	[createTime] [varchar](8) NULL,
	[expirydate] [varchar](8) NULL,
	[lastupdatedate] [varchar](8) NULL,
	[employeeId] [int] NULL,
	[email] [varchar](250) NULL,
	[roleId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblUsers] ON 

INSERT [dbo].[tblUsers] ([id], [userName], [password], [status], [language], [createDate], [createTime], [expirydate], [lastupdatedate], [employeeId], [email], [roleId]) VALUES (1, N'admin@test.com', N'tEyWQ3Fb55pFjqIxM+olsRliCf7vo514', 1, N'en', N'20170531', N'1038', NULL, NULL, NULL, N'kiki.wisaka@mii.co.id', 2)
INSERT [dbo].[tblUsers] ([id], [userName], [password], [status], [language], [createDate], [createTime], [expirydate], [lastupdatedate], [employeeId], [email], [roleId]) VALUES (2, N'test@test.com', N'tEyWQ3Fb55pFjqIxM+olsRliCf7vo514', 1, N'en', N'20171216', N'0517', NULL, NULL, NULL, N'kiki.wisaka@mii.co.id', 2)
INSERT [dbo].[tblUsers] ([id], [userName], [password], [status], [language], [createDate], [createTime], [expirydate], [lastupdatedate], [employeeId], [email], [roleId]) VALUES (3, N'user1@test.com', N'tEyWQ3Fb55pFjqIxM+olsRliCf7vo514', 1, N'en', N'20180125', N'1416', NULL, NULL, NULL, N'kiki.wisaka@mii.co.id', 3)
INSERT [dbo].[tblUsers] ([id], [userName], [password], [status], [language], [createDate], [createTime], [expirydate], [lastupdatedate], [employeeId], [email], [roleId]) VALUES (4, N'headuser@test.com', N'tEyWQ3Fb55pFjqIxM+olsRliCf7vo514', 1, N'en', N'20180125', N'1416', NULL, NULL, NULL, N'sutrisno@mii.co.id', 4)
INSERT [dbo].[tblUsers] ([id], [userName], [password], [status], [language], [createDate], [createTime], [expirydate], [lastupdatedate], [employeeId], [email], [roleId]) VALUES (5, N'adminapprover@test.com', N'tEyWQ3Fb55pFjqIxM+olsRliCf7vo514', 1, N'en', N'20180125', N'1416', NULL, NULL, NULL, N'Vika.Andini@mii.co.id', 5)
INSERT [dbo].[tblUsers] ([id], [userName], [password], [status], [language], [createDate], [createTime], [expirydate], [lastupdatedate], [employeeId], [email], [roleId]) VALUES (6, N'Superadmin@test.com', N'tEyWQ3Fb55pFjqIxM+olsRliCf7vo514', 1, N'en', N'20180125', N'1416', NULL, NULL, NULL, N'Vika.Andini@mii.co.id', 1)
SET IDENTITY_INSERT [dbo].[tblUsers] OFF
