﻿using HR.Domain;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Data.Entity;

namespace HR.Infrastructure.Repositories
{
    public class CorrelatedSektorDetailRepository : ICorrelatedSektorDetailRepository
    {
        private readonly IDatabaseContext _databaseContext;
        public CorrelatedSektorDetailRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public CorrelatedSektorDetail Get(int id)
        {
            return _databaseContext.CorrelatedSektorDetails.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<CorrelatedSektorDetail> GetByCorrelatedSectorRiskRegistrasiCol(int correlatedSektorId, int riskRegistrasiCol)
        {
            return _databaseContext.CorrelatedSektorDetails.Where(x => x.CorrelatedSektorId == correlatedSektorId && x.RiskRegistrasiIdCol == riskRegistrasiCol && x.IsDelete == false).ToList();
        }

        public IEnumerable<CorrelatedSektorDetail> GetByCorrelatedSectorRiskRegistrasiRow(int correlatedSektorId, int riskRegistrasiRow)
        {
            return _databaseContext.CorrelatedSektorDetails.Where(x => x.CorrelatedSektorId == correlatedSektorId && x.RiskRegistrasiIdRow == riskRegistrasiRow && x.IsDelete == false).ToList();
        }

        public IEnumerable<CorrelatedSektorDetail> GetByCorrelatedSektorId(int correlatedSektorId)
        {
            return _databaseContext.CorrelatedSektorDetails.Where(x => x.CorrelatedSektorId == correlatedSektorId).ToList();
        }

        public IEnumerable<CorrelatedSektorDetailLite> GetByCorrelatedSektorIdLite(int correlateedSektorId)
        {
            return _databaseContext.CorrelatedSektorDetails.Include(x=> x.CorrelationMatrix).Where(x => x.CorrelatedSektorId == correlateedSektorId)
                .Select(x=> new CorrelatedSektorDetailLite(){ Id = x.Id, CorrelatedSektorId = x.CorrelatedSektorId, RiskRegistrasiIdRow = x.RiskRegistrasiIdRow,
                    RiskRegistrasiIdCol = x.RiskRegistrasiIdCol, CorrelationMatrixId = x.CorrelationMatrixId, IsDelete = x.IsDelete, CorrelationMatrixValue = x.CorrelationMatrix.Nilai}).ToList();
        }

        public IEnumerable<CorrelatedSektorDetail> GetOldData(int correlatedSektorId)
        {
            return _databaseContext.CorrelatedSektorDetails.Where(x => x.CorrelatedSektorId == correlatedSektorId).ToList();
        }

        public void Insert(IList<CorrelatedSektorDetail> collection)
        {
            foreach (var item in collection)
            {
                this.Insert(item);
            }
        }

        public void Insert(CorrelatedSektorDetail model)
        {
            _databaseContext.CorrelatedSektorDetails.Add(model);
        }

        public CorrelatedSektorDetail IsExisitOnAdding(int correlataedSektor, int riskRegistrasiIdRow, int riskRegistrasiIdCol)
        {
            return _databaseContext.CorrelatedSektorDetails.Where(x => x.CorrelatedSektorId == correlataedSektor && x.RiskRegistrasiIdRow == riskRegistrasiIdRow && x.RiskRegistrasiIdCol == riskRegistrasiIdCol).FirstOrDefault();
        }

        public void Update(CorrelatedSektorDetail model)
        {
            _databaseContext.CorrelatedSektorDetails.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }
    }
}
