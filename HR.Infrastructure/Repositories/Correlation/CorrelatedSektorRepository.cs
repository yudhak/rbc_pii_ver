﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace HR.Infrastructure.Repositories
{
    public class CorrelatedSektorRepository : ICorrelatedSektorRepository
    {
        private readonly IDatabaseContext _databaseContext;
        private readonly IUserRepository _userRepository;
        private readonly IScenarioRepository _scenarioRepository;

        public CorrelatedSektorRepository(IDatabaseContext databaseContext, IUserRepository userRepository, IScenarioRepository scenarioRepository)
        {
            _databaseContext = databaseContext;
            _userRepository = userRepository;
            _scenarioRepository = scenarioRepository;
        }
        
        public CorrelatedSektor Get(int id)
        {
            return _databaseContext.CorrelatedSektors.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<CorrelatedSektor> GetAll()
        {
            return _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false).ToList();
        }

        public IEnumerable<CorrelatedSektor> GetByScenarioDefaultId(int scenarioId, string keyword)
        {
            IEnumerable<CorrelatedSektor> sektorList = _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false && x.ScenarioId == scenarioId).ToList();

            IList<CorrelatedSektor> result = new List<CorrelatedSektor>();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (sektorList.Count() > 0)
                {
                    foreach (var item in sektorList)
                    {
                        if (item.NamaSektor.ToLower().Contains(keyword.ToLower()))
                            result.Add(item);
                    }
                }
            }
            else
            {
                result = sektorList.ToList();
            }
            #endregion filter

            return result;
            //return _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false).ToList();
        }

        public IEnumerable<CorrelatedSektor> GetByScenarioDefaultId(int scenarioId, string keyword, int user)
        {

            IEnumerable<CorrelatedSektor> sektorList = new List<CorrelatedSektor>();
            IList<CorrelatedSektor> result = new List<CorrelatedSektor>();
            IList<CorrelatedSektor> sektorListDraft = new List<CorrelatedSektor>();

            if (scenarioId != 0)
            {
                var scenarioSelect = _scenarioRepository.Get(scenarioId);
                scenarioId = scenarioSelect.Id;
            }
            else
            {
                scenarioId = 0;
            }
            
            var userGet = _userRepository.Get(user);

            if (userGet.RoleId == 5 || userGet.RoleId == 3)
            {
                var list = from scenario in _scenarioRepository.GetAll().ToList()
                                  .Where(mapping => mapping.IsDelete == false && mapping.StatusId != 4)
                           from correlatedSektor in _databaseContext.CorrelatedSektors.ToList()
                         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId != null && mapping.StatusId != 4)
                           select correlatedSektor;
                foreach (var item in list)
                {
                    sektorListDraft.Add(item);
                }
                //sektorList = _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false  && x.StatusId != null && x.StatusId != 4).OrderBy(x => x.Id).ToList();
            }
            else
            {
                var list = from scenario in _scenarioRepository.GetAll().ToList()
                                  .Where(mapping => mapping.IsDelete == false && mapping.Id == scenarioId && mapping.StatusId != 4)
                           from correlatedSektor in _databaseContext.CorrelatedSektors.ToList()
                         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId != null && mapping.StatusId != 4)
                           select correlatedSektor;
                //var list = from scenario in _scenarioRepository.GetAll().ToList()
                //                  .Where(mapping => mapping.IsDelete == false && mapping.Id == scenarioId && mapping.CreateBy == user)
                //           from correlatedSektor in _databaseContext.CorrelatedSektors.ToList()
                //         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId != null && mapping.StatusId != 4)
                //           select correlatedSektor;
                foreach (var item in list)
                {
                    sektorListDraft.Add(item);
                }
                //sektorList = _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false && x.ScenarioId == scenarioId && x.StatusId != null && x.StatusId != 4).OrderBy(x => x.Id).ToList();
            }

            //foreach (var item in sektorList)
            //{
            //    sektorListDraft.Add(item);
            //}
            if (userGet.RoleId == 5 || userGet.RoleId == 3)
            {
                //sektorListDraft = _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false && x.StatusId == null).OrderByDescending(x => x.Id).ToList();
                var list = from scenario in _scenarioRepository.GetAll().ToList()
                                  .Where(mapping => mapping.IsDelete == false && mapping.StatusId != 4)
                           from correlatedSektor in _databaseContext.CorrelatedSektors.ToList()
                         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId == null)
                           select correlatedSektor;
                foreach (var item in list)
                {
                    sektorListDraft.Add(item);
                }
            }
            else
            {
                var list = from scenario in _scenarioRepository.GetAll().ToList()
                                  .Where(mapping => mapping.IsDelete == false && mapping.Id == scenarioId && mapping.CreateBy == user && mapping.StatusId != 4)
                           from correlatedSektor in _databaseContext.CorrelatedSektors.ToList()
                         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId == null)
                           select correlatedSektor;
                foreach (var item in list)
                {
                    sektorListDraft.Add(item);
                }
                //sektorListDraft = _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false && x.ScenarioId == scenarioId  && x.StatusId == null).ToList();
            }

            // sektorListDraft.OrderBy(x => x.StatusId == 2);

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (sektorListDraft.Count() > 0)
                {
                    foreach (var item in sektorListDraft)
                    {
                        if (item.NamaSektor.ToLower().Contains(keyword.ToLower()))
                            result.Add(item);
                    }
                }
            }
            else
            {
                result = sektorListDraft.ToList();
            }
            #endregion filter

            return result;
            //return _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false).ToList();
        }

        public IEnumerable<CorrelatedSektor> GetByScenarioDefaultId(int scenarioId, string keyword,int id, int user)
        {

            IEnumerable<CorrelatedSektor> sektorList = new List<CorrelatedSektor>();
            IList<CorrelatedSektor> result = new List<CorrelatedSektor>();
            IList<CorrelatedSektor> sektorListDraft = new List<CorrelatedSektor>();

            if (scenarioId != 0)
            {
                var scenarioSelect = _scenarioRepository.Get(scenarioId);
                scenarioId = scenarioSelect.Id;
            }
            else
            {
                scenarioId = 0;
            }

            var userGet = _userRepository.Get(user);

            if (userGet.RoleId == 5 || userGet.RoleId == 3)
            {
                var list = from scenario in _scenarioRepository.GetAll().ToList()
                                  .Where(mapping => mapping.IsDelete == false && mapping.StatusId != 4)
                           from correlatedSektor in _databaseContext.CorrelatedSektors.ToList()
                         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId != null && mapping.StatusId != 4)
                           select correlatedSektor;
                foreach (var item in list)
                {
                    sektorListDraft.Add(item);
                }
                //sektorList = _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false  && x.StatusId != null && x.StatusId != 4).OrderBy(x => x.Id).ToList();
            }
            else
            {
                var list = from scenario in _scenarioRepository.GetAll().ToList()
                                  .Where(mapping => mapping.IsDelete == false && mapping.Id == scenarioId && mapping.StatusId != 4)
                           from correlatedSektor in _databaseContext.CorrelatedSektors.ToList()
                         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId != null && mapping.StatusId != 4)
                           select correlatedSektor;
                //var list = from scenario in _scenarioRepository.GetAll().ToList()
                //                  .Where(mapping => mapping.IsDelete == false && mapping.Id == scenarioId && mapping.CreateBy == user)
                //           from correlatedSektor in _databaseContext.CorrelatedSektors.ToList()
                //         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId != null && mapping.StatusId != 4)
                //           select correlatedSektor;
                foreach (var item in list)
                {
                    sektorListDraft.Add(item);
                }
                //sektorList = _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false && x.ScenarioId == scenarioId && x.StatusId != null && x.StatusId != 4).OrderBy(x => x.Id).ToList();
            }

            //foreach (var item in sektorList)
            //{
            //    sektorListDraft.Add(item);
            //}
            if (userGet.RoleId == 5 || userGet.RoleId == 3)
            {
                //sektorListDraft = _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false && x.StatusId == null).OrderByDescending(x => x.Id).ToList();
                var list = from scenario in _scenarioRepository.GetAll().ToList()
                                  .Where(mapping => mapping.IsDelete == false && mapping.StatusId != 4)
                           from correlatedSektor in _databaseContext.CorrelatedSektors.ToList()
                         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId == null)
                           select correlatedSektor;
                foreach (var item in list)
                {
                    sektorListDraft.Add(item);
                }
            }
            else
            {
                var list = from scenario in _scenarioRepository.GetAll().ToList()
                                  .Where(mapping => mapping.IsDelete == false && mapping.Id == scenarioId && mapping.CreateBy == user && mapping.StatusId != 4)
                           from correlatedSektor in _databaseContext.CorrelatedSektors.ToList()
                         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId == null)
                           select correlatedSektor;
                foreach (var item in list)
                {
                    sektorListDraft.Add(item);
                }
                //sektorListDraft = _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false && x.ScenarioId == scenarioId  && x.StatusId == null).ToList();
            }

            // sektorListDraft.OrderBy(x => x.StatusId == 2);

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (sektorListDraft.Count() > 0)
                {
                    foreach (var item in sektorListDraft)
                    {
                        if (id == 1)
                        {
                            if (item.NamaSektor.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 2)
                        {
                            var scenarioCS = _scenarioRepository.Get(item.ScenarioId);
                            if (scenarioCS.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 3)
                        {
                            if (item.Status != null)
                            {
                                if (item.Status.StatusDescription.ToLower().Contains(keyword.ToLower()))
                                    result.Add(item);
                            }
                            if (item.Status == null && "draft".Contains(keyword.ToLower()))
                            {
                                result.Add(item);

                            }
                        }

                    }
                }
            }
            else
            {
                result = sektorListDraft.ToList();
            }
            #endregion filter

            return result;
            //return _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false).ToList();
        }

        public IEnumerable<CorrelatedSektor> GetByScenarioDefaultId(int scenarioId, string keyword, int id,int id2, int user)
        {

            IEnumerable<CorrelatedSektor> sektorList = new List<CorrelatedSektor>();
            IList<CorrelatedSektor> result = new List<CorrelatedSektor>();
            IList<CorrelatedSektor> sektorListDraft = new List<CorrelatedSektor>();

            if (scenarioId != 0)
            {
                var scenarioSelect = _scenarioRepository.Get(scenarioId);
                scenarioId = scenarioSelect.Id;
            }
            else
            {
                scenarioId = 0;
            }

            var userGet = _userRepository.Get(user);

            if (userGet.RoleId == 5 || userGet.RoleId == 3)
            {
                var list = from scenario in _scenarioRepository.GetAll().ToList()
                                  .Where(mapping => mapping.IsDelete == false && mapping.StatusId != 4)
                           from correlatedSektor in _databaseContext.CorrelatedSektors.ToList()
                         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId != null && mapping.StatusId != 4)
                           select correlatedSektor;
                foreach (var item in list)
                {
                    sektorListDraft.Add(item);
                }
                //sektorList = _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false  && x.StatusId != null && x.StatusId != 4).OrderBy(x => x.Id).ToList();
            }
            else
            {
                var list = from scenario in _scenarioRepository.GetAll().ToList()
                                  .Where(mapping => mapping.IsDelete == false && mapping.Id == scenarioId && mapping.StatusId != 4).OrderBy(x => x.NamaScenario)
                           from correlatedSektor in _databaseContext.CorrelatedSektors.ToList()
                         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId != null && mapping.StatusId != 4)
                           select correlatedSektor;
                //var list = from scenario in _scenarioRepository.GetAll().ToList()
                //                  .Where(mapping => mapping.IsDelete == false && mapping.Id == scenarioId && mapping.CreateBy == user)
                //           from correlatedSektor in _databaseContext.CorrelatedSektors.ToList()
                //         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId != null && mapping.StatusId != 4)
                //           select correlatedSektor;
                foreach (var item in list)
                {
                    sektorListDraft.Add(item);
                }
                //sektorList = _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false && x.ScenarioId == scenarioId && x.StatusId != null && x.StatusId != 4).OrderBy(x => x.Id).ToList();
            }
            //foreach (var item in sektorList)
            //{
            //    sektorListDraft.Add(item);
            //}
            if (userGet.RoleId == 5 || userGet.RoleId == 3)
            {
                //sektorListDraft = _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false && x.StatusId == null).OrderByDescending(x => x.Id).ToList();
                var list = from scenario in _scenarioRepository.GetAll().ToList()
                                  .Where(mapping => mapping.IsDelete == false && mapping.StatusId != 4).OrderBy(x => x.NamaScenario)
                           from correlatedSektor in _databaseContext.CorrelatedSektors.ToList()
                         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId == null)
                           select correlatedSektor;
                foreach (var item in list)
                {
                    sektorListDraft.Add(item);
                }
            }
            else
            {
                var list = from scenario in _scenarioRepository.GetAll().ToList()
                                  .Where(mapping => mapping.IsDelete == false && mapping.Id == scenarioId && mapping.CreateBy == user && mapping.StatusId != 4)
                           from correlatedSektor in _databaseContext.CorrelatedSektors.ToList()
                         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId == null)
                           select correlatedSektor;
                foreach (var item in list)
                {
                    sektorListDraft.Add(item);
                }
                //sektorListDraft = _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false && x.ScenarioId == scenarioId  && x.StatusId == null).ToList();
            }

            // sektorListDraft.OrderBy(x => x.StatusId == 2);

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (sektorListDraft.Count() > 0)
                {
                    foreach (var item in sektorListDraft)
                    {
                        if (id == 1)
                        {
                            if (id2 == 5 || (item.Status == null && id2 == 4))
                            {
                                if (item.NamaSektor.ToLower().Contains(keyword.ToLower()))
                                    result.Add(item);
                            }
                            if (item.Status != null && id2 != 5)
                            {
                                if (item.NamaSektor.ToLower().Contains(keyword.ToLower()) && item.StatusId == id2)
                                    result.Add(item);

                            }
                            //if (item.Status == null && id2 == 1)
                            //{
                            //    if (item.Sektor.NamaSektor.ToLower().Contains(keyword.ToLower()))
                            //        result.Add(item);

                            //}
                            //if (item.NamaSektor.ToLower().Contains(keyword.ToLower()))
                            //    result.Add(item);
                        }
                        if (id == 2)
                        {
                            var scenarioCS = _scenarioRepository.Get(item.ScenarioId);
                            if (id2 == 5 || (item.Status == null && id2 == 1))
                            {
                                if (scenarioCS.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                    result.Add(item);
                            }
                            if (item.Status != null)
                            {
                                //if (item.Project.NamaProject.ToLower().Contains(keyword.ToLower()) && item.StatusId == id2)
                                //    result.Add(item);
                                if (scenarioCS.NamaScenario.ToLower().Contains(keyword.ToLower()) && item.StatusId == id2)
                                    result.Add(item);
                            }
                            
                            //if (scenarioCS.NamaScenario.ToLower().Contains(keyword.ToLower()))
                            //    result.Add(item);
                        }
                        //if (id == 3)
                        //{
                        //    if (item.Status != null)
                        //    {
                        //        if (item.Status.StatusDescription.ToLower().Contains(keyword.ToLower()))
                        //            result.Add(item);
                        //    }
                        //    if (item.Status == null && "draft".Contains(keyword.ToLower()))
                        //    {
                        //        result.Add(item);

                        //    }
                        //}

                    }
                }
            }
            else
            {
                if (id2 == 0)
                {
                    result = sektorListDraft.ToList();
                }
                else
                {
                    foreach (var item in sektorListDraft)
                    {
                        if (id2 == 5 || (item.Status == null && id2 == 4))
                        {
                            result.Add(item);
                        }
                        if (item.Status != null && id2 != 5)
                        {
                            if (item.StatusId == id2)
                                result.Add(item);
                            //if (item.Status.StatusDescription.ToLower().Contains(keyword.ToLower()))
                            //    result.Add(item);
                        }
                    }
                }
               
                //result = sektorListDraft.ToList();
            }
            #endregion filter
                var projects = _scenarioRepository.GetAll().OrderBy( x => x.NamaScenario)    // your starting point - table in the "from" statement
                .Join(result, // the source table of the inner join
                proj => proj.Id,        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
                scdet => scdet.ScenarioId,   // Select the foreign key (the second part of the "on" clause)
                (proj, scdet) => scdet).ToList(); // selection
                return projects;
            //return _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false).ToList();
        }

        public IEnumerable<CorrelatedSektor> GetByScenarioIdIsZero()
        {
            return _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false && x.ScenarioId == 0).ToList();
        }

        public IEnumerable<CorrelatedSektor> GetByScenarioIdSektorIdList(int scenarioId, int sektorId)
        {
            return _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false && x.ScenarioId == scenarioId && x.SektorId == sektorId).ToList();
        }

        public IEnumerable<CorrelatedSektor> GetByScenarioIdSektorIdList2(int scenarioId, int sektorId, int createBy)
        {
            return _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false && x.ScenarioId == scenarioId && x.SektorId == sektorId && x.CreateBy == createBy).ToList();
        }

        public void Insert(CorrelatedSektor model)
        {
            _databaseContext.CorrelatedSektors.Add(model);
        }

        public void Insert(IList<CorrelatedSektor> collections)
        {
            foreach (var item in collections)
            {
                this.Insert(item);
            }
        }

        public void Update(CorrelatedSektor model)
        {
            _databaseContext.CorrelatedSektors.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }

        public void Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            if(model != null)
                _databaseContext.CorrelatedSektors.Remove(model);

        }

        public void Delete2(int id)
        {
            var model = this.Get(id);
            if (model != null)
                _databaseContext.CorrelatedSektors.Remove(model);
        }

        public CorrelatedSektor GetByScenarioIdSektorId(int scenarioId, int sektorId)
        {
            return _databaseContext.CorrelatedSektors.Where(x => x.ScenarioId == scenarioId && x.SektorId == sektorId && x.IsDelete == false).FirstOrDefault();
        }

        public int GetByScenarioIdSektorIdCst(int scenarioId, int sektorId)
        {
            return _databaseContext.CorrelatedSektors.Where(x => x.ScenarioId == scenarioId && x.SektorId == sektorId && x.IsDelete == false).Select(x=> new { x.Id }).FirstOrDefault().Id;
        }

        public IEnumerable<CorrelatedSektor> GetByScenarioIdAll(int scenarioId)
        {
            return _databaseContext.CorrelatedSektors.Where(x => x.ScenarioId == scenarioId).ToList();
            //return _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false).ToList();
        }

        public IEnumerable<CorrelatedSektor> GetByScenarioIdIsDeleteFalse(int scenarioId)
        {
            return _databaseContext.CorrelatedSektors.Where(x => x.ScenarioId == scenarioId && x.IsDelete == false).ToList();
            //return _databaseContext.CorrelatedSektors.Where(x => x.IsDelete == false).ToList();
        }
    }
}
