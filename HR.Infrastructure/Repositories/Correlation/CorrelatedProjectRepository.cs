﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace HR.Infrastructure.Repositories
{
    public class CorrelatedProjectRepository : ICorrelatedProjectRepository
    {
        private readonly IDatabaseContext _databaseContext;
        private readonly IUserRepository _userRepository;
        private readonly IScenarioRepository _scenarioRepository;

        public CorrelatedProjectRepository(IDatabaseContext databaseContext, IUserRepository userRepository, IScenarioRepository scenarioRepository)
        {
            _databaseContext = databaseContext;
            _userRepository = userRepository;
            _scenarioRepository = scenarioRepository;
        }
        
        public CorrelatedProject Get(int id)
        {
            return _databaseContext.CorrelatedProjects.SingleOrDefault(x => x.Id == id);
        }

        public CorrelatedProject GetIsApproved(int id)
        {
            return _databaseContext.CorrelatedProjects.SingleOrDefault(x => x.IsApproved == true && x.ScenarioId == id && x.IsDelete == false);
        }



        public IEnumerable<CorrelatedProject> GetIsApprovedList(int id)
        {
            return _databaseContext.CorrelatedProjects.Where(x => x.IsApproved == true && x.ScenarioId == id && x.IsDelete == false);
        }

        public IEnumerable<CorrelatedProject> GetAll()
        {
            return _databaseContext.CorrelatedProjects.Where(x => x.IsDelete == false).ToList();
        }

        public IEnumerable<CorrelatedProject> GetByScenarioProjectSektorId(int scenarioId, int projectId, int sektorId)
        {
            return _databaseContext.CorrelatedProjects.Where(x => x.ScenarioId == scenarioId && x.ProjectId == projectId && x.SektorId == sektorId && x.IsDelete == false).ToList();
        }

        public IEnumerable<CorrelatedProject> GetByScenarioProjectSektorId2(int scenarioId, int projectId, int sektorId, int createBy)
        {
            return _databaseContext.CorrelatedProjects.Where(x => x.ScenarioId == scenarioId && x.ProjectId == projectId && x.SektorId == sektorId && x.IsDelete == false && x.CreateBy == createBy).ToList();
        }

        public IEnumerable<CorrelatedProject> GetByScenarioId(int scenarioId, string keyword, int id)
        {
            IEnumerable<CorrelatedProject> sektorList = _databaseContext.CorrelatedProjects.Where(x => x.ScenarioId == scenarioId && x.IsDelete == false).ToList();
           
            IList<CorrelatedProject> result = new List<CorrelatedProject>();


            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (sektorList.Count() > 0)
                {
                    foreach (var item in sektorList)
                    {
                        if (id == 1)
                        {
                            if (item.Sektor.NamaSektor.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 2)
                        {
                            if (item.Project.NamaProject.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                    }
                }
            }
            else
            {
                result = sektorList.ToList();
            }
            #endregion filter

            return result;
        }

        public IEnumerable<CorrelatedProject> GetByScenarioIdAll(int scenarioId)
        {
            return _databaseContext.CorrelatedProjects.Where(x => x.ScenarioId == scenarioId).ToList();
        }

        public IEnumerable<CorrelatedProject> GetByScenarioId(int scenarioId, string keyword, int id, int user)
        {
            //IEnumerable<CorrelatedProject> sektorList = _databaseContext.CorrelatedProjects.Where(x => x.ScenarioId == scenarioId && x.IsDelete == false).ToList();
           
            IList<CorrelatedProject> result = new List<CorrelatedProject>();
            IList<CorrelatedProject> sektorListDraft = new List<CorrelatedProject>();
            IEnumerable<CorrelatedProject> sektorList = new List<CorrelatedProject>();
            var userGet = _userRepository.Get(user);
            if (userGet.RoleId == 3 || userGet.RoleId == 5)
            {
                var list = from scenario in _scenarioRepository.GetAll().ToList()
                                  .Where(mapping => mapping.IsDelete == false && mapping.StatusId != 4)
                           from correlatedSektor in _databaseContext.CorrelatedProjects.ToList()
                         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId != null && mapping.StatusId != 4)
                           select correlatedSektor;
                foreach (var item in list)
                {
                    sektorListDraft.Add(item);
                }
                //sektorList = _databaseContext.CorrelatedProjects.Where(x => x.IsDelete == false && x.StatusId != null && x.StatusId != 4 && x.Scenario.IsDelete == false).OrderByDescending(x => x.Id).ToList();
            }
            else
            {
                var list = from scenario in _scenarioRepository.GetAll().ToList()
                                  .Where(mapping => mapping.IsDelete == false && mapping.Id == scenarioId && mapping.StatusId != 4)
                           from correlatedSektor in _databaseContext.CorrelatedProjects.ToList()
                         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId != null && mapping.StatusId != 4 && mapping.Project.UserId == userGet.Id)
                           select correlatedSektor;
                foreach (var item in list)
                {
                    sektorListDraft.Add(item);
                }
                //sektorList = _databaseContext.CorrelatedProjects.Where(x => x.ScenarioId == scenarioId && x.IsDelete == false && x.StatusId != null && x.StatusId != 4 && x.Scenario.IsDelete == false).OrderByDescending(x => x.Id).ToList();
            }

            if (userGet.RoleId == 3 || userGet.RoleId == 5)
            {
                var list = from scenario in _scenarioRepository.GetAll().ToList()
                                  .Where(mapping => mapping.IsDelete == false && mapping.StatusId != 4)
                           from correlatedSektor in _databaseContext.CorrelatedProjects.ToList()
                         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId == null)
                           select correlatedSektor;

               // var sektorListDraftNew = _databaseContext.CorrelatedProjects.Where(x => x.Scenario.IsDelete == false && x.IsDelete == false && x.StatusId == null).ToList();
                foreach (var item in list)
                {
                    sektorListDraft.Add(item);
                }
            }
            else
            {
                var list = from scenario in _scenarioRepository.GetAll().ToList()
                                 .Where(mapping => mapping.IsDelete == false && mapping.Id == scenarioId && mapping.StatusId != 4)
                           from correlatedSektor in _databaseContext.CorrelatedProjects.ToList()
                         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId == null && mapping.Project.UserId == userGet.Id)
                           select correlatedSektor;
                //var sektorListDraftNew = _databaseContext.CorrelatedProjects.Where(x => x.Scenario.IsDelete == false && x.IsDelete == false && x.Project.UserId == user && x.Scenario.IsDefault == true).ToList();
                foreach (var item in list)
                {
                    sektorListDraft.Add(item);
                }

                // sektorListDraft = _databaseContext.CorrelatedProjects.Where(x => x.IsDelete == false ).ToList();
            }



            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (sektorListDraft.Count() > 0)
                {
                    foreach (var item in sektorListDraft)
                    {
                        if (id == 1)
                        {
                            if (item.Sektor.NamaSektor.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 2)
                        {
                            if (item.Project.NamaProject.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 3)
                        {
                            var scenarioCP = _scenarioRepository.Get(item.ScenarioId);
                            if (scenarioCP.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                    }
                }
            }
            else
            {
                result = sektorListDraft.ToList();
            }
            #endregion filter

            return result;
        }

        public IEnumerable<CorrelatedProject> GetByScenarioId(int scenarioId, string keyword, int id, int id2, int user)
        {
            //IEnumerable<CorrelatedProject> sektorList = _databaseContext.CorrelatedProjects.Where(x => x.ScenarioId == scenarioId && x.IsDelete == false).ToList();

            IList<CorrelatedProject> result = new List<CorrelatedProject>();
            IList<CorrelatedProject> sektorListDraft = new List<CorrelatedProject>();
            IEnumerable<CorrelatedProject> sektorList = new List<CorrelatedProject>();
            var userGet = _userRepository.Get(user);
            if (userGet.RoleId == 3 || userGet.RoleId == 5)
            {
                var list = from scenario in _scenarioRepository.GetAll().ToList()
                                  .Where(mapping => mapping.IsDelete == false && mapping.StatusId != 4)
                           from correlatedSektor in _databaseContext.CorrelatedProjects.ToList()
                         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId != null && mapping.StatusId != 4)
                           select correlatedSektor;
                foreach (var item in list)
                {
                    sektorListDraft.Add(item);
                }
                //sektorList = _databaseContext.CorrelatedProjects.Where(x => x.IsDelete == false && x.StatusId != null && x.StatusId != 4 && x.Scenario.IsDelete == false).OrderByDescending(x => x.Id).ToList();
            }
            else
            {
                var list = from scenario in _scenarioRepository.GetAll().ToList()
                                  .Where(mapping => mapping.IsDelete == false && mapping.Id == scenarioId && mapping.StatusId != 4)
                           from correlatedSektor in _databaseContext.CorrelatedProjects.ToList()
                         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId != null && mapping.StatusId != 4 && mapping.Project.UserId == userGet.Id)
                           select correlatedSektor;
                foreach (var item in list)
                {
                    sektorListDraft.Add(item);
                }
                //sektorList = _databaseContext.CorrelatedProjects.Where(x => x.ScenarioId == scenarioId && x.IsDelete == false && x.StatusId != null && x.StatusId != 4 && x.Scenario.IsDelete == false).OrderByDescending(x => x.Id).ToList();
            }

            if (userGet.RoleId == 3 || userGet.RoleId == 5)
            {
                var list = from scenario in _scenarioRepository.GetAll().ToList()
                                  .Where(mapping => mapping.IsDelete == false && mapping.StatusId != 4)
                           from correlatedSektor in _databaseContext.CorrelatedProjects.ToList()
                         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId == null)
                           select correlatedSektor;

                // var sektorListDraftNew = _databaseContext.CorrelatedProjects.Where(x => x.Scenario.IsDelete == false && x.IsDelete == false && x.StatusId == null).ToList();
                foreach (var item in list)
                {
                    sektorListDraft.Add(item);
                }
            }
            else
            {
                var list = from scenario in _scenarioRepository.GetAll().ToList()
                                 .Where(mapping => mapping.IsDelete == false && mapping.Id == scenarioId && mapping.StatusId != 4)
                           from correlatedSektor in _databaseContext.CorrelatedProjects.ToList()
                         .Where(mapping => mapping.ScenarioId == scenario.Id && mapping.IsDelete == false && mapping.StatusId == null && mapping.Project.UserId == userGet.Id)
                           select correlatedSektor;
                //var sektorListDraftNew = _databaseContext.CorrelatedProjects.Where(x => x.Scenario.IsDelete == false && x.IsDelete == false && x.Project.UserId == user && x.Scenario.IsDefault == true).ToList();
                foreach (var item in list)
                {
                    sektorListDraft.Add(item);
                }

                // sektorListDraft = _databaseContext.CorrelatedProjects.Where(x => x.IsDelete == false ).ToList();
            }



            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (sektorListDraft.Count() > 0)
                {
                    foreach (var item in sektorListDraft)
                    {
                        if (id == 1)
                        {
                            if (id2 == 5 || (item.Status == null && id2 == 4))
                            {
                                if (item.Sektor.NamaSektor.ToLower().Contains(keyword.ToLower()))
                                    result.Add(item);
                            }
                            if (item.Status != null && id2 != 5)
                            {
                                if (item.Sektor.NamaSektor.ToLower().Contains(keyword.ToLower()) && item.StatusId == id2)
                                    result.Add(item);

                            }
                            //if (item.Status == null && id2 == 1)
                            //{
                            //    if (item.Sektor.NamaSektor.ToLower().Contains(keyword.ToLower()))
                            //        result.Add(item);

                            
                        }
                        if (id == 2)
                        {
                            if (id2 == 5 || (item.Status == null && id2 == 4))
                            {
                                if (item.Project.NamaProject.ToLower().Contains(keyword.ToLower()))
                                    result.Add(item);
                            }
                            if (item.Status != null && id2 != 5)
                            {
                                if (item.Project.NamaProject.ToLower().Contains(keyword.ToLower()) && item.StatusId == id2)
                                    result.Add(item);
                            }
                           
                        }
                        if (id == 3)
                        {
                            var scenarioCP = _scenarioRepository.Get(item.ScenarioId);
                            if (id2 == 5 || (item.Status == null && id2 == 4))
                            {
                                if (scenarioCP.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                    result.Add(item);
                            }
                            if (item.Status != null && id2 != 5)
                            {
                                if (scenarioCP.NamaScenario.ToLower().Contains(keyword.ToLower()) && item.StatusId == id2)
                                    result.Add(item);
                            }
                        }
                    }
                }
            }
            else
            {
                if (id2 == 0)
                {
                    result = sektorListDraft.ToList();
                }
                else
                {
                    foreach (var item in sektorListDraft)
                    {
                        if (id2 == 5 || (item.Status == null && id2 == 4))
                        {
                            result.Add(item);
                        }
                        if (item.Status != null && id2 != 5)
                        {
                            if (item.StatusId == id2)
                                result.Add(item);
                            //if (item.Status.StatusDescription.ToLower().Contains(keyword.ToLower()))
                            //    result.Add(item);
                        }
                    }
                }
                //result = sektorListDraft.ToList();
            }
            #endregion filter
            var projects = _scenarioRepository.GetAll().OrderBy(x => x.NamaScenario)    // your starting point - table in the "from" statement
              .Join(result, // the source table of the inner join
              proj => proj.Id,        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
              scdet => scdet.ScenarioId,   // Select the foreign key (the second part of the "on" clause)
              (proj, scdet) => scdet).ToList(); // selection
            return projects;
            //return result;
        }


        public void Insert(CorrelatedProject model)
        {
            _databaseContext.CorrelatedProjects.Add(model);
        }

        public void Update(CorrelatedProject model)
        {
            _databaseContext.CorrelatedProjects.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            CorrelatedProject model = this.Get(id);
            if (model == null)
                return;

            _databaseContext.CorrelatedProjects.Remove(model);
        }

        public CorrelatedProject GetByScenarioIdProjectId(int scenarioId)
        {
            return _databaseContext.CorrelatedProjects.Where(x => x.ScenarioId == scenarioId && x.IsApproved == true && x.IsDelete == false).FirstOrDefault();
        }

        public IEnumerable<CorrelatedProject> GetByScenarioIdCompareCalculation(int scenarioId)
        {
            return _databaseContext.CorrelatedProjects.Where(x => x.ScenarioId == scenarioId && x.IsDelete == false).ToList();
        }

        public CorrelatedProject GetByScenarioIdProjectIdAll(int scenarioId, int projectId)
        {
            return _databaseContext.CorrelatedProjects.Where(x => x.ScenarioId == scenarioId && x.ProjectId == projectId).FirstOrDefault();
        }

        public IEnumerable<CorrelatedProject> GetByScenarioIdProjectIdAllList(int scenarioId, int projectId)
        {
            return _databaseContext.CorrelatedProjects.Where(x => x.ScenarioId == scenarioId && x.ProjectId == projectId).ToList();
        }

        public IEnumerable<CorrelatedProject> GetByScenarioIdProjectIdList(int scenarioId, int projectId)
        {
            return _databaseContext.CorrelatedProjects.Where(x => x.ScenarioId == scenarioId && x.ProjectId == projectId && x.IsDelete == false).ToList();
        }

        public CorrelatedProject GetByScenarioIdProjectIdAllFalse(int scenarioId, int projectId)
        {
            return _databaseContext.CorrelatedProjects.Where(x => x.ScenarioId == scenarioId && x.ProjectId == projectId && x.IsDelete == false).FirstOrDefault();
        }

        public IEnumerable<CorrelatedProject> GetByScenarioIdIsDeleteFalse(int scenarioId)
        {
            return _databaseContext.CorrelatedProjects.Where(x => x.ScenarioId == scenarioId && x.IsDelete == false).ToList();
        }

    }
}
