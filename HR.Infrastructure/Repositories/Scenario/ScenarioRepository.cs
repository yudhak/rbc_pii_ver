﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Data;

namespace HR.Infrastructure.Repositories
{
    public class ScenarioRepository : IScenarioRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public ScenarioRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public Scenario Get(int? id)
        {
            return _databaseContext.Scenarios.SingleOrDefault(x => x.Id == id);
        }

        public Scenario GetDefault()
        {
            return _databaseContext.Scenarios.SingleOrDefault(x => x.IsDefault == true && x.IsDelete == false);
        }

        public IEnumerable<Scenario> GetAll()
        {
            return _databaseContext.Scenarios.Where(x => x.IsDelete == false).ToList();
        }

        public IEnumerable<Scenario> GetAll(string keyword, int userId)
        {
            IEnumerable<Scenario> scenarioListDraft = _databaseContext.Scenarios.Where(x => x.IsDelete == false && x.StatusId != null).OrderByDescending(x=>x.Id).ToList();
            IList<Scenario> result = new List<Scenario>();
            IList<Scenario> scenarioList = _databaseContext.Scenarios.Where(x => x.IsDelete == false && x.StatusId == null && x.CreateBy == userId).OrderByDescending(x => x.Id).ToList();
            foreach(var item in scenarioListDraft.ToList())
            {
                scenarioList.Add(item);
            }
            //var userDraft = _databaseContext.Users.Where(x => x.UserName == ContextBoundObject.UserName).SingleOrDefault();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (scenarioList.Count() > 0)
                {
                    foreach (var item in scenarioList)
                    {
                        if (item.NamaScenario.ToLower().Contains(keyword.ToLower()))
                            result.Add(item);
                    }
                }
            }
            else
            {
                result = scenarioList.ToList();
            }
            #endregion filter

            return result;
        }


        public IEnumerable<Scenario> GetAll(string keyword,int id, int userId)
        {
            IEnumerable<Scenario> scenarioListDraft = _databaseContext.Scenarios.Where(x => x.IsDelete == false && x.StatusId != null).OrderByDescending(x => x.Id).ToList();
            IList<Scenario> result = new List<Scenario>();
            IList<Scenario> scenarioList = _databaseContext.Scenarios.Where(x => x.IsDelete == false && x.StatusId == null && x.CreateBy == userId).OrderByDescending(x => x.Id).ToList();
            foreach (var item in scenarioListDraft.ToList())
            {
                scenarioList.Add(item);
            }
            //var userDraft = _databaseContext.Users.Where(x => x.UserName == ContextBoundObject.UserName).SingleOrDefault();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (scenarioList.Count() > 0)
                {
                    foreach (var item in scenarioList)
                    {
                        if (id == 1)
                        {
                            if (item.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 2)
                        {

                            if (item.Status != null)
                            {
                                if (item.Status.StatusDescription.ToLower().Contains(keyword.ToLower()))
                                    result.Add(item);
                            }
                            if (item.Status == null && "draft".Contains(keyword.ToLower()))
                            {
                                result.Add(item);

                            }

                        }
                    }
                }
            }
            else
            {
                result = scenarioList.ToList();
            }
            #endregion filter

            return result;
        }

        public IEnumerable<Scenario> GetAll(string keyword, int id,int id2, int userId)
        {
            IEnumerable<Scenario> scenarioListDraft = _databaseContext.Scenarios.Where(x => x.IsDelete == false && x.StatusId != null).OrderByDescending(x => x.Id).ToList();
            IList<Scenario> result = new List<Scenario>();
            IList<Scenario> scenarioList = _databaseContext.Scenarios.Where(x => x.IsDelete == false && x.StatusId == null && x.CreateBy == userId).OrderByDescending(x => x.Id).ToList();
            foreach (var item in scenarioListDraft.ToList())
            {
                scenarioList.Add(item);
            }
            //var userDraft = _databaseContext.Users.Where(x => x.UserName == ContextBoundObject.UserName).SingleOrDefault();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (scenarioList.Count() > 0)
                {
                    foreach (var item in scenarioList)
                    {
                        //if (id == 1)
                        //{
                            if (id2 == 5 || (item.Status == null && id2 == 4))
                            {
                                if (item.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                    result.Add(item);
                            }
                            if (item.Status != null && id2 != 5)
                            {
                                if (item.NamaScenario.ToLower().Contains(keyword.ToLower()) && item.StatusId == id2)
                                    result.Add(item);
                                //if (item.Status.StatusDescription.ToLower().Contains(keyword.ToLower()))
                                //    result.Add(item);
                            }
                            //if (item.NamaScenario.ToLower().Contains(keyword.ToLower()))
                            //    result.Add(item);
                        //}
                        //if (id == 2)
                        //{

                        //    if (item.Status != null)
                        //    {
                        //        if (item.Status.StatusDescription.ToLower().Contains(keyword.ToLower()))
                        //            result.Add(item);
                        //    }
                        //    if (item.Status == null && "draft".Contains(keyword.ToLower()))
                        //    {
                        //        result.Add(item);

                        //    }

                        //}
                    }
                }
            }
            else
            {
                if (id2 == 0)
                {
                    result = scenarioList.ToList();
                }
                else
                {
                    foreach (var item in scenarioList)
                    {
                        if (id2 == 5 || (item.Status == null && id2 == 4))
                        {
                            result.Add(item);
                        }
                        if (item.Status != null && id2 != 5)
                        {
                            if (item.StatusId == id2)
                                result.Add(item);
                            //if (item.Status.StatusDescription.ToLower().Contains(keyword.ToLower()))
                            //    result.Add(item);
                        }
                    }
                }
                
               // result = scenarioList.ToList();
            }
            #endregion filter

            return result;
        }

        public IEnumerable<Scenario> GetAllByName(int id)
        {
            var model = Get(id);
            return _databaseContext.Scenarios.Where(x => x.IsDelete == false && x.NamaScenario == model.NamaScenario).ToList();
        }

        public IEnumerable<Scenario> GetAllByName2(int id, int user)
        {
            var model = Get(id);

            return _databaseContext.Scenarios.Where(x => x.IsDelete == false && x.NamaScenario == model.NamaScenario && x.CreateBy == user && x.StatusId != 2).ToList();
        }

        public int GetAllByName2Collate(int user, string namaScenar)
        {
            return _databaseContext.Scenarios.Where(x => x.IsDelete == false && x.NamaScenario == namaScenar && x.CreateBy == user && x.StatusId != 2).ToList().Count();

            //var model = Get(id);
            //string cnnString = System.Configuration.ConfigurationManager.ConnectionStrings["HRConnString"].ConnectionString;
            //SqlConnection cnn = new SqlConnection(cnnString);
            //SqlCommand cmd = new SqlCommand();
            //cmd.Connection = cnn;
            //cmd.CommandTimeout = 500;
            //cmd.CommandType = System.Data.CommandType.Text;
            //cmd.CommandText = "SELECT  FROM[RBC_PII_Email].[dbo].[tblScenarios] where createBy = @creBy and statusId != 2 and isDelete = 0 and namaScenario = @scenName COLLATE SQL_Latin1_General_CP1_CS_AS";
            ////add any parameters the stored procedure might require
            //List<SqlParameter> prm = new List<SqlParameter>()
            //             {
            //                 new SqlParameter("@creBy", SqlDbType.Int) {Value = user},
            //                 new SqlParameter("@scenName", SqlDbType.VarChar) {Value = namaScenar}
            //             };
            //cmd.Parameters.AddRange(prm.ToArray());
            //cnn.Open();
            //var o = cmd.ExecuteNonQuery();
            //cnn.Close();
            //List<int> menari = o;
            //var model2 = Get(o);
            //return null;
        }

        public void Insert(Scenario model)
        {
            _databaseContext.Scenarios.Add(model);
        }

        public bool IsExist(string namaScenario)
        {
            var results = _databaseContext.Scenarios.Where(x => x.NamaScenario.ToLower() == namaScenario.ToLower() && x.IsDelete == false).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public bool IsExist(int id, string namaScenario)
        {
            var results = _databaseContext.Scenarios.Where(x => x.NamaScenario.ToLower() == namaScenario.ToLower() && x.IsDelete == false && x.Id != id).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public void Update(Scenario model)
        {
            _databaseContext.Scenarios.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }

        public void Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            if (model != null)
                _databaseContext.Scenarios.Remove(model);
        }

        public IEnumerable<Scenario> GetAllForProductValidate()
        {
            return _databaseContext.Scenarios.Where(x => x.IsDelete == false && x.StatusId != null).ToList();
        }
    }
}
