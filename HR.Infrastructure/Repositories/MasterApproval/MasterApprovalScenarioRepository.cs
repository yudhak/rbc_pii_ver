﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace HR.Infrastructure.Repositories
{
    public class MasterApprovalScenarioRepository : IMasterApprovalScenarioRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public MasterApprovalScenarioRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public MasterApprovalScenario Get(int? id)
        {
            return _databaseContext.MasterApprovalScenarios.SingleOrDefault(x => x.Id == id);
        }

        public MasterApprovalScenario GetByUserId(int id)
        {
            return _databaseContext.MasterApprovalScenarios.SingleOrDefault(x => x.UserId == id && x.IsDelete == false);
        }

        public MasterApprovalScenario GetByRoleId(int roleId)
        {
            //var user = _databaseContext.MasterApprovalScenarios    // your starting point - table in the "from" statement
            //        .Join(_databaseContext.Users.Where(x => x.IsDelete == false || x.IsDelete == null && x.RoleId == roleId), // the source table of the inner join
            //        proj => proj.UserId,        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
            //        scdet => scdet.Id,   // Select the foreign key (the second part of the "on" clause)
            //        (proj, scdet) => proj).SingleOrDefault(); // selection
            //return user;
            int noUrut = 0;
            switch (roleId)
            {
                case 3:
                    noUrut = 2;
                    break;
                case 4:
                    noUrut = 1;
                    break;
                case 5:
                    noUrut = 3;
                    break;
                default:
                    break;
            }
            return _databaseContext.MasterApprovalScenarios.SingleOrDefault(x => x.NomorUrutStatus == noUrut);
        }

        public IEnumerable<MasterApprovalScenario> GetAll()
        {
            return _databaseContext.MasterApprovalScenarios.Where(x => x.IsDelete == false).ToList();
        }

        public IEnumerable<MasterApprovalScenario> GetAllByMenuId(int menuId)
        {
            return _databaseContext.MasterApprovalScenarios.Where(x => x.IsDelete == false && x.MenuId == menuId).OrderBy( x => x.NomorUrutStatus ).ToList();
        }

        public IEnumerable<MasterApprovalScenario> GetAllByMenuIdIsDeleteTrue(int menuId)
        {
            return _databaseContext.MasterApprovalScenarios.Where(x => x.MenuId == menuId).OrderBy(x => x.NomorUrutStatus).ToList();
        }

        public MasterApprovalScenario GetByNomorUrut(int nomorUrut)
        {
            return _databaseContext.MasterApprovalScenarios.Where(x => x.IsDelete == false && x.NomorUrutStatus == nomorUrut).SingleOrDefault();
        }

        public MasterApprovalScenario GetByMenuIdNomorUrutId(int menuId, int nomorUrut)
        {
            return _databaseContext.MasterApprovalScenarios.Where(x => x.IsDelete == false && x.MenuId == menuId && x.NomorUrutStatus == nomorUrut).SingleOrDefault();
        }

        public IEnumerable<MasterApprovalScenario> GetByCreateBy(int createBy)
        {
            return _databaseContext.MasterApprovalScenarios.Where(x => x.IsDelete == false && x.CreateBy == createBy).ToList();
        }

        //public IEnumerable<MasterApprovalScenario> GetAll(string keyword)
        //{
        //    IEnumerable<MasterApprovalScenario> masterApprovalScenarioList = _databaseContext.MasterApprovalScenarios.Where(x => x.IsDelete == false).ToList();
        //    IList<MasterApprovalScenario> result = new List<MasterApprovalScenario>();

        //    #region filter
        //    if (!string.IsNullOrEmpty(keyword))
        //    {
        //        if (masterApprovalScenarioList.Count() > 0)
        //        {
        //            foreach (var item in masterApprovalScenarioList)
        //            {
        //                if (item.Menu.Name.ToLower().Contains(keyword.ToLower()))
        //                    result.Add(item);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        result = masterApprovalScenarioList.ToList();
        //    }
        //    #endregion filter

        //    return result;
        //}

        public void Insert(MasterApprovalScenario model)
        {
            _databaseContext.MasterApprovalScenarios.Add(model);
        }

        public void Insert(IList<MasterApprovalScenario> collections)
        {
            foreach (var item in collections)
            {
                this.Insert(item);
            }
        }

        public bool IsExist(int id, int menuId, int userId, int nomorUrutStatus)
        {
            var results = _databaseContext.MasterApprovalScenarios.Where(x => x.Id != id && x.MenuId == menuId && x.UserId == userId && 
            x.NomorUrutStatus == nomorUrutStatus&&  x.IsDelete == false).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public void Update(MasterApprovalScenario model)
        {
            _databaseContext.MasterApprovalScenarios.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }

        public void UpdateList(IList<MasterApprovalScenario> collections)
        {
            foreach (var item in collections)
            {
                this.Update(item);
            }
        }

        public void Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            if (model != null)
                _databaseContext.MasterApprovalScenarios.Remove(model);
        }
    }
}
