﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace HR.Infrastructure.Repositories
{
    public class MasterApprovalRiskMatrixProjectRepository : IMasterApprovalRiskMatrixProjectRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public MasterApprovalRiskMatrixProjectRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public MasterApprovalRiskMatrixProject Get(int? id)
        {
            return _databaseContext.MasterApprovalRiskMatrixProjects.SingleOrDefault(x => x.Id == id);
        }

        public MasterApprovalRiskMatrixProject GetByUserId(int id)
        {
            return _databaseContext.MasterApprovalRiskMatrixProjects.SingleOrDefault(x => x.UserId == id && x.IsDelete == false);
        }

        public MasterApprovalRiskMatrixProject GetByProjectIdUserId(int projectId, int userId)
        {
            return _databaseContext.MasterApprovalRiskMatrixProjects.SingleOrDefault(x => x.ProjectId == projectId && x.UserId == userId && x.IsDelete == false);
        }

        public MasterApprovalRiskMatrixProject GetByProjectIdRoleId(int projectId, int roleId)
        {
            //var user = _databaseContext.MasterApprovalRiskMatrixProjects.Where(x => x.ProjectId == projectId)    // your starting point - table in the "from" statement
            //.Join(_databaseContext.Users.Where(x => x.IsDelete == false || x.IsDelete == null && x.RoleId == roleId), // the source table of the inner join
            //proj => proj.UserId,        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
            //scdet => scdet.Id,   // Select the foreign key (the second part of the "on" clause)
            //(proj, scdet) => proj).SingleOrDefault(); // selection
            //    return user;
            int noUrut = 0;
            switch (roleId)
            {
                case 3:
                    noUrut = 2;
                    break;
                case 4:
                    noUrut = 1;
                    break;
                case 5:
                    noUrut = 3;
                    break;
                default:
                    break;
            }
            return _databaseContext.MasterApprovalRiskMatrixProjects.SingleOrDefault(x => x.NomorUrutStatus == noUrut && x.ProjectId == projectId && x.IsDelete == false);
        }

        public IEnumerable<MasterApprovalRiskMatrixProject> GetAll()
        {
            return _databaseContext.MasterApprovalRiskMatrixProjects.Where(x => x.IsDelete == false).ToList();
        }

        public IEnumerable<MasterApprovalRiskMatrixProject> GetAllByMenuId(int menuId)
        {
            return _databaseContext.MasterApprovalRiskMatrixProjects.Where(x => x.IsDelete == false && x.MenuId == menuId).ToList();
        }

        public IEnumerable<MasterApprovalRiskMatrixProject> GetAllByProjectId(int projectId)
        {
            return _databaseContext.MasterApprovalRiskMatrixProjects.Where(x => x.IsDelete == false && x.ProjectId == projectId).ToList();
        }
        //public IEnumerable<MasterApprovalScenario> GetAll(string keyword)
        //{
        //    IEnumerable<MasterApprovalScenario> masterApprovalScenarioList = _databaseContext.MasterApprovalScenarios.Where(x => x.IsDelete == false).ToList();
        //    IList<MasterApprovalScenario> result = new List<MasterApprovalScenario>();

        //    #region filter
        //    if (!string.IsNullOrEmpty(keyword))
        //    {
        //        if (masterApprovalScenarioList.Count() > 0)
        //        {
        //            foreach (var item in masterApprovalScenarioList)
        //            {
        //                if (item.Menu.Name.ToLower().Contains(keyword.ToLower()))
        //                    result.Add(item);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        result = masterApprovalScenarioList.ToList();
        //    }
        //    #endregion filter

        //    return result;
        //}

        public void Insert(MasterApprovalRiskMatrixProject model)
        {
            _databaseContext.MasterApprovalRiskMatrixProjects.Add(model);
        }

        public void Insert(IList<MasterApprovalRiskMatrixProject> collections)
        {
            foreach (var item in collections)
            {
                this.Insert(item);
            }
        }

        public bool IsExist(int id, int menuId, int projectId, int userId, int nomorUrutStatus)
        {
            var results = _databaseContext.MasterApprovalRiskMatrixProjects.Where(x => x.Id != id && x.MenuId == menuId && x.ProjectId == projectId && x.UserId == userId && 
            x.NomorUrutStatus == nomorUrutStatus &&  x.IsDelete == false).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public void Update(MasterApprovalRiskMatrixProject model)
        {
            _databaseContext.MasterApprovalRiskMatrixProjects.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }

        public void Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            if (model != null)
                _databaseContext.MasterApprovalRiskMatrixProjects.Remove(model);
        }
    }
}
