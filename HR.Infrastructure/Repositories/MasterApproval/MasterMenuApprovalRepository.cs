﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;


namespace HR.Infrastructure.Repositories
{
    public class MasterMenuApprovalRepository : IMasterMenuApprovalRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public MasterMenuApprovalRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public MasterMenuApproval Get(int id)
        {
            return _databaseContext.MasterMenuApprovals.SingleOrDefault(x => x.Id == id);
        }


        public IEnumerable<MasterMenuApproval> GetAll()
        {
            //return _databaseContext.AssetDatas.AsQueryable();
            return _databaseContext.MasterMenuApprovals.ToList();

        }

        public IEnumerable<MasterMenuApproval> GetAll(string keyword)
        {
            IEnumerable<MasterMenuApproval> masterApprovalList = _databaseContext.MasterMenuApprovals.ToList();
            IList<MasterMenuApproval> result = new List<MasterMenuApproval>();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (masterApprovalList.Count() > 0)
                {
                    int dataNamaStatus = 0;
                    foreach (var item in masterApprovalList)
                    {
                        if (item.Name.ToLower().Contains(keyword.ToLower()))
                        {
                            result.Add(item);
                            dataNamaStatus += 1;
                        }
                    }

                    //if (dataNamaProject > 0)
                    //{
                    //    foreach (var item in result)
                    //    {
                    //        if (item.Minimum.ToString().ToLower().Contains(keyword.ToLower()))
                    //            result.Add(item);
                    //    }
                    //}
                    //else
                    //{
                    //    result = projectList.ToList();
                    //    foreach (var item in result)
                    //    {
                    //        if (item.Minimum.ToString().ToLower().Contains(keyword.ToLower()))
                    //            result.Add(item);
                    //    }
                    //}
                    
                }
            }
            else
            {
                result = masterApprovalList.ToList();
            }
            #endregion filter

            return result;
        }

    }
}
