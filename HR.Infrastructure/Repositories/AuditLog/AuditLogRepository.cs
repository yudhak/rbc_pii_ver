﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace HR.Infrastructure.Repositories
{
    public class AuditLogRepository : IAuditLogRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public AuditLogRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public AuditLog Get(int id)
        {
            return _databaseContext.AuditLogs.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<AuditLog> GetAll(int id)
        {
            //return _databaseContext.AssetDatas.AsQueryable();
            var results = _databaseContext.AuditLogs.OrderByDescending(x=>x.Id).ToList();
            if(id == 1)
            {
                results = results.Where(x => x.MenuId == 24 || x.MenuId == 25 || x.MenuId == 26 || x.MenuId == 29 || x.MenuId == 30 || x.MenuId == 31).ToList();
            }
            if (id == 2)
            {
                results = results.Where(x => x.MenuId == 34 || x.MenuId == 35 || x.MenuId == 36).ToList();
            }
            if (id == 3)
            {
                results = results.Where(x => x.MenuId == 39 || x.MenuId == 40 || x.MenuId == 41 || x.MenuId == 1 || x.MenuId == 2 || x.MenuId == 3).ToList();
            }
            if (id == 4)
            {
                results = results.Where(x => x.MenuId == 44 || x.MenuId == 45 || x.MenuId == 46 || x.MenuId == 49).ToList();
            }
            if (id == 5)
            {
                results = results.Where(x => x.MenuId == 52 || x.MenuId == 53 || x.MenuId == 54).ToList();
            }
            if (id == 6)
            {
                results = results.Where(x => x.MenuId == 57 || x.MenuId == 58 || x.MenuId == 59).ToList();
            }
            if (id == 7)
            {
                results = results.Where(x => x.MenuId == 62 || x.MenuId == 63 || x.MenuId == 64).ToList();
            }
            if (id == 8)
            {
                results = results.Where(x => x.MenuId == 67 || x.MenuId == 68 || x.MenuId == 69).ToList();
            }
            if (id == 9)
            {
                results = results.Where(x => x.MenuId == 73 || x.MenuId == 74 || x.MenuId == 75 || x.MenuId == 76).ToList();
            }
            if (id == 10)
            {
                results = results.Where(x => x.MenuId == 79 || x.MenuId == 80 || x.MenuId == 81).ToList();
            }
            if (id == 11)
            {
                results = results.Where(x => x.MenuId == 84 || x.MenuId == 85).ToList();
            }
            if (id == 12)
            {
                results = results.Where(x => x.MenuId == 88 || x.MenuId == 89 || x.MenuId == 90 || x.MenuId == 4 || x.MenuId == 5).ToList();
            }
            if (id == 13)
            {
                results = results.Where(x => x.MenuId == 94 || x.MenuId == 95 || x.MenuId == 98 || x.MenuId == 99).ToList();
            }
            if (id == 14)
            {
                results = results.Where(x => x.MenuId == 108 || x.MenuId == 107).ToList();
            }
            if (id == 15)
            {
                results = results.Where(x => x.MenuId == 115 || x.MenuId == 116).ToList();
            }
            if (id == 16)
            {
                results = results.Where(x => x.MenuId == 121 || x.MenuId == 122).ToList();
            }
            return results;
        }

        public void Insert(AuditLog model)
        {
            _databaseContext.AuditLogs.Add(model);
        }
    }
}
