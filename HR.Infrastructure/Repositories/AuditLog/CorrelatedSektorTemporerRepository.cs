﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace HR.Infrastructure.Repositories
{
    public class CorrelatedSektorTemporerRepository : ICorrelatedSektorTemporerRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public CorrelatedSektorTemporerRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public CorrelatedSektorTemporer Get(int id)
        {
            return _databaseContext.CorrelatedSektorTemporers.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<CorrelatedSektorTemporer> GetAll()
        {
            //return _databaseContext.AssetDatas.AsQueryable();
            return _databaseContext.CorrelatedSektorTemporers.ToList();

        }

        public void Insert(CorrelatedSektorTemporer model)
        {
            _databaseContext.CorrelatedSektorTemporers.Add(model);
        }

        public void Delete(int id, int deleteBy, DateTime deleteDate)
        {
            //var model = this.Get(id);
            //if (model != null)
            //    _databaseContext.Scenarios.Remove(model);
        }
    }
}
