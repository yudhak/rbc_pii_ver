﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace HR.Infrastructure.Repositories
{
    public class RiskMatriksTemporerRepository : IRiskMatriksTemporerRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public RiskMatriksTemporerRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public RiskMatriksTemporer Get(int id)
        {
            return _databaseContext.RiskMatriksTemporers.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<RiskMatriksTemporer> GetAll()
        {
            //return _databaseContext.AssetDatas.AsQueryable();
            return _databaseContext.RiskMatriksTemporers.ToList();

        }

        public void Insert(RiskMatriksTemporer model)
        {
            _databaseContext.RiskMatriksTemporers.Add(model);
        }

        public void Delete(int id, int deleteBy, DateTime deleteDate)
        {
            //var model = this.Get(id);
            //if (model != null)
            //    _databaseContext.Scenarios.Remove(model);
        }
    }
}
