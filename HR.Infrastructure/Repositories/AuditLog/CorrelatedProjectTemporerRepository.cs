﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace HR.Infrastructure.Repositories
{
    public class CorrelatedProjectTemporerRepository : ICorrelatedProjectTemporerRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public CorrelatedProjectTemporerRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public CorrelatedProjectTemporer Get(int id)
        {
            return _databaseContext.CorrelatedProjectTemporers.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<CorrelatedProjectTemporer> GetAll()
        {
            //return _databaseContext.AssetDatas.AsQueryable();
            return _databaseContext.CorrelatedProjectTemporers.ToList();

        }

        public void Insert(CorrelatedProjectTemporer model)
        {
            _databaseContext.CorrelatedProjectTemporers.Add(model);
        }

        public void Delete(int id, int deleteBy, DateTime deleteDate)
        {
            //var model = this.Get(id);
            //if (model != null)
            //    _databaseContext.Scenarios.Remove(model);
        }
    }
}
