﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;

namespace HR.Infrastructure.Repositories
{
    public class RoleAccessRepository : IRoleAccessRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public RoleAccessRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public RoleAccess Get(int id)
        {
            return _databaseContext.RoleAccesses.SingleOrDefault(x => x.Id == id);
        }
            
        public IEnumerable<RoleAccess> GetAll()
        {            
            return _databaseContext.RoleAccesses.ToList();
        }

        public IEnumerable<RoleAccess> GetAllOldAccess(int id)
        {
            return _databaseContext.RoleAccesses.Where(x=>x.FlagFromFront == id).ToList();
        }

        public void Insert(RoleAccess model)
        {
            _databaseContext.RoleAccesses.Add(model);
        }

        public void Update(RoleAccess model)
        {
            _databaseContext.RoleAccesses.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var model = this.Get(id);
            if (model != null)
                _databaseContext.RoleAccesses.Remove(model);
        }
    }
}
