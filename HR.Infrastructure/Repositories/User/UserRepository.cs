﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace HR.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IDatabaseContext _databaseContext;
        public UserRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public User Get(int id)
        {
            return _databaseContext.Users.SingleOrDefault(x => x.Id.Equals(id));
        }

        public IEnumerable<User> GetAll()
        {
            return _databaseContext.Users.AsQueryable();
        }

        public IEnumerable<User> GetAll(string keyword)
        {
            IEnumerable<User> userList = _databaseContext.Users.AsQueryable().Where(x => x.IsDelete == false || x.IsDelete == null);
            IList<User> result = new List<User>();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (userList.Count() > 0)
                {
                    foreach (var item in userList)
                    {
                        if (item.UserName.ToLower().Contains(keyword.ToLower()))
                            result.Add(item);
                    }
                }
            }
            else
            {
                result = userList.ToList();
            }
            #endregion filter
            return result;
        }

        public IEnumerable<User> GetAll(string keyword, int field)
        {
            IEnumerable<User> userList = _databaseContext.Users.Where(x => x.IsDelete == false || x.IsDelete == null).ToList();
            
            IList<User> result = new List<User>();
            int idRole = 0;
            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (userList.Count() > 0)
                {
                    foreach (var item in userList)
                    {
                        idRole = item.RoleId;
                        var role = _databaseContext.Roles.SingleOrDefault(x => x.Id == idRole);
                        if (field == 1)
                        {
                            if (item.UserName.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }

                        if (field == 3)
                        {
                            if (item.RoleId != 0)
                            {
                                
                                if (role.Name.ToLower().Contains(keyword.ToLower()))
                                    result.Add(item);
                            }
                            
                        }
                    }
                   
                }
            }
            else
            {
                //if (field == 0)
               // {
                    result = userList.ToList();
               // }
                //else
                //{
                //    foreach (var item in userList)
                //    {

                //        if (field == 1)
                //        {
                //            if (item.UserName.ToLower().Contains(keyword.ToLower()))
                //                result.Add(item);
                //        }

                //        if (id2 == 5 || (item.Status == null && id2 == 4))
                //        {
                //            result.Add(item);
                //        }
                //        if (item.Status != null && id2 != 5)
                //        {
                //            if (item.StatusId == id2)
                //                result.Add(item);
                //            //if (item.Status.StatusDescription.ToLower().Contains(keyword.ToLower()))
                //            //    result.Add(item);
                //        }
                //    }
                //}
                
            }
            #endregion filter
            return result;
        }

        public User Find(string username)
        {
            return _databaseContext.Users
                .Where(p => p.UserName.Equals(username))
                .SingleOrDefault();
        }


        public bool EmailExist(string username)
        {
            throw new NotImplementedException();
        }

        public bool isExist(string username)
        {
            var result = this.Find(username);
            if (result != null)
                return true;
            else
                return false;
        }

        //mendapatkan role 
        public Role GetRoleMaster(int id)
        {
            
            switch (id)
            {
                case 1:
                    return _databaseContext.Roles
                    .Where(p => p.Id.Equals(3))
                    .SingleOrDefault();
                case 2:
                    return _databaseContext.Roles
                    .Where(p => p.Id.Equals(4))
                    .SingleOrDefault();
                case 3:
                    return _databaseContext.Roles
                    .Where(p => p.Id.Equals(5))
                    .SingleOrDefault();
                default:
                    return _databaseContext.Roles
                    .Where(p => p.Id.Equals(2))
                    .SingleOrDefault();
                    break;
            }

            //id = id + 2;
            //return _databaseContext.Roles
            //    .Where(p => p.Id.Equals(id))
            //    .SingleOrDefault();
        }

        //mendapatkan role dari user ID
        public Role GetRoleUser(int id)
        {
            User user = this.Get(id);

            if (user != null)
            {
                return _databaseContext.Roles
                .Where(p => p.Id.Equals(user.RoleId))
                .SingleOrDefault();
            }
            return null;

        }

        //public Role GetRole(int id)
        //{
        //    return _databaseContext.Roles
        //       .Where(p => p.Id.Equals(id))
        //       .SingleOrDefault();
        //}

        public IEnumerable<User> GetAllFilter()
        {
            return _databaseContext.Users.Where(x => x.RoleId == 5);
        }

        public void Insert(User model)
        {
            _databaseContext.Users.Add(model);
        }

        public bool IsExist(int id, string userName)
        {
            var results = _databaseContext.Users.Where(x => x.UserName.ToLower() == userName.ToLower() && x.Id != id).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public bool IsExist(string userName)
        {
            var results = _databaseContext.Users.Where(x => x.UserName.ToLower() == userName.ToLower()).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public void Update(User model)
        {
            _databaseContext.Users.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }
        public void Delete(int id)
        {
            var model = this.Get(id);
            if (model != null)
                _databaseContext.Users.Remove(model);
        }

        public void Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            if (model != null)
                model.RemoveUser(deleteBy, deleteDate);
            //_databaseContext.Users.Remove(model);
        }
    }
}
