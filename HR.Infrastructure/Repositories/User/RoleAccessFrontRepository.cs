﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;

namespace HR.Infrastructure.Repositories
{
    public class RoleAccessFrontRepository : IRoleAccessFrontRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public RoleAccessFrontRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public RoleAccessFront Get(int id)
        {
            return _databaseContext.RoleAccessFronts.SingleOrDefault(x => x.Id == id);
        }
            
        public IEnumerable<RoleAccessFront> GetAll(string keyword, int id)
        { 
            IEnumerable<RoleAccessFront> sektorList = _databaseContext.RoleAccessFronts.Where(x => x.IsDelete == false).OrderBy(x => x.Role.Name).ToList();
            IList<RoleAccessFront> result = new List<RoleAccessFront>();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (sektorList.Count() > 0)
                {
                    foreach (var item in sektorList)
                    {
                        if (id == 1)
                        {
                            if (item.Role.Name.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 2)
                        {
                            if (item.Menu.Name.ToString().ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                    }
                }
            }
            else
            {
                result = sektorList.ToList();
            }
            #endregion filter

            return result;
        }

        public void Insert(RoleAccessFront model)
        {
            _databaseContext.RoleAccessFronts.Add(model);
        }

        public void Update(RoleAccessFront model)
        {
            _databaseContext.RoleAccessFronts.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var model = this.Get(id);
            if (model != null)
                _databaseContext.RoleAccessFronts.Remove(model);
        }

        public bool IsExist(int menuId, int roleId)
        {
            var results = _databaseContext.RoleAccessFronts.Where(x => x.MenuId == menuId && x.RoleId == roleId && x.IsDelete == false).ToList();
            if (results.Count > 0)
                return true;
            return false;
        }
    }
}
