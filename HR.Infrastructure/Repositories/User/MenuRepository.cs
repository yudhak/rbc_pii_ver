﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Infrastructure.Repositories
{
    public class MenuRepository : IMenuRepository
    {
        private readonly IDatabaseContext _databaseContext;
        public MenuRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public Menu Get(int id)
        {
            return _databaseContext.Menus.SingleOrDefault(x => x.Id.Equals(id));
        }

        public IEnumerable<Menu> GetAll()
        {
            //var results = _databaseContext.Menus.AsQueryable();
            var results = _databaseContext.Menus.Where(x => x.IsOnMenu == true).ToList();
            return results;
        }

        public IList<Menu> GetChild(int parentId)
        {
            return _databaseContext.Menus.Where(x => x.Parent.Id == parentId ).ToList();
        }

        public Menu GetParent(int? parentId, Menu menu)
        {
            var menu2 = _databaseContext.Menus.Where(x => x.Sequence == parentId && x.ParentId != x.Sequence).ToList();
            foreach(var item in menu2)
            {
                if(item.Id < menu.Id)
                {
                    return item;
                }
            }
            return null;
        }

        public IQueryable<Menu> GetMenu()
        {
            return _databaseContext.Menus.AsQueryable();
        }

        public IEnumerable<Menu> GetSimilarName(int id)
        {
            var menu = _databaseContext.Menus.SingleOrDefault(x => x.Id.Equals(id));
            return _databaseContext.Menus.Where(x => x.Name.Contains(menu.Name)).ToList();
        }
    }
}
