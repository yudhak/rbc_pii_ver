﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;


namespace HR.Infrastructure.Repositories
{
    public class AvailableCapitalProjectedRepository : IAvailableCapitalProjectedRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public AvailableCapitalProjectedRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public AvailableCapitalProjected Get(int id)
        {
            return _databaseContext.AvailableCapitalProjecteds.SingleOrDefault(x => x.Id == id);
        }
        public AvailableCapitalProjected GetByScenarioIdYear(int scenarioId, int year, string source)
        {
            return _databaseContext.AvailableCapitalProjecteds.SingleOrDefault(x => x.ScenarioId == scenarioId && x.Year == year && x.Source == source);
        }

        public IEnumerable<AvailableCapitalProjected> GetAll()
        {
            return _databaseContext.AvailableCapitalProjecteds.ToList();
        }

        public void Insert(AvailableCapitalProjected model)
        {
            _databaseContext.AvailableCapitalProjecteds.Add(model);
        }

        public void Update(AvailableCapitalProjected model)
        {
            _databaseContext.AvailableCapitalProjecteds.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }

        public void Delete()
        {
            var result = _databaseContext.AvailableCapitalProjecteds.ToList();
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    _databaseContext.AvailableCapitalProjecteds.Remove(item);
                }
            }
        }
    }
}
