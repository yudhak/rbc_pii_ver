﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;


namespace HR.Infrastructure.Repositories
{
    public class ScenarioCalculationRepository : IScenarioCalculationRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public ScenarioCalculationRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public ScenarioCalculation Get(int id)
        {
            return _databaseContext.ScenarioCalculations.SingleOrDefault(x => x.Id == id);
        }

        public ScenarioCalculation GetByScenarioId(int id)
        {
            return _databaseContext.ScenarioCalculations.SingleOrDefault(x => x.ScenarioId == id);
        }

        public IEnumerable<ScenarioCalculation> GetAll()
        {
            return _databaseContext.ScenarioCalculations.ToList();
        }

        public void Insert(ScenarioCalculation model)
        {
            _databaseContext.ScenarioCalculations.Add(model);
        }

        public void Delete()
        {
            var result = _databaseContext.ScenarioCalculations.ToList();
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    _databaseContext.ScenarioCalculations.Remove(item);
                }
            }
        }

        public void Delete(ScenarioCalculation model)
        {
            _databaseContext.ScenarioCalculations.Remove(model);
        }
    }
}
