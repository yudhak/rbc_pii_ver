﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;


namespace HR.Infrastructure.Repositories
{
    public class ProjectCalculationRepository : IProjectCalculationRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public ProjectCalculationRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public ProjectCalculation Get(int id)
        {
            return _databaseContext.ProjectCalculations.SingleOrDefault(x => x.Id == id);
        }

        public ProjectCalculation GetByProjectId(int id)
        {
            return _databaseContext.ProjectCalculations.SingleOrDefault(x => x.ProjectId == id);
        }

        public IEnumerable<ProjectCalculation> GetByScenarioCalculationId(int id)
        {
            return _databaseContext.ProjectCalculations.Where(x => x.ScenarioCalculationId == id).ToList();
        }

        public IEnumerable<ProjectCalculation> GetAll()
        {
            return _databaseContext.ProjectCalculations.ToList();
        }

        public void Insert(ProjectCalculation model)
        {
            _databaseContext.ProjectCalculations.Add(model);
        }

        public void Delete()
        {
            var result = _databaseContext.ProjectCalculations.ToList();
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    _databaseContext.ProjectCalculations.Remove(item);
                }
            }
        }

        public void Delete(ProjectCalculation model)
        {
            _databaseContext.ProjectCalculations.Remove(model);
        }

        public void Update(ProjectCalculation model)
        {
            _databaseContext.ProjectCalculations.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }
    }
}
