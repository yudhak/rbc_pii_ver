﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Data;

namespace HR.Infrastructure.Repositories
{
    public class ApprovalRepository : IApprovalRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public ApprovalRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public Approval Get(int id)
        {
            return _databaseContext.Approval.SingleOrDefault(x => x.Id == id);
        }

        public string GetByActive(int id, int idUser, string source)
        {
            var queryGetUserName = "";
            if (id != 0 && idUser != 0 && source != null)
            {
                
                switch (source)
                {
                    case "Scenario":
                        var scenario = _databaseContext.Scenarios.SingleOrDefault(x => x.Id == id);
                        //var scenarioGet = _databaseContext.Scenarios.FirstOrDefault(x => x.NamaScenario == scenario.NamaScenario && x.CreateBy == scenario.CreateBy && x.StatusId == 4 && x.IsDelete == false).Id;
                        string cnnString = System.Configuration.ConfigurationManager.ConnectionStrings["HRConnString"].ConnectionString;
                        SqlConnection cnn = new SqlConnection(cnnString);
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = cnn;
                        cmd.CommandTimeout = 600;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = "SELECT [id],[namaScenario],[isDefault],[likehoodId],[createBy],[createDate],[updateBy],[updateDate],[isDelete],[deleteDate],[statusId] FROM[tblScenarios] where createBy = @creBy and statusId = 4 and isDelete = 0 and namaScenario = @scenName COLLATE SQL_Latin1_General_CP1_CS_AS";
                        //add any parameters the stored procedure might require
                        List<SqlParameter> prm = new List<SqlParameter>()
                         {
                             new SqlParameter("@creBy", SqlDbType.Int) {Value = scenario.CreateBy},
                             new SqlParameter("@scenName", SqlDbType.VarChar) {Value = scenario.NamaScenario}
                         };
                        cmd.Parameters.AddRange(prm.ToArray());
                        cnn.Open();
                        object o = cmd.ExecuteScalar();
                        int scenarioIdGet = Convert.ToInt32(o);
                        cnn.Close();
                        queryGetUserName =
                                    (from app in _databaseContext.Approval
                                     join mas in _databaseContext.MasterApprovalScenarios on app.NomorUrutStatus + 1 equals mas.NomorUrutStatus
                                     join usr in _databaseContext.Users on mas.UserId equals usr.Id
                                     where app.RequestId == scenarioIdGet && app.IsActive == true
                                     select usr.UserName).SingleOrDefault();
                        break;
                    case "RiskMatrixProject":
                        var riskMatrix = _databaseContext.RiskMatrixProjects.SingleOrDefault(x => x.Id == id);
                        int riskMatrixGet;
                        if (riskMatrix.UpdateBy != null)
                        {
                            riskMatrixGet = _databaseContext.RiskMatrixProjects.SingleOrDefault(x => x.ProjectId == riskMatrix.ProjectId
                            && x.ScenarioId == riskMatrix.ScenarioId
                            && x.RiskMatrixId == riskMatrix.RiskMatrixId 
                            && x.CreateBy == riskMatrix.UpdateBy
                            && x.StatusId == 4 && x.IsDelete == false).Id;
                        }
                        else
                        {
                            riskMatrixGet = _databaseContext.RiskMatrixProjects.SingleOrDefault(x => x.ProjectId == riskMatrix.ProjectId
                            && x.ScenarioId == riskMatrix.ScenarioId
                            && x.RiskMatrixId == riskMatrix.RiskMatrixId 
                            && x.CreateBy == riskMatrix.CreateBy
                            && x.StatusId == 4 && x.IsDelete == false).Id;
                        }
                        queryGetUserName =
                                    (from app in _databaseContext.Approval
                                     join mas in _databaseContext.MasterApprovalRiskMatrixProjects on app.NomorUrutStatus + 1 equals mas.NomorUrutStatus
                                     join usr in _databaseContext.Users on mas.UserId equals usr.Id
                                     where app.RequestId == riskMatrixGet && mas.ProjectId == riskMatrix.ProjectId && app.IsActive == true
                                     select usr.UserName).SingleOrDefault();
                        break;
                    case "CorrelatedProject":
                        var correlatedProject = _databaseContext.CorrelatedProjects.SingleOrDefault(x => x.Id == id);
                        int correlatedProjecGet;
                        if (correlatedProject.UpdateBy != null)
                        {
                            correlatedProjecGet = _databaseContext.CorrelatedProjects.SingleOrDefault(x => x.ProjectId == correlatedProject.ProjectId
                            && x.ScenarioId == correlatedProject.ScenarioId
                            && x.SektorId == correlatedProject.SektorId
                            && x.CreateBy == correlatedProject.UpdateBy
                            && x.StatusId == 4 && x.IsDelete == false).Id;
                        }
                        else
                        {
                            correlatedProjecGet = _databaseContext.CorrelatedProjects.SingleOrDefault(x => x.ProjectId == correlatedProject.ProjectId
                            && x.ScenarioId == correlatedProject.ScenarioId
                            && x.SektorId == correlatedProject.SektorId
                            && x.CreateBy == correlatedProject.CreateBy
                            && x.StatusId == 4 && x.IsDelete == false).Id;
                        }
                        queryGetUserName =
                                    (from app in _databaseContext.Approval
                                     join mas in _databaseContext.MasterApprovalCorrelatedProjects on app.NomorUrutStatus + 1 equals mas.NomorUrutStatus
                                     join usr in _databaseContext.Users on mas.UserId equals usr.Id
                                     where app.RequestId == correlatedProjecGet && mas.ProjectId == correlatedProject.ProjectId && app.IsActive == true
                                     select usr.UserName).SingleOrDefault();

                        break;
                    case "CorrelatedSektor":
                        var correlatedSektor = _databaseContext.CorrelatedSektors.SingleOrDefault(x => x.Id == id);
                        int correlatedSektorGet;
                        if (correlatedSektor.UpdateBy != null)
                        {
                            correlatedSektorGet = _databaseContext.CorrelatedSektors.SingleOrDefault( x => 
                            x.ScenarioId == correlatedSektor.ScenarioId
                            && x.NamaSektor == correlatedSektor.NamaSektor
                            && x.SektorId == correlatedSektor.SektorId
                            && x.CreateBy == correlatedSektor.UpdateBy
                            && x.StatusId == 4 && x.IsDelete == false).Id;
                        }
                        else
                        {
                            correlatedSektorGet = _databaseContext.CorrelatedSektors.SingleOrDefault( x =>
                            x.ScenarioId == correlatedSektor.ScenarioId
                            && x.NamaSektor == correlatedSektor.NamaSektor
                            && x.SektorId == correlatedSektor.SektorId
                            && x.CreateBy == correlatedSektor.CreateBy
                            && x.StatusId == 4 && x.IsDelete == false).Id;
                        }
                        queryGetUserName =
                                    (from app in _databaseContext.Approval
                                     join mas in _databaseContext.MasterApprovalCorrelatedSektors on app.NomorUrutStatus + 1 equals mas.NomorUrutStatus
                                     join usr in _databaseContext.Users on mas.UserId equals usr.Id
                                     where app.RequestId == correlatedSektorGet  && app.IsActive == true
                                     select usr.UserName).SingleOrDefault();
                        break;
                    default:
                        
                        break;
                }


                //string userName = queryGetUserNam.ToString();
                if (queryGetUserName != null )
                {
                    return queryGetUserName.ToString();
                }
                else
                {
                    return null;
                }
                
            }
                return queryGetUserName.ToString();

        }

        public IEnumerable<Approval> GetAll()
        {

            return _databaseContext.Approval.ToList();
        }

        public IEnumerable<Approval> GetAll(string keyword)
        {
            IEnumerable<Approval> scenarioList = _databaseContext.Approval.Where(x => x.IsDelete == false).ToList();
            IList<Approval> result = new List<Approval>();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (scenarioList.Count() > 0)
                {
                    foreach (var item in scenarioList)
                    {
                        if (item.SourceApproval.ToLower().Contains(keyword.ToLower()))
                            result.Add(item);
                    }
                }
            }
            else
            {
                result = scenarioList.ToList();
            }
            #endregion filter

            return result;
        }

        public IEnumerable<Approval> GetAllBaseOnSource(int id, string sourceApproval)
        {
            return _databaseContext.Approval.Where( x=> x.RequestId == id && x.SourceApproval == sourceApproval && x.IsDelete == false).ToList();
        }

        public Approval GetBaseOnRequestId(int id)
        {
            return _databaseContext.Approval.SingleOrDefault(x => x.RequestId == id);
        }

        public Approval GetIdCreateByNoUrut(int id, int createBy, int noUrut)
        {
            return _databaseContext.Approval.SingleOrDefault(x => x.RequestId == id && x.CreateBy == createBy && x.NomorUrutStatus == noUrut && x.IsDelete == false);
        }


        public void Insert(Approval model)
        {
            _databaseContext.Approval.Add(model);
        }


        public void Update(Approval model)
        {
            _databaseContext.Approval.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }

        public void Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            if (model != null)
                model.Delete(deleteBy, deleteDate);
        }

        public void Delete(int id, string source, int deleteBy, DateTime deleteDate)
        {
            IEnumerable<Approval> approvals = this.GetAllBaseOnSource(id, source);

            foreach (var item in approvals)
            {
                this.Delete(item.Id, deleteBy, deleteDate);
            }

            if (approvals == null)
                return;
        }

        public IEnumerable<Approval> GetSentItem(bool sentItem, int userId, string keyword, int fieldId)
        {
            var approvalList = _databaseContext.Approval.Where(x => x.CreateBy == userId).ToList();
            IList<Approval> result = new List<Approval>();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (approvalList.Count > 0)
                {
                    for (int i = 0; i < approvalList.Count; i++)
                    {
                        var requestId = approvalList[i].RequestId;
                        switch (fieldId)
                        {
                            case 1:
                                var sceanario = _databaseContext.Scenarios.Where(x => x.Id == requestId).FirstOrDefault();
                                if (sceanario != null)
                                {
                                    if (sceanario.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                    {
                                        result.Add(approvalList[i]);
                                    }
                                }
                                break;
                            case 2:
                                if (approvalList[i].SourceApproval.ToLower().Contains(keyword.ToLower()))
                                {
                                    result.Add(approvalList[i]);
                                }
                                break;
                            case 3:
                                switch (approvalList[i].SourceApproval)
                                {
                                    case "RiskMatrixProject":
                                        var riskMatrix = _databaseContext.RiskMatrixProjects.Where(x => x.Id == requestId).FirstOrDefault();
                                        if (riskMatrix != null)
                                        {
                                            if (riskMatrix.Project.NamaProject.ToLower().Contains(keyword.ToLower()))
                                            {
                                                result.Add(approvalList[i]);
                                            }
                                        }
                                        break;
                                    case "Scenario":
                                        var scena = _databaseContext.Scenarios.Where(x => x.Id == requestId).FirstOrDefault();
                                        if (scena != null)
                                        {
                                            if (scena.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                            {
                                                result.Add(approvalList[i]);
                                            }
                                        }
                                        break;
                                    case "CorrelatedProject":
                                        var correlatedProject = _databaseContext.CorrelatedProjects.Where(x => x.Id == requestId).FirstOrDefault();
                                        if (correlatedProject != null)
                                        {
                                            if (correlatedProject.Project.NamaProject.ToLower().Contains(keyword.ToLower()))
                                            {
                                                result.Add(approvalList[i]);
                                            }
                                        }
                                        break;
                                    case "CorrelatedSektor":
                                        var correlatedSektor = _databaseContext.CorrelatedSektors.Where(x => x.Id == requestId).FirstOrDefault();
                                        if (correlatedSektor != null)
                                        {
                                            if (correlatedSektor.NamaSektor.ToLower().Contains(keyword.ToLower()))
                                            {
                                                result.Add(approvalList[i]);
                                            }
                                        }
                                        break;
                                }
                                break;
                        }
                    }
                }
            }
            else
            {
                result = approvalList.ToList();
            }
            #endregion filter

            return result.OrderByDescending(x => x.Id).ToList();
        }

        public IEnumerable<Approval> GetSentItem(bool sentItem, int userId, string keyword, int fieldId, int fieldId2)
        {
            var approvalList = _databaseContext.Approval.Where(x => x.CreateBy == userId).ToList();
            IList<Approval> result = new List<Approval>();
            IList<Approval> results = new List<Approval>();
            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (approvalList.Count > 0)
                {
                    for (int i = 0; i < approvalList.Count; i++)
                    {
                        var requestId = approvalList[i].RequestId;
                        switch (fieldId)
                        {
                            case 1:
                                var sceanario = _databaseContext.Scenarios.Where(x => x.Id == requestId).FirstOrDefault();
                                if (fieldId2 == 5 || (approvalList[i].Status == null && fieldId2 == 4))
                                {
                                    if (sceanario != null)
                                    {
                                        if (sceanario.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                        {
                                            result.Add(approvalList[i]);
                                        }
                                    }
                                }
                                if (approvalList[i].Status != null && fieldId2 != 5)
                                {
                                    if (sceanario != null)
                                    {
                                        if (sceanario.NamaScenario.ToLower().Contains(keyword.ToLower()) && approvalList[i].StatusId == fieldId2)
                                        {
                                            result.Add(approvalList[i]);
                                        }
                                    }
                                }
                                
                                //if (sceanario != null)
                                //{
                                //    if (sceanario.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                //    {
                                //        result.Add(approvalList[i]);
                                //    }
                                //}
                                break;
                            case 2:
                                if (fieldId2 == 5 || (approvalList[i].Status == null && fieldId2 == 4))
                                {
                                    if (approvalList[i].SourceApproval.ToLower().Contains(keyword.ToLower()))
                                    {
                                        result.Add(approvalList[i]);
                                    }
                                }
                                if (approvalList[i].Status != null && fieldId2 != 5)
                                {
                                    if (approvalList[i].SourceApproval.ToLower().Contains(keyword.ToLower()) && approvalList[i].StatusId == fieldId2)
                                    {
                                        result.Add(approvalList[i]);
                                    }
                                }

                                //if (approvalList[i].SourceApproval.ToLower().Contains(keyword.ToLower()))
                                //{
                                //    result.Add(approvalList[i]);
                                //}
                                break;
                            case 3:
                                switch (approvalList[i].SourceApproval)
                                {
                                    case "RiskMatrixProject":
                                        var riskMatrix = _databaseContext.RiskMatrixProjects.Where(x => x.Id == requestId).FirstOrDefault();
                                        if (fieldId2 == 5 || (approvalList[i].Status == null && fieldId2 == 4))
                                        {
                                            if (riskMatrix != null)
                                            {
                                                if (riskMatrix.Project.NamaProject.ToLower().Contains(keyword.ToLower()))
                                                {
                                                    result.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        if (approvalList[i].Status != null && fieldId2 != 5)
                                        {
                                            if (riskMatrix != null)
                                            {
                                                if (riskMatrix.Project.NamaProject.ToLower().Contains(keyword.ToLower()) && approvalList[i].StatusId == fieldId2)
                                                {
                                                    result.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        break;
                                    case "Scenario":
                                        var scena = _databaseContext.Scenarios.Where(x => x.Id == requestId).FirstOrDefault();
                                        if (fieldId2 == 5 || (approvalList[i].Status == null && fieldId2 == 4))
                                        {
                                            if (scena != null)
                                            {
                                                if (scena.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                                {
                                                    result.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        if (approvalList[i].Status != null && fieldId2 != 5)
                                        {
                                            if (scena != null)
                                            {
                                                if (scena.NamaScenario.ToLower().Contains(keyword.ToLower()) && approvalList[i].StatusId == fieldId2)
                                                {
                                                    result.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        //if (scena != null)
                                        //{
                                        //    if (scena.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                        //    {
                                        //        result.Add(approvalList[i]);
                                        //    }
                                        //}
                                        break;
                                    case "CorrelatedProject":
                                        var correlatedProject = _databaseContext.CorrelatedProjects.Where(x => x.Id == requestId).FirstOrDefault();
                                        if (fieldId2 == 5 || (approvalList[i].Status == null && fieldId2 == 4))
                                        {
                                            if (correlatedProject != null)
                                            {
                                                if (correlatedProject.Project.NamaProject.ToLower().Contains(keyword.ToLower()))
                                                {
                                                    result.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        if (approvalList[i].Status != null && fieldId2 != 5)
                                        {
                                            if (correlatedProject != null)
                                            {
                                                if (correlatedProject.Project.NamaProject.ToLower().Contains(keyword.ToLower()) && approvalList[i].StatusId == fieldId2)
                                                {
                                                    result.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        //if (correlatedProject != null)
                                        //{
                                        //    if (correlatedProject.Project.NamaProject.ToLower().Contains(keyword.ToLower()))
                                        //    {
                                        //        result.Add(approvalList[i]);
                                        //    }
                                        //}
                                        break;
                                    case "CorrelatedSektor":
                                        var correlatedSektor = _databaseContext.CorrelatedSektors.Where(x => x.Id == requestId).FirstOrDefault();
                                        if (fieldId2 == 5 || (approvalList[i].Status == null && fieldId2 == 4))
                                        {
                                            if (correlatedSektor != null)
                                            {
                                                if (correlatedSektor.NamaSektor.ToLower().Contains(keyword.ToLower()))
                                                {
                                                    result.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        if (approvalList[i].Status != null && fieldId2 != 5)
                                        {
                                            if (correlatedSektor != null)
                                            {
                                                if (correlatedSektor.NamaSektor.ToLower().Contains(keyword.ToLower()) && approvalList[i].StatusId == fieldId2)
                                                {
                                                    result.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        //if (correlatedSektor != null)
                                        //{
                                        //    if (correlatedSektor.NamaSektor.ToLower().Contains(keyword.ToLower()))
                                        //    {
                                        //        result.Add(approvalList[i]);
                                        //    }
                                        //}
                                        break;
                                }
                                break;
                        }
                    }
                }
            }
            else
            {
                if (fieldId2 == 0)
                {
                    results = approvalList;
                    //result = approvalList.ToList();
                }
                else
                {
                    foreach (var item in approvalList)
                    {
                        if (fieldId2 == 5 || (item.Status == null && fieldId2 == 4))
                        {
                            results.Add(item);
                        }
                        if (item.Status != null && fieldId2 != 5)
                        {
                            if (item.StatusId == fieldId2)
                                results.Add(item);
                            //if (item.Status.StatusDescription.ToLower().Contains(keyword.ToLower()))
                            //    result.Add(item);
                        }
                    }
                }
               // result = approvalList.ToList();
            }
            #endregion filter

            return results.OrderByDescending(x => x.Id).ToList();
        }

        public IEnumerable<Approval> GetByRequestId(int requestId)
        {
            return _databaseContext.Approval.Where(x => x.RequestId == requestId).ToList();
        }

        public IEnumerable<Approval> GetItemActive(string keyword, int fieldId)
        {
            var approvalList = _databaseContext.Approval.Where(x => x.IsActive == true).ToList();
            IList<Approval> result = new List<Approval>();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (approvalList.Count > 0)
                {
                    for (int i = 0; i < approvalList.Count; i++)
                    {
                        var requestId = approvalList[i].RequestId;
                        switch (fieldId)
                        {
                            case 1:
                                var sceanario = _databaseContext.Scenarios.Where(x => x.Id == requestId).FirstOrDefault();
                                if (sceanario != null)
                                {
                                    if (sceanario.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                    {
                                        result.Add(approvalList[i]);
                                    }
                                }
                                break;
                            case 2:
                                if (approvalList[i].SourceApproval.ToLower().Contains(keyword.ToLower()))
                                {
                                    result.Add(approvalList[i]);
                                }
                                break;
                            case 3:
                                switch (approvalList[i].SourceApproval)
                                {
                                    case "RiskMatrixProject":
                                        var riskMatrix = _databaseContext.RiskMatrixProjects.Where(x => x.Id == requestId).FirstOrDefault();
                                        if (riskMatrix != null)
                                        {
                                            if (riskMatrix.Project.NamaProject.ToLower().Contains(keyword.ToLower()))
                                            {
                                                result.Add(approvalList[i]);
                                            }
                                        }
                                        break;
                                    case "Scenario":
                                        var scena = _databaseContext.Scenarios.Where(x => x.Id == requestId).FirstOrDefault();
                                        if (scena != null)
                                        {
                                            if (scena.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                            {
                                                result.Add(approvalList[i]);
                                            }
                                        }
                                        break;
                                    case "CorrelatedProject":
                                        var correlatedProject = _databaseContext.CorrelatedProjects.Where(x => x.Id == requestId).FirstOrDefault();
                                        if (correlatedProject != null)
                                        {
                                            if (correlatedProject.Project.NamaProject.ToLower().Contains(keyword.ToLower()))
                                            {
                                                result.Add(approvalList[i]);
                                            }
                                        }
                                        break;
                                    case "CorrelatedSektor":
                                        var correlatedSektor = _databaseContext.CorrelatedSektors.Where(x => x.Id == requestId).FirstOrDefault();
                                        if (correlatedSektor != null)
                                        {
                                            if (correlatedSektor.NamaSektor.ToLower().Contains(keyword.ToLower()))
                                            {
                                                result.Add(approvalList[i]);
                                            }
                                        }
                                        break;
                                }
                                break;
                        }
                    }
                }
            }
            else
            {
                result = approvalList.ToList();
            }
            #endregion filter

            return result.OrderByDescending(x => x.Id).ToList();
        }

        public IEnumerable<Approval> GetItemActive(string keyword, int fieldId, int fieldId2)
        {
            var approvalList = _databaseContext.Approval.Where(x => x.IsActive == true).ToList();
            IList<Approval> result = new List<Approval>();
            IList<Approval> results = new List<Approval>();
            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (approvalList.Count > 0)
                {
                    for (int i = 0; i < approvalList.Count; i++)
                    {
                        var requestId = approvalList[i].RequestId;
                        var sceanario = _databaseContext.Scenarios.Where(x => x.Id == requestId).FirstOrDefault();
                        switch (fieldId)
                        {
                            case 1:
                                switch (approvalList[i].SourceApproval)
                                {
                                    case "RiskMatrixProject":
                                        var riskMatrix = _databaseContext.RiskMatrixProjects.Where(x => x.Id == requestId).FirstOrDefault();
                                        sceanario = _databaseContext.Scenarios.Where(x => x.Id == riskMatrix.ScenarioId).FirstOrDefault();
                                        if (fieldId2 == 5 || (approvalList[i].Status == null && fieldId2 == 4))
                                        {
                                            if (sceanario != null)
                                            {
                                                if (sceanario.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                                {
                                                    results.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        if (approvalList[i].Status != null && fieldId2 != 5)
                                        {
                                            if (sceanario != null)
                                            {
                                                if (sceanario.NamaScenario.ToLower().Contains(keyword.ToLower()) && approvalList[i].StatusId == fieldId2)
                                                {
                                                    results.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        break;
                                    case "Scenario":
                                        if (fieldId2 == 5 || (approvalList[i].Status == null && fieldId2 == 4))
                                        {
                                            if (sceanario != null)
                                            {
                                                if (sceanario.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                                {
                                                    results.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        if (approvalList[i].Status != null && fieldId2 != 5)
                                        {
                                            if (sceanario != null)
                                            {
                                                if (sceanario.NamaScenario.ToLower().Contains(keyword.ToLower()) && approvalList[i].StatusId == fieldId2)
                                                {
                                                    results.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        break;
                                    case "CorrelatedProject":
                                        var correlatedProject = _databaseContext.CorrelatedProjects.Where(x => x.Id == requestId).FirstOrDefault();
                                        sceanario = _databaseContext.Scenarios.Where(x => x.Id == correlatedProject.ScenarioId).FirstOrDefault();
                                        if (fieldId2 == 5 || (approvalList[i].Status == null && fieldId2 == 4))
                                        {
                                            if (sceanario != null)
                                            {
                                                if (sceanario.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                                {
                                                    results.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        if (approvalList[i].Status != null && fieldId2 != 5)
                                        {
                                            if (sceanario != null)
                                            {
                                                if (sceanario.NamaScenario.ToLower().Contains(keyword.ToLower()) && approvalList[i].StatusId == fieldId2)
                                                {
                                                    results.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        break;
                                    case "CorrelatedSektor":
                                        var correlatedSektor = _databaseContext.CorrelatedSektors.Where(x => x.Id == requestId).FirstOrDefault();
                                        sceanario = _databaseContext.Scenarios.Where(x => x.Id == correlatedSektor.ScenarioId).FirstOrDefault();
                                        if (fieldId2 == 5 || (approvalList[i].Status == null && fieldId2 == 4))
                                        {
                                            if (sceanario != null)
                                            {
                                                if (sceanario.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                                {
                                                    results.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        if (approvalList[i].Status != null && fieldId2 != 5)
                                        {
                                            if (sceanario != null)
                                            {
                                                if (sceanario.NamaScenario.ToLower().Contains(keyword.ToLower()) && approvalList[i].StatusId == fieldId2)
                                                {
                                                    results.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        break;
                                }   
                                break;
                            case 2:
                                if (fieldId2 == 5 || (approvalList[i].Status == null && fieldId2 == 4))
                                {
                                    if (approvalList[i].SourceApproval.ToLower().Contains(keyword.ToLower()))
                                    {
                                        results.Add(approvalList[i]);
                                    }
                                }
                                if (approvalList[i].Status != null && fieldId2 != 5)
                                {
                                    if (approvalList[i].SourceApproval.ToLower().Contains(keyword.ToLower()) && approvalList[i].StatusId == fieldId2)
                                    {
                                        results.Add(approvalList[i]);
                                    }
                                }
                                break;
                            case 3:
                                switch (approvalList[i].SourceApproval)
                                {
                                    case "RiskMatrixProject":
                                        var riskMatrix = _databaseContext.RiskMatrixProjects.Where(x => x.Id == requestId).FirstOrDefault();
                                        if (fieldId2 == 5 || (approvalList[i].Status == null && fieldId2 == 4))
                                        {
                                            if (riskMatrix.Project.NamaProject.ToLower().Contains(keyword.ToLower()))
                                            {
                                                results.Add(approvalList[i]);
                                            }
                                        }
                                        if (approvalList[i].Status != null && fieldId2 != 5)
                                        {
                                            if (riskMatrix.Project.NamaProject.ToLower().Contains(keyword.ToLower()) && approvalList[i].StatusId == fieldId2)
                                            {
                                                results.Add(approvalList[i]);
                                            }
                                        }
                                        break;
                                    case "Scenario":
                                        var scena = _databaseContext.Scenarios.Where(x => x.Id == requestId).FirstOrDefault();
                                        if (fieldId2 == 5 || (approvalList[i].Status == null && fieldId2 == 4))
                                        {
                                            if (scena != null)
                                            {
                                                if (scena.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                                {
                                                    results.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        if (approvalList[i].Status != null && fieldId2 != 5)
                                        {
                                            if (scena != null)
                                            {
                                                if (scena.NamaScenario.ToLower().Contains(keyword.ToLower()) && approvalList[i].StatusId == fieldId2)
                                                {
                                                    results.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        break;
                                    case "CorrelatedProject":
                                        var correlatedProject = _databaseContext.CorrelatedProjects.Where(x => x.Id == requestId).FirstOrDefault();
                                        if (fieldId2 == 5 || (approvalList[i].Status == null && fieldId2 == 4))
                                        {
                                            if (correlatedProject != null)
                                            {
                                                if (correlatedProject.Project.NamaProject.ToLower().Contains(keyword.ToLower()))
                                                {
                                                    results.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        if (approvalList[i].Status != null && fieldId2 != 5)
                                        {
                                            if (correlatedProject != null)
                                            {
                                                if (correlatedProject.Project.NamaProject.ToLower().Contains(keyword.ToLower()) && approvalList[i].StatusId == fieldId2)
                                                {
                                                    results.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        break;
                                    case "CorrelatedSektor":
                                        var correlatedSektor = _databaseContext.CorrelatedSektors.Where(x => x.Id == requestId).FirstOrDefault();
                                        if (fieldId2 == 5 || (approvalList[i].Status == null && fieldId2 == 4))
                                        {
                                            if (correlatedSektor != null)
                                            {
                                                if (correlatedSektor.NamaSektor.ToLower().Contains(keyword.ToLower()))
                                                {
                                                    results.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        if (approvalList[i].Status != null && fieldId2 != 5)
                                        {
                                            if (correlatedSektor != null)
                                            {
                                                if (correlatedSektor.NamaSektor.ToLower().Contains(keyword.ToLower()) && approvalList[i].StatusId == fieldId2)
                                                {
                                                    results.Add(approvalList[i]);
                                                }
                                            }
                                        }
                                        break;
                                }
                                break;
                        }
                    }
                }
            }
            else
            {
                if (fieldId2 == 0)
                {
                   // result = approvalList.ToList();
                    results = approvalList;
                }
                else
                {
                    foreach (var item in approvalList)
                    {
                        if (fieldId2 == 5 || (item.Status == null && fieldId2 == 4))
                        {
                            results.Add(item);
                        }
                        if (item.Status != null && fieldId2 != 5)
                        {
                            if (item.StatusId == fieldId2)
                                results.Add(item);
                            //if (item.Status.StatusDescription.ToLower().Contains(keyword.ToLower()))
                            //    result.Add(item);
                        }
                    }
                }
                //result = approvalList.ToList();
            }
            #endregion filter

            return results.OrderByDescending(x => x.Id).ToList();
        }
    }
}
