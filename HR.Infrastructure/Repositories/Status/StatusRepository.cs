﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;


namespace HR.Infrastructure.Repositories
{
    public class StatusRepository : IStatusRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public StatusRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public Status Get(int id)
        {
            return _databaseContext.Status.SingleOrDefault(x => x.Id == id);
        }

        //get status base on role id when data is add
        public Status GetStatus(int? id)
        {
            if (id == 3)
            {
                return _databaseContext.Status.SingleOrDefault(x => x.Id == 2);
            }
            else
            {
                return _databaseContext.Status.SingleOrDefault(x => x.Id == 1);
            }
            
        }

        public IEnumerable<Status> GetAll()
        {
            //return _databaseContext.AssetDatas.AsQueryable();
            return _databaseContext.Status.ToList();

        }

        public IEnumerable<Status> GetAll(string keyword)
        {
            IEnumerable<Status> statusList = _databaseContext.Status.ToList();
            IList<Status> result = new List<Status>();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (statusList.Count() > 0)
                {
                    int dataNamaStatus = 0;
                    foreach (var item in statusList)
                    {
                        if (item.StatusDescription.ToLower().Contains(keyword.ToLower()))
                        {
                            result.Add(item);
                            dataNamaStatus += 1;
                        }
                    }

                    //if (dataNamaProject > 0)
                    //{
                    //    foreach (var item in result)
                    //    {
                    //        if (item.Minimum.ToString().ToLower().Contains(keyword.ToLower()))
                    //            result.Add(item);
                    //    }
                    //}
                    //else
                    //{
                    //    result = projectList.ToList();
                    //    foreach (var item in result)
                    //    {
                    //        if (item.Minimum.ToString().ToLower().Contains(keyword.ToLower()))
                    //            result.Add(item);
                    //    }
                    //}
                    
                }
            }
            else
            {
                result = statusList.ToList();
            }
            #endregion filter

            return result;
        }

    }
}
