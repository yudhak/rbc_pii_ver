﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;


namespace HR.Infrastructure.Repositories
{
    public class AssetDataRepository : IAssetDataRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public AssetDataRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public AssetData Get(int id)
        {
            return _databaseContext.AssetDatas.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<AssetData> GetAll()
        {
            return _databaseContext.AssetDatas.Where(x => x.IsDelete == false).ToList();
        }

        public IEnumerable<AssetData> GetAll(string keyword, int id)
        {
            IEnumerable<AssetData> assetDataList = _databaseContext.AssetDatas.Where(x => x.IsDelete == false).ToList();
            IList<AssetData> result = new List<AssetData>();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (assetDataList.Count() > 0)
                {
                    foreach (var item in assetDataList)
                    {
                        if (id == 1)
                        {
                            if (item.AssetClass.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 2)
                        {
                            if (item.TermAwal.ToString().ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 3)
                        {
                            if (item.TermAkhir.ToString().ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 4)
                        {
                            if (item.AssumedReturnPercentage.ToString().ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 5)
                        {
                            if (item.OutstandingStartYears.ToString().ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 6)
                        {
                            if (item.OutstandingEndYears.ToString().ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 7)
                        {
                            if (item.AssetValue.ToString().ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 8)
                        {
                            if (item.Porpotion.ToString().ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 9)
                        {
                            if (item.AssumedReturn.ToString().ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                    }
                }
            }
            else
            {
                result = assetDataList.ToList();
            }
            #endregion filter

            return result;
        }

        public void Insert(AssetData model)
        {
            _databaseContext.AssetDatas.Add(model);
        }

        public bool IsExist(int id, string assetClass)
        {
            var results = _databaseContext.AssetDatas.Where(x => x.AssetClass.ToLower() == assetClass.ToLower() && x.IsDelete == false && x.Id != id).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public bool IsExist(string assetClass)
        {
            var results = _databaseContext.AssetDatas.Where(x => x.AssetClass.ToLower() == assetClass.ToLower() && x.IsDelete == false).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public void Update(AssetData model)
        {
            _databaseContext.AssetDatas.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }

        public void Delete(int id, int deleteBy, DateTime deleteDate)
        {
            throw new NotImplementedException();
        }
    }
}
