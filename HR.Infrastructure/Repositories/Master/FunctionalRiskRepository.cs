﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;


namespace HR.Infrastructure.Repositories
{
    public class FunctionalRiskRepository : IFunctionalRiskRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public FunctionalRiskRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public FunctionalRisk Get(int id)
        {
            return _databaseContext.FunctionalRisks.SingleOrDefault(x => x.Id == id);
        }

        public FunctionalRisk GetYellowByMatrix(int mId, int sId)
        {
            return _databaseContext.FunctionalRisks.SingleOrDefault(x => x.MatrixId == mId && x.ColorCommentId == 2 && x.ScenarioId == sId);
        }

        public FunctionalRisk GetCommentByColor(int mId, int sId, int cId)
        {
            return _databaseContext.FunctionalRisks.SingleOrDefault(x => x.MatrixId == mId && x.ColorCommentId == cId && x.ScenarioId == sId);
        }

        public IEnumerable<FunctionalRisk> GetAll(string keyword, int id)
        {
            IEnumerable<FunctionalRisk> functionalRisk = _databaseContext.FunctionalRisks.Where(x => x.IsDelete == false).ToList();
            IList<FunctionalRisk> result = new List<FunctionalRisk>();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (functionalRisk.Count() > 0)
                {
                    foreach (var item in functionalRisk)
                    {
                        if (id == 1)
                        {
                            if (item.Matrix.NamaMatrix.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 2)
                        {
                            if (item.Matrix.NamaFormula.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 3)
                        {
                            if (item.Scenario.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 4)
                        {
                            if (item.Definisi != null)
                            {
                                if (item.Definisi.ToLower().Contains(keyword.ToLower()))
                                    result.Add(item);
                            }                          
                        }
                        if (id == 5)
                        {
                            if (item.NilaiMaksimum.ToString().ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 6)
                        {
                            if (item.NilaiMinimum.ToString().ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 7)
                        {
                            if(item.Komentar != null)
                            {
                                if (item.Komentar.ToLower().Contains(keyword.ToLower()))
                                    result.Add(item);
                            }                           
                        }
                    }
                }
            }
            else
            {
                result = functionalRisk.ToList();
            }
            #endregion filter

            return result;
        }
        public IEnumerable<FunctionalRisk> GetAllFalse()
        {
            return _databaseContext.FunctionalRisks.ToList();
        }

        public IEnumerable<FunctionalRisk> GetByScenarioId(int scenarioId)
        {
            return _databaseContext.FunctionalRisks.Where(x => x.ScenarioId == scenarioId).ToList();
        }

        public void Insert(FunctionalRisk model)
        {
            _databaseContext.FunctionalRisks.Add(model);
        }

        public bool IsExist(int id, string definisi)
        {
            var results = _databaseContext.FunctionalRisks.Where(x => x.Id != id && x.Definisi == definisi && x.IsDelete == false).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public bool IsExist(string definisi)
        {
            var results = _databaseContext.FunctionalRisks.Where(x => x.Definisi == definisi && x.IsDelete == false).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public void Update(FunctionalRisk model)
        {
            _databaseContext.FunctionalRisks.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }

        public void Delete(int id, int deleteBy, DateTime deleteDate)
        {
            throw new NotImplementedException();
        }
    }
}
