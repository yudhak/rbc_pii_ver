﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace HR.Infrastructure.Repositories
{
    public class StageRepository : IStageRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public StageRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public Stage Get(int id)
        {
            return _databaseContext.Stages.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<Stage> GetAll()
        {
            return _databaseContext.Stages.Where(x => x.IsDelete == false).ToList();
        }
        public IEnumerable<Stage> GetAll(string keyword, int id)
        {
            IEnumerable<Stage> stageList = _databaseContext.Stages.Where(x => x.IsDelete == false).ToList();
            IList<Stage> result = new List<Stage>();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (stageList.Count() > 0)
                {
                    foreach (var item in stageList)
                    {
                        if(id == 1)
                        {
                            if (item.NamaStage.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 2)
                        {
                            if (item.Keterangan != null)
                            {
                                if (item.Keterangan.ToLower().Contains(keyword.ToLower()))
                                    result.Add(item);
                            }                           
                        }
                    }
                }
            }
            else
            {
                result = stageList.ToList();
            }
            #endregion filter

            return result;
        }

        public void Insert(Stage model)
        {
            _databaseContext.Stages.Add(model);
        }

        public bool IsExist(string namaStage)
        {
            var results = _databaseContext.Stages.Where(x => x.NamaStage.ToLower() == namaStage.ToLower() && x.IsDelete == false).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public bool IsExist(int id, string namaStage)
        {
            var results = _databaseContext.Stages.Where(x => x.NamaStage.ToLower() == namaStage.ToLower() && x.IsDelete == false && x.Id != id).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public void Update(Stage model)
        {
            _databaseContext.Stages.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }

        public void Delete(int id, int deleteBy, DateTime deleteDate)
        {
            throw new NotImplementedException();
        }
    }
}
