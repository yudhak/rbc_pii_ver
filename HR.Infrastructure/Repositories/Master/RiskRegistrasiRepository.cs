﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;


namespace HR.Infrastructure.Repositories
{
    public class RiskRegistrasiRepository : IRiskRegistrasiRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public RiskRegistrasiRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public RiskRegistrasi Get(int id)
        {
            return _databaseContext.RiskRegistrasis.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<RiskRegistrasi> GetAll()
        {
            return _databaseContext.RiskRegistrasis.Where(x => x.IsDelete == false).OrderBy(o => o.KodeMRisk).ToList();
        }
        public IEnumerable<RiskRegistrasi> GetAll(string keyword, int id)
        {
            IEnumerable<RiskRegistrasi> riskRegistrasiList = _databaseContext.RiskRegistrasis.Where(x => x.IsDelete == false).ToList();
            IList<RiskRegistrasi> result = new List<RiskRegistrasi>();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (riskRegistrasiList.Count() > 0)
                {
                    foreach (var item in riskRegistrasiList)
                    {
                        if(id == 1)
                        {
                            if (item.KodeMRisk.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if(id == 2)
                        {
                            if (item.NamaCategoryRisk.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if(id == 3)
                        {
                            if (item.Definisi.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if(id == 4)
                        {
                            if (item.Minimum.ToString().ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if(id == 5)
                        {
                            if (item.Maximum.ToString().ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }                     
                    }
                }
            }
            else
            {
                result = riskRegistrasiList.ToList();
            }
            #endregion filter

            return result;
        }

        public void Insert(RiskRegistrasi model)
        {
            _databaseContext.RiskRegistrasis.Add(model);
        }

        public bool IsExist(int id, string namaCategoryRisk)
        {
            var results = _databaseContext.RiskRegistrasis.Where(x => x.NamaCategoryRisk.ToLower() == namaCategoryRisk.ToLower() && x.IsDelete == false && x.Id != id).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public bool IsExist(string kodeMRisk)
        {
            var results = _databaseContext.RiskRegistrasis.Where(x => x.KodeMRisk.ToLower() == kodeMRisk.ToLower() && x.IsDelete == false).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public void Update(RiskRegistrasi model)
        {
            _databaseContext.RiskRegistrasis.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }

        public void Delete(int id, int deleteBy, DateTime deleteDate)
        {
            throw new NotImplementedException();
        }
    }
}
