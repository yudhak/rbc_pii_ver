﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace HR.Infrastructure.Repositories
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly IDatabaseContext _databaseContext;
        public ProjectRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public void Delete(int id, int? deleteBy, DateTime? deleteDate)
        {
            throw new NotImplementedException();
        }

        public Project Get(int id)
        {
            return _databaseContext.Projects.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<Project> GetAll(string keyword, int id)
        {
            IEnumerable<Project> projectList = _databaseContext.Projects.Where(x => x.IsDelete == false).ToList();
            IList<Project> result = new List<Project>();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (projectList.Count() > 0)
                {   
                    int dataNamaProject = 0;
                    foreach (var item in projectList)
                    {
                        if(id == 1)
                        {
                            if (item.NamaProject.ToLower().Contains(keyword.ToLower()))
                            {
                                result.Add(item);
                                dataNamaProject += 1;
                            }
                        }
                        if (id == 2)
                        {
                            if (item.Sektor.NamaSektor.ToLower().Contains(keyword.ToLower()))
                            {
                                result.Add(item);
                                dataNamaProject += 1;
                            }
                        }
                        if (id == 3)
                        {
                            if (item.Maximum.ToString().ToLower().Contains(keyword.ToLower()))
                            {
                                result.Add(item);
                                dataNamaProject += 1;
                            }
                        }
                        if (id == 4)
                        {
                            if (item.Tahapan.NamaTahapan.ToLower().Contains(keyword.ToLower()))
                            {
                                result.Add(item);
                                dataNamaProject += 1;
                            }
                        }
                        if (id == 5)
                        {
                            if (item.Sektor.NamaSektor.ToLower().Contains(keyword.ToLower()))
                            {
                                result.Add(item);
                                dataNamaProject += 1;
                            }
                        }
                        if (id == 6)
                        {
                            if (item.User.UserName.ToLower().Contains(keyword.ToLower()))
                            {
                                result.Add(item);
                                dataNamaProject += 1;
                            }
                        }
                    }

                    //if (dataNamaProject > 0)
                    //{
                    //    foreach (var item in result)
                    //    {
                    //        if (item.Minimum.ToString().ToLower().Contains(keyword.ToLower()))
                    //            result.Add(item);
                    //    }
                    //}
                    //else
                    //{
                    //    result = projectList.ToList();
                    //    foreach (var item in result)
                    //    {
                    //        if (item.Minimum.ToString().ToLower().Contains(keyword.ToLower()))
                    //            result.Add(item);
                    //    }
                    //}
                    
                }
            }
            else
            {
                result = projectList.ToList();
            }
            #endregion filter

            return result;
        }

        public IEnumerable<Project> GetAll()
        {
            IEnumerable<Project> projectList = _databaseContext.Projects.Where(x => x.IsDelete == false).ToList();
            IList<Project> result = new List<Project>();

            return projectList;
        }

        public void Insert(Project model)
        {
            _databaseContext.Projects.Add(model);
        }

        public bool IsExist(int id, string namaProject)
        {
            var results = _databaseContext.Projects.Where(x => x.NamaProject.ToLower() == namaProject.ToLower() && x.IsDelete == false && x.Id != id).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public bool IsExist(string namaProject)
        {
            var results = _databaseContext.Projects.Where(x => x.NamaProject.ToLower() == namaProject.ToLower() && x.IsDelete == false).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public void Update(Project model)
        {
            _databaseContext.Projects.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }

        public IEnumerable<Project> GetAllActive()
        {
            return _databaseContext.Projects.Where(x => x.IsActive == true && x.IsDelete == false).ToList();
        }

        public Project GetProjectActive(int id)
        {
            return _databaseContext.Projects.SingleOrDefault(x => x.Id == id && x.IsActive == true && x.IsDelete == false);
        }

        public IEnumerable<Project> GetByTahapan(int scenarioId, string tahapanIds)
        {
            var tahapanIdCollection = tahapanIds.Split(',').Select(Int32.Parse).ToList();
            var projectByScenario = _databaseContext.ScenarioDetails.Where(x => x.ScenarioId == scenarioId && x.IsDelete == false).ToList();

            IList<Project> projects = new List<Project>();
            if (tahapanIdCollection.Count > 0)
            {
                for (int i = 0; i < tahapanIdCollection.Count; i++)
                {
                    var tahapId = tahapanIdCollection[i];
                    //var project = _databaseContext.Projects.Where(x => x.IsDelete == false && x.IsActive == true && x.TahapanId == tahapId).ToList();
                    if (projectByScenario != null)
                    {
                        var data = projectByScenario.Where(x => x.Project.TahapanId == tahapId).ToList();
                        for (int p = 0; p < data.Count; p++)
                        {
                            projects.Add(data[p].Project);
                        }
                    }
                }
            }

            return projects;
        }

        public IEnumerable<Project> GetByTahapan(List<ScenarioDetail> projectScenario)
        {
            //var projectScenario = _databaseContext.ScenarioDetails.Where(x => x.ScenarioId == scenarioId && x.IsDelete == false).ToList();
            var projectMonitoring = _databaseContext.Projects.Where(x => x.TahapanId == 6).ToList();


            var projects = projectMonitoring    // your starting point - table in the "from" statement
                .Join(projectScenario, // the source table of the inner join
                proj => proj.Id,        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
                scdet => scdet.ProjectId,   // Select the foreign key (the second part of the "on" clause)
                (proj, scdet) => proj).ToList(); // selection
            return projects;
        }
    }
}
