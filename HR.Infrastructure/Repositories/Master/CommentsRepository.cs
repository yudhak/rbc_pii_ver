﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;


namespace HR.Infrastructure.Repositories
{
    public class CommentsRepository : ICommentsRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public CommentsRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public Comments Get(int id)
        {
            return _databaseContext.Commentss.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<Comments> GetAll()
        {
            return _databaseContext.Commentss.Where(x => x.IsDelete == false).ToList();
        }
        public IEnumerable<Comments> GetByColorId(int colorId)
        {
            return _databaseContext.Commentss.Where(x => x.ColorCommentId == colorId && x.IsDelete == false).ToList();
        }

        public IEnumerable<Comments> GetByColorId(int colorId, string keyword)
        {
            IEnumerable<Comments> projectList = _databaseContext.Commentss.Where(x => x.ColorCommentId == colorId && x.IsDelete == false).ToList();
            IList<Comments> result = new List<Comments>();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (projectList.Count() > 0)
                {
                    foreach (var item in projectList)
                    {
                        if (item.Comment.ToLower().Contains(keyword.ToLower()))
                            result.Add(item);
                    }
                }
            }
            else
            {
                result = projectList.ToList();
            }
            #endregion filter

            return result;
        }

        public void Insert(Comments model)
        {
            _databaseContext.Commentss.Add(model);
        }

        public bool IsExist(int id, string comment, int colorCommentId)
        {
            var results = _databaseContext.Commentss.Where(x => x.Comment.ToLower() == comment.ToLower() && x.ColorCommentId == colorCommentId && x.IsDelete == false && x.Id != id).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public bool IsExist(string comment, int colorCommentId)
        {
            var results = _databaseContext.Commentss.Where(x => x.Comment == comment && x.ColorCommentId == colorCommentId && x.IsDelete == false).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public void Update(Comments model)
        {
            _databaseContext.Commentss.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }

        public void Delete(int id, int deleteBy, DateTime deleteDate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Comments> GetByDefaultColorId()
        {
            var colorDefault = _databaseContext.ColorComments.Where(x => x.IsDefault == true && x.IsDelete == false).SingleOrDefault();
            return _databaseContext.Commentss.Where(x => x.ColorCommentId == colorDefault.Id && x.IsDelete == false).ToList();
        }
    }
}
