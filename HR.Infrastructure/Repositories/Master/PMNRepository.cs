﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;


namespace HR.Infrastructure.Repositories
{
    public class PMNRepository : IPMNRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public PMNRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public PMN Get(int id)
        {
            return _databaseContext.PMNs.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<PMN> GetAll()
        {
            return _databaseContext.PMNs.Where(x => x.IsDelete == false).ToList();
        }
        public IEnumerable<PMN> GetAll(string keyword, int id)
        {
            IEnumerable<PMN> pmnDataList = _databaseContext.PMNs.Where(x => x.IsDelete == false).ToList();
            IList<PMN> result = new List<PMN>();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (pmnDataList.Count() > 0)
                {
                    foreach (var item in pmnDataList)
                    {
                        if (id == 1)
                        {
                            if (item.PMNToModalDasarCap.ToString().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 2)
                        {
                            if (item.RecourseDelay.ToString().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 3)
                        {
                            if (item.DelayYears.ToString().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 4)
                        {
                            if (item.OpexGrowth.ToString().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 5)
                        {
                            if (item.Opex.ToString().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 6)
                        {
                            if (item.ValuePMNToModalDasarCap.ToString().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                    }
                }
            }
            else
            {
                result = pmnDataList.ToList();
            }
            #endregion filter

            return result;
        }

        public void Insert(PMN model)
        {
            _databaseContext.PMNs.Add(model);
        }

        public bool IsExist(int id, int pmnToModalDasarCap)
        {
            var results = _databaseContext.PMNs.Where(x => x.PMNToModalDasarCap == pmnToModalDasarCap && x.IsDelete == false && x.Id != id).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public bool IsExist(int pmnToModalDasarCap)
        {
            var results = _databaseContext.PMNs.Where(x => x.PMNToModalDasarCap == pmnToModalDasarCap && x.IsDelete == false).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public void Update(PMN model)
        {
            _databaseContext.PMNs.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }

        public void Delete(int id, int deleteBy, DateTime deleteDate)
        {
            throw new NotImplementedException();
        }
    }
}
