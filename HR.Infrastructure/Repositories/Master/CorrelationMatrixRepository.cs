﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace HR.Infrastructure.Repositories
{
    public class CorrelationMatrixRepository : ICorrelationMatrixRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public CorrelationMatrixRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public CorrelationMatrix Get(int id)
        {
            return _databaseContext.CorrelationMatrixs.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<CorrelationMatrix> GetAll()
        {
            return _databaseContext.CorrelationMatrixs.Where(x => x.IsDelete == false).OrderByDescending(o => o.Nilai).ToList();
        }
        public IEnumerable<CorrelationMatrix> GetAll(string keyword, int id)
        {
            IEnumerable<CorrelationMatrix> correlationMatrixList = _databaseContext.CorrelationMatrixs.Where(x => x.IsDelete == false).ToList();
            IList<CorrelationMatrix> result = new List<CorrelationMatrix>();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (correlationMatrixList.Count() > 0)
                {
                    foreach (var item in correlationMatrixList)
                    {
                        if(id == 1)
                        {
                            if (item.NamaCorrelationMatrix.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 2)
                        {
                            if (item.Nilai.ToString().ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                    }
                }
            }
            else
            {
                result = correlationMatrixList.ToList();
            }
            #endregion filter

            return result;
        }

        public IEnumerable<CorrelationMatrix> GetAllAscNilai()
        {
            return _databaseContext.CorrelationMatrixs.Where(x => x.IsDelete == false).OrderBy(o => o.Nilai).ToList();
        }

        public void Insert(CorrelationMatrix model)
        {
            _databaseContext.CorrelationMatrixs.Add(model);
        }

        public bool IsExist(string namaCorrelationMatrix)
        {
            var results = _databaseContext.CorrelationMatrixs.Where(x => x.NamaCorrelationMatrix == namaCorrelationMatrix && x.IsDelete == false).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public bool IsExist(int id, string namaCorrelationMatrix)
        {
            var results = _databaseContext.CorrelationMatrixs.Where(x => x.NamaCorrelationMatrix == namaCorrelationMatrix && x.IsDelete == false && x.Id != id).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public void Update(CorrelationMatrix model)
        {
            _databaseContext.CorrelationMatrixs.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }
    }
}
