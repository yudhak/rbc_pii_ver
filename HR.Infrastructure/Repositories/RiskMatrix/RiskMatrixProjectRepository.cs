﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace HR.Infrastructure.Repositories
{
    public class RiskMatrixProjectRepository : IRiskMatrixProjectRepository
    {
        private readonly IDatabaseContext _databaseContext;

        public RiskMatrixProjectRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public RiskMatrixProject Get(int id)
        {
            return _databaseContext.RiskMatrixProjects.SingleOrDefault(x => x.Id == id);
        }

        public RiskMatrixProject GetByScenarioIdProjectId(int scenarioId, int projectId)
        {
            return _databaseContext.RiskMatrixProjects.SingleOrDefault(x => x.ScenarioId == scenarioId && x.ProjectId == projectId && x.IsDelete == false && x.StatusId == 2);
            //return _databaseContext.RiskMatrixProjects.SingleOrDefault(x => x.ScenarioId == scenarioId && x.ProjectId == projectId && x.IsDelete == false);
        }

        public RiskMatrixProject GetByScenarioIdProjectId2(int scenarioId, int projectId)
        {
            //return _databaseContext.RiskMatrixProjects.SingleOrDefault(x => x.ScenarioId == scenarioId && x.ProjectId == projectId && x.IsDelete == false && x.StatusId == 2);
            return _databaseContext.RiskMatrixProjects.FirstOrDefault(x => x.ScenarioId == scenarioId && x.ProjectId == projectId && x.IsDelete == false && x.StatusId != 4);
        }

        public RiskMatrixProject GetByScenarioIdProjectIdAll(int scenarioId, int projectId)
        {
            return _databaseContext.RiskMatrixProjects.SingleOrDefault(x => x.ScenarioId == scenarioId && x.ProjectId == projectId );
        }

        public IEnumerable<RiskMatrixProject> GetByScenarioIdProjectIdAllList(int scenarioId, int projectId)
        {
            return _databaseContext.RiskMatrixProjects.Where(x => x.ScenarioId == scenarioId && x.ProjectId == projectId).ToList();
        }

        public IEnumerable<RiskMatrixProject> GetByScenarioIdProjectIdList(int scenarioId, int projectId)
        {
            return _databaseContext.RiskMatrixProjects.Where(x => x.ScenarioId == scenarioId && x.ProjectId == projectId && x.IsDelete == false).ToList();
        }

        public RiskMatrixProject GetByProjectId(int projectId)
        {
            return _databaseContext.RiskMatrixProjects.SingleOrDefault(x => x.ProjectId == projectId && x.IsDelete == false);
        }

        public IEnumerable<RiskMatrixProject> GetAll()
        {
            return _databaseContext.RiskMatrixProjects.Where(x => x.IsDelete == false).ToList();
        }
        public IEnumerable<RiskMatrixProject> GetAll(string keyword, int id)
        {
            IEnumerable<RiskMatrixProject> riskMatrixProjectList = _databaseContext.RiskMatrixProjects.Where(x => x.IsDelete == false && x.Scenario.IsDelete == false).ToList();
            IList<RiskMatrixProject> result = new List<RiskMatrixProject>();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (riskMatrixProjectList.Count() > 0)
                {
                    foreach (var item in riskMatrixProjectList)
                    {                       
                        if (id == 1)
                        {
                            if (item.Scenario.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 2)
                        {
                            if (item.Project.NamaProject.ToLower().Contains(keyword.ToLower()))
                                result.Add(item);
                        }
                        if (id == 3)
                        {
                            if (item.Status != null)
                            {
                                if (item.Status.StatusDescription.ToLower().Contains(keyword.ToLower()))
                                    result.Add(item);
                            }
                            if (item.Status == null && "draft".Contains(keyword.ToLower()))
                            {
                                result.Add(item);

                            }

                        }
                    }
                }
            }
            else
            {
                result = riskMatrixProjectList.ToList();
            }
            #endregion filter

            return result;
        }

        public IEnumerable<RiskMatrixProject> GetAll(string keyword, int id, int id2)
        {
            IEnumerable<RiskMatrixProject> riskMatrixProjectList = _databaseContext.RiskMatrixProjects.Where(x => x.IsDelete == false && x.Scenario.IsDelete == false).ToList();
            IList<RiskMatrixProject> result = new List<RiskMatrixProject>();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (riskMatrixProjectList.Count() > 0)
                {
                    foreach (var item in riskMatrixProjectList)
                    {
                        if (id == 1)
                        {
                            if (id2 == 5 || (item.Status == null && id2 == 4 ))
                            {
                                if (item.Scenario.NamaScenario.ToLower().Contains(keyword.ToLower()))
                                    result.Add(item);
                            }
                            if (item.Status != null && id2 != 5)
                            {
                                if (item.Scenario.NamaScenario.ToLower().Contains(keyword.ToLower()) && item.StatusId == id2)
                                    result.Add(item);
                                //if (item.Status.StatusDescription.ToLower().Contains(keyword.ToLower()))

                            }
                        }

                        if (id == 2)
                        {
                            if (id2 == 5 || (item.Status == null && id2 == 4))
                            {
                                if (item.Project.NamaProject.ToLower().Contains(keyword.ToLower()))
                                    result.Add(item);
                            }
                            if (item.Status != null && id2 != 5)
                            {
                                if (item.Project.NamaProject.ToLower().Contains(keyword.ToLower()) && item.StatusId == id2)
                                    result.Add(item);
                                //if (item.Status.StatusDescription.ToLower().Contains(keyword.ToLower()))

                            }
                        }
                        //if (id == 3)
                        //{
                        //    if (item.Status != null)
                        //    {
                        //        if (item.Status.StatusDescription.ToLower().Contains(keyword.ToLower()))
                        //            result.Add(item);
                        //    }
                        //    if (item.Status == null && "draft".Contains(keyword.ToLower()))
                        //    {
                        //        result.Add(item);

                        //    }

                        //}
                    }
                }
            }
            else
            {
                if (id2 == 0)
                {
                    result = riskMatrixProjectList.ToList();
                }
                else
                {
                    foreach (var item in riskMatrixProjectList)
                    {
                        if (id2 == 5 || (item.Status == null && id2 == 4))
                        {
                            result.Add(item);
                        }
                        if (item.Status != null && id2 != 5)
                        {
                            if (item.StatusId == id2)
                                result.Add(item);
                            //if (item.Status.StatusDescription.ToLower().Contains(keyword.ToLower()))
                            //    result.Add(item);
                        }
                    }
                }
                
                // result = riskMatrixProjectList.ToList();
            }
            #endregion filter

            return result;
        }


        public IEnumerable<RiskMatrixProject> GetAllData()
        {
            return _databaseContext.RiskMatrixProjects.AsQueryable();
        }
        public IEnumerable<RiskMatrixProject> GetAllData(string keyword)
        {
            IEnumerable<RiskMatrixProject> riskMatrixProjectList = _databaseContext.RiskMatrixProjects.Where(x => x.IsDelete == false).ToList();
            IList<RiskMatrixProject> result = new List<RiskMatrixProject>();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (riskMatrixProjectList.Count() > 0)
                {
                    foreach (var item in riskMatrixProjectList)
                    {
                        if (item.Project.NamaProject.ToLower().Contains(keyword.ToLower()))
                            result.Add(item);
                    }
                }
            }
            else
            {
                result = riskMatrixProjectList.ToList();
            }
            #endregion filter

            return result;
        }

        public IEnumerable<RiskMatrixProject> GetByScenarioId(int scenarioId)
        {
            return _databaseContext.RiskMatrixProjects.Where(x => x.IsDelete == false && x.ScenarioId == scenarioId).ToList();
        }
        public IEnumerable<RiskMatrixProject> GetByScenarioIdAll(int scenarioId)
        {
            return _databaseContext.RiskMatrixProjects.Where(x => x.ScenarioId == scenarioId).ToList();
        }


        public IEnumerable<RiskMatrixProject> GetByProjectRiskMatrixScenarioId(int projectId, int riskMatrixId, int scenarioId)
        {
            return _databaseContext.RiskMatrixProjects.Where(x => x.IsDelete == false && x.ProjectId == projectId && x.RiskMatrixId == riskMatrixId && x.ScenarioId == scenarioId).ToList();
        }

        public IEnumerable<RiskMatrixProject> GetByProjectRiskMatrixScenarioId2(int projectId, int riskMatrixId, int scenarioId, int createBy)
        {
            return _databaseContext.RiskMatrixProjects.Where(x => x.IsDelete == false && x.ProjectId == projectId && x.RiskMatrixId == riskMatrixId && x.ScenarioId == scenarioId && x.CreateBy == createBy).ToList();
        }

        public IEnumerable<RiskMatrixProject> GetAllByScenarioId(int scenarioId)
        {
            return _databaseContext.RiskMatrixProjects.Where(x => x.ScenarioId == scenarioId).ToList();
        }

        public void Insert(RiskMatrixProject model)
        {
            _databaseContext.RiskMatrixProjects.Add(model);
        }

        public void Insert(IList<RiskMatrixProject> collections)
        {
            foreach (var item in collections)
            {
                this.Insert(item);
            }
        }

        public bool IsExist(int projectId)
        {
            var results = _databaseContext.RiskMatrixProjects.Where(x => x.ProjectId == projectId && x.IsDelete == false).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public bool IsExist(int id, int projectId)
        {
            var results = _databaseContext.RiskMatrixProjects.Where(x => x.ProjectId == projectId && x.IsDelete == false && x.Id != id).ToList();
            if (results.Count > 0)
                return true;

            return false;
        }

        public void Update(RiskMatrixProject model)
        {
            _databaseContext.RiskMatrixProjects.Attach(model);
            _databaseContext.Entry(model).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var result = _databaseContext.RiskMatrixProjects.Where(x => x.Id == id).FirstOrDefault();
            if (result != null)
            {
                _databaseContext.RiskMatrixProjects.Remove(result);
            }
            
        }

        public void DeleteByScenarioIdProjectId(int scenarioId,int projectId)
        {
            var result = _databaseContext.RiskMatrixProjects.Where(x => x.ScenarioId == scenarioId && x.ProjectId == projectId).FirstOrDefault();
            _databaseContext.RiskMatrixProjects.Remove(result);
        }

        public IEnumerable<RiskMatrixProject> GetAllByProjectIdNeedDelete(int projectId)
        {
            return _databaseContext.RiskMatrixProjects.Where(x => x.ProjectId == projectId).ToList();
        }

        public IEnumerable<RiskMatrixProject> GetAll(string keyword)
        {
            IEnumerable<RiskMatrixProject> riskMatrixProjectList = _databaseContext.RiskMatrixProjects.Where(x => x.IsDelete == false && x.Scenario.IsDefault == true && x.Project.IsActive == true).ToList();
            IList<RiskMatrixProject> result = new List<RiskMatrixProject>();

            #region filter
            if (!string.IsNullOrEmpty(keyword))
            {
                if (riskMatrixProjectList.Count() > 0)
                {
                    foreach (var item in riskMatrixProjectList)
                    {
                        if (item.Project.NamaProject.ToLower().Contains(keyword.ToLower()))
                            result.Add(item);
                    }
                }
            }
            else
            {
                result = riskMatrixProjectList.ToList();
            }
            #endregion filter

            return result;
        }
    }
}
