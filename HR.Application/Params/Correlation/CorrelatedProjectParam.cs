﻿using System;

namespace HR.Application.Params
{
    public class CorrelatedProjectParam
    {
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DeleteDate { get; set; }
        public int? StatusId { get; set; }

        public CorrelatedProjectParam() { }
    }

    public class CorrelatedProjectApprovalParam
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public int SektorId { get; set; }
        public string SektorName { get; set; }
    }
}
