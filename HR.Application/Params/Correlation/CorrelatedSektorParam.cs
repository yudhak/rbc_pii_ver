﻿using System.ComponentModel.DataAnnotations;
using System;

namespace HR.Application.Params
{
    public class CorrelatedSektorParam
    {
        public string NamaSektor{ get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DeleteDate { get; set; }
        public int? StatusId { get; set; }

        public CorrelatedSektorParam() { }
    }
}
