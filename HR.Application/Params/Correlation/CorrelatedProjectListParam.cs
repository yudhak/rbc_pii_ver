﻿using System.Collections.Generic;

namespace HR.Application.Params
{
    public class CorrelatedProjectListParameter : PaginationParam
    {
        public IList<GeneralCollectionParameter> Collection { get; set; }
        public IList<GeneralFilterParameter> Filter { get; set; }
        //public int userId { get; set; }
        public CorrelatedProjectListParameter()
        {
            this.Collection = new List<GeneralCollectionParameter>();
            this.Filter = new List<GeneralFilterParameter>();
        }
    }
}
