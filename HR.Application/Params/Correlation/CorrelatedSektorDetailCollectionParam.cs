﻿using System;
using System.Collections.Generic;

namespace HR.Application.Params
{
    public class CorrelatedSektorDetailCollectionParam
    {
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DeleteDate { get; set; }
        public int CorrelatedSektorId { get; set; }


        //Untuk mengecek update dari proses Approval atau bukan
        //Jika True maka update dilakukan dari Approval Service
        //Jika False maka update dilakukan pada Tabel Tersebut
        public bool? IsStatusApproval { get; set; }
        //Jika tombol kirim diklik
        public bool? IsSend { get; set; }
        public int? StatusId { get; set; }
        public int? NomorUrutStatus { get; set; }
        public string TemplateNotif { get; set; }
        public bool? IsDraftApproval { get; set; }
        public bool? IsUpdate { get; set; }
        public string Keterangan { get; set; }

        public CorrelatedSektorDetailCollection[] CorrelatedSektorDetailCollection { get; set; }

        public CorrelatedSektorDetailCollectionParam() { }
    }

    public class CorrelatedSektorDetailCollection
    {
        public int RiskRegistrasiId { get; set; }
        public RiskRegistrasiValues[] RiskRegistrasiValues { get; set; }

        public CorrelatedSektorDetailCollection() { }
    }

    public class RiskRegistrasiValues
    {
        public int RiskRegistrasiIdRow { get; set; }
        public int RiskRegistrasiIdCol { get; set; }
        public int CorrelationMatrixId { get; set; }

        public RiskRegistrasiValues() { }
    }
}
