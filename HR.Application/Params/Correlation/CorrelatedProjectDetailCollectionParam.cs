﻿using System;

namespace HR.Application.Params
{
    public class CorrelatedProjectDetailCollectionParam
    {
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DeleteDate { get; set; }
        public int CorrelatedProjectId { get; set; }
        public int ProjectId { get; set; }
        public string NamaProject { get; set; }
        public int SektorId { get; set; }
        public string NamaSektor { get; set; }
        public CorrelatedProjectDetailCollection[] CorrelatedProjectDetailCollection { get; set; }

        //Untuk mengecek update dari proses Approval atau bukan
        //Jika True maka update dilakukan dari Approval Service
        //Jika False maka update dilakukan pada Tabel Tersebut
        public bool? IsStatusApproval { get; set; }
        //Jika tombol kirim diklik
        public bool? IsSend { get; set; }
        public int? StatusId { get; set; }
        public int? NomorUrutStatus { get; set; }
        public bool? IsDraftApproval { get; set; }
        public bool? IsUpdate { get; set; }
        public string Keterangan { get; set; }

        public string TemplateNotif { get; set; }
        public CorrelatedProjectDetailCollectionParam() { }
    }

    public class CorrelatedProjectDetailCollection
    {
        public int ProjectId { get; set; }
        public CorrelatedProjectMatrixValues[] CorrelatedProjectMatrixValues { get; set; }

        public CorrelatedProjectDetailCollection() { }
    }

    public class CorrelatedProjectMatrixValues
    {
        public int ProjectIdRow { get; set; }
        public int ProjectIdCol { get; set; }
        public int CorrelationMatrixId { get; set; }

        public CorrelatedProjectMatrixValues() { }
    }
}
