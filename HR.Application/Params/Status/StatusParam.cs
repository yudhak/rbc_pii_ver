﻿//using System.ComponentModel.DataAnnotations;
using System;

namespace HR.Application.Params
{
    public class StatusParam 
    {
        public int Id { get; set; }
        public string StatusDescription { get; set; }
        public StatusParam() { }
    }
}
