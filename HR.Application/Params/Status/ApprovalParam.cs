﻿//using System.ComponentModel.DataAnnotations;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application.Params
{
    public class ApprovalParam
    {
        //Atribut Approval
        public int? StatusId { get; set; }
        public int? NomorUrutStatus { get; set; }
        public int? RequestId { get; set; }
        public string StatusApproval { get; set; }
        public string SourceApproval { get; set; }
        public string Keterangan { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DeleteDate { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsUpdateStatus { get; set; }
        public bool? IsUpdate { get; set; }
        public string TypePesan { get; set; }
        public string TemplateNotif { get; set; }
        public bool? IsSend { get; set; }
        public int? Id { get; set; }
        //Atribut notif email
        //public int? NomorUrutStatus { get; set; }
        //public Status MyProperty { get; set; }

        //Atribut StageTahunRiskMatrix
        public int? RiskMatrixProjectId { get; set; }
        public int? Tahun { get; set; }
        public DateTime? StartProject { get; set; }
        public DateTime? EndProject { get; set; }
        public IList<StageValue> StageValue { get; set; }

        //Atribut StageTahunRiskMatrixDetails
        public RiskMatrixCollection[] RiskMatrixCollection { get; set; }


        //Atribut Scenario
        public int? LikehoodId { get; set; }
        public string NamaScenario { get; set; }
        public string[] ProjectId { get; set; }
        public bool? IsDefault { get; set; }


        //Atribut Correlated Project Details
        public int? CorrelatedProjectId { get; set; }
        public int? ProjectIdCP { get; set; }
        public string NamaProject { get; set; }
        public int? SektorId { get; set; }
        public string NamaSektor { get; set; }
        public CorrelatedProjectDetailCollection[] CorrelatedProjectDetailCollection { get; set; }

        //Atribut Correlated Sektor Details
        public int? CorrelatedSektorId { get; set; }
        public CorrelatedSektorDetailCollection[] CorrelatedSektorDetailCollection { get; set; }
        

    }
}
