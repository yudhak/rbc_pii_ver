﻿using HR.Domain;
using System.Collections.Generic;

namespace HR.Application.Params
{
    public class MainDashboardResult
    {
        public int ScenarioId { get; set; }
        public string NamaScenario { get; set; }
        public AggregationInterDiversifiedProjectCapital[] AggregationInterDiversifiedProjectCapital { get; set; }
        public RiskCapitalByProject[] Project { get; set; } 
        public int[] Year { get; set; }
        public List<FunctionalRiskResult> FunctionalRiskResultTableResults { get; set; }
        public List<FunctionalRiskDashboard> FunctionalRiskMainDashboard { get; set; }
        public InterProjectDiversifiedAggregationSektor InterProjectDiversifiedAggregationSektor { get; set; }
        public AssetClassDefault[] AssetClassDefault { get; set; }

        public MainDashboardResult() { }
    }
    public class ReportMainDashboard
    {
        public int ScenarioId { get; set; }
        public string NamaScenario { get; set; }
        public AggregationInterDiversifiedProjectCapital[] AggregationInterDiversifiedProjectCapital { get; set; }
        public RiskCapitalByProject[] Project { get; set; }
        public int[] Year { get; set; }
        public List<FunctionalRiskResult> FunctionalRiskResultTableResults { get; set; }
        public List<FunctionalRiskDashboard> FunctionalRiskMainDashboard { get; set; }
        public InterProjectDiversifiedAggregationSektor InterProjectDiversifiedAggregationSektor { get; set; }
        public AssetClassDefault[] AssetClassDefault { get; set; }
        public Result Result { get; set; }

        public ReportMainDashboard() { }
    }


    public class RiskCapitalByProject
    {
        public int ProjectId { get; set; }
        public string NamaProject { get; set; }
        public RiskCapitalByProjectValue[] Values { get; set; }

        public RiskCapitalByProject() { }
    }

    public class RiskCapitalByProjectValue
    {
        public int ProjectId { get; set; }
        public int Year { get; set; }
        public decimal Value { get; set; }

        public RiskCapitalByProjectValue() { }
    }

    public class FunctionalRiskResult
    {
        public int MatrixId { get; set; }
        public string NamaMatrix { get; set; }
        public string NamaFormula { get; set; }
        public List<FunctionalRiskScenarioResult> Scenarios { get; set; }

        public FunctionalRiskResult() { }
    }

    public class FunctionalRiskScenarioResult
    {
        public string Scenario { get; set; }
        public string Calculations { get; set; }
        public string CollorComment { get; set; }
        public string Comment { get; set; }
        public FunctionalRiskScenarioResult() { }
    }

    public class FunctionalRiskDashboard
    {
        public int MatrixId { get; set; }
        public string NamaMatrix { get; set; }
        public string NamaFormula { get; set; }
        public List<FunctionalRiskColorDashboard> Colors { get; set; }
        public FunctionalRiskDashboard() { }
    }

    public class FunctionalRiskColorDashboard
    {
        public string Color { get; set; }
        public List<FunctionalRiskScenarioDashboard> Scenarios { get; set; }
        public FunctionalRiskColorDashboard() { }
    }

    public class FunctionalRiskScenarioDashboard
    {
        public string Scenario { get; set; }
        public string Definisi { get; set; }
       
        public FunctionalRiskScenarioDashboard() { }
    }
}
