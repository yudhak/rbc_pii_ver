﻿using HR.Domain;

namespace HR.Application.Params
{
    public class AggregationUndiversifiedProjectCapital
    {
        public int aggregationYears { get; set; }
        public decimal? TotalPerYear { get; set; }
        public decimal? TotalPerYearLower { get; set; }
        public decimal? TotalPerYearUpper { get; set; }
        public AggregationUndiversifiedProjectCollection[] AggregationUndiversifiedProjectCollection { get; set; }
        public AggregationUndiversifiedProjectCapital() { }
    }
    public class AggregationUndiversifiedProjectCollection
    {
        public int ProjectId { get; set; }
        public int SektorId { get; set; }
        public int Year { get; set; }
        public decimal? Total { get; set; }
        public AggregationUndiversifiedProjectCollection() { }
    }
}
