﻿//using System.ComponentModel.DataAnnotations;
using System;

namespace HR.Application.Params
{
    public class AvailableCapitalProjectedParam
    {
        public DataChanged ListChanged { get; set; }

        public AvailableCapitalProjectedParam() { }
    }
    public class DataChanged
    {
        public ListChanged[] DataCapital { get; set; }

        public ListChanged[] DataOpex { get; set; }

        public DataChanged() { }
    }

    public class ListChanged
    {
        public int Year { get; set; }
        public decimal? Value { get; set; }

        public ListChanged() { }
    }
}
