﻿using HR.Domain;

namespace HR.Application.Params
{
    public class AggregationIntraDiversifiedProjectCapital
    {
        public int aggregationYears { get; set; }
        public decimal? TotalPerYear { get; set; }
        public AggregationIntraDiversifiedProjectCollection[] AggregationIntraDiversifiedProjectCollection { get; set; }
        public AggregationIntraDiversifiedProjectCapital() { }
    }
    public class AggregationIntraDiversifiedProjectCollection
    {
        public int ProjectId { get; set; }
        public int SektorId { get; set; }
        public int Year { get; set; }
        public decimal? Total { get; set; }
        public decimal? TotalLower { get; set; }
        public decimal? TotalUpper { get; set; }
        public AggregationIntraDiversifiedProjectCollection() { }
    }
}
