﻿using HR.Domain;
using System.Collections.Generic;

namespace HR.Application.Params
{
    public class AggregationSektor
    {
        public SektorLite[] Sektor { get; set; }
        public UndiversifiedAggregationSektor UndiversifiedAggregationSektor { get; set; }
        public IntraProjectDiversifiedAggregationSektor IntraProjectDiversifiedAggregationSektor { get; set; }
        public InterProjectDiversifiedAggregationSektor InterProjectDiversifiedAggregationSektor { get; set; }

        public AggregationSektor() { }
    }

    public class UndiversifiedAggregationSektor
    {
        public YearCollection[] YearCollection { get; set; }


        public UndiversifiedAggregationSektor() { }
    }

    public class IntraProjectDiversifiedAggregationSektor
    {
        public SektorLite[] SektorIntraDiversified { get; set; }
        public YearCollection[] YearCollection { get; set; }


        public IntraProjectDiversifiedAggregationSektor() { }
    }

    public class InterProjectDiversifiedAggregationSektor
    {
        public SektorLite[] SektorInterDiversified { get; set; }
        public YearCollection[] YearCollection { get; set; }

        public InterProjectDiversifiedAggregationSektor() { }
    }

    public class YearCollection
    {
        public int Year { get; set; }
        public decimal Total { get; set; }
        public YearValues[] YearValues { get; set; }

        public YearCollection() { }
    }

    public class YearValues
    {
        public int SektorId { get; set; }
        public decimal Value { get; set; }

        public YearValues() { }
    }

    public class SektorLite
    {
        public int SektorId { get; set; }
        public string NamaSektor { get; set; }

        public SektorLite() { }
    }
}
