﻿using HR.Domain;
using System.Collections.Generic;

namespace HR.Application.Params
{
    public class AssetProjectionLiquidity
    {
        public AssetClassDefault[] AssetClassDefault { get; set; }
        public AssetSummaryDefault AssetSummaryDefault { get; set; }
        public AssetSummaryCollection[] AssetSummaryCollection { get; set; }
        public AssetClassCollection[] AssetClassProjectionCollection { get; set; }
        public AssetProjectionIlustration[] AssetProjectionIlustration { get; set; }
        public AssetClassCollection[] AssetClassLiquidityCollection { get; set; }
        public YearList[] LiquidAsset { get; set; }

        public AssetProjectionLiquidity() { }
    }

    public class AssetClassDefault
    {
        public string AssetClass { get; set; }
        public decimal Proportion { get; set; }
        public decimal TermAwal { get; set; }
        public decimal TermAkhir { get; set; }
        public decimal AssumedReturn { get; set; }
        public decimal OutstandingYearAwal { get; set; }
        public decimal OutstandingYearAkhir { get; set; }
        public decimal AssetValue { get; set; }

        public AssetClassDefault() { }
    }

    public class AssetSummaryDefault
    {
        public decimal AvailableCapitalProjected { get; set; }
        public decimal TotalUndiversifiedCapital { get; set; }
        public decimal TotalDiversifiedCapital { get; set; }
        public decimal AvailableCapitalProjectedBeforeOpex { get; set; }
        public decimal OperatingExpenses { get; set; }
        public decimal AvailableCapitalProjectedBeforeClaim { get; set; }

        public AssetSummaryDefault() { }
    }

    public class AssetSummaryCollection
    {
        public int Year { get; set; }
        public decimal AvailableCapitalProjected { get; set; }
        public decimal TotalUndiversifiedCapital { get; set; }
        public decimal TotalDiversifiedCapital { get; set; }
        public decimal AvailableCapitalProjectedBeforeOpex { get; set; }
        public decimal OperatingExpenses { get; set; }
        public decimal AvailableCapitalProjectedBeforeClaim { get; set; }

        public bool isEditedAvailableCapital { get; set; }
        public bool isEditedOpex { get; set; }

        public AssetSummaryCollection() { }
    }

    public class AssetProjectionIlustration
    {
        public int Year { get; set; }
        public decimal RecoursePayment { get; set; }
        public decimal AvailableCapitalRecourse { get; set; }

        public bool isEditedAvailableCapital { get; set; }

        public AssetProjectionIlustration() { }
    }

    public class AssetClassCollection
    {
        //public string AssetClass { get; set; }
        //public YearList[] YearList { get; set; }
        public int Year { get; set; }
        public AssetList[] AssetList { get; set; }
        public AssetClassCollection() { }
    }
    public class AssetList
    {
        //public int Year { get; set; }
        public string AssetClass { get; set; }
        public decimal Value { get; set; }

        public AssetList() { }
    }
    public class YearList
    {
        public int Year { get; set; }
        public decimal Value { get; set; }

        public YearList() { }
    }
}
