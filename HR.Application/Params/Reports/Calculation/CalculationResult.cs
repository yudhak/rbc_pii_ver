﻿using HR.Domain;
using System.Collections.Generic;

namespace HR.Application.Params
{
    public class CalculationResult
    {
        public int ScenarioId { get; set; }
        public string NamaScenario { get; set; }
        public AggregationOfProject AggregationOfProject { get; set; }
        public UndiversifiedRiskCapitalProjectCollection[] ProjectDetail { get; set; }
        public AggregationSektor AggregationSektor { get; set; }
        public AggregationRisk AggregationRisk { get; set; }
        public int[] CollectionYears { get; set; }
        public AssetProjectionLiquidity AssetProjectionLiquidity { get; set; }
        public int[] ProjectMonitoring { get; set; }
        public int[] SektorMonitoring { get; set; }
        public int[] RiskRegistrasiMonitoring { get; set; }


        public CalculationResult() { }
    }
    public class AggregationOfProject
    {
        public AggregationUndiversifiedProjectCapital[] AggregationUndiversifiedProjectCapital { get; set; }
        public AggregationOfProjectCollection[] AggregationOfProjectCollection { get; set; }
        public AggregationIntraDiversifiedProjectCapital[] AggregationIntraDiversifiedProjectCapital { get; set; }
        public AggregationInterDiversifiedProjectCapital[] AggregationInterDiversifiedProjectCapital { get; set; }
        public AggregationOfProject() { }
    }
    public class AggregationOfProjectCollection
    {
        public int ProjectId { get; set; }
        public string NamaProject { get; set; }
        public int SektorId { get; set; }
        public string NamaSektor { get; set; }
        public AggregationOfProjectCollection() { }
    }
    public class UndiversifiedRiskCapitalProjectCollection
    {
        public int ScenarioId { get; set; }
        public string NamaScenario { get; set; }
        public int ProjectId { get; set; }
        public string NamaProject { get; set; }
        public int SektorId { get; set; }
        public string NamaSektor { get; set; }
        public UndiversifiedYearCollection[] UndiversifiedYearCollection { get; set; }
        public IList<DiversifiedRiskCapitalCollection> DiversifiedRiskCapitalCollection { get; set; }
        public IList<CorrelatedSektorDetailLite> CorrelatedSektorDetail { get; set; }
        public RiskRegistrasi[] RiskRegistrasi { get; set; }

        public UndiversifiedRiskCapitalProjectCollection() { }
    }

    public class UndiversifiedYearCollection
    {
        public int Year { get; set; }
        public YearValue[] YearValue { get; set; }
        public decimal? Total { get; set; }
        public decimal? TotalLower { get; set; }
        public decimal? TotalUpper { get; set; }

        public UndiversifiedYearCollection() { }
    }

    public class YearValue
    {
        public int ScenarioId { get; set; }
        public int RiskRegistrasiId { get; set; }
        public int LikehoodId { get; set; }
        public decimal? ValueUndiversified { get; set; }
        public decimal? ValueUndiversifiedLower { get; set; }
        public decimal? ValueUndiversifiedUpper { get; set; }

        public YearValue() { }
    }
}
