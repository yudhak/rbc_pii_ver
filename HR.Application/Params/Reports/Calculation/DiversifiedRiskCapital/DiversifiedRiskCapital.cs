﻿using HR.Domain;

namespace HR.Application.Params
{
    public class DiversifiedRiskCapital
    {
        public DiversifiedRiskCapitalCollection[] DiversifiedRiskCapitalCollection { get; set; }

        public DiversifiedRiskCapital() { }
    }

    public class DiversifiedRiskCapitalCollection
    {
        public int Year { get; set; }
        public decimal? Total { get; set; }
        public decimal? TotalLower { get; set; }
        public decimal? TotalUpper { get; set; }
        public RiskValueCollection[] RiskValueCollection { get; set; }

        public DiversifiedRiskCapitalCollection() { }
    }

    public class RiskValueCollection
    {
        public int RiskRegistrasiId { get; set; }
        public decimal? Value { get; set; }
        public decimal? ValueLower { get; set; }
        public decimal? ValueUpper { get; set; }

        public RiskValueCollection() { }
    }

    public class YearTotalDiversified
    {
        public int Year { get; set; }
        public decimal? Total { get; set; }
        public decimal? TotalLower { get; set; }
        public decimal? TotalUpper { get; set; }

        public YearTotalDiversified() { }
    }
}
