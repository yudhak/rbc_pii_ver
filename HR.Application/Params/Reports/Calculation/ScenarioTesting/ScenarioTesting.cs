﻿using HR.Domain;

namespace HR.Application.Params
{
    public class ScenarioTesting
    {
        public ScenarioTestingCollection[] ScenarioTestingCollection { get; set; }
        public Sensitivity Sensitivity { get; set; }
        public StressTesting StressTesting { get; set; }
        public int[] Year { get; set; }

        public ScenarioTesting() { }
    }

    public class ScenarioTestingCollection
    {
        public int ScenarioId { get; set; }
        public string NamaScenario { get; set; }
        public ScenarioTestingYearCollection[] ScenarioTestingYearCollection { get; set; }

        public ScenarioTestingCollection() { }
    }
    public class ScenarioTestingYearCollection
    {
        public int Year { get; set; }
        public decimal? TotalUndiversifiedCapital { get; set; }
        public decimal? TotalDiversifiedCapital { get; set; }

        public ScenarioTestingYearCollection() { }
    }

    public class Sensitivity
    {
        public Undiversified[] Undiversified { get; set; }
        public Diversified[] Diversified { get; set; }
        public Sensitivity() { }
    }
    public class Undiversified
    {
        public int ScenarioId { get; set; }
        public string NamaScenario { get; set; }
        public ValuePerYear[] UpperValue { get; set; }
        public ValuePerYear[] LowerValue { get; set; }
        public ValuePerYear[] MidValue { get; set; }
        public Undiversified() { }
    }
    public class Diversified
    {
        public int ScenarioId { get; set; }
        public string NamaScenario { get; set; }
        public ValuePerYear[] UpperValue { get; set; }
        public ValuePerYear[] LowerValue { get; set; }
        public ValuePerYear[] MidValue { get; set; }
        public Diversified() { }
    }

    public class ValuePerYear
    {
        public int Year { get; set; }
        public decimal? Value { get; set; }
        public ValuePerYear() { }
    }
    public class StressTesting
    {
        public RiskCapital[] RiskCapital { get; set; }
        public AvailableCapital[] AvailableCapital { get; set; }
        public TotalLiquidity[] TotalLiquidity { get; set; }

        public StressTesting() { }
    }
    public class RiskCapital
    {
        public int ScenarioId { get; set; }
        public string NamaScenario { get; set; }
        public ValuePerYear[] ValuePerYear { get; set; }
        public RiskCapital() { }
    }
    public class AvailableCapital
    {
        public int ScenarioId { get; set; }
        public string NamaScenario { get; set; }
        public ValuePerYear[] ValuePerYear { get; set; }
        public AvailableCapital() { }
    }
    public class TotalLiquidity
    {
        public int ScenarioId { get; set; }
        public string NamaScenario { get; set; }
        public ValuePerYear[] ValuePerYear { get; set; }
        public TotalLiquidity() { }
    }
}