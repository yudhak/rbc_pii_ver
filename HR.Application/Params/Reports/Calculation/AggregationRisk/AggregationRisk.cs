﻿using HR.Domain;
using System.Collections.Generic;

namespace HR.Application.Params
{
    public class AggregationRisk
    {
        //public RiskRegistrasiLite[] RiskRegistrasi{ get; set; }
        public RiskRegistrasi[] RiskRegistrasi { get; set; }
        public UndiversifiedAggregationRisk UndiversifiedAggregationRisk { get; set; }
        public IntraProjectDiversifiedAggregationRisk IntraProjectDiversifiedAggregationRisk { get; set; }
        public InterProjectDiversifiedAggregationRisk InterProjectDiversifiedAggregationRisk { get; set; }

        public AggregationRisk() { }
    }

    public class UndiversifiedAggregationRisk
    {
        public RiskYearCollection[] RiskYearCollection { get; set; }

        public UndiversifiedAggregationRisk() { }
    }

    public class IntraProjectDiversifiedAggregationRisk
    {
        public RiskYearCollection[] RiskYearCollection { get; set; }

        public IntraProjectDiversifiedAggregationRisk() { }
    }

    public class InterProjectDiversifiedAggregationRisk
    {
        public RiskYearCollection[] RiskYearCollection { get; set; }

        public InterProjectDiversifiedAggregationRisk() { }
    }

    public class RiskYearCollection
    {
        public int Year { get; set; }
        public decimal? Total { get; set; }
        public RiskYearValue[] RiskYearValue { get; set; }

        public RiskYearCollection() { }
    }

    public class RiskYearValue
    {
        public int RiskRegistrasiId { get; set; }
        public decimal? Value { get; set; }

        public RiskYearValue() { }
    }
}
