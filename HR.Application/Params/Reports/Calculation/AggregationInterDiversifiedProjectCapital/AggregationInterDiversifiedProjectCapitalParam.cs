﻿using HR.Domain;

namespace HR.Application.Params
{
    public class AggregationInterDiversifiedProjectCapital
    {
        public int aggregationYears { get; set; }
        public decimal? TotalPerYear { get; set; }
        public decimal? TotalPerYearLower { get; set; }
        public decimal? TotalPerYearUpper { get; set; }
        public AggregationInterDiversifiedProjectCollection[] AggregationInterDiversifiedProjectCollection { get; set; }
        public AggregationInterDiversifiedProjectCapital() { }
    }
    public class AggregationInterDiversifiedProjectCollection
    {
        public int ProjectId { get; set; }
        public int SektorId { get; set; }
        public int Year { get; set; }
        public decimal? Total { get; set; }
        public AggregationInterDiversifiedProjectCollection() { }
    }

    public class ProjectValueCollection
    {
        public int ProjectId { get; set; }
        public decimal? Value { get; set; }

        public ProjectValueCollection() { }
    }

    public class YearTotalAggregationInterDiversified
    {
        public int Year { get; set; }
        public decimal? Total { get; set; }
        public decimal? TotalLower { get; set; }
        public decimal? TotalUpper { get; set; }

        public YearTotalAggregationInterDiversified() { }
    }
}
