﻿using HR.Domain;
using System.Collections.Generic;

namespace HR.Application.Params
{
    public class Calculation
    {
        public CalculationResult CalculationResult { get; set; }
        public ScenarioTesting ScenarioTesting { get; set; }

        public Calculation() { }
    }
}