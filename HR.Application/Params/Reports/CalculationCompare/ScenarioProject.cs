﻿using HR.Domain;
using System.Collections.Generic;

namespace HR.Application.Params
{
    public class ScenarioCollection
    {
        public ScenarioProject[] ScenarioProject { get; set; }

        public ScenarioCollection() { }
    }

    public class ScenarioProject
    {
        public int ScenarioId { get; set; }
        public int [] ProjectId { get; set; }

        public ScenarioProject() { }
    }

    public class ScenarioProjectDetail
    {
        public int ScenarioId { get; set; }
        public Project[] ProjectId { get; set; }

        public ScenarioProjectDetail() { }
    }
}