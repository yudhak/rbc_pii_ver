﻿using HR.Domain;
using System.Collections.Generic;

namespace HR.Application.Params
{
    public class SensitivityStressTesting
    {
        public SensitivityStress SensitivityResult { get; set; }
        public StressTestingResult StressTestingResult { get; set; }

        public SensitivityStressTesting() { }
    }
    public class SensitivityStress
    {
        public SensitivityStressCollection[] SensitivityStressCollection { get; set; }
        public SensitivityStressScenarioCollection[] SensitivityStressScenarioCollection { get; set; }
        public SensitivityStressYearCollection[] SensitivityStressYearCollection { get; set; }
        public SensitivityStress() { }
    }
    public class StressTestingResult
    {
        public SensitivityStressCollection[] SensitivityStressCollection { get; set; }
        public SensitivityStressScenarioCollection[] RiskCapitalCollection { get; set; }
        public SensitivityStressScenarioCollection[] AvailableCapitalCollection { get; set; }
        public SensitivityStressYearCollection[] SensitivityStressYearCollection { get; set; }
        public StressTestingResult() { }
    }
    public class SensitivityStressCollection
    {
        public string Name { get; set; }
        public SensitivityStressItem[] Values { get; set; }

        public SensitivityStressCollection() { }
    }
    public class SensitivityStressScenarioCollection
    {
        public string Name { get; set; }
        public string NamaScenario { get; set; }
        public int ScenarioId { get; set; }

        public SensitivityStressItem[] Values { get; set; }

        public SensitivityStressScenarioCollection() { }
    }
    public class SensitivityStressItem
    {
        public string Name { get; set; }
        public int Year { get; set; }
        public decimal Value { get; set; }

        public SensitivityStressItem() { }
    }
    public class SensitivityStressYearCollection
    {
        public int Year { get; set; }

        public SensitivityStressYearCollection() { }
    }
}

