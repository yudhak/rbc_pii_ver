﻿using HR.Domain;
using System.Collections.Generic;

namespace HR.Application.Params
{
    public class DiversifiedBenefitAchieved
    {
        public SektorLite[] Sektor { get; set; }
        public DiversifiedBenefitAchievedBySektor DiversifiedBenefitAchievedBySektor { get; set; }
        public DiversifiedBenefitAchievedByProject DiversifiedBenefitAchievedByProject { get; set; }
        public DiversifiedBenefitAchievedCollection DiversifiedBenefitAchievedCollection { get; set; }

        public DiversifiedBenefitAchieved() { }
    }

    public class DiversifiedBenefitAchievedBySektor
    {
        public SektorLite[] SektorDiversifiedBenefitAchieved { get; set; }
        public YearSektorCollection[] YearSektorCollection { get; set; }

        public DiversifiedBenefitAchievedBySektor() { }
    }

    public class DiversifiedBenefitAchievedByProject
    {
        public AggregationOfProjectCollection[] ProjectDiversifiedBenefitAchieved { get; set; }
        public YearProjectCollection[] YearProjectCollection { get; set; }

        public DiversifiedBenefitAchievedByProject() { }
    }
    public class DiversifiedBenefitAchievedCollection
    {
        public UndiversifiedBenefit[] UndiversifiedBenefitAchieved { get; set; }
        public DiversifiedBenefit[] DiversifiedBenefitAchieved { get; set; }
        public InterProjectBenefit[] InterProjectBenefit { get; set; }
        public DiversifiedBenefitAchievedCollection() { }
    }
    public class UndiversifiedBenefit
    {
        public int Year { get; set; }
        public decimal? Total { get; set; }
        public UndiversifiedBenefit() { }
    }
    public class DiversifiedBenefit
    {
        public int Year { get; set; }
        public decimal? Total { get; set; }
        public DiversifiedBenefit() { }
    }
    public class InterProjectBenefit
    {
        public int Year { get; set; }
        public decimal? Total { get; set; }
        public InterProjectBenefit() { }
    }

    public class YearSektorCollection
    {
        public int Year { get; set; }
        public decimal Total { get; set; }
        public YearSektorValue[] YearSektorValue { get; set; }

        public YearSektorCollection() { }
    }

    public class YearProjectCollection
    {
        public int Year { get; set; }
        public decimal? Total { get; set; }
        public YearProjectValue[] YearProjectValue { get; set; }

        public YearProjectCollection() { }
    }

    public class YearSektorValue
    {
        public int SektorId { get; set; }
        public string NamaSektor { get; set; }
        public decimal Value { get; set; }

        public YearSektorValue() { }
    }

    public class YearProjectValue
    {
        public int ProjectId { get; set; }
        public decimal? Value { get; set; }

        public YearProjectValue() { }
    }

    public class ProjectLite
    {
        public int ProjectId { get; set; }
        public string NamaProject { get; set; }

        public ProjectLite() { }
    }
}
