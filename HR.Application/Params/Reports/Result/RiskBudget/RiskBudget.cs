﻿using HR.Domain;
using System.Collections.Generic;

namespace HR.Application.Params
{
    public class RiskBudget
    {
        public RiskBudgetByProject RiskBudgetByProject { get; set; }
        public RiskBudgetBySektor RiskBudgetBySektor { get; set; }
        public RiskBudgetByRisk RiskBudgetByRisk { get; set; }

        public RiskBudget() { }
    }

    #region ByRisk
    public class RiskBudgetByRisk
    {
        public RiskBudgetCollectionByRisk[] RiskBudgetCollectionByRisk { get; set; }
        public RiskBudgetYearCollection[] YearCollection { get; set; }
        public RiskRegistrasiLite[] RiskRegistrasiCollection { get; set; }

        public RiskBudgetByRisk() { }
    }

    public class RiskBudgetCollectionByRisk
    {
        public string Name { get; set; }
        public RiskBudgetItemByRisk[] Values { get; set; }

        public RiskBudgetCollectionByRisk() { }
    }

    public class RiskBudgetItemByRisk
    {
        public int RiskRegistrasiId { get; set; }
        public string KodeRisk { get; set; }
        public int Year { get; set; }
        public decimal Value { get; set; }

        public RiskBudgetItemByRisk() { }
    }

    #endregion ByRisk

    #region ByProject
    public class RiskBudgetByProject
    {
        public RiskBudgetCollection[] RiskBudgetCollection { get; set; }
        public RiskBudgetYearCollection[] RiskBudgetYearCollection { get; set; }
        public ProjectLite[] ProjectCollection { get; set; }

        public RiskBudgetByProject() { }
    }

    public class RiskBudgetCollection
    {
        public string Name { get; set; }
        public RiskBudgetItem[] Values { get; set; }

        public RiskBudgetCollection() { }
    }

    public class RiskBudgetItem
    {
        public int ProjectId { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public decimal Value { get; set; }

        public RiskBudgetItem() { }
    }
    #endregion ByProject

    #region BySektor
    public class RiskBudgetBySektor
    {
        public RiskBudgetCollectionBySektor[] RiskBudgetCollectionBySektor { get; set; }
        public RiskBudgetYearCollection[] YearCollection { get; set; }
        public SektorLite[] SektorCollection { get; set; }

        public RiskBudgetBySektor() { }
    }

    public class RiskBudgetCollectionBySektor
    {
        public string Name { get; set; }
        public RiskBudgetItemBySektor[] Values { get; set; }

        public RiskBudgetCollectionBySektor() { }
    }

    public class RiskBudgetItemBySektor
    {
        public int SektorId { get; set; }
        public string NamaSektor { get; set; }
        public int Year { get; set; }
        public decimal Value { get; set; }

        public RiskBudgetItemBySektor() { }
    }
    #endregion BySektor
    public class RiskBudgetYearCollection
    {
        public int Year { get; set; }
        public RiskBudgetYearCollection() { }
    }
}
