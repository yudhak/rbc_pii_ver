﻿using HR.Domain;
using System.Collections.Generic;

namespace HR.Application.Params
{
    public class LeverageRatio
    {
        public LeverageItem[] LeverageItem { get; set; }
        public YearCollectionLeverage[] YearCollectionLeverage { get; set; }

        public LeverageRatio() { }
    }

    public class LeverageItem
    {
        public string Name { get; set; }
        public ItemValue[] Values { get; set; }

        public LeverageItem() { }
    }

    public class YearCollectionLeverage
    {
        public int Year { get; set; }

        public YearCollectionLeverage() { }
    }

    public class ItemValue
    {
        public string Name { get; set; }
        public int Year { get; set; }
        public decimal Value { get; set; }

        public ItemValue() { }
    }
}
