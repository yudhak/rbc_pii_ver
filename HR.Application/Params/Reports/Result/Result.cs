﻿using HR.Domain;
using System.Collections.Generic;

namespace HR.Application.Params
{
    public class Result
    {
        public int ScenarioId { get; set; }
        public string NamaScenario { get; set; }
        public DiversifiedBenefitAchieved DiversifiedBenefitAchieved { get; set; }
        public LeverageRatio LeverageRatio { get; set; }
        public AvailableVSRiskCapital AvailableVSRiskCapital { get; set; }
        public Liquidity Liquidity { get; set; }
        public SensitivityStressTesting SensitivityStressTesting { get; set; }
        public RiskBudget RiskBudget { get; set; }
        public int[] ProjectMonitoring { get; set; }
        public int[] SektorMonitoring { get; set; }



        public Result() { }
    }
}
