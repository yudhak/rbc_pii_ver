﻿using HR.Domain;
using System.Collections.Generic;

namespace HR.Application.Params
{
    public class AvailableVSRiskCapital
    {
        public AvailableVSRiskCapitalCollection[] AvailableVSRiskCapitalCollection { get; set; }
        public AvailableVSRiskCapitalYearCollection[] AvailableVSRiskCapitalYearCollection { get; set; }

        public AvailableVSRiskCapital() { }
    }

    public class AvailableVSRiskCapitalCollection
    {
        public string Name { get; set; }
        public AvailableVSRiskCapitalItem[] Values { get; set; }

        public AvailableVSRiskCapitalCollection() { }
    }
    public class AvailableVSRiskCapitalItem
    {
        public string Name { get; set; }
        public int Year { get; set; }
        public decimal Value { get; set; }

        public AvailableVSRiskCapitalItem() { }
    }

    public class AvailableVSRiskCapitalYearCollection
    {
        public int Year { get; set; }

        public AvailableVSRiskCapitalYearCollection() { }
    }
}
