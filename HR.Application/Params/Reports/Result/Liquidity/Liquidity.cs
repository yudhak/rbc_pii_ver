﻿using HR.Domain;
using System.Collections.Generic;

namespace HR.Application.Params
{
    public class Liquidity
    {
        public LiquidityCollection[] LiquidityCollection { get; set; }
        public LiquidityYearCollection[] LiquidityYearCollection { get; set; }

        public Liquidity() { }
    }

    public class LiquidityCollection
    {
        public string Name { get; set; }
        public LiquidityItem[] Values { get; set; }

        public LiquidityCollection() { }
    }
    public class LiquidityItem
    {
        public string Name { get; set; }
        public int Year { get; set; }
        public decimal Value { get; set; }

        public LiquidityItem() { }
    }

    public class LiquidityYearCollection
    {
        public int Year { get; set; }

        public LiquidityYearCollection() { }
    }
}
