﻿using System;
using System.Collections.Generic;

namespace HR.Application.Params
{
    public class MasterMenuApprovalParam
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? MenuId { get; set; }

        public MasterMenuApprovalParam() { }
    }
}
