﻿using HR.Application.DTO;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application.Params
{
    public class RiskMatrixCollectionParam
    {
        //untuk target//
        public int Id { get; set; }
        public int StageId { get; set; }
        //public int RiskMatrixProjectId { get; set; }
        public int? Tahun { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public bool? IsUpdate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DeleteDate { get; set; }

        //public RiskMatrixProjectDTO RiskMatrixProject { get; set; }
        public Stage Stage { get; set; }

        public RiskMatrixCollection[] RiskMatrixCollection { get; set; }
        //untuk target yg disimpan

        //untuk sumber
        public int StageTahunRiskMatrixId { get; set; } //Tahun
        public decimal? MaximumNilaiExpose { get; set; }
        public IList<RiskMatrixValue> RiskMatrixValue { get; set; }

        public RiskMatrixCollectionParam() { }
    }

    //public class RiskMatrixCollection
    //{
    //    public int StageTahunRiskMatrixId { get; set; } //Tahun
    //    public decimal? MaximumNilaiExpose { get; set; }
    //    public IList<RiskMatrixValue> RiskMatrixValue { get; set; }

    //    public RiskMatrixCollection() { }
    //}

    //public class RiskMatrixValue
    //{
    //    public int RiskRegistrasiId { get; set; }
    //    public decimal?[] Values { get; set; } // index[0] = Exposure Value, index[1] = LikelihoodDetailId

    //    public RiskMatrixValue() { }
    //}
}
