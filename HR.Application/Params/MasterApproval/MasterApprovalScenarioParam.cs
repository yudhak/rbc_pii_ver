﻿using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;

namespace HR.Application.Params
{
    public class MasterApprovalScenarioParam
    {
        public int? MenuId { get; set; }
        public int? UserId { get; set; }
        public int?[] UserIdList { get; set; }
        public int? NomorUrutStatus { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DeleteDate { get; set; }

        public MasterApprovalScenarioParam() { }
    }

    public class MasterApprovalScenarioDetailListParam
    {
        public IList<MasterApprovalScenarioParam> MasterApprovalScenarioParams { get; set; }

        public MasterApprovalScenarioDetailListParam() { }
    }
}
