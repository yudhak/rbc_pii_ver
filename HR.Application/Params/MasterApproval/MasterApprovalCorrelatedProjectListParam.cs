﻿using System.Collections.Generic;

namespace HR.Application.Params
{
    public class MasterApprovalCorrelatedProjectListParam : PaginationParam
    {
        public IList<GeneralCollectionParameter> Collection { get; set; }
        public IList<GeneralFilterParameter> Filter { get; set; }

        public MasterApprovalCorrelatedProjectListParam()
        {
            this.Collection = new List<GeneralCollectionParameter>();
            this.Filter = new List<GeneralFilterParameter>();
        }
    }
}
