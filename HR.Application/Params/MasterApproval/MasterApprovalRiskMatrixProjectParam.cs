﻿using System.ComponentModel.DataAnnotations;
using System;

namespace HR.Application.Params
{
    public class MasterApprovalRiskMatrixProjectParam
    {
        public int? MenuId { get; set; }
        public int? UserId { get; set; }
        public int?[] UserIdList { get; set; }
        public int? ProjectId { get; set; }
        public int? NomorUrutStatus { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DeleteDate { get; set; }

        public MasterApprovalRiskMatrixProjectParam() { }
    }
}
