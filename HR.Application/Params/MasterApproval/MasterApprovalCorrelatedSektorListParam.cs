﻿using System.Collections.Generic;

namespace HR.Application.Params
{
    public class MasterApprovalCorrelatedSektorListParam : PaginationParam
    {
        public IList<GeneralCollectionParameter> Collection { get; set; }
        public IList<GeneralFilterParameter> Filter { get; set; }

        public MasterApprovalCorrelatedSektorListParam()
        {
            this.Collection = new List<GeneralCollectionParameter>();
            this.Filter = new List<GeneralFilterParameter>();
        }
    }
}
