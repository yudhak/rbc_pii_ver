﻿using System.Collections.Generic;

namespace HR.Application.Params
{
    public class MasterApprovalRiskMatrixProjectListParam : PaginationParam
    {
        public IList<GeneralCollectionParameter> Collection { get; set; }
        public IList<GeneralFilterParameter> Filter { get; set; }

        public MasterApprovalRiskMatrixProjectListParam()
        {
            this.Collection = new List<GeneralCollectionParameter>();
            this.Filter = new List<GeneralFilterParameter>();
        }
    }
}
