﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Application.Params
{
    public class GeneralCollectionParameter
    {
        public string Field { get; set; }
        public IList<string> Values { get; set; }
        public GeneralCollectionParameter()
        {

        }
    }
}
