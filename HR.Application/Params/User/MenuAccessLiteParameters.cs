﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Application.Params
{
    public class MenuAccessLiteParameters
    {
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
    }
}
