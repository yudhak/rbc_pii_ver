﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Application.Params
{
    public class RoleAccessFrontParam
    {
        public int MenuId { get; set; }
        public int RoleId { get; set; }
        public bool? Tambah { get; set; }
        public bool? Ubah { get; set; }
        public bool? Hapus { get; set; }
        public bool? Lihat { get; set; }
        public bool? Detail { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public bool? IsActive { get; set; }
        public RoleAccessFrontParam() { }
    }
}
