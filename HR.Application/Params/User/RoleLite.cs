﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Application.Params
{
    public class RoleLite
    {
        public string Name { get; set; }
        public string Action { get; set; }

        public RoleLite() { }
    }
}
