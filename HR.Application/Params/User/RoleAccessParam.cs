﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Application.Params
{
    public class RoleAccessParam
    {
        public int MenuId { get; set; }
        public int RoleId { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsActive { get; set; }
        public int? FlagFromFront { get; set; }
        public string[] Access { get; set; }
        public RoleAccessParam() { }
    }
}
