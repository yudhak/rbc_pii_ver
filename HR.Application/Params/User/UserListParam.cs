﻿using System.Collections.Generic;

namespace HR.Application.Params
{
    public class UserListParameter : PaginationParam
    {
        public IList<GeneralCollectionParameter> Collection { get; set; }
        public IList<GeneralFilterParameter> Filter { get; set; }

        public bool userFilter { get; set; }

        public int userFilterRole { get; set; }
        //public bool userFilter { get; set; }
        //public bool userFilter { get; set; }

        public UserListParameter()
        {
            this.Collection = new List<GeneralCollectionParameter>();
            this.Filter = new List<GeneralFilterParameter>();
        }
    }
}
