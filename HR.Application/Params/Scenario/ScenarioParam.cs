﻿using System.ComponentModel.DataAnnotations;
using System;

namespace HR.Application.Params
{
    public class ScenarioParam
    {
        [Required]
        public string NamaScenario { get; set; }
        public int LikehoodId { get; set; }
        public string[] ProjectId { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DeleteDate { get; set; }
        //public bool? Status { get; set; }
        public int? StatusId { get; set; }
        public bool? IsDefault { get; set; }
        public bool? IsUpdate { get; set; }
        public bool? IsScenarioDefault { get; set; }
        public string TemplateNotif { get; set; }
        public string Keterangan { get; set; }

        //Untuk mengecek update dari proses Approval atau bukan
        //Jika True maka update dilakukan dari Approval Service
        //Jika False maka update dilakukan pada Tabel Tersebut
        public bool? IsStatusApproval { get; set; }
        //Jika tombol kirim diklik
        public bool? IsSend { get; set; }
        public string TypePesan { get; set; }
        public bool? IsDraftApproval { get; set; }
        public int? NomorUrutStatus { get; set; }
        public int? IdScenarioLama { get; set; }

        public ScenarioParam() { }
    }
}
