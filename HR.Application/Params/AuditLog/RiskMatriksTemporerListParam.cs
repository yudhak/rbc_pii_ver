﻿using System.Collections.Generic;

namespace HR.Application.Params
{
    public class RiskMatriksTemporerListParam : PaginationParam
    {
        public IList<GeneralCollectionParameter> Collection { get; set; }
        public IList<GeneralFilterParameter> Filter { get; set; }

        public RiskMatriksTemporerListParam()
        {
            this.Collection = new List<GeneralCollectionParameter>();
            this.Filter = new List<GeneralFilterParameter>();
        }
    }
}
