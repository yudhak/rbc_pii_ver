﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HR.Application.Params
{
    public class AuditLogParam2
    {
        public int Id { get; set; }
        public string MenuId { get; set; }
        public string TableModified { get; set; }
        public string DataObjekId { get; set; }
        public string ColumnModified { get; set; }
        public string DataAwal { get; set; }
        public string DataAkhir { get; set; }        
        public string LogTimestamp { get; set; }
        public string ModifiedBy { get; set; }
        public string Aksi { get; set; }

        public AuditLogParam2() { }
    }
}
