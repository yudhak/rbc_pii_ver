﻿using System.Collections.Generic;

namespace HR.Application.Params
{
    public class CorrelatedSektorTemporerListParam : PaginationParam
    {
        public IList<GeneralCollectionParameter> Collection { get; set; }
        public IList<GeneralFilterParameter> Filter { get; set; }

        public CorrelatedSektorTemporerListParam()
        {
            this.Collection = new List<GeneralCollectionParameter>();
            this.Filter = new List<GeneralFilterParameter>();
        }
    }
}
