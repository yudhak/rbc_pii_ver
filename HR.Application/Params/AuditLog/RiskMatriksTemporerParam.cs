﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HR.Application.Params
{
    public class RiskMatriksTemporerParam
    {
        public int? MenuId { get; set; }
        public string TableModified { get; set; }
        public int? DataObjekId { get; set; }
        public string ColumnModified { get; set; }
        public string DataAwal { get; set; }
        public string DataAkhir { get; set; }        
        public DateTime LogTimestamp { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime DeleteDate { get; set; }
        public int? DeleteBy { get; set; }

        public RiskMatriksTemporerParam() { }
    }
}
