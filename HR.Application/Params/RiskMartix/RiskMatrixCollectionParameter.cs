﻿using System;
using System.Collections.Generic;

namespace HR.Application.Params
{
    public class RiskMatrixCollectionParameter
    {
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DeleteDate { get; set; }
        public RiskMatrixCollection[] RiskMatrixCollection { get; set; }

        //Untuk mengecek update dari proses Approval atau bukan
        //Jika True maka update dilakukan dari Approval Service
        //Jika False maka update dilakukan pada Tabel Tersebut
        public bool? IsStatusApproval { get; set; }
        //Jika tombol kirim diklik
        public bool? IsSend { get; set; }
        public bool? IsDraftApproval { get; set; }
        public bool? IsUpdate { get; set; }
        public int ? StatusId { get; set; }
        public int? NomorUrutStatus { get; set; }
        public string Keterangan { get; set; }
        public string TemplateNotif { get; set; }

        public RiskMatrixCollectionParameter() { }
    }

    public class RiskMatrixCollection
    {
        public bool Warna { get; set; }
        public int StageTahunRiskMatrixId { get; set; } //Tahun
        public decimal? MaximumNilaiExpose { get; set; }
        public IList<RiskMatrixValue> RiskMatrixValue { get; set; }

        public RiskMatrixCollection() { }
    }

    public class RiskMatrixValue
    {
        public int RiskRegistrasiId { get; set; }
        public decimal?[] Values { get; set; } // index[0] = Exposure Value, index[1] = LikelihoodDetailId
        public bool?[] Colors { get; set; }
        public RiskMatrixValue() { }
    }
}
