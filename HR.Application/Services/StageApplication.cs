﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;
using HR.Infrastructure;

namespace HR.Application
{
    public class StageService : IStageService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IStageRepository _stageRepository;
        private readonly IAuditLogService _auditLogService;       


        public StageService(IUnitOfWork uow, IStageRepository stageRepository, IAuditLogService auditLogService)
        {
            _unitOfWork = uow;
            _stageRepository = stageRepository;
            _auditLogService = auditLogService;            
        }

        #region Query
        public Stage Get(int id)
        {
            return _stageRepository.Get(id);
        }

        public IEnumerable<Stage> GetAll()
        {
            return _stageRepository.GetAll();
        }
        public IEnumerable<Stage> GetAll(string keyword, int id)
        {
            return _stageRepository.GetAll(keyword, id);
        }

        public void IsExistOnEditing(int id, string namaStage)
        {
            if (_stageRepository.IsExist(id, namaStage))
            {
                throw new ApplicationException(string.Format("Nama Stage {0} sudah ada.", namaStage));
            }
        }

        public void isExistOnAdding(string namaStage)
        {
            if (_stageRepository.IsExist(namaStage))
            {
                throw new ApplicationException(string.Format("Nama Stage {0} sudah ada.", namaStage));
            }
        }
        #endregion Query

        #region Manipulation
        public int Add(StageParam param)
        {
            int id;
            Validate.NotNull(param.NamaStage, "Nama Stage wajib diisi.");
            Validate.NotNull(param.Keterangan, "Keterangan wajib diisi.");

            isExistOnAdding(param.NamaStage);
            using (_unitOfWork)
            {
                Stage model = new Stage(param.NamaStage, param.Keterangan, param.CreateBy, param.CreateDate);
                _stageRepository.Insert(model);
                _unitOfWork.Commit();
                id = model.Id;

                //Audit Log ADD 
                int audit = _auditLogService.AddStageAudit(param, id);               
            }
            return id;
        }

        public int Update(int id, StageParam param)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Nama Stage tidak ditemukan.");

            Validate.NotNull(param.NamaStage, "Nama Stage wajib diisi.");
            Validate.NotNull(param.Keterangan, "Keterangan wajib diisi.");

            IsExistOnEditing(id, param.NamaStage);
            using (_unitOfWork)
            {
                //Audit Log UPDATE 
                int audit = _auditLogService.UpdateStageAudit(param, id);                

                model.Update(param.NamaStage, param.Keterangan, param.UpdateBy, param.UpdateDate);
                _stageRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;               
            }
            return id;
        }

        public int Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Nama Stage tidak ditemukan.");

            using (_unitOfWork)
            {
                model.Delete(deleteBy, deleteDate);
                _stageRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;

                //Audit Log Delete 
                int audit = _auditLogService.DeleteStageAudit(id, deleteBy);
            }
            return id;
        }
        #endregion Manipulation
    }
}
