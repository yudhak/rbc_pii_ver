﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;

namespace HR.Application
{
    public class StatusService : IStatusService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IStatusRepository _statusRepository;

        public StatusService(IUnitOfWork uow, IStatusRepository statusRepository)
        {
            _unitOfWork = uow;
            _statusRepository = statusRepository;
        }

        #region Query
        public IEnumerable<Status> GetAll(string keyword)
        {
            return _statusRepository.GetAll(keyword);
        }

        public IEnumerable<Status> GetAll()
        {
            return _statusRepository.GetAll();
        }

        public Status Get(int id)
        {
            return _statusRepository.Get(id);
        }

        #endregion Query

        #region Manipulation 

        public Status GetStatus(int id)
        {
            return _statusRepository.GetStatus(id);
        }

        #endregion Manipulation
    }
}
