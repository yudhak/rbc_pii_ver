﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;

namespace HR.Application
{
    public class MasterApprovalCorrelatedProjectService: IMasterApprovalCorrelatedProjectService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICommentsRepository _commentsRepository;
        private readonly IColorCommentRepository _colorCommentRepository;
        private readonly IMasterApprovalCorrelatedProjectRepository _masterApprovalCorrelatedProjectRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly ICorrelatedProjectRepository _correlatedProjectRepository;
        private readonly IAuditLogService _auditLogService;
        private readonly IUserRepository _userRepositoy;
        private readonly IMenuRepository _menuRepositoy;

        public MasterApprovalCorrelatedProjectService(IUnitOfWork uow, ICommentsRepository commentsRepository, IColorCommentRepository colorCommentRepository,
            IMasterApprovalCorrelatedProjectRepository masterApprovalCorrelatedProjectRepository, IAuditLogService auditLogService, IUserRepository userRepository
            , IMenuRepository menuRepositoy, IProjectRepository projectRepository, ICorrelatedProjectRepository correlatedProjectRepository)
        {
            _unitOfWork = uow;
            _commentsRepository = commentsRepository;
            _colorCommentRepository = colorCommentRepository;
            _masterApprovalCorrelatedProjectRepository = masterApprovalCorrelatedProjectRepository;
            _auditLogService = auditLogService;
            _userRepositoy = userRepository;
            _menuRepositoy = menuRepositoy;
            _projectRepository = projectRepository;
            _correlatedProjectRepository = correlatedProjectRepository;
        }

        #region Query
        public IEnumerable<MasterApprovalCorrelatedProject> GetAll()
        {
            return _masterApprovalCorrelatedProjectRepository.GetAll();
        }

        //public IEnumerable<MasterApprovalCorrelatedProject> GetAll(string keyword)
        //{
        //    return _masterApprovalCorrelatedProjectRepository.GetAll(keyword);
        //}

        public IEnumerable<MasterApprovalCorrelatedProject> GetAllByProjectId(int projectId)
        {
            return _masterApprovalCorrelatedProjectRepository.GetAllByProjectId(projectId);
        }

        public MasterApprovalCorrelatedProject Get(int id)
        {
            return _masterApprovalCorrelatedProjectRepository.Get(id);
        }

        public void IsExistOnEditing(int id, int menuId, int projectId, int userId, int nomorUrutStatus)
        {
            if (_masterApprovalCorrelatedProjectRepository.IsExist(id, menuId, projectId, userId, nomorUrutStatus))
            {
                throw new ApplicationException(string.Format("Master Approval Correlated Project {0} sudah ada.", id));
            }
        }
  
        #endregion Query

        #region Manipulation 
        public int Add(MasterApprovalCorrelatedProjectParam param)
        {
           // int id = 1;
            Validate.NotNull(param.UserIdList, "User wajib diisi.");

            if (param.UserIdList.Count() != 3)
            {
                throw new ApplicationException(string.Format("User Approval kurang lengkap (Harus 3 User)."));
            }
            if (param.UserIdList.Count() > 3)
            {
                throw new ApplicationException(string.Format("Jumlah maksimum User tidak sesuai (Maksimal 3 User)."));
            }

            IList<User> userlist = new List<User>();

            int nomorUrutStatus = 1;

            foreach (var item in param.UserIdList)
            {
                var user = _userRepositoy.Get(item.GetValueOrDefault());
                Validate.NotNull(param.UserIdList, "User tidak ditemukan.");
                userlist.Add(user);
            }

            var menu = _menuRepositoy.Get(param.MenuId.GetValueOrDefault());
            Validate.NotNull(menu, "Menu tidak ditemukan.");

            var project = _projectRepository.Get(param.ProjectId.GetValueOrDefault());
            Validate.NotNull(project, "Project tidak ditemukan.");
            if (project.IsActive != true)
            {
                throw new ApplicationException(string.Format("Project tidak aktif."));
            }

            using (_unitOfWork)
            {
                IList<MasterApprovalCorrelatedProject> masterApprovalCorrelatedProjectList = new List<MasterApprovalCorrelatedProject>();

                foreach (var item in userlist)
                {
                    MasterApprovalCorrelatedProject masterApprovalCorrelatedProject = new MasterApprovalCorrelatedProject(menu, project, item, nomorUrutStatus, param.CreateBy, param.CreateDate);
                    masterApprovalCorrelatedProjectList.Add(masterApprovalCorrelatedProject);
                    nomorUrutStatus++;
                }
                _masterApprovalCorrelatedProjectRepository.Insert(masterApprovalCorrelatedProjectList);


                _unitOfWork.Commit();

            }

            return project.Id;
        }

        public int Update(int projectId, MasterApprovalCorrelatedProjectParam param)
        {
            var project = _projectRepository.Get(param.ProjectId.GetValueOrDefault());
            Validate.NotNull(project, "Project tidak ditemukan.");
            if (project.IsActive != true)
            {
                throw new ApplicationException(string.Format("Project tidak aktif."));
            }

            var model = this.GetAllByProjectId(projectId);
            Validate.NotNull(model, "Master Approval Correlated Project tidak ditemukan.");

            if (model.Count() == 0)
            {
                throw new ApplicationException(string.Format("Master Approval Correlated Project tidak ditemukan."));
            }

            Validate.NotNull(param.UserIdList, "User wajib diisi.");

            if (param.UserIdList.Count() != 3)
            {
                throw new ApplicationException(string.Format("User Approval kurang lengkap (Harus 3 User)."));
            }
            if (param.UserIdList.Count() > 3)
            {
                throw new ApplicationException(string.Format("Jumlah maksimum User tidak sesuai (Maksimal 3 User)."));
            }

            IList<User> userlist = new List<User>();

            int[] nomorUrutStatusList = new int[3];

            foreach (var item in param.UserIdList)
            {
                var user = _userRepositoy.Get(item.GetValueOrDefault());
                Validate.NotNull(param.UserIdList, "User tidak ditemukan.");
                userlist.Add(user);
            }

            var menu = _menuRepositoy.Get(param.MenuId.GetValueOrDefault());
            Validate.NotNull(menu, "Menu tidak ditemukan.");

            using (_unitOfWork)
            {
                foreach (var item in model)
                {
                    item.Update(userlist[item.NomorUrutStatus.GetValueOrDefault() - 1], param.UpdateBy, param.UpdateDate);
                    _masterApprovalCorrelatedProjectRepository.Update(item);
                }
                //Audit Log UPDATE 
                //int audit = _auditLogService.UpdateCommentAudit(param, id);

                _unitOfWork.Commit();

            }
            return project.Id;
        }

        public int Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Master Approval Correlated Project tidak ditemukan.");

            using (_unitOfWork)
            {
                model.Delete(deleteBy, deleteDate);
                _masterApprovalCorrelatedProjectRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;

                //Audit Log DELETE
                //int audit = _auditLogService.DeleteCommentAudit(id, deleteBy);
            }
            return id;
        }

        public int DeleteByProjectId(int projectId, int deleteBy, DateTime deleteDate)
        {
            var model = this.GetAllByProjectId(projectId);
            Validate.NotNull(model, "Master Approval Correlated Project tidak ditemukan.");

            if (model.Count() == 0)
            {
                throw new ApplicationException(string.Format("Master Approval Correlated Project tidak ditemukan."));
            }

            using (_unitOfWork)
            {
                foreach (var item in model)
                {
                    item.Delete(deleteBy, deleteDate);
                    _masterApprovalCorrelatedProjectRepository.Update(item);
                }
                
                _unitOfWork.Commit();

                //Audit Log DELETE
                //int audit = _auditLogService.DeleteCommentAudit(id, deleteBy);
            }
            return projectId;
        }
        #endregion Manipulation
    }
}
