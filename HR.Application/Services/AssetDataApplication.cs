﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;

namespace HR.Application
{
    public class AssetDataService : IAssetDataService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAssetDataRepository _assetDataRepository;
        private readonly IAuditLogService _auditLogService;

        public AssetDataService(IUnitOfWork uow, IAssetDataRepository assetDataRepository, IAuditLogService auditLogService)
        {
            _unitOfWork = uow;
            _assetDataRepository = assetDataRepository;
            _auditLogService = auditLogService;
        }

        #region Query
        public IEnumerable<AssetData> GetAll()
        {
            return _assetDataRepository.GetAll();
        }
        public IEnumerable<AssetData> GetAll(string keyword, int id)
        {
            return _assetDataRepository.GetAll(keyword, id);
        }

        public AssetData Get(int id)
        {
            return _assetDataRepository.Get(id);
        }

        public void IsExistOnEditing(int id, string assetClass, int termAwal, int termAkhir, int outstandingStartYears, int outstandingEndYears, decimal assetValue, decimal porpotion, decimal assumedReturnPercentage, decimal assumedReturn, bool? status)
        {
            if (_assetDataRepository.IsExist(id, assetClass))
            {
                throw new ApplicationException(string.Format("Asset Class {0} sudah ada.", assetClass));
            }
        }

        public void isExistOnAdding(string assetClass)
        {
            if (_assetDataRepository.IsExist(assetClass))
            {
                throw new ApplicationException(string.Format("Asset Class {0} sudah ada.", assetClass));
            }
        }
        #endregion Query

        #region Manipulation 
        public int Add(AssetDataParam param)
        {
            int id;
            Validate.NotNull(param.AssetClass, "Asset Class wajib diisi.");

            isExistOnAdding(param.AssetClass);
            using (_unitOfWork)
            {
                AssetData model = new AssetData(param.AssetClass, param.TermAwal, param.TermAkhir, param.OutstandingStartYears, param.OutstandingEndYears, param.AssetValue, param.Porpotion, param.AssumedReturnPercentage, param.AssumedReturn, param.CreateBy, param.CreateDate, param.Status);
                _assetDataRepository.Insert(model);
                _unitOfWork.Commit();
                id = model.Id;

                //Audit Log ADD 
                int audit = _auditLogService.AddAssetDataAudit(param, id);
            }

            return id;
        }

        public int Update(int id, AssetDataParam param)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Asset Class tidak ditemukan.");

            IsExistOnEditing(id, param.AssetClass, param.TermAwal, param.TermAkhir, param.OutstandingStartYears, param.OutstandingEndYears, param.AssetValue, param.Porpotion, param.AssumedReturnPercentage, param.AssumedReturn, param.Status);
            using (_unitOfWork)
            {
                //Audit Log UPDATE 
                int audit = _auditLogService.UpdateAssetDataAudit(param, id);

                model.Update(param.AssetClass, param.TermAwal, param.TermAkhir, param.OutstandingStartYears, param.OutstandingEndYears, param.AssetValue, param.Porpotion, param.AssumedReturnPercentage, param.AssumedReturn, param.UpdateBy, param.UpdateDate, param.Status);
                _assetDataRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;
            }
            return id;
        }

        public int Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Asset Class tidak ditemukan.");

            using (_unitOfWork)
            {
                model.Delete(deleteBy, deleteDate);
                _assetDataRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;

                //Audit Log Delete 
                int audit = _auditLogService.DeleteAssetDataAudit(id, deleteBy);
            }
            return id;
        }
        #endregion Manipulation
    }
}
