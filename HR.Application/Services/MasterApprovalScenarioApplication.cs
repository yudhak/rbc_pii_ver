﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;

namespace HR.Application
{
    public class MasterApprovalScenarioService : IMasterApprovalScenarioService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICommentsRepository _commentsRepository;
        private readonly IColorCommentRepository _colorCommentRepository;
        private readonly IMasterApprovalScenarioRepository _masterApprovalScenarioRepository;
        private readonly IAuditLogService _auditLogService;
        private readonly IUserRepository _userRepositoy;
        private readonly IMenuRepository _menuRepositoy;

        public MasterApprovalScenarioService(IUnitOfWork uow, ICommentsRepository commentsRepository, IColorCommentRepository colorCommentRepository, 
            IMasterApprovalScenarioRepository masterApprovalScenarioRepository, IAuditLogService auditLogService, IUserRepository userRepository
            , IMenuRepository menuRepositoy)
        {
            _unitOfWork = uow;
            _commentsRepository = commentsRepository;
            _colorCommentRepository = colorCommentRepository;
            _masterApprovalScenarioRepository = masterApprovalScenarioRepository;
            _auditLogService = auditLogService;
            _userRepositoy = userRepository;
            _menuRepositoy = menuRepositoy;
        }

        #region Query
        public IEnumerable<MasterApprovalScenario> GetAll()
        {
            return _masterApprovalScenarioRepository.GetAll();
        }

        //public IEnumerable<MasterApprovalScenario> GetAll(string keyword)
        //{
        //    return _masterApprovalScenarioRepository.GetAll(keyword);
        //}

        public IEnumerable<MasterApprovalScenario> GetAllByMenuId(int menuid)
        {
            return _masterApprovalScenarioRepository.GetAllByMenuId(menuid);
        }

        public IEnumerable<MasterApprovalScenario> GetAllByMenuIdIsDeleteTrue(int menuid)
        {
            return _masterApprovalScenarioRepository.GetAllByMenuIdIsDeleteTrue(menuid);
        }

        public MasterApprovalScenario Get(int id)
        {
            return _masterApprovalScenarioRepository.Get(id);
        }

        public void IsExistOnEditing(int id, int menuId, int userId, int nomorUrutStatus)
        {
            if (_masterApprovalScenarioRepository.IsExist(id, menuId, userId, nomorUrutStatus))
            {
                throw new ApplicationException(string.Format("Master Approval {0} sudah ada.", id));
            }
        }

        public IEnumerable<MasterApprovalScenario> GetByCreateBy(int createBy)
        {
            return _masterApprovalScenarioRepository.GetByCreateBy(createBy);
        }

        #endregion Query

        #region Manipulation 
        public int Add(MasterApprovalScenarioParam param)
        {
            Validate.NotNull(param.UserIdList, "User wajib diisi.");

            if (param.UserIdList.Count() != 3)
            {
                throw new ApplicationException(string.Format("User Approval kurang lengkap (Harus 3 User)."));
            }
            if (param.UserIdList.Count() > 3)
            {
                throw new ApplicationException(string.Format("Jumlah maksimum User tidak sesuai (Maksimal 3 User)."));
            }

            IList<User> userlist = new List<User>();

            //int[] nomorUrutStatusList = new int[3];

            int nomorUrutStatus = 1;

            foreach (var item in param.UserIdList)
            {
                var user = _userRepositoy.Get(item.GetValueOrDefault());
                Validate.NotNull(user, "User ke {0} tidak ditemukan.");
                userlist.Add(user);
            }

            var menu = _menuRepositoy.Get(param.MenuId.GetValueOrDefault());
            Validate.NotNull(menu, "Menu tidak ditemukan.");

            using (_unitOfWork)
            {
                IList<MasterApprovalScenario> masterApprovalScenarioList = new List<MasterApprovalScenario>();

                foreach (var item in userlist)
                {
                    MasterApprovalScenario masterApprovalScenario = new MasterApprovalScenario(menu, item, nomorUrutStatus, param.CreateBy, param.CreateDate);
                    masterApprovalScenarioList.Add(masterApprovalScenario);
                    nomorUrutStatus++;
                }
                _masterApprovalScenarioRepository.Insert(masterApprovalScenarioList);


                _unitOfWork.Commit();

            }

            return menu.Id;
        }

        public int Update(int menuId, MasterApprovalScenarioParam param)
        {

            var menu = _menuRepositoy.Get(menuId);
            Validate.NotNull(menu, "Master Approval Scenario dengan menu {0} tidak ditemukan.", menuId);
            //menghitung jumlah user
            int userInput = 0;
            var model = this.GetAllByMenuIdIsDeleteTrue(menuId);
            Validate.NotNull(model, "Master Approval Scenario dengan menu {} ditemukan.");

            if (model.Count() == 0)
            {
                throw new ApplicationException(string.Format("Master Approval Scenario tidak ditemukan."));
            }

            Validate.NotNull(param.UserIdList, "User wajib diisi.");

            if (param.UserIdList.Count() != 3)
            {
                throw new ApplicationException(string.Format("User Approval kurang lengkap (Min 2 User)."));
            }
            if (param.UserIdList.Count() > 3)
            {
                throw new ApplicationException(string.Format("Jumlah maksimum User tidak sesuai (Maksimal 3 User)."));
            }

            IList<User> userlist = new List<User>();

            foreach (var item in param.UserIdList)
            {
                var user = _userRepositoy.Get(item.GetValueOrDefault());
                Validate.NotNull(param.UserIdList, "User tidak ditemukan.");
                if (user != null)
                {
                    userInput++;
                }
                userlist.Add(user);
            }

            if (userInput < 2 && userInput > 3)
            {
                throw new ApplicationException(string.Format("User Approval kurang lengkap (Min 2 User)."));
            }
            using (_unitOfWork)
            {
                //if (model.Count() == 2)
                //{
                //    for (int i = 0; i < model.Count(); i++)
                //    {

                //    }
                //}
                //if (model.Count() == 3)
                //{
                    foreach (var item in model)
                    {
                    //validate if user null or not
                        if (userlist[item.NomorUrutStatus.GetValueOrDefault() - 1] == null)
                        {
                            item.Delete(param.UpdateBy, param.UpdateDate);
                            _masterApprovalScenarioRepository.Update(item);
                        }
                        if (userlist[item.NomorUrutStatus.GetValueOrDefault() - 1] != null)
                        {
                            item.Update(userlist[item.NomorUrutStatus.GetValueOrDefault() - 1], param.UpdateBy, param.UpdateDate);
                            _masterApprovalScenarioRepository.Update(item);
                        }  
                    }
               // }
                //Audit Log UPDATE 
                //int audit = _auditLogService.UpdateCommentAudit(param, id);

                _unitOfWork.Commit();

            }
            return menuId;
        }

        public int Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Master Approval Scenario tidak ditemukan.");

            using (_unitOfWork)
            {
                model.Delete(deleteBy, deleteDate);
                _masterApprovalScenarioRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;

                //Audit Log DELETE
                //int audit = _auditLogService.DeleteCommentAudit(id, deleteBy);
            }
            return id;
        }

        public int DeleteByMenuId(int menuId, int deleteBy, DateTime deleteDate)
        {
            var model = this.GetAllByMenuId(menuId);
            Validate.NotNull(model, "Master Approval Scenario tidak ditemukan.");

            if (model.Count() == 0)
            {
                throw new ApplicationException(string.Format("Master Approval Scenario tidak ditemukan."));
            }

            using (_unitOfWork)
            {
                foreach (var item in model)
                {
                    item.Delete(deleteBy, deleteDate);
                    _masterApprovalScenarioRepository.Update(item);
                }

                _unitOfWork.Commit();

                //Audit Log DELETE
                //int audit = _auditLogService.DeleteCommentAudit(id, deleteBy);
            }
            return menuId;
        }
        #endregion Manipulation
    }
}
