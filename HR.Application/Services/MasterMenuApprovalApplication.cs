﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;

namespace HR.Application
{
    public class MasterMenuApprovalService : IMasterMenuApprovalService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMasterMenuApprovalRepository _masterMenuApprovalRepository;

        public MasterMenuApprovalService(IUnitOfWork uow, IMasterMenuApprovalRepository masterMenuApprovalRepository)
        {
            _unitOfWork = uow;
            _masterMenuApprovalRepository = masterMenuApprovalRepository;
        }

        #region Query
        public IEnumerable<MasterMenuApproval> GetAll(string keyword)
        {
            return _masterMenuApprovalRepository.GetAll(keyword);
        }

        public IEnumerable<MasterMenuApproval> GetAll()
        {
            return _masterMenuApprovalRepository.GetAll();
        }

        public MasterMenuApproval Get(int id)
        {
            return _masterMenuApprovalRepository.Get(id);
        }

        #endregion Query
    }
}
