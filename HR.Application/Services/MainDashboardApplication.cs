﻿using System;
using HR.Domain;
using System.Collections.Generic;
using System.Linq;
using HR.Common;
using HR.Application.Params;
using HR.Common.Validation;

namespace HR.Application
{
    public class MainDashboardService : IMainDashboardService
    {
        private readonly ICorrelatedProjectRepository _correlatedProjectRepository;
        private readonly ICorrelatedProjectDetailRepository _correlatedProjectDetailRepository;
        private readonly IRiskRegistrasiRepository _riskRegistrasiRepository;
        private readonly IScenarioRepository _scenarioRepository;
        private readonly IRiskMatrixProjectRepository _riskMatrixProjectRepository;
        private readonly IStageTahunRiskMatrixRepository _stageTahunRiskMatrixRepository;
        private readonly IStageTahunRiskMatrixDetailRepository _stageTahunRiskMatrixDetailRepository;
        private readonly ISektorRepository _sektorRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly ICorrelatedSektorRepository _correlatedSektorRepository;
        private readonly ICorrelatedSektorDetailRepository _correlatedSektorDetailRepository;
        private readonly IResultService _resultService;
        private readonly IMatrixRepository _matrixRepository;
        private readonly IFunctionalRiskRepository _functionalRiskRepository;
        private readonly IScenarioCalculationRepository _scenarioCalculationRepository;
        private readonly IAssetDataRepository _assetDataRepository;
        private readonly IPMNRepository _pmnRepository;
        private readonly IProjectCalculationRepository _projectCalculationRepository;
        private readonly IScenarioDetailRepository _scenarioDetailRepository;
        private readonly ICalculationService _calculationService;

        public MainDashboardService(ICorrelatedProjectRepository correlatedProjectRepository, ICorrelatedProjectDetailRepository correlatedProjectDetailRepository, IRiskRegistrasiRepository riskRegistrasiRepository,
            IScenarioRepository scenarioRepository, IRiskMatrixProjectRepository riskMatrixProjectRepository, IStageTahunRiskMatrixRepository stageTahunRiskMatrixRepository, IStageTahunRiskMatrixDetailRepository stageTahunRiskMatrixDetailRepository,
            ISektorRepository sektorRepository, IProjectRepository projectRepository, ICorrelatedSektorRepository correlatedSektorRepository, ICorrelatedSektorDetailRepository correlatedSektorDetailRepository, IResultService resultService
            , IMatrixRepository matrixRepository, IFunctionalRiskRepository functionalRiskRepository, IScenarioCalculationRepository scenarioCalculationRepository, IAssetDataRepository assetDataRepository, IPMNRepository pmnRepository,
            IProjectCalculationRepository projectCalculationRepository, IScenarioDetailRepository scenarioDetailRepository, ICalculationService calculationService)
        {
            _correlatedProjectRepository = correlatedProjectRepository;
            _correlatedProjectDetailRepository = correlatedProjectDetailRepository;
            _riskRegistrasiRepository = riskRegistrasiRepository;
            _scenarioRepository = scenarioRepository;
            _riskMatrixProjectRepository = riskMatrixProjectRepository;
            _stageTahunRiskMatrixRepository = stageTahunRiskMatrixRepository;
            _stageTahunRiskMatrixDetailRepository = stageTahunRiskMatrixDetailRepository;
            _sektorRepository = sektorRepository;
            _projectRepository = projectRepository;
            _correlatedSektorRepository = correlatedSektorRepository;
            _correlatedSektorDetailRepository = correlatedSektorDetailRepository;
            _resultService = resultService;
            _matrixRepository = matrixRepository;
            _functionalRiskRepository = functionalRiskRepository;
            _scenarioCalculationRepository = scenarioCalculationRepository;
            _assetDataRepository = assetDataRepository;
            _pmnRepository = pmnRepository;
            _projectCalculationRepository = projectCalculationRepository;
            _scenarioDetailRepository = scenarioDetailRepository;
            _calculationService = calculationService;
        }

        // year generator
        public IList<int> GenerateYear(int scenarioId)
        {
            var scenario = _scenarioRepository.Get(scenarioId);
            var likelihoodDetail = scenario.Likehood.LikehoodDetails;
            var riskRegistrasi = _riskRegistrasiRepository.GetAll().ToList();
            //var projects = scenario.ScenarioDetail.Where(x => x.IsDelete == false).ToList();
            //var projects = _projectRepository.GetAll().Where(x => x.IsActive == true)    // your starting point - table in the "from" statement
            //.Join(scenario.ScenarioDetail.Where(x => x.IsDelete == false), // the source table of the inner join
            //proj => proj.Id,        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
            //scdet => scdet.ProjectId,   // Select the foreign key (the second part of the "on" clause)
            //(proj, scdet) => scdet).ToList(); // selection
            var projects = from scenarioCal in _scenarioCalculationRepository.GetAll().ToList()
                           .Where(mapping => mapping.ScenarioId == scenario.Id)
                           from projectCal in _projectCalculationRepository.GetAll().ToList()
                           .Where(mapping => mapping.ScenarioCalculationId == scenarioCal.Id)
                           from scenarioDetail in _scenarioDetailRepository.GetAll().ToList()
                           .Where(mapping => mapping.ProjectId == projectCal.ProjectId && mapping.ScenarioId == scenario.Id)
                           select scenarioDetail;

            List<int> tahunAwal = new List<int>();
            List<int> tahunAkhir = new List<int>();

            if (projects.Count() > 0)
            {
                foreach (var item in projects)
                {
                    var riskMatrixProject = _riskMatrixProjectRepository.GetByScenarioIdProjectId(scenario.Id, item.ProjectId);
                    Validate.NotNull(riskMatrixProject, "Risk Matrix proyek tidak ditemukan.");

                    var project = item.Project;
                    var stageTahunRiskMatrix = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectId(riskMatrixProject.Id).ToList();
                    if (stageTahunRiskMatrix.Count > 0)
                    {
                        foreach (var dataStage in stageTahunRiskMatrix)
                        {
                            decimal? totalPerYear = 0;

                            List<decimal?> Total = new List<decimal?>();

                            var stageTahunDetail = _stageTahunRiskMatrixDetailRepository.GetByStageTahunRiskMatrixId(dataStage.Id).ToList();

                            Total.Add(totalPerYear);
                        }
                        tahunAwal.Add(project.TahunAwalProject.Year);
                        tahunAkhir.Add(project.TahunAkhirProject.Year);
                    }
                    else
                    {
                        throw new ApplicationException(string.Format("Tahun Stage pada proyek {0} dengan skenario {1} tidak ditemukan / belum diisi. Silakan cek di menu Riskio Matriks - Tambah Risiko Matriks dan pilih proyek {0}.", project.NamaProject, scenario.NamaScenario));
                    }
                }
            }
            //int thnAwal = tahunAwal.Min();
            int thnAwal = 2017;
            int thnAkhir = tahunAkhir.Max();
            int interval = thnAkhir - thnAwal;
            List<int> tahun = new List<int>();
            for (int i = 0; i <= interval; i++)
            {
                tahun.Add(i + thnAwal);
            }
            return tahun;
        }

        //Haris
        public void ValidateCollorComment(FunctionalRiskScenarioResult functionalRiskScenarioResult, FunctionalRisk validateColor, int matrixId, int scenarioId, decimal valueConverter)
        {
            if (valueConverter > validateColor.NilaiMaksimum)
            {
                functionalRiskScenarioResult.CollorComment = "Green";
                var validateComent = _functionalRiskRepository.GetCommentByColor(matrixId, scenarioId, 1);
                functionalRiskScenarioResult.Comment = validateComent.Komentar;
            }
            if (valueConverter <= validateColor.NilaiMaksimum && valueConverter >= validateColor.NilaiMinimum)
            {
                functionalRiskScenarioResult.CollorComment = "Yellow";
                var validateComent = _functionalRiskRepository.GetCommentByColor(matrixId, scenarioId, 2);
                functionalRiskScenarioResult.Comment = validateComent.Komentar;
            }
            if (valueConverter < validateColor.NilaiMinimum)
            {
                functionalRiskScenarioResult.CollorComment = "Red";
                var validateComent = _functionalRiskRepository.GetCommentByColor(matrixId, scenarioId, 3);
                functionalRiskScenarioResult.Comment = validateComent.Komentar;
            }
        }

        public void ValidateCollorComment2(FunctionalRiskScenarioResult functionalRiskScenarioResult, FunctionalRisk validateColor, int matrixId, int scenarioId, decimal valueConverter)
        {
            if (valueConverter > validateColor.NilaiMaksimum)
            {
                functionalRiskScenarioResult.CollorComment = "Red";
                var validateComent = _functionalRiskRepository.GetCommentByColor(matrixId, scenarioId, 1);
                functionalRiskScenarioResult.Comment = validateComent.Komentar;
            }
            if (valueConverter <= validateColor.NilaiMaksimum && valueConverter >= validateColor.NilaiMinimum)
            {
                functionalRiskScenarioResult.CollorComment = "Yellow";
                var validateComent = _functionalRiskRepository.GetCommentByColor(matrixId, scenarioId, 2);
                functionalRiskScenarioResult.Comment = validateComent.Komentar;
            }
            if (valueConverter < validateColor.NilaiMinimum)
            {
                functionalRiskScenarioResult.CollorComment = "Green";
                var validateComent = _functionalRiskRepository.GetCommentByColor(matrixId, scenarioId, 3);
                functionalRiskScenarioResult.Comment = validateComent.Komentar;
            }
        }
        //End Haris

        public MainDashboardResult GetAggregationOfProject()
        {
            MainDashboardResult result = new MainDashboardResult();

            var scenario = _scenarioRepository.GetDefault();
            Validate.NotNull(scenario, "Skenario default tidak ditemukan. Silakan set default salah satu Skenario dahulu.");
            int indexOtherScenario = 1;
            #region Haris
            var scenarioList = _scenarioCalculationRepository.GetAll().ToList();
            List<int> otherScenario = new List<int>();

            foreach(var item in scenarioList)
            {
                if(item.ScenarioId != scenario.Id)
                {
                    otherScenario.Add(item.ScenarioId);
                }
            }

            var scenario1 = scenario;
            var scenario2 = scenario;
            if (otherScenario.Count < 1 && scenario == null)
            {
                throw new ApplicationException(string.Format("Data kalkulasi skenario tidak ditemukan."));
            }
            else if (otherScenario.Count == 1 && scenario != null)
            {
               scenario1 = _scenarioRepository.Get(otherScenario[0]);
               scenario2 = _scenarioRepository.Get(otherScenario[0]);
            }
            else if (otherScenario.Count >= 2 && scenario != null)
            {
                scenario1 = _scenarioRepository.Get(otherScenario[1]);
                scenario2 = _scenarioRepository.Get(otherScenario[0]);
            }   
            //var scenario1 = _scenarioRepository.Get(otherScenario[indexOtherScenario]);
            //var scenario2 = _scenarioRepository.Get(otherScenario[0]);
            var yearNow = DateTimeOffset.Now.Year.ToString();
            //var yearNow = "2017";
            var dataResult = _resultService.GetAllDataResult(scenario.Id, 0, 0, 0);
            IList<Matrix> matrixs = _matrixRepository.GetAll().ToList();
            IList<FunctionalRisk> functionalRisks = _functionalRiskRepository.GetAll(null,0).ToList();
            List<FunctionalRiskResult> functionalRiskResultList = new List<FunctionalRiskResult>();
            List<FunctionalRiskDashboard> functionalRiskDashboardList = new List<FunctionalRiskDashboard>();
            //Functional Risk Result
            foreach (var matrixItem in matrixs.ToList())
            {
                List<FunctionalRiskScenarioResult> functionalRiskScenarioResultList = new List<FunctionalRiskScenarioResult>();
                //item 1, 2, 3, 4
                if (matrixItem.Id == 1 || matrixItem.Id == 2 || matrixItem.Id ==3 || matrixItem.Id == 4)
                {
                    FunctionalRiskResult functionalRiskResult = new FunctionalRiskResult();
                    functionalRiskResult.MatrixId = matrixItem.Id;
                    functionalRiskResult.NamaMatrix = matrixItem.NamaMatrix;
                    functionalRiskResult.NamaFormula = matrixItem.NamaFormula;
                    var data1 = Convert.ToDecimal(0);

                    //getScenario 1 calculation
                    if (matrixItem.Id == 1)
                    {
                        foreach (var item2 in dataResult.LeverageRatio.LeverageItem[4].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                                functionalRiskScenarioResult.Scenario = scenario.NamaScenario;
                                var ValueConvert = Math.Round(item2.Value, 2, MidpointRounding.AwayFromZero);
                                functionalRiskScenarioResult.Calculations = Convert.ToString(ValueConvert);
                                var validateColor = _functionalRiskRepository.GetYellowByMatrix(matrixItem.Id, scenario.Id);
                                Validate.NotNull(validateColor, "Functional Risk untuk skenario " + scenario.NamaScenario + " belum ada. Harap diisi terlebih dahulu di Menu Master Ambang Batas");

                                //getColorComment
                                ValidateCollorComment(functionalRiskScenarioResult, validateColor, matrixItem.Id, scenario.Id, ValueConvert);
                                functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                            }                           
                        }
                    }
                    if (matrixItem.Id == 2)
                    {
                        foreach (var item2 in dataResult.LeverageRatio.LeverageItem[5].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                                functionalRiskScenarioResult.Scenario = scenario.NamaScenario;
                                var ValueConvert = Math.Round(item2.Value, 2, MidpointRounding.AwayFromZero);
                                functionalRiskScenarioResult.Calculations = Convert.ToString(ValueConvert);
                                var validateColor = _functionalRiskRepository.GetYellowByMatrix(matrixItem.Id, scenario.Id);

                                //getColorComment
                                ValidateCollorComment(functionalRiskScenarioResult, validateColor, matrixItem.Id, scenario.Id, ValueConvert);
                                functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                            }
                        }
                    }
                    if (matrixItem.Id == 3)
                    {
                        foreach (var item2 in dataResult.LeverageRatio.LeverageItem[6].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                                functionalRiskScenarioResult.Scenario = scenario.NamaScenario;
                                var ValueConvert = Math.Round(item2.Value, 2, MidpointRounding.AwayFromZero);
                                functionalRiskScenarioResult.Calculations = Convert.ToString(ValueConvert);
                                var validateColor = _functionalRiskRepository.GetYellowByMatrix(matrixItem.Id, scenario.Id);

                                //getColorComment
                                ValidateCollorComment(functionalRiskScenarioResult, validateColor, matrixItem.Id, scenario.Id, ValueConvert);
                                functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                            }
                        }
                    }
                    if (matrixItem.Id == 4)
                    {
                        FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                        functionalRiskScenarioResult.Scenario = scenario.NamaScenario;                        
                        var data2 = Convert.ToDecimal(0);
                        foreach (var item2 in dataResult.AvailableVSRiskCapital.AvailableVSRiskCapitalCollection[0].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                data1 = item2.Value;
                            }
                        }
                        foreach (var item2 in dataResult.AvailableVSRiskCapital.AvailableVSRiskCapitalCollection[2].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                data2 = item2.Value;
                            }
                        }
                        if (data2 == 0 || data2 == null)
                        {
                            data2 = 1;
                        }
                        var nilai = data1/data2;
                        var ValueConvert = Math.Round(nilai, 2, MidpointRounding.AwayFromZero);
                        functionalRiskScenarioResult.Calculations = Convert.ToString(ValueConvert);
                        var validateColor = _functionalRiskRepository.GetYellowByMatrix(matrixItem.Id, scenario.Id);
                        Validate.NotNull(validateColor, "Functional Risk untuk skenario " + scenario.NamaScenario + " belum ada. Harap diisi terlebih dahulu di Menu Master Ambang Batas");
                        //getColorComment
                        ValidateCollorComment(functionalRiskScenarioResult, validateColor, matrixItem.Id, scenario.Id, ValueConvert);
                        functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                    }
                    
                    //getScenario 2 & 3 calculation
                    if (matrixItem.Id == 1)
                    {
                        foreach (var item2 in dataResult.AvailableVSRiskCapital.AvailableVSRiskCapitalCollection[1].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                data1 = item2.Value;
                            }
                        }
                    }
                    
                    if (matrixItem.Id == 2)
                    {
                        foreach (var item2 in dataResult.AvailableVSRiskCapital.AvailableVSRiskCapitalCollection[3].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                data1 = item2.Value;
                            }
                        }
                    }

                    if (matrixItem.Id == 3 || matrixItem.Id == 4)
                    {
                        foreach (var item2 in dataResult.AvailableVSRiskCapital.AvailableVSRiskCapitalCollection[0].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                data1 = item2.Value;
                            }
                        }
                    }

                    foreach (var item2 in dataResult.SensitivityStressTesting.StressTestingResult.RiskCapitalCollection.ToList())
                    {
                        if(item2.NamaScenario == scenario1.NamaScenario)
                        {
                            FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                            functionalRiskScenarioResult.Scenario = scenario1.NamaScenario;
                            foreach(var item3 in item2.Values.ToList())
                            {
                                if (item3.Year.ToString() == yearNow)
                                {
                                    var data2 = item3.Value;
                                    if(data2 == 0 || data2 == null)
                                    {
                                        data2 = 1;
                                    }
                                    var nilai = data1 / data2;
                                    var ValueConvert = Math.Round(nilai, 2, MidpointRounding.AwayFromZero);
                                    functionalRiskScenarioResult.Calculations = Convert.ToString(ValueConvert);
                                    var validateColor = _functionalRiskRepository.GetYellowByMatrix(matrixItem.Id, scenario1.Id);
                                    Validate.NotNull(validateColor, "Functional Risk untuk skenario " + scenario1.NamaScenario + " belum ada. Harap diisi terlebih dahulu di Menu Master Ambang Batas.");
                                    //getColorComment
                                    ValidateCollorComment(functionalRiskScenarioResult, validateColor, matrixItem.Id, scenario1.Id, ValueConvert);

                                    functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                                }
                            }                           
                            
                        }
                        if(item2.NamaScenario == scenario2.NamaScenario)
                        {
                            FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                            functionalRiskScenarioResult.Scenario = scenario2.NamaScenario;
                            foreach (var item3 in item2.Values.ToList())
                            {
                                if (item3.Year.ToString() == yearNow)
                                {
                                    var data2 = item3.Value;
                                    if (data2 == 0 || data2 == null)
                                    {
                                        data2 = 1;
                                    }
                                    var nilai = data1 / data2;
                                    var ValueConvert = Math.Round(nilai, 2, MidpointRounding.AwayFromZero);
                                    functionalRiskScenarioResult.Calculations = Convert.ToString(ValueConvert);
                                    var validateColor = _functionalRiskRepository.GetYellowByMatrix(matrixItem.Id, scenario2.Id);
                                    Validate.NotNull(validateColor, "Functional Risk untuk skenario " + scenario2.NamaScenario + " belum ada. Harap diisi terlebih dahulu di Menu Master Ambang Batas.");
                                    //getColorComment
                                    ValidateCollorComment(functionalRiskScenarioResult, validateColor, matrixItem.Id, scenario2.Id, ValueConvert);
                                    
                                    functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                                }
                            }
                        }
                    }

                    functionalRiskResult.Scenarios = functionalRiskScenarioResultList;
                    functionalRiskResultList.Add(functionalRiskResult);
                }

                if (matrixItem.Id == 5)
                {
                    FunctionalRiskResult functionalRiskResult = new FunctionalRiskResult();
                    functionalRiskResult.MatrixId = matrixItem.Id;
                    functionalRiskResult.NamaMatrix = matrixItem.NamaMatrix;
                    functionalRiskResult.NamaFormula = matrixItem.NamaFormula;
                    var data1 = Convert.ToDecimal(0);

                    //getScenario 1 calculation
                    if (matrixItem.Id == 5)
                    {
                        foreach (var item2 in dataResult.RiskBudget.RiskBudgetBySektor.RiskBudgetCollectionBySektor[0].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                                functionalRiskScenarioResult.Scenario = scenario.NamaScenario;
                                var ValueConvert = Math.Round(item2.Value, 2, MidpointRounding.AwayFromZero);
                                functionalRiskScenarioResult.Calculations = Convert.ToString(ValueConvert) + "%";
                                var validateColor = _functionalRiskRepository.GetYellowByMatrix(matrixItem.Id, scenario.Id);

                                //getColorComment
                                ValidateCollorComment2(functionalRiskScenarioResult, validateColor, matrixItem.Id, scenario.Id, ValueConvert);
                                functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                            }
                        }
                    }
                    //getScenario 2 calculation
                    if (matrixItem.Id == 5)
                    {
                        FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                        functionalRiskScenarioResult.Scenario = scenario1.NamaScenario;
                        functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                    }
                    //getScenario 3 calculation
                    if (matrixItem.Id == 5)
                    {
                        FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                        functionalRiskScenarioResult.Scenario = scenario2.NamaScenario;
                        functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                    }

                    functionalRiskResult.Scenarios = functionalRiskScenarioResultList;
                    functionalRiskResultList.Add(functionalRiskResult);
                }

                if (matrixItem.Id == 6)
                {
                    FunctionalRiskResult functionalRiskResult = new FunctionalRiskResult();
                    functionalRiskResult.MatrixId = matrixItem.Id;
                    functionalRiskResult.NamaMatrix = matrixItem.NamaMatrix;
                    functionalRiskResult.NamaFormula = matrixItem.NamaFormula;
                    var data1 = Convert.ToDecimal(0);

                    //getScenario 1 calculation
                    if (matrixItem.Id == 6)
                    {
                        foreach (var item2 in dataResult.RiskBudget.RiskBudgetByProject.RiskBudgetCollection[0].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                                functionalRiskScenarioResult.Scenario = scenario.NamaScenario;
                                var ValueConvert = Math.Round(item2.Value, 2, MidpointRounding.AwayFromZero);
                                functionalRiskScenarioResult.Calculations = Convert.ToString(ValueConvert) + "%";
                                var validateColor = _functionalRiskRepository.GetYellowByMatrix(matrixItem.Id, scenario.Id);

                                //getColorComment
                                ValidateCollorComment2(functionalRiskScenarioResult, validateColor, matrixItem.Id, scenario.Id, ValueConvert);
                                functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                            }
                        }
                    }
                    //getScenario 2 calculation
                    if (matrixItem.Id == 6)
                    {
                        FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                        functionalRiskScenarioResult.Scenario = scenario1.NamaScenario;
                        functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                    }
                    //getScenario 3 calculation
                    if (matrixItem.Id == 6)
                    {
                        FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                        functionalRiskScenarioResult.Scenario = scenario2.NamaScenario;
                        functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                    }

                    functionalRiskResult.Scenarios = functionalRiskScenarioResultList;
                    functionalRiskResultList.Add(functionalRiskResult);
                }

                if (matrixItem.Id == 7)
                {
                    FunctionalRiskResult functionalRiskResult = new FunctionalRiskResult();
                    functionalRiskResult.MatrixId = matrixItem.Id;
                    functionalRiskResult.NamaMatrix = matrixItem.NamaMatrix;
                    functionalRiskResult.NamaFormula = matrixItem.NamaFormula;
                    var data1 = Convert.ToDecimal(0);

                    //getScenario 1 calculation
                    if (matrixItem.Id == 7)
                    {
                        foreach (var item2 in dataResult.RiskBudget.RiskBudgetByRisk.RiskBudgetCollectionByRisk[0].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                                functionalRiskScenarioResult.Scenario = scenario.NamaScenario;
                                var ValueConvert = Math.Round(item2.Value, 2, MidpointRounding.AwayFromZero);
                                functionalRiskScenarioResult.Calculations = Convert.ToString(ValueConvert) + "%";
                                var validateColor = _functionalRiskRepository.GetYellowByMatrix(matrixItem.Id, scenario.Id);

                                //getColorComment
                                ValidateCollorComment2(functionalRiskScenarioResult, validateColor, matrixItem.Id, scenario.Id, ValueConvert);
                                functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                            }
                        }
                    }
                    //getScenario 2 calculation
                    if (matrixItem.Id == 7)
                    {
                        FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                        functionalRiskScenarioResult.Scenario = scenario1.NamaScenario;
                        functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                    }
                    //getScenario 3 calculation
                    if (matrixItem.Id == 7)
                    {
                        FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                        functionalRiskScenarioResult.Scenario = scenario2.NamaScenario;
                        functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                    }

                    functionalRiskResult.Scenarios = functionalRiskScenarioResultList;
                    functionalRiskResultList.Add(functionalRiskResult);
                }
            }
            result.FunctionalRiskResultTableResults = functionalRiskResultList;

            //Functional Risk Dashboard
            foreach (var matrixItem in matrixs.ToList())
            {
                List<FunctionalRiskColorDashboard> functionalRiskColorDashboardList = new List<FunctionalRiskColorDashboard>();
                if (matrixItem.Id != 0)
                {
                    FunctionalRiskDashboard functionalRiskDashboard = new FunctionalRiskDashboard();
                    functionalRiskDashboard.MatrixId = matrixItem.Id;
                    functionalRiskDashboard.NamaMatrix = matrixItem.NamaMatrix;
                    functionalRiskDashboard.NamaFormula = matrixItem.NamaFormula;
                    var data1 = Convert.ToDecimal(0);                   
                    if (matrixItem.Id != 0)
                    {
                        List<FunctionalRiskScenarioDashboard> functionalRiskScenarioDashboardList = new List<FunctionalRiskScenarioDashboard>();
                        FunctionalRiskColorDashboard functionalRiskColorDashboard = new FunctionalRiskColorDashboard();
                        functionalRiskColorDashboard.Color = "Green";                      
                        if(matrixItem.Id != 0)
                        {
                            var validateDefinisi = _functionalRiskRepository.GetCommentByColor(matrixItem.Id, scenario.Id, 1);
                            FunctionalRiskScenarioDashboard functionalRiskScenarioDashboard = new FunctionalRiskScenarioDashboard();
                            functionalRiskScenarioDashboard.Scenario = scenario.NamaScenario;
                            functionalRiskScenarioDashboard.Definisi = validateDefinisi.Definisi;
                            functionalRiskScenarioDashboardList.Add(functionalRiskScenarioDashboard);
                        }
                        if (matrixItem.Id != 0)
                        {
                            var validateDefinisi = _functionalRiskRepository.GetCommentByColor(matrixItem.Id, scenario1.Id, 1);
                            FunctionalRiskScenarioDashboard functionalRiskScenarioDashboard = new FunctionalRiskScenarioDashboard();
                            functionalRiskScenarioDashboard.Scenario = scenario1.NamaScenario;
                            functionalRiskScenarioDashboard.Definisi = validateDefinisi.Definisi;
                            functionalRiskScenarioDashboardList.Add(functionalRiskScenarioDashboard);
                        }
                        if (matrixItem.Id != 0)
                        {
                            var validateDefinisi = _functionalRiskRepository.GetCommentByColor(matrixItem.Id, scenario2.Id, 1);
                            FunctionalRiskScenarioDashboard functionalRiskScenarioDashboard = new FunctionalRiskScenarioDashboard();
                            functionalRiskScenarioDashboard.Scenario = scenario2.NamaScenario;
                            functionalRiskScenarioDashboard.Definisi = validateDefinisi.Definisi;
                            functionalRiskScenarioDashboardList.Add(functionalRiskScenarioDashboard);
                        }
                        functionalRiskColorDashboard.Scenarios = functionalRiskScenarioDashboardList;
                        functionalRiskColorDashboardList.Add(functionalRiskColorDashboard);
                    }
                    if (matrixItem.Id != 0)
                    {
                        List<FunctionalRiskScenarioDashboard> functionalRiskScenarioDashboardList = new List<FunctionalRiskScenarioDashboard>();
                        FunctionalRiskColorDashboard functionalRiskColorDashboard = new FunctionalRiskColorDashboard();
                        functionalRiskColorDashboard.Color = "Yellow";
                        if (matrixItem.Id != 0)
                        {
                            var validateDefinisi = _functionalRiskRepository.GetCommentByColor(matrixItem.Id, scenario.Id, 2);
                            FunctionalRiskScenarioDashboard functionalRiskScenarioDashboard = new FunctionalRiskScenarioDashboard();
                            functionalRiskScenarioDashboard.Scenario = scenario.NamaScenario;
                            functionalRiskScenarioDashboard.Definisi = validateDefinisi.Definisi;
                            functionalRiskScenarioDashboardList.Add(functionalRiskScenarioDashboard);
                        }
                        if (matrixItem.Id != 0)
                        {
                            var validateDefinisi = _functionalRiskRepository.GetCommentByColor(matrixItem.Id, scenario1.Id, 2);
                            FunctionalRiskScenarioDashboard functionalRiskScenarioDashboard = new FunctionalRiskScenarioDashboard();
                            functionalRiskScenarioDashboard.Scenario = scenario1.NamaScenario;
                            functionalRiskScenarioDashboard.Definisi = validateDefinisi.Definisi;
                            functionalRiskScenarioDashboardList.Add(functionalRiskScenarioDashboard);
                        }
                        if (matrixItem.Id != 0)
                        {
                            var validateDefinisi = _functionalRiskRepository.GetCommentByColor(matrixItem.Id, scenario2.Id, 2);
                            FunctionalRiskScenarioDashboard functionalRiskScenarioDashboard = new FunctionalRiskScenarioDashboard();
                            functionalRiskScenarioDashboard.Scenario = scenario2.NamaScenario;
                            functionalRiskScenarioDashboard.Definisi = validateDefinisi.Definisi;
                            functionalRiskScenarioDashboardList.Add(functionalRiskScenarioDashboard);
                        }
                        functionalRiskColorDashboard.Scenarios = functionalRiskScenarioDashboardList;
                        functionalRiskColorDashboardList.Add(functionalRiskColorDashboard);
                    }
                    if (matrixItem.Id != 0)
                    {
                        List<FunctionalRiskScenarioDashboard> functionalRiskScenarioDashboardList = new List<FunctionalRiskScenarioDashboard>();
                        FunctionalRiskColorDashboard functionalRiskColorDashboard = new FunctionalRiskColorDashboard();
                        functionalRiskColorDashboard.Color = "Red";
                        if (matrixItem.Id != 0)
                        {
                            var validateDefinisi = _functionalRiskRepository.GetCommentByColor(matrixItem.Id, scenario.Id, 3);
                            FunctionalRiskScenarioDashboard functionalRiskScenarioDashboard = new FunctionalRiskScenarioDashboard();
                            functionalRiskScenarioDashboard.Scenario = scenario.NamaScenario;
                            functionalRiskScenarioDashboard.Definisi = validateDefinisi.Definisi;
                            functionalRiskScenarioDashboardList.Add(functionalRiskScenarioDashboard);
                        }
                        if (matrixItem.Id != 0)
                        {
                            var validateDefinisi = _functionalRiskRepository.GetCommentByColor(matrixItem.Id, scenario1.Id, 3);
                            FunctionalRiskScenarioDashboard functionalRiskScenarioDashboard = new FunctionalRiskScenarioDashboard();
                            functionalRiskScenarioDashboard.Scenario = scenario1.NamaScenario;
                            functionalRiskScenarioDashboard.Definisi = validateDefinisi.Definisi;
                            functionalRiskScenarioDashboardList.Add(functionalRiskScenarioDashboard);
                        }
                        if (matrixItem.Id != 0)
                        {
                            var validateDefinisi = _functionalRiskRepository.GetCommentByColor(matrixItem.Id, scenario2.Id, 3);
                            FunctionalRiskScenarioDashboard functionalRiskScenarioDashboard = new FunctionalRiskScenarioDashboard();
                            functionalRiskScenarioDashboard.Scenario = scenario2.NamaScenario;
                            functionalRiskScenarioDashboard.Definisi = validateDefinisi.Definisi;
                            functionalRiskScenarioDashboardList.Add(functionalRiskScenarioDashboard);
                        }
                        functionalRiskColorDashboard.Scenarios = functionalRiskScenarioDashboardList;
                        functionalRiskColorDashboardList.Add(functionalRiskColorDashboard);
                    }
                    functionalRiskDashboard.Colors = functionalRiskColorDashboardList;
                    functionalRiskDashboardList.Add(functionalRiskDashboard);
                }
            }
            result.FunctionalRiskMainDashboard = functionalRiskDashboardList;
            #endregion Haris

            var likelihoodDetail = scenario.Likehood.LikehoodDetails;
            var riskRegistrasi = _riskRegistrasiRepository.GetAll().ToList();
            

            //var projects = scenario.ScenarioDetail.Where(x => x.IsDelete == false).ToList();
            //var projects = _projectRepository.GetAll().Where(x => x.IsActive == true && x.Tahapan.NamaTahapan == "Monitoring" && x.IsDelete == false)    // your starting point - table in the "from" statement
            //.Join(scenario.ScenarioDetail.Where(x => x.IsDelete == false), // the source table of the inner join
            //proj => proj.Id,        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
            //scdet => scdet.ProjectId,   // Select the foreign key (the second part of the "on" clause)
            //(proj, scdet) => scdet).ToList(); // selection
            var scenarioCalculation = _scenarioCalculationRepository.GetAll().ToList();

            IList<ScenarioDetail> projects = new List<ScenarioDetail>();
            var listPro = new int[0];
            var scenarioDefaultExist = scenarioCalculation.Where(x => x.ScenarioId == scenario.Id).SingleOrDefault();
            ScenarioProject scenarioProject = new ScenarioProject();
            if (scenarioDefaultExist == null)
            {
                var projects3 = from projectCal in _projectRepository.GetAll().ToList()
                               .Where(mapping => mapping.IsActive == true && mapping.IsDelete == false)
                                from scenarioDetail in _scenarioDetailRepository.GetAll().ToList()
                                .Where(mapping => mapping.ProjectId == projectCal.Id && mapping.ScenarioId == scenario.Id && mapping.IsDelete == false)
                                select scenarioDetail;
                projects = projects3.ToList();
                scenarioProject.ScenarioId = scenario.Id;
                scenarioProject.ProjectId = projects.Select(c => c.ProjectId).Distinct().ToArray();
            }
            else
            {
                var projects2 = from scenarioCal in _scenarioCalculationRepository.GetAll().ToList()
                                .Where(mapping => mapping.ScenarioId == scenario.Id)
                                from projectCal in _projectCalculationRepository.GetAll().ToList()
                                .Where(mapping => mapping.ScenarioCalculationId == scenarioCal.Id)
                                from scenarioDetail in _scenarioDetailRepository.GetAll().ToList()
                                .Where(mapping => mapping.ProjectId == projectCal.ProjectId && mapping.ScenarioId == scenario.Id && mapping.IsDelete == false)
                                select scenarioDetail;
                projects = projects2.ToList();
                scenarioProject.ScenarioId = scenario.Id;
                scenarioProject.ProjectId = projects.Select(c => c.ProjectId).Distinct().ToArray();
            }

            var yearCollection = _calculationService.GenerateYear(scenarioProject);
            CalculationResult calculationResult = new CalculationResult();
            calculationResult.CollectionYears = yearCollection.ToArray();

            IList<UndiversifiedRiskCapitalProjectCollection> undiversifiedRiskCapitalProjectCollection = new List<UndiversifiedRiskCapitalProjectCollection>();
            AggregationOfProject aggregationOfProject = new AggregationOfProject();

            IList<ProjectLite> projectCollection = new List<ProjectLite>();
            IList<RiskCapitalByProject> projectList = new List<RiskCapitalByProject>();
            IList<AggregationOfProjectCollection> aggregationOfProjectCollections = new List<AggregationOfProjectCollection>();
            if (projects.Count() > 0)
            {
                foreach (var project in projects)
                {
                    AggregationOfProjectCollection aggregationOfProjectCollection = new AggregationOfProjectCollection();
                    aggregationOfProjectCollection.NamaProject = project.Project.NamaProject;
                    aggregationOfProjectCollection.ProjectId = project.Project.Id;
                    aggregationOfProjectCollection.NamaSektor = project.Project.Sektor.NamaSektor;
                    aggregationOfProjectCollection.SektorId = project.Project.SektorId;
                    aggregationOfProjectCollections.Add(aggregationOfProjectCollection);

                    ProjectLite projectLite = new ProjectLite();
                    projectLite.ProjectId = project.Project.Id;
                    projectLite.NamaProject = project.Project.NamaProject;

                    RiskCapitalByProject riskCapitalByProject = new RiskCapitalByProject();
                    riskCapitalByProject.ProjectId = project.Project.Id;
                    riskCapitalByProject.NamaProject = project.Project.NamaProject;

                    projectCollection.Add(projectLite);
                    projectList.Add(riskCapitalByProject);
                }
                aggregationOfProject.AggregationOfProjectCollection = aggregationOfProjectCollections.ToArray();

                foreach (var item in projects)
                {
                    IList<UndiversifiedYearCollection> undiversifiedYearCollection = new List<UndiversifiedYearCollection>();

                    var riskMatrixProject = _riskMatrixProjectRepository.GetByScenarioIdProjectId(scenario.Id, item.ProjectId);
                    Validate.NotNull(riskMatrixProject, "Risk Matrix untuk proyek {0} pada skenario {1} tidak ditemukan / belum diisi.", item.Project.NamaProject, scenario.NamaScenario);

                    var project = item.Project;
                    var stageTahunRiskMatrix = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectId(riskMatrixProject.Id).ToList();

                    if (stageTahunRiskMatrix.Count > 0)
                    {
                        foreach (var dataStage in stageTahunRiskMatrix)
                        {
                            decimal? totalPerYear = 0;

                            UndiversifiedYearCollection undiversifiedYear = new UndiversifiedYearCollection();
                            undiversifiedYear.Year = dataStage.Tahun;

                            IList<YearValue> yearValues = new List<YearValue>();

                            var stageTahunDetail = _stageTahunRiskMatrixDetailRepository.GetByStageTahunRiskMatrixId(dataStage.Id).ToList();
                            if (stageTahunDetail.Count > 0)
                            {
                                foreach (var detailItem in stageTahunDetail)
                                {
                                    decimal? val = 0;
                                    var likehoodFiltered = likelihoodDetail.Where(x => x.Id == detailItem.LikehoodDetailId).FirstOrDefault();
                                    if (likehoodFiltered != null)
                                    {
                                        var nilaiAverage = likehoodFiltered.Average / 100;
                                        Validate.NotNull(nilaiAverage, "Nilai Average pada Risk Matrix dengan skenario {0} proyek {1} Risk Registrasi {2} tahun {3} bernilai null / belum diisi.", scenario.NamaScenario, item.Project.NamaProject, detailItem.RiskRegistrasi.KodeMRisk, detailItem.StageTahunRiskMatrix.Tahun);
                                        var nilaiExpose = detailItem.NilaiExpose;
                                        Validate.NotNull(nilaiExpose, "Nilai Exposure pada Risk Matrix dengan skenario {0} proyek {1} Risk Registrasi {2} tahun {3} bernilai null / belum diisi.", scenario.NamaScenario, item.Project.NamaProject, detailItem.RiskRegistrasi.KodeMRisk, detailItem.StageTahunRiskMatrix.Tahun);

                                        val = nilaiExpose * nilaiAverage;
                                    }
                                    YearValue year = new YearValue();
                                    year.ScenarioId = scenario.Id;
                                    year.RiskRegistrasiId = detailItem.RiskRegistrasiId;
                                    year.LikehoodId = scenario.LikehoodId;
                                    year.ValueUndiversified = val;

                                    totalPerYear += val;

                                    yearValues.Add(year);
                                }
                            }
                            //formating value
                            var getTwoDigitUndiversifiedRiskCapitalTotalDecimal = decimal.Round(totalPerYear.Value, 28);

                            undiversifiedYear.YearValue = yearValues.ToArray();
                            undiversifiedYear.Total = getTwoDigitUndiversifiedRiskCapitalTotalDecimal;
                            undiversifiedYearCollection.Add(undiversifiedYear);
                        }

                        var projectDetail = _calculationService.GetUndiversifiedByProject(item.ProjectId, scenario.Id);
                        undiversifiedRiskCapitalProjectCollection.Add(projectDetail);
                        UndiversifiedRiskCapitalProjectCollection undiversifiedRiskCapitalProject = new UndiversifiedRiskCapitalProjectCollection();
                        undiversifiedRiskCapitalProject.ProjectId = project.Id;
                        undiversifiedRiskCapitalProject.NamaProject = project.NamaProject;
                        undiversifiedRiskCapitalProject.SektorId = project.SektorId;
                        undiversifiedRiskCapitalProject.NamaSektor = project.Sektor.NamaSektor;
                        undiversifiedRiskCapitalProject.UndiversifiedYearCollection = undiversifiedYearCollection.ToArray();
                        undiversifiedRiskCapitalProject.RiskRegistrasi = riskRegistrasi.ToArray();
                    }
                    else
                    {
                        throw new ApplicationException(string.Format("Risk Matrix untuk proyek {0} pada skenario {1} tidak ditemukan / belum diisi.", item.Project.NamaProject, scenario.NamaScenario));
                    }
                }
            }
            else
            {
                throw new ApplicationException(string.Format("Daftar proyek tidak ditemukan dalam skenario {0}", scenario.NamaScenario));
            }

            calculationResult.ScenarioId = scenario.Id;
            calculationResult.NamaScenario = scenario.NamaScenario;
            calculationResult.ProjectDetail = undiversifiedRiskCapitalProjectCollection.ToArray();


            #region Generate AggregationOfProject
            //get aggregation project undiversified
            var aggregationUndiversifiedProjectCapital = _calculationService.GetAggregationUndiversifiedProjectCapital(yearCollection, calculationResult.ProjectDetail);
            aggregationOfProject.AggregationUndiversifiedProjectCapital = aggregationUndiversifiedProjectCapital.ToArray();

            //get aggregation project intra-diversified
            var aggregationIntraDiversifiedProjectCapital = _calculationService.GetAggregationIntraDiversifiedProjectCapital(yearCollection, calculationResult.ProjectDetail);
            aggregationOfProject.AggregationIntraDiversifiedProjectCapital = aggregationIntraDiversifiedProjectCapital.ToArray();

            //get aggregation project inter-diversified
            var aggregationInterDiversifiedProjectCapital = _calculationService.GetAggregationInterDiversifiedProjectCapital(scenario.Id, yearCollection, aggregationOfProject);
            aggregationOfProject.AggregationInterDiversifiedProjectCapital = aggregationInterDiversifiedProjectCapital.ToArray();
            calculationResult.AggregationOfProject = aggregationOfProject;
            #endregion Generate AggregationOfProject

            #region Generate AggregationOfSektor
            AggregationSektor aggregationSektor = new AggregationSektor();

            //var sektors = _sektorRepository.GetAll().ToList();
            var sektors = aggregationOfProjectCollections.Select(x => new SektorLite { SektorId = x.SektorId, NamaSektor = x.NamaSektor }).GroupBy(x => new { x.SektorId, x.NamaSektor }).Select(x => x.FirstOrDefault()).ToArray();

            //InterDiversified
            var interDiversified = _calculationService.GenerateInterProjectDiversified(scenario, sektors, aggregationOfProjectCollections, aggregationInterDiversifiedProjectCapital);
            #endregion Generate AggregationOfSektor

            #region Generate Asset Projection
            var assetProjection = GenerateAssetProjectionInvestmentStrategy(aggregationOfProject.AggregationInterDiversifiedProjectCapital, yearCollection, scenario, aggregationOfProject);
            #endregion Generate Asset Projection

            //set value by year
            foreach (var item in projectList)
            {
                IList<RiskCapitalByProjectValue> projectValList = new List<RiskCapitalByProjectValue>();
                for (int i = 0; i < yearCollection.Count; i++)
                {
                    var data = aggregationInterDiversifiedProjectCapital.Where(x => x.aggregationYears == yearCollection[i]).FirstOrDefault();
                    var pro = data.AggregationInterDiversifiedProjectCollection.FirstOrDefault(x => x.ProjectId == item.ProjectId);


                    RiskCapitalByProjectValue projectVal = new RiskCapitalByProjectValue();
                    projectVal.ProjectId = item.ProjectId;
                    projectVal.Year = pro.Year;
                    projectVal.Value = Convert.ToDecimal(pro.Total);

                    projectValList.Add(projectVal);
                }

                item.Values = projectValList.ToArray();
            }

            result.ScenarioId = scenario.Id;
            result.NamaScenario = scenario.NamaScenario;
            result.AggregationInterDiversifiedProjectCapital = aggregationInterDiversifiedProjectCapital.ToArray();
            result.Project = projectList.ToArray();
            result.Year = yearCollection.ToArray();
            result.InterProjectDiversifiedAggregationSektor = interDiversified;
            result.AssetClassDefault = assetProjection.ToArray();

            return result;
        }
        
        #region 5. AssetProjection
        public IList<AssetClassDefault> GenerateAssetProjectionInvestmentStrategy(IList<AggregationInterDiversifiedProjectCapital> interProjectDiversified, IList<int> years, Scenario scenario, AggregationOfProject aggregationOfProject)
        {
            #region AssetClassDefault
            var PMNCurrentYear = (_pmnRepository.GetAll().ToList()).Where(x => x.PMNToModalDasarCap == 2017).FirstOrDefault();
            decimal valuePMNCurrent;
            if (PMNCurrentYear != null)
            {
                valuePMNCurrent = PMNCurrentYear.ValuePMNToModalDasarCap;
            }
            else
            {
                throw new ApplicationException(string.Format("Tahun PMN untuk modal dasar cap {0} pada master data tidak ditemukan.", DateTime.Now.Year));
            }

            IList<AssetClassDefault> assetClassDefaults = new List<AssetClassDefault>();

            var asset = _assetDataRepository.GetAll();
            if (asset.Count() > 0)
            {
                foreach (var item in asset)
                {
                    AssetClassDefault assetClassDefault = new AssetClassDefault();
                    assetClassDefault.AssetClass = item.AssetClass;
                    assetClassDefault.Proportion = item.Porpotion;
                    assetClassDefault.TermAwal = item.TermAwal;
                    assetClassDefault.TermAkhir = item.TermAkhir;
                    assetClassDefault.AssumedReturn = item.AssumedReturn;
                    assetClassDefault.OutstandingYearAwal = item.OutstandingStartYears;
                    assetClassDefault.OutstandingYearAkhir = item.OutstandingEndYears;
                    //calculate AssetValue
                    assetClassDefault.AssetValue = item.AssetValue + valuePMNCurrent * item.Porpotion / 100;
                    assetClassDefaults.Add(assetClassDefault);
                }
            }
            #endregion AssetClassDefault

            return assetClassDefaults;

        }
        #endregion 5. AssetProjection

        public ReportMainDashboard GetReportMainDashboard()
        {
            ReportMainDashboard result = new ReportMainDashboard();

            var scenario = _scenarioRepository.GetDefault();
            Validate.NotNull(scenario, "Skenario default tidak ditemukan. Silakan set default salah satu Skenario dahulu.");

            #region Haris
            var scenarioList = _scenarioCalculationRepository.GetAll().ToList();
            List<int> otherScenario = new List<int>();

            foreach (var item in scenarioList)
            {
                if (item.ScenarioId != scenario.Id)
                {
                    otherScenario.Add(item.ScenarioId);
                }
            }

            if (otherScenario.Count < 2)
            {
                throw new ApplicationException(string.Format("Data kalkulasi skenario tidak ditemukan."));
            }
            var scenario1 = _scenarioRepository.Get(otherScenario[1]);
            var scenario2 = _scenarioRepository.Get(otherScenario[0]);
            var yearNow = DateTimeOffset.Now.Year.ToString();
            //var yearNow = "2017";
            var dataResult = _resultService.GetAllDataResult(scenario.Id, 0, 0, 0);
            IList<Matrix> matrixs = _matrixRepository.GetAll().ToList();
            IList<FunctionalRisk> functionalRisks = _functionalRiskRepository.GetAll(null, 0).ToList();
            List<FunctionalRiskResult> functionalRiskResultList = new List<FunctionalRiskResult>();
            List<FunctionalRiskDashboard> functionalRiskDashboardList = new List<FunctionalRiskDashboard>();
            //Functional Risk Result
            foreach (var matrixItem in matrixs.ToList())
            {
                List<FunctionalRiskScenarioResult> functionalRiskScenarioResultList = new List<FunctionalRiskScenarioResult>();
                //item 1, 2, 3, 4
                if (matrixItem.Id == 1 || matrixItem.Id == 2 || matrixItem.Id == 3 || matrixItem.Id == 4)
                {
                    FunctionalRiskResult functionalRiskResult = new FunctionalRiskResult();
                    functionalRiskResult.MatrixId = matrixItem.Id;
                    functionalRiskResult.NamaMatrix = matrixItem.NamaMatrix;
                    functionalRiskResult.NamaFormula = matrixItem.NamaFormula;
                    var data1 = Convert.ToDecimal(0);

                    //getScenario 1 calculation
                    if (matrixItem.Id == 1)
                    {
                        foreach (var item2 in dataResult.LeverageRatio.LeverageItem[4].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                                functionalRiskScenarioResult.Scenario = scenario.NamaScenario;
                                var ValueConvert = Math.Round(item2.Value, 2, MidpointRounding.AwayFromZero);
                                functionalRiskScenarioResult.Calculations = Convert.ToString(ValueConvert);
                                var validateColor = _functionalRiskRepository.GetYellowByMatrix(matrixItem.Id, scenario.Id);
                                Validate.NotNull(validateColor, "Functional Risk untuk skenario " + scenario.NamaScenario + " belum ada. Harap diisi terlebih dahulu di Menu Master Ambang Batas");

                                //getColorComment
                                ValidateCollorComment(functionalRiskScenarioResult, validateColor, matrixItem.Id, scenario.Id, ValueConvert);
                                functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                            }
                        }
                    }
                    if (matrixItem.Id == 2)
                    {
                        foreach (var item2 in dataResult.LeverageRatio.LeverageItem[5].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                                functionalRiskScenarioResult.Scenario = scenario.NamaScenario;
                                var ValueConvert = Math.Round(item2.Value, 2, MidpointRounding.AwayFromZero);
                                functionalRiskScenarioResult.Calculations = Convert.ToString(ValueConvert);
                                var validateColor = _functionalRiskRepository.GetYellowByMatrix(matrixItem.Id, scenario.Id);

                                //getColorComment
                                ValidateCollorComment(functionalRiskScenarioResult, validateColor, matrixItem.Id, scenario.Id, ValueConvert);
                                functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                            }
                        }
                    }
                    if (matrixItem.Id == 3)
                    {
                        foreach (var item2 in dataResult.LeverageRatio.LeverageItem[6].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                                functionalRiskScenarioResult.Scenario = scenario.NamaScenario;
                                var ValueConvert = Math.Round(item2.Value, 2, MidpointRounding.AwayFromZero);
                                functionalRiskScenarioResult.Calculations = Convert.ToString(ValueConvert);
                                var validateColor = _functionalRiskRepository.GetYellowByMatrix(matrixItem.Id, scenario.Id);

                                //getColorComment
                                ValidateCollorComment(functionalRiskScenarioResult, validateColor, matrixItem.Id, scenario.Id, ValueConvert);
                                functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                            }
                        }
                    }
                    if (matrixItem.Id == 4)
                    {
                        FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                        functionalRiskScenarioResult.Scenario = scenario.NamaScenario;
                        var data2 = Convert.ToDecimal(0);
                        foreach (var item2 in dataResult.AvailableVSRiskCapital.AvailableVSRiskCapitalCollection[0].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                data1 = item2.Value;
                            }
                        }
                        foreach (var item2 in dataResult.AvailableVSRiskCapital.AvailableVSRiskCapitalCollection[2].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                data2 = item2.Value;
                            }
                        }
                        if (data2 == 0 || data2 == null)
                        {
                            data2 = 1;
                        }
                        var nilai = data1 / data2;
                        var ValueConvert = Math.Round(nilai, 2, MidpointRounding.AwayFromZero);
                        functionalRiskScenarioResult.Calculations = Convert.ToString(ValueConvert);
                        var validateColor = _functionalRiskRepository.GetYellowByMatrix(matrixItem.Id, scenario.Id);
                        Validate.NotNull(validateColor, "Functional Risk untuk skenario " + scenario.NamaScenario + " belum ada. Harap diisi terlebih dahulu di Menu Master Ambang Batas");
                        //getColorComment
                        ValidateCollorComment(functionalRiskScenarioResult, validateColor, matrixItem.Id, scenario.Id, ValueConvert);
                        functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                    }

                    //getScenario 2 & 3 calculation
                    if (matrixItem.Id == 1)
                    {
                        foreach (var item2 in dataResult.AvailableVSRiskCapital.AvailableVSRiskCapitalCollection[1].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                data1 = item2.Value;
                            }
                        }
                    }

                    if (matrixItem.Id == 2)
                    {
                        foreach (var item2 in dataResult.AvailableVSRiskCapital.AvailableVSRiskCapitalCollection[3].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                data1 = item2.Value;
                            }
                        }
                    }

                    if (matrixItem.Id == 3 || matrixItem.Id == 4)
                    {
                        foreach (var item2 in dataResult.AvailableVSRiskCapital.AvailableVSRiskCapitalCollection[0].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                data1 = item2.Value;
                            }
                        }
                    }

                    foreach (var item2 in dataResult.SensitivityStressTesting.StressTestingResult.RiskCapitalCollection.ToList())
                    {
                        if (item2.NamaScenario == scenario1.NamaScenario)
                        {
                            FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                            functionalRiskScenarioResult.Scenario = scenario1.NamaScenario;
                            foreach (var item3 in item2.Values.ToList())
                            {
                                if (item3.Year.ToString() == yearNow)
                                {
                                    var data2 = item3.Value;
                                    if (data2 == 0 || data2 == null)
                                    {
                                        data2 = 1;
                                    }
                                    var nilai = data1 / data2;
                                    var ValueConvert = Math.Round(nilai, 2, MidpointRounding.AwayFromZero);
                                    functionalRiskScenarioResult.Calculations = Convert.ToString(ValueConvert);
                                    var validateColor = _functionalRiskRepository.GetYellowByMatrix(matrixItem.Id, scenario1.Id);
                                    Validate.NotNull(validateColor, "Functional Risk untuk skenario " + scenario1.NamaScenario + " belum ada. Harap diisi terlebih dahulu di Menu Master Ambang Batas.");
                                    //getColorComment
                                    ValidateCollorComment(functionalRiskScenarioResult, validateColor, matrixItem.Id, scenario1.Id, ValueConvert);

                                    functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                                }
                            }

                        }
                        if (item2.NamaScenario == scenario2.NamaScenario)
                        {
                            FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                            functionalRiskScenarioResult.Scenario = scenario2.NamaScenario;
                            foreach (var item3 in item2.Values.ToList())
                            {
                                if (item3.Year.ToString() == yearNow)
                                {
                                    var data2 = item3.Value;
                                    if (data2 == 0 || data2 == null)
                                    {
                                        data2 = 1;
                                    }
                                    var nilai = data1 / data2;
                                    var ValueConvert = Math.Round(nilai, 2, MidpointRounding.AwayFromZero);
                                    functionalRiskScenarioResult.Calculations = Convert.ToString(ValueConvert);
                                    var validateColor = _functionalRiskRepository.GetYellowByMatrix(matrixItem.Id, scenario2.Id);
                                    Validate.NotNull(validateColor, "Functional Risk untuk skenario " + scenario2.NamaScenario + " belum ada. Harap diisi terlebih dahulu di Menu Master Ambang Batas.");
                                    //getColorComment
                                    ValidateCollorComment(functionalRiskScenarioResult, validateColor, matrixItem.Id, scenario2.Id, ValueConvert);

                                    functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                                }
                            }
                        }
                    }

                    functionalRiskResult.Scenarios = functionalRiskScenarioResultList;
                    functionalRiskResultList.Add(functionalRiskResult);
                }

                if (matrixItem.Id == 5)
                {
                    FunctionalRiskResult functionalRiskResult = new FunctionalRiskResult();
                    functionalRiskResult.MatrixId = matrixItem.Id;
                    functionalRiskResult.NamaMatrix = matrixItem.NamaMatrix;
                    functionalRiskResult.NamaFormula = matrixItem.NamaFormula;
                    var data1 = Convert.ToDecimal(0);

                    //getScenario 1 calculation
                    if (matrixItem.Id == 5)
                    {
                        foreach (var item2 in dataResult.RiskBudget.RiskBudgetBySektor.RiskBudgetCollectionBySektor[0].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                                functionalRiskScenarioResult.Scenario = scenario.NamaScenario;
                                var ValueConvert = Math.Round(item2.Value, 2, MidpointRounding.AwayFromZero);
                                functionalRiskScenarioResult.Calculations = Convert.ToString(ValueConvert) + "%";
                                var validateColor = _functionalRiskRepository.GetYellowByMatrix(matrixItem.Id, scenario.Id);

                                //getColorComment
                                ValidateCollorComment2(functionalRiskScenarioResult, validateColor, matrixItem.Id, scenario.Id, ValueConvert);
                                functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                            }
                        }
                    }
                    //getScenario 2 calculation
                    if (matrixItem.Id == 5)
                    {
                        FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                        functionalRiskScenarioResult.Scenario = scenario1.NamaScenario;
                        functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                    }
                    //getScenario 3 calculation
                    if (matrixItem.Id == 5)
                    {
                        FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                        functionalRiskScenarioResult.Scenario = scenario2.NamaScenario;
                        functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                    }

                    functionalRiskResult.Scenarios = functionalRiskScenarioResultList;
                    functionalRiskResultList.Add(functionalRiskResult);
                }

                if (matrixItem.Id == 6)
                {
                    FunctionalRiskResult functionalRiskResult = new FunctionalRiskResult();
                    functionalRiskResult.MatrixId = matrixItem.Id;
                    functionalRiskResult.NamaMatrix = matrixItem.NamaMatrix;
                    functionalRiskResult.NamaFormula = matrixItem.NamaFormula;
                    var data1 = Convert.ToDecimal(0);

                    //getScenario 1 calculation
                    if (matrixItem.Id == 6)
                    {
                        foreach (var item2 in dataResult.RiskBudget.RiskBudgetByProject.RiskBudgetCollection[0].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                                functionalRiskScenarioResult.Scenario = scenario.NamaScenario;
                                var ValueConvert = Math.Round(item2.Value, 2, MidpointRounding.AwayFromZero);
                                functionalRiskScenarioResult.Calculations = Convert.ToString(ValueConvert) + "%";
                                var validateColor = _functionalRiskRepository.GetYellowByMatrix(matrixItem.Id, scenario.Id);

                                //getColorComment
                                ValidateCollorComment2(functionalRiskScenarioResult, validateColor, matrixItem.Id, scenario.Id, ValueConvert);
                                functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                            }
                        }
                    }
                    //getScenario 2 calculation
                    if (matrixItem.Id == 6)
                    {
                        FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                        functionalRiskScenarioResult.Scenario = scenario1.NamaScenario;
                        functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                    }
                    //getScenario 3 calculation
                    if (matrixItem.Id == 6)
                    {
                        FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                        functionalRiskScenarioResult.Scenario = scenario2.NamaScenario;
                        functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                    }

                    functionalRiskResult.Scenarios = functionalRiskScenarioResultList;
                    functionalRiskResultList.Add(functionalRiskResult);
                }

                if (matrixItem.Id == 7)
                {
                    FunctionalRiskResult functionalRiskResult = new FunctionalRiskResult();
                    functionalRiskResult.MatrixId = matrixItem.Id;
                    functionalRiskResult.NamaMatrix = matrixItem.NamaMatrix;
                    functionalRiskResult.NamaFormula = matrixItem.NamaFormula;
                    var data1 = Convert.ToDecimal(0);

                    //getScenario 1 calculation
                    if (matrixItem.Id == 7)
                    {
                        foreach (var item2 in dataResult.RiskBudget.RiskBudgetByRisk.RiskBudgetCollectionByRisk[0].Values.ToList())
                        {
                            if (item2.Year.ToString() == yearNow)
                            {
                                FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                                functionalRiskScenarioResult.Scenario = scenario.NamaScenario;
                                var ValueConvert = Math.Round(item2.Value, 2, MidpointRounding.AwayFromZero);
                                functionalRiskScenarioResult.Calculations = Convert.ToString(ValueConvert) + "%";
                                var validateColor = _functionalRiskRepository.GetYellowByMatrix(matrixItem.Id, scenario.Id);

                                //getColorComment
                                ValidateCollorComment2(functionalRiskScenarioResult, validateColor, matrixItem.Id, scenario.Id, ValueConvert);
                                functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                            }
                        }
                    }
                    //getScenario 2 calculation
                    if (matrixItem.Id == 7)
                    {
                        FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                        functionalRiskScenarioResult.Scenario = scenario1.NamaScenario;
                        functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                    }
                    //getScenario 3 calculation
                    if (matrixItem.Id == 7)
                    {
                        FunctionalRiskScenarioResult functionalRiskScenarioResult = new FunctionalRiskScenarioResult();
                        functionalRiskScenarioResult.Scenario = scenario2.NamaScenario;
                        functionalRiskScenarioResultList.Add(functionalRiskScenarioResult);
                    }

                    functionalRiskResult.Scenarios = functionalRiskScenarioResultList;
                    functionalRiskResultList.Add(functionalRiskResult);
                }
            }
            result.FunctionalRiskResultTableResults = functionalRiskResultList;

            //Functional Risk Dashboard
            foreach (var matrixItem in matrixs.ToList())
            {
                List<FunctionalRiskColorDashboard> functionalRiskColorDashboardList = new List<FunctionalRiskColorDashboard>();
                if (matrixItem.Id != 0)
                {
                    FunctionalRiskDashboard functionalRiskDashboard = new FunctionalRiskDashboard();
                    functionalRiskDashboard.MatrixId = matrixItem.Id;
                    functionalRiskDashboard.NamaMatrix = matrixItem.NamaMatrix;
                    functionalRiskDashboard.NamaFormula = matrixItem.NamaFormula;
                    var data1 = Convert.ToDecimal(0);
                    if (matrixItem.Id != 0)
                    {
                        List<FunctionalRiskScenarioDashboard> functionalRiskScenarioDashboardList = new List<FunctionalRiskScenarioDashboard>();
                        FunctionalRiskColorDashboard functionalRiskColorDashboard = new FunctionalRiskColorDashboard();
                        functionalRiskColorDashboard.Color = "Green";
                        if (matrixItem.Id != 0)
                        {
                            var validateDefinisi = _functionalRiskRepository.GetCommentByColor(matrixItem.Id, scenario.Id, 1);
                            FunctionalRiskScenarioDashboard functionalRiskScenarioDashboard = new FunctionalRiskScenarioDashboard();
                            functionalRiskScenarioDashboard.Scenario = scenario.NamaScenario;
                            functionalRiskScenarioDashboard.Definisi = validateDefinisi.Definisi;
                            functionalRiskScenarioDashboardList.Add(functionalRiskScenarioDashboard);
                        }
                        if (matrixItem.Id != 0)
                        {
                            var validateDefinisi = _functionalRiskRepository.GetCommentByColor(matrixItem.Id, scenario1.Id, 1);
                            FunctionalRiskScenarioDashboard functionalRiskScenarioDashboard = new FunctionalRiskScenarioDashboard();
                            functionalRiskScenarioDashboard.Scenario = scenario1.NamaScenario;
                            functionalRiskScenarioDashboard.Definisi = validateDefinisi.Definisi;
                            functionalRiskScenarioDashboardList.Add(functionalRiskScenarioDashboard);
                        }
                        if (matrixItem.Id != 0)
                        {
                            var validateDefinisi = _functionalRiskRepository.GetCommentByColor(matrixItem.Id, scenario2.Id, 1);
                            FunctionalRiskScenarioDashboard functionalRiskScenarioDashboard = new FunctionalRiskScenarioDashboard();
                            functionalRiskScenarioDashboard.Scenario = scenario2.NamaScenario;
                            functionalRiskScenarioDashboard.Definisi = validateDefinisi.Definisi;
                            functionalRiskScenarioDashboardList.Add(functionalRiskScenarioDashboard);
                        }
                        functionalRiskColorDashboard.Scenarios = functionalRiskScenarioDashboardList;
                        functionalRiskColorDashboardList.Add(functionalRiskColorDashboard);
                    }
                    if (matrixItem.Id != 0)
                    {
                        List<FunctionalRiskScenarioDashboard> functionalRiskScenarioDashboardList = new List<FunctionalRiskScenarioDashboard>();
                        FunctionalRiskColorDashboard functionalRiskColorDashboard = new FunctionalRiskColorDashboard();
                        functionalRiskColorDashboard.Color = "Yellow";
                        if (matrixItem.Id != 0)
                        {
                            var validateDefinisi = _functionalRiskRepository.GetCommentByColor(matrixItem.Id, scenario.Id, 2);
                            FunctionalRiskScenarioDashboard functionalRiskScenarioDashboard = new FunctionalRiskScenarioDashboard();
                            functionalRiskScenarioDashboard.Scenario = scenario.NamaScenario;
                            functionalRiskScenarioDashboard.Definisi = validateDefinisi.Definisi;
                            functionalRiskScenarioDashboardList.Add(functionalRiskScenarioDashboard);
                        }
                        if (matrixItem.Id != 0)
                        {
                            var validateDefinisi = _functionalRiskRepository.GetCommentByColor(matrixItem.Id, scenario1.Id, 2);
                            FunctionalRiskScenarioDashboard functionalRiskScenarioDashboard = new FunctionalRiskScenarioDashboard();
                            functionalRiskScenarioDashboard.Scenario = scenario1.NamaScenario;
                            functionalRiskScenarioDashboard.Definisi = validateDefinisi.Definisi;
                            functionalRiskScenarioDashboardList.Add(functionalRiskScenarioDashboard);
                        }
                        if (matrixItem.Id != 0)
                        {
                            var validateDefinisi = _functionalRiskRepository.GetCommentByColor(matrixItem.Id, scenario2.Id, 2);
                            FunctionalRiskScenarioDashboard functionalRiskScenarioDashboard = new FunctionalRiskScenarioDashboard();
                            functionalRiskScenarioDashboard.Scenario = scenario2.NamaScenario;
                            functionalRiskScenarioDashboard.Definisi = validateDefinisi.Definisi;
                            functionalRiskScenarioDashboardList.Add(functionalRiskScenarioDashboard);
                        }
                        functionalRiskColorDashboard.Scenarios = functionalRiskScenarioDashboardList;
                        functionalRiskColorDashboardList.Add(functionalRiskColorDashboard);
                    }
                    if (matrixItem.Id != 0)
                    {
                        List<FunctionalRiskScenarioDashboard> functionalRiskScenarioDashboardList = new List<FunctionalRiskScenarioDashboard>();
                        FunctionalRiskColorDashboard functionalRiskColorDashboard = new FunctionalRiskColorDashboard();
                        functionalRiskColorDashboard.Color = "Red";
                        if (matrixItem.Id != 0)
                        {
                            var validateDefinisi = _functionalRiskRepository.GetCommentByColor(matrixItem.Id, scenario.Id, 3);
                            FunctionalRiskScenarioDashboard functionalRiskScenarioDashboard = new FunctionalRiskScenarioDashboard();
                            functionalRiskScenarioDashboard.Scenario = scenario.NamaScenario;
                            functionalRiskScenarioDashboard.Definisi = validateDefinisi.Definisi;
                            functionalRiskScenarioDashboardList.Add(functionalRiskScenarioDashboard);
                        }
                        if (matrixItem.Id != 0)
                        {
                            var validateDefinisi = _functionalRiskRepository.GetCommentByColor(matrixItem.Id, scenario1.Id, 3);
                            FunctionalRiskScenarioDashboard functionalRiskScenarioDashboard = new FunctionalRiskScenarioDashboard();
                            functionalRiskScenarioDashboard.Scenario = scenario1.NamaScenario;
                            functionalRiskScenarioDashboard.Definisi = validateDefinisi.Definisi;
                            functionalRiskScenarioDashboardList.Add(functionalRiskScenarioDashboard);
                        }
                        if (matrixItem.Id != 0)
                        {
                            var validateDefinisi = _functionalRiskRepository.GetCommentByColor(matrixItem.Id, scenario2.Id, 3);
                            FunctionalRiskScenarioDashboard functionalRiskScenarioDashboard = new FunctionalRiskScenarioDashboard();
                            functionalRiskScenarioDashboard.Scenario = scenario2.NamaScenario;
                            functionalRiskScenarioDashboard.Definisi = validateDefinisi.Definisi;
                            functionalRiskScenarioDashboardList.Add(functionalRiskScenarioDashboard);
                        }
                        functionalRiskColorDashboard.Scenarios = functionalRiskScenarioDashboardList;
                        functionalRiskColorDashboardList.Add(functionalRiskColorDashboard);
                    }
                    functionalRiskDashboard.Colors = functionalRiskColorDashboardList;
                    functionalRiskDashboardList.Add(functionalRiskDashboard);
                }
            }
            result.FunctionalRiskMainDashboard = functionalRiskDashboardList;
            #endregion Haris

            var likelihoodDetail = scenario.Likehood.LikehoodDetails;
            var riskRegistrasi = _riskRegistrasiRepository.GetAll().ToList();
            //var projects = scenario.ScenarioDetail.Where(x => x.IsDelete == false).ToList();
            //var projects = _projectRepository.GetAll().Where(x => x.IsActive == true && x.Tahapan.NamaTahapan == "Monitoring" && x.IsDelete == false)    // your starting point - table in the "from" statement
            //.Join(scenario.ScenarioDetail.Where(x => x.IsDelete == false), // the source table of the inner join
            //proj => proj.Id,        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
            //scdet => scdet.ProjectId,   // Select the foreign key (the second part of the "on" clause)
            //(proj, scdet) => scdet).ToList(); // selection
            var scenarioCalculation = _scenarioCalculationRepository.GetAll().ToList();

            IList<ScenarioDetail> projects = new List<ScenarioDetail>();
            var listPro = new int[0];
            var scenarioDefaultExist = scenarioCalculation.Where(x => x.ScenarioId == scenario.Id).SingleOrDefault();
            ScenarioProject scenarioProject = new ScenarioProject();
            if (scenarioDefaultExist == null)
            {
                //listPro = _projectRepository.GetAll().Where(x => x.IsActive == true && x.IsDelete == false)    // your starting point - table in the "from" statement
                //            .Join(scenario.ScenarioDetail.Where(x => x.IsDelete == false), // the source table of the inner join
                //            proj => proj.Id,        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
                //            scdet => scdet.ProjectId,   // Select the foreign key (the second part of the "on" clause)
                //            (proj, scdet) => scdet.ProjectId).ToArray(); // selection

                var projects3 = from projectCal in _projectRepository.GetAll().ToList()
                               .Where(mapping => mapping.IsActive == true && mapping.IsDelete == false)
                                from scenarioDetail in _scenarioDetailRepository.GetAll().ToList()
                                .Where(mapping => mapping.ProjectId == projectCal.Id && mapping.ScenarioId == scenario.Id && mapping.IsDelete == false)
                                select scenarioDetail;
                projects = projects3.ToList();
                scenarioProject.ScenarioId = scenario.Id;
                scenarioProject.ProjectId = projects.Select(c => c.ProjectId).Distinct().ToArray();
            }
            else
            {
                var projects2 = from scenarioCal in _scenarioCalculationRepository.GetAll().ToList()
                                .Where(mapping => mapping.ScenarioId == scenario.Id)
                                from projectCal in _projectCalculationRepository.GetAll().ToList()
                                .Where(mapping => mapping.ScenarioCalculationId == scenarioCal.Id)
                                from scenarioDetail in _scenarioDetailRepository.GetAll().ToList()
                                .Where(mapping => mapping.ProjectId == projectCal.ProjectId && mapping.ScenarioId == scenario.Id && mapping.IsDelete == false)
                                select scenarioDetail;
                projects = projects2.ToList();
                scenarioProject.ScenarioId = scenario.Id;
                scenarioProject.ProjectId = projects.Select(c => c.ProjectId).Distinct().ToArray();
            }

            var yearCollection = _calculationService.GenerateYear(scenarioProject);
            CalculationResult calculationResult = new CalculationResult();
            calculationResult.CollectionYears = yearCollection.ToArray();

            IList<UndiversifiedRiskCapitalProjectCollection> undiversifiedRiskCapitalProjectCollection = new List<UndiversifiedRiskCapitalProjectCollection>();
            AggregationOfProject aggregationOfProject = new AggregationOfProject();

            IList<ProjectLite> projectCollection = new List<ProjectLite>();
            IList<RiskCapitalByProject> projectList = new List<RiskCapitalByProject>();
            IList<AggregationOfProjectCollection> aggregationOfProjectCollections = new List<AggregationOfProjectCollection>();
            if (projects.Count > 0)
            {
                foreach (var project in projects)
                {
                    AggregationOfProjectCollection aggregationOfProjectCollection = new AggregationOfProjectCollection();
                    aggregationOfProjectCollection.NamaProject = project.Project.NamaProject;
                    aggregationOfProjectCollection.ProjectId = project.Project.Id;
                    aggregationOfProjectCollection.NamaSektor = project.Project.Sektor.NamaSektor;
                    aggregationOfProjectCollection.SektorId = project.Project.SektorId;
                    aggregationOfProjectCollections.Add(aggregationOfProjectCollection);

                    ProjectLite projectLite = new ProjectLite();
                    projectLite.ProjectId = project.Project.Id;
                    projectLite.NamaProject = project.Project.NamaProject;

                    RiskCapitalByProject riskCapitalByProject = new RiskCapitalByProject();
                    riskCapitalByProject.ProjectId = project.Project.Id;
                    riskCapitalByProject.NamaProject = project.Project.NamaProject;

                    projectCollection.Add(projectLite);
                    projectList.Add(riskCapitalByProject);
                }
                aggregationOfProject.AggregationOfProjectCollection = aggregationOfProjectCollections.ToArray();

                foreach (var item in projects)
                {
                    IList<UndiversifiedYearCollection> undiversifiedYearCollection = new List<UndiversifiedYearCollection>();

                    var riskMatrixProject = _riskMatrixProjectRepository.GetByScenarioIdProjectId(scenario.Id, item.ProjectId);
                    Validate.NotNull(riskMatrixProject, "Risk Matrix untuk proyek {0} pada skenario {1} tidak ditemukan / belum diisi.", item.Project.NamaProject, scenario.NamaScenario);

                    var project = item.Project;
                    var stageTahunRiskMatrix = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectId(riskMatrixProject.Id).ToList();

                    if (stageTahunRiskMatrix.Count > 0)
                    {
                        foreach (var dataStage in stageTahunRiskMatrix)
                        {
                            decimal? totalPerYear = 0;

                            UndiversifiedYearCollection undiversifiedYear = new UndiversifiedYearCollection();
                            undiversifiedYear.Year = dataStage.Tahun;

                            IList<YearValue> yearValues = new List<YearValue>();

                            var stageTahunDetail = _stageTahunRiskMatrixDetailRepository.GetByStageTahunRiskMatrixId(dataStage.Id).ToList();
                            if (stageTahunDetail.Count > 0)
                            {
                                foreach (var detailItem in stageTahunDetail)
                                {
                                    decimal? val = 0;
                                    var likehoodFiltered = likelihoodDetail.Where(x => x.Id == detailItem.LikehoodDetailId).FirstOrDefault();
                                    if (likehoodFiltered != null)
                                    {
                                        var nilaiAverage = likehoodFiltered.Average / 100;
                                        Validate.NotNull(nilaiAverage, "Nilai Average pada Risk Matrix dengan skenario {0} proyek {1} Risk Registrasi {2} tahun {3} bernilai null / belum diisi.", scenario.NamaScenario, item.Project.NamaProject, detailItem.RiskRegistrasi.KodeMRisk, detailItem.StageTahunRiskMatrix.Tahun);
                                        var nilaiExpose = detailItem.NilaiExpose;
                                        Validate.NotNull(nilaiExpose, "Nilai Exposure pada Risk Matrix dengan skenario {0} proyek {1} Risk Registrasi {2} tahun {3} bernilai null / belum diisi.", scenario.NamaScenario, item.Project.NamaProject, detailItem.RiskRegistrasi.KodeMRisk, detailItem.StageTahunRiskMatrix.Tahun);

                                        val = nilaiExpose * nilaiAverage;
                                    }
                                    YearValue year = new YearValue();
                                    year.ScenarioId = scenario.Id;
                                    year.RiskRegistrasiId = detailItem.RiskRegistrasiId;
                                    year.LikehoodId = scenario.LikehoodId;
                                    year.ValueUndiversified = val;

                                    totalPerYear += val;

                                    yearValues.Add(year);
                                }
                            }
                            //formating value
                            var getTwoDigitUndiversifiedRiskCapitalTotalDecimal = decimal.Round(totalPerYear.Value, 28);

                            undiversifiedYear.YearValue = yearValues.ToArray();
                            undiversifiedYear.Total = getTwoDigitUndiversifiedRiskCapitalTotalDecimal;
                            undiversifiedYearCollection.Add(undiversifiedYear);
                        }

                        var projectDetail = _calculationService.GetUndiversifiedByProject(item.ProjectId, scenario.Id);
                        undiversifiedRiskCapitalProjectCollection.Add(projectDetail);
                        UndiversifiedRiskCapitalProjectCollection undiversifiedRiskCapitalProject = new UndiversifiedRiskCapitalProjectCollection();
                        undiversifiedRiskCapitalProject.ProjectId = project.Id;
                        undiversifiedRiskCapitalProject.NamaProject = project.NamaProject;
                        undiversifiedRiskCapitalProject.SektorId = project.SektorId;
                        undiversifiedRiskCapitalProject.NamaSektor = project.Sektor.NamaSektor;
                        undiversifiedRiskCapitalProject.UndiversifiedYearCollection = undiversifiedYearCollection.ToArray();
                        undiversifiedRiskCapitalProject.RiskRegistrasi = riskRegistrasi.ToArray();
                    }
                    else
                    {
                        throw new ApplicationException(string.Format("Risk Matrix untuk proyek {0} pada skenario {1} tidak ditemukan / belum diisi.", item.Project.NamaProject, scenario.NamaScenario));
                    }
                }
            }
            else
            {
                throw new ApplicationException(string.Format("Daftar proyek tidak ditemukan dalam skenario {0}", scenario.NamaScenario));
            }

            calculationResult.ScenarioId = scenario.Id;
            calculationResult.NamaScenario = scenario.NamaScenario;
            calculationResult.ProjectDetail = undiversifiedRiskCapitalProjectCollection.ToArray();


            #region Generate AggregationOfProject
            //get aggregation project undiversified
            var aggregationUndiversifiedProjectCapital = _calculationService.GetAggregationUndiversifiedProjectCapital(yearCollection, calculationResult.ProjectDetail);
            aggregationOfProject.AggregationUndiversifiedProjectCapital = aggregationUndiversifiedProjectCapital.ToArray();

            //get aggregation project intra-diversified
            var aggregationIntraDiversifiedProjectCapital = _calculationService.GetAggregationIntraDiversifiedProjectCapital(yearCollection, calculationResult.ProjectDetail);
            aggregationOfProject.AggregationIntraDiversifiedProjectCapital = aggregationIntraDiversifiedProjectCapital.ToArray();

            //get aggregation project inter-diversified
            var aggregationInterDiversifiedProjectCapital = _calculationService.GetAggregationInterDiversifiedProjectCapital(scenario.Id, yearCollection, aggregationOfProject);
            aggregationOfProject.AggregationInterDiversifiedProjectCapital = aggregationInterDiversifiedProjectCapital.ToArray();
            calculationResult.AggregationOfProject = aggregationOfProject;
            #endregion Generate AggregationOfProject

            #region Generate AggregationOfSektor
            AggregationSektor aggregationSektor = new AggregationSektor();

            //var sektors = _sektorRepository.GetAll().ToList();
            var sektors = aggregationOfProjectCollections.Select(x => new SektorLite { SektorId = x.SektorId, NamaSektor = x.NamaSektor }).GroupBy(x => new { x.SektorId, x.NamaSektor }).Select(x => x.FirstOrDefault()).ToArray();

            //InterDiversified
            var interDiversified = _calculationService.GenerateInterProjectDiversified(scenario, sektors, aggregationOfProjectCollections, aggregationInterDiversifiedProjectCapital);
            #endregion Generate AggregationOfSektor

            #region Generate Asset Projection
            var assetProjection = GenerateAssetProjectionInvestmentStrategy(aggregationOfProject.AggregationInterDiversifiedProjectCapital, yearCollection, scenario, aggregationOfProject);
            #endregion Generate Asset Projection

            //set value by year
            foreach (var item in projectList)
            {
                IList<RiskCapitalByProjectValue> projectValList = new List<RiskCapitalByProjectValue>();
                for (int i = 0; i < yearCollection.Count; i++)
                {
                    var data = aggregationInterDiversifiedProjectCapital.Where(x => x.aggregationYears == yearCollection[i]).FirstOrDefault();
                    var pro = data.AggregationInterDiversifiedProjectCollection.FirstOrDefault(x => x.ProjectId == item.ProjectId);


                    RiskCapitalByProjectValue projectVal = new RiskCapitalByProjectValue();
                    projectVal.ProjectId = item.ProjectId;
                    projectVal.Year = pro.Year;
                    projectVal.Value = Convert.ToDecimal(pro.Total);

                    projectValList.Add(projectVal);
                }

                item.Values = projectValList.ToArray();
            }

            result.ScenarioId = scenario.Id;
            result.NamaScenario = scenario.NamaScenario;
            result.AggregationInterDiversifiedProjectCapital = aggregationInterDiversifiedProjectCapital.ToArray();
            result.Project = projectList.ToArray();
            result.Year = yearCollection.ToArray();
            result.InterProjectDiversifiedAggregationSektor = interDiversified;
            result.AssetClassDefault = assetProjection.ToArray();
            result.Result = dataResult;

            return result;
        }

    }
}
