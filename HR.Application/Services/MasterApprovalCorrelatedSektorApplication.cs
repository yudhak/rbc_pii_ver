﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;

namespace HR.Application
{
    public class MasterApprovalCorrelatedSektorService : IMasterApprovalCorrelatedSektorService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICommentsRepository _commentsRepository;
        private readonly IColorCommentRepository _colorCommentRepository;
        private readonly IMasterApprovalCorrelatedSektorRepository _masterApprovalCorrelatedSektorRepository;
        private readonly ISektorRepository _sektorRepository;
        private readonly ICorrelatedSektorRepository _correlatedSektorRepository;
        private readonly IAuditLogService _auditLogService;
        private readonly IUserRepository _userRepositoy;
        private readonly IMenuRepository _menuRepositoy;

        public MasterApprovalCorrelatedSektorService(IUnitOfWork uow, ICommentsRepository commentsRepository, IColorCommentRepository colorCommentRepository,
            IMasterApprovalCorrelatedSektorRepository masterApprovalCorrelatedSektorRepository, IAuditLogService auditLogService, IUserRepository userRepository
            , IMenuRepository menuRepositoy, ISektorRepository sektorRepository, ICorrelatedSektorRepository correlatedSektorRepository)
        {
            _unitOfWork = uow;
            _commentsRepository = commentsRepository;
            _colorCommentRepository = colorCommentRepository;
            _masterApprovalCorrelatedSektorRepository = masterApprovalCorrelatedSektorRepository;
            _auditLogService = auditLogService;
            _userRepositoy = userRepository;
            _menuRepositoy = menuRepositoy;
            _sektorRepository = sektorRepository;
            _correlatedSektorRepository = correlatedSektorRepository;
        }

        #region Query
        public IEnumerable<MasterApprovalCorrelatedSektor> GetAll()
        {
            return _masterApprovalCorrelatedSektorRepository.GetAll();
        }

        //public IEnumerable<MasterApprovalCorrelatedSektor> GetAll(string keyword)
        //{
        //    return _masterApprovalCorrelatedSektorRepository.GetAll(keyword);
        //}

        public IEnumerable<MasterApprovalCorrelatedSektor> GetAllBySektorId(int sektorId)
        {
            return _masterApprovalCorrelatedSektorRepository.GetAllBySektorId(sektorId);
        }

        public IEnumerable<MasterApprovalCorrelatedSektor> GetAllByMenuId(int menuId)
        {
            return _masterApprovalCorrelatedSektorRepository.GetAllByMenuId(menuId);
        }

        public MasterApprovalCorrelatedSektor Get(int id)
        {
            return _masterApprovalCorrelatedSektorRepository.Get(id);
        }

        public MasterApprovalCorrelatedSektor GetBySektorIdUserId(int sektorId, int userId)
        {
            return _masterApprovalCorrelatedSektorRepository.GetBySektorIdUserid(sektorId, userId);
        }


        public void IsExistOnEditing(int id, int menuId, int sektorId, int userId, int nomorUrutStatus)
        {
            if (_masterApprovalCorrelatedSektorRepository.IsExist(id, menuId, sektorId, userId, nomorUrutStatus))
            {
                throw new ApplicationException(string.Format("Master Approval Correlated Sektor {0} sudah ada.", id));
            }
        }
  
        #endregion Query

        #region Manipulation 
        public int Add(MasterApprovalCorrelatedSektorParam param)
        {
           // int id = 1;
            Validate.NotNull(param.UserIdList, "User wajib diisi.");

            if (param.UserIdList.Count() != 3)
            {
                throw new ApplicationException(string.Format("User Approval kurang lengkap (Harus 3 User)."));
            }
            if (param.UserIdList.Count() > 3)
            {
                throw new ApplicationException(string.Format("Jumlah maksimum User tidak sesuai (Maksimal 3 User)."));
            }

            IList<User> userlist = new List<User>();

            //int[] nomorUrutStatusList = new int[3];

            int nomorUrutStatus = 1;

            foreach (var item in param.UserIdList)
            {
                var user = _userRepositoy.Get(item.GetValueOrDefault());
                Validate.NotNull(param.UserIdList, "User tidak ditemukan.");
                userlist.Add(user);
            }

            var menu = _menuRepositoy.Get(param.MenuId.GetValueOrDefault());
            Validate.NotNull(menu, "Menu tidak ditemukan.");

            using (_unitOfWork)
            {
                IList<MasterApprovalCorrelatedSektor> masterApprovalCorrelatedSektorList = new List<MasterApprovalCorrelatedSektor>();

                foreach (var item in userlist)
                {
                    MasterApprovalCorrelatedSektor masterApprovalCorrelatedSektor = new MasterApprovalCorrelatedSektor(menu,  item, nomorUrutStatus, param.CreateBy, param.CreateDate);
                    masterApprovalCorrelatedSektorList.Add(masterApprovalCorrelatedSektor);
                    nomorUrutStatus++;
                }
                _masterApprovalCorrelatedSektorRepository.Insert(masterApprovalCorrelatedSektorList);

                _unitOfWork.Commit();
            }

            return menu.Id;
        }

        public int Update(int menuId, MasterApprovalCorrelatedSektorParam param)
        {
            var menu = _menuRepositoy.Get(menuId);
            Validate.NotNull(menu, "Master Approval Scenario dengan menu {0} tidak ditemukan.", menuId);

            var model = this.GetAllByMenuId(menuId);
            Validate.NotNull(model, "Master Approval Scenario dengan menu {} ditemukan.");

            if (model.Count() == 0)
            {
                throw new ApplicationException(string.Format("Master Approval Correlated Sektor tidak ditemukan."));
            }

            Validate.NotNull(param.UserIdList, "User wajib diisi.");

            if (param.UserIdList.Count() != 3)
            {
                throw new ApplicationException(string.Format("User Approval kurang lengkap (Harus 3 User)."));
            }
            if (param.UserIdList.Count() > 3)
            {
                throw new ApplicationException(string.Format("Jumlah maksimum User tidak sesuai (Maksimal 3 User)."));
            }

            IList<User> userlist = new List<User>();

            //int[] nomorUrutStatusList = new int[3];

            //int nomorUrutStatus = 1;

            foreach (var item in param.UserIdList)
            {
                var user = _userRepositoy.Get(item.GetValueOrDefault());
                Validate.NotNull(param.UserIdList, "User tidak ditemukan.");
                userlist.Add(user);
            }

            //for (int i = 0; i < param.UserIdList.Length; i++)
            //{
            //    var user = _userRepositoy.Get(param.UserIdList[i].GetValueOrDefault());
            //    Validate.NotNull(param.UserIdList, "User tidak ditemukan.");
            //    userlist.Add(user);

            //}

            //var menu = _menuRepositoy.Get(param.MenuId.GetValueOrDefault());
            //Validate.NotNull(menu, "Menu tidak ditemukan.");

            using (_unitOfWork)
            {
                foreach (var item in model)
                {
                    item.Update(userlist[item.NomorUrutStatus.GetValueOrDefault() - 1], param.UpdateBy, param.UpdateDate);
                    _masterApprovalCorrelatedSektorRepository.Update(item);
                }

                //Audit Log UPDATE 
                //int audit = _auditLogService.UpdateCommentAudit(param, id);

                _unitOfWork.Commit();

            }
            return menu.Id;
        }

        public int Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Master Approval Correlated Sektor tidak ditemukan.");

            using (_unitOfWork)
            {
                model.Delete(deleteBy, deleteDate);
                _masterApprovalCorrelatedSektorRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;

                //Audit Log DELETE
               // int audit = _auditLogService.DeleteCommentAudit(id, deleteBy);
            }
            return id;
        }

        public int DeleteByMenuId(int menuId, int deleteBy, DateTime deleteDate)
        {
            var model = this.GetAllByMenuId(menuId);
            Validate.NotNull(model, "Master Approval Correlated Sektor tidak ditemukan.");

            if (model.Count() == 0)
            {
                throw new ApplicationException(string.Format("Master Approval Correlated Sektor tidak ditemukan."));
            }


            using (_unitOfWork)
            {
                foreach (var item in model)
                {
                    item.Delete(deleteBy, deleteDate);
                    _masterApprovalCorrelatedSektorRepository.Update(item);
                }

                _unitOfWork.Commit();

                //Audit Log DELETE
                //int audit = _auditLogService.DeleteCommentAudit(id, deleteBy);
            }
            return menuId;
        }
        #endregion Manipulation
    }
}
