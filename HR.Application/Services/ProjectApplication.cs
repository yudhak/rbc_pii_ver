﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using HR.Application.Params;
using HR.Core;
using HR.Domain;
using System.Linq;
using HR.Infrastructure;

namespace HR.Application
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IProjectRepository _projectRepository;
        private readonly ITahapanRepository _tahapanRepository;
        private readonly ISektorRepository _sektorRepository;
        private readonly IRiskRegistrasiRepository _riskRegistrasiRepository;
        private readonly IProjectRiskRegistrasiRepository _projectRiskRegistrasiRepository;
        private readonly IProjectRiskStatusRepository _projectRiskStatusRepository;
        private readonly IUserRepository _userRepository;
        private readonly IAuditLogService _auditLogService;
        private readonly IStatusRepository _statusRepository;
        private readonly IScenarioDetailRepository _scenarioDetailRepository;
        private readonly IScenarioRepository _scenarioRepository;
        private readonly IDatabaseContext _databaseContext;
        private readonly IRiskMatrixProjectRepository _riskMatrixProjectRepository;
        //private readonly IApprovalRepository _approvalRepository;

        public ProjectService(IUnitOfWork uow, IProjectRepository projectRepository, ITahapanRepository tahapanRepository, ISektorRepository sektorRepository, IRiskRegistrasiRepository riskRegistrasiRepository, 
            IProjectRiskRegistrasiRepository projectRiskRegistrasiRepository, IProjectRiskStatusRepository projectRiskStatusRepository, 
            IAuditLogService auditLogService, IUserRepository userRepository,
            IStatusRepository statusRepository, IScenarioDetailRepository scenarioDetailRepository, IScenarioRepository scenarioRepository
            , IDatabaseContext databaseContext, IRiskMatrixProjectRepository riskMatrixProjectRepository)
        {
            _unitOfWork = uow;
            _projectRepository = projectRepository;
            _tahapanRepository = tahapanRepository;
            _sektorRepository = sektorRepository;
            _riskRegistrasiRepository = riskRegistrasiRepository;
            _projectRiskRegistrasiRepository = projectRiskRegistrasiRepository;
            _projectRiskStatusRepository = projectRiskStatusRepository;
            _auditLogService = auditLogService;
            _userRepository = userRepository;
            _statusRepository = statusRepository;
            _scenarioDetailRepository = scenarioDetailRepository;
            _scenarioRepository = scenarioRepository;
            _databaseContext = databaseContext;
            _riskMatrixProjectRepository = riskMatrixProjectRepository;
        }

        #region Query
        public IEnumerable<Project> GetAll(string keyword, int id)
        {
            return _projectRepository.GetAll(keyword, id);
        }

        public IEnumerable<Project> GetSemuanya()
        {
            return _projectRepository.GetAllActive();
        }

        public Project Get(int id)
        {
            return _projectRepository.Get(id);
        }


        public IEnumerable<Project> GetByTahapan(int scenarioId, string tahapanIds)
        {
            return _projectRepository.GetByTahapan(scenarioId, tahapanIds);
        }


        public Project GetWith(int id)
        {
            var obj = _projectRepository.Get(id);
            foreach (var item in obj.ProjectRiskRegistrasi.ToList())
            {
                if (item.IsDelete == true)
                {
                    obj.ProjectRiskRegistrasi.Remove(item);
                }
            }

            foreach (var item in obj.ProjectRiskStatus.ToList())
            {
                if (item.IsProjectUsed == false)
                {
                    obj.ProjectRiskStatus.Remove(item);
                }
            }

            return obj;
        }

        public void IsExistOnEditing(int id, string namaProject, string keterangan)
        {
            if (_projectRepository.IsExist(id, namaProject))
            {
                throw new ApplicationException(string.Format("Nama Project {0} sudah ada.", namaProject));
            }
        }

        public void isExistOnAdding(string namaProject)
        {
            if (_projectRepository.IsExist(namaProject))
            {
                throw new ApplicationException(string.Format("Nama Project {0} sudah ada.", namaProject));
            }
        }
        #endregion Query

        #region Manipulation
        public int Add(ProjectParam param)
        {
            int id;
            Validate.NotNull(param.NamaProject, "Nama Proyek harus diisi.");
            Validate.NotNull(param.TahunAwalProject, "Awal Project harus diisi.");
            Validate.NotNull(param.TahunAkhirProject, "Akhir Project harus diisi.");
            Validate.NotNull(param.Minimum, "Minimum harus diisi.");
            Validate.NotNull(param.Maximum, "Maximum harus diisi.");
            Validate.NotNull(param.RiskRegistrasiId, "Risk Category harus diisi.");

            Tahapan tahapan = _tahapanRepository.Get(param.TahapanId);
            Validate.NotNull(tahapan, "TahapanId {0} tidak ditemukan di database.", param.TahapanId);

            Sektor sektor = _sektorRepository.Get(param.SektorId);
            Validate.NotNull(sektor, "SektorId {0} tidak ditemukan di database.", param.SektorId);

            var user = _userRepository.Get(param.UserId);
            Validate.NotNull(user, "UserId {0} tidak ditemukan di database.", param.UserId);

            isExistOnAdding(param.NamaProject);
            using (_unitOfWork)
            {

                Project model = new Project(tahapan, sektor, param.NamaProject, param.TahunAwalProject, param.TahunAkhirProject, user, param.TahapanId, param.Minimum,                 
                    param.Maximum, param.SektorId, param.Keterangan, param.IsActive, param.CreateBy, param.CreateDate);
                _projectRepository.Insert(model);                       

                var riskRegistrasi = _riskRegistrasiRepository.GetAll().ToList();
                IList<ProjectRiskRegistrasi> projectRisksAll = GenereateProjectRiskRegistrasiAll(riskRegistrasi, model, param);                
                _projectRiskRegistrasiRepository.Insert(projectRisksAll);

                _unitOfWork.Commit();
                id = model.Id;

                //Audit Log ADD 
                int audit = _auditLogService.AddProjectAudit(param, id);
                foreach(var item in model.ProjectRiskStatus)
                {
                    int audit2 = _auditLogService.AddProjectRiskStatusAudit(item, model);
                }
                _unitOfWork.Commit();
            }

            return id;
        }

        public int Update(int id, ProjectParam param)
        {
            int modelId;

            Project model = Get(id);

            Validate.NotNull(param.NamaProject, "Nama Proyek harus diisi.");
            Validate.NotNull(param.TahunAwalProject, "Awal Project harus diisi.");
            Validate.NotNull(param.TahunAkhirProject, "Akhir Project harus diisi.");
            Validate.NotNull(param.Minimum, "Minimum harus diisi.");
            Validate.NotNull(param.Maximum, "Maximum harus diisi.");
            Validate.NotNull(param.RiskRegistrasiId, "Kategori Resiko harus diisi.");

            Tahapan tahapan = _tahapanRepository.Get(param.TahapanId);
            Validate.NotNull(tahapan, "Tahapan Project tidak ditemukan.");

            Sektor sektor = _sektorRepository.Get(param.SektorId);
            Validate.NotNull(sektor, "Sektor Project tidak ditemukan.");

            var projectRisk = _projectRiskRegistrasiRepository.GetByProjectId(id);
            Validate.NotNull(projectRisk, "Kategori Resiko tidak ditemukan.");

            var user = _userRepository.Get(param.UserId);
            Validate.NotNull(user, "UserId {0} tidak ditemukan di database.", param.UserId);

            IsExistOnEditing(id, param.NamaProject, param.Keterangan);

            IList<ProjectRiskStatus> riskStatus = new List<ProjectRiskStatus>();
            using (_unitOfWork)
            {
                //Audit Log Update 
                int audit = _auditLogService.UpdateProjectAudit(param, id);

                model.Update(tahapan, sektor, param.NamaProject, param.TahunAwalProject, param.TahunAkhirProject, user,
                    param.TahapanId, param.Minimum, param.Maximum, param.SektorId, param.Keterangan, param.IsActive,
                    param.UpdateBy, param.UpdateDate);                
                
                _projectRepository.Update(model);               

                //delete old project risk 
                RemoveOldProjectRisk(id, model, param);                        

                _unitOfWork.Commit();
                modelId = model.Id;
            }

            return modelId;
        }

        public int Delete(int id, int deleteBy, DateTime deleteDate)
        {
            int modelId;
            Project model = Get(id);
            Validate.NotNull(model, "Project tidak ditemukan.");

            var ProjectInScenario = _scenarioDetailRepository.GetAllProjectInScenario(id);
            var ScenarioProcessAndAccepted = _scenarioRepository.GetAllForProductValidate();
            if(ProjectInScenario.Count() != 0)
            {
                foreach(var item in ProjectInScenario)
                {
                    foreach(var item2 in ScenarioProcessAndAccepted)
                    {
                        if(item.ScenarioId == item2.Id)
                        {
                            string nullObj = null;
                            Validate.NotNull(nullObj, "Project tidak bisa dihapus karena telah digunakan pada Scenario yang telah diterima atau sedang diproses.");
                        }
                    }
                }
            }

            using (_unitOfWork)
            {
                model.Delete(deleteBy, deleteDate);
                _projectRepository.Update(model);

                //delete project risk status
                RemoveProjectRiskStatus(model);

                //delete in scenario Details
                var getScenarioDetailProject = _scenarioDetailRepository.GetAllProjectInScenarioNeedDelete(id);
                if(getScenarioDetailProject.Count() != 0)
                {
                    foreach(var item in getScenarioDetailProject.ToList())
                    {
                        _databaseContext.ScenarioDetails.Remove(item);
                    }

                    //delete in risk Matrix Project
                    var getRiskMatrixProject = _riskMatrixProjectRepository.GetAllByProjectIdNeedDelete(id);
                    if (getRiskMatrixProject.Count() != 0)
                    {
                        foreach (var item in getRiskMatrixProject.ToList())
                        {
                            _databaseContext.RiskMatrixProjects.Remove(item);
                        }
                    }

                    //delete in risk Correlated Project
                    var getCorrelatedProject = _databaseContext.CorrelatedProjects.Where(x => x.ProjectId == id).ToList();
                    if (getCorrelatedProject.Count() != 0)
                    {
                        foreach (var item in getCorrelatedProject.ToList())
                        {
                            _databaseContext.CorrelatedProjects.Remove(item);
                        }
                    }

                    //delete in risk Correlated sector
                    var getCorrelatedSector = _databaseContext.CorrelatedSektors.Where(x => x.SektorId == model.SektorId).ToList();
                    if (getCorrelatedSector.Count() != 0)
                    {
                        foreach (var item in getCorrelatedSector.ToList())
                        {
                            var SectorNeedDelete = _databaseContext.ScenarioDetails.Where(x => x.ScenarioId == item.ScenarioId && x.IsDelete == false);
                            var GakUsahHapus = false;
                            foreach (var item2 in SectorNeedDelete.ToList())
                            {
                                if (item2.ProjectId != id)
                                {
                                    var checkSektor = _databaseContext.Projects.SingleOrDefault(x => x.Id == item2.ProjectId);
                                    if (checkSektor.SektorId == model.SektorId)
                                    {
                                        GakUsahHapus = true;
                                    }
                                }
                            }
                            if (GakUsahHapus == false)
                            {
                                _databaseContext.CorrelatedSektors.Remove(item);
                            }
                        }
                    }
                }              

                _unitOfWork.Commit();
                modelId = model.Id;

                //Audit Log Delete 
                int audit = _auditLogService.DeleteProjectAudit(id, deleteBy);
            }

            return modelId;
        }

        #endregion Manipulation

        #region Private
        private IList<ProjectRiskRegistrasi> GenereateProjectRiskRegistrasiAll(List<RiskRegistrasi> list, Project project, ProjectParam param)
        {
            IList<ProjectRiskRegistrasi> projectRisksAll = new List<ProjectRiskRegistrasi>();           
            foreach (var item in list)
            {
                ProjectRiskRegistrasi projectRisk = new ProjectRiskRegistrasi(project, item, param.CreateBy, param.CreateDate);
                projectRisksAll.Add(projectRisk);
            }
            IList<ProjectRiskRegistrasi> projectRisks = GenereateProjectRiskRegistrasi(param, project);
          
            foreach (var item in projectRisksAll)
            {
                int Batasan = 0;
                foreach(var item2 in projectRisks)
                {
                    if(Batasan == 0)
                    {
                        if (item.RiskRegistrasiId == item2.RiskRegistrasiId)
                        {
                            item.IsDelete = false;
                            Batasan = 1;
                        }
                        else
                        {
                            item.IsDelete = true;                            
                        }
                    }                    
                }
            }
            project.AddProjectRisk(projectRisksAll);
            return projectRisksAll;
        }

        private IList<ProjectRiskRegistrasi> GenereateProjectRiskRegistrasi(ProjectParam param, Project model)
        {
            IList<ProjectRiskRegistrasi> projectRisks = new List<ProjectRiskRegistrasi>();
            IList<ProjectRiskStatus> riskStatus = new List<ProjectRiskStatus>();
            List<int> riskRegistrasiIds = new List<int>();

            if (param.RiskRegistrasiId != null)
            {
                for (int i = 0; i < param.RiskRegistrasiId.Length; i++)
                {
                    int n;
                    if (Int32.TryParse(param.RiskRegistrasiId[i], out n))
                    {
                        if (!riskRegistrasiIds.Contains(n))
                            riskRegistrasiIds.Add(n);
                    }
                }

                foreach (var item in riskRegistrasiIds)
                {
                    var risk = _riskRegistrasiRepository.Get(item);
                    Validate.NotNull(risk, "Risk Category id " + item + " is not found.");

                    ProjectRiskRegistrasi projectRisk = new ProjectRiskRegistrasi(model, risk, param.CreateBy, param.CreateDate);
                    projectRisks.Add(projectRisk);

                    ProjectRiskStatus status = new ProjectRiskStatus(model, risk.Id, risk.KodeMRisk, risk.NamaCategoryRisk, risk.Definisi, true);
                    riskStatus.Add(status);
                }

                //Set Project Risk Status for this project
                var dataRiskStatus = GenerateRiskStatus(riskStatus, model);
                model.AddProjectRiskStatus(dataRiskStatus);

            }
            return projectRisks;
        }
        
 		private IList<ProjectRiskStatus> GenerateRiskStatus(IList<ProjectRiskStatus> status, Project project)
        {
            var allRisks = _riskRegistrasiRepository.GetAll().ToList();
            IList<ProjectRiskStatus> riskStatus = new List<ProjectRiskStatus>();

            for (int i = 0; i < allRisks.Count; i++)
            {
                var filtered = status.Where(x => x.KodeMRisk == allRisks[i].KodeMRisk).FirstOrDefault();
                if (filtered == null)
                {
                    ProjectRiskStatus model = new ProjectRiskStatus(project, allRisks[i].Id, allRisks[i].KodeMRisk, allRisks[i].NamaCategoryRisk, allRisks[i].Definisi, false);
                    status.Add(model);
                }
            }
            return status.OrderBy(x => x.KodeMRisk).ToList();
        }

        private void RemoveOldProjectRisk(int id, Project model, ProjectParam param)
        {
            //insert new project risk
            IList<ProjectRiskRegistrasi> projectRisks = GenereateProjectRiskRegistrasiUpdate(param, model);

            var projectRisk = _projectRiskRegistrasiRepository.GetByProjectId(id);
            var list = _riskRegistrasiRepository.GetAll().ToList();

            IList<ProjectRiskRegistrasi> projectRisksAll = new List<ProjectRiskRegistrasi>();
            foreach (var item in list)
            {
                ProjectRiskRegistrasi projectRiskNewL = new ProjectRiskRegistrasi(model, item, param.CreateBy, param.CreateDate);
                projectRisksAll.Add(projectRiskNewL);
            }
            var projectRiskNew = projectRisksAll;

            if (projectRiskNew != null)
            {
                foreach (var item in projectRiskNew)
                {
                    int Batasan = 0;
                    foreach (var item2 in projectRisks)
                    {
                        if (Batasan == 0)
                        {
                            if (item.RiskRegistrasiId == item2.RiskRegistrasiId)
                            {
                                item.IsDelete = false;
                                Batasan = 1;
                            }
                            else
                            {
                                item.IsDelete = true;
                            }
                        }                        
                    }                   
                }                           
            }

            foreach (var item in projectRisk)
            {
                foreach (var item2 in projectRiskNew)
                {
                    if (item.RiskRegistrasiId == item2.RiskRegistrasiId)
                    {
                        if(item.IsDelete!= item2.IsDelete)
                        {
                            item.IsDelete = item2.IsDelete;
                            item.UpdateBy = param.UpdateBy;
                            item.UpdateDate = param.UpdateDate;
                            _projectRiskRegistrasiRepository.Update(item);
                            _unitOfWork.Commit();
                        }                       
                    }
                }                
            }
        }

        private IList<ProjectRiskRegistrasi> GenereateProjectRiskRegistrasiUpdate(ProjectParam param, Project model)
        {
            IList<ProjectRiskRegistrasi> projectRisks = new List<ProjectRiskRegistrasi>();
            IList<ProjectRiskStatus> riskStatus = new List<ProjectRiskStatus>();
            List<int> riskRegistrasiIds = new List<int>();

            if (param.RiskRegistrasiId != null)
            {
                for (int i = 0; i < param.RiskRegistrasiId.Length; i++)
                {
                    int n;
                    if (Int32.TryParse(param.RiskRegistrasiId[i], out n))
                    {
                        if (!riskRegistrasiIds.Contains(n))
                            riskRegistrasiIds.Add(n);
                    }
                }

                foreach (var item in riskRegistrasiIds)
                {
                    var risk = _riskRegistrasiRepository.Get(item);
                    Validate.NotNull(risk, "Risk Category id " + item + " is not found.");

                    ProjectRiskRegistrasi projectRisk = new ProjectRiskRegistrasi(model, risk, param.CreateBy, param.CreateDate);
                    projectRisks.Add(projectRisk);

                    ProjectRiskStatus status = new ProjectRiskStatus(model, risk.Id, risk.KodeMRisk, risk.NamaCategoryRisk, risk.Definisi, true);
                    riskStatus.Add(status);
                }
              
                var dataRiskStatus = GenerateRiskStatusUpdate(riskStatus, model, param);
            }
            return projectRisks;
        }

        private IList<ProjectRiskStatus> GenerateRiskStatusUpdate(IList<ProjectRiskStatus> status, Project project, ProjectParam param)
        {
            var allRisks = _riskRegistrasiRepository.GetAll().ToList();
            IList<ProjectRiskStatus> riskStatus = new List<ProjectRiskStatus>();

            for (int i = 0; i < allRisks.Count; i++)
            {
                var filtered = status.Where(x => x.KodeMRisk == allRisks[i].KodeMRisk).FirstOrDefault();
                if (filtered == null)
                {
                    ProjectRiskStatus model = new ProjectRiskStatus(project, allRisks[i].Id, allRisks[i].KodeMRisk, allRisks[i].NamaCategoryRisk, allRisks[i].Definisi, false);
                    status.Add(model);                    
                }
            }
            var projectStatus = _projectRiskStatusRepository.GetByProjectId(project.Id);

            foreach (var item in projectStatus)
            {
                foreach (var item2 in status)
                {
                    if (item.RiskRegistrasiId == item2.RiskRegistrasiId)
                    {
                        int audit2 = _auditLogService.UpdateProjectRiskStatusAudit(item2, project, item);
                        item.IsProjectUsed = item2.IsProjectUsed;                       
                    }
                }
                _projectRiskStatusRepository.Update(item);
                _unitOfWork.Commit();
            }

            foreach(var item2 in status)
            {
                int find = 0;
                foreach(var item in projectStatus)
                {
                    if(find != 1)
                    {
                        if (item2.KodeMRisk == item.KodeMRisk)
                        {
                            find = 1;
                        }
                    }                   
                }
                if(find == 0)
                {
                    _projectRiskStatusRepository.Insert(item2);
                    var risk = _riskRegistrasiRepository.Get(item2.RiskRegistrasiId);
                    ProjectRiskRegistrasi model = new ProjectRiskRegistrasi(project, risk, param.UpdateBy, param.UpdateDate);
                    _projectRiskRegistrasiRepository.Insert(model);
                    _unitOfWork.Commit();

                    //Add Audit
                    int audit2 = _auditLogService.AddProjectRiskStatusAuditByUpdate(item2, project.UpdateBy);
                }
            }
            return projectStatus.OrderBy(x => x.KodeMRisk).ToList();
        }

        private void RemoveProjectRiskStatus(Project model)
        {
            var riskStatus = _projectRiskStatusRepository.GetByProjectId(model.Id);
            if (riskStatus != null)
            {
                foreach (var item in riskStatus)
                {
                    model.RemoveProjecRiskStatus(item);
                    _projectRiskStatusRepository.Delete(item.Id);
                }
            }
        }

        private void UpdateProjectRiskStatus(Project model)
        {
            var riskStatus = _projectRiskStatusRepository.GetByProjectId(model.Id);
            if (riskStatus != null)
            {
                foreach (var item in riskStatus)
                {
                    model.RemoveProjecRiskStatus(item);
                    _projectRiskStatusRepository.Delete(item.Id);
                }
            }
        }

        #endregion Private
    }
}
