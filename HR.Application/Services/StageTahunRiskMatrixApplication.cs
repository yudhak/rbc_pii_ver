﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;

namespace HR.Application
{
    public class StageTahunRiskMatrixService : IStageTahunRiskMatrixService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IStageTahunRiskMatrixRepository _stageTahunRiskMatrixRepository;
        private readonly IRiskMatrixProjectRepository _riskMatrixProjectRepository;
        private readonly IStageRepository _stageRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly ILikehoodDetailRepository _likehoodDetailRepository;
        private readonly IScenarioRepository _scenarioRepository;
        private readonly IStageTahunRiskMatrixDetailRepository _stageTahunRiskMatrixDetailRepository;
        private readonly IAuditLogService _auditLogService;
        private readonly IUserRepository _userRepository;
        private readonly IStatusRepository _statusRepository;
        private readonly IApprovalRepository _approvalRepository;
        private readonly IRiskMatrixRepository _riskMatrixRepository;
        private readonly IMasterApprovalRiskMatrixProjectRepository _masterApprovalRiskMatrixProjectRepository;


        public StageTahunRiskMatrixService(IUnitOfWork uow, IStageTahunRiskMatrixRepository stageTahunRiskMatrixRepository, IRiskMatrixProjectRepository riskMatrixProjectRepository, IStageRepository stageRepository, 
            ILikehoodDetailRepository likehoodDetailRepository, IProjectRepository projectRepository, IScenarioRepository scenarioRepository, IStageTahunRiskMatrixDetailRepository stageTahunRiskMatrixDetailRepository
            , IAuditLogService auditLogService, IUserRepository userRepository, IStatusRepository statusRepository, IApprovalRepository approvalRepository, IRiskMatrixRepository riskMatrixRepository,
            IMasterApprovalRiskMatrixProjectRepository masterApprovalRiskMatrixProjectRepository)
        {
            _unitOfWork = uow;
            _stageTahunRiskMatrixRepository = stageTahunRiskMatrixRepository;
            _riskMatrixProjectRepository = riskMatrixProjectRepository;
            _stageRepository = stageRepository;
            _likehoodDetailRepository = likehoodDetailRepository;
            _projectRepository = projectRepository;
            _scenarioRepository = scenarioRepository;
            _stageTahunRiskMatrixDetailRepository = stageTahunRiskMatrixDetailRepository;
            _auditLogService = auditLogService;
            _userRepository = userRepository;
            _statusRepository = statusRepository;
            _approvalRepository = approvalRepository;
            _riskMatrixRepository = riskMatrixRepository;
            _masterApprovalRiskMatrixProjectRepository = masterApprovalRiskMatrixProjectRepository;
        }

        #region Query
        public IEnumerable<StageTahunRiskMatrix> GetAll()
        {
            return _stageTahunRiskMatrixRepository.GetAll();
        }

        public StageTahunRiskMatrix Get(int id)
        {
            return _stageTahunRiskMatrixRepository.Get(id);
        }
        
        public IEnumerable<StageTahunRiskMatrix> GetByRiskMatrixProjectIdForHeaderTable(int riskMatrixProjectId)
        {
            return _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectId(riskMatrixProjectId).OrderBy(x=>x.Tahun);
        }

        public StageTahunRiskMatrix GetByTahun(int riskMatrixProjectId, int tahun)
        {
            return _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectIdYear(riskMatrixProjectId, tahun);
        }


        public StageTahunRiskMatrixParam GetByRiskMatrixProjectId(int riskMatrixProjectId)
        {
            StageTahunRiskMatrixParam stageTahunRiskMatrixParam = new StageTahunRiskMatrixParam();
            IList<StageValue> stageValues = new List<StageValue>();

            var data = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectId(riskMatrixProjectId);
            var stages = data.GroupBy(y => y.StageId).ToList();

            if (stages.Count > 0)
            {
                for (int r = 0; r < stages.Count(); r++)
                {
                    StageValue stageValue = new StageValue(); //temporary

                    List<int> values = new List<int>();
                    
                    stageValue.StageId = stages[r].Key;
                    List<int> years = new List<int>();

                    for (int s = 0; s < stages[r].Count(); s++)
                    {
                        
                        var staVal = stages[r].ToList();

                        var year = staVal[s].Tahun;
                        years.Add(year);
                    }
                    var thnMulai = years.Min();
                    var thnSelesai = years.Max();
                    values.Add(thnMulai);
                    values.Add(thnSelesai);
                    int[] valueExLi = values.ToArray();

                    stageValue.Values = valueExLi;
                    stageValues.Add(stageValue);
                }
                stageTahunRiskMatrixParam.StageValue = stageValues.ToArray();
            }
            return stageTahunRiskMatrixParam;            
        }

        public void IsExistOnEditing(int id, int riskMatrixProjectId)
        {
            if (_stageTahunRiskMatrixRepository.IsExist(id, riskMatrixProjectId))
            {
                throw new ApplicationException(string.Format("StageTahunRiskMatrix {0} Sudah Ada.", riskMatrixProjectId));
            }
        }

        public void isExistOnAdding(int riskMatrixProjectId)
        {
            if (_stageTahunRiskMatrixRepository.IsExist(riskMatrixProjectId))
            {
                throw new ApplicationException(string.Format("StageTahunRiskMatrix {0} Sudah Ada.", riskMatrixProjectId));
            }
        }
        #endregion Query

        #region Manipulation 
        public int Add(StageTahunRiskMatrixParam param)
        {
            var id = 0;
            var riskMatrixProject = _riskMatrixProjectRepository.Get(param.RiskMatrixProjectId);
            var project = _projectRepository.Get(riskMatrixProject.Project.Id);
            IList<StageTahunRiskMatrix> stageTahun = new List<StageTahunRiskMatrix>();

            

            isExistOnAdding(param.RiskMatrixProjectId);
            using (_unitOfWork)
            {
                IList<int> generateYears = GenerateYear(param);

                for (int i = 0; i < generateYears.Count; i++)
                {

                    IList<int> generateStages = GenerateStage(param);
                    var stage = _stageRepository.Get(generateStages[i]);

                    StageTahunRiskMatrix model = new StageTahunRiskMatrix(riskMatrixProject, stage, generateYears[i], param.CreateBy, param.CreateDate);
                    _stageTahunRiskMatrixRepository.Insert(model);
                    stageTahun.Add(model);
                }
                var st = _riskMatrixProjectRepository.Get(riskMatrixProject.Id);
                st.AddStageTahun(stageTahun);

                _unitOfWork.Commit();

                //Audit Log Add 
                foreach (var item in stageTahun)
                {
                    int audit = _auditLogService.AddStageTahunRiskMatrixAudit(item);
                }



                //insert into approval
                //if (status != null)
                //{
                //    AddApproval(riskMatrixProject.Id, status, param.CreateBy, param.CreateDate, nomorUrutStatus);
                //}
                _unitOfWork.Commit();
            }
            return id;
        }

        public int Update(int id, StageTahunRiskMatrixParam param)
        {
            var modelId = 0;
            var riskMatrixProject = _riskMatrixProjectRepository.Get(param.RiskMatrixProjectId);
            var project = _projectRepository.Get(riskMatrixProject.Project.Id);
            
            using (_unitOfWork)
            {
                //get old data
                var oldStageTahun = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectId(param.RiskMatrixProjectId).ToList();

                SetOldStageAndNewStage(oldStageTahun, param, riskMatrixProject);
                _unitOfWork.Commit();
            }
            return modelId;
        }
        
       
        #endregion Manipulation
        public  IList<int> GenerateYear (StageTahunRiskMatrixParam param)
        {
            IList<int> years = new List<int>();
            int thnAwal = param.StartProject.Year;
            int thnAkhir = param.EndProject.Year;
            int interval = thnAkhir - thnAwal;
            int[] tahun = new int[interval + 1];
            for (int i = 0; i <= interval; i++)
            {
                tahun[i] = i + thnAwal;
                var year = tahun[i];
                years.Add(year);
            }
            return years;
        }
        public IList<int> GenerateStage(StageTahunRiskMatrixParam param)
        {
            IList<int> contentStages = new List<int>();
            foreach (var item in param.StageValue)
                {
                    var stage = _stageRepository.Get(item.StageId);
                    Validate.NotNull(stage, "Stage tidak ditemukan.");

                    var startStage = item.Values[0];
                    Validate.NotNull(startStage, "Stage Awal tidak ditemukan.");

                    var endStage = item.Values[1];
                    Validate.NotNull(endStage, "Stage Akhir tidak ditemukan.");

                    int intervalStage = endStage - startStage;
                    int[] content = new int[intervalStage + 1];
                    for (int j = 0; j <= intervalStage; j++)
                    {
                        content[j] = item.StageId;
                        var contents = content[j];
                        contentStages.Add(contents);
                    }
                }
            return contentStages;
        }
        public int Delete(int id, int deleteBy, DateTime deleteDate)
        {
            throw new NotImplementedException();
        }

        private void SetOldStageAndNewStage(IList<StageTahunRiskMatrix> oldData, StageTahunRiskMatrixParam param, RiskMatrixProject riskMatrixProject)
        {
            Dictionary<int, dynamic> oldStage = new Dictionary<int, dynamic>();

            var newStage = GetNewStageTahun(param);

            if(oldData.Count > 0)
            {
                foreach (var item in oldData)
                {
                    oldStage.Add(item.Tahun, item.StageId);
                }
            }
            
            //remove year in oldStage And update Data
            foreach(var item in oldStage.ToList())
            {
                int tahunAda = 0;
                foreach(var item2 in newStage.ToList())
                {
                    if (item.Key == item2.Key)
                    {
                        tahunAda = 1;
                        foreach(var itemUpdate in oldData.ToList())
                        {
                            if(itemUpdate.Tahun == item.Key)
                            {
                                var model = _stageTahunRiskMatrixRepository.Get(itemUpdate.Id);
                                model.AfterRemove(item2.Value, param.UpdateBy, param.UpdateDate);
                                oldStage.Remove(item.Key);
                                newStage.Remove(item2.Key);
                            }                           
                        }
                    }
                }
                if(tahunAda != 1)
                {
                    foreach(var itemHapus in oldData.ToList())
                    {
                        if(itemHapus.Tahun == item.Key)
                        {
                            var model = _stageTahunRiskMatrixRepository.Get(itemHapus.Id);
                            model.Delete(param.UpdateBy, param.UpdateDate);
                            oldStage.Remove(item.Key);                           
                        }
                    }
                }
            }

            var oldDataCallBack = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectIdDelete(riskMatrixProject.Id);

            //callback year that has been deleted
            foreach (var item in oldDataCallBack.ToList())
            {
                foreach(var item2 in newStage.ToList())
                {
                    if(item.Tahun == item2.Key)
                    {
                        item.RemoveDelete(param.UpdateBy, param.UpdateDate);
                        item.AfterRemove(item2.Value);
                        newStage.Remove(item2.Key);
                        var collectionDetail = _stageTahunRiskMatrixDetailRepository.GetByStageTahunRiskMatrixIdHasRemoved(item.Id);
                        foreach (var col in collectionDetail.ToList())
                        {
                            col.RemoveDelete();
                        }
                    }
                }
            }
                        
            if (oldStage.Count > newStage.Count)
            {
                var differentialData = oldStage.Except(newStage); 

                if (differentialData.Count() > 0)
                {
                    foreach (var item in differentialData)
                    {
                        var stage = _stageRepository.Get(item.Value);
                        string stageString = Convert.ToString(stage.Id);
                        var stageId = Int32.Parse(stageString);
                        var year = item.Key;

                        //check exisiting new daata
                        var currentData = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectIdYearStage(riskMatrixProject.Id, year, stageId);

                        if (currentData.Count() > 0)
                        {
                            foreach (var itemCurrent in currentData)
                            {
                                //change status
                                var currentModel = _stageTahunRiskMatrixRepository.Get(itemCurrent.Id);
                                Validate.NotNull(currentModel, "Stage Tahun Risk Matrix ID tidak ditemukan.");

                                currentModel.SetInActive(param.UpdateBy, param.UpdateDate);
                                _stageTahunRiskMatrixRepository.Update(currentModel);

                                //remove from model
                                //var riskMatrixProjectModel = _riskMatrixProjectRepository.GetByScenarioIdProjectId(itemCurrent.RiskMatrixProject.ScenarioId, itemCurrent.RiskMatrixProject.ProjectId);
                                //riskMatrixProjectModel.RemoveStageTahun(currentModel);
                            }
                        } 
                        else
                        {
                            //insert new data
                            StageTahunRiskMatrix model = new StageTahunRiskMatrix(riskMatrixProject, stage, item.Key, param.UpdateBy, param.UpdateDate);
                            _stageTahunRiskMatrixRepository.Insert(model);
                        }
                    }
                }
            }

            else if (oldStage.Count < newStage.Count)
            {
                var differentialData = newStage.Except(oldStage);

                if (differentialData.Count() > 0)
                {
                    foreach (var item in differentialData)
                    {
                        var stage = _stageRepository.Get(item.Value);
                        string stageString = Convert.ToString(stage.Id);
                        var stageId = Int32.Parse(stageString);
                        var year = item.Key;

                        //check exisiting new daata
                        var currentData = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectIdYearStage(riskMatrixProject.Id, year, stageId);

                        if (currentData.Count() > 0)
                        {
                            foreach (var itemCurrent in currentData)
                            {
                                //change status
                                var currentModel = _stageTahunRiskMatrixRepository.Get(itemCurrent.Id);
                                Validate.NotNull(currentModel, "Stage Tahun Risk Matrix ID tidak ditemukan.");

                                currentModel.SetActive(param.UpdateBy, param.UpdateDate);
                                _stageTahunRiskMatrixRepository.Update(currentModel);
                            }
                        }
                        else
                        {
                            //insert new data
                            StageTahunRiskMatrix model = new StageTahunRiskMatrix(riskMatrixProject, stage, item.Key, param.UpdateBy, param.UpdateDate);
                            _stageTahunRiskMatrixRepository.Insert(model);
                        }
                    }
                }
            }
        }

        private Dictionary<int, dynamic> GetNewStageTahun(StageTahunRiskMatrixParam param)
        {
            Dictionary<int, dynamic> newStage = new Dictionary<int, dynamic>();
            IList<int> generateYears = GenerateYear(param);
            for (int i = 0; i < generateYears.Count; i++)
            {
                IList<int> generateStages = GenerateStage(param);
                newStage.Add(generateYears[i], generateStages[i]);
            }

            return newStage;
        }

        private void RemoveOldStageInRiskMatrixProject(RiskMatrixProject riskMatrixProject)
        {
            if(riskMatrixProject.StageTahunRiskMatrix != null)
            {
                foreach (var item in riskMatrixProject.StageTahunRiskMatrix)
                {
                    riskMatrixProject.RemoveStageTahun(item);
                }
            }
        }

        //private void AddApproval(int idParam,  Status status, int? createBy, DateTime? createDate, int nomorUrutStatus)
        //{
        //    var user = _userRepository.Get(createBy.GetValueOrDefault());

        //    var userRole = _userRepository.GetRoleUser(user.Id);

        //    string statusApproval = status.StatusDescription + " By " + user.UserName + " Role " + userRole.Name;
        //    string keterangan = "";
        //    string sourceApproval = "RiskMatrixProject";
        //    Approval model = new Approval(idParam, statusApproval,
        //    sourceApproval, status, user.Id, createDate,  keterangan, nomorUrutStatus);

        //    _approvalRepository.Insert(model);

        //}
    }
}
