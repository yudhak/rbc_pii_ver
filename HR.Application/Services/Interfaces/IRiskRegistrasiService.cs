﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface IRiskRegistrasiService
    {
        IEnumerable<RiskRegistrasi> GetAll();
        IEnumerable<RiskRegistrasi> GetAll(string keyword, int id);
        RiskRegistrasi Get(int id);
        int Add(RiskRegistrasiParam param);
        int Update(int id, RiskRegistrasiParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
    }
}
