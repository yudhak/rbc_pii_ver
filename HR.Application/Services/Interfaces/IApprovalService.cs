﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface IApprovalService
    {
        IEnumerable<Approval> GetAll();
        IEnumerable<ApprovalExtend> GetSentItem(string keyword, int id, bool sentItem, int userId);
        IEnumerable<ApprovalExtend> GetSentItem(string keyword, int id, int id2, bool sentItem, int userId);
        IEnumerable<ApprovalExtend> GetByRequestId(int requestId);
        Approval Get(int id);
        string GetByActive(int id, int idUser, string source);
        //Approval GetScenario(int id, string sourceApproval);
        int Add(ApprovalParam param);
       // void Update(int id, ApprovalParam param);
        int UpdateStatus(int id, ApprovalParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
    }
}
