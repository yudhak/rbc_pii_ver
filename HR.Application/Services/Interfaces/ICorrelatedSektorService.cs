﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface ICorrelatedSektorService
    {
        IEnumerable<CorrelatedSektor> GetAll();
        IEnumerable<CorrelatedSektor> GetByScenarioDefaultId(string keyword);
        IEnumerable<CorrelatedSektor> GetByScenarioDefaultId(string keyword, int user);
        IEnumerable<CorrelatedSektor> GetByScenarioDefaultId(string keyword,int id, int user);
        IEnumerable<CorrelatedSektor> GetByScenarioDefaultId(string keyword, int id, int id2, int user);
        CorrelatedSektor Get(int id);
        int Add(CorrelatedSektorParam param);
        int Update(int id, CorrelatedSektorParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
    }
}
