﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface ICorrelatedSektorTemporerService
    {
        IEnumerable<CorrelatedSektorTemporer> GetAll();
        CorrelatedSektorTemporer Get(int id);
        int Add(CorrelatedSektorTemporer param);
        void Delete(int id, DateTime deleteDate, int deleteBy);
       // RiskMatrix
        //int SubmitRiskMatrixProjectAudit(int id, int updateBy);
        //int SubmitRiskMatrixProjectAudit(RiskMatrixCollectionParameter param, int id);
        //int ApproveRiskMatrixProjectAudit(ApprovalParam param, int otherRiskMatrixProjectId, int id);
        //int RejectRiskMatrixProjectAudit(int id,  int UpdateBy);
        //int SetApproveRiskMatrixProjectAudit(int id, int updateBy);
        ////RiskRegistrasi
        //int AddRiskRegistrasiAudit(RiskRegistrasiParam param, int id);
        //int UpdateRiskRegistrasiAudit(RiskRegistrasiParam param, int id);
        //int DeleteRiskRegistrasiAudit(int id, int deleteBy);
        ////Sektor
        //int AddSektorAudit(SektorParam param, int id);
        //int UpdateSektorAudit(SektorParam param, int id);
        //int DeleteSektorAudit(int id, int deleteBy);
        ////Stage
        //int AddStageAudit(StageParam param, int id);
        //int UpdateStageAudit(StageParam param, int id);
        //int DeleteStageAudit(int id, int deleteBy);
        ////StageTahunRiskMatrix
        //int AddStageTahunRiskMatrixAudit(StageTahunRiskMatrix param);
        //int UpdateStageTahunRiskMatrixAudit(string param, int id, int userr);
        ////StageTahunRiskMatrixDetail
        //int UpdateStageTahunRiskMatrixDetailAudit(StageTahunRiskMatrixDetail param, StageTahunRiskMatrixDetail param2);        
    }
}
