﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface IProjectService
    {
        IEnumerable<Project> GetAll(string keyword, int id);
        IEnumerable<Project> GetSemuanya();
        IEnumerable<Project> GetByTahapan(int scenarioId, string tahapanIds);
        Project Get(int id);
        Project GetWith(int id);
        int Add(ProjectParam param);
        int Update(int id, ProjectParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
    }
}
