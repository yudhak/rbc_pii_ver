﻿using HR.Application.Params;
using HR.Domain;

namespace HR.Application
{
    public interface ICorrelationRiskAntarProjectService
    {
        CorrelationRiskAntarProjectParam GetByCorrelationRiskAntarSektorId(int correlationRiskAntarSektorId);
        int Add(CorrelationRiskAntarProjectParam param);
    }
}
