﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Application
{
    public interface IUserResetPasswordService
    {
        void ResetPassword(string userName, string url);
    }
}
