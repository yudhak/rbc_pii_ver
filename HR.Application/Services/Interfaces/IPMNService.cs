﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface IPMNService
    {
        IEnumerable<PMN> GetAll();
        IEnumerable<PMN> GetAll(string keyword, int id);
        PMN Get(int id);
        int Add(PMNParam param);
        int Update(int id, PMNParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
    }
}
