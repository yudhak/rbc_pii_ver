﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface ISubRiskRegistrasiService
    {
        IEnumerable<SubRiskRegistrasi> GetAll();
        IEnumerable<SubRiskRegistrasi> GetByRiskId(int riskId, string keyword, int id);

        SubRiskRegistrasi Get(int id);
        int Add(SubRiskRegistrasiParam param);
        int Update(int id, SubRiskRegistrasiParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
    }
}
