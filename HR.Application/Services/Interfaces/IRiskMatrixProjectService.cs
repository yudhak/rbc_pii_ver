﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface IRiskMatrixProjectService
    {
        IEnumerable<RiskMatrixProject> GetAll();
        IEnumerable<RiskMatrixProject> GetAll(string keyword);
        IEnumerable<RiskMatrixProject> GetAll(string keyword, int id, int userId);
        IEnumerable<RiskMatrixProject> GetAll(string keyword, int id,int id2, int userId);
        IEnumerable<RiskMatrixProject> GetAll2(string keyword, int id, int userId);
        IEnumerable<RiskMatrixProject> GetAllBaseId(string keyword, int id);
        IEnumerable<RiskMatrixProject> GetAllData();
        IEnumerable<RiskMatrixProject> GetAllData(string keyword);
        RiskMatrixProject Get(int id);
        int Add(RiskMatrixProjectParam param);
        int Update(int id, RiskMatrixProjectParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
        IEnumerable<RiskMatrixProject> GetAllProjectActive();
        IEnumerable<RiskMatrixProject> GetAllProjectActive(string keyword, int id, int userId);
        IEnumerable<RiskMatrixProject> GetAllProjectActive(string keyword);
    }
}
