﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface IMasterApprovalScenarioService
    {
       
        IEnumerable<MasterApprovalScenario> GetAll();
        IEnumerable<MasterApprovalScenario> GetAllByMenuId(int menuid);
        IEnumerable<MasterApprovalScenario> GetAllByMenuIdIsDeleteTrue(int menuid);
        //IEnumerable<MasterApprovalScenario> GetAll(string keyword);
        MasterApprovalScenario Get(int id);
        int Add(MasterApprovalScenarioParam param);
        int Update(int menuId, MasterApprovalScenarioParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
        int DeleteByMenuId(int id, int deleteBy, DateTime deleteDate);
        IEnumerable<MasterApprovalScenario> GetByCreateBy(int createBy);
    }
}
