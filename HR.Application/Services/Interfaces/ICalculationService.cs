﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Application
{
    public interface ICalculationService
    {
        //UndiversifiedRiskCapitalProjectCollection GetUndiversifiedByProject(int projectId);
        //IList<CalculationResult> GetAllDataCalculationByScenarioId(int scenarioId);
        Calculation GetAllDataCalculationByScenarioId();
        void GenerteScenarioCalculationRisk(ScenarioCollection param);
        UndiversifiedRiskCapitalProjectCollection GetUndiversifiedByProject(int projectId, int scenarioId);
        AggregationSektor GetUndiversifiedAggregationOfSector(int scenarioId, AggregationOfProject aggregationOfProject);
        UndiversifiedAggregationRisk GetUndiversifiedAggregationOfRisk(IList<RiskRegistrasi> risk, IList<int> generateYear, IList<UndiversifiedRiskCapitalProjectCollection> undiversified);
        StressTesting GetStressTesting(int[] year, IList<CalculationResult> calculationResult);
        Sensitivity GetSensitivity(int[] year, IList<CalculationResult> calculationResult);
        IList<ScenarioTestingCollection> GetScenarioTestingCollection(int[] year, IList<CalculationResult> calculationResult);
        IntraProjectDiversifiedAggregationRisk GetIntraProjectDiversifiedAggregationOfRisk(IList<RiskRegistrasi> risk, IList<int> generateYear, IList<UndiversifiedRiskCapitalProjectCollection> diversified);
        InterProjectDiversifiedAggregationRisk GetInterProjectDiversifiedAggregationOfRisk(IList<RiskRegistrasi> risk, IList<int> generateYear, IList<RiskYearCollection> intraDiversifiedRisk, IList<AggregationIntraDiversifiedProjectCapital> intraProjectDiversified, IList<AggregationInterDiversifiedProjectCapital> interProjectDiversified);
        IList<AggregationUndiversifiedProjectCapital> GetAggregationUndiversifiedProjectCapital(IList<int> generateYear, IList<UndiversifiedRiskCapitalProjectCollection> undiversifiedRiskCapitalProjectCollection);
        IList<AggregationIntraDiversifiedProjectCapital> GetAggregationIntraDiversifiedProjectCapital(IList<int> generateYear, IList<UndiversifiedRiskCapitalProjectCollection> undiversifiedRiskCapitalProjectCollection);
        IList<AggregationInterDiversifiedProjectCapital> GetAggregationInterDiversifiedProjectCapital(int scenarioId, IList<int> generateYear, AggregationOfProject aggregationOfProject);
        IList<int> GenerateYear(ScenarioProject scenarios);
        IntraProjectDiversifiedAggregationSektor GenerateIntraProjectDiversified(Scenario scenario, IList<SektorLite> sektorCollection,
            IList<AggregationOfProjectCollection> projectCollection, IList<AggregationIntraDiversifiedProjectCapital> aggregationIntraCollection);
        InterProjectDiversifiedAggregationSektor GenerateInterProjectDiversified(Scenario scenario, IList<SektorLite> sektorCollection,
            IList<AggregationOfProjectCollection> projectCollection, IList<AggregationInterDiversifiedProjectCapital> aggregationInterCollection);
        AssetProjectionLiquidity GenerateAssetProjectionInvestmentStrategy(IList<AggregationInterDiversifiedProjectCapital> interProjectDiversified, IList<int> years, Scenario scenario, AggregationOfProject aggregationOfProject);
        int AddAvailableCapitalProjected(AvailableCapitalProjectedParam param);
    }
}
