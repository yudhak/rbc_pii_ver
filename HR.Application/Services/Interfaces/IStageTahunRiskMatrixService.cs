﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface IStageTahunRiskMatrixService
    {
        IEnumerable<StageTahunRiskMatrix> GetAll(); 
        IEnumerable<StageTahunRiskMatrix> GetByRiskMatrixProjectIdForHeaderTable(int riskMatrixProjectId);
        StageTahunRiskMatrix Get(int id);
        StageTahunRiskMatrixParam GetByRiskMatrixProjectId(int riskMatrixProjectId);
        //IEnumerable<StageTahunRiskMatrix> GetByRiskMatrixProjectId(int riskMatrixProjectId);
        int Add(StageTahunRiskMatrixParam param);
        int Update(int id, StageTahunRiskMatrixParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
    }
}
