﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Application
{
    public interface IMainDashboardService
    {
        MainDashboardResult GetAggregationOfProject();
        ReportMainDashboard GetReportMainDashboard();
    }
}
