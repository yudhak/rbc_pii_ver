﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface ICommentsService
    {
        IEnumerable<Comments> GetAll();
        IEnumerable<Comments> GetByColorId(int colorId);
        IEnumerable<Comments> GetByColorId(int colorId, string keyword);
        Comments Get(int id);
        int Add(CommentsParam param);
        int Update(int id, CommentsParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
        IEnumerable<Comments> GetByDefault();
    }
}
