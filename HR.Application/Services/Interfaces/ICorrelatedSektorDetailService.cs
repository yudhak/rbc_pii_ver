﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;


namespace HR.Application
{
    public interface ICorrelatedSektorDetailService
    {
        CorrelatedSektorDetailCollectionParam GetByCorrelatedSektorId(int correlatedSektorId);
        int Add(CorrelatedSektorDetailCollectionParam param);
        int Update2(int id, CorrelatedSektorDetailCollectionParam param);
        int Duplicate(CorrelatedSektorDetailCollectionParam param);
        int AddMurni(CorrelatedSektorDetailCollectionParam param);
    }
}
