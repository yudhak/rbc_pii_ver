﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface IStageService
    {
        IEnumerable<Stage> GetAll();
        IEnumerable<Stage> GetAll(string keyword, int id);
        Stage Get(int id);
        int Add(StageParam param);
        int Update(int id, StageParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
    }
}
