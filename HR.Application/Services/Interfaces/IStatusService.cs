﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface IStatusService
    {
        IEnumerable<Status> GetAll(string keyword);
        IEnumerable<Status> GetAll();
        Status Get(int id);

    }
}
