﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface IScenarioService
    {
        IEnumerable<Scenario> GetAll();
        IEnumerable<Scenario> GetAll(string keyword, int userId);
        IEnumerable<Scenario> GetAll(string keyword,int id, int userId);
        IEnumerable<Scenario> GetAll(string keyword, int id,int id2, int userId);
        Scenario Get(int id);
        Scenario GetWithProject(int id);
        int Add(ScenarioParam param);
        int Update(int id, ScenarioParam param);
        int Update2(int id, ScenarioParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
        int SetDefault(int id, int? updateBy, DateTime? updateDate);
        Scenario GetDefault();
        void RemoveDefault(int updateBy, DateTime updateDate);
        int Duplicate(ScenarioParam param);
        int UpdateScenarioMurni(int id, ScenarioParam param);
        
    }
}
