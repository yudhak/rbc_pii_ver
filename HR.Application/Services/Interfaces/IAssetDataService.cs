﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface IAssetDataService
    {
        IEnumerable<AssetData> GetAll();
        IEnumerable<AssetData> GetAll(string keyword, int id);
        AssetData Get(int id);
        int Add(AssetDataParam param);
        int Update(int id, AssetDataParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
    }
}
