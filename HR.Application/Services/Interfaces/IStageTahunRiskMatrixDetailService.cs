﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface IStageTahunRiskMatrixDetailService
    {
        IEnumerable<StageTahunRiskMatrixDetail> GetByStageTahunRiskMatrixId(int stageTahunRiskMatrixId);
        RiskMatrixCollectionParameter GetByRiskMatrixProjectId(int riskMatrixProjectId);
        //IEnumerable<StageTahunRiskMatrixDetail> GetByRiskMatrixProjectId(int riskMatrixProjectId);
        int Add(RiskMatrixCollectionParameter param);
        //int Add(StageTahunRiskMatrixDetailParam param);
        int Update(int id, RiskMatrixCollectionParameter param);
        int Update2(int id, RiskMatrixCollectionParameter param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
        int UpdateMurni(int id, RiskMatrixCollectionParameter param);
        int Duplicate(RiskMatrixCollectionParameter param, RiskMatrixProject riskMatrixProject);
    }
}
