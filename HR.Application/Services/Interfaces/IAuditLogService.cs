﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface IAuditLogService
    {
        IEnumerable<AuditLog> GetAll(int id, string awal, string akhir);
        IList<AuditLogParam2> GetAllConverter(IEnumerable<AuditLog> audits);
        AuditLog Get(int id);
        int Add(AuditLogParam param);

        //AssetData Audit
        int AddAssetDataAudit(AssetDataParam param, int id);
        int UpdateAssetDataAudit(AssetDataParam param, int id);
        int DeleteAssetDataAudit(int id, int deleteBy);
        //Comment
        int AddCommentAudit(CommentsParam param, int id);
        int UpdateCommentAudit(CommentsParam param, int id);
        int DeleteCommentAudit(int id, int deleteBy);
        //CorrelationMatrix
        int AddCorrelationMatrixAudit(CorrelationMatrixParam param, int id);
        int UpdateCorrelationMatrixAudit(CorrelationMatrixParam param, int id);
        int DeleteCorrelationMatrixAudit(int id, int deleteBy);
        //CorrelationProjectDetail
        int CorrelationProjectDetailAudit(CorrelatedProjectDetail param, CorrelationMatrix dataBaru, int modifBy);
        int AddCorrelationProjectDetailAudit(int id, int createBy);
        //CorrelationSektorDetail
        int CorrelationSektorDetailAudit(CorrelatedSektorDetail param, CorrelationMatrix dataBaru, int modifBy);
        int AddCorrelationSektorDetailAudit(int id, int createBy);
        //FunctionalRisk
        int AddFunctionalRiskAudit(FunctionalRiskParam param, int id);
        int UpdateFunctionalRiskAudit(FunctionalRiskParam param, int id);
        //Likehood
        int AddLikehoodAudit(LikehoodParam param, int id);
        int UpdateLikehoodAudit(LikehoodParam param, int id);
        int DeleteLikehoodAudit(int id, int deleteBy);
        int LikehoodSetDefault(Likehood model);
        int LikehoodRemoveDefault(Likehood model, int? updateBy);
        //LikehoodDetail
        int AddLikehoodDetailAudit(LikehoodDetailParam param, int id);
        int UpdateLikehoodDetailAudit(LikehoodDetailParam param, int id);
        //OverAllComments
        int AddOverAllCommentsAudit(OverAllCommentsParam param, int id);
        int UpdateOverAllCommentsAudit(OverAllCommentsParam param, int id);
        //PMN
        int AddPMNAudit(PMNParam param, int id);
        int UpdatePMNAudit(PMNParam param, int id);
        int DeletePMNAudit(int id, int deleteBy);
        //Project
        int AddProjectAudit(ProjectParam param, int id);
        int UpdateProjectAudit(ProjectParam param, int id);
        int DeleteProjectAudit(int id, int deleteBy);
        //ProjectRiskRegistrasi
        int AddProjectRiskRegistrasiAudit(IList<ProjectRiskRegistrasi> collection);
        //ProjectRiskStatus
        int AddProjectRiskStatusAudit(ProjectRiskStatus param, Project project);
        int UpdateProjectRiskStatusAudit(ProjectRiskStatus param, Project project, ProjectRiskStatus paramLama);
        int AddProjectRiskStatusAuditByUpdate(ProjectRiskStatus param, int? updateBy);
        //RiskMatrix
        int AddRiskMatrixAudit(Scenario param, int id);
        int DeleteRiskMatrixProjectAudit(int id, int deleteBy);
        //RiskRegistrasi
        int AddRiskRegistrasiAudit(RiskRegistrasiParam param, int id);
        int UpdateRiskRegistrasiAudit(RiskRegistrasiParam param, int id);
        int DeleteRiskRegistrasiAudit(int id, int deleteBy);
        //Scenario
        int AddScenarioAudit(ScenarioParam param, int id);
        int UpdateScenarioAudit(ScenarioParam param, int id);
        int DeleteScenarioAudit(int id, int deleteBy);
        int ScenarioSetDefault(Scenario model);
        int ScenarioRemoveDefault(Scenario model, int? updateBy);
        int ScenarioApproval(int id, int? updateBy, string dataAwal, string dataAkhir);
        //ScenarioDetail
        int AddScenarioDetailAudit(IList<ScenarioDetail> collection);
        int UpdateScenarioDetailAudit(ScenarioDetail param, Scenario scenario, ScenarioDetail param2);
        int AddScenarioDetailAuditByUpdate(ScenarioDetail param, int? updateBy);
        //Sektor
        int AddSektorAudit(SektorParam param, int id);
        int UpdateSektorAudit(SektorParam param, int id);
        int DeleteSektorAudit(int id, int deleteBy);
        //Stage
        int AddStageAudit(StageParam param, int id);
        int UpdateStageAudit(StageParam param, int id);
        int DeleteStageAudit(int id, int deleteBy);
        //StageTahunRiskMatrix
        int AddStageTahunRiskMatrixAudit(StageTahunRiskMatrix param);
        int UpdateStageTahunRiskMatrixAudit(string param, int id, int userr);
        int MaxStageTahunRiskMatrixAudit(int id, string valueLama, string valueBaru, int user);
        //StageTahunRiskMatrixDetail
        int UpdateStageTahunRiskMatrixDetailAudit(StageTahunRiskMatrixDetail param, StageTahunRiskMatrixDetail param2);
        int AddStageTahunRiskMatrixDetailAudit(int id, int update);
        //SubRiskRegistrasi
        int AddSubRiskRegistrasiAudit(SubRiskRegistrasiParam param, int id);
        int UpdateSubRiskRegistrasiAudit(SubRiskRegistrasiParam param, int id);
        int UpdateSubRiskRegistrasiAudit2(int id, int deleteBy);
        //Tahapan
        int AddTahapanAudit(TahapanParam param, int id);
        int UpdateTahapanAudit(TahapanParam param, int id);
        int DeleteTahapanDataAudit(int id, int deleteBy);

    }
}
