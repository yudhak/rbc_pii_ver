﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface IFunctionalRiskService
    {
        IEnumerable<FunctionalRisk> GetAll(string keyword, int id);
        IEnumerable<FunctionalRisk> GetByScenarioId(int scenarioId);
        FunctionalRisk Get(int id);
        int Add(FunctionalRiskAddParam param);
        int Update(int id, FunctionalRiskParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
    }
}
