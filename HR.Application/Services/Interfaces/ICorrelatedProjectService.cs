﻿using HR.Application.Params;
using HR.Domain;
using System.Collections.Generic;

namespace HR.Application
{
    public interface ICorrelatedProjectService
    {
        IEnumerable<CorrelatedProject> GetAll();
        IEnumerable<CorrelatedProject> GetByScenarioDefaultId(string keyword, int id);
        IEnumerable<CorrelatedProject> GetByScenarioDefaultId(string keyword, int id, int user);
        IEnumerable<CorrelatedProject> GetByScenarioDefaultId(string keyword, int id, int id2, int user);
        CorrelatedProject Get(int id);
        IEnumerable<Project> GetProjectByScenarioDefault(int scenarioId);
        IEnumerable<Project> GetProjectByScenarioDefault(int scenarioId, int user);
    }
}
