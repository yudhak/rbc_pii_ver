﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface IMasterApprovalCorrelatedProjectService
    {
        IEnumerable<MasterApprovalCorrelatedProject> GetAll();
        IEnumerable<MasterApprovalCorrelatedProject> GetAllByProjectId(int projectId);
        //IEnumerable<MasterApprovalCorrelatedProject> GetAll(string keyword);
        MasterApprovalCorrelatedProject Get(int id);
        int Add(MasterApprovalCorrelatedProjectParam param);
        int Update(int id, MasterApprovalCorrelatedProjectParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
        int DeleteByProjectId(int id, int deleteBy, DateTime deleteDate);
    }
}
