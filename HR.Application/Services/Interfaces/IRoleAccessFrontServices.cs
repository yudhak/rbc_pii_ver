﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface IRoleAccessFrontServices
    {
        IEnumerable<RoleAccessFront> GetAll(string keyword, int id);
        RoleAccessFront Get(int id);
        int Add(RoleAccessFrontParam param);
        int Update(int id, RoleAccessFrontParam param);
        int Delete(int id, int userId, DateTime getDate);
    }
}
