﻿using HR.Application.Params;
using System;

namespace HR.Application
{
    public interface ICorrelatedProjectDetailService
    {
        CorrelatedProjectDetailCollectionParam GetByCorrelatedProjectId(int correlatedProjectId);
        int Add(CorrelatedProjectDetailCollectionParam param);
        int Update2(int id, CorrelatedProjectDetailCollectionParam param);
        int SetIsApproved(int id, int updateBy, DateTime updateDate);
        int AddMurni(CorrelatedProjectDetailCollectionParam param);
        int Duplicate(CorrelatedProjectDetailCollectionParam param);
    }
}
