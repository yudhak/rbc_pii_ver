﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface ILikehoodService
    {
        IEnumerable<Likehood> GetAll();
        IEnumerable<Likehood> GetAll(string keyword);
        Likehood Get(int id);
        int Add(LikehoodParam param);
        int Update(int id, LikehoodParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
        int Default(int id, LikehoodParam param);
        Likehood GetDefault();
    }
}
