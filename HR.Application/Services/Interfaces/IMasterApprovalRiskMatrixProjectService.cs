﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface IMasterApprovalRiskMatrixProjectService
    {
        IEnumerable<RiskMatrixProject> GetAll();
        IEnumerable<MasterApprovalRiskMatrixProject> GetAllByProjectId(int projectId);
        //IEnumerable<MasterApprovalRiskMatrixProject> GetAll(string keyword);
        MasterApprovalRiskMatrixProject Get(int id);
        int Add(MasterApprovalRiskMatrixProjectParam param);
        int Update(int id, MasterApprovalRiskMatrixProjectParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
        int DeleteByProjectId(int id, int deleteBy, DateTime deleteDate);
    }
}
