﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface ITahapanService
    {
        IEnumerable<Tahapan> GetAll();
        IEnumerable<Tahapan> GetAll(string keyword, int id);
        Tahapan Get(int id);
        int Add(TahapanParam param);
        int Update(int id, TahapanParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
    }
}
