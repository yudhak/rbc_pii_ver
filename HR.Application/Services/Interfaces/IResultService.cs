﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Application
{
    public interface IResultService
    {
        Result GetAllDataResult(int Id, int projectId, int riskRegistrasiId, int sektorId);
    }
}
