﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface IRoleAccessServices
    {
        IEnumerable<RoleAccess> GetAll();
        RoleAccess Get(int id);
        int Add(RoleAccessParam param);
        int Update(int id, RoleAccessParam param);
    }
}
