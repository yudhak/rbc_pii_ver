﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface IMasterApprovalCorrelatedSektorService
    {
        IEnumerable<MasterApprovalCorrelatedSektor> GetAll();
        IEnumerable<MasterApprovalCorrelatedSektor> GetAllBySektorId(int sektorId);
        IEnumerable<MasterApprovalCorrelatedSektor> GetAllByMenuId(int sektorId);
        //IEnumerable<MasterApprovalCorrelatedSektor> GetAll(string keyword);
        MasterApprovalCorrelatedSektor Get(int id);
        MasterApprovalCorrelatedSektor GetBySektorIdUserId(int sektorId, int userId);
        int Add(MasterApprovalCorrelatedSektorParam param);
        int Update(int id, MasterApprovalCorrelatedSektorParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
        int DeleteByMenuId(int id, int deleteBy, DateTime deleteDate);
    }
}
