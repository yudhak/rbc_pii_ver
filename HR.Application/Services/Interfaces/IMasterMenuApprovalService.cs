﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface IMasterMenuApprovalService
    {
        IEnumerable<MasterMenuApproval> GetAll(string keyword);
        IEnumerable<MasterMenuApproval> GetAll();
        MasterMenuApproval Get(int id);

    }
}
