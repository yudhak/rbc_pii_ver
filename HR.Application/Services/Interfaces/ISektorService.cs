﻿using HR.Application.Params;
using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application
{
    public interface ISektorService
    {
        IEnumerable<Sektor> GetAll();
        IEnumerable<Sektor> GetAll(string keyword, int id);
        Sektor Get(int id);
        int Add(SektorParam param);
        int Update(int id, SektorParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
    }
}
