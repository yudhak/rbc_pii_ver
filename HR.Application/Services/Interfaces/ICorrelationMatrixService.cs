﻿using System;
using System.Collections.Generic;
using HR.Application.Params;
using HR.Domain;

namespace HR.Application
{
    public interface ICorrelationMatrixService
    {
        IEnumerable<CorrelationMatrix> GetAll();
        IEnumerable<CorrelationMatrix> GetAll(string keyword, int id);
        IEnumerable<CorrelationMatrix> GetAllAscNilai();
        CorrelationMatrix Get(int id);
        int Add(CorrelationMatrixParam param);
        int Update(int id, CorrelationMatrixParam param);
        int Delete(int id, int deleteBy, DateTime deleteDate);
    }
}
