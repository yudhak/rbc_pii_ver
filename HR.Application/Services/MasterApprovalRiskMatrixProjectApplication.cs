﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;

namespace HR.Application
{
    public class MasterApprovalRiskMatrixProjectService : IMasterApprovalRiskMatrixProjectService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICommentsRepository _commentsRepository;
        private readonly IColorCommentRepository _colorCommentRepository;
        private readonly IMasterApprovalRiskMatrixProjectRepository _masterApprovalRiskMatrixProjectRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly IAuditLogService _auditLogService;
        private readonly IUserRepository _userRepositoy;
        private readonly IMenuRepository _menuRepositoy;
        private readonly IRiskMatrixProjectRepository _riskMatrixProjectRepository;

        public MasterApprovalRiskMatrixProjectService(IUnitOfWork uow, ICommentsRepository commentsRepository, IColorCommentRepository colorCommentRepository,
            IMasterApprovalRiskMatrixProjectRepository masterApprovalRiskMatrixProjectRepository, IAuditLogService auditLogService, IUserRepository userRepository
            , IMenuRepository menuRepositoy, IProjectRepository projectRepository, IRiskMatrixProjectRepository riskMatrixProjectRepository)
        {
            _unitOfWork = uow;
            _commentsRepository = commentsRepository;
            _colorCommentRepository = colorCommentRepository;
            _masterApprovalRiskMatrixProjectRepository = masterApprovalRiskMatrixProjectRepository;
            _auditLogService = auditLogService;
            _userRepositoy = userRepository;
            _menuRepositoy = menuRepositoy;
            _projectRepository = projectRepository;
            _riskMatrixProjectRepository = riskMatrixProjectRepository;
        }

        #region Query
        public IEnumerable<RiskMatrixProject> GetAll()
        {
            return _riskMatrixProjectRepository.GetAll().ToList().Where(x => x.Scenario.IsDefault == true && x.Project.IsActive == true);
        }

        public IEnumerable<MasterApprovalRiskMatrixProject> GetAllByProjectId(int projectId)
        {
            return _masterApprovalRiskMatrixProjectRepository.GetAllByProjectId(projectId);
        }

        public MasterApprovalRiskMatrixProject Get(int id)
        {
            return _masterApprovalRiskMatrixProjectRepository.Get(id);
        }

        public void IsExistOnEditing(int id, int menuId, int projectId, int userId, int nomorUrutStatus)
        {
            if (_masterApprovalRiskMatrixProjectRepository.IsExist(id, menuId,projectId, userId, nomorUrutStatus))
            {
                throw new ApplicationException(string.Format("Master Approval Risk Matrix Project {0} sudah ada.", id));
            }
        }
  
        #endregion Query

        #region Manipulation 
        public int Add(MasterApprovalRiskMatrixProjectParam param)
        {
            //int id = 1;
            Validate.NotNull(param.UserIdList, "User wajib diisi.");

            if (param.UserIdList.Count() != 3)
            {
                throw new ApplicationException(string.Format("User Approval kurang lengkap (Harus 3 User)."));
            }
            if (param.UserIdList.Count() > 3)
            {
                throw new ApplicationException(string.Format("Jumlah maksimum User tidak sesuai (Maksimal 3 User)."));
            }

            IList<User> userlist = new List<User>();

            int[] nomorUrutStatusList = new int[3];

            int nomorUrutStatus = 1;

            foreach (var item in param.UserIdList)
            {
                var user = _userRepositoy.Get(item.GetValueOrDefault());
                Validate.NotNull(param.UserIdList, "User tidak ditemukan.");
                userlist.Add(user);
            }

            //for (int i = 0; i < param.UserIdList.Length; i++)
            //{
            //    var user = _userRepositoy.Get(param.UserIdList[i].GetValueOrDefault());
            //    Validate.NotNull(param.UserIdList, "User tidak ditemukan.");
            //    userlist.Add(user);
    
            //}

            var menu = _menuRepositoy.Get(param.MenuId.GetValueOrDefault());
            Validate.NotNull(menu, "Menu tidak ditemukan.");

            //var riskMatrixProject = _riskMatrixProjectRepository.Get(param.ProjectId.GetValueOrDefault());
            //Validate.NotNull(menu, "Risk Matrix Project tidak ditemukan.");

            var project = _projectRepository.Get(param.ProjectId.GetValueOrDefault());
            Validate.NotNull(project, "Project tidak ditemukan.");
            if (project.IsActive != true)
            {
                throw new ApplicationException(string.Format("Project tidak aktif."));
            }

            using (_unitOfWork)
            {
                IList<MasterApprovalRiskMatrixProject> masterApprovalRiskMatrixProjectList = new List<MasterApprovalRiskMatrixProject>();

                foreach (var item in userlist)
                {
                    MasterApprovalRiskMatrixProject masterApprovalRiskMatrixProject = new MasterApprovalRiskMatrixProject(menu, project, item, nomorUrutStatus, param.CreateBy, param.CreateDate);
                    masterApprovalRiskMatrixProjectList.Add(masterApprovalRiskMatrixProject);
                    nomorUrutStatus++;
                }
                _masterApprovalRiskMatrixProjectRepository.Insert(masterApprovalRiskMatrixProjectList);


                _unitOfWork.Commit();

            }

            return project.Id;
        }

        public int Update(int projectId, MasterApprovalRiskMatrixProjectParam param)
        {
            //kalau bisa tambahkan validasi
            var project = _projectRepository.Get(param.ProjectId.GetValueOrDefault());
            Validate.NotNull(project, "Project tidak ditemukan.");
            if (project.IsActive != true)
            {
                throw new ApplicationException(string.Format("Project tidak aktif."));
            }

            var model = this.GetAllByProjectId(project.Id);
            Validate.NotNull(model, "Master Approval Risk Matrix Project tidak ditemukan.");

            if (model.Count() == 0)
            {
                throw new ApplicationException(string.Format("Master Approval Risk Matrix Project tidak ditemukan."));
            }

            Validate.NotNull(param.UserIdList, "User wajib diisi.");

            if (param.UserIdList.Count() != 3)
            {
                throw new ApplicationException(string.Format("User Approval kurang lengkap (Harus 3 User)."));
            }
            if (param.UserIdList.Count() > 3)
            {
                throw new ApplicationException(string.Format("Jumlah maksimum User tidak sesuai (Maksimal 3 User)."));
            }

            IList<User> userlist = new List<User>();


            foreach (var item in param.UserIdList)
            {
                var user = _userRepositoy.Get(item.GetValueOrDefault());
                Validate.NotNull(param.UserIdList, "User tidak ditemukan.");
                userlist.Add(user);
            }

            //var menu = _menuRepositoy.Get(param.MenuId.GetValueOrDefault());
            //Validate.NotNull(menu, "Menu tidak ditemukan.");

            using (_unitOfWork)
            {
                foreach (var item in model)
                {
                    item.Update(userlist[item.NomorUrutStatus.GetValueOrDefault() - 1], param.UpdateBy, param.UpdateDate);
                    _masterApprovalRiskMatrixProjectRepository.Update(item);
                }

                //Audit Log UPDATE 
                //int audit = _auditLogService.UpdateCommentAudit(param, id);

                _unitOfWork.Commit();
                // id = model.Id;
            }
            return project.Id;

        }

        public int Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Master Approval Risk Matrix Project tidak ditemukan.");



            using (_unitOfWork)
            {
                model.Delete(deleteBy, deleteDate);
                _masterApprovalRiskMatrixProjectRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;

                //Audit Log DELETE
                //int audit = _auditLogService.DeleteCommentAudit(id, deleteBy);
            }
            return id;
        }


        public int DeleteByProjectId(int projectId, int deleteBy, DateTime deleteDate)
        {
            var model = this.GetAllByProjectId(projectId);
            Validate.NotNull(model, "Master Approval Risk Matrix Project tidak ditemukan.");

            if (model.Count() == 0)
            {
                throw new ApplicationException(string.Format("Master Approval Risk Matrix Project tidak ditemukan."));
            }

            using (_unitOfWork)
            {
                foreach (var item in model)
                {
                    item.Delete(deleteBy, deleteDate);
                    _masterApprovalRiskMatrixProjectRepository.Update(item);
                }
                
                _unitOfWork.Commit();

                //Audit Log DELETE
                //int audit = _auditLogService.DeleteCommentAudit(id, deleteBy);
            }
            return projectId;
        }
        #endregion Manipulation
    }
}
