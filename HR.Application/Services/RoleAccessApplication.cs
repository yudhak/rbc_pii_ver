﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;
using HR.Infrastructure;
using HR.Common;

namespace HR.Application
{
    public class RoleAccessService : IRoleAccessServices
    {
        private readonly IRoleAccessRepository _roleAccessRepository;
        private readonly IMenuRepository _menuRepository;
        private readonly IUnitOfWork _unitOfWork;

        public RoleAccessService(IRoleAccessRepository roleAccessRepository, IMenuRepository menuRepository, IUnitOfWork unitOfWork)
        {
            _roleAccessRepository = roleAccessRepository;
            _unitOfWork = unitOfWork;
            _menuRepository = menuRepository;
        }

        #region Query
        public IEnumerable<RoleAccess> GetAll()
        {
            return _roleAccessRepository.GetAll();
        }

        public RoleAccess Get(int id)
        {
            return _roleAccessRepository.Get(id);
        }

        public IEnumerable<Menu> GetSimilarName(int id)
        {
            return _menuRepository.GetSimilarName(id);
        }
        #endregion Query

        #region Manipulation 
        public int Add(RoleAccessParam param)
        {
            int id = 0;
            RoleAccess model = new RoleAccess(param.RoleId, param.MenuId, param.CreateBy, param.CreateDate, param.IsActive, param.FlagFromFront);
            _roleAccessRepository.Insert(model);
            return id;
        }

        public int Update(int id, RoleAccessParam param)
        {
            return id;
        }
        #endregion Manipulation
    }
}
