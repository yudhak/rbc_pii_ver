﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;
using HR.Infrastructure;

namespace HR.Application
{
    public class RiskMatrixProjectService : IRiskMatrixProjectService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRiskMatrixProjectRepository _riskMatrixProjectRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly IRiskMatrixRepository _riskMatrixRepository;
        private readonly IScenarioRepository _scenarioRepository;
        private readonly IDatabaseContext _databaseContext;

        public RiskMatrixProjectService(IUnitOfWork uow, IRiskMatrixProjectRepository riskMatrixProjectRepository, IProjectRepository projectRepository, 
            IRiskMatrixRepository riskMatrixRepository, IScenarioRepository scenarioRepository, IDatabaseContext databaseContext)
        {
            _unitOfWork = uow;
            _riskMatrixProjectRepository = riskMatrixProjectRepository;
            _projectRepository = projectRepository;
            _riskMatrixRepository = riskMatrixRepository;
            _scenarioRepository = scenarioRepository;
            _databaseContext = databaseContext;
        }

        #region Query
        public IEnumerable<RiskMatrixProject> GetAll()
        {
            return _riskMatrixProjectRepository.GetAll().ToList().Where(x => x.Scenario.IsDefault == true);
        }

        public IEnumerable<RiskMatrixProject> GetAll(string keyword, int id, int userId)
        {
            //return _riskMatrixProjectRepository.GetAll().ToList().Where(x => x.Scenario.IsDefault == true);
            var User = _databaseContext.Users.Where(x => x.Id == userId).SingleOrDefault();
            if (User.RoleId == 5 || User.RoleId == 3)
            {
                return _riskMatrixProjectRepository.GetAll(keyword, id).ToList().Where(x => x.Scenario.IsDelete == false && x.Scenario.StatusId != 4);
            }
            else
            {
                return _riskMatrixProjectRepository.GetAll(keyword, id).ToList().Where(x => x.Scenario.IsDelete == false && x.Project.UserId == userId && x.Scenario.IsDefault == true && x.Scenario.StatusId != 4);
            }
        }

        public IEnumerable<RiskMatrixProject> GetAll(string keyword, int id,int id2, int userId)
        {
            //return _riskMatrixProjectRepository.GetAll().ToList().Where(x => x.Scenario.IsDefault == true);
            var User = _databaseContext.Users.Where(x => x.Id == userId).SingleOrDefault();
            if (User.RoleId == 5 || User.RoleId == 3)
            {
                return _riskMatrixProjectRepository.GetAll(keyword, id, id2).ToList().Where(x => x.Scenario.IsDelete == false && x.Scenario.StatusId != 4);
            }
            else
            {
                return _riskMatrixProjectRepository.GetAll(keyword, id).ToList().Where(x => x.Scenario.IsDelete == false && x.Project.UserId == userId && x.Scenario.IsDefault == true && x.Scenario.StatusId != 4);
            }
        }

        public IEnumerable<RiskMatrixProject> GetAll2(string keyword, int id, int userId)
        {
            var User = _databaseContext.Users.Where(x => x.Id == userId).SingleOrDefault();
            if (User.RoleId == 1 )
            {
                return _riskMatrixProjectRepository.GetAll(keyword, id).ToList().Where(x => x.Scenario.IsDelete == false);
                //return _riskMatrixProjectRepository.GetAll(keyword, id).ToList();
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<RiskMatrixProject> GetAll(string keyword)
        {
            return _riskMatrixProjectRepository.GetAll(keyword, 0).ToList().Where(x => x.Scenario.IsDelete == false && x.Scenario.IsDefault == true);
        }
        public IEnumerable<RiskMatrixProject> GetAllBaseId(string keyword, int userId)
        {
            IEnumerable<RiskMatrixProject> riskMatrixListDraft = _databaseContext.RiskMatrixProjects.Where(x => x.IsDelete == false && x.Scenario.IsDefault == true && x.StatusId != null).OrderByDescending(x => x.Id).ToList();
            IList<RiskMatrixProject> result = new List<RiskMatrixProject>();
            IList<RiskMatrixProject> riskMatrixList = _databaseContext.RiskMatrixProjects.Where(x => x.IsDelete == false && x.Scenario.IsDefault == true && x.StatusId == null).OrderByDescending(x => x.Id).ToList();
            foreach (var item in riskMatrixListDraft.ToList())
            {
                riskMatrixList.Add(item);
            }

            //return _riskMatrixProjectRepository.GetAll(keyword).ToList().Where(x => x.Scenario.IsDefault == true && x.Scenario.IsDelete == false);
            return riskMatrixList;

        }

        public IEnumerable<RiskMatrixProject> GetAllData()
        {
            return _riskMatrixProjectRepository.GetAllData();
        }
        public IEnumerable<RiskMatrixProject> GetAllData(string keyword)
        {
            return _riskMatrixProjectRepository.GetAllData(keyword);
        }

        public RiskMatrixProject Get(int id)
        {
            return _riskMatrixProjectRepository.Get(id);
        }

        public void IsExistOnEditing(int id, int projectId)
        {
            if (_riskMatrixProjectRepository.IsExist(id, projectId))
            {
                throw new ApplicationException(string.Format("Project already exist.", projectId));
            }
        }

        public void isExistOnAdding(int projectId)
        {
            if (_riskMatrixProjectRepository.IsExist(projectId))
            {
                throw new ApplicationException(string.Format("Project {0} already exist.", projectId));
            }
        }
        #endregion Query

        #region Manipulation 
        public int Add(RiskMatrixProjectParam param)
        {
            int id;
            var project = _projectRepository.Get(param.ProjectId);
            var riskMatrix = _riskMatrixRepository.Get(param.RiskMatrixId);
            var scenario = _scenarioRepository.Get(param.ScenarioId);
            Validate.NotNull(param.ProjectId, "Project wajib diisi.");

            isExistOnAdding(param.ProjectId);
            using (_unitOfWork)
            {
                RiskMatrixProject model = new RiskMatrixProject(project, riskMatrix, scenario, param.CreateBy, param.CreateDate);
                _riskMatrixProjectRepository.Insert(model);
                _unitOfWork.Commit();
                id = model.Id;
            }

            return id;
        }

        public int Update(int id, RiskMatrixProjectParam param)
        {
            var model = this.Get(id);
            var project = _projectRepository.Get(param.ProjectId);
            var riskMatrix = _riskMatrixRepository.Get(param.RiskMatrixId);
            var scenario = _scenarioRepository.Get(param.ScenarioId);
            Validate.NotNull(model, "Project tidak ditemukan.");

            IsExistOnEditing(id, param.ProjectId);
            using (_unitOfWork)
            {
                model.Update(project, riskMatrix, scenario, param.UpdateBy, param.UpdateDate);
                _riskMatrixProjectRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;
            }
            return id;
        }
        
        public int Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Nama Category Risk tidak ditemukan.");

            using (_unitOfWork)
            {
                model.Delete(deleteBy, deleteDate);
                _riskMatrixProjectRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;
            }
            return id;
        }

        public IEnumerable<RiskMatrixProject> GetAllProjectActive()
        {
            return _riskMatrixProjectRepository.GetAll().ToList().Where(x => x.Scenario.IsDefault == true && x.Project.IsActive == true);
        }

        public IEnumerable<RiskMatrixProject> GetAllProjectActive(string keyword, int id, int userId)
        {
            var User = _databaseContext.Users.Where(x => x.Id == userId).SingleOrDefault();
            if (User.RoleId == 5 || User.RoleId == 3)
            {
                return _riskMatrixProjectRepository.GetAll(keyword, id).ToList().Where(x => x.Scenario.IsDefault == true && x.Scenario.IsDelete == false);
            }
            else
            {
                return _riskMatrixProjectRepository.GetAll(keyword, id).ToList().Where(x => x.Scenario.IsDelete == false && x.Project.UserId == userId && x.Scenario.IsDefault == true);
            }
        }

        public IEnumerable<RiskMatrixProject> GetAllProjectActive(string keyword)
        {
            return _riskMatrixProjectRepository.GetAll(keyword).ToList().Where(x => x.Scenario.IsDelete == false && x.Scenario.IsDefault == true);
        }
        #endregion Manipulation
    }
}
