﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;
using HR.Infrastructure;
using HR.Common;

namespace HR.Application
{
    public class AuditLogService : IAuditLogService
    {       
        private readonly IAuditLogRepository _auditLogRepository;
        private readonly IDatabaseContext _databaseContext;
        private readonly IUserRepository _userRepository;

        public AuditLogService(IAuditLogRepository auditLogRepository, IDatabaseContext databaseContext, IUserRepository userRepository)
        {          
            _auditLogRepository = auditLogRepository;
            _databaseContext = databaseContext;
            _userRepository = userRepository;
        }

        #region Query
        public IEnumerable<AuditLog> GetAll(int id, string awal, string akhir)
        {
            var result = _auditLogRepository.GetAll(id);
            if (!string.IsNullOrEmpty(awal))
            {
                int tahun = Convert.ToInt32(awal.Substring(0, 4));
                int bulan = Convert.ToInt32(awal.Substring(4, 2));
                int tanggal = Convert.ToInt32(awal.Substring(6,2));
                DateTime date = new DateTime(tahun, bulan, tanggal);
                result = result.Where(x => x.LogTimestamp >= date);
            }
            if (!string.IsNullOrEmpty(akhir))
            {
                int tahun = Convert.ToInt32(akhir.Substring(0, 4));
                int bulan = Convert.ToInt32(akhir.Substring(4, 2));
                int tanggal = Convert.ToInt32(akhir.Substring(6, 2));
                DateTime date = new DateTime(tahun, bulan, tanggal);
                TimeSpan ts = new TimeSpan(23, 59, 0);
                date = date + ts;
                result = result.Where(x => x.LogTimestamp <= date);
            }
            return result;
        }

        public AuditLog Get(int id)
        {
            return _auditLogRepository.Get(id);
        }        

        public IList<AuditLogParam2> GetAllConverter(IEnumerable<AuditLog> audits)
        {

            IList<AuditLogParam2> colls = new List<AuditLogParam2>();
            foreach (var item in audits)
            {
                var userAudit = _userRepository.Get(Convert.ToInt32(item.ModifiedBy));
                var waktu = item.LogTimestamp.ToString("dd MMM yyyy HH:mm:ss");
                string namaObjek = "";
                string namaMenu = "";
                string aksi = "";
                if (item.MenuId < 4 && item.MenuId > 0)
                {
                    var data = _databaseContext.ProjectRiskStatus.Where(x => x.Id == item.DataObjekId).Single();
                    var dataProject = _databaseContext.Projects.Where(x => x.Id == data.ProjectId).Single();
                    namaObjek = dataProject.NamaProject+" - " + data.NamaCategoryRisk;
                    namaMenu = "Master-Data - Proyek";
                    if (item.MenuId == 1)
                        aksi = "Tambah";
                    if (item.MenuId == 2)
                        aksi = "Ubah";
                    if (item.MenuId == 3)
                        aksi = "Hapus";
                }
                if (item.MenuId < 6 && item.MenuId > 3)
                {
                    var data = _databaseContext.ScenarioDetails.Where(x => x.Id == item.DataObjekId).Single();
                    var skenari = _databaseContext.Scenarios.Where(x => x.Id == data.ScenarioId).Single();
                    namaObjek = skenari.NamaScenario + " - "+ data.Project.NamaProject;
                    namaMenu = "Skenario";
                    aksi = "Ubah";
                }
                if (item.MenuId < 27 && item.MenuId > 20)
                {
                    var data = _databaseContext.RiskRegistrasis.Where(x => x.Id == item.DataObjekId).Single();
                    namaObjek = data.NamaCategoryRisk;
                    namaMenu = "Master-Data - Kategori Risiko";
                    if (item.MenuId == 24)
                        aksi = "Tambah";
                    if (item.MenuId == 25)
                        aksi = "Ubah";
                    if (item.MenuId == 26)
                        aksi = "Hapus";
                }
                if (item.MenuId < 32 && item.MenuId > 28)
                {
                    var data = _databaseContext.SubRiskRegistrasis.Where(x => x.Id == item.DataObjekId).Single();
                    namaObjek = data.KodeRisk;
                    namaMenu = "Master-Data - Kategori Resiko - Info";
                    if (item.MenuId == 29)
                        aksi = "Tambah";
                    if (item.MenuId == 30)
                        aksi = "Ubah";
                    if (item.MenuId == 31)
                        aksi = "Hapus";
                }
                if (item.MenuId < 37 && item.MenuId > 32)
                {
                    var data = _databaseContext.Sektors.Where(x => x.Id == item.DataObjekId).Single();
                    namaObjek = data.NamaSektor;
                    namaMenu = "Master-Data - Sektor";
                    if (item.MenuId == 34)
                        aksi = "Tambah";
                    if (item.MenuId == 35)
                        aksi = "Ubah";
                    if (item.MenuId == 36)
                        aksi = "Hapus";
                }
                if (item.MenuId < 42 && item.MenuId > 37)
                {
                    var data = _databaseContext.Projects.Where(x => x.Id == item.DataObjekId).Single();
                    namaObjek = data.NamaProject;
                    namaMenu = "Master-Data - Proyek";
                    if (item.MenuId == 39)
                        aksi = "Tambah";
                    if (item.MenuId == 40)
                        aksi = "Ubah";
                    if (item.MenuId == 41)
                        aksi = "Hapus";
                }
                if (item.MenuId < 47 && item.MenuId > 43)
                {
                    var data = _databaseContext.Likehoods.Where(x => x.Id == item.DataObjekId).Single();
                    namaObjek = data.NamaLikehood;
                    namaMenu = "Master-Data - Parameter";
                    if (item.MenuId == 44)
                        aksi = "Tambah";
                    if (item.MenuId == 45)
                        aksi = "Ubah";
                    if (item.MenuId == 46)
                        aksi = "Hapus";
                }
                if (item.MenuId == 49)
                {
                    var data = _databaseContext.LikehoodDetails.Where(x => x.Id == item.DataObjekId).Single();
                    var likeli = _databaseContext.Likehoods.Where(x => x.Id == data.LikehoodId).Single();
                    namaObjek = likeli.NamaLikehood + " - "+ data.DefinisiLikehood;
                    namaMenu = "Master-Data - Parameter - Info";
                    aksi = "Ubah";
                }
                if (item.MenuId < 56 && item.MenuId > 51)
                {
                    var data = _databaseContext.AssetDatas.Where(x => x.Id == item.DataObjekId).Single();
                    namaObjek = data.AssetClass;
                    namaMenu = "Master-Data - Asset Data";
                    if (item.MenuId == 52)
                        aksi = "Tambah";
                    if (item.MenuId == 53)
                        aksi = "Ubah";
                    if (item.MenuId == 54)
                        aksi = "Hapus";
                }
                if (item.MenuId < 60 && item.MenuId > 56)
                {
                    var data = _databaseContext.PMNs.Where(x => x.Id == item.DataObjekId).Single();
                    namaObjek = data.PMNToModalDasarCap.ToString();
                    namaMenu = "Master-Data - Asset Data";
                    if (item.MenuId == 57)
                        aksi = "Tambah";
                    if (item.MenuId == 58)
                        aksi = "Ubah";
                    if (item.MenuId == 59)
                        aksi = "Hapus";
                }
                if (item.MenuId < 65 && item.MenuId > 61)
                {
                    var data = _databaseContext.Tahapans.Where(x => x.Id == item.DataObjekId).Single();
                    namaObjek = data.NamaTahapan;
                    namaMenu = "Master-Data - Tahapan Penjaminan";
                    if (item.MenuId == 62)
                        aksi = "Tambah";
                    if (item.MenuId == 63)
                        aksi = "Ubah";
                    if (item.MenuId == 64)
                        aksi = "Hapus";
                }
                if (item.MenuId < 70 && item.MenuId > 66)
                {
                    var data = _databaseContext.Stages.Where(x => x.Id == item.DataObjekId).Single();
                    namaObjek = data.NamaStage;
                    namaMenu = "Master-Data - Tahapan Proyek";
                    if (item.MenuId == 67)
                        aksi = "Tambah";
                    if (item.MenuId == 68)
                        aksi = "Ubah";
                    if (item.MenuId == 69)
                        aksi = "Hapus";
                }
                if (item.MenuId < 77 && item.MenuId > 73)
                {
                    var data = _databaseContext.Commentss.Where(x => x.Id == item.DataObjekId).Single();
                    namaObjek = data.Comment;
                    namaMenu = "Master-Data - Komentar - Info";
                    if (item.MenuId == 74)
                        aksi = "Tambah";
                    if (item.MenuId == 75)
                        aksi = "Ubah";
                    if (item.MenuId == 76)
                        aksi = "Hapus";
                }
                if (item.MenuId < 82 && item.MenuId > 78)
                {
                    var data = _databaseContext.CorrelationMatrixs.Where(x => x.Id == item.DataObjekId).Single();
                    namaObjek = data.NamaCorrelationMatrix;
                    namaMenu = "Master-Data - Matriks Korelasi";
                    if (item.MenuId == 79)
                        aksi = "Tambah";
                    if (item.MenuId == 80)
                        aksi = "Ubah";
                    if (item.MenuId == 81)
                        aksi = "Hapus";
                }
                if (item.MenuId == 84)
                {
                    var data = _databaseContext.FunctionalRisks.Where(x => x.Id == item.DataObjekId).Single();
                    namaObjek = data.Scenario.NamaScenario+ " - " + data.ColorComment.Warna + " - " + data.Matrix.NamaMatrix;
                    namaMenu = "Master - Treshold";
                    if (item.MenuId == 84)
                        aksi = "Ubah";
                }
                if (item.MenuId < 91 && item.MenuId > 87)
                {
                    var data = _databaseContext.Scenarios.Where(x => x.Id == item.DataObjekId).Single();
                    namaObjek = data.NamaScenario;
                    namaMenu = "Skenario";
                    if (item.MenuId == 88)
                        aksi = "Tambah";
                    if (item.MenuId == 89)
                        aksi = "Ubah";
                    if (item.MenuId == 90)
                        aksi = "Hapus";
                }
                if (item.MenuId ==94)
                {
                    var data = _databaseContext.StageTahunRiskMatrixs.Where(x => x.Id == item.DataObjekId).Single();
                    namaObjek = data.RiskMatrixProject.Scenario.NamaScenario + " - " + data.RiskMatrixProject.Project.NamaProject + " - " + data.Stage.NamaStage;
                    namaMenu = "Matriks Risiko";
                    aksi = "Tambah";                    
                }
                if (item.MenuId < 96 && item.MenuId > 94)
                {
                    var data = _databaseContext.StageTahunRiskMatrixs.Where(x => x.Id == item.DataObjekId).Single();
                    namaObjek = data.RiskMatrixProject.Scenario.NamaScenario + " - " + data.RiskMatrixProject.Project.NamaProject + " - " + data.Stage.NamaStage +  ", " + data.Tahun;
                    namaMenu = "Matriks Risiko";
                    if (item.MenuId == 95)
                        aksi = "Ubah";
                    if (item.MenuId == 96)
                        aksi = "Hapus";
                }
                if (item.MenuId == 98)
                {
                    var data = _databaseContext.RiskMatrixProjects.Where(x => x.Id == item.DataObjekId).Single();
                    namaObjek = data.Scenario.NamaScenario + " - " + data.Project.NamaProject;
                    namaMenu = "Matriks Risiko";
                    if (item.MenuId == 98)
                        aksi = "Tambah";
                }
                if (item.MenuId == 99)
                {
                    var data = _databaseContext.StageTahunRiskMatrixDetails.Where(x => x.Id == item.DataObjekId).Single();
                    namaObjek = data.StageTahunRiskMatrix.RiskMatrixProject.Scenario.NamaScenario + " - " + data.StageTahunRiskMatrix.RiskMatrixProject.Project.NamaProject + " - " + "(" + data.StageTahunRiskMatrix.Tahun + "," + data.RiskRegistrasi.KodeMRisk + ")";
                    namaMenu = "Matriks Risiko";
                    if (item.MenuId == 99)
                        aksi = "Ubah";
                }
                if (item.MenuId == 107)
                {
                    var data = _databaseContext.CorrelatedSektors.Where(x => x.Id == item.DataObjekId).Single();
                    var skenario = _databaseContext.Scenarios.Where(x => x.Id == data.ScenarioId).Single();
                    var sektor = _databaseContext.Sektors.Where(x => x.Id == data.SektorId).Single();
                    namaObjek = skenario.NamaScenario + " - " + sektor.NamaSektor;
                    namaMenu = "Korelasi - Korelasi Kategori Risiko";
                    if (item.MenuId == 107)
                        aksi = "Tambah";
                }
                if (item.MenuId == 108)
                {
                    var data = _databaseContext.CorrelatedSektorDetails.Where(x => x.Id == item.DataObjekId).Single();
                    var skenario = _databaseContext.Scenarios.Where(x => x.Id == data.CorrelatedSektor.ScenarioId).Single();
                    var sektor = _databaseContext.Sektors.Where(x => x.Id == data.CorrelatedSektor.SektorId).Single();
                    namaObjek = skenario.NamaScenario + " - " + sektor.NamaSektor + " - (Baris " + data.RiskRegistrasiIdRow + ", Kolom " + data.RiskRegistrasiIdCol + ")"; ;
                    namaMenu = "Korelasi - Korelasi Kategori Risiko";
                    if (item.MenuId == 108)
                        aksi = "Ubah";
                }
                if (item.MenuId == 115)
                {
                    var data = _databaseContext.CorrelatedProjects.Where(x => x.Id == item.DataObjekId).Single();
                    var skenario = _databaseContext.Scenarios.Where(x => x.Id == data.ScenarioId).Single();
                    var sektor = _databaseContext.Projects.Where(x => x.Id == data.ProjectId).Single();
                    namaObjek = skenario.NamaScenario + " - " + sektor.NamaProject;
                    namaMenu = "Korelasi - Korelasi Proyek";
                    if (item.MenuId == 115)
                        aksi = "Tambah";
                }
                if (item.MenuId == 116)
                {
                    var data = _databaseContext.CorrelatedProjectDetails.Where(x => x.Id == item.DataObjekId).Single();
                    var skenario = _databaseContext.Scenarios.Where(x => x.Id == data.CorrelatedProject.ScenarioId).Single();
                    var sektor = _databaseContext.Projects.Where(x => x.Id == data.CorrelatedProject.ProjectId).Single();
                    namaObjek = skenario.NamaScenario + " - " + sektor.NamaProject + " - (Baris " + data.ProjectIdRow + ", Kolom " + data.ProjectIdCol + ")"; ;
                    namaMenu = "Korelasi - Korelasi Proyek";
                    if (item.MenuId == 116)
                        aksi = "Ubah";
                }
                if (item.MenuId == 121)
                {
                    var data = _databaseContext.OverAllCommentss.Where(x => x.Id == item.DataObjekId).Single();
                    namaObjek = data.ColorComment.Warna;
                    namaMenu = "Komentar Kesuluruhan";
                    if (item.MenuId == 121)
                        aksi = "Tambah";
                }
                if (item.MenuId == 122)
                {
                    var data = _databaseContext.OverAllCommentss.Where(x => x.Id == item.DataObjekId).Single();
                    namaObjek = data.ColorComment.Warna;
                    namaMenu = "Komentar Kesuluruhan";
                    if (item.MenuId == 122)
                        aksi = "Ubah";
                }

                AuditLogParam2 model = new AuditLogParam2
                {
                    ColumnModified = item.ColumnModified,
                    DataAkhir = item.DataAkhir,
                    DataAwal = item.DataAwal,
                    DataObjekId = namaObjek,
                    Id = item.Id,
                    LogTimestamp = waktu,
                    MenuId = namaMenu,
                    ModifiedBy = userAudit.UserName,
                    TableModified = item.TableModified,
                    Aksi = aksi
                };
                colls.Add(model);
            }
            return colls;
        }
        #endregion Query

        #region Manipulation 
        public int Add(AuditLogParam param)
        {
            int id;                       
            AuditLog model = new AuditLog(param.MenuId, param.TableModified, param.DataObjekId, param.ColumnModified, param.DataAwal, param.DataAkhir, param.LogTimestamp, param.ModifiedBy);
            _auditLogRepository.Insert(model);
            _databaseContext.SaveChanges();
            id = model.Id;           
            return id;
        }
        public int Add2(AuditLogParam param)
        {
            int id;
            AuditLog model = new AuditLog(param.MenuId, param.TableModified, param.DataObjekId, param.ColumnModified, param.DataAwal, param.DataAkhir, param.LogTimestamp, param.ModifiedBy);
            _auditLogRepository.Insert(model);
            id = model.Id;
            return id;
        }
        #endregion Manipulation

        #region TranslateParameter AssetData
        public int AddAssetDataAudit(AssetDataParam param, int id)
        {
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate")
                {
                    var dataAkhir = paramBaru.ToString();
                    if (col.Name == "AssumentReturn" || col.Name == "AssetValue" || col.Name == "Porpotion" || col.Name == "AssumedReturnPercentage" || col.Name == "AssumedReturn")
                    {
                        dataAkhir = String.Format("{0:0.0000000000000}", paramBaru);
                    }
                    var auditParam = new AuditLogParam
                    {
                        MenuId = Convert.ToInt32(EventLogType.AsetData),
                        TableModified = "tblAssetData",
                        DataObjekId = id,
                        ColumnModified = col.Name,
                        DataAwal = "",
                        DataAkhir = dataAkhir,
                        LogTimestamp = DateHelper.GetDateTime(),
                        ModifiedBy = param.CreateBy
                    };
                    int audit = Add(auditParam);
                }                
            }
            return id;
        }

        public int UpdateAssetDataAudit(AssetDataParam param, int id)
        {
            var data = _databaseContext.AssetDatas.SingleOrDefault(x => x.Id == id);
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                var paramLama = data.GetType().GetProperty(col.Name).GetValue(data, null);
                var dataAwal = "";
                if (paramBaru != null && col.Name != "UpdateBy" && col.Name != "UpdateDate")
                {
                    var dataAkhir = paramBaru.ToString();
                    if (col.Name == "AssumentReturn" || col.Name == "AssetValue" || col.Name == "Porpotion" || col.Name == "AssumedReturnPercentage" || col.Name == "AssumedReturn")
                    {
                        dataAkhir = String.Format("{0:0.0000000000000}", paramBaru);
                    }
                    if (paramLama != null)
                    {
                        dataAwal = paramLama.ToString();
                    }
                    if (dataAkhir != dataAwal)
                    {
                        var auditParam = new AuditLogParam
                        {
                            MenuId = Convert.ToInt32(EventLogType.AsetDataUpdate),
                            TableModified = "tblAssetData",
                            DataObjekId = id,
                            ColumnModified = col.Name,
                            DataAwal = dataAwal,
                            DataAkhir = dataAkhir,
                            LogTimestamp = DateHelper.GetDateTime(),
                            ModifiedBy = param.UpdateBy
                        };
                        int audit = Add(auditParam);
                    }
                }                
            }
            return id;
        }

        public int DeleteAssetDataAudit(int id, int deleteBy)
        {
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.AsetDataDelete),
                TableModified = "tblAssetData",
                DataObjekId = id,
                ColumnModified = "IsDeleted",
                DataAwal = "false",
                DataAkhir = "true",
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = deleteBy
            };
            int audit = Add(auditParam);
            return id;
        }
        #endregion TranslateParameter AssetData

        #region TranslateParameter Comment
        public int AddCommentAudit(CommentsParam param, int id)
        {
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate" && col.Name != "ColorCommentId")
                {
                    var auditParam = new AuditLogParam
                    {
                        MenuId = Convert.ToInt32(EventLogType.KomentarDetail),
                        TableModified = "tblComments",
                        DataObjekId = id,
                        ColumnModified = col.Name,
                        DataAwal = "",
                        DataAkhir = paramBaru.ToString(),
                        LogTimestamp = DateHelper.GetDateTime(),
                        ModifiedBy = param.CreateBy
                    };
                    int audit = Add(auditParam);
                }
            }
            return id;
        }

        public int UpdateCommentAudit(CommentsParam param, int id)
        {
            var data = _databaseContext.Commentss.SingleOrDefault(x => x.Id == id);
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                var paramLama = data.GetType().GetProperty(col.Name).GetValue(data, null);
                var dataAwal = "";
                if (paramBaru != null && col.Name != "UpdateBy" && col.Name != "UpdateDate")
                {
                    var dataAkhir = paramBaru.ToString();
                    if (paramLama != null)
                    {
                        dataAwal = paramLama.ToString();
                    }
                    if (dataAkhir != dataAwal)
                    {
                        var auditParam = new AuditLogParam
                        {
                            MenuId = Convert.ToInt32(EventLogType.KomentarDetailUpdate),
                            TableModified = "tblComments",
                            DataObjekId = id,
                            ColumnModified = col.Name,
                            DataAwal = dataAwal,
                            DataAkhir = dataAkhir,
                            LogTimestamp = DateHelper.GetDateTime(),
                            ModifiedBy = param.UpdateBy
                        };
                        int audit = Add(auditParam);
                    }
                }
            }
            return id;
        }

        public int DeleteCommentAudit(int id, int deleteBy)
        {
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.KomentarDetailDelete),
                TableModified = "tblComment",
                DataObjekId = id,
                ColumnModified = "IsDeleted",
                DataAwal = "false",
                DataAkhir = "true",
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = deleteBy
            };
            int audit = Add(auditParam);
            return id;
        }
        #endregion TranslateParameter CorrelationMatrix

        #region TranslateParameter CorrelationMatrix
        public int AddCorrelationMatrixAudit(CorrelationMatrixParam param, int id)
        {
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate")
                {
                    var dataAkhir = paramBaru.ToString();
                    if (col.Name == "Nilai")
                    {
                        dataAkhir = String.Format("{0:0.000}", paramBaru);
                    }
                    var auditParam = new AuditLogParam
                    {
                        MenuId = Convert.ToInt32(EventLogType.MatriksKorelasi),
                        TableModified = "tblCorrelationMatrix",
                        DataObjekId = id,
                        ColumnModified = col.Name,
                        DataAwal = "",
                        DataAkhir = dataAkhir,
                        LogTimestamp = DateHelper.GetDateTime(),
                        ModifiedBy = param.CreateBy
                    };
                    int audit = Add(auditParam);
                }               
            }
            return id;
        }

        public int UpdateCorrelationMatrixAudit(CorrelationMatrixParam param, int id)
        {
            var data = _databaseContext.CorrelationMatrixs.SingleOrDefault(x => x.Id == id);
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                var paramLama = data.GetType().GetProperty(col.Name).GetValue(data, null);
                var dataAwal = "";
                if (paramBaru != null && col.Name != "UpdateBy" && col.Name != "UpdateDate")
                {
                    var dataAkhir = paramBaru.ToString();
                    if (col.Name == "Nilai")
                    {
                        dataAkhir = String.Format("{0:0.000}", paramBaru);
                    }
                    if (paramLama != null)
                    {
                        dataAwal = paramLama.ToString();
                    }
                    if (dataAkhir != dataAwal)
                    {
                        var auditParam = new AuditLogParam
                        {
                            MenuId = Convert.ToInt32(EventLogType.MatriksKorelasiUpdate),
                            TableModified = "tblCorrelationMatrix",
                            DataObjekId = id,
                            ColumnModified = col.Name,
                            DataAwal = dataAwal,
                            DataAkhir = dataAkhir,
                            LogTimestamp = DateHelper.GetDateTime(),
                            ModifiedBy = param.UpdateBy
                        };
                        int audit = Add(auditParam);
                    }
                }                
            }
            return id;
        }

        public int DeleteCorrelationMatrixAudit(int id, int deleteBy)
        {
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.MatriksKorelasiDelete),
                TableModified = "tblCorrelationMatrix",
                DataObjekId = id,
                ColumnModified = "IsDeleted",
                DataAwal = "false",
                DataAkhir = "true",
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = deleteBy
            };
            int audit = Add(auditParam);
            return id;
        }
        #endregion TranslateParameter CorrelationMatrix

        #region TranslateParameter CorrelatedProjectDetail
        public int CorrelationProjectDetailAudit(CorrelatedProjectDetail param, CorrelationMatrix dataBaru, int modifBy)
        {
            var data = _databaseContext.CorrelatedProjectDetails.SingleOrDefault(x => x.Id == param.Id);
            var dataAkhir = dataBaru.Id.ToString();
            var paramLama = data.GetType().GetProperty("CorrelationMatrixId").GetValue(data, null);
            var dataAwal = paramLama.ToString();
            if (dataAkhir != dataAwal)
            {
                var auditParam = new AuditLogParam
                {
                    MenuId = Convert.ToInt32(EventLogType.KorelasiProject),
                    TableModified = "tblCorrelatedProjectDetails",
                    DataObjekId = param.Id,
                    ColumnModified = "CorrelationMatrixId",
                    DataAwal = dataAwal,
                    DataAkhir = dataAkhir,
                    LogTimestamp = DateHelper.GetDateTime(),
                    ModifiedBy = modifBy
                };
                int audit = Add(auditParam);
            }
            return param.Id;
        }

        public int AddCorrelationProjectDetailAudit(int id, int createBy)
        {
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.KorelasiProjectAdd),
                TableModified = "tblCorrelatedProjectDetails",
                DataObjekId = id,
                ColumnModified = " ",
                DataAwal = " ",
                DataAkhir = " ",
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = createBy
            };

            int audit = Add2(auditParam);
            return id;
        }
        #endregion TranslateParameter CorrelatedProjectDetail

        #region TranslateParameter CorrelatedSektorDetail
        public int CorrelationSektorDetailAudit(CorrelatedSektorDetail param, CorrelationMatrix dataBaru, int modifBy)
        {
            var data = _databaseContext.CorrelatedSektorDetails.SingleOrDefault(x => x.Id == param.Id);           
            var dataAkhir = dataBaru.Id.ToString();
            var paramLama = data.GetType().GetProperty("CorrelationMatrixId").GetValue(data, null);
            var dataAwal = paramLama.ToString();
            if (dataAkhir != dataAwal)
            {
                var auditParam = new AuditLogParam
                {
                    MenuId = Convert.ToInt32(EventLogType.Korelasi),
                    TableModified = "tblCorrelatedSektorDetails",
                    DataObjekId = param.Id,
                    ColumnModified = "CorrelationMatrixId",
                    DataAwal = dataAwal,
                    DataAkhir = dataAkhir,
                    LogTimestamp = DateHelper.GetDateTime(),
                    ModifiedBy = modifBy
                };
                int audit = Add(auditParam);
            }
            return param.Id;
        }

        public int AddCorrelationSektorDetailAudit(int id, int createBy)
        {
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.KorelasiAdd),
                TableModified = "tblCorrelatedSektorDetails",
                DataObjekId = id,
                ColumnModified = " ",
                DataAwal = " ",
                DataAkhir = " ",
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = createBy
            };

            int audit = Add2(auditParam);
            return id;
        }
        #endregion TranslateParameter CorrelatedSektorDetail

        #region TranslateParameter FunctionalRisk
        public int AddFunctionalRiskAudit(FunctionalRiskParam param, int id)
        {
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate")
                {
                    var auditParam = new AuditLogParam
                    {
                        MenuId = Convert.ToInt32(EventLogType.AmbangBatasRisiko),
                        TableModified = "tblFunctionalRisk",
                        DataObjekId = id,
                        ColumnModified = col.Name,
                        DataAwal = "",
                        DataAkhir = paramBaru.ToString(),
                        LogTimestamp = DateHelper.GetDateTime(),
                        ModifiedBy = param.CreateBy
                    };
                    //int audit = Add(auditParam);
                }
            }
            return id;
        }

        public int UpdateFunctionalRiskAudit(FunctionalRiskParam param, int id)
        {
            var dataLama = _databaseContext.FunctionalRisks.Where(x => x.Id == id).SingleOrDefault();
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);              
                if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate" && col.Name != "UpdateBy" && col.Name != "UpdateDate" && col.Name != "DeleteDate" && col.Name != "IsDelete" && col.Name != "CollorCommentId" && col.Name != "ScenarioId" && col.Name != "Scenario")
                {
                    var paramLama = dataLama.GetType().GetProperty(col.Name).GetValue(dataLama, null);
                    if (paramLama == null)
                    {
                        paramLama = " ";
                    }
                    if(col.Name == "NilaiMaksimum" || col.Name == "NilaiMinimum")
                    {
                        paramBaru = String.Format("{0:0.0000000000000}", paramBaru);
                    }
                    if (paramLama.ToString() != paramBaru.ToString())
                    {
                        var auditParam = new AuditLogParam
                        {
                            MenuId = Convert.ToInt32(EventLogType.AmbangBatasRisiko),
                            TableModified = "tblFunctionalRisk",
                            DataObjekId = id,
                            ColumnModified = col.Name,
                            DataAwal = paramLama.ToString(),
                            DataAkhir = paramBaru.ToString(),
                            LogTimestamp = DateHelper.GetDateTime(),
                            ModifiedBy = param.UpdateBy
                        };
                        int audit = Add(auditParam);
                    }                                      
                }
            }
            return id;
        }
        #endregion TranslateParameter FunctionalRisk

        #region TranslateParameter Likehood
        public int AddLikehoodAudit(LikehoodParam param, int id)
        {
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate")
                {
                    var auditParam = new AuditLogParam
                    {
                        MenuId = Convert.ToInt32(EventLogType.Likehood),
                        TableModified = "tblLikehood",
                        DataObjekId = id,
                        ColumnModified = col.Name,
                        DataAwal = "",
                        DataAkhir = paramBaru.ToString(),
                        LogTimestamp = DateHelper.GetDateTime(),
                        ModifiedBy = param.CreateBy
                    };
                    int audit = Add(auditParam);
                }
            }
            return id;
        }

        public int UpdateLikehoodAudit(LikehoodParam param, int id)
        {
            var data = _databaseContext.Likehoods.SingleOrDefault(x => x.Id == id);
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                var paramLama = data.GetType().GetProperty(col.Name).GetValue(data, null);
                var dataAwal = "";
                if (paramBaru != null && col.Name != "UpdateBy" && col.Name != "UpdateDate")
                {
                    var dataAkhir = paramBaru.ToString();
                    if (paramLama != null)
                    {
                        dataAwal = paramLama.ToString();
                    }
                    if (dataAkhir != dataAwal)
                    {
                        var auditParam = new AuditLogParam
                        {
                            MenuId = Convert.ToInt32(EventLogType.LikehoodUpdate),
                            TableModified = "tblLikehood",
                            DataObjekId = id,
                            ColumnModified = col.Name,
                            DataAwal = dataAwal,
                            DataAkhir = dataAkhir,
                            LogTimestamp = DateHelper.GetDateTime(),
                            ModifiedBy = param.UpdateBy
                        };
                        int audit = Add(auditParam);
                    }
                }
            }
            return id;
        }

        public int DeleteLikehoodAudit(int id, int deleteBy)
        {
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.LikehoodDelete),
                TableModified = "tblLikehood",
                DataObjekId = id,
                ColumnModified = "IsDeleted",
                DataAwal = "false",
                DataAkhir = "true",
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = deleteBy
            };
            int audit = Add(auditParam);
            return id;
        }

        public int LikehoodSetDefault(Likehood model)
        {
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.LikehoodDelete),
                TableModified = "tblLikehood",
                DataObjekId = model.Id,
                ColumnModified = "status",
                DataAwal = "false",
                DataAkhir = "true",
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = model.UpdateBy
            };
            int audit = Add(auditParam);
            return model.Id;
        }

        public int LikehoodRemoveDefault(Likehood model, int? updateBy)
        {
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.LikehoodDelete),
                TableModified = "tblLikehood",
                DataObjekId = model.Id,
                ColumnModified = "status",
                DataAwal = "true",
                DataAkhir = "false",
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = model.UpdateBy
            };
            int audit = Add(auditParam);
            return model.Id;
        }
        #endregion TranslateParameter Likehood

        #region TranslateParameter LikehoodDetail
        public int AddLikehoodDetailAudit(LikehoodDetailParam param, int id)
        {
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate")
                {
                    var dataAkhir = paramBaru.ToString();
                    if (col.Name == "Lower" || col.Name == "Upper" || col.Name == "Average")
                    {
                        dataAkhir = String.Format("{0:0.000}", paramBaru);
                    }
                    var auditParam = new AuditLogParam
                    {
                        MenuId = Convert.ToInt32(EventLogType.LikehoodDetail),
                        TableModified = "tblLikehoodDetail",
                        DataObjekId = id,
                        ColumnModified = col.Name,
                        DataAwal = "",
                        DataAkhir = dataAkhir,
                        LogTimestamp = DateHelper.GetDateTime(),
                        ModifiedBy = param.CreateBy
                    };
                    int audit = Add(auditParam);
                }                
            }
            return id;
        }

        public int UpdateLikehoodDetailAudit(LikehoodDetailParam param, int id)
        {
            var data = _databaseContext.LikehoodDetails.SingleOrDefault(x => x.Id == id);
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                var paramLama = data.GetType().GetProperty(col.Name).GetValue(data, null);
                var dataAwal = "";
                if (paramBaru != null && col.Name != "UpdateBy" && col.Name != "UpdateDate")
                {
                    var dataAkhir = paramBaru.ToString();
                    if (col.Name == "Lower" || col.Name == "Upper" || col.Name == "Average")
                    {
                        dataAkhir = String.Format("{0:0.000}", paramBaru);
                    }
                    if (paramLama != null)
                    {
                        dataAwal = paramLama.ToString();
                    }
                    if (dataAkhir != dataAwal)
                    {
                        var auditParam = new AuditLogParam
                        {
                            MenuId = Convert.ToInt32(EventLogType.LikehoodDetail),
                            TableModified = "tblLikehoodDetail",
                            DataObjekId = id,
                            ColumnModified = col.Name,
                            DataAwal = dataAwal,
                            DataAkhir = dataAkhir,
                            LogTimestamp = DateHelper.GetDateTime(),
                            ModifiedBy = param.UpdateBy
                        };
                        int audit = Add(auditParam);
                    }
                }                
            }
            return id;
        }
        #endregion TranslateParameter Likehood

        #region TranslateParameter OverAllComments
        public int AddOverAllCommentsAudit(OverAllCommentsParam param, int id)
        {
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate" && col.Name != "ColorCommentId")
                {
                    var auditParam = new AuditLogParam
                    {
                        MenuId = Convert.ToInt32(EventLogType.OverallComment),
                        TableModified = "tblOverAllComments",
                        DataObjekId = id,
                        ColumnModified = col.Name,
                        DataAwal = "",
                        DataAkhir = paramBaru.ToString(),
                        LogTimestamp = DateHelper.GetDateTime(),
                        ModifiedBy = param.CreateBy
                    };
                    int audit = Add(auditParam);
                }                
            }
            return id;
        }

        public int UpdateOverAllCommentsAudit(OverAllCommentsParam param, int id)
        {
            var data = _databaseContext.OverAllCommentss.SingleOrDefault(x => x.Id == id);
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);                
                var dataAwal = "";
                if (paramBaru != null && col.Name != "UpdateBy" && col.Name != "UpdateDate" && col.Name != "ColorCommentId" && col.Name != "IsDefault")
                {
                    var paramLama = data.GetType().GetProperty(col.Name).GetValue(data, null);
                    var dataAkhir = paramBaru.ToString();
                    if (paramLama != null)
                    {
                        dataAwal = paramLama.ToString();
                    }
                    if (dataAkhir != dataAwal)
                    {
                        var auditParam = new AuditLogParam
                        {
                            MenuId = Convert.ToInt32(EventLogType.OverallCommentUpdate),
                            TableModified = "tblOverAllComments",
                            DataObjekId = id,
                            ColumnModified = col.Name,
                            DataAwal = dataAwal,
                            DataAkhir = dataAkhir,
                            LogTimestamp = DateHelper.GetDateTime(),
                            ModifiedBy = param.UpdateBy
                        };
                        int audit = Add(auditParam);
                    }
                }                
            }
            return id;
        }
        #endregion TranslateParameter OverAllComments

        #region TranslateParameter PMN
        public int AddPMNAudit(PMNParam param, int id)
        {
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate")
                {
                    var dataAkhir = paramBaru.ToString();
                    if (col.Name == "RecourseDelay" || col.Name == "DelayYears" || col.Name == "OpexGrowth" || col.Name == "Opex" || col.Name == "ValuePMNToModalDasarCap")
                    {
                        dataAkhir = String.Format("{0:0.0000000000000}", paramBaru);
                    }
                    var auditParam = new AuditLogParam
                    {
                        MenuId = Convert.ToInt32(EventLogType.PMN),
                        TableModified = "tblPMN",
                        DataObjekId = id,
                        ColumnModified = col.Name,
                        DataAwal = "",
                        DataAkhir = dataAkhir,
                        LogTimestamp = DateHelper.GetDateTime(),
                        ModifiedBy = param.CreateBy
                    };
                    int audit = Add(auditParam);
                }
            }
            return id;
        }

        public int UpdatePMNAudit(PMNParam param, int id)
        {
            var data = _databaseContext.PMNs.SingleOrDefault(x => x.Id == id);
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                var paramLama = data.GetType().GetProperty(col.Name).GetValue(data, null);
                var dataAwal = "";
                if (paramBaru != null && col.Name != "UpdateBy" && col.Name != "UpdateDate")
                {
                    var dataAkhir = paramBaru.ToString();
                    if (col.Name == "RecourseDelay" || col.Name == "DelayYears" || col.Name == "OpexGrowth" || col.Name == "Opex" || col.Name == "ValuePMNToModalDasarCap")
                    {
                        dataAkhir = String.Format("{0:0.0000000000000}", paramBaru);
                    }
                    if (paramLama != null)
                    {
                        dataAwal = paramLama.ToString();
                    }
                    if (dataAkhir != dataAwal)
                    {
                        var auditParam = new AuditLogParam
                        {
                            MenuId = Convert.ToInt32(EventLogType.PMNUpdate),
                            TableModified = "tblPMN",
                            DataObjekId = id,
                            ColumnModified = col.Name,
                            DataAwal = dataAwal,
                            DataAkhir = dataAkhir,
                            LogTimestamp = DateHelper.GetDateTime(),
                            ModifiedBy = param.UpdateBy
                        };
                        int audit = Add(auditParam);
                    }
                }
            }
            return id;
        }

        public int DeletePMNAudit(int id, int deleteBy)
        {
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.PMNDelete),
                TableModified = "tblPMN",
                DataObjekId = id,
                ColumnModified = "IsDeleted",
                DataAwal = "false",
                DataAkhir = "true",
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = deleteBy
            };
            int audit = Add(auditParam);
            return id;
        }
        #endregion TranslateParameter PMN

        #region TranslateParameter Project
        public int AddProjectAudit(ProjectParam param, int id)
        {
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate" && col.Name != "RiskRegistrasiId")
                {
                    var dataAkhir = paramBaru.ToString();
                    if (col.Name == "Minimum" || col.Name == "Maximum")
                    {
                        dataAkhir = String.Format("{0:0.00}", paramBaru);
                    }
                    var auditParam = new AuditLogParam
                    {
                        MenuId = Convert.ToInt32(EventLogType.Proyek),
                        TableModified = "tblProject",
                        DataObjekId = id,
                        ColumnModified = col.Name,
                        DataAwal = "",
                        DataAkhir = dataAkhir,
                        LogTimestamp = DateHelper.GetDateTime(),
                        ModifiedBy = param.CreateBy
                    };
                    int audit = Add(auditParam);
                }
            }
            return id;
        }

        public int UpdateProjectAudit(ProjectParam param, int id)
        {
            var data = _databaseContext.Projects.SingleOrDefault(x => x.Id == id);
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);                
                var dataAwal = "";
                if (paramBaru != null && col.Name != "UpdateBy" && col.Name != "UpdateDate" && col.Name != "RiskRegistrasiId" && col.Name != "UserId")
                {
                    var paramLama = data.GetType().GetProperty(col.Name).GetValue(data, null);
                    var dataAkhir = paramBaru.ToString();
                    if (col.Name == "Minimum" || col.Name == "Maximum")
                    {
                        dataAkhir = String.Format("{0:0.00}", paramBaru);
                    }
                    if (paramLama != null)
                    {
                        dataAwal = paramLama.ToString();
                    }
                    if (dataAkhir != dataAwal)
                    {
                        var auditParam = new AuditLogParam
                        {
                            MenuId = Convert.ToInt32(EventLogType.ProyekUpdate),
                            TableModified = "tblProject",
                            DataObjekId = id,
                            ColumnModified = col.Name,
                            DataAwal = dataAwal,
                            DataAkhir = dataAkhir,
                            LogTimestamp = DateHelper.GetDateTime(),
                            ModifiedBy = param.UpdateBy
                        };
                        int audit = Add(auditParam);
                    }
                }
            }
            return id;
        }

        public int DeleteProjectAudit(int id, int deleteBy)
        {
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.ProyekDelete),
                TableModified = "tblProject",
                DataObjekId = id,
                ColumnModified = "IsDeleted",
                DataAwal = "false",
                DataAkhir = "true",
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = deleteBy
            };
            int audit = Add(auditParam);
            return id;
        }
        #endregion TranslateParameter Project

        #region TranslateParameter ProjectRiskRegistrasi
        public int AddProjectRiskRegistrasiAudit(IList<ProjectRiskRegistrasi> collection)
        {
            foreach (var param in collection)
            {
                var id = Convert.ToInt32(param.GetType().GetProperty("Id").GetValue(param, null));
                foreach (var col in param.GetType().GetProperties())
                {
                    var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                    if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate" && col.Name != "IsDelete" && col.Name != "Project" && col.Name != "RiskRegistrasi" && col.Name != "Id")
                    {
                        var auditParam = new AuditLogParam
                        {
                            MenuId = Convert.ToInt32(EventLogType.RiskRegistrasi),
                            TableModified = "tblProjectRiskRegistrasi",
                            DataObjekId = id,
                            ColumnModified = col.Name,
                            DataAwal = "",
                            DataAkhir = paramBaru.ToString(),
                            LogTimestamp = DateHelper.GetDateTime(),
                            ModifiedBy = param.UpdateBy
                        };
                        int audit = Add(auditParam);
                    }
                }                
            }
            return 5;
        }
        #endregion TranslateParameter ProjectRiskRegistrasi

        #region TranslateParameter ProjectRiskStatus
        public int AddProjectRiskStatusAudit(ProjectRiskStatus param, Project project)
        {
            if(param.IsProjectUsed == true)
            {
                foreach (var col in param.GetType().GetProperties())
                {
                    var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                    if (paramBaru != null && col.Name != "RiskRegistrasiId" && col.Name != "NamaCategoryRisk" && col.Name != "Definisi" && col.Name != "Id" && col.Name != "KodeMRisk" && col.Name != "ProjectId")
                    {
                        var auditParam = new AuditLogParam
                        {
                            MenuId = Convert.ToInt32(EventLogType.ProyekRiskStatus),
                            TableModified = "tblProjectRiskStatus",
                            DataObjekId = param.Id,
                            ColumnModified = col.Name,
                            DataAwal = "",
                            DataAkhir = paramBaru.ToString(),
                            LogTimestamp = DateHelper.GetDateTime(),
                            ModifiedBy = project.CreateBy
                        };
                        int audit = Add(auditParam);
                    }
                }
            }            
            return 5;
        }

        public int UpdateProjectRiskStatusAudit(ProjectRiskStatus param, Project project, ProjectRiskStatus param2)
        {
            foreach (var col in param.GetType().GetProperties())
            {
                var paramLama = param2.GetType().GetProperty(col.Name).GetValue(param2, null);
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                if (paramBaru != null && col.Name != "RiskRegistrasiId" && col.Name != "NamaCategoryRisk" && col.Name != "Definisi" && col.Name != "Id")
                {
                    var dataAwal = paramLama.ToString();
                    var dataAkhir = paramBaru.ToString();
                    if(dataAwal != dataAkhir)
                    {
                        var auditParam = new AuditLogParam
                        {
                            MenuId = Convert.ToInt32(EventLogType.ProyekRiskStatusUpdate),
                            TableModified = "tblProjectRiskStatus",
                            DataObjekId = param2.Id,
                            ColumnModified = col.Name,
                            DataAwal = dataAwal,
                            DataAkhir = dataAkhir,
                            LogTimestamp = DateHelper.GetDateTime(),
                            ModifiedBy = project.UpdateBy
                        };
                        int audit = Add(auditParam);
                    }                  
                }
            }
            return 5;
        }

        public int AddProjectRiskStatusAuditByUpdate(ProjectRiskStatus param, int? updateBy)
        {

            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                if (paramBaru != null && col.Name != "RiskRegistrasiId" && col.Name != "NamaCategoryRisk" && col.Name != "Definisi" && col.Name != "Id")
                {
                    var auditParam = new AuditLogParam
                    {
                        MenuId = Convert.ToInt32(EventLogType.ProyekRiskStatusUpdate),
                        TableModified = "tblProjectRiskStatus",
                        DataObjekId = param.Id,
                        ColumnModified = col.Name,
                        DataAwal = "",
                        DataAkhir = paramBaru.ToString(),
                        LogTimestamp = DateHelper.GetDateTime(),
                        ModifiedBy = updateBy
                    };
                    int audit = Add(auditParam);
                }
            }
            return 5;
        }
        #endregion TranslateParameter ProjectRiskStatus

        #region TranslateParameter RiskMatrix
        public int AddRiskMatrixAudit(Scenario param, int id)
        {
            //var paramBaru = param.Id.ToString();
            //var auditParam = new AuditLogParam
            //{
            //    MenuId = Convert.ToInt32(EventLogType.RiskMatriks),
            //    TableModified = "tblRiskMatrix",
            //    DataObjekId = id,
            //    ColumnModified = "ScenarioId",
            //    DataAwal = "",
            //    DataAkhir = paramBaru.ToString(),
            //    LogTimestamp = DateHelper.GetDateTime(),
            //    ModifiedBy = param.CreateBy
            //};
            //int audit = Add(auditParam);
            return id;
        }

        public int DeleteRiskMatrixProjectAudit(int id, int deleteBy)
        {
            //var auditParam = new AuditLogParam
            //{
            //    MenuId = Convert.ToInt32(EventLogType.ScenarioDelete),
            //    TableModified = "tblRiskMatrixProject",
            //    DataObjekId = id,
            //    ColumnModified = "IsDeleted",
            //    DataAwal = "false",
            //    DataAkhir = "true",
            //    LogTimestamp = DateHelper.GetDateTime(),
            //    ModifiedBy = deleteBy
            //};
            //int audit = Add(auditParam);
            return id;
        }
        #endregion TranslateParameter RiskMatrix   

        #region TranslateParameter RiskRegistrasi
        public int AddRiskRegistrasiAudit(RiskRegistrasiParam param, int id)
        {
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate")
                {
                    var dataAkhir = paramBaru.ToString();
                    if (col.Name == "Minimum" || col.Name == "Maximum")
                    {
                        dataAkhir = String.Format("{0:0.00}", paramBaru);
                    }
                    var auditParam = new AuditLogParam
                    {
                        MenuId = Convert.ToInt32(EventLogType.RiskRegistrasi),
                        TableModified = "tblRiskRegistrasi",
                        DataObjekId = id,
                        ColumnModified = col.Name,
                        DataAwal = "",
                        DataAkhir = dataAkhir,
                        LogTimestamp = DateHelper.GetDateTime(),
                        ModifiedBy = param.CreateBy
                    };
                    int audit = Add(auditParam);
                }
            }
            return id;
        }

        public int UpdateRiskRegistrasiAudit(RiskRegistrasiParam param, int id)
        {
            var data = _databaseContext.RiskRegistrasis.SingleOrDefault(x => x.Id == id);
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                var paramLama = data.GetType().GetProperty(col.Name).GetValue(data, null);
                var dataAwal = "";
                if (paramBaru != null && col.Name != "UpdateBy" && col.Name != "UpdateDate")
                {
                    var dataAkhir = paramBaru.ToString();
                    if (col.Name == "Minimum" || col.Name == "Maximum")
                    {
                        dataAkhir = String.Format("{0:0.00}", paramBaru);
                    }
                    if (paramLama != null)
                    {
                        dataAwal = paramLama.ToString();
                    }
                    if (dataAkhir != dataAwal)
                    {
                        var auditParam = new AuditLogParam
                        {
                            MenuId = Convert.ToInt32(EventLogType.RiskRegistrasiUpdate),
                            TableModified = "tblRiskRegistrasi",
                            DataObjekId = id,
                            ColumnModified = col.Name,
                            DataAwal = dataAwal,
                            DataAkhir = dataAkhir,
                            LogTimestamp = DateHelper.GetDateTime(),
                            ModifiedBy = param.UpdateBy
                        };
                        int audit = Add(auditParam);
                    }
                }
            }
            return id;
        }

        public int DeleteRiskRegistrasiAudit(int id, int deleteBy)
        {
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.RiskRegistrasiDelete),
                TableModified = "tblRiskRegistrasi",
                DataObjekId = id,
                ColumnModified = "IsDeleted",
                DataAwal = "false",
                DataAkhir = "true",
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = deleteBy
            };
            int audit = Add(auditParam);
            return id;
        }
        #endregion TranslateParameter RiskRegistrasi       

        #region TranslateParameter Scenario
        public int AddScenarioAudit(ScenarioParam param, int id)
        {
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate" && col.Name != "ProjectId" && col.Name != "TemplateNotif" && col.Name != "IsSend")
                {
                    var auditParam = new AuditLogParam
                    {
                        MenuId = Convert.ToInt32(EventLogType.Scenario),
                        TableModified = "tblScenario",
                        DataObjekId = id,
                        ColumnModified = col.Name,
                        DataAwal = "",
                        DataAkhir = paramBaru.ToString(),
                        LogTimestamp = DateHelper.GetDateTime(),
                        ModifiedBy = param.CreateBy
                    };
                    int audit = Add(auditParam);
                }
            }
            return id;
        }

        public int UpdateScenarioAudit(ScenarioParam param, int id)
        {

            param.IsStatusApproval = null;
            param.IsSend = null;
            var data = _databaseContext.Scenarios.SingleOrDefault(x => x.Id == id);
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);                
                var dataAwal = "";
                if (paramBaru != null && col.Name != "IsScenarioDefault" && col.Name != "UpdateBy" && col.Name != "UpdateDate" && col.Name != "ProjectId" && col.Name != "IsUpdate" && col.Name != "TemplateNotif")
                {
                    var paramLama = data.GetType().GetProperty(col.Name).GetValue(data, null);
                    var dataAkhir = paramBaru.ToString();
                    if (paramLama != null)
                    {
                        dataAwal = paramLama.ToString();
                    }
                    if (dataAkhir != dataAwal)
                    {
                        var auditParam = new AuditLogParam
                        {
                            MenuId = Convert.ToInt32(EventLogType.ScenarioUpdate),
                            TableModified = "tblScenario",
                            DataObjekId = id,
                            ColumnModified = col.Name,
                            DataAwal = dataAwal,
                            DataAkhir = dataAkhir,
                            LogTimestamp = DateHelper.GetDateTime(),
                            ModifiedBy = param.UpdateBy
                        };
                        int audit = Add(auditParam);
                    }
                }                
            }
            return id;
        }

        public int DeleteScenarioAudit(int id, int deleteBy)
        {
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.ScenarioDelete),
                TableModified = "tblScenario",
                DataObjekId = id,
                ColumnModified = "IsDeleted",
                DataAwal = "false",
                DataAkhir = "true",
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = deleteBy
            };
            int audit = Add(auditParam);
            return id;
        }

        public int ScenarioSetDefault(Scenario model)
        {
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.ScenarioUpdate),
                TableModified = "tblScenario",
                DataObjekId = model.Id,
                ColumnModified = "isDefault",
                DataAwal = "false",
                DataAkhir = "true",
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = model.UpdateBy
            };
            int audit = Add(auditParam);
            return model.Id;
        }

        public int ScenarioRemoveDefault(Scenario model, int? updateBy)
        {
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.ScenarioUpdate),
                TableModified = "tblScenario",
                DataObjekId = model.Id,
                ColumnModified = "isDefault",
                DataAwal = "true",
                DataAkhir = "false",
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = model.UpdateBy
            };
            int audit = Add(auditParam);
            return model.Id;
        }

        public int ScenarioApproval(int id, int? updateBy, string dataAwal, string dataAkhir)
        {
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.ScenarioApproval),
                TableModified = "tblScenario",
                DataObjekId = id,
                ColumnModified = "isDefault",
                DataAwal = dataAwal,
                DataAkhir = dataAkhir,
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = updateBy
            };
            //int audit = Add(auditParam);
            return id;
        }
        #endregion TranslateParameter Scenario        

        #region TranslateParameter ScenarioDetail
        public int AddScenarioDetailAudit(IList<ScenarioDetail> collection)
        {
            foreach (var param in collection)
            {
                if(param.IsDelete == false)
                {
                    var id = Convert.ToInt32(param.GetType().GetProperty("Id").GetValue(param, null));
                    foreach (var col in param.GetType().GetProperties())
                    {
                        var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                        if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate" && col.Name != "Project" && col.Name != "Id" && col.Name != "ProjectId" && col.Name != "ScenarioId")
                        {
                            var auditParam = new AuditLogParam
                            {
                                MenuId = Convert.ToInt32(EventLogType.ScenarioDetailAdd),
                                TableModified = "tblScenarioDetail",
                                DataObjekId = id,
                                ColumnModified = col.Name,
                                DataAwal = "",
                                DataAkhir = paramBaru.ToString(),
                                LogTimestamp = DateHelper.GetDateTime(),
                                ModifiedBy = param.CreateBy
                            };
                            int audit = Add(auditParam);
                        }
                    }
                }                
            }
            return 5;
        }

        public int UpdateScenarioDetailAudit(ScenarioDetail param, Scenario scenario, ScenarioDetail param2)
        {            
            var paramLama = param2.GetType().GetProperty("IsDelete").GetValue(param2, null);
            var paramBaru = param.GetType().GetProperty("IsDelete").GetValue(param, null);
            var dataAwal = paramLama.ToString();
            var dataAkhir = paramBaru.ToString();
            if (dataAwal != dataAkhir)
            {
                int valid = 0;
                if (param.IsDelete == false)
                {
                    valid = Convert.ToInt32(EventLogType.ScenarioDetailAdd);
                }
                else
                {
                    valid = Convert.ToInt32(EventLogType.ScenarioDetailDelete);
                }
                var auditParam = new AuditLogParam
                {
                    MenuId = valid,
                    TableModified = "tblScenarioDetail",
                    DataObjekId = param2.Id,
                    ColumnModified = "IsDelete",
                    DataAwal = dataAwal,
                    DataAkhir = dataAkhir,
                    LogTimestamp = DateHelper.GetDateTime(),
                    ModifiedBy = scenario.UpdateBy
                };
                int audit = Add(auditParam);
            }
            return 5;
        }

        public int AddScenarioDetailAuditByUpdate(ScenarioDetail param, int? updateBy)
        {
            if(param.IsDelete == false)
            {
                var paramBaru = param.GetType().GetProperty("IsDelete").GetValue(param, null);
                var auditParam = new AuditLogParam
                {
                    MenuId = Convert.ToInt32(EventLogType.ScenarioDetailAdd),
                    TableModified = "tblScenarioDetail",
                    DataObjekId = param.Id,
                    ColumnModified = "IsDelete",
                    DataAwal = "",
                    DataAkhir = paramBaru.ToString(),
                    LogTimestamp = DateHelper.GetDateTime(),
                    ModifiedBy = updateBy
                };
                int audit = Add(auditParam);                
            }
            return 5;
        }
        #endregion TranslateParameter ProjectRiskRegistrasi

        #region TranslateParameter Sektor
        public int AddSektorAudit(SektorParam param, int id)
        {
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate")
                {
                    var dataAkhir = paramBaru.ToString();
                    if (col.Name == "Minimum" || col.Name == "Maximum")
                    {
                        dataAkhir = String.Format("{0:0.00}", paramBaru);
                    }
                    var auditParam = new AuditLogParam
                    {
                        MenuId = Convert.ToInt32(EventLogType.SektorProyek),
                        TableModified = "tblSektor",
                        DataObjekId = id,
                        ColumnModified = col.Name,
                        DataAwal = "",
                        DataAkhir = dataAkhir,
                        LogTimestamp = DateHelper.GetDateTime(),
                        ModifiedBy = param.CreateBy
                    };
                    int audit = Add(auditParam);
                }
            }
            return id;
        }

        public int UpdateSektorAudit(SektorParam param, int id)
        {
            var data = _databaseContext.Sektors.SingleOrDefault(x => x.Id == id);
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                var paramLama = data.GetType().GetProperty(col.Name).GetValue(data, null);
                var dataAwal = "";
                if (paramBaru != null && col.Name != "UpdateBy" && col.Name != "UpdateDate")
                {
                    var dataAkhir = paramBaru.ToString();
                    if (col.Name == "Minimum" || col.Name == "Maximum")
                    {
                        dataAkhir = String.Format("{0:0.00}", paramBaru);
                    }
                    if (paramLama != null)
                    {
                        dataAwal = paramLama.ToString();
                    }
                    if (dataAkhir != dataAwal)
                    {
                        var auditParam = new AuditLogParam
                        {
                            MenuId = Convert.ToInt32(EventLogType.SektorProyekUpdate),
                            TableModified = "tblSektor",
                            DataObjekId = id,
                            ColumnModified = col.Name,
                            DataAwal = dataAwal,
                            DataAkhir = dataAkhir,
                            LogTimestamp = DateHelper.GetDateTime(),
                            ModifiedBy = param.UpdateBy
                        };
                        int audit = Add(auditParam);
                    }
                }
            }
            return id;
        }

        public int DeleteSektorAudit(int id, int deleteBy)
        {
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.SektorProyekDelete),
                TableModified = "tblSektor",
                DataObjekId = id,
                ColumnModified = "IsDeleted",
                DataAwal = "false",
                DataAkhir = "true",
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = deleteBy
            };
            int audit = Add(auditParam);
            return id;
        }
        #endregion TranslateParameter Stage        

        #region TranslateParameter Stage
        public int AddStageAudit(StageParam param, int id)
        {
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate")
                {
                    var auditParam = new AuditLogParam
                    {
                        MenuId = Convert.ToInt32(EventLogType.TahapanProyek),
                        TableModified = "tblStage",
                        DataObjekId = id,
                        ColumnModified = col.Name,
                        DataAwal = "",
                        DataAkhir = paramBaru.ToString(),
                        LogTimestamp = DateHelper.GetDateTime(),
                        ModifiedBy = param.CreateBy
                    };
                    int audit = Add(auditParam);
                }                
            }
            return id;
        }

        public int UpdateStageAudit(StageParam param, int id)
        {
            var data = _databaseContext.Stages.SingleOrDefault(x => x.Id == id);
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                var paramLama = data.GetType().GetProperty(col.Name).GetValue(data, null);
                var dataAwal = "";
                if (paramBaru != null && col.Name != "UpdateBy" && col.Name != "UpdateDate")
                {
                    var dataAkhir = paramBaru.ToString();
                    if (paramLama != null)
                    {
                        dataAwal = paramLama.ToString();
                    }
                    if (dataAkhir != dataAwal)
                    {
                        var auditParam = new AuditLogParam
                        {
                            MenuId = Convert.ToInt32(EventLogType.TahapanProyekUpdate),
                            TableModified = "tblStage",
                            DataObjekId = id,
                            ColumnModified = col.Name,
                            DataAwal = dataAwal,
                            DataAkhir = dataAkhir,
                            LogTimestamp = DateHelper.GetDateTime(),
                            ModifiedBy = param.UpdateBy
                        };
                        int audit = Add(auditParam);
                    }
                }
            }
            return id;
        }

        public int DeleteStageAudit(int id, int deleteBy)
        {
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.TahapanProyekDelete),
                TableModified = "tblStage",
                DataObjekId = id,
                ColumnModified = "IsDeleted",
                DataAwal = "false",
                DataAkhir = "true",
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = deleteBy
            };
            int audit = Add(auditParam);
            return id;
        }
        #endregion TranslateParameter Stage        

        #region TranslateParameter StageTahunRiskMatrix
        public int AddStageTahunRiskMatrixAudit(StageTahunRiskMatrix param)
        {
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate" && col.Name != "IsUpdate" && col.Name != "IsDelete" && col.Name != "RiskMatrixProject" && col.Name != "Stage" && col.Name != "RiskMatrixProjectId" && col.Name != "StageId" && col.Name != "Id")
                {
                    var auditParam = new AuditLogParam
                    {
                        MenuId = Convert.ToInt32(EventLogType.RiskMatriks),
                        TableModified = "tblStageTahunRiskMatrix",
                        DataObjekId = param.Id,
                        ColumnModified = col.Name,
                        DataAwal = "",
                        DataAkhir = paramBaru.ToString(),
                        LogTimestamp = DateHelper.GetDateTime(),
                        ModifiedBy = param.CreateBy
                    };
                    int audit = Add(auditParam);
                }
            }
            return param.Id;
        }

        public int UpdateStageTahunRiskMatrixAudit(string param, int id, int userr)
        {
            var data = _databaseContext.StageTahunRiskMatrixs.SingleOrDefault(x => x.Id == id);           
            var dataAwal = data.GetType().GetProperty("StageId").GetValue(data, null).ToString();
            
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.RiskMatriksUpdate),
                TableModified = "tblStageTahunRiskMatrix",
                DataObjekId = id,
                ColumnModified = "StageId",
                DataAwal = dataAwal,
                DataAkhir = param,
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = userr
            };
            int audit = Add(auditParam);
            return id;
        }

        public int MaxStageTahunRiskMatrixAudit(int id, string valueLama, string valueBaru, int user)
        {
            if(valueLama != valueBaru)
            {
                if (valueLama == null)
                {
                    valueLama = "";
                }
                var auditParam = new AuditLogParam
                {
                    MenuId = Convert.ToInt32(EventLogType.RiskMatriksUpdate),
                    TableModified = "tblStageTahunRiskMatrix",
                    DataObjekId = id,
                    ColumnModified = "maximumNilaiExpose",
                    DataAwal = valueLama,
                    DataAkhir = valueBaru,
                    LogTimestamp = DateHelper.GetDateTime(),
                    ModifiedBy = user
                };
                int audit = Add2(auditParam);
                return audit;
            }
            return 5;
        }
        #endregion TranslateParameter Stage        

        #region TranslateParameter StageTahunRiskMatrixDetail
        public int UpdateStageTahunRiskMatrixDetailAudit(StageTahunRiskMatrixDetail param, StageTahunRiskMatrixDetail param2)
        {            
            foreach (var col in param.GetType().GetProperties())
            {
                var paramLama = param.GetType().GetProperty(col.Name).GetValue(param, null);
                var paramBaru = param2.GetType().GetProperty(col.Name).GetValue(param2, null);              
                if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate" && col.Name != "IsUpdate" && col.Name != "IsDelete" && col.Name != "UpdateBy" && col.Name != "UpdateDate"
                    && col.Name != "StageTahunRiskMatrixId" && col.Name != "RiskRegistrasiId" && col.Name != "RiskMatrixProjectId" && col.Name != "StageTahunRiskMatrix" && col.Name != "Id"
                    && col.Name != "RiskRegistrasi" && col.Name != "LikehoodDetail")
                {
                    int objekId = Convert.ToInt32(param.GetType().GetProperty("Id").GetValue(param, null));
                    var dataAwal = paramLama.ToString();
                    var dataAkhir = paramBaru.ToString();
                    if (col.Name == "NilaiExpose")
                    {
                        var dataTemp = String.Format("{0:0.0000000000000}", paramBaru);
                        dataAkhir = dataTemp.ToString(); ;
                    }
                    if(dataAkhir != dataAwal)
                    {
                        var auditParam = new AuditLogParam
                        {
                            MenuId = Convert.ToInt32(EventLogType.RiskMatriksDetailUpdate),
                            TableModified = "tblStageTahunRiskMatrixDetail",
                            DataObjekId = objekId,
                            ColumnModified = col.Name,
                            DataAwal = dataAwal,
                            DataAkhir = dataAkhir,
                            LogTimestamp = DateHelper.GetDateTime(),
                            ModifiedBy = param2.CreateBy
                        };
                        int audit = Add(auditParam);
                    }                  
                }
            }
            return param.Id;
        }

        public int AddStageTahunRiskMatrixDetailAudit(int id, int createBy)
        {
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.RiskMatriksDetailAdd),
                TableModified = "tblStageTahunRiskMatrixDetail",
                DataObjekId = id,
                ColumnModified = " ",
                DataAwal = " ",
                DataAkhir = " ",
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = createBy
            };

            int audit = Add2(auditParam);
            return id;
        }
        #endregion TranslateParameter StageTahunRiskMatrixDetail        

        #region TranslateParameter SubRiskRegistrasi
        public int AddSubRiskRegistrasiAudit(SubRiskRegistrasiParam param, int id)
        {
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate" && col.Name != "RiskRegistrasiId")
                {
                    var auditParam = new AuditLogParam
                    {
                        MenuId = Convert.ToInt32(EventLogType.RiskRegistrasiDetailAdd),
                        TableModified = "tblSubRiskRegistrasi",
                        DataObjekId = id,
                        ColumnModified = col.Name,
                        DataAwal = "",
                        DataAkhir = paramBaru.ToString(),
                        LogTimestamp = DateHelper.GetDateTime(),
                        ModifiedBy = param.CreateBy
                    };
                    int audit = Add(auditParam);
                }
            }
            return id;
        }

        public int UpdateSubRiskRegistrasiAudit(SubRiskRegistrasiParam param, int id)
        {
            var data = _databaseContext.SubRiskRegistrasis.SingleOrDefault(x => x.Id == id);
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                var paramLama = data.GetType().GetProperty(col.Name).GetValue(data, null);
                var dataAwal = "";
                if (paramBaru != null && col.Name != "UpdateBy" && col.Name != "UpdateDate")
                {
                    var dataAkhir = paramBaru.ToString();
                    if (paramLama != null)
                    {
                        dataAwal = paramLama.ToString();
                    }
                    if (dataAkhir != dataAwal)
                    {
                        var auditParam = new AuditLogParam
                        {
                            MenuId = Convert.ToInt32(EventLogType.RiskRegistrasiDetailUpdate),
                            TableModified = "tblSubRiskRegistrasi",
                            DataObjekId = id,
                            ColumnModified = col.Name,
                            DataAwal = dataAwal,
                            DataAkhir = dataAkhir,
                            LogTimestamp = DateHelper.GetDateTime(),
                            ModifiedBy = param.UpdateBy
                        };
                        int audit = Add(auditParam);
                    }
                }
            }
            return id;
        }

        public int UpdateSubRiskRegistrasiAudit2(int id, int deleteBy)
        {
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.RiskRegistrasiDetailDelete),
                TableModified = "tblSubRiskRegistrasi",
                DataObjekId = id,
                ColumnModified = "IsDeleted",
                DataAwal = "false",
                DataAkhir = "true",
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = deleteBy
            };
            int audit = Add(auditParam);
            return id;
        }
        #endregion TranslateParameter RiskRegistrasi       

        #region TranslateParameter Tahapan
        public int AddTahapanAudit(TahapanParam param, int id)
        {
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate")
                {
                    var auditParam = new AuditLogParam
                    {
                        MenuId = Convert.ToInt32(EventLogType.TahapanPenjaminan),
                        TableModified = "tblTahapan",
                        DataObjekId = id,
                        ColumnModified = col.Name,
                        DataAwal = "",
                        DataAkhir = paramBaru.ToString(),
                        LogTimestamp = DateHelper.GetDateTime(),
                        ModifiedBy = param.CreateBy
                    };
                    int audit = Add(auditParam);
                }                
            }
            return id;
        }

        public int UpdateTahapanAudit(TahapanParam param, int id)
        {
            var data = _databaseContext.Tahapans.SingleOrDefault(x => x.Id == id);
            foreach (var col in param.GetType().GetProperties())
            {
                var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
                var paramLama = data.GetType().GetProperty(col.Name).GetValue(data, null);
                var dataAwal = "";
                if (paramBaru != null && col.Name != "UpdateBy" && col.Name != "UpdateDate")
                {
                    var dataAkhir = paramBaru.ToString();
                    if (paramLama != null)
                    {
                        dataAwal = paramLama.ToString();
                    }
                    if (dataAkhir != dataAwal)
                    {
                        var auditParam = new AuditLogParam
                        {
                            MenuId = Convert.ToInt32(EventLogType.TahapanPenjaminanUpdate),
                            TableModified = "tblTahapan",
                            DataObjekId = id,
                            ColumnModified = col.Name,
                            DataAwal = dataAwal,
                            DataAkhir = dataAkhir,
                            LogTimestamp = DateHelper.GetDateTime(),
                            ModifiedBy = param.UpdateBy
                        };
                        int audit = Add(auditParam);
                    }
                }               
            }
            return id;
        }

        public int DeleteTahapanDataAudit(int id, int deleteBy)
        {
            var auditParam = new AuditLogParam
            {
                MenuId = Convert.ToInt32(EventLogType.TahapanPenjaminanDelete),
                TableModified = "tblTahapan",
                DataObjekId = id,
                ColumnModified = "IsDeleted",
                DataAwal = "false",
                DataAkhir = "true",
                LogTimestamp = DateHelper.GetDateTime(),
                ModifiedBy = deleteBy
            };
            int audit = Add(auditParam);
            return id;
        }
        #endregion TranslateParameter Tahapan

    }
}
