﻿using HR.Core;
using HR.Domain;
using System;
using System.Collections.Generic;
using HR.Application.DTO;
using HR.Application.Templates.Email;
using HR.Infrastructure.ExternalServices.Email;
using System.Net.Mail;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.IO;
using HR.Common.Extensions;
using HR.Application;

namespace HR.Application
{
    public class EmailService : IEmailService
    {
        private string senderEmail;
        private string senderName;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserRepository _userRepository;
        private readonly IScenarioRepository _scenarioRepository;
        private readonly IRiskMatrixProjectRepository _riskMatrixProjectRepository;
        private readonly ICorrelatedProjectRepository _correlatedProjectRepository;
        private readonly ICorrelatedSektorRepository _correlatedSektorRepository;
        private readonly ISektorRepository _sektorRepository;

        public EmailService(IUnitOfWork unitOfWork, IUserRepository userRepository, IScenarioRepository scenariorepository,
        IRiskMatrixProjectRepository riskMatrixProjectRepository, ICorrelatedProjectRepository correlatedProjectRepository, ICorrelatedSektorRepository correlatedSektorRepository,
        ISektorRepository sektorRepository)
        {
            _unitOfWork = unitOfWork;
            _userRepository = userRepository;
            _scenarioRepository = scenariorepository;
            _riskMatrixProjectRepository = riskMatrixProjectRepository;
            _correlatedProjectRepository = correlatedProjectRepository;
            _correlatedSektorRepository = correlatedSektorRepository;
            _sektorRepository = sektorRepository;

            NameValueCollection appConfig = ConfigurationManager.AppSettings;
            senderEmail = appConfig["EmailSender"];
            senderName = appConfig["EmailSenderName"];
        }

        public void NotifyNewAccount(User user, bool async = false)
        {


            var mailMessage = new RegisterNewUser().Generate(user);
            new GmailClient().Send(mailMessage);
        }

        public void NotifyResetPassword(string source, string messageType, int requestId, int emailKepada, IList<int> emailCC, int emailDari, string templateNotif)
        {
            //string reqToken = "1101";
            //URL Website (IP Address)
            //string url = "http://localhost:2681/";
            var mailTo = _userRepository.Get(emailKepada).Email;
            var mailFrom = _userRepository.Get(emailDari).Email;
            IList<string> mailCollection = new List<string>();
            foreach(var item in emailCC)
            {
                var userCC = _userRepository.Get(item).Email;
                mailCollection.Add(userCC);
            }
            string mailCC = string.Join("; ", mailCollection);
            string title = null;
            switch (source)
            {
                case "Scenario":
                    title = _scenarioRepository.Get(requestId).NamaScenario;
                    break;

                case "RiskMatrixProject":
                    title = _riskMatrixProjectRepository.Get(requestId).Project.NamaProject;
                    break;
                case "CorrelatedProject":
                    title = _correlatedProjectRepository.Get(requestId).Project.NamaProject;
                    break;
                case "CorrelatedSektor":
                    var sektor = _correlatedSektorRepository.Get(requestId);
                    title = sektor.NamaSektor;
                    break;
            }
            var mailMessage = new ResetPassword().Generate(title, source, mailTo, mailCC, mailFrom, templateNotif);
            new GmailClient().Send(mailMessage);
        }

        public void NotifyResetPassword(string source, string messageType, int requestId, IList<int> emailKepada, int emailDari, string templateNotif)
        {
            //string reqToken = "1101";
            //URL Website (IP Address)
            //string url = "http://localhost:2681/";
            //var mailTo = _userRepository.Get(emailKepada).Email;
            var mailFrom = _userRepository.Get(emailDari).Email;
            IList<string> mailCollection = new List<string>();
            foreach (var item in emailKepada)
            {
                var userCC = _userRepository.Get(item).Email;
                mailCollection.Add(userCC);
            }
            string mailTo = string.Join("; ", mailCollection);
            string title = null;
            string mailCC = null;
            switch (source)
            {
                case "Scenario":
                    title = _scenarioRepository.Get(requestId).NamaScenario;
                    break;

                case "RiskMatrixProject":
                    title = _riskMatrixProjectRepository.Get(requestId).Project.NamaProject;
                    break;
                case "CorrelatedProject":
                    title = _correlatedProjectRepository.Get(requestId).Project.NamaProject;
                    break;
                case "CorrelatedSektor":
                    var sektor = _correlatedSektorRepository.Get(requestId);
                    title = sektor.NamaSektor;
                    break;
            }
            var mailMessage = new ResetPassword().Generate(title, source, mailTo, mailCC, mailFrom, templateNotif);
            new GmailClient().Send(mailMessage);
        }

        public void Send(string toEmail, string subject, string bodyMessage, bool async = false)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(senderEmail, senderName);
            mailMessage.To.Add(toEmail);
            mailMessage.Subject = subject;
            mailMessage.Body = bodyMessage;
            mailMessage.BodyEncoding = UTF8Encoding.UTF8;
            mailMessage.IsBodyHtml = true;
            //new GmailClient().Send(mailMessage);
        }

        public void Send(string fromEmail, string fromName, string toEmail, string subject, string bodyMessage, bool async = false)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(fromEmail, fromName);
            mailMessage.To.Add(toEmail);
            mailMessage.Subject = subject;
            mailMessage.Body = bodyMessage;
            mailMessage.BodyEncoding = UTF8Encoding.UTF8;
            mailMessage.IsBodyHtml = true;
            new GmailClient().Send(mailMessage);
        }
        public void SendPayrollInformation(string toAddress, string subject, string bodyMessage, bool async = false)
        {
            try
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(senderEmail, senderName);
                mailMessage.To.Add(toAddress);
                mailMessage.Subject = subject;
                mailMessage.Body = bodyMessage;
                mailMessage.BodyEncoding = UTF8Encoding.UTF8;
                mailMessage.IsBodyHtml = true;
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                new GmailClient().Send(mailMessage);
            }
            catch (Exception x)
            {
                string errror = x.Message;
            }
        }

        public void SendAutoValidateInformation(string toAddress, string subject, string bodyMessage, bool async = false)
        {
            try
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(senderEmail, senderName);
                mailMessage.To.Add(toAddress);
                mailMessage.Subject = subject;
                mailMessage.Body = bodyMessage;
                mailMessage.BodyEncoding = UTF8Encoding.UTF8;
                mailMessage.IsBodyHtml = true;
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                new GmailClient().Send(mailMessage);
            }
            catch (Exception x)
            {
                string errror = x.Message;
            }
        }

        public void SendGmail(IList<string> toAddress, string subject, string bodyMessage, byte[] attachmentFile, string attachmentFileName, string fileType, bool async = false)
        {
            try
            {
                using (Stream fileStream = new MemoryStream(attachmentFile))
                {
                    MailMessage mailMessage = new MailMessage();
                    mailMessage.From = new MailAddress(senderEmail, senderName);
                    mailMessage.Subject = subject;
                    mailMessage.Body = bodyMessage;
                    mailMessage.BodyEncoding = UTF8Encoding.UTF8;
                    mailMessage.IsBodyHtml = true;
                    mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                    foreach (var address in toAddress)
                    {
                        mailMessage.To.Add(address);
                        mailMessage.To.Add(address);
                    }

                    if (attachmentFile.IsNotNull())
                    {
                        Attachment attachment = new Attachment(fileStream, attachmentFileName, fileType); //"application/pdf"
                        mailMessage.Attachments.Add(attachment);
                    };

                    new GmailClient().Send(mailMessage);
                }
            }
            catch (Exception x)
            {
                string errror = x.Message;
            }

        }

        public void SendWithoutAttachment(IList<string> toAddress, string subject, string bodyMessage, bool async = false)
        {
            try
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(senderEmail, senderName);
                mailMessage.Subject = subject;
                mailMessage.Body = bodyMessage;
                mailMessage.BodyEncoding = UTF8Encoding.UTF8;
                mailMessage.IsBodyHtml = true;
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                foreach (var address in toAddress)
                {
                    mailMessage.To.Add(address);
                }

                new GmailClient().Send(mailMessage);
            }
            catch (Exception x)
            {
                string errror = x.Message;
            }

        }

        public void SendGmailFull(IList<string> toAddress, IList<string> ccAddress, IList<string> bccAddress, string subject, string bodyMessage, bool async)
        {
            try
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(senderEmail, senderName);
                mailMessage.Subject = subject;
                mailMessage.Body = bodyMessage;
                mailMessage.BodyEncoding = UTF8Encoding.UTF8;
                mailMessage.IsBodyHtml = true;
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                foreach (var address in toAddress)
                    mailMessage.To.Add(address);

                if (ccAddress.Count > 0)
                {
                    foreach (var address in ccAddress)
                        mailMessage.CC.Add(address);
                }

                if (bccAddress.Count > 0)
                {
                    foreach (var address in bccAddress)
                        mailMessage.Bcc.Add(address);
                }

                if (async)
                {
                    new GmailClient().Send(mailMessage);
                }
                else
                {
                    new GmailClient().Send(mailMessage);
                }
            }
            catch (Exception x)
            {
                string errror = x.Message;
            }

        }
    }
}
