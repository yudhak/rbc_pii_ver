﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using HR.Application.Params;
using HR.Core;
using HR.Domain;

namespace HR.Application
{
    public class LikehoodService : ILikehoodService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILikehoodRepository _likehoodRepository;
        private readonly ILikehoodDetailRepository _likehoodDetailRepository;
        private readonly IAuditLogService _auditLogService;

        public LikehoodService(IUnitOfWork uow, ILikehoodRepository likehoodRepository, ILikehoodDetailRepository likehoodDetailRepository, IAuditLogService auditLogService)
        {
            _unitOfWork = uow;
            _likehoodRepository = likehoodRepository;
            _likehoodDetailRepository = likehoodDetailRepository;
            _auditLogService = auditLogService;
        }

        #region Query
        public IEnumerable<Likehood> GetAll()
        {
            return _likehoodRepository.GetAll();
        }
        public IEnumerable<Likehood> GetAll(string keyword)
        {
            return _likehoodRepository.GetAll(keyword);
        }

        public Likehood Get(int id)
        {
            return _likehoodRepository.Get(id);
        }

        public Likehood GetDefault()
        {
            return _likehoodRepository.GetDefault();
        }

        public void IsExistOnEditing(int id, string namaLikehood)
        {
            if (_likehoodRepository.IsExist(id, namaLikehood))
            {
                throw new ApplicationException(string.Format("Nama Likehood {0} sudah ada.", namaLikehood));
            }
        }

        public void isExistOnAdding(string namaLikehood)
        {
            if (_likehoodRepository.IsExist(namaLikehood))
            {
                throw new ApplicationException(string.Format("Nama Likehood {0} sudah ada.", namaLikehood));
            }
        }
        #endregion Query

        #region Manipulation 
        public int Add(LikehoodParam param)
        {
            int id;
            Validate.NotNull(param.NamaLikehood, "Nama Likehood wajib diisi.");

            //count average here 
            isExistOnAdding(param.NamaLikehood);

            //Definition Likelihoods
            string[] definition = { "VL", "L", "M", "H", "VH" };
            IList<LikehoodDetail> likehoodDetails = new List<LikehoodDetail>();

            using (_unitOfWork)
            {
                Likehood model = new Likehood(param.NamaLikehood, param.CreateBy, param.CreateDate);
                _likehoodRepository.Insert(model);

                foreach (var item in definition)
                {
                    LikehoodDetail modelDetail = new LikehoodDetail(item, 0, 0, 0, model.Id, param.CreateBy, param.CreateDate);
                    likehoodDetails.Add(modelDetail);
                    _likehoodDetailRepository.Insert(modelDetail);
                }

                model.AddLikehoodDetail(likehoodDetails);

                _unitOfWork.Commit();
                id = model.Id;

                //Audit Log ADD 
                int audit = _auditLogService.AddLikehoodAudit(param, id);
            }

            return id;
        }

        public int Update(int id, LikehoodParam param)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Nama Likehood tidak ditemukan.");

            IsExistOnEditing(id, param.NamaLikehood);
            using (_unitOfWork)
            {
                //Audit Log UPDATE 
                int audit = _auditLogService.UpdateLikehoodAudit(param, id);

                model.Update(param.NamaLikehood, param.UpdateBy, param.UpdateDate);
                _likehoodRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;
            }
            return id;
        }

        public int Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Nama Likehood tidak ditemukan.");

            var modelDetail = _likehoodDetailRepository.GetByLikehoodId(id);
            using (_unitOfWork)
            {
                model.Delete(deleteBy, deleteDate);
                _likehoodRepository.Update(model);

                foreach (var item in modelDetail)
                {
                    item.Delete(model.Id, deleteBy, deleteDate);
                }

                _unitOfWork.Commit();
                id = model.Id;

                //Audit Log DELETE
                int audit = _auditLogService.DeleteLikehoodAudit(id, deleteBy);
            }
            return id;
        }

        public int Default(int id, LikehoodParam param)
        {
            int modelId;
            var model = this.Get(id);
            Validate.NotNull(model, "Nama Likehood tidak ditemukan.");

            var modelDefault = GetDefault();

            using (_unitOfWork)
            {
                if (modelDefault != null)
                {
                    //Remove current default
                    modelDefault.RemoveDefault(param.UpdateBy, param.UpdateDate);
                    _likehoodRepository.Update(modelDefault);
                    //Audit Log Default
                    int audit = _auditLogService.LikehoodRemoveDefault(modelDefault, param.UpdateBy);
                }
                //Set new default
                model.SetDefault(param.UpdateBy, param.UpdateDate);
                _likehoodRepository.Update(model);
                //Audit Log Default
                int audit2 = _auditLogService.LikehoodSetDefault(model);
                _unitOfWork.Commit();
            }
            
            modelId = model.Id;

            return modelId;
        }
        #endregion Manipulation
    }
}
