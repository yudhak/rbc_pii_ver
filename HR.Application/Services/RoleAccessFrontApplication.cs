﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;
using HR.Infrastructure;
using HR.Common;

namespace HR.Application
{
    public class RoleAccessFrontService : IRoleAccessFrontServices
    {
        private readonly IRoleAccessFrontRepository _roleAccessFrontRepository;
        private readonly IMenuRepository _menuRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRoleAccessServices _roleAccessServices;
        private readonly IMenuService _menuService;
        private readonly IRoleAccessRepository _roleAccessRepository;

        public RoleAccessFrontService(IRoleAccessFrontRepository roleAccessFrontRepository, IMenuRepository menuRepository, IUnitOfWork unitOfWork, IRoleAccessServices roleAccessServices, IMenuService menuService, IRoleAccessRepository roleAccessRepository)
        {
            _roleAccessFrontRepository = roleAccessFrontRepository;
            _unitOfWork = unitOfWork;
            _menuRepository = menuRepository;
            _roleAccessServices = roleAccessServices;
            _menuService = menuService;
            _roleAccessRepository = roleAccessRepository;
        }

        #region Query
        public IEnumerable<RoleAccessFront> GetAll(string keyword, int id)
        {
            return _roleAccessFrontRepository.GetAll(keyword, id);
        }

        public RoleAccessFront Get(int id)
        {
            return _roleAccessFrontRepository.Get(id);
        }

        public IEnumerable<Menu> GetSimilarName(int id)
        {
            return _menuRepository.GetSimilarName(id);
        }

        public void isExistOnAdding(int menuId, int roleId)
        {
            if (_roleAccessFrontRepository.IsExist(menuId, roleId))
            {
                throw new ApplicationException(string.Format("This menu already exist for this User Role."));
            }
            var menuUtama = _menuRepository.Get(menuId);
            if(menuUtama.ParentId != menuUtama.Sequence)
            {
                var childMenu = _menuRepository.GetChild(menuUtama.Sequence);
                if (childMenu != null)
                {
                    foreach (var item in childMenu)
                    {
                        if (_roleAccessFrontRepository.IsExist(item.Id, roleId))
                        {
                            throw new ApplicationException(string.Format("This menu have child menu that already exist for this User Role."));
                        }
                        if(item.Sequence != item.ParentId)
                        {
                            var grandChild = _menuRepository.GetChild(item.Sequence);
                            if (grandChild != null)
                            {
                                foreach (var item2 in grandChild)
                                {
                                    if (_roleAccessFrontRepository.IsExist(item2.Id, roleId))
                                    {
                                        throw new ApplicationException(string.Format("This menu have grand child menu that already exist for this User Role."));
                                    }
                                }
                            }
                        }               
                    }
                }
            }           
            var parentMenu = _menuRepository.GetParent(menuUtama.ParentId, menuUtama);
            if(parentMenu != null)
            {
                if (_roleAccessFrontRepository.IsExist(parentMenu.Id, roleId))
                {
                    throw new ApplicationException(string.Format("This menu have parent menu that already exist for this User Role."));
                }
                var parentMenu2 = _menuRepository.GetParent(parentMenu.ParentId, menuUtama);
                if (parentMenu2 != null)
                {
                    if (_roleAccessFrontRepository.IsExist(parentMenu2.Id, roleId))
                    {
                        throw new ApplicationException(string.Format("This menu have grand parent menu that already exist for this User Role."));
                    }
                }
            }                     
        }
        #endregion Query

        #region Manipulation 
        public int Add(RoleAccessFrontParam param)
        {
            isExistOnAdding(param.MenuId, param.RoleId);
            int id = 0;
            using (_unitOfWork)
            {
                
                RoleAccessFront model = new RoleAccessFront(param.RoleId, param.MenuId, param.Tambah, param.Ubah, param.Hapus, param.Lihat, param.Detail, param.IsActive, param.CreateBy, param.CreateDate);
                _roleAccessFrontRepository.Insert(model);
                _unitOfWork.Commit();
                id = model.Id;
                //RoleAccessParam roleAccessParam = new RoleAccessParam
                //{
                //    CreateBy = param.CreateBy,
                //    CreateDate = param.CreateDate,
                //    IsActive = param.IsActive,
                //    MenuId = param.MenuId,
                //    RoleId = param.RoleId,
                //    FlagFromFront = id
                //};
                //if(param.IsActive == true)
                //{
                //    int addParentIntoRoleAccess = _roleAccessServices.Add(roleAccessParam);
                //    AddChildIntoRoleAccess(param, roleAccessParam);
                //    _unitOfWork.Commit();
                //}               
            }           
            return id;
        }

        public int Update(int id, RoleAccessFrontParam param)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Id Setting Menu tidak ditemukan.");
            if(param.RoleId == model.RoleId)
            {
                bool validate = true;
                if(param.MenuId == model.MenuId)
                {
                    validate = false;
                }
                if(param.MenuId <= 20 && model.MenuId <= 20)
                {
                    validate = false;
                }
                if (param.MenuId > 20 && param.MenuId < 86 && model.MenuId > 20 && model.MenuId < 86)
                {
                    validate = false;
                }
                if (param.MenuId > 20 && param.MenuId < 86 && model.MenuId > 20 && model.MenuId < 86)
                {
                    validate = false;
                }
                if (param.MenuId > 99 && param.MenuId < 116 && model.MenuId > 99 && model.MenuId < 116)
                {
                    validate = false;
                }
                if (validate == true)
                {
                    isExistOnAdding(param.MenuId, param.RoleId);
                }

            }           
            if (param.RoleId != model.RoleId)
            {
                isExistOnAdding(param.MenuId, param.RoleId);
            }
            using (_unitOfWork)
            {
                var oldAccess = _roleAccessRepository.GetAllOldAccess(id);
                foreach(var item in oldAccess)
                {
                    _roleAccessRepository.Delete(item.Id);
                }
                model.Update(param.RoleId, param.MenuId, param.Tambah, param.Ubah, param.Hapus, param.Lihat, param.Detail, param.IsActive, param.UpdateBy, param.UpdateDate);
                _roleAccessFrontRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;
                //RoleAccessParam roleAccessParam = new RoleAccessParam
                //{
                //    CreateBy = param.UpdateBy,
                //    CreateDate = param.UpdateDate,
                //    IsActive = param.IsActive,
                //    MenuId = param.MenuId,
                //    RoleId = param.RoleId,
                //    FlagFromFront = id
                //};
                //if (param.IsActive == true)
                //{
                //    int addParentIntoRoleAccess = _roleAccessServices.Add(roleAccessParam);
                //    AddChildIntoRoleAccess(param, roleAccessParam);
                //    _unitOfWork.Commit();
                //}
            }
            return id;
        }

        public int Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Id Setting Menu tidak ditemukan.");

            using (_unitOfWork)
            {
                //var oldAccess = _roleAccessRepository.GetAllOldAccess(id);
                //foreach (var item in oldAccess)
                //{
                //    _roleAccessRepository.Delete(item.Id);
                //}
                model.Delete(deleteBy, deleteDate);
                _roleAccessFrontRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;
            }
            return id;
        }
        #endregion Manipulation

        #region Private
        public void AddChildIntoRoleAccess(RoleAccessFrontParam param, RoleAccessParam roleAccessParam)
        {
            var parentMenu = _menuRepository.Get(param.MenuId);
            if(parentMenu.Sequence != parentMenu.ParentId)
            {
                var menu = _menuRepository.GetChild(parentMenu.Sequence);
                foreach (var item in menu)
                {
                    if (item.ActionName == "index" || item.ActionName == "index2")
                    {
                        roleAccessParam.MenuId = item.Id;
                        int child1 = _roleAccessServices.Add(roleAccessParam);
                        if (item.Sequence != item.ParentId)
                        {
                            var menu2 = _menuRepository.GetChild(item.Sequence);
                            if (menu2 != null)
                            {
                                foreach (var item2 in menu2)
                                {
                                    if (item2.ActionName == "index" || item2.ActionName == "index2")
                                    {
                                        roleAccessParam.MenuId = item2.Id;
                                        int child2 = _roleAccessServices.Add(roleAccessParam);
                                    }
                                    if (param.Tambah == true)
                                    {
                                        if (item2.ActionName == "add")
                                        {
                                            roleAccessParam.MenuId = item2.Id;
                                            int child2 = _roleAccessServices.Add(roleAccessParam);
                                        }
                                    }
                                    if (param.Ubah == true)
                                    {
                                        if (item2.ActionName == "update")
                                        {
                                            roleAccessParam.MenuId = item2.Id;
                                            int child2 = _roleAccessServices.Add(roleAccessParam);
                                        }
                                    }
                                    if (param.Hapus == true)
                                    {
                                        if (item2.ActionName == "delete")
                                        {
                                            roleAccessParam.MenuId = item2.Id;
                                            int child2 = _roleAccessServices.Add(roleAccessParam);
                                        }
                                    }
                                    if (param.Detail == true)
                                    {
                                        if (item2.ActionName == "detail")
                                        {
                                            roleAccessParam.MenuId = item2.Id;
                                            int child2 = _roleAccessServices.Add(roleAccessParam);
                                            if (item2.Sequence != item2.ParentId)
                                            {
                                                var menu3 = _menuRepository.GetChild(item2.Sequence);
                                                if (menu3 != null)
                                                {
                                                    foreach (var item3 in menu3)
                                                    {
                                                        if (item3.ActionName == "index" || item3.ActionName == "index2")
                                                        {
                                                            roleAccessParam.MenuId = item3.Id;
                                                            int child3 = _roleAccessServices.Add(roleAccessParam);
                                                        }
                                                        if (param.Tambah == true)
                                                        {
                                                            if (item3.ActionName == "add")
                                                            {
                                                                roleAccessParam.MenuId = item3.Id;
                                                                int child3 = _roleAccessServices.Add(roleAccessParam);
                                                            }
                                                        }
                                                        if (param.Ubah == true)
                                                        {
                                                            if (item3.ActionName == "update")
                                                            {
                                                                roleAccessParam.MenuId = item3.Id;
                                                                int child3 = _roleAccessServices.Add(roleAccessParam);
                                                            }
                                                        }
                                                        if (param.Hapus == true)
                                                        {
                                                            if (item3.ActionName == "delete")
                                                            {
                                                                roleAccessParam.MenuId = item3.Id;
                                                                int child3 = _roleAccessServices.Add(roleAccessParam);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (param.Tambah == true)
                    {
                        if (item.ActionName == "add")
                        {
                            roleAccessParam.MenuId = item.Id;
                            int child1 = _roleAccessServices.Add(roleAccessParam);
                        }
                    }
                    if (param.Ubah == true)
                    {
                        if (item.ActionName == "update")
                        {
                            roleAccessParam.MenuId = item.Id;
                            int child1 = _roleAccessServices.Add(roleAccessParam);
                        }
                    }
                    if (param.Hapus == true)
                    {
                        if (item.ActionName == "delete")
                        {
                            roleAccessParam.MenuId = item.Id;
                            int child1 = _roleAccessServices.Add(roleAccessParam);
                        }
                    }
                    if (param.Detail == true)
                    {
                        if (item.ActionName == "detail")
                        {
                            roleAccessParam.MenuId = item.Id;
                            int child1 = _roleAccessServices.Add(roleAccessParam);
                            if (item.Sequence != item.ParentId)
                            {
                                var menu2 = _menuRepository.GetChild(item.Sequence);
                                if (menu2 != null)
                                {
                                    foreach (var item2 in menu2)
                                    {
                                        if (item2.ActionName == "index" || item2.ActionName == "index2")
                                        {
                                            roleAccessParam.MenuId = item2.Id;
                                            int child2 = _roleAccessServices.Add(roleAccessParam);
                                        }
                                        if (param.Tambah == true)
                                        {
                                            if (item2.ActionName == "add")
                                            {
                                                roleAccessParam.MenuId = item2.Id;
                                                int child2 = _roleAccessServices.Add(roleAccessParam);
                                            }
                                        }
                                        if (param.Ubah == true)
                                        {
                                            if (item2.ActionName == "update")
                                            {
                                                roleAccessParam.MenuId = item2.Id;
                                                int child2 = _roleAccessServices.Add(roleAccessParam);
                                            }
                                        }
                                        if (param.Hapus == true)
                                        {
                                            if (item2.ActionName == "delete")
                                            {
                                                roleAccessParam.MenuId = item2.Id;
                                                int child2 = _roleAccessServices.Add(roleAccessParam);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }            
        }
        #endregion Private
    }
}
