﻿using System;
using HR.Domain;
using System.Collections.Generic;
using System.Linq;
using HR.Common;
using HR.Application.Params;
using HR.Common.Validation;
using HR.Infrastructure;

namespace HR.Application
{
    public class CompareCalculationService : ICompareCalculationService
    {
        private readonly IDatabaseContext _iDatabaseContext;
        private readonly IRiskMatrixProjectRepository _riskMatrixProjectRepository;
        private readonly IStageTahunRiskMatrixRepository _stageTahunRiskMatrixRepository;
        private readonly IStageTahunRiskMatrixDetailRepository _stageTahunRiskMatrixDetailRepository;
        private readonly IRiskRegistrasiRepository _riskRegistrasiRepository;
        private readonly ILikehoodDetailRepository _likehoodDetailRepository;
        private readonly IScenarioRepository _scenarioRepository;
        private readonly ICorrelatedSektorRepository _correlatedSektorRepository;
        private readonly ICorrelatedProjectRepository _correlatedProjectRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly IScenarioDetailRepository _scenarioDetailRepository;
        private readonly ICorrelatedSektorDetailRepository _correlatedSektorDetailRepository;
        private readonly ICorrelatedProjectDetailRepository _correlatedProjectDetailRepository;
        private readonly ISektorRepository _sektorRepository;
        private readonly IAssetDataRepository _assetDataRepository;
        private readonly IPMNRepository _pmnRepository;
        private readonly IScenarioCalculationRepository _scenarioCalculationRepository;
        private readonly IAvailableCapitalProjectedRepository _availableCapitalProjectedRepository;
        private readonly ICalculationService _calculationService;


        public CompareCalculationService(IRiskMatrixProjectRepository riskMatrixProjectRepository, IStageTahunRiskMatrixRepository stageTahunRiskMatrixRepository, IStageTahunRiskMatrixDetailRepository stageTahunRiskMatrixDetailRepository,
            IRiskRegistrasiRepository riskRegistrasiRepository, ILikehoodDetailRepository likehoodDetailRepository, IScenarioRepository scenarioRepository, ICorrelatedSektorRepository correlatedSektorRepository, ICorrelatedProjectRepository correlatedProjectRepository,
            IProjectRepository projectRepository, IScenarioDetailRepository scenarioDetailRepository, ICorrelatedSektorDetailRepository correlatedSektorDetailRepository, ICorrelatedProjectDetailRepository correlatedProjectDetailRepository, ISektorRepository sektorRepository,
            IAssetDataRepository assetDataRepository, IPMNRepository pmnRepository, IScenarioCalculationRepository scenarioCalculationRepository, IDatabaseContext iDatabaseContext, IAvailableCapitalProjectedRepository availableCapitalProjectedRepository,
            ICalculationService calculationService)
        {
            _iDatabaseContext = iDatabaseContext;
            _riskMatrixProjectRepository = riskMatrixProjectRepository;
            _stageTahunRiskMatrixRepository = stageTahunRiskMatrixRepository;
            _stageTahunRiskMatrixDetailRepository = stageTahunRiskMatrixDetailRepository;
            _riskRegistrasiRepository = riskRegistrasiRepository;
            _likehoodDetailRepository = likehoodDetailRepository;
            _scenarioRepository = scenarioRepository;
            _correlatedSektorRepository = correlatedSektorRepository;
            _correlatedProjectRepository = correlatedProjectRepository;
            _projectRepository = projectRepository;
            _scenarioDetailRepository = scenarioDetailRepository;
            _correlatedSektorDetailRepository = correlatedSektorDetailRepository;
            _correlatedProjectDetailRepository = correlatedProjectDetailRepository;
            _sektorRepository = sektorRepository;
            _assetDataRepository = assetDataRepository;
            _pmnRepository = pmnRepository;
            _scenarioCalculationRepository = scenarioCalculationRepository;
            _availableCapitalProjectedRepository = availableCapitalProjectedRepository;
            _calculationService = calculationService;
        }

        public Calculation GetCompareCalculation(ScenarioCollection param)
        {
            var scenarioDefault = _scenarioRepository.GetDefault();
            Calculation calculation = new Calculation();
            IList<CalculationResult> calculationResults = new List<CalculationResult>();
            ScenarioTesting scenarioTesting = new ScenarioTesting();
            IList<int> tahun = new List<int>();
            

            IList<ScenarioProject> scenarioProjects = new List<ScenarioProject>();

            var scenarioDefaultExist = param.ScenarioProject.Where(x=> x.ScenarioId == scenarioDefault.Id).SingleOrDefault();

            if (scenarioDefaultExist == null)
            {
                var projects = _projectRepository.GetAll().Where(x => x.IsActive == true && x.IsDelete == false)    // your starting point - table in the "from" statement
                    .Join(scenarioDefault.ScenarioDetail.Where(x => x.IsDelete == false), // the source table of the inner join
                    proj => proj.Id,        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
                    scdet => scdet.ProjectId,   // Select the foreign key (the second part of the "on" clause)
                    (proj, scdet) =>  scdet.ProjectId ).ToArray(); // selection
                ScenarioProject scenarioProject = new ScenarioProject();
                scenarioProject.ScenarioId = scenarioDefault.Id;
                scenarioProject.ProjectId = projects;
                scenarioProjects.Add(scenarioProject);
            }

            foreach (var item in param.ScenarioProject)
            {
                scenarioProjects.Add(item);
            }

            foreach (var scenarios in scenarioProjects)
            {
                if (scenarios != null)
                {
                    var scenario = _scenarioRepository.Get(scenarios.ScenarioId);
                    Validate.NotNull(scenario, "Skenario id {0} tidak ditemukan.", scenario.Id);
                    var likelihoodDetail = scenario.Likehood.LikehoodDetails;
                    var riskRegistrasi = _riskRegistrasiRepository.GetAll().ToList();
                    var sektors = _sektorRepository.GetAll().ToList();
                    //var projects = scenario.ScenarioDetail.Where(x => x.IsDelete == false).ToList();
                    var proyek = _projectRepository.GetAll().Where(x => x.IsDelete == false)    // your starting point - table in the "from" statement
                    .Join(scenario.ScenarioDetail.Where(x => x.IsDelete == false), // the source table of the inner join
                    proj => proj.Id,        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
                    scdet => scdet.ProjectId,   // Select the foreign key (the second part of the "on" clause)
                    (proj, scdet) =>  scdet ).ToList(); // selection

                    var projects = proyek    // your starting point - table in the "from" statement
                    .Join(scenarios.ProjectId, // the source table of the inner join
                    proj => proj.ProjectId,        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
                    scdet => scdet,   // Select the foreign key (the second part of the "on" clause)
                    (proj, scdet) => proj).ToList(); // selection

                    CalculationResult calculationResult = new CalculationResult();

                    var generateYear = _calculationService.GenerateYear(scenarios);
                    calculationResult.CollectionYears = generateYear.ToArray();

                    IList<UndiversifiedRiskCapitalProjectCollection> undiversifiedRiskCapitalProjectCollection = new List<UndiversifiedRiskCapitalProjectCollection>();
                    AggregationOfProject aggregationOfProject = new AggregationOfProject();

                    IList<AggregationOfProjectCollection> aggregationOfProjectCollections = new List<AggregationOfProjectCollection>();
                    if (projects.Count > 0)
                    {
                        foreach (var item in projects)
                        {
                            IList<UndiversifiedYearCollection> undiversifiedYearCollection = new List<UndiversifiedYearCollection>();

                            var riskMatrixProject = _riskMatrixProjectRepository.GetByScenarioIdProjectId2(scenario.Id, item.ProjectId);
                            Validate.NotNull(riskMatrixProject, "Risk Matrix untuk proyek {0} pada skenario {1} tidak ditemukan / belum diisi.", item.Project.NamaProject, scenario.NamaScenario);

                            var project = item.Project;
                            var stageTahunRiskMatrix = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectId(riskMatrixProject.Id).ToList();

                            if (stageTahunRiskMatrix.Count > 0)
                            {
                                var projectDetail = _calculationService.GetUndiversifiedByProject(item.ProjectId, scenario.Id);
                                undiversifiedRiskCapitalProjectCollection.Add(projectDetail);
                                UndiversifiedRiskCapitalProjectCollection undiversifiedRiskCapitalProject = new UndiversifiedRiskCapitalProjectCollection();
                                undiversifiedRiskCapitalProject.ProjectId = project.Id;
                                undiversifiedRiskCapitalProject.NamaProject = project.NamaProject;
                                undiversifiedRiskCapitalProject.SektorId = project.SektorId;
                                undiversifiedRiskCapitalProject.NamaSektor = project.Sektor.NamaSektor;
                                undiversifiedRiskCapitalProject.UndiversifiedYearCollection = undiversifiedYearCollection.ToArray();
                                undiversifiedRiskCapitalProject.RiskRegistrasi = riskRegistrasi.ToArray();
                            }
                            else
                            {
                                throw new ApplicationException(string.Format("Risk Matrix untuk proyek {0} pada skenario {1} tidak ditemukan / belum diisi.", item.Project.NamaProject, scenario.NamaScenario));
                            }
                            AggregationOfProjectCollection aggregationOfProjectCollection = new AggregationOfProjectCollection();
                            aggregationOfProjectCollection.NamaProject = item.Project.NamaProject;
                            aggregationOfProjectCollection.ProjectId = item.Project.Id;
                            aggregationOfProjectCollection.NamaSektor = item.Project.Sektor.NamaSektor;
                            aggregationOfProjectCollection.SektorId = item.Project.SektorId;
                            aggregationOfProjectCollections.Add(aggregationOfProjectCollection);
                        }
                        aggregationOfProject.AggregationOfProjectCollection = aggregationOfProjectCollections.ToArray();
                    }

                    calculationResult.ScenarioId = scenario.Id;
                    calculationResult.NamaScenario = scenario.NamaScenario;
                    calculationResult.ProjectDetail = undiversifiedRiskCapitalProjectCollection.ToArray();

                    #region Generate AggregationOfProject
                    //get aggregation project undiversified
                    var aggregationUndiversifiedProjectCapital = _calculationService.GetAggregationUndiversifiedProjectCapital(generateYear, calculationResult.ProjectDetail);
                    aggregationOfProject.AggregationUndiversifiedProjectCapital = aggregationUndiversifiedProjectCapital.ToArray();
                    //get aggregation project intra-diversified
                    var aggregationIntraDiversifiedProjectCapital = _calculationService.GetAggregationIntraDiversifiedProjectCapital(generateYear, calculationResult.ProjectDetail);
                    aggregationOfProject.AggregationIntraDiversifiedProjectCapital = aggregationIntraDiversifiedProjectCapital.ToArray();
                    //get aggregation project inter-diversified
                    var aggregationInterDiversifiedProjectCapital = _calculationService.GetAggregationInterDiversifiedProjectCapital(scenario.Id, generateYear, aggregationOfProject);
                    aggregationOfProject.AggregationInterDiversifiedProjectCapital = aggregationInterDiversifiedProjectCapital.ToArray();
                    calculationResult.AggregationOfProject = aggregationOfProject;
                    #endregion Generate AggregationOfProject

                    #region Generate AggregationOfSektor
                    AggregationSektor aggregationSektor = new AggregationSektor();
                    //Undiversified
                    var aggregationOfSektor = _calculationService.GetUndiversifiedAggregationOfSector(scenario.Id, aggregationOfProject);
                    //IntraDiversified
                    var intraDiversified = _calculationService.GenerateIntraProjectDiversified(scenario, aggregationOfSektor.Sektor, aggregationOfProjectCollections, aggregationIntraDiversifiedProjectCapital);
                    //InterDiversified
                    var interDiversified = _calculationService.GenerateInterProjectDiversified(scenario, aggregationOfSektor.Sektor, aggregationOfProjectCollections, aggregationInterDiversifiedProjectCapital);
                    #endregion Generate AggregationOfSektor

                    #region Generate AggregationOfRisk
                    AggregationRisk aggregationRisk = new AggregationRisk();
                    //get aggregation risk registrasi
                    aggregationRisk.RiskRegistrasi = riskRegistrasi.ToArray();
                    //get aggregation risk undiversified
                    var undiversifiedAggregationRisk = _calculationService.GetUndiversifiedAggregationOfRisk(riskRegistrasi, generateYear, calculationResult.ProjectDetail);
                    aggregationRisk.UndiversifiedAggregationRisk = undiversifiedAggregationRisk;
                    //get aggregation risk intra-diversified 
                    var intraProjectDiversifiedAggregationRisk = _calculationService.GetIntraProjectDiversifiedAggregationOfRisk(riskRegistrasi, generateYear, calculationResult.ProjectDetail);
                    aggregationRisk.IntraProjectDiversifiedAggregationRisk = intraProjectDiversifiedAggregationRisk;
                    //get aggregation risk intra-diversified 
                    var interProjectDiversifiedAggregationRisk = _calculationService.GetInterProjectDiversifiedAggregationOfRisk(riskRegistrasi, generateYear, intraProjectDiversifiedAggregationRisk.RiskYearCollection, aggregationIntraDiversifiedProjectCapital, aggregationInterDiversifiedProjectCapital);
                    aggregationRisk.InterProjectDiversifiedAggregationRisk = interProjectDiversifiedAggregationRisk;

                    calculationResult.AggregationRisk = aggregationRisk;
                    #endregion Generate AggregationOfRisk

                    aggregationSektor.UndiversifiedAggregationSektor = aggregationOfSektor.UndiversifiedAggregationSektor;
                    aggregationSektor.Sektor = aggregationOfSektor.Sektor;
                    aggregationSektor.IntraProjectDiversifiedAggregationSektor = intraDiversified;
                    aggregationSektor.InterProjectDiversifiedAggregationSektor = interDiversified;

                    calculationResult.AggregationSektor = aggregationSektor;

                    var assetProjection = _calculationService.GenerateAssetProjectionInvestmentStrategy(aggregationOfProject.AggregationInterDiversifiedProjectCapital, generateYear, scenario, aggregationOfProject);
                    calculationResult.AssetProjectionLiquidity = assetProjection;

                    calculationResults.Add(calculationResult);

                    //Generate tahun Scenario Testing
                    var years = generateYear.Except(tahun);
                    foreach (var yearValue in years)
                    {
                        tahun.Add(yearValue);
                    }

                }
            }
            foreach (var calculationResult in calculationResults)
            {
                if (calculationResult.ScenarioId == scenarioDefault.Id)
                {
                    calculation.CalculationResult = calculationResult;
                }
            }

            var arrayTahun = tahun.ToArray();
            Array.Sort(arrayTahun);
            scenarioTesting.Year = arrayTahun;
            calculation.ScenarioTesting = scenarioTesting;

            var scenarioTestingCollection = _calculationService.GetScenarioTestingCollection(arrayTahun, calculationResults);
            var sensitivity = _calculationService.GetSensitivity(arrayTahun, calculationResults);
            var stressTesting = _calculationService.GetStressTesting(arrayTahun, calculationResults);

            //Calculation Scenario Testing
            calculation.ScenarioTesting.ScenarioTestingCollection = scenarioTestingCollection.ToArray();
            calculation.ScenarioTesting.Sensitivity = sensitivity;
            calculation.ScenarioTesting.StressTesting = stressTesting;

            return calculation;
        }
    }
}
