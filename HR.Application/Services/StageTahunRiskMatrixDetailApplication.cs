﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;
using HR.Infrastructure;

namespace HR.Application
{
	public class StageTahunRiskMatrixDetailService : IStageTahunRiskMatrixDetailService
	{
		private readonly IUnitOfWork _unitOfWork;
		private readonly IStageTahunRiskMatrixRepository _stageTahunRiskMatrixRepository;
		private readonly IStageTahunRiskMatrixDetailRepository _stageTahunRiskMatrixDetailRepository;
		private readonly IRiskRegistrasiRepository _riskRegistrasiRepository;
		private readonly ILikehoodRepository _likehoodRepository;
		private readonly ILikehoodDetailRepository _likehoodDetailRepository;
		private readonly IAuditLogService _auditLogService;
		private readonly IRiskMatrixProjectRepository _riskMatrixProjectRepository;
		private readonly IStatusRepository _statusRepository;
		private readonly IUserRepository _userRepository;
		private readonly IApprovalRepository _approvalRepository;
		private readonly IDatabaseContext _databaseContext;
		private readonly IProjectRepository _projectRepository;
		private readonly IEmailService _emailService;
		private readonly IUserService _userService;
		private readonly IRiskMatriksTemporerRepository _riskMatriksTemporerRepository;

		private readonly IMasterApprovalRiskMatrixProjectRepository _masterApprovalRiskMatrixProjectRepository;

		public StageTahunRiskMatrixDetailService(IUnitOfWork unitOfWork, IStageTahunRiskMatrixRepository stageTahunRiskMatrixRepository, IStageTahunRiskMatrixDetailRepository stageTahunRiskMatrixDetailRepository,
			IRiskRegistrasiRepository riskRegistrasiRepository, ILikehoodRepository likehoodRepository, ILikehoodDetailRepository likehoodDetailRepository, 
			IAuditLogService auditLogService, IRiskMatrixProjectRepository riskMatrixProjectRepository, IStatusRepository statusRepository, IUserRepository userRepository, IApprovalRepository approvalRepository,
	 
			 IMasterApprovalRiskMatrixProjectRepository masterApprovalRiskMatrixProjectRepository, IDatabaseContext databaseContext, IProjectRepository projectRepository
			, IEmailService emailService, IUserService userService,  IRiskMatriksTemporerRepository riskMatriksTemporerRepository)
		{
			_unitOfWork = unitOfWork;
			_stageTahunRiskMatrixRepository = stageTahunRiskMatrixRepository;
			_stageTahunRiskMatrixDetailRepository = stageTahunRiskMatrixDetailRepository;
			_riskRegistrasiRepository = riskRegistrasiRepository;
			_likehoodRepository = likehoodRepository;
			_likehoodDetailRepository = likehoodDetailRepository;
			_auditLogService = auditLogService;
			_riskMatrixProjectRepository = riskMatrixProjectRepository;
			_userRepository = userRepository;
			_statusRepository = statusRepository;
			_approvalRepository = approvalRepository;
			_masterApprovalRiskMatrixProjectRepository = masterApprovalRiskMatrixProjectRepository;
			_databaseContext = databaseContext;
			_projectRepository = projectRepository;
			_emailService = emailService;
			_userService = userService;
			//_riskMatriksTemporerService = riskMatriksTemporerService;
			_riskMatriksTemporerRepository = riskMatriksTemporerRepository;
		}

		public void IsExist(int riskMatrixProjectId)
		{
			if (_stageTahunRiskMatrixDetailRepository.IsExist(riskMatrixProjectId))
			{
				throw new ApplicationException(string.Format("Risk Matriks project sudah ada."));
			}
		}

		public IEnumerable<StageTahunRiskMatrixDetail> GetByStageTahunRiskMatrixId(int stageTahunRiskMatrixId)
		{
			return _stageTahunRiskMatrixDetailRepository.GetByStageTahunRiskMatrixId(stageTahunRiskMatrixId);
		}

		public RiskMatrixCollectionParameter GetByRiskMatrixProjectId(int riskMatrixProjectId)
		{
			RiskMatrixCollectionParameter collection = new RiskMatrixCollectionParameter();
			IList<RiskMatrixCollection> matrixCollection = new List<RiskMatrixCollection>();
			var riskMatrixProject = _riskMatrixProjectRepository.Get(riskMatrixProjectId);
			collection.StatusId = riskMatrixProject.StatusId;
			var data = _stageTahunRiskMatrixDetailRepository.GetByRiskMatrixProjectId(riskMatrixProjectId);
			if (data.Count() > 0)
			{
				var cred = data.ToList();
				var years = data.GroupBy(y => y.StageTahunRiskMatrixId).ToList();

				collection.CreateBy = cred[0].CreateBy;
				collection.CreateDate = cred[0].CreateDate;
				if (years.Count > 0)
				{
					for (int i = 0; i < years.Count; i++)
					{
						RiskMatrixCollection matrixCollectionByYear = new RiskMatrixCollection();
						IList<RiskMatrixValue> matrixValues = new List<RiskMatrixValue>();

						var yearId = years[i].Key;
						matrixCollectionByYear.StageTahunRiskMatrixId = yearId;

						for (int r = 0; r < years[i].Count(); r++)
						{
							RiskMatrixValue matrixValue = new RiskMatrixValue(); //temporary

							IList<decimal?> values = new List<decimal?>();
							IList<bool?> warnas = new List<bool?>();

							var matVal = years[i].ToList();
							matrixValue.RiskRegistrasiId = matVal[r].RiskRegistrasiId;

							warnas.Add(Convert.ToBoolean(matVal[r].WarnaExpose));
							warnas.Add(Convert.ToBoolean(matVal[r].WarnaLikelihood));

							var exposure = matVal[r].NilaiExpose;
							var likehoodDetail = matVal[r].LikehoodDetailId;
							values.Add(exposure);
							values.Add(likehoodDetail);
							decimal?[] valueExLi = values.ToArray();
							bool?[] warnasExLi = warnas.ToArray();

							matrixValue.Colors = warnasExLi;
							matrixValue.Values = valueExLi;
							matrixValues.Add(matrixValue);
						}
						var maximumExposure = _stageTahunRiskMatrixRepository.Get(yearId);
						Validate.NotNull(maximumExposure, "Stage tahun risk matrix dengan id {0} tidak ditemukan.", yearId);

						matrixCollectionByYear.RiskMatrixValue = matrixValues;
						matrixCollectionByYear.MaximumNilaiExpose = maximumExposure.MaximumNilaiExpose;
						matrixCollectionByYear.Warna = Convert.ToBoolean(maximumExposure.IsUpdate);
						matrixCollection.Add(matrixCollectionByYear);
					}
								   
					collection.RiskMatrixCollection = matrixCollection.ToArray();
				}
			}
			return collection;
		}

		public int Add(RiskMatrixCollectionParameter param)
		{
			int id = 0;
			var riskMatrixProject = _stageTahunRiskMatrixRepository.Get(param.RiskMatrixCollection[0].StageTahunRiskMatrixId);
			Validate.NotNull(riskMatrixProject, "Risk Matrix Project ID tidak boleh kosong.");

			IsExist(riskMatrixProject.RiskMatrixProjectId);
			using (_unitOfWork)
			{
				for (int i = 0; i < param.RiskMatrixCollection.Length; i++)
				{
					var stageTahunRiskMatrix = _stageTahunRiskMatrixRepository.Get(param.RiskMatrixCollection[i].StageTahunRiskMatrixId);
					Validate.NotNull(stageTahunRiskMatrix, "Stage Tahun Risk Matrix tidak boleh kosong.");

					foreach (var item in param.RiskMatrixCollection[i].RiskMatrixValue)
					{
						var riskRegistrasi = _riskRegistrasiRepository.Get(item.RiskRegistrasiId);
						Validate.NotNull(riskRegistrasi, "Risk Regsitrasi tidak boleh kosong.");

						int likehoodDetailId = 0;
						if (item.Values[1] != 0)
						{
							var likehoodDetail = _likehoodDetailRepository.Get((int)item.Values[1]);
							Validate.NotNull(likehoodDetail, "Definisi Likelihood tidak boleh kosong.");

							likehoodDetailId = likehoodDetail.Id;
						}

						var exposure = item.Values[0];
						decimal? nilaiExposure = exposure;
						StageTahunRiskMatrixDetail model = new StageTahunRiskMatrixDetail(stageTahunRiskMatrix, riskRegistrasi, stageTahunRiskMatrix.RiskMatrixProject.Id, likehoodDetailId, nilaiExposure, param.CreateBy, param.CreateDate);
						_stageTahunRiskMatrixDetailRepository.Insert(model);
						id = model.Id;
					}
					_unitOfWork.Commit();
				}
			}
			return id;
		}

		public int Update(int id, RiskMatrixCollectionParameter param)
		{
			int modelId = 0;
			var stageTahunRiskMatrixWithId = _stageTahunRiskMatrixRepository.Get(param.RiskMatrixCollection[0].StageTahunRiskMatrixId);
			var oldStageTahunRiskMatrixDetail = _stageTahunRiskMatrixDetailRepository.GetByRiskMatrixProjectId(stageTahunRiskMatrixWithId.RiskMatrixProjectId);
			int jumlahdetail = 0;
			var riskMatrixProject = _riskMatrixProjectRepository.Get(stageTahunRiskMatrixWithId.RiskMatrixProjectId);
			var checkAddn = _stageTahunRiskMatrixDetailRepository.GetByRiskMatrixProjectId(id);
			if (checkAddn.Count() == 0 || checkAddn == null)
			{
				int hey = _auditLogService.AddStageTahunRiskMatrixDetailAudit(id, Convert.ToInt32(param.UpdateBy));
			}
				
			int emailDari = 0;
			int emailKepada = 0;
			List<int> emailCC = new List<int>();
			string source = "RiskMatrixProject";
			string typePesan = "";
			Status statusEmail = null;
			string templateNotif;
			string keterangan = "";
			int requestId = 0;
			var userUpdate = _userService.Get(param.UpdateBy.GetValueOrDefault());
			if (riskMatrixProject.StatusId == 2)
			{
				var PerluExistingName = _riskMatrixProjectRepository.GetByProjectRiskMatrixScenarioId2(riskMatrixProject.ProjectId, riskMatrixProject.RiskMatrixId, riskMatrixProject.ScenarioId, param.UpdateBy.GetValueOrDefault()).Where(x => x.StatusId != 2);
				int ValidateName = PerluExistingName.Count();
				if (ValidateName >= 1)
				{
					string dodol = null;
					Validate.NotNull(dodol, "You cannot have 2 draft with same risk matrix.");
					return 0;
				}
				else
				{
					var CheckMaster = _masterApprovalRiskMatrixProjectRepository.GetAllByProjectId(stageTahunRiskMatrixWithId.RiskMatrixProject.ProjectId);
					var checkoutMaster = CheckMaster.Count();
					if (checkoutMaster == 0)
					{
						Validate.NotNull(null, "User must setting Approval for Risk Matrix first.");
					}
					int nomorUrutStatus = 0;
					int dodol = Convert.ToInt32(param.UpdateBy);
					var masterApproval = _masterApprovalRiskMatrixProjectRepository.GetByProjectIdUserId(riskMatrixProject.ProjectId,dodol);
					
					if (masterApproval != null)
					{
						nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
					}
					if (masterApproval == null && (userUpdate.RoleId == 3 || userUpdate.RoleId == 4 || userUpdate.RoleId == 5))
					{
						masterApproval = _masterApprovalRiskMatrixProjectRepository.GetByProjectIdRoleId(riskMatrixProject.ProjectId, userUpdate.RoleId);
						nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
					}
					if (nomorUrutStatus != 0)
					{
						param.IsUpdate = true;
						var duplicate = Duplicate(param, riskMatrixProject);
						
						var status = _statusRepository.GetStatus(nomorUrutStatus);
						if (param.IsSend == true && status.Id != 2)
						{
							param.IsDraftApproval = true;
							int idDuplicate = Duplicate(param, riskMatrixProject);

							AddApproval(idDuplicate, status, param.UpdateBy, param.UpdateDate, nomorUrutStatus);

							param.StatusId = status.Id;
						   // int idTemporer = _riskMatriksTemporerService.SubmitRiskMatrixProjectAudit(duplicate, param.UpdateBy.GetValueOrDefault());
							//_riskMatriksTemporerService.
							_databaseContext.SaveChanges();

							#region email
							if (param.IsSend == true && param.IsStatusApproval == false || param.IsStatusApproval == null)
							{
								if (nomorUrutStatus != 3)
								{
									emailDari = param.UpdateBy.GetValueOrDefault();

									var master = _masterApprovalRiskMatrixProjectRepository.GetAllByProjectId(riskMatrixProject.ProjectId).Where(x => x.NomorUrutStatus == nomorUrutStatus + 1).SingleOrDefault();
									Validate.NotNull(master, "Master Approval Risk Matrix Project is not found.");


									typePesan = status.StatusDescription;

									requestId = riskMatrixProject.Id;

									if (nomorUrutStatus < 2)
									{
										emailKepada = master.UserId.GetValueOrDefault();
										master = _masterApprovalRiskMatrixProjectRepository.GetAllByProjectId(riskMatrixProject.ProjectId).Where(x => x.NomorUrutStatus == nomorUrutStatus + 2).SingleOrDefault();
										Validate.NotNull(master, "Master Approval Risk Matrix Project is not found.");

										emailCC.Add(master.UserId.GetValueOrDefault());
										//source
										 _emailService.NotifyResetPassword(source, typePesan, requestId, emailKepada, emailCC, emailDari, param.TemplateNotif);

									}
									else
									{
										emailCC.Add(master.UserId.GetValueOrDefault());

										_emailService.NotifyResetPassword(source, typePesan, requestId, emailCC, emailDari, param.TemplateNotif);
									}

								}
							}
							#endregion email
						}
						if (status.Id == 2)
						{
							if (param.IsSend == true)
							{
								//var deleteScenario = Delete2(id, Convert.ToInt32(param.UpdateBy), Convert.ToDateTime(param.UpdateDate));
								var deleteRiskMatrixProject = Update2(duplicate, param);
							}
						}
						
						return duplicate;
					}
					else
					{
						param.IsUpdate = true;
						var duplicate = Duplicate(param, riskMatrixProject);
						var status = _statusRepository.GetStatus(nomorUrutStatus);
						if (param.IsSend == true)
						{
							param.IsDraftApproval = true;
							int idDuplicate = Duplicate(param, riskMatrixProject);

							AddApproval(idDuplicate, status, param.UpdateBy, param.UpdateDate, nomorUrutStatus);

							param.StatusId = status.Id;
							//int idTemporer = _riskMatriksTemporerService.SubmitRiskMatrixProjectAudit(duplicate, param.UpdateBy.GetValueOrDefault());
							_databaseContext.SaveChanges();

							#region email
							if (param.IsSend == true && param.IsStatusApproval == false || param.IsStatusApproval == null)
							{
								if (nomorUrutStatus != 3)
								{
									emailDari = param.UpdateBy.GetValueOrDefault();

									var master = _masterApprovalRiskMatrixProjectRepository.GetAllByProjectId(riskMatrixProject.ProjectId).Where(x => x.NomorUrutStatus == nomorUrutStatus + 1).SingleOrDefault();
									Validate.NotNull(master, "Master Approval Risk Matrix Project is not found.");


									typePesan = status.StatusDescription;

									requestId = riskMatrixProject.Id;

									if (nomorUrutStatus < 2)
									{
										emailKepada = master.UserId.GetValueOrDefault();
										master = _masterApprovalRiskMatrixProjectRepository.GetAllByProjectId(riskMatrixProject.ProjectId).Where(x => x.NomorUrutStatus == nomorUrutStatus + 2).SingleOrDefault();
										Validate.NotNull(master, "Master Approval Risk Matrix Project is not found.");

										emailCC.Add(master.UserId.GetValueOrDefault());
										//source
										 _emailService.NotifyResetPassword(source, typePesan, requestId, emailKepada, emailCC, emailDari, param.TemplateNotif);

									}
									else
									{
										emailCC.Add(master.UserId.GetValueOrDefault());

										_emailService.NotifyResetPassword(source, typePesan, requestId, emailCC, emailDari, param.TemplateNotif);
									}

								}
							}
							#endregion email
						}
						ValidasiWarnaDiSemuaTahun(id);
						return duplicate;                        
					}
				}
			}
			else
			{
				var PerluExistingName = _riskMatrixProjectRepository.GetByProjectRiskMatrixScenarioId(riskMatrixProject.ProjectId, riskMatrixProject.RiskMatrixId, riskMatrixProject.ScenarioId);
				int ValidateName = PerluExistingName.Count();

				using (_unitOfWork)
				{
					#region Approval
					Status status = null;

					bool IsSend;
					bool IsStatusApproval;
					//Get Status from role id
					int nomorUrutStatus = 0;
					if (riskMatrixProject.StatusId != 2)
					{
						//Jika Risk Matrix Project saat ini statusnya 1/submit, tidak bisa diedit
						if (riskMatrixProject.StatusId == 1 && param.IsStatusApproval == null)
						{
							throw new ApplicationException(string.Format("Risiko Matriks tidak bisa diedit karena dalam proses approval"));
						}
						//Jika Risk Matrix Project saat ini statusnya 3/reject, maka akan kirim kembali ke Approval 
						//atau jika user menekan tombol kirim, maka akan kirim ke tabel Approval
						else if ((riskMatrixProject.StatusId == 3 && param.IsSend == true) && param.IsStatusApproval == null || param.IsSend == true && param.IsStatusApproval == null)
						{
							var CheckMaster = _masterApprovalRiskMatrixProjectRepository.GetAllByProjectId(stageTahunRiskMatrixWithId.RiskMatrixProject.ProjectId);
							var checkoutMaster = CheckMaster.Count();
							if (checkoutMaster == 0)
							{
								Validate.NotNull(null, "User must setting Approval for Risk Matrix first.");
							}
							var userCreate = _userRepository.Get(param.UpdateBy.GetValueOrDefault());
							Validate.NotNull(userCreate, "User is not found.");

							//get role level from user id
							var masterApproval = _masterApprovalRiskMatrixProjectRepository.GetByProjectIdUserId(riskMatrixProject.ProjectId, userCreate.Id);
							if (masterApproval != null)
							{
								nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
							}
							if (masterApproval == null && (userCreate.RoleId == 3 || userCreate.RoleId == 4 || userCreate.RoleId == 5))
							{
								masterApproval = _masterApprovalRiskMatrixProjectRepository.GetByProjectIdRoleId(riskMatrixProject.ProjectId, userCreate.RoleId);
								nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
							}
							status = _statusRepository.GetStatus(nomorUrutStatus);
							Validate.NotNull(status, "Status is not found.");

							if (status != null && status.Id != 2)
							{
								param.IsUpdate = true;
								param.IsDraftApproval = true;
								int idDuplicate = Duplicate(param, riskMatrixProject);

								//insert into approval
								AddApproval(idDuplicate, status, param.UpdateBy, param.UpdateDate, nomorUrutStatus);
								//int idTemporer = _riskMatriksTemporerService.SubmitRiskMatrixProjectAudit(riskMatrixProject.Id, param.UpdateBy.GetValueOrDefault());
								_databaseContext.SaveChanges();
							}
							//_unitOfWork.Commit();

							riskMatrixProject.UpdateStatus(status, param.UpdateBy, param.UpdateDate);
							param.StatusId = status.Id;
							
						}
					}
					IsStatusApproval = param.IsStatusApproval.GetValueOrDefault();
					param.IsStatusApproval = null;
					IsSend = param.IsSend.GetValueOrDefault();
					param.IsSend = null;
					templateNotif = param.TemplateNotif;
					param.TemplateNotif = null;
					param.IsDraftApproval = null;
					param.IsUpdate = null;
					if (param.StatusId != null)
					{
						statusEmail = _statusRepository.Get(param.StatusId.GetValueOrDefault());
						param.StatusId = null;
					}
					if (param.NomorUrutStatus.GetValueOrDefault() != 0)
					{
						nomorUrutStatus = param.NomorUrutStatus.GetValueOrDefault();
						param.NomorUrutStatus = null;
					}
					keterangan = param.Keterangan;
					param.Keterangan = null;
					#endregion Approval
					IList<StageTahunRiskMatrixDetail> newStageTahunRiskMatrixDetail = new List<StageTahunRiskMatrixDetail>();
					//insert new data

					for (int i = 0; i < param.RiskMatrixCollection.Length; i++)
					{
						var stageTahunRiskMatrix = _stageTahunRiskMatrixRepository.Get(param.RiskMatrixCollection[i].StageTahunRiskMatrixId);
						Validate.NotNull(stageTahunRiskMatrix, "Stage Tahun Risk Matrix tidak boleh kosong.");
						foreach (var item in param.RiskMatrixCollection[i].RiskMatrixValue)
						{
							var riskRegistrasi = _riskRegistrasiRepository.Get(item.RiskRegistrasiId);
							Validate.NotNull(riskRegistrasi, "Risk Regsitrasi tidak boleh kosong.");

							int likehoodDetailId = 0;
							Validate.NotNull(item.Values[1], "Risk registrasi {0} tidak boleh null.", riskRegistrasi.KodeMRisk);
							Validate.NotNull(item.Values[0], "Risk registrasi {0} tidak boleh null / kosong.", riskRegistrasi.KodeMRisk);

							if (item.Values[1] != 0)
							{
								var likehoodDetail = _likehoodDetailRepository.Get((int)item.Values[1]);
								Validate.NotNull(likehoodDetail, "Definisi Likelihood tidak boleh kosong.");

								likehoodDetailId = likehoodDetail.Id;
							}

							var exposure = item.Values[0];
							decimal? nilaiExposure = exposure;
							StageTahunRiskMatrixDetail model = new StageTahunRiskMatrixDetail(stageTahunRiskMatrix, riskRegistrasi, stageTahunRiskMatrix.RiskMatrixProject.Id, likehoodDetailId, nilaiExposure, param.UpdateBy, param.UpdateDate);

							newStageTahunRiskMatrixDetail.Add(model);
							var checkIn = _stageTahunRiskMatrixDetailRepository.GetByRiskMatrixProjectId(stageTahunRiskMatrix.RiskMatrixProject.Id);
							if (checkIn.Count() == 0 || checkIn == null)
							{
								_stageTahunRiskMatrixDetailRepository.Insert(model);                                
								jumlahdetail++;
							}
						}

						//update maximum expose    
						var maximumDataList = param.RiskMatrixCollection[i].RiskMatrixValue.ToList().OrderByDescending(x => x.Values[0]);
						var maximumData = String.Format("{0:0.0000000000000}", maximumDataList.FirstOrDefault().Values[0]);
						var checkMaxExpos = String.Format("{0:0.0000000000000}", param.RiskMatrixCollection[i].MaximumNilaiExpose);
						if (stageTahunRiskMatrix.MaximumNilaiExpose != null)
						{
							var parentMaxDefault = param.RiskMatrixCollection.Where(x => x.StageTahunRiskMatrixId == stageTahunRiskMatrix.Id).SingleOrDefault();
							var valueMaxDefault = parentMaxDefault.RiskMatrixValue.OrderByDescending(x => x.Values[0]).ToList();
							if (stageTahunRiskMatrix.MaximumNilaiExpose != Convert.ToDecimal(checkMaxExpos))
							{
								stageTahunRiskMatrix.IsUpdate = true;
							}
							if (parentMaxDefault.MaximumNilaiExpose == valueMaxDefault[0].Values[0])
							{
								stageTahunRiskMatrix.IsUpdate = false;
							}
						}                        
						var maximumExpose = param.RiskMatrixCollection[i].MaximumNilaiExpose;                       
						Validate.NotNull(maximumExpose, "Nilai Maximum Exposure tidak boleh kosong. Silakan isi nilai Maximum Expose tahun {0}.", stageTahunRiskMatrix.Tahun);
						if (Convert.ToDecimal(maximumData) != Convert.ToDecimal(checkMaxExpos))
						{                            
							int AuditMaxExposure = _auditLogService.MaxStageTahunRiskMatrixAudit(stageTahunRiskMatrix.Id, String.Format("{0:0.0000000000000}", stageTahunRiskMatrix.MaximumNilaiExpose), checkMaxExpos, Convert.ToInt32(param.UpdateBy));
							stageTahunRiskMatrix.IsUpdate = true;
						}                        
						stageTahunRiskMatrix.UpdateMaximumExposeValue(Convert.ToDecimal(param.RiskMatrixCollection[i].MaximumNilaiExpose), param.UpdateBy, param.UpdateDate);
						_stageTahunRiskMatrixRepository.Update(stageTahunRiskMatrix);

					}

					#region warnaRm
					var butuhUpdateWarna = false;
					var checkButuhUpdateWarna = _riskMatrixProjectRepository.GetByScenarioIdProjectIdList(riskMatrixProject.ScenarioId, riskMatrixProject.ProjectId);
					if (checkButuhUpdateWarna.Count() > 1)
					{
						butuhUpdateWarna = true;
					}
					var riskMatrixProjectsSumber = _riskMatrixProjectRepository.GetByScenarioIdProjectIdAllList(riskMatrixProject.ScenarioId, riskMatrixProject.ProjectId).FirstOrDefault();
					var dataRiskMatrixProjectsSumber = _stageTahunRiskMatrixDetailRepository.GetByRiskMatrixProjectId(riskMatrixProjectsSumber.Id).ToList();                    
					#endregion warnaRm

					foreach (var item in oldStageTahunRiskMatrixDetail)
					{

						foreach (var item2 in newStageTahunRiskMatrixDetail)
						{
							if (item.StageTahunRiskMatrixId == item2.StageTahunRiskMatrixId && item.RiskRegistrasiId == item2.RiskRegistrasiId)
							{
								Validate.NotNull(item2.NilaiExpose, "SALAAAAAAAAAAAAAAH.");
								var dataAkhir = String.Format("{0:0.0000000000000}", item2.NilaiExpose);
								var dataAwal = String.Format("{0:0.0000000000000}", item.NilaiExpose); 
								if (dataAkhir != dataAwal || item.LikehoodDetailId != item2.LikehoodDetailId)
								{                                    
									//Audit Log Update
									int audit = _auditLogService.UpdateStageTahunRiskMatrixDetailAudit(item, item2);                                    

									item.Update(item.RiskRegistrasi, item2.LikehoodDetailId, item2.NilaiExpose, param.UpdateBy, param.UpdateDate);
									_stageTahunRiskMatrixDetailRepository.Update(item);
								}
								#region WarnaRm2
								if (butuhUpdateWarna)
								{
									var dataSumber = dataRiskMatrixProjectsSumber.Where(x => x.RiskRegistrasiId == item2.RiskRegistrasiId && x.StageTahunRiskMatrix.Tahun == item.StageTahunRiskMatrix.Tahun).SingleOrDefault();
									if(dataSumber == null)
									{
										var stageTahunn = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectIdYear(dataRiskMatrixProjectsSumber[0].RiskMatrixProjectId, item.StageTahunRiskMatrix.Tahun);
										StageTahunRiskMatrixDetail modelWarna = new StageTahunRiskMatrixDetail(stageTahunn, item.RiskRegistrasi, dataRiskMatrixProjectsSumber[0].RiskMatrixProjectId, item.LikehoodDetailId, item.NilaiExpose, param.UpdateBy, param.UpdateDate);
										_stageTahunRiskMatrixDetailRepository.Insert(modelWarna);
										_databaseContext.SaveChanges();
									}
									else
									{
										var dataSumber2 = String.Format("{0:0.0000000000000}", dataSumber.NilaiExpose);
										if (dataAkhir != dataSumber2)
										{
											item.WarnaExpose = true;
										}
										if (item2.LikehoodDetailId != dataSumber.LikehoodDetailId)
										{
											item.WarnaLikelihood = true;
										}
									}                                    
								}
								#endregion WarnaRm2                                
							}
						}
					}

					var Group = oldStageTahunRiskMatrixDetail.GroupBy(x => x.StageTahunRiskMatrixId);
					var GroupNew = newStageTahunRiskMatrixDetail.GroupBy(x => x.StageTahunRiskMatrixId);

					foreach (var g1 in GroupNew)
					{
						int adaBelum = 0;
						foreach (var g2 in Group)
						{
							if (g1.Key == g2.Key)
							{
								adaBelum = 1;
							}
						}
						if (adaBelum == 0)
						{
							foreach (var item in g1)
							{
								_stageTahunRiskMatrixDetailRepository.Insert(item);
								jumlahdetail++;
							}
						}
					}

					foreach (var g1 in Group)
					{
						int adaBelum = 0;
						foreach (var g2 in GroupNew)
						{
							if (g1.Key == g2.Key)
							{
								adaBelum = 1;
							}
						}
						if (adaBelum == 0)
						{
							foreach (var item in g1)
							{
								item.Delete(Convert.ToInt32(param.UpdateBy), Convert.ToDateTime(param.UpdateDate));
								_stageTahunRiskMatrixDetailRepository.Update(item);
							}
						}
					}

					_databaseContext.SaveChanges();

					#region email
					//jika submit
					if (IsSend == true && IsStatusApproval == false)
					{

						if (nomorUrutStatus != 3)
						{
							emailDari = param.UpdateBy.GetValueOrDefault();

							var master = _masterApprovalRiskMatrixProjectRepository.GetAllByProjectId(riskMatrixProject.ProjectId).Where(x => x.NomorUrutStatus == nomorUrutStatus + 1).SingleOrDefault();
							Validate.NotNull(master, "Master Approval Risk Matrix Project is not found.");


							typePesan = status.StatusDescription;

							requestId = riskMatrixProject.Id;

							if (nomorUrutStatus < 2)
							{
								emailKepada = master.UserId.GetValueOrDefault();
								master = _masterApprovalRiskMatrixProjectRepository.GetAllByProjectId(riskMatrixProject.ProjectId).Where(x => x.NomorUrutStatus == nomorUrutStatus + 2).SingleOrDefault();
								Validate.NotNull(master, "Master Approval Risk Matrix Project is not found.");

								emailCC.Add(master.UserId.GetValueOrDefault());
								//source
								 _emailService.NotifyResetPassword(source, typePesan, requestId, emailKepada, emailCC, emailDari, templateNotif);

							}
							else
							{
								emailCC.Add(master.UserId.GetValueOrDefault());
								_emailService.NotifyResetPassword(source, typePesan, requestId, emailCC, emailDari, templateNotif);
							}

						}

					}
					//jika approve
					if (IsSend == false && IsStatusApproval == true)
					{
						if (nomorUrutStatus != 3)
						{
							emailDari = param.UpdateBy.GetValueOrDefault();

							var master2 = _masterApprovalRiskMatrixProjectRepository.GetAllByProjectId(riskMatrixProject.ProjectId).Where(x => x.NomorUrutStatus == nomorUrutStatus + 1).SingleOrDefault();
							emailKepada = master2.UserId.GetValueOrDefault();

							var master = _masterApprovalRiskMatrixProjectRepository.GetAllByProjectId(riskMatrixProject.ProjectId);
							Validate.NotNull(master, "Master Approval Risk Matrix Project tidak ditemukan.");

							foreach (var item in master)
							{
								emailCC.Add(item.UserId.GetValueOrDefault());
							}
							//
							emailCC.Add(riskMatrixProject.CreateBy.GetValueOrDefault());

							Validate.NotNull(statusEmail, "Status description approve Risk Matrix Project tidak ditemukan.");
							//
							typePesan = statusEmail.StatusDescription;

							//
							requestId = riskMatrixProject.Id;

							//source
							 _emailService.NotifyResetPassword(source, typePesan, requestId, emailKepada, emailCC, emailDari, templateNotif);

						}
					}
					#endregion email                    

					var riskMatrixProjectDataBaru = _riskMatrixProjectRepository.GetByScenarioIdProjectIdList(riskMatrixProject.ScenarioId, riskMatrixProject.ProjectId).Where(x => x.StatusId == 4).ToList();
					if(riskMatrixProjectDataBaru.Count != 0)
					{
						CheckWarna(riskMatrixProject.Id);
					}
					_unitOfWork.Commit();
					ValidasiWarnaDiSemuaTahun(id);
					if (nomorUrutStatus == 3)
					{
						var deleteRiskMatrixProject = Update2(id, param);
					}
				}

			}            
			return modelId;            
		}

		public int Delete(int id, int deleteBy, DateTime deleteDate)
		{
			throw new NotImplementedException();
		}

		private void RemoveCurrentData(RiskMatrixCollectionParameter param)
		{
			var stageTahunRiskMatrix = _stageTahunRiskMatrixRepository.Get(param.RiskMatrixCollection[0].StageTahunRiskMatrixId);
			_stageTahunRiskMatrixDetailRepository.Delete(stageTahunRiskMatrix.RiskMatrixProjectId);
		}

		private void AddApproval(int idParam,  Status status, int? createBy, DateTime? createDate, int nomorUrutStatus)
		{
			var user = _userRepository.Get(createBy.GetValueOrDefault());
			Validate.NotNull(user, "User Create is not found.");

			var userRole = _userRepository.GetRoleUser(user.Id);
			Validate.NotNull(userRole, "User Role Create is not found.");

			string statusApproval = status.StatusDescription + " By " + user.UserName + " Role " + userRole.Name;
			string keterangan = "";
			string sourceApproval = "RiskMatrixProject";

			Approval model = new Approval(idParam, statusApproval,
			sourceApproval, status, user.Id, createDate, keterangan, nomorUrutStatus);

			_approvalRepository.Insert(model);

		}
		
		public int Duplicate(RiskMatrixCollectionParameter param, RiskMatrixProject riskMatrixProject)
		{          
			int id;
			int jumlahdetailnya = 0;
		   // var stageTahunRiskMatrixWithId = _stageTahunRiskMatrixRepository.Get(param.RiskMatrixCollection[0].StageTahunRiskMatrixId);

			#region Approval
			Status status = null;
			int nomorUrutStatus = 0;
			if (param.IsSend == true && param.IsUpdate == null)
			{
				status = _statusRepository.Get(4);
				Validate.NotNull(status, "Status is not found.");
			}
			if (param.IsSend == true && param.IsUpdate == true && param.IsDraftApproval == true)
			{

				status = _statusRepository.Get(4);
				Validate.NotNull(status, "Status is not found.");
			}
			if (param.IsSend == true && param.IsUpdate == true && param.IsDraftApproval == null)
			{
				status = _statusRepository.Get(4);
				var checkMasterRiskMatrixProject = _masterApprovalRiskMatrixProjectRepository.GetAllByProjectId(riskMatrixProject.ProjectId);
				var checkoutMaster = checkMasterRiskMatrixProject.Count();
				if (checkoutMaster == 0)
				{
					Validate.NotNull(null, "User must setting Approval for Risk Matrix Project first.");
				}
				var userCreate = _userRepository.Get(param.UpdateBy.GetValueOrDefault());
				Validate.NotNull(userCreate, "User is not found.");

				//get role level from user id
				var masterApproval = _masterApprovalRiskMatrixProjectRepository.GetByProjectIdUserId(riskMatrixProject.ProjectId, userCreate.Id);
				if (masterApproval != null)
				{
					nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
				}
				if (masterApproval == null && (userCreate.RoleId == 3 || userCreate.RoleId == 4 || userCreate.RoleId == 5))
				{
					masterApproval = _masterApprovalRiskMatrixProjectRepository.GetByProjectIdRoleId(riskMatrixProject.ProjectId, userCreate.RoleId);
					nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
				}

				status = _statusRepository.GetStatus(nomorUrutStatus);

				//status = _statusRepository.Get(1);
				Validate.NotNull(status, "Status is not found.");

			}
			#endregion Approval

			RiskMatrixProject model = new RiskMatrixProject(riskMatrixProject.Project, riskMatrixProject.RiskMatrix, riskMatrixProject.Scenario, param.UpdateBy, param.UpdateDate, status);
			_riskMatrixProjectRepository.Insert(model);
			_databaseContext.SaveChanges();            
			// int audit = _auditLogService.UpdateStageTahunRiskMatrixDetailAudit(item, model.Id);

			IList<StageTahunRiskMatrix> stageTahunRiskMatrixListLama = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectId(riskMatrixProject.Id).ToList();
			IList<StageTahunRiskMatrix> stageTahun = new List<StageTahunRiskMatrix>();

			foreach (var item in stageTahunRiskMatrixListLama)
			{
				StageTahunRiskMatrix modelStageTahunRiskMatrix = new StageTahunRiskMatrix(model, item.Stage, item.Tahun, param.UpdateBy, param.UpdateDate);
				_stageTahunRiskMatrixRepository.Insert(modelStageTahunRiskMatrix);
				stageTahun.Add(modelStageTahunRiskMatrix);
			}

			_databaseContext.SaveChanges();
			//var stageTahunRiskMatrixWithId = _stageTahunRiskMatrixRepository.Get(param.RiskMatrixCollection[0].StageTahunRiskMatrixId);
			var stageTahunRiskMatrixWithId = stageTahun[0];
			var oldStageTahunRiskMatrixDetail = _stageTahunRiskMatrixDetailRepository.GetByRiskMatrixProjectId(stageTahunRiskMatrixWithId.RiskMatrixProjectId);
			
			IList<StageTahunRiskMatrixDetail> newStageTahunRiskMatrixDetail = new List<StageTahunRiskMatrixDetail>();
			//insert new data
			int stage = 0;
			int tahun = 0;
		   // var currentStageTahunRiskMatrix = _stageTahunRiskMatrixRepository.Get(1);
			for (int i = 0; i < param.RiskMatrixCollection.Length; i++)
			{                
				var stageTahunRiskMatrix = stageTahun[i];
				Validate.NotNull(stageTahunRiskMatrix, "Stage Tahun Risk Matrix tidak boleh kosong.");
				//Validate.NotNull(currentStageTahunRiskMatrix, "Stage Tahun Risk Matrix tidak boleh kosong.");
				foreach (var item in param.RiskMatrixCollection[i].RiskMatrixValue)
				{
					var riskRegistrasi = _riskRegistrasiRepository.Get(item.RiskRegistrasiId);
					Validate.NotNull(riskRegistrasi, "Risk Regsitrasi tidak boleh kosong.");

					int likehoodDetailId = 0;
					Validate.NotNull(item.Values[1], "Risk registrasi {0} tidak boleh null.", riskRegistrasi.KodeMRisk);
					Validate.NotNull(item.Values[0], "Risk registrasi {0} tidak boleh null / kosong.", riskRegistrasi.KodeMRisk);

					if (item.Values[1] != 0)
					{
						var likehoodDetail = _likehoodDetailRepository.Get((int)item.Values[1]);
						Validate.NotNull(likehoodDetail, "Definisi Likelihood tidak boleh kosong.");

						likehoodDetailId = likehoodDetail.Id;
					}

					var exposure = item.Values[0];
					decimal? nilaiExposure = exposure;
					StageTahunRiskMatrixDetail modelStageTahunRiskMatrixDetailNew = new StageTahunRiskMatrixDetail(stageTahunRiskMatrix, riskRegistrasi, model.Id, likehoodDetailId, nilaiExposure, param.UpdateBy, param.UpdateDate);

					newStageTahunRiskMatrixDetail.Add(modelStageTahunRiskMatrixDetailNew);
					var checkIn = _stageTahunRiskMatrixDetailRepository.GetByRiskMatrixProjectId(model.Id);
					if (checkIn.Count() == 0 || checkIn == null)
					{
						_stageTahunRiskMatrixDetailRepository.Insert(modelStageTahunRiskMatrixDetailNew);
						jumlahdetailnya++;
					}
				}

				//update maximum expose               
				var checkMaxExpos = String.Format("{0:0.0000000000000}", param.RiskMatrixCollection[i].MaximumNilaiExpose);
				if (stageTahunRiskMatrix.MaximumNilaiExpose != null)
				{
					var parentMaxDefault = param.RiskMatrixCollection.Where(x => x.StageTahunRiskMatrixId == stageTahunRiskMatrix.Id).SingleOrDefault();
					var valueMaxDefault = parentMaxDefault.RiskMatrixValue.OrderByDescending(x => x.Values[0]).ToList();
					if (stageTahunRiskMatrix.MaximumNilaiExpose != Convert.ToDecimal(checkMaxExpos))
					{
						stageTahunRiskMatrix.IsUpdate = true;
					}
					if (parentMaxDefault.MaximumNilaiExpose == valueMaxDefault[0].Values[0])
					{
						stageTahunRiskMatrix.IsUpdate = false;
					}
				}                
				var maximumExpose = param.RiskMatrixCollection[i].MaximumNilaiExpose;
				Validate.NotNull(maximumExpose, "Nilai Maximum Exposure tidak boleh kosong. Silakan isi nilai Maximum Expose tahun {0}.", stageTahunRiskMatrix.Tahun);
				
				stageTahunRiskMatrix.UpdateMaximumExposeValue(Convert.ToDecimal(param.RiskMatrixCollection[i].MaximumNilaiExpose), param.UpdateBy, param.UpdateDate);
				_stageTahunRiskMatrixRepository.Update(stageTahunRiskMatrix);
				//_databaseContext.SaveChanges();
			}

			#region warnaRm
			_databaseContext.SaveChanges();
			var dataBaruUpdateWarna = _stageTahunRiskMatrixDetailRepository.GetByRiskMatrixProjectId(stageTahunRiskMatrixWithId.RiskMatrixProjectId);
			var checkButuhUpdateWarna = _riskMatrixProjectRepository.GetByScenarioIdProjectIdAllList(riskMatrixProject.ScenarioId, riskMatrixProject.ProjectId);    
			var riskMatrixProjectsSumber = checkButuhUpdateWarna.FirstOrDefault();            
			var dataDuplicateWarna = _stageTahunRiskMatrixDetailRepository.GetByRiskMatrixProjectId(riskMatrixProjectsSumber.Id).ToList();
			foreach (var item in dataBaruUpdateWarna)
			{
				foreach (var item2 in dataDuplicateWarna)
				{
					if(item.RiskRegistrasiId == item2.RiskRegistrasiId && item.StageTahunRiskMatrix.Tahun == item2.StageTahunRiskMatrix.Tahun)
					{
						var dataAkhir = String.Format("{0:0.0000000000000}", item.NilaiExpose);
						var dataSumber2 = String.Format("{0:0.0000000000000}", item2.NilaiExpose);
						if (dataAkhir != dataSumber2)
						{
							item.WarnaExpose = true;
						}
						if (item2.LikehoodDetailId != item.LikehoodDetailId)
						{
							item.WarnaLikelihood = true;
						}
					}                    
				}
			}
			#endregion warnaRm

			var Group = oldStageTahunRiskMatrixDetail.GroupBy(x => x.StageTahunRiskMatrixId);
			var GroupNew = newStageTahunRiskMatrixDetail.GroupBy(x => x.StageTahunRiskMatrixId);

			foreach (var g1 in GroupNew)
			{
				int adaBelum = 0;
				foreach (var g2 in Group)
				{
					if (g1.Key == g2.Key)
					{
						adaBelum = 1;
					}
				}
				if (adaBelum == 0)
				{
					foreach (var item in g1)
					{
						//_stageTahunRiskMatrixDetailRepository.Insert(item);
					}
				}
			}
			foreach (var g1 in Group)
			{
				int adaBelum = 0;
				foreach (var g2 in GroupNew)
				{
					if (g1.Key == g2.Key)
					{
						adaBelum = 1;
					}
				}
				if (adaBelum == 0)
				{
					foreach (var item in g1)
					{
						item.Delete(Convert.ToInt32(param.UpdateBy), Convert.ToDateTime(param.UpdateDate));
						_stageTahunRiskMatrixDetailRepository.Update(item);
					}
				}
			}
			id = model.Id;
			_databaseContext.SaveChanges();

			IList<StageTahunRiskMatrix> stageTahunRiskMatrixListIsUpdate = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectId(id).ToList();
			foreach (var item in stageTahunRiskMatrixListLama)
			{
				foreach(var item2 in stageTahunRiskMatrixListIsUpdate)
				{
					if (item.Tahun == item2.Tahun)
					{
						item2.IsUpdate = item.IsUpdate;
					}
				}
			}
			_databaseContext.SaveChanges();
			if(param.IsSend == false)
			{
				foreach (var item in stageTahunRiskMatrixListLama)
				{
					foreach (var item2 in stageTahunRiskMatrixListIsUpdate)
					{
						if (item.Tahun == item2.Tahun)
						{
							var exp = String.Format("{0:0.0000000000000}", item2.MaximumNilaiExpose);
							if(Convert.ToDecimal(exp) != item.MaximumNilaiExpose)
							{
								item.IsUpdate = false;
							}                            
						}
					}
				}
			}
			_databaseContext.SaveChanges();
			ValidasiWarnaDiSemuaTahun(id);
			return id;
		}

		public int Update2(int id, RiskMatrixCollectionParameter param)
		{
			var stageTahunRiskMatrixWithId = _stageTahunRiskMatrixRepository.Get(param.RiskMatrixCollection[0].StageTahunRiskMatrixId);
			var oldStageTahunRiskMatrixDetail = _stageTahunRiskMatrixDetailRepository.GetByRiskMatrixProjectId(stageTahunRiskMatrixWithId.RiskMatrixProjectId);
			
			var model = _riskMatrixProjectRepository.Get(id);
			Validate.NotNull(model, "Risk Matrix Project is not found.");

			var PerluExistingName = _riskMatrixProjectRepository.GetByProjectRiskMatrixScenarioId(model.ProjectId, model.RiskMatrixId, model.ScenarioId);
			int ValidateName = PerluExistingName.Count();

			using (_unitOfWork)
			{
				#region Approval
				bool isSend = param.IsSend.GetValueOrDefault();
				int nomorUrutStatus = 0;

				Status statusEmail = null;
				if (param.StatusId != null)
				{
					statusEmail = _statusRepository.Get(param.StatusId.GetValueOrDefault());
					param.StatusId = null;
				}

				int emailDari = param.UpdateBy.GetValueOrDefault();
				int emailKepada = model.CreateBy.GetValueOrDefault();
				List<int> emailCC = new List<int>();

				string typePesan = "";
				string source = "RiskMatrixProject";
				int requestId = 0;
				string templateNotif = param.TemplateNotif;
				param.TemplateNotif = null;
				string keterangan = param.Keterangan;
				param.Keterangan = null;
				bool isStatusApproval = param.IsStatusApproval.GetValueOrDefault();
				param.IsStatusApproval = null;

				if (param.NomorUrutStatus.GetValueOrDefault() != 0)
				{
					nomorUrutStatus = param.NomorUrutStatus.GetValueOrDefault();
					param.NomorUrutStatus = null;
				}
				#endregion Approval

				_unitOfWork.Commit();
				
				IList<StageTahunRiskMatrixDetail> newStageTahunRiskMatrixDetail = new List<StageTahunRiskMatrixDetail>();
				//insert new data
				
				for (int i = 0; i < param.RiskMatrixCollection.Length; i++)
				{
					var stageTahunRiskMatrix = _stageTahunRiskMatrixRepository.Get(param.RiskMatrixCollection[i].StageTahunRiskMatrixId);
					Validate.NotNull(stageTahunRiskMatrix, "Stage Tahun Risk Matrix tidak boleh kosong.");
					foreach (var item in param.RiskMatrixCollection[i].RiskMatrixValue)
					{
						var riskRegistrasi = _riskRegistrasiRepository.Get(item.RiskRegistrasiId);
						Validate.NotNull(riskRegistrasi, "Risk Regsitrasi tidak boleh kosong.");

						int likehoodDetailId = 0;
						Validate.NotNull(item.Values[1], "Risk registrasi {0} tidak boleh null.", riskRegistrasi.KodeMRisk);
						Validate.NotNull(item.Values[0], "Risk registrasi {0} tidak boleh null / kosong.", riskRegistrasi.KodeMRisk);

						if (item.Values[1] != 0)
						{
							var likehoodDetail = _likehoodDetailRepository.Get((int)item.Values[1]);
							Validate.NotNull(likehoodDetail, "Definisi Likelihood tidak boleh kosong.");

							likehoodDetailId = likehoodDetail.Id;
						}

						var exposure = item.Values[0];
						decimal? nilaiExposure = exposure;
						StageTahunRiskMatrixDetail model2 = new StageTahunRiskMatrixDetail(stageTahunRiskMatrix, riskRegistrasi, stageTahunRiskMatrix.RiskMatrixProject.Id, likehoodDetailId, nilaiExposure, param.UpdateBy, param.UpdateDate);

						newStageTahunRiskMatrixDetail.Add(model2);
						var checkIn = _stageTahunRiskMatrixDetailRepository.GetByRiskMatrixProjectId(stageTahunRiskMatrix.RiskMatrixProject.Id);
						if (checkIn.Count() == 0 || checkIn == null)
						{
							_stageTahunRiskMatrixDetailRepository.Insert(model2);
						}
					}
					//update maximum expose
					var maximumDataList = param.RiskMatrixCollection[i].RiskMatrixValue.ToList().OrderByDescending(x => x.Values[0]);
					var maximumData = String.Format("{0:0.0000000000000}", maximumDataList.FirstOrDefault().Values[0]);
					var checkMaxExpos = String.Format("{0:0.0000000000000}", param.RiskMatrixCollection[i].MaximumNilaiExpose);
					if (stageTahunRiskMatrix.MaximumNilaiExpose != null)
					{
						var parentMaxDefault = param.RiskMatrixCollection.Where(x => x.StageTahunRiskMatrixId == stageTahunRiskMatrix.Id).SingleOrDefault();
						var valueMaxDefault = parentMaxDefault.RiskMatrixValue.OrderByDescending(x => x.Values[0]).ToList();
						if (stageTahunRiskMatrix.MaximumNilaiExpose != Convert.ToDecimal(checkMaxExpos))
						{
							stageTahunRiskMatrix.IsUpdate = true;
						}
						if (parentMaxDefault.MaximumNilaiExpose == valueMaxDefault[0].Values[0])
						{
							stageTahunRiskMatrix.IsUpdate = false;
						}
					}
					var maximumExpose = param.RiskMatrixCollection[i].MaximumNilaiExpose;
					Validate.NotNull(maximumExpose, "Nilai Maximum Exposure tidak boleh kosong. Silakan isi nilai Maximum Expose tahun {0}.", stageTahunRiskMatrix.Tahun);
					if (Convert.ToDecimal(maximumData) != Convert.ToDecimal(checkMaxExpos))
					{                        
						int AuditMaxExposure = _auditLogService.MaxStageTahunRiskMatrixAudit(stageTahunRiskMatrix.Id, String.Format("{0:0.0000000000000}", stageTahunRiskMatrix.MaximumNilaiExpose), checkMaxExpos, Convert.ToInt32(param.UpdateBy));
					}
					stageTahunRiskMatrix.UpdateMaximumExposeValue(Convert.ToDecimal(param.RiskMatrixCollection[i].MaximumNilaiExpose), param.UpdateBy, param.UpdateDate);
					_stageTahunRiskMatrixRepository.Update(stageTahunRiskMatrix);
					_databaseContext.SaveChanges();
				}

				#region WarnaRm
				var butuhUpdateWarna = false;
				var checkButuhUpdateWarna = _riskMatrixProjectRepository.GetByScenarioIdProjectIdList(model.ScenarioId, model.ProjectId);
				if (checkButuhUpdateWarna.Count() > 1)
				{
					if (model.StatusId != 1)
					{
						butuhUpdateWarna = true;
					}
				}
				var riskMatrixProjectsSumber = _riskMatrixProjectRepository.GetByScenarioIdProjectIdAllList(model.ScenarioId, model.ProjectId).FirstOrDefault();
				var dataRiskMatrixProjectsSumber = _stageTahunRiskMatrixDetailRepository.GetByRiskMatrixProjectId(riskMatrixProjectsSumber.Id).ToList();
				#endregion WarnaRm

				foreach (var item in oldStageTahunRiskMatrixDetail)
				{
					foreach (var item2 in newStageTahunRiskMatrixDetail)
					{
						if (item.StageTahunRiskMatrixId == item2.StageTahunRiskMatrixId && item.RiskRegistrasiId == item2.RiskRegistrasiId)
						{
							Validate.NotNull(item2.NilaiExpose, "SALAAAAAAAAAAAAAAH.");
							var dataAkhir = String.Format("{0:0.0000000000000}", item2.NilaiExpose);
							var dataAwal = String.Format("{0:0.0000000000000}", item.NilaiExpose);
							if (dataAkhir != dataAwal || item.LikehoodDetailId != item2.LikehoodDetailId)
							{
								//Audit Log Update
								int audit = _auditLogService.UpdateStageTahunRiskMatrixDetailAudit(item, item2);

								item.Update(item.RiskRegistrasi, item2.LikehoodDetailId, item2.NilaiExpose, param.UpdateBy, param.UpdateDate);
								_stageTahunRiskMatrixDetailRepository.Update(item);
							}
							#region WarnaRm2
							if (butuhUpdateWarna)
							{
								var dataSumber = dataRiskMatrixProjectsSumber.Where(x => x.RiskRegistrasiId == item2.RiskRegistrasiId && x.StageTahunRiskMatrix.Tahun == item.StageTahunRiskMatrix.Tahun).Single();
								if (dataSumber == null)
								{
									var stageTahunn = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectIdYear(dataRiskMatrixProjectsSumber[0].RiskMatrixProjectId, item.StageTahunRiskMatrix.Tahun);
									StageTahunRiskMatrixDetail modelWarna = new StageTahunRiskMatrixDetail(stageTahunn, item.RiskRegistrasi, dataRiskMatrixProjectsSumber[0].RiskMatrixProjectId, item.LikehoodDetailId, item.NilaiExpose, param.UpdateBy, param.UpdateDate);
									_stageTahunRiskMatrixDetailRepository.Insert(modelWarna);
									_databaseContext.SaveChanges();
								}
								else
								{
									var dataSumber2 = String.Format("{0:0.0000000000000}", dataSumber.NilaiExpose);
									if (dataAkhir != dataSumber2)
									{
										item.WarnaExpose = true;
									}
									if (item2.LikehoodDetailId != dataSumber.LikehoodDetailId)
									{
										item.WarnaLikelihood = true;
									}
								}                                
							}
							#endregion WarnaRm2                                
						}
					}
				}                

				var Group = oldStageTahunRiskMatrixDetail.GroupBy(x => x.StageTahunRiskMatrixId);
				var GroupNew = newStageTahunRiskMatrixDetail.GroupBy(x => x.StageTahunRiskMatrixId);
				foreach (var g1 in GroupNew)
				{
					int adaBelum = 0;
					foreach (var g2 in Group)
					{
						if (g1.Key == g2.Key)
						{
							adaBelum = 1;
						}
					}
					if (adaBelum == 0)
					{
						foreach (var item in g1)
						{
							_stageTahunRiskMatrixDetailRepository.Insert(item);
						}
					}
				}

				foreach (var g1 in Group)
				{
					int adaBelum = 0;
					foreach (var g2 in GroupNew)
					{
						if (g1.Key == g2.Key)
						{
							adaBelum = 1;
						}
					}
					if (adaBelum == 0)
					{
						foreach (var item in g1)
						{
							item.Delete(Convert.ToInt32(param.UpdateBy), Convert.ToDateTime(param.UpdateDate));
							_stageTahunRiskMatrixDetailRepository.Update(item);
						}
					}
				}

				var riskMatrixProjectSame = _riskMatrixProjectRepository.GetByProjectRiskMatrixScenarioId2(model.ProjectId, model.RiskMatrixId, model.ScenarioId, model.CreateBy.GetValueOrDefault());
				var otherRiskMatrixId = 0;
				bool IsFirst = true;
				foreach (var item in riskMatrixProjectSame)
				{
					if (item.StatusId == 2 && item.Id != model.Id)
					{
						IsFirst = false;
					}
				}

				if (IsFirst)
				{
					foreach (var item in riskMatrixProjectSame)
					{
						if (item.StatusId == 1)
						{
							Delete2(item.Id, Convert.ToInt32(param.UpdateBy), Convert.ToDateTime(param.UpdateDate));
						}
					}
				}
				else
				{
					foreach (var item in riskMatrixProjectSame)
					{
						if (item.StatusId == 1)
						{
							//item.SetNullStatus(Convert.ToInt32(param.CreateBy), Convert.ToDateTime(param.CreateDate));
							Delete2(item.Id, Convert.ToInt32(param.UpdateBy), Convert.ToDateTime(param.UpdateDate));
						}
						if (item.StatusId == 2 && item.Id != model.Id)
						{
							Delete2(item.Id, Convert.ToInt32(param.UpdateBy), Convert.ToDateTime(param.UpdateDate));
						}
					}
				}
				_databaseContext.SaveChanges();
				var correlatedProjectSame = _riskMatrixProjectRepository.GetByProjectRiskMatrixScenarioId(model.ProjectId, model.RiskMatrixId, model.ScenarioId);

				foreach (var item in correlatedProjectSame)
				{
					if (item.StatusId == 2 && item.Id != model.Id)
					{
						Delete2(item.Id, Convert.ToInt32(param.UpdateBy), Convert.ToDateTime(param.UpdateDate));
					}
					if (item.StatusId == 1 && item.Id != model.Id)
					{
						Delete2(item.Id, Convert.ToInt32(param.UpdateBy), Convert.ToDateTime(param.UpdateDate));
					}
				}
				_databaseContext.SaveChanges();
				#region email
				//untuk notifikasi email approve
				if (isSend == false && isStatusApproval == true)
				{
					if (nomorUrutStatus == 3)
					{

						var master = _masterApprovalRiskMatrixProjectRepository.GetAllByProjectId(model.ProjectId);
						Validate.NotNull(master, "Master Approval Risk Matrix Project tidak ditemukan.");

						foreach (var item in master)
						{
							emailCC.Add(item.UserId.GetValueOrDefault());
						}
						if (!emailCC.Contains(emailKepada))
						{
							emailCC.Add(emailKepada);
						}
						//
						typePesan = statusEmail.StatusDescription; ;

						//
						requestId = model.Id;


						_emailService.NotifyResetPassword(source, typePesan, requestId, emailCC, emailDari, templateNotif);
					}
				}
				#endregion email
				_unitOfWork.Commit();
				id = model.Id;
				ValidasiWarnaDiSemuaTahun(id);
			}
			return id;
		}

		public int Delete2(int id, int deleteBy, DateTime deleteDate)
			{
				RiskMatrixProject modelRiskMatrix = _riskMatrixProjectRepository.Get(id);
				 Validate.NotNull(modelRiskMatrix, "Risk Matrix Project is not found.");

				 modelRiskMatrix.Delete(deleteBy, deleteDate);
				_riskMatrixProjectRepository.Update(modelRiskMatrix);

				_databaseContext.SaveChanges();
				//Audit Log Delete 
				int audit = _auditLogService.DeleteRiskMatrixProjectAudit(id, deleteBy);
			 return id;
			}

		public int UpdateMurni(int id, RiskMatrixCollectionParameter param)
		{
			int modelId = 0;
			var stageTahunRiskMatrixWithId = _stageTahunRiskMatrixRepository.Get(param.RiskMatrixCollection[0].StageTahunRiskMatrixId);
			var oldStageTahunRiskMatrixDetail = _stageTahunRiskMatrixDetailRepository.GetByRiskMatrixProjectId(stageTahunRiskMatrixWithId.RiskMatrixProjectId);

			var riskMatrixProject = _riskMatrixProjectRepository.Get(stageTahunRiskMatrixWithId.RiskMatrixProjectId);

			IList<StageTahunRiskMatrixDetail> newStageTahunRiskMatrixDetail = new List<StageTahunRiskMatrixDetail>();
			//insert new data
			for (int i = 0; i < param.RiskMatrixCollection.Length; i++)
			{
				var stageTahunRiskMatrix = _stageTahunRiskMatrixRepository.Get(param.RiskMatrixCollection[i].StageTahunRiskMatrixId);
				Validate.NotNull(stageTahunRiskMatrix, "Stage Tahun Risk Matrix tidak boleh kosong.");
				foreach (var item in param.RiskMatrixCollection[i].RiskMatrixValue)
				{
					var riskRegistrasi = _riskRegistrasiRepository.Get(item.RiskRegistrasiId);
					Validate.NotNull(riskRegistrasi, "Risk Regsitrasi tidak boleh kosong.");

					int likehoodDetailId = 0;
					Validate.NotNull(item.Values[1], "Risk registrasi {0} tidak boleh null.", riskRegistrasi.KodeMRisk);
					Validate.NotNull(item.Values[0], "Risk registrasi {0} tidak boleh null / kosong.", riskRegistrasi.KodeMRisk);

					if (item.Values[1] != 0)
					{
						var likehoodDetail = _likehoodDetailRepository.Get((int)item.Values[1]);
						Validate.NotNull(likehoodDetail, "Definisi Likelihood tidak boleh kosong.");

						likehoodDetailId = likehoodDetail.Id;
					}

					var exposure = item.Values[0];
					decimal? nilaiExposure = exposure;
					StageTahunRiskMatrixDetail model = new StageTahunRiskMatrixDetail(stageTahunRiskMatrix, riskRegistrasi, stageTahunRiskMatrix.RiskMatrixProject.Id, likehoodDetailId, nilaiExposure, param.UpdateBy, param.UpdateDate);

					newStageTahunRiskMatrixDetail.Add(model);
					var checkIn = _stageTahunRiskMatrixDetailRepository.GetByRiskMatrixProjectId(stageTahunRiskMatrix.RiskMatrixProject.Id);
					if (checkIn.Count() == 0 || checkIn == null)
					{
						_stageTahunRiskMatrixDetailRepository.Insert(model);
					}
				}

				//update maximum expose
				var maximumDataList = param.RiskMatrixCollection[i].RiskMatrixValue.ToList().OrderByDescending(x => x.Values[0]);
				var maximumData = String.Format("{0:0.0000000000000}", maximumDataList.FirstOrDefault().Values[0]);
				var checkMaxExpos = String.Format("{0:0.0000000000000}", param.RiskMatrixCollection[i].MaximumNilaiExpose);
				if (Convert.ToDecimal(maximumData) != Convert.ToDecimal(checkMaxExpos))
				{
					int AuditMaxExposure = _auditLogService.MaxStageTahunRiskMatrixAudit(stageTahunRiskMatrix.Id, String.Format("{0:0.0000000000000}", stageTahunRiskMatrix.MaximumNilaiExpose), checkMaxExpos, Convert.ToInt32(param.UpdateBy));
				}
				var maximumExpose = param.RiskMatrixCollection[i].MaximumNilaiExpose;
				Validate.NotNull(maximumExpose, "Nilai Maximum Exposure tidak boleh kosong. Silakan isi nilai Maximum Expose tahun {0}.", stageTahunRiskMatrix.Tahun);
				stageTahunRiskMatrix.UpdateMaximumExposeValue(Convert.ToDecimal(param.RiskMatrixCollection[i].MaximumNilaiExpose), param.UpdateBy, param.UpdateDate);
				_stageTahunRiskMatrixRepository.Update(stageTahunRiskMatrix);                
			}

			#region WarnaRm
			var butuhUpdateWarna = false;
			var checkButuhUpdateWarna = _riskMatrixProjectRepository.GetByScenarioIdProjectIdList(riskMatrixProject.ScenarioId, riskMatrixProject.ProjectId);
			if (checkButuhUpdateWarna.Count() > 1)
			{
				if (riskMatrixProject.StatusId != 1)
				{
					butuhUpdateWarna = true;
				}
			}
			var riskMatrixProjectsSumber = _riskMatrixProjectRepository.GetByScenarioIdProjectIdAllList(riskMatrixProject.ScenarioId, riskMatrixProject.ProjectId).FirstOrDefault();
			var dataRiskMatrixProjectsSumber = _stageTahunRiskMatrixDetailRepository.GetByRiskMatrixProjectId(riskMatrixProjectsSumber.Id);
			#endregion WarnaRm

			foreach (var item in oldStageTahunRiskMatrixDetail)
			{
				foreach (var item2 in newStageTahunRiskMatrixDetail)
				{
					if (item.StageTahunRiskMatrixId == item2.StageTahunRiskMatrixId && item.RiskRegistrasiId == item2.RiskRegistrasiId)
					{
						Validate.NotNull(item2.NilaiExpose, "SALAAAAAAAAAAAAAAH.");
						var dataAkhir = String.Format("{0:0.0000000000000}", item2.NilaiExpose);
						var dataAwal = String.Format("{0:0.0000000000000}", item.NilaiExpose); ;
						if (dataAkhir != dataAwal || item.LikehoodDetailId != item2.LikehoodDetailId)
						{
							//Audit Log Update
							int audit = _auditLogService.UpdateStageTahunRiskMatrixDetailAudit(item, item2);

							item.Update(item.RiskRegistrasi, item2.LikehoodDetailId, item2.NilaiExpose, param.UpdateBy, param.UpdateDate);
							_stageTahunRiskMatrixDetailRepository.Update(item);
						}
						#region WarnaRm2
						if (butuhUpdateWarna)
						{
							var dataSumber = dataRiskMatrixProjectsSumber.Where(x => x.RiskRegistrasiId == item2.RiskRegistrasiId && x.StageTahunRiskMatrix.Tahun == item.StageTahunRiskMatrix.Tahun).Single();
							var dataSumber2 = String.Format("{0:0.0000000000000}", dataSumber.NilaiExpose);
							if (dataAkhir != dataSumber2)
							{
								item.WarnaExpose = true;
							}
							if (item2.LikehoodDetailId != dataSumber.LikehoodDetailId)
							{
								item.WarnaLikelihood = true;
							}
						}
						#endregion WarnaRm2                                
					}
				}
			}

			var Group = oldStageTahunRiskMatrixDetail.GroupBy(x => x.StageTahunRiskMatrixId);
			var GroupNew = newStageTahunRiskMatrixDetail.GroupBy(x => x.StageTahunRiskMatrixId);

			foreach (var g1 in GroupNew)
			{
				int adaBelum = 0;
				foreach (var g2 in Group)
				{
					if (g1.Key == g2.Key)
					{
						adaBelum = 1;
					}
				}
				if (adaBelum == 0)
				{
					foreach (var item in g1)
					{
						_stageTahunRiskMatrixDetailRepository.Insert(item);
					}
				}
			}

			foreach (var g1 in Group)
			{
				int adaBelum = 0;
				foreach (var g2 in GroupNew)
				{
					if (g1.Key == g2.Key)
					{
						adaBelum = 1;
					}
				}
				if (adaBelum == 0)
				{
					foreach (var item in g1)
					{
						item.Delete(Convert.ToInt32(param.UpdateBy), Convert.ToDateTime(param.UpdateDate));
						_stageTahunRiskMatrixDetailRepository.Update(item);
					}
				}
			}

			_databaseContext.SaveChanges();
			ValidasiWarnaDiSemuaTahun(id);


			return modelId;
		}

		private void ValidasiWarnaDiSemuaTahun(int id)
		{
			var StageTahunRiskMatrix2018 = _databaseContext.StageTahunRiskMatrixs.Where(x => x.RiskMatrixProjectId == id).ToList();
			foreach(var item in StageTahunRiskMatrix2018.ToList())
			{
				var maxExposeDetail = _databaseContext.StageTahunRiskMatrixDetails.Where(x => x.StageTahunRiskMatrixId == item.Id).OrderByDescending(x=>x.NilaiExpose).ToList();
				if(item.MaximumNilaiExpose == maxExposeDetail[0].NilaiExpose)
				{
					item.IsUpdate = false;
				}
				else
				{
					item.IsUpdate = true;
				}
			}
			_databaseContext.SaveChanges();
		}

		private void CheckWarna(int id)
		{
			#region warnaRm
			var riskMatrixProject = _riskMatrixProjectRepository.Get(id);
			var riskMatrixProjectDataBaru = _riskMatrixProjectRepository.GetByScenarioIdProjectIdList(riskMatrixProject.ScenarioId, riskMatrixProject.ProjectId).Where(x => x.StatusId == 4).FirstOrDefault();
			var dataBaruUpdateWarna = _stageTahunRiskMatrixDetailRepository.GetByRiskMatrixProjectId(riskMatrixProjectDataBaru.Id);
			var checkButuhUpdateWarna = _riskMatrixProjectRepository.GetByScenarioIdProjectIdAllList(riskMatrixProject.ScenarioId, riskMatrixProject.ProjectId);
			var riskMatrixProjectsSumber = checkButuhUpdateWarna.FirstOrDefault();
			var dataDuplicateWarna = _stageTahunRiskMatrixDetailRepository.GetByRiskMatrixProjectId(riskMatrixProjectsSumber.Id).ToList();
			foreach (var item in dataBaruUpdateWarna)
			{
				foreach (var item2 in dataDuplicateWarna)
				{
					if (item.RiskRegistrasiId == item2.RiskRegistrasiId && item.StageTahunRiskMatrix.Tahun == item2.StageTahunRiskMatrix.Tahun)
					{
						var dataAkhir = String.Format("{0:0.0000000000000}", item.NilaiExpose);
						var dataSumber2 = String.Format("{0:0.0000000000000}", item2.NilaiExpose);
						if (dataAkhir != dataSumber2)
						{
							item.WarnaExpose = true;
						}
						if (item2.LikehoodDetailId != item.LikehoodDetailId)
						{
							item.WarnaLikelihood = true;
						}
					}
				}
			}
			#endregion warnaRm
		}
	}
}
