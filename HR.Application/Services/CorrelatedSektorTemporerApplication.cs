﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;
using HR.Infrastructure;
using HR.Common;

namespace HR.Application
{
    public class CorrelatedSektorTemporerApplication : ICorrelatedSektorTemporerService
    {
        private readonly ICorrelatedSektorTemporerRepository _correlatedSektorTemporerRepository;
        private readonly IDatabaseContext _databaseContext;
        private readonly IRiskMatrixProjectRepository _riskMatriksProjectRepository;

        public CorrelatedSektorTemporerApplication(ICorrelatedSektorTemporerRepository correlatedSektorTemporerRepository, IDatabaseContext databaseContext, IRiskMatrixProjectRepository riskMatriksProjectRepository)
        {
            _correlatedSektorTemporerRepository = correlatedSektorTemporerRepository;
            _databaseContext = databaseContext;
            _riskMatriksProjectRepository = riskMatriksProjectRepository;
        }

        #region Query
        public IEnumerable<CorrelatedSektorTemporer> GetAll()
        {
            return _correlatedSektorTemporerRepository.GetAll();
        }

        public CorrelatedSektorTemporer Get(int id)
        {
            return _correlatedSektorTemporerRepository.Get(id);
        }        
        #endregion Query

        #region Manipulation 
        public int Add(CorrelatedSektorTemporer param)
        {
            int id;

            //var riskMatrixProject = _riskMatriksProjectRepository.Get(param.DataObjekId.GetValueOrDefault());
            //Validate.NotNull(riskMatrixProject, "Data Risk Matrix Project tidak ditemukan");

            CorrelatedSektorTemporer model = new CorrelatedSektorTemporer(param.MenuId, param.TableModified, param.DataObjekId, param.ColumnModified, param.DataAwal, param.DataAkhir, param.LogTimestamp, param.ModifiedBy);
            _correlatedSektorTemporerRepository.Insert(model);
            _databaseContext.SaveChanges();
            id = model.Id;           
            return id;
        }

        public void Delete(int id, DateTime deleteDate, int deleteBy)
        {

            //var  = _riskMatriksProjectRepository.Get(param.DataObjekId.GetValueOrDefault());
            //Validate.NotNull(riskMatrixProject, "Data Risk Matrix Project tidak ditemukan");

            //RiskMatriksTemporer model = new RiskMatriksTemporer(param.MenuId, param.TableModified, riskMatrixProject.Id, param.ColumnModified, param.DataAwal, param.DataAkhir, param.LogTimestamp, param.ModifiedBy);
            //_riskMatriksTemporerRepository.Insert(model);
            //_databaseContext.SaveChanges();
            //id = model.Id;
            //return id;
        }
        #endregion Manipulation

        //#region RiskMatrixProject
        //public int SubmitRiskMatrixProjectAudit(int id, int updateBy)
        //{
        //    var auditParam = new RiskMatriksTemporerParam
        //    {
        //        MenuId = Convert.ToInt32(EventLogType.RiskMatriks),
        //        TableModified = "tblRiskMatrixProjects",
        //        DataObjekId = id,
        //        ColumnModified = "StatusId",
        //        DataAwal = "",
        //        DataAkhir = "1",
        //        LogTimestamp = DateHelper.GetDateTime(),
        //        ModifiedBy = updateBy
        //    };
        //    int audit = Add(auditParam);

        //    return id;
        //}

        //public int SubmitRiskMatrixProjectAudit(RiskMatrixCollectionParameter param, int id)
        //{
        //    foreach (var col in param.GetType().GetProperties())
        //    {
        //        var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
        //        if (paramBaru != null && col.Name != "CreateBy" && col.Name != "CreateDate" && col.Name != "IsUpdate" && col.Name != "IsDelete" && col.Name != "RiskMatrixProject" && col.Name != "Stage")
        //        {
        //            var auditParam = new RiskMatriksTemporerParam
        //            {
        //                MenuId = Convert.ToInt32(EventLogType.RiskMatriks),
        //                TableModified = "tblStageTahunRiskMatrix",
        //                DataObjekId = id,
        //                ColumnModified = col.Name,
        //                DataAwal = "",
        //                DataAkhir = paramBaru.ToString(),
        //                LogTimestamp = DateHelper.GetDateTime(),
        //                ModifiedBy = param.CreateBy
        //            };
        //            int audit = Add(auditParam);
        //        }
        //    }
        //    return id;
        //}

        //public int ApproveRiskMatrixProjectAudit(ApprovalParam param,int otherRiskMatrixProjectId, int id)
        //{
        //    //param.IsUpdateStatus = null;
        //    //param.IsUpdate = null;
        //    //param.TypePesan = null;
        //    //param.TemplateNotif = null;
        //    //param.IsSend = null;
        //    //param.Id = null;
        //    //param.RiskMatrixProjectId = null;
        //    //param.Tahun = null;
        //    //param.StartProject = null;
        //    //param.EndProject = null;
        //    //param.StageValue = null;
        //    //param.RiskMatrixCollection = null;
        //    //param.LikehoodId = null;
        //    //param.NamaScenario = null;
        //    //param.ProjectId = null;
        //    //param.IsDefault = null;
        //    //param.CorrelatedProjectId = null;
        //    //param.ProjectIdCP = null;
        //    //param.SektorId = null;
        //    //param.NamaSektor = null;
        //    //param.CorrelatedProjectDetailCollection = null;
        //    //param.CorrelatedSektorId = null;
        //    //param.CorrelatedSektorDetailCollection = null;
        //    foreach (var col in param.GetType().GetProperties())
        //    {
        //        var paramBaru = param.GetType().GetProperty(col.Name).GetValue(param, null);
        //        if (paramBaru != null && col.Name != "RequestId" && col.Name != "UpdateDate" && col.Name != "UpdateBy" && col.Name != "IsDelete" && col.Name != "DeleteDate")
        //        {
        //            var auditParam = new RiskMatriksTemporerParam
        //            {
        //                MenuId = Convert.ToInt32(EventLogType.RiskMatriks),
        //                TableModified = "tblApproval",
        //                DataObjekId = id,
        //                ColumnModified = col.Name,
        //                DataAwal = "",
        //                DataAkhir = paramBaru.ToString(),
        //                LogTimestamp = DateHelper.GetDateTime(),
        //                ModifiedBy = param.UpdateBy
        //            };
        //            int audit = Add(auditParam);
        //        }
        //        else if (paramBaru != null && col.Name == "RequestId")
        //        {
        //            var auditParam = new RiskMatriksTemporerParam
        //            {
        //                MenuId = Convert.ToInt32(EventLogType.RiskMatriks),
        //                TableModified = "tblApproval",
        //                DataObjekId = id,
        //                ColumnModified = col.Name,
        //                DataAwal = "",
        //                DataAkhir = otherRiskMatrixProjectId.ToString(),
        //                LogTimestamp = DateHelper.GetDateTime(),
        //                ModifiedBy = param.UpdateBy
        //            };
        //            int audit = Add(auditParam);
        //        }
        //    }
        //    return id;
        //}

        //public int RejectRiskMatrixProjectAudit (int id, int updateBy)
        //{
        //    var auditParam = new RiskMatriksTemporerParam
        //    {
        //        MenuId = Convert.ToInt32(EventLogType.RiskMatriks),
        //        TableModified = "tblRiskMatrixProjects",
        //        DataObjekId = id,
        //        ColumnModified = "StatusId",
        //        DataAwal = "1",
        //        DataAkhir = "3",
        //        LogTimestamp = DateHelper.GetDateTime(),
        //        ModifiedBy = updateBy
        //    };
        //    int audit = Add(auditParam);
        //    return id;
        //}

        //public int SetApproveRiskMatrixProjectAudit(int id, int updateBy)
        //{
        //    var auditParam = new RiskMatriksTemporerParam
        //    {
        //        MenuId = Convert.ToInt32(EventLogType.RiskMatriks),
        //        TableModified = "tblRiskMatrixProjects",
        //        DataObjekId = id,
        //        ColumnModified = "StatusId",
        //        DataAwal = "1",
        //        DataAkhir = "2",
        //        LogTimestamp = DateHelper.GetDateTime(),
        //        ModifiedBy = updateBy
        //    };
        //    int audit = Add(auditParam);
        //    return id;
        //}
        //#endregion RiskMatrixProject
    }
}
