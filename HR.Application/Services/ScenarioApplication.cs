﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using HR.Application.Params;
using HR.Core;
using HR.Domain;
using System.Linq;
using HR.Infrastructure;
using HR.Application;
using System.Data.SqlClient;
using System.Data;

namespace HR.Application
{
    public class ScenarioService : IScenarioService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IScenarioRepository _scenarioRepository;
        private readonly IScenarioDetailRepository _scenarioDetailRepository;
        private readonly ILikehoodRepository _likehoodRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly IRiskMatrixRepository _riskMatrixRepository;
        private readonly ISektorRepository _sektorRepository;
        private readonly ICorrelationRiskAntarSektorRepository _correlationRiskAntarSektorRepository;
        private readonly ICorrelationRiskAntarProjectRepository _correlationRiskAntarProjectRepository;
        private readonly ICorrelatedSektorRepository _correlatedSectorRepository;
        private readonly ICorrelatedSektorDetailRepository _correlatedSektorDetailRepository;
        private readonly IRiskMatrixProjectRepository _riskMatrixProjectRepository;
        private readonly ICorrelatedProjectRepository _correlatedProjectRepository;
        private readonly ICorrelatedProjectDetailRepository _correlatedProjectDetailRepository;
        private readonly IUserRepository _userRepository;
        private readonly IAuditLogService _auditLogService;
        private readonly IStatusRepository _statusRepository;
        private readonly IApprovalRepository _approvalRepository;
        private readonly IDatabaseContext _databaseContext;
        private readonly IFunctionalRiskRepository _functionalRiskRepository;
        private readonly IMatrixRepository _matrixRepository;
        private readonly IColorCommentRepository _colorCommentRepository;
        private readonly IMasterApprovalScenarioRepository _masterApprovalScenarioRepository;
        private readonly IEmailService _emailService;
        private readonly IUserService _userService;
        private readonly IMasterApprovalScenarioService _masterApprovalScenarioService;

        private readonly IStageTahunRiskMatrixRepository _stageTahunRiskMatrixRepository;
        private readonly IStageTahunRiskMatrixDetailRepository _stageTahunRiskMatrixDetailRepository;
        private readonly IScenarioCalculationRepository _scenarioCalculationRepository;
        private readonly IProjectCalculationRepository _projectCalculationRepository;

        public ScenarioService(IUnitOfWork unitOfWork, IDatabaseContext databaseContext, IScenarioRepository scenarioRepository, IScenarioDetailRepository scenarioDetailRepository,
            ILikehoodRepository likehoodRepository, IProjectRepository projectRepository, IRiskMatrixRepository riskMatrixRepository,
            ISektorRepository sektorRepository, ICorrelationRiskAntarSektorRepository correlationRiskAntarSektorRepository,
            ICorrelationRiskAntarProjectRepository correlationRiskAntarProjectRepository, ICorrelatedSektorRepository correlatedSectorRepository,
            IRiskMatrixProjectRepository riskMatrixProjectRepository, ICorrelatedProjectRepository correlatedProjectRepository, IUserRepository userRepository,
            IAuditLogService auditLogService, IFunctionalRiskRepository functionalRiskRepository, IMatrixRepository matrixRepository, IColorCommentRepository colorCommentRepository
            , IStatusRepository statusRepository, IApprovalRepository approvalRepository, IMasterApprovalScenarioRepository masterApprovalScenarioRepository,

            IStageTahunRiskMatrixRepository stageTahunRiskMatrixRepository, IEmailService emailService, IUserService userService, IMasterApprovalScenarioService masterApprovalScenarioService,
            ICorrelatedSektorDetailRepository correlatedSektorDetailRepository, ICorrelatedProjectDetailRepository correlatedProjectDetailRepository, IStageTahunRiskMatrixDetailRepository stageTahunRiskMatrixDetailRepository,
            IScenarioCalculationRepository scenarioCalculationRepository, IProjectCalculationRepository projectCalculationRepository)
        {
            _unitOfWork = unitOfWork;
            _scenarioRepository = scenarioRepository;
            _scenarioDetailRepository = scenarioDetailRepository;
            _likehoodRepository = likehoodRepository;
            _projectRepository = projectRepository;
            _riskMatrixRepository = riskMatrixRepository;
            _sektorRepository = sektorRepository;
            _correlationRiskAntarSektorRepository = correlationRiskAntarSektorRepository;
            _correlationRiskAntarProjectRepository = correlationRiskAntarProjectRepository;
            _correlatedSectorRepository = correlatedSectorRepository;
            _riskMatrixProjectRepository = riskMatrixProjectRepository;
            _correlatedProjectRepository = correlatedProjectRepository;
            _userRepository = userRepository;
            _auditLogService = auditLogService;
            _statusRepository = statusRepository;
            _approvalRepository = approvalRepository;
            _databaseContext = databaseContext;
            _functionalRiskRepository = functionalRiskRepository;
            _matrixRepository = matrixRepository;
            _colorCommentRepository = colorCommentRepository;
            _masterApprovalScenarioRepository = masterApprovalScenarioRepository;
            _correlatedProjectDetailRepository = correlatedProjectDetailRepository;
            _correlatedSektorDetailRepository = correlatedSektorDetailRepository;
            _stageTahunRiskMatrixRepository = stageTahunRiskMatrixRepository;
            _stageTahunRiskMatrixDetailRepository = stageTahunRiskMatrixDetailRepository;
            _emailService = emailService;
            _userService = userService;
            _masterApprovalScenarioService = masterApprovalScenarioService;
            _scenarioCalculationRepository = scenarioCalculationRepository;
            _projectCalculationRepository = projectCalculationRepository;
        }

        #region Query
        public IEnumerable<Scenario> GetAll()
        {
            return _scenarioRepository.GetAll();
        }
        public IEnumerable<Scenario> GetAll(string keyword, int userId)
        {
            return _scenarioRepository.GetAll(keyword, userId);
        }

        public IEnumerable<Scenario> GetAll(string keyword,int id, int userId)
        {
            return _scenarioRepository.GetAll(keyword,id, userId);
        }
        public IEnumerable<Scenario> GetAll(string keyword, int id, int id2, int userId)
        {
            return _scenarioRepository.GetAll(keyword, id, id2, userId);
        }

        public Scenario Get(int id)
        {
            return _scenarioRepository.Get(id);
        }

        public Scenario GetWithProject(int id)
        {
            var obj = _scenarioRepository.Get(id);
            foreach (var item in obj.ScenarioDetail.ToList())
            {
                if (item.IsDelete == true)
                {
                    obj.ScenarioDetail.Remove(item);
                }
            }

            return obj;
        }

        public Scenario GetDefault()
        {
            return _scenarioRepository.GetDefault();
        }

        public int SetDefault(int id, int? updateBy, DateTime? updateDate)
        {
            int modelId;
            var modelNewDefault = this.Get(id);

            Validate.NotNull(modelNewDefault, "Scenario is not found.");
            var modelOldDefault = this.GetDefault();

            #region Approval
            if (modelNewDefault.StatusId != 2)
            {
                throw new ApplicationException(string.Format("Scenario belum di Approve"));
            }

            #endregion Approval

            using (_unitOfWork)
            {

                if (modelOldDefault != null)
                {
                    //Remove current default
                    modelOldDefault.RemoveDefault(updateBy, updateDate);
                    _scenarioRepository.Update(modelOldDefault);
                    //Audit Log Default
                    int audit = _auditLogService.ScenarioRemoveDefault(modelOldDefault, updateBy);
                }

                //set new default
                modelNewDefault.SetDefault(updateBy, updateDate);
                //Audit Log Default
                int audit2 = _auditLogService.ScenarioSetDefault(modelNewDefault);

                //insert into risk matrix project
                var riskMatrix = _riskMatrixRepository.GetByScenarioId(id);
                GenerateRiskMatrixProject(modelNewDefault, riskMatrix, updateBy, updateDate);

                //Update table CorrelatedProject
                UpdateCorrelatedProject(modelNewDefault, updateBy, updateDate);


                //set status risk matrix
                SetStatusRiskMatrix(modelNewDefault, modelOldDefault, updateBy, updateDate);
                //riskMatrix.Delete(updateBy, updateDate);

                //set scenarioId in CorrelatedSektor
                // TEMPORARY COMMENT BY KIKIW
                SetScenarioInFunctionalRisk(modelNewDefault, modelOldDefault, updateBy, updateDate);

                _unitOfWork.Commit();

                //get list correlated project then add to approval
                //GenerateCorrelatedProjectApproval(modelNewDefault, role, status);
                //_unitOfWork.Commit();
            }



            modelId = modelNewDefault.Id;

            return modelId;
        }

        public void RemoveDefault(int updateBy, DateTime updateDate)
        {
            throw new NotImplementedException();
        }

        public void IsExistOnEditing(int id, string namaScenario)
        {
            if (_scenarioRepository.IsExist(id, namaScenario))
            {
                throw new ApplicationException(string.Format("Nama Scenario {0} already exist.", namaScenario));
            }
        }

        public void isExistOnAdding(string namaScenario)
        {
            if (_scenarioRepository.IsExist(namaScenario))
            {
                throw new ApplicationException(string.Format("Nama Scenario {0} already exist.", namaScenario));
            }
        }
        #endregion Query

        //#region Manipulation
        public int Add(ScenarioParam param)
        {

            //get user from createBy
            var userCreate = _userRepository.Get(param.CreateBy.GetValueOrDefault());
            Validate.NotNull(userCreate, "User Create tidak dapat ditemukan.");

            var userCreateRole = _userRepository.GetRoleUser(userCreate.Id);
            //check role user
            if (userCreate.RoleId != 3 && userCreate.RoleId != 5)
            {
                throw new ApplicationException(string.Format("User dengan role {0} tidak bisa membuat Scenario", userCreateRole.Description));
            }
            int id;
            Validate.NotNull(param.NamaScenario, "Nama Scenario is required.");

            var likelihood = _likehoodRepository.Get(param.LikehoodId);
            Validate.NotNull(likelihood, "Likelihood is not found.");

            #region Approval
            Status status = null;
            //get status from role id
            //Role role = null;
            int nomorUrutStatus = 0;

            //emailDari tipenya int
            int emailDari = 0;
            //emailKepada tipe int
            int emailKepada = 0;

            //dalam bentuk array cc
            List<int> emailCC = new List<int>();
            //emailCC[0] = 0;
            //emailCC[1] = 0;
            //emailCC[2] = 0;

            //tipe pesan ada 2 string Skenario dan Submit
            string source = "Scenario";
            string typePesan = "";
            string templateNotif = param.TemplateNotif;
            param.TemplateNotif = null;
            //requestId tipe int IDScenario
            int requestId = 0;

            //jika user menekan tombol kirim, maka akan kirim ke tabel Approval
            if (param.IsSend == true)
            {
                var checkMasterScenario = _masterApprovalScenarioRepository.GetAll();
                var checkoutMaster = checkMasterScenario.Count();
                if (checkoutMaster == 0)
                {
                    Validate.NotNull(null, "User must setting Approval for scenario first.");
                }

                //get role level from user id
                var masterApproval = _masterApprovalScenarioRepository.GetByUserId(userCreate.Id);
                if (masterApproval != null)
                {
                    nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
                }
                if (masterApproval == null && (userCreate.RoleId == 3 || userCreate.RoleId == 4 || userCreate.RoleId == 5))
                {
                    masterApproval = _masterApprovalScenarioRepository.GetByRoleId(userCreate.RoleId);
                    nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
                }
                status = _statusRepository.GetStatus(nomorUrutStatus);
                Validate.NotNull(status, "Status is not found.");
            }
            #endregion Approval

            isExistOnAdding(param.NamaScenario);
            using (_unitOfWork)
            {
                //Scenario model = new Scenario(likelihood, param.NamaScenario, param.CreateBy, param.CreateDate);
                Scenario model = new Scenario(likelihood, param.NamaScenario, param.CreateBy, param.CreateDate, status);
                _scenarioRepository.Insert(model);

                //insert scenario detail

                //IList<ScenarioDetail> scenarioDetails = GenerateScenarioDetail(param, model);
                //_scenarioDetailRepository.Insert(scenarioDetails);
                var projectAll = _projectRepository.GetAll().ToList();
                IList<ScenarioDetail> scenarioDetailAll = GenerateScenarioDetailAll(projectAll, param, model);
                _scenarioDetailRepository.Insert(scenarioDetailAll);
                _unitOfWork.Commit();

                //insert into correlation risk antar sector
                var correlationRiskAntarSektor = GenereateCorrelationRiskAntarSektor(model, scenarioDetailAll, param.CreateBy, param.CreateDate);

                //insert into risk matrix
                //RiskMatrix modelRiskMatrix = new RiskMatrix(model, param.CreateBy, param.CreateDate);

                RiskMatrix modelRiskMatrix = new RiskMatrix(model, param.CreateBy, param.CreateDate);
                _riskMatrixRepository.Insert(modelRiskMatrix);
                _unitOfWork.Commit();

                var riskMatrix = _riskMatrixRepository.GetByScenarioId(model.Id);
                Validate.NotNull(riskMatrix, "Risk Matrix is not found.");

                //setRiskMatrixProject
                SetRiskMatrixProject(model, riskMatrix, param.CreateBy, param.CreateDate);
                _unitOfWork.Commit();

                //generate correlatedProject
                GenerateCorrelatedProject(model, param.CreateBy, param.CreateDate);
                _unitOfWork.Commit();

                //test get user for approval
                var user = _userRepository.GetAll();

                id = model.Id;

                //Audit Log ADD 
                int audit = _auditLogService.AddScenarioAudit(param, id);

                //int audit2 = _auditLogService.AddScenarioDetailAudit(scenarioDetails);
                int audit2 = _auditLogService.AddScenarioDetailAudit(scenarioDetailAll);
                int id2 = modelRiskMatrix.Id;
                int audit3 = _auditLogService.AddRiskMatrixAudit(model, id2);

                //insert into approval 
                var userApprove = _userRepository.Get(param.CreateBy.GetValueOrDefault());
                if (param.IsSend == true && userApprove.RoleId != 5)
                {
                    int auditApproval = _auditLogService.ScenarioApproval(id, param.CreateBy, "Draft", "Submit");
                }
                if (param.IsSend == true && userApprove.RoleId == 5)
                {
                    int auditApproval = _auditLogService.ScenarioApproval(id, param.CreateBy, "Draft", "Success");
                }
                //notifikasi email
                #region email
                if (status != null && status.Id != 2)
                {
                    int idDuplicate = Duplicate(param);
                    AddApproval(idDuplicate, status, param.CreateBy, param.CreateDate, nomorUrutStatus);
                    _unitOfWork.Commit();
                    if (nomorUrutStatus != 3)
                    {
                        emailDari = param.CreateBy.GetValueOrDefault();

                        var master = _masterApprovalScenarioRepository.GetByNomorUrut(nomorUrutStatus + 1);
                        Validate.NotNull(master, "Master Approval Scenario is not found.");
                        emailKepada = master.UserId.GetValueOrDefault();
                        typePesan = status.StatusDescription;
                        requestId = model.Id;
                        if (nomorUrutStatus < 2)
                        {
                            master = _masterApprovalScenarioRepository.GetByNomorUrut(nomorUrutStatus + 2);
                            Validate.NotNull(master, "Master Approval Scenario is not found.");
                            emailCC.Add(master.UserId.GetValueOrDefault());
                            // source code notifikasi email
                            _emailService.NotifyResetPassword(source, typePesan, requestId, emailKepada, emailCC, emailDari, templateNotif);
                        }
                        else
                        {
                            emailCC.Add(master.UserId.GetValueOrDefault());
                            _emailService.NotifyResetPassword(source, typePesan, requestId, emailCC, emailDari, templateNotif);
                        }

                    }

                }
                #endregion email
                _unitOfWork.Commit();
            }
            return id;

        }

        public int Update(int id, ScenarioParam param)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Nama Scenario is not found.");
            var userUpdate = _userRepository.Get(param.UpdateBy.GetValueOrDefault());

            if (userUpdate.RoleId != 3 && userUpdate.RoleId != 5)
            {
                ValidateCorrelatedProject(model, param);
                ValidateCorrelatedSektor(model, param);
                ValidateRiskMatrixProject(model, param);
            }
            //validasi untuk status approval dari masing2 tabel


            //variabel untuk email
            int emailDari = 0;
            int emailKepada = 0;
            List<int> emailCC = new List<int>();
            string source = "Scenario";
            string typePesan = "";
            Status statusEmail = null;
            string templateNotif;
            int requestId = 0;
            string keterangan = "";

            int nomorUrutStatus = 0;
            //Update ketika statusnya sudah d approve
            if (model.StatusId == 2)
            {
               
                if (userUpdate.RoleId != 3 && userUpdate.RoleId != 5)
                {
                    if (model.IsDefault != true)
                    {
                        throw new ApplicationException(string.Format("Scenario harus di set default agar bisa diedit"));
                    }
                }
                var PerluExistingName = _scenarioRepository.GetAllByName2Collate(Convert.ToInt32(param.UpdateBy), param.NamaScenario);
               // int ValidateName = PerluExistingName.Count();
                if (PerluExistingName >= 1)
                {
                    string dodol = null;
                    Validate.NotNull(dodol, "You cannot have 2 draft with same scenario name.");
                    return 0;
                }
                else
                {
                    var checkMasterScenario = _masterApprovalScenarioRepository.GetAll();
                    var checkoutMaster = checkMasterScenario.Count();
                    if (checkoutMaster == 0)
                    {
                        Validate.NotNull(null, "User must setting Approval for scenario first.");
                    }
                    int dodol = Convert.ToInt32(param.UpdateBy);
                    var masterApproval = _masterApprovalScenarioRepository.GetByUserId(dodol);

                   
                    if (masterApproval != null)
                    {
                        nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
                    }
                    if (masterApproval == null && (userUpdate.RoleId == 3 || userUpdate.RoleId == 4 || userUpdate.RoleId == 5))
                    {
                        masterApproval = _masterApprovalScenarioRepository.GetByRoleId(userUpdate.RoleId);
                        nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
                    }
                    if (nomorUrutStatus != 0)
                    {
                        param.IsUpdate = true;
                        param.IsDraftApproval = null;
                        param.IdScenarioLama = model.Id;
                        var duplicate = Duplicate(param);

                        var status = _statusRepository.GetStatus(nomorUrutStatus);
                        if (param.IsSend == true && status.Id != 2)
                        {
                            param.IsDraftApproval = true;
                            param.IdScenarioLama = null;
                            int idDuplicate = Duplicate(param);

                            AddApproval(idDuplicate, status, param.UpdateBy, param.UpdateDate, nomorUrutStatus);
                            _databaseContext.SaveChanges();
                            #region email
                            if (param.IsSend == true && param.IsStatusApproval == false || param.IsStatusApproval == null)
                            {
                                //Audit Approval
                                if (param.IsSend == true && userUpdate.RoleId != 5)
                                {
                                    int auditApproval = _auditLogService.ScenarioApproval(id, param.UpdateBy, "Draft", "Submit");
                                }
                                if (param.IsSend == true && userUpdate.RoleId == 5)
                                {
                                    int auditApproval = _auditLogService.ScenarioApproval(id, param.UpdateBy, "Draft", "Success");
                                }

                                if (nomorUrutStatus != 3)
                                {
                                    emailDari = param.UpdateBy.GetValueOrDefault();

                                    var master = _masterApprovalScenarioRepository.GetByNomorUrut(nomorUrutStatus + 1);
                                    Validate.NotNull(master, "Master Approval Scenario is not found.");
                                    emailKepada = master.UserId.GetValueOrDefault();

                                    typePesan = status.StatusDescription;

                                    requestId = model.Id;

                                    if (nomorUrutStatus < 2)
                                    {
                                        master = _masterApprovalScenarioRepository.GetByNomorUrut(nomorUrutStatus + 2);
                                        Validate.NotNull(master, "Master Approval Scenario is not found.");

                                        emailCC.Add(master.UserId.GetValueOrDefault());
                                        //source
                                        _emailService.NotifyResetPassword(source, typePesan, requestId, emailKepada, emailCC, emailDari, param.TemplateNotif);
                                    }
                                    else
                                    {
                                        emailCC.Add(master.UserId.GetValueOrDefault());
                                        _emailService.NotifyResetPassword(source, typePesan, requestId, emailCC, emailDari, param.TemplateNotif);
                                    }
                                }
                            }
                            #endregion email
                        }
                        if (status.Id == 2)
                        {
                            if (param.IsSend == true)
                            {
                                if (userUpdate.RoleId != 5)
                                {
                                    //Audit Approval
                                    int auditApproval = _auditLogService.ScenarioApproval(id, param.CreateBy, "Success", "Submit");
                                }
                                //var deleteScenario = Delete2(id, Convert.ToInt32(param.UpdateBy), Convert.ToDateTime(param.UpdateDate));
                                var deleteScenario = Update2(duplicate, param);                                
                            }
                        }
                        return duplicate;
                    }
                    else
                    {
                        param.IsUpdate = true;
                        param.IdScenarioLama = model.Id;
                        var duplicate = Duplicate(param);
                        var status = _statusRepository.GetStatus(nomorUrutStatus);
                        if (param.IsSend == true)
                        {
                            param.IsDraftApproval = true;
                            param.IdScenarioLama = null;
                            int idDuplicate = Duplicate(param);

                            AddApproval(idDuplicate, status, param.UpdateBy, param.UpdateDate, nomorUrutStatus);
                            _databaseContext.SaveChanges();

                            #region email
                            if (param.IsSend == true && param.IsStatusApproval == false || param.IsStatusApproval == null)
                            {
                                if (nomorUrutStatus != 3)
                                {
                                    emailDari = param.UpdateBy.GetValueOrDefault();

                                    var master = _masterApprovalScenarioRepository.GetByNomorUrut(nomorUrutStatus + 1);
                                    Validate.NotNull(master, "Master Approval Scenario is not found.");

                                    emailKepada = master.UserId.GetValueOrDefault();

                                    typePesan = status.StatusDescription;

                                    requestId = model.Id;

                                    if (nomorUrutStatus < 2)
                                    {
                                        master = _masterApprovalScenarioRepository.GetByNomorUrut(nomorUrutStatus + 2);
                                        Validate.NotNull(master, "Master Approval Scenario is not found.");

                                        emailCC.Add(master.UserId.GetValueOrDefault());
                                        //source
                                         _emailService.NotifyResetPassword(source, typePesan, requestId, emailKepada, emailCC, emailDari, param.TemplateNotif);
                                    }
                                    else
                                    {
                                        emailCC.Add(master.UserId.GetValueOrDefault());
                                        _emailService.NotifyResetPassword(source, typePesan, requestId, emailCC, emailDari, param.TemplateNotif);
                                    }

                                }
                            }
                            #endregion email
                        }
                        return duplicate;
                    }
                }
            }
            //Update ketika status bukan approval
            else
            {

                var likelihood = _likehoodRepository.Get(param.LikehoodId);
                Validate.NotNull(likelihood, "Likelihood is not found.");

                //var PerluExistingName = _scenarioRepository.GetAllByName2Collate(Convert.ToInt32(param.UpdateBy), param.NamaScenario);
               // int ValidateName = PerluExistingName.Count();
                //if (PerluExistingName >= 1)
                //{
                //    string dodol = null;
                //    Validate.NotNull(dodol, "You cannot have 2 draft with same scenario name.");
                //    return 0;
                //}
                //var PerluExistingName = _scenarioRepository.GetAllByName(model.Id);
                //int ValidateName = PerluExistingName.Count();
                ////if (ValidateName == 1)
                ////{
                ////    IsExistOnEditing(id, param.NamaScenario);

                ////}
                using (_unitOfWork)
                {
                    #region Approval
                    Status status = null;
                   // 
                    //Get Status from role id
                    //int nomorUrutStatus = 0;

                    //requestId tipe int IDScenario

                    bool isSend;
                    bool isStatusApproval;
                    if (model.StatusId != 2)
                    {

                        //Jika Scenario saat ini statusnya 1/submit, tidak bisa diedit
                        if (model.StatusId == 1 && param.IsStatusApproval == null)
                        {
                            throw new ApplicationException(string.Format("Scenario tidak bisa diedit karena dalam proses approval"));
                        }
                        //Jika Scenario saat ini statusnya 3/reject, maka akan kirim kembali ke Approval param.StatusId
                        //atau jika user menekan tombol kirim(IsSend = true), maka akan kirim ke tabel Approval

                        else if ((model.StatusId == 3 && param.IsSend == true) && param.IsStatusApproval == null || param.IsSend == true && param.IsStatusApproval == null)
                        {
                            var checkMasterScenario = _masterApprovalScenarioRepository.GetAll();
                            var checkoutMaster = checkMasterScenario.Count();
                            if (checkoutMaster == 0)
                            {
                                Validate.NotNull(null, "User must setting Approval for scenario first.");
                            }
                            var userCreate = _userRepository.Get(param.UpdateBy.GetValueOrDefault());
                            Validate.NotNull(userCreate, "User is not found.");

                            //get role level from user id
                            var masterApproval = _masterApprovalScenarioRepository.GetByUserId(userCreate.Id);
                            if (masterApproval != null)
                            {
                                nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
                            }
                            if (masterApproval == null && (userCreate.RoleId == 3 || userCreate.RoleId == 4 || userCreate.RoleId == 5))
                            {
                                masterApproval = _masterApprovalScenarioRepository.GetByRoleId(userCreate.RoleId);
                                nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
                            }
                            status = _statusRepository.GetStatus(nomorUrutStatus);
                            Validate.NotNull(status, "Status is not found.");
                            if (status != null && status.Id != 2)
                            {
                                param.IsDraftApproval = true;
                                int idDuplicate = Duplicate(param);
                                param.IsUpdate = true;
                                //insert into approval
                                AddApproval(idDuplicate, status, param.UpdateBy, param.UpdateDate, nomorUrutStatus);
                                _databaseContext.SaveChanges();
                            }
                            
                            //_unitOfWork.Commit();
                            model.UpdateStatus(status, param.UpdateBy, param.UpdateDate);
                        }
                        //jika user menekan tombol kirim, maka akan kirim ke tabel Approval
                    }
                    emailKepada = model.UpdateBy.GetValueOrDefault();
                    isStatusApproval = param.IsStatusApproval.GetValueOrDefault();
                    param.IsStatusApproval = null;
                    isSend = param.IsSend.GetValueOrDefault();
                    param.IsSend = null;
                    if (param.NomorUrutStatus.GetValueOrDefault() != 0)
                    {
                        nomorUrutStatus = param.NomorUrutStatus.GetValueOrDefault();
                        param.NomorUrutStatus = null;
                    }
                    if (param.StatusId != null)
                    {
                        statusEmail = _statusRepository.Get(param.StatusId.GetValueOrDefault());
                        param.StatusId = null;
                    }
                    templateNotif = param.TemplateNotif;
                    param.TemplateNotif = null;
                    param.IsUpdate = null;
                    param.IsDraftApproval = null;
                    keterangan = param.Keterangan;
                    param.Keterangan = null;
                    #endregion Approval
                    //Audit Log ADD 
                    int audit = _auditLogService.UpdateScenarioAudit(param, id);
                    model.Update(likelihood, param.NamaScenario, param.UpdateBy, param.UpdateDate, status);
                    _scenarioRepository.Update(model);

                    RemoveOldScenarioDetailUpdate(id, model, param);
                    UpdateCorrelatedSektor(model, param.UpdateBy, param.UpdateDate, 0);
                    _unitOfWork.Commit();

                    var riskMatrix = _riskMatrixRepository.GetByScenarioId(id);
                    GenerateRiskMatrixProject(model, riskMatrix, param.UpdateBy, param.UpdateDate);

                    //Update table CorrelatedProject
                    UpdateCorrelatedProject(model, param.UpdateBy, param.UpdateDate);

                    //Audit Log ADD 
                    int id2 = riskMatrix.Id;
                    int audit3 = _auditLogService.AddRiskMatrixAudit(model, id2);
                    // }
                    _databaseContext.SaveChanges();
                    #region email notif
                    //Untuk notifikasi email submit
                    if (isSend == true && isStatusApproval == false)
                    {
                        //AddApproval(id, status, param.UpdateBy, param.UpdateDate, nomorUrutStatus);

                        if (nomorUrutStatus != 3)
                        {
                            emailDari = param.UpdateBy.GetValueOrDefault();

                            var master = _masterApprovalScenarioRepository.GetByNomorUrut(nomorUrutStatus + 1);
                            Validate.NotNull(master, "Master Approval Scenario tidak ditemukan.");

                            //
                            emailKepada = master.UserId.GetValueOrDefault();


                            typePesan = status.StatusDescription;

                            requestId = model.Id;

                            if (nomorUrutStatus < 2)
                            {
                                master = _masterApprovalScenarioRepository.GetByNomorUrut(nomorUrutStatus + 2);
                                Validate.NotNull(master, "Master Approval Scenario tidak ditemukan.");

                                emailCC.Add(master.UserId.GetValueOrDefault());
                                //source
                                _emailService.NotifyResetPassword(source, typePesan, requestId, emailKepada, emailCC, emailDari, templateNotif);
                            }
                            else
                            {
                                emailCC.Add(master.UserId.GetValueOrDefault());
                                _emailService.NotifyResetPassword(source, typePesan, requestId, emailCC, emailDari, templateNotif);
                            }



                        }
                    }

                    //untuk notifikasi email approve
                    if (isSend == false && isStatusApproval == true)
                    {
                        //AddApproval(id, status, param.UpdateBy, param.UpdateDate, nomorUrutStatus);

                        if (nomorUrutStatus != 3)
                        {
                            emailDari = param.UpdateBy.GetValueOrDefault();

                            var master2 = _masterApprovalScenarioRepository.GetByNomorUrut(nomorUrutStatus + 1);
                            emailKepada = master2.UserId.GetValueOrDefault();

                            var master = _masterApprovalScenarioRepository.GetAll();
                            Validate.NotNull(master, "Master Approval Scenario tidak ditemukan.");

                            foreach (var item in master)
                            {
                                emailCC.Add(item.UserId.GetValueOrDefault());
                            }
                            //
                            emailCC.Add(model.CreateBy.GetValueOrDefault());

                            Validate.NotNull(statusEmail, "Status description approve scenario tidak ditemukan.");
                            //
                            typePesan = statusEmail.StatusDescription;

                            //
                            requestId = model.Id;

                            //source
                            _emailService.NotifyResetPassword(source, typePesan, requestId, emailKepada, emailCC, emailDari, templateNotif);

                        }
                    }
                    #endregion email notif
                    _unitOfWork.Commit();
                    id = model.Id;
                    //Audit Log ADD                 
                    if (nomorUrutStatus == 3)
                    {
                        var deleteScenario = Update2(id, param);
                    }
                }
                return id;
            }
        }        

        public int Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Nama Scenario is not found.");


            RiskMatrix modelRiskMatrix = _riskMatrixRepository.GetByScenarioId(model.Id);
            Validate.NotNull(modelRiskMatrix, "Risk Matrix is not found.");

            //if (model.StatusId != null)
            //{
            //    throw new ApplicationException(string.Format("Scenario tidak bisa dihapus karna dalam proses approval"));
            //}


            using (_unitOfWork)
            {
                model.Delete(deleteBy, deleteDate);
                _scenarioRepository.Update(model);
                
                //delete risk matrix
                modelRiskMatrix.Delete(deleteBy, deleteDate);

                //delete approval  scenario
                //RemoveApproval(id, deleteBy, deleteDate, namaScenario);

                _unitOfWork.Commit();
                id = model.Id;

                //Audit Log Delete 
                int audit = _auditLogService.DeleteScenarioAudit(id, deleteBy);
            }
            return id;
        }

        public int Delete2(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Nama Scenario is not found.");

            RiskMatrix modelRiskMatrix = _riskMatrixRepository.GetByScenarioId(model.Id);
            Validate.NotNull(modelRiskMatrix, "Risk Matrix is not found.");

            model.Delete(deleteBy, deleteDate);
            _scenarioRepository.Update(model);
            
            //delete risk matrix
            modelRiskMatrix.Delete(deleteBy, deleteDate);
            id = model.Id;
            _databaseContext.SaveChanges();
            return id;
        }

        //#endregion Manipulation

        #region Private
        private IList<ScenarioDetail> GenerateScenarioDetailAll(List<Project> list, ScenarioParam param, Scenario model)
        {
            IList<ScenarioDetail> scenarioDetailAll = new List<ScenarioDetail>();
            foreach (var item in list)
            {
                ScenarioDetail scenarioDetail = new ScenarioDetail(item, model.Id, param.CreateBy, param.CreateDate);
                scenarioDetailAll.Add(scenarioDetail);
            }
            IList<ScenarioDetail> scenarioDetails = GenerateScenarioDetail(param, model);

            foreach (var item in scenarioDetailAll)
            {
                int Batasan = 0;
                foreach (var item2 in scenarioDetails)
                {
                    if (Batasan == 0)
                    {
                        if (item.ProjectId == item2.ProjectId)
                        {
                            item.IsDelete = false;
                            Batasan = 1;
                        }
                        else
                        {
                            item.IsDelete = true;
                        }
                    }
                }
            }
            model.AddScenarioDetail(scenarioDetailAll);
            return scenarioDetailAll;
        }

        private IList<ScenarioDetail> GenerateScenarioDetail(ScenarioParam param, Scenario model)
        {
            IList<ScenarioDetail> scenarioDetails = new List<ScenarioDetail>();
            List<int> projectIds = new List<int>();

            if (param.ProjectId != null)
            {
                for (int i = 0; i < param.ProjectId.Length; i++)
                {
                    int n;
                    if (Int32.TryParse(param.ProjectId[i], out n))
                    {
                        if (!projectIds.Contains(n))
                            projectIds.Add(n);
                    }
                }

                foreach (var item in projectIds)
                {
                    var project = _projectRepository.Get(item);
                    Validate.NotNull(project, "Project " + project.NamaProject + " is not found.");

                    ScenarioDetail scenarioDetail = new ScenarioDetail(project, model.Id, param.CreateBy, param.CreateDate);
                    scenarioDetails.Add(scenarioDetail);
                }
                //model.AddScenarioDetail(scenarioDetails);
            }
            return scenarioDetails;
        }

        private void RemoveOldScenarioDetailUpdate(int id, Scenario model, ScenarioParam param)
        {
            //insert new project risk
            IList<ScenarioDetail> scenarioDetails = GenerateScenarioDetailUpdate(param, model);

            var scenarioDetailDB = _scenarioDetailRepository.GetAllByScenarioId(id);
            var list = _projectRepository.GetAll().ToList();

            IList<ScenarioDetail> ScenarioDetailsAll = new List<ScenarioDetail>();
            foreach (var item in list)
            {
                ScenarioDetail scenarioDetailNewL = new ScenarioDetail(item, model.Id, param.CreateBy, param.CreateDate);
                ScenarioDetailsAll.Add(scenarioDetailNewL);
            }
            var scenarioDetailNew = ScenarioDetailsAll;

            if (scenarioDetailNew != null)
            {
                foreach (var item in scenarioDetailNew)
                {
                    int Batasan = 0;
                    foreach (var item2 in scenarioDetails)
                    {
                        if (Batasan == 0)
                        {
                            if (item.ProjectId == item2.ProjectId)
                            {
                                item.IsDelete = false;
                                Batasan = 1;
                            }
                            else
                            {
                                item.IsDelete = true;
                            }
                        }
                    }
                }
            }

            foreach (var item in scenarioDetailDB)
            {
                foreach (var item2 in scenarioDetailNew)
                {
                    if (item.ProjectId == item2.ProjectId)
                    {
                        if (item.IsDelete != item2.IsDelete)
                        {
                            int audit2 = _auditLogService.UpdateScenarioDetailAudit(item2, model, item);

                            item.IsDelete = item2.IsDelete;
                            item.UpdateBy = param.UpdateBy;
                            item.UpdateDate = param.UpdateDate;
                            _scenarioDetailRepository.Update(item);
                            _unitOfWork.Commit();
                        }
                    }
                }
            }

            foreach (var item2 in scenarioDetailNew)
            {
                int find = 0;
                foreach (var item in scenarioDetailDB)
                {
                    if (find != 1)
                    {
                        if (item2.ProjectId == item.ProjectId)
                        {
                            find = 1;
                        }
                    }
                }
                if (find == 0)
                {
                    _scenarioDetailRepository.Insert(item2);
                    _unitOfWork.Commit();

                    //Add Audit
                    int audit2 = _auditLogService.AddScenarioDetailAuditByUpdate(item2, model.UpdateBy);
                }
            }
        }

        private void RemoveOldScenarioDetail(int id, Scenario model, int? deleteBy, DateTime? deleteDate)
        {
            var scenarioDetail = _scenarioDetailRepository.GetByScenarioId(id);
            if (scenarioDetail != null)
            {
                foreach (var item in scenarioDetail)
                {
                    model.RemoveScenarioDetail(item);
                    _scenarioDetailRepository.Delete(item.Id);
                }
            }
        }

        private IList<ScenarioDetail> GenerateScenarioDetailUpdate(ScenarioParam param, Scenario model)
        {
            IList<ScenarioDetail> scenarioDetails = new List<ScenarioDetail>();
            List<int> projectIds = new List<int>();

            if (param.ProjectId != null)
            {
                for (int i = 0; i < param.ProjectId.Length; i++)
                {
                    int n;
                    if (Int32.TryParse(param.ProjectId[i], out n))
                    {
                        if (!projectIds.Contains(n))
                            projectIds.Add(n);
                    }
                }

                foreach (var item in projectIds)
                {
                    var project = _projectRepository.Get(item);
                    Validate.NotNull(project, "Project " + project.NamaProject + " is not found.");

                    ScenarioDetail scenarioDetail = new ScenarioDetail(project, model.Id, param.CreateBy, param.CreateDate);
                    scenarioDetails.Add(scenarioDetail);
                }
            }
            return scenarioDetails;
        }

        private IList<CorrelationRiskAntarSektor> GenereateCorrelationRiskAntarSektor(Scenario scenario, IList<ScenarioDetail> scenarioDetails, int? createBy, DateTime? createDate)
        {
            IList<CorrelationRiskAntarSektor> collectionCorrelationSektor = new List<CorrelationRiskAntarSektor>();

            IList<ScenarioDetail> scenarioDetailNew = new List<ScenarioDetail>();

            if (scenarioDetails.Count > 0)
            {
                foreach (var item in scenarioDetails)
                {
                    //CorrelationRiskAntarSektor modelCorrelationSektor = new CorrelationRiskAntarSektor(scenario, item.Project.Sektor, item.Project, createBy, createDate);
                    //_correlationRiskAntarSektorRepository.Insert(modelCorrelationSektor);
                    //collectionCorrelationSektor.Add(modelCorrelationSektor);
                    if (item.IsDelete == false)
                    {
                        //Use for list of Correlation Risk Antar Project
                        CorrelationRiskAntarSektor modelCorrelationSektor = new CorrelationRiskAntarSektor(scenario, item.Project.Sektor, item.Project, createBy, createDate);
                        _correlationRiskAntarSektorRepository.Insert(modelCorrelationSektor);
                        collectionCorrelationSektor.Add(modelCorrelationSektor);
                        scenarioDetailNew.Add(item);
                    }
                }
            }
            //insert into correlated sector
            IList<CorrelatedSektor> correlatedSektors = GenerateCorrelatedSector(scenarioDetailNew, scenario, createBy, createDate);
            _correlatedSectorRepository.Insert(correlatedSektors);

            return collectionCorrelationSektor;
        }

        private void UpdateCorrelationRiskAntarSektor(Scenario scenario, int? updateBy, DateTime? updateDate)
        {
            Validate.NotNull(updateBy, "Id UpdateBy tidak bisa ditemukan");

            IList<CorrelationRiskAntarSektor> oldCorrelationRiskAntarSektorList = new List<CorrelationRiskAntarSektor>();

            IList<CorrelationRiskAntarSektor> newCorrelationRiskAntarSektorList = new List<CorrelationRiskAntarSektor>();

            IList<ScenarioDetail> newScenarioDetailList = _scenarioDetailRepository.GetByScenarioId(scenario.Id).ToList();

            IList<CorrelationRiskAntarSektor> oldCorrelationRiskAntarSektorDataList = _correlationRiskAntarSektorRepository.GetByScenarioIdAll(scenario.Id).ToList();

            if (newScenarioDetailList != null)
            {
                foreach (var item in newScenarioDetailList)
                {
                    CorrelationRiskAntarSektor model = _correlationRiskAntarSektorRepository.GetByProjectIdScenarioIdAll(item.ProjectId, item.ScenarioId);
                    if (model == null)
                    {
                        CorrelationRiskAntarSektor modelCorrelationRiskAntarSektor = new CorrelationRiskAntarSektor(scenario, item.Project.Sektor, item.Project, updateBy, updateDate);
                        newCorrelationRiskAntarSektorList.Add(modelCorrelationRiskAntarSektor);
                        _correlationRiskAntarSektorRepository.Insert(modelCorrelationRiskAntarSektor);
                    }
                    if (model != null)
                    {
                        CorrelationRiskAntarSektor modelCorrelationSektor = model;
                        oldCorrelationRiskAntarSektorList.Add(modelCorrelationSektor);
                    }
                }

                foreach (var item in oldCorrelationRiskAntarSektorDataList)
                {
                    item.Delete(updateBy.GetValueOrDefault(), updateDate.GetValueOrDefault());
                    _correlationRiskAntarSektorRepository.Update(item);
                }

                foreach (var item in oldCorrelationRiskAntarSektorDataList)
                {
                    if (oldCorrelationRiskAntarSektorList != null)
                    {
                        foreach (var item2 in oldCorrelationRiskAntarSektorList)
                        {
                            if (item.ScenarioId == item2.ScenarioId && item.ProjectId == item2.ProjectId)
                            {
                                item.RemoveDelete(updateBy.GetValueOrDefault(), updateDate.GetValueOrDefault());
                                _correlationRiskAntarSektorRepository.Update(item);
                            }
                            //var contoh = _correlationRiskAntarSektorRepository.GetByProjectIdScenarioId(item2.ProjectId, item2.ScenarioId);
                            //if (contoh == null)
                            //{
                            //   // item.Delete(updateBy.GetValueOrDefault(), updateDate.GetValueOrDefault());
                            //    _correlationRiskAntarSektorRepository.Delete(item.Id, updateBy.GetValueOrDefault(), updateDate.GetValueOrDefault());
                            //    _correlationRiskAntarSektorRepository.Update(item);
                            //}
                            //else if (contoh != null)
                            //{
                            //    oldCorrelationSektorList.Remove(item2);
                            //    if (oldCorrelationSektorList.Count() == 0)
                            //    {
                            //        break;
                            //    }

                            //}
                        }
                    }
                }
            }


        }

        private void UpdateCorrelatedProject(Scenario scenario, int? updateBy, DateTime? updateDate)
        {
            //Cara menghapus data pada tablenya secara langsung
            //var correlationRiskAntarSektor = _correlationRiskAntarSektorRepository.GetByScenarioDefault(scenario.Id).ToList();

            //if (correlationRiskAntarSektor.Count > 0)
            //{
            //    var correlatedProject = _correlatedProjectRepository.GetByScenarioId(scenario.Id).ToList();
            //    if(correlatedProject.Count > 0)
            //    {
            //        foreach (var item in correlatedProject)
            //        {
            //            _correlatedProjectRepository.Delete(item.Id);
            //            //lakukan hapus juga pada approval correlatedProject
            //            //_approvalRepository.Delete(item.Id, sourceApproval);
            //        }
            //    }

            //    foreach (var item in correlationRiskAntarSektor)
            //    {
            //        CorrelatedProject model = new CorrelatedProject(scenario, item.ProjectId, item.SektorId, updateBy, updateDate);
            //        _correlatedProjectRepository.Insert(model);
            //       // AddApproval(item.Id, role, status, createBy, createDate, sourceApproval);
            //    }
            //}



            //Cara mengupdate IsDelete = true pada table 
            //var correlationRiskAntarSektor = _correlatedProjectRepository.GetByScenarioId(scenario.Id, null, 0).ToList();

            //IList<CorrelatedProject> oldCorrelatedProject = new List<CorrelatedProject>();

            //if (correlationRiskAntarSektor.Count > 0)
            //{
            //    var correlatedProject = _correlatedProjectRepository.GetByScenarioId(scenario.Id, null, 0).ToList();
            //    if (correlatedProject.Count > 0)
            //    {
            //        foreach (var item in correlatedProject)
            //        {
            //            item.Delete(updateBy, updateDate);
            //            _correlatedProjectRepository.Update(item);
            //            //lakukan hapus juga pada approval correlatedProject
            //            //_approvalRepository.Delete(item.Id, sourceApproval);
            //        }
            //    }

            //    foreach (var item in correlationRiskAntarSektor)
            //    {
            //        var correlatedProjectOld = _correlatedProjectRepository.GetByScenarioIdProjectIdAll(item.ScenarioId, item.ProjectId);
            //        if (correlatedProjectOld == null)
            //        {
            //            CorrelatedProject model = new CorrelatedProject(scenario, item.ProjectId, item.SektorId, updateBy, updateDate);
            //            _correlatedProjectRepository.Insert(model);
            //        }
            //        else
            //        {
            //            correlatedProjectOld.RemoveDelete(updateBy, updateDate);
            //            _correlatedProjectRepository.Update(correlatedProjectOld);

            //            //var oldCorrelatedProjectDetailList = _correlatedProjectDetailRepository.GetByCorrelatedProjectId(correlatedProjectOld.Id);

            //            //if (oldCorrelatedProjectDetailList != null)
            //            //{
            //            //    foreach (var itemOldCorrelatedProjectDetail in oldCorrelatedProjectDetailList)
            //            //    {
            //            //        itemOldCorrelatedProjectDetail.RemoveDelete(updateBy, updateDate);
            //            //        _correlatedProjectDetailRepository.Update(itemOldCorrelatedProjectDetail);
            //            //    }
            //            //}
            //        }
            //        // AddApproval(item.Id, role, status, createBy, createDate, sourceApproval);
            //    }

            //    //var correlatedProjectList = _correlatedProjectRepository.GetByScenarioId(scenario.Id, null, 0);

            //    //IList<CorrelatedProjectDetail> CorrelatedProjectDetailList = new List<CorrelatedProjectDetail>();

            //    //foreach (var item in correlatedProjectList)
            //    //{
            //    //    //var oldCorrelatedProjectDetailList = _correlatedProjectDetailRepository.GetByCorrelatedProjectId(item.Id);
            //    //    //if (oldCorrelatedProjectDetailList != null)
            //    //    //{
            //    //    //    foreach (var itemCorrelatedProjectDetail in oldCorrelatedProjectDetailList)
            //    //    //    {
            //    //    //        itemCorrelatedProjectDetail.Delete(updateBy, updateDate);
            //    //    //        _correlatedProjectDetailRepository.Update(itemCorrelatedProjectDetail);
            //    //    //    }
            //    //    //}


            //    //    //if (item.IsDelete == true && item.StatusId != null)
            //    //    //{
            //    //    //    throw new ApplicationException(string.Format("Correlated Project tidak bisa diedit karena dalam proses approval"));
            //    //    //}
            //    //}

            //}

            var getScenarioDetail = scenario.ScenarioDetail.Where(x => x.IsDelete == false);

            IList<int> oldProjects = new List<int>();
            IList<int> newProjects = new List<int>();
            IList<int> removeProjects = new List<int>();
            IList<int> addProjects = new List<int>();

            IList<int> oldSameProjects = new List<int>();

            var currentProject = _correlatedProjectRepository.GetByScenarioIdAll(scenario.Id).Where(x => x.IsDelete == false);
            foreach (var pro in currentProject)
            {
                if (!oldProjects.Contains(pro.ProjectId))
                {
                    oldProjects.Add(pro.ProjectId);
                }
            }

            foreach (var item in getScenarioDetail)
            {
                var newProject = item.Project;
                newProjects.Add(newProject.Id);
            }

            if (newProjects.Count > 0)
            {
                foreach (var item in newProjects)
                {
                    var temData = _correlatedProjectRepository.GetByScenarioIdProjectIdList(scenario.Id, item);
                    if (temData.Count() == 0)
                    {
                        addProjects.Add(item);
                    }
                    else
                    {
                        if (!oldProjects.Contains(item))
                        {
                            oldSameProjects.Add(item);
                        }
                        
                    }
                }
            }

            if (oldProjects.Count > 0)
            {
                var tempNeedToRemove = oldProjects.Except(newProjects);
                if (tempNeedToRemove.Count() > 0)
                {
                    foreach (var item in tempNeedToRemove)
                    {
                        removeProjects.Add(item);
                        //CorrelatedProject correlatedProject = _correlatedProjectRepository.GetByScenarioIdProjectIdAllFalse(scenario.Id, item);

                        //int y = 0;
                        //if (correlatedProject != null)
                        //{
                        //    correlatedProject.Delete(updateBy, updateDate);
                        //}
                        
                    }
                }
            }
            //add new project
            if (addProjects.Count > 0)
            {
                IList<CorrelatedProject> addCorrelatedProjects = new List<CorrelatedProject>();
                //

                foreach (var item in addProjects)
                {
                    var project = _projectRepository.Get(item);
                    if (project != null)
                    {
                        CorrelatedProject riskMatrixProject = new CorrelatedProject(scenario, project.Id, project.SektorId, project.UserId, updateDate);
                        addCorrelatedProjects.Add(riskMatrixProject);
                        _correlatedProjectRepository.Insert(riskMatrixProject);
                    }
                }
                //_correlatedProjectRepository.Insert(addCorrelatedProjects);

                //_databaseContext.SaveChanges();

                //foreach (var item in addRiskMatrixProjects)
                //{
                //    //add to table approval 
                //    //AddApproval(item.Id, role, status, createBy, createDate, sourceApproval);
                //}
            }

            if (removeProjects.Count > 0)
            {
                foreach (var item in removeProjects)
                {
                    var project = _correlatedProjectRepository.GetByScenarioIdProjectIdList(scenario.Id, item);
                    if (project != null)
                    {
                        foreach (var item2 in project)
                        {
                            item2.Delete(updateBy, updateDate);
                        }
                    }
                }
            }

            if (oldSameProjects.Count > 0)
            {
                foreach (var item in oldSameProjects)
                {
                    var temData = _correlatedProjectRepository.GetByScenarioIdProjectIdList(scenario.Id, item);
                    if (temData != null)
                    {
                        foreach (var item2 in temData)
                        {
                            item2.RemoveDelete(updateBy, updateDate);
                        }
                    }
                }
            }

        }

        private void UpdateCorrelatedSektor(Scenario scenario, int? updateBy, DateTime? updateDate, int? createBy)
        {
            Validate.NotNull(updateBy, "Id UpdateBy tidak bisa ditemukan");
            var getScenarioDetail = scenario.ScenarioDetail.Where(x => x.IsDelete == false);

            IList<int> oldSektors = new List<int>();
            IList<int> newSektors = new List<int>();
            IList<int> removeSektors = new List<int>();
            IList<int> addSektors = new List<int>();

            IList<int> oldSameSektors = new List<int>();

            var currentSektor = _correlatedSectorRepository.GetByScenarioIdIsDeleteFalse(scenario.Id);
            foreach (var pro in currentSektor)
            {
                oldSektors.Add(pro.SektorId);
            }

            foreach (var item in getScenarioDetail)
            {
                var newSektor = item.Project.Sektor;
                if (!newSektors.Contains(newSektor.Id))
                {
                    newSektors.Add(newSektor.Id);
                }
            }

            if (newSektors.Count > 0)
            {
                foreach (var item in newSektors)
                {
                    var temData = _correlatedSectorRepository.GetByScenarioIdSektorIdList(scenario.Id, item);
                    if (temData.Count() == 0)
                    {
                        addSektors.Add(item);
                    }
                    else
                    {
                        if (!oldSameSektors.Contains(item))
                        {
                            oldSameSektors.Add(item);
                        }
                        
                    }
                }
            }

            if (oldSektors.Count > 0)
            {
                var tempNeedToRemove = oldSektors.Except(newSektors);
                if (tempNeedToRemove.Count() > 0)
                {
                    foreach (var item in tempNeedToRemove)
                    {
                        removeSektors.Add(item);
                    }
                }
            }

            if (addSektors.Count > 0)
            {
                IList<CorrelatedSektor> addCorrelatedSektors = new List<CorrelatedSektor>();
                //

                foreach (var item in addSektors)
                {
                    var sektor = _sektorRepository.Get(item);
                    if (sektor != null)
                    {
                        CorrelatedSektor correlatedSektor = new CorrelatedSektor(sektor.Id, sektor.NamaSektor, scenario, updateBy, updateDate);
                        _correlatedSectorRepository.Insert(correlatedSektor);
                    }
                }
            }

            if (removeSektors.Count > 0)
            {
                foreach (var item in removeSektors)
                {
                    var sektor = _correlatedSectorRepository.GetByScenarioIdSektorIdList(scenario.Id, item);
                    if (sektor != null)
                    {
                        foreach (var item2 in sektor)
                        {
                            item2.Delete(updateBy.GetValueOrDefault(), updateDate.GetValueOrDefault());
                        }
                        
                    }
                }
            }

            if (oldSameSektors.Count > 0)
            {
                foreach (var item in oldSameSektors)
                {
                    var temData = _correlatedSectorRepository.GetByScenarioIdSektorIdList(scenario.Id, item);
                    if (temData != null)
                    {
                        foreach (var item2 in temData)
                        {
                            item2.RemoveDelete(updateBy.GetValueOrDefault(), updateDate.GetValueOrDefault());
                        }
                        
                    }

                }
            }


        }

        private IList<CorrelatedSektor> GenerateCorrelatedSector(IList<ScenarioDetail> scenarioDetails, Scenario scenario, int? createBy, DateTime? createDate)
        {
            var sektors = _sektorRepository.GetAll();
            IList<Sektor> currentSektor = new List<Sektor>();
            IList<CorrelatedSektor> currentCorrelatedSektor = new List<CorrelatedSektor>();

            if (scenarioDetails != null)
            {
                foreach (var item in scenarioDetails)
                {
                    var sektor = item.Project.Sektor;
                    if (!currentSektor.Contains(sektor))
                    {
                        currentSektor.Add(sektor);
                        CorrelatedSektor correlatedSector = new CorrelatedSektor(sektor.Id, sektor.NamaSektor, scenario, createBy, createDate);
                        currentCorrelatedSektor.Add(correlatedSector);
                    }
                }
            }

            return currentCorrelatedSektor;
        }

        private void GenerateRiskMatrixProject(Scenario scenario, RiskMatrix riskMatrix, int? createBy, DateTime? createDate)
        {

            var getScenarioDetail = scenario.ScenarioDetail.Where(x => x.IsDelete == false);

            IList<int> oldProjects = new List<int>();
            IList<int> newProjects = new List<int>();
            IList<int> removeProjects = new List<int>();
            IList<int> addProjects = new List<int>();

            IList<int> oldSameProjects = new List<int>();

            var currentProject = _riskMatrixProjectRepository.GetByScenarioId(scenario.Id);
            foreach (var pro in currentProject)
            {
                oldProjects.Add(pro.ProjectId);
            }

            foreach (var item in getScenarioDetail)
            {
                var newProject = item.Project;
                newProjects.Add(newProject.Id);
            }

            if (newProjects.Count > 0)
            {

                foreach (var item in newProjects)
                {
                    var temData = _riskMatrixProjectRepository.GetByScenarioIdProjectIdList(scenario.Id, item);
                    if (temData.Count() == 0)
                    {
                        addProjects.Add(item);
                    }
                    else
                    {
                        if (!oldSameProjects.Contains(item))
                        {
                            oldSameProjects.Add(item);
                        }
                        
                    }
                }
            }
            //if (temData.Count() > 1)
            //{
            //    foreach (var item2 in temData)
            //    {
            //        item2.Delete(createBy, createDate);
            //    }
            //    addProjects.Add(item);
            //}
            //else

            //if (newProjects.Count > 0)
            //  {
            //      foreach (var item in newProjects)
            //      {
            //          var temData = _riskMatrixProjectRepository.GetByScenarioIdProjectIdAllList(scenario.Id, item);
            //          if (temData == null)
            //          {
            //              addProjects.Add(item);
            //          }
            //          else
            //          {
            //              oldSameProjects.Add(item);
            //          }
            //      }
            //  }

            if (oldProjects.Count > 0)
            {
                var tempNeedToRemove = oldProjects.Except(newProjects);
                if (tempNeedToRemove.Count() > 0)
                {
                    foreach (var item in tempNeedToRemove)
                    {
                        removeProjects.Add(item);
                    }
                }
            }

            //add new project
            if (addProjects.Count > 0)
            {
                IList<RiskMatrixProject> addRiskMatrixProjects = new List<RiskMatrixProject>();
                //

                foreach (var item in addProjects)
                {
                    var project = _projectRepository.Get(item);
                    if (project != null)
                    {
                        RiskMatrixProject riskMatrixProject = new RiskMatrixProject(project, riskMatrix, scenario, project.UserId, createDate);
                        addRiskMatrixProjects.Add(riskMatrixProject);
                    }
                }
                _riskMatrixProjectRepository.Insert(addRiskMatrixProjects);


            }
            if (removeProjects.Count > 0)
            {
                foreach (var item in removeProjects)
                {
                    var project = _riskMatrixProjectRepository.GetByScenarioIdProjectIdList(scenario.Id, item);
                    if (project != null)
                    {
                        foreach (var item2 in project)
                        {
                            item2.Delete(createBy, createDate);
                        }
                    }
                }
            }

            if (oldSameProjects.Count > 0)
            {
                foreach (var item in oldSameProjects)
                {
                    var temData = _riskMatrixProjectRepository.GetByScenarioIdProjectIdList(scenario.Id, item);
                    if (temData != null)
                    {
                        foreach (var item2 in temData)
                        {
                            item2.RemoveDelete(createBy, createDate);
                        }
                        
                    }

                }
            }


        }

        private void RemoveCurrentRiskMatrixProject(Scenario scenarioDefault, int? updateBy, DateTime? updateDate)
        {
            var riskMatrixProject = _riskMatrixProjectRepository.GetByScenarioId(scenarioDefault.Id);
            foreach (var item in riskMatrixProject)
            {
                item.Delete(updateBy, updateDate);
                _riskMatrixProjectRepository.Update(item);
            }
        }

        private void SetStatusRiskMatrix(Scenario scenarioNewDefault, Scenario scenarioOldDefault, int? createBy, DateTime? createDate)
        {
            RiskMatrix oldRiskMatrix = null;
            RiskMatrix newRiskMatrix = null;

            if (scenarioOldDefault != null)
            {
                oldRiskMatrix = _riskMatrixRepository.GetByScenarioId(scenarioOldDefault.Id);
            }

            if (scenarioNewDefault != null)
            {
                newRiskMatrix = _riskMatrixRepository.GetByScenarioId(scenarioNewDefault.Id);
            }

            if (oldRiskMatrix != null)
            {
                oldRiskMatrix.Delete(createBy, createDate);
                _riskMatrixRepository.Update(oldRiskMatrix);
            }

            if (newRiskMatrix != null)
            {
                newRiskMatrix.SetActive(createBy, createDate);
                _riskMatrixRepository.Update(newRiskMatrix);
            }
        }

        private void SetRiskMatrixProject(Scenario scenarioNew, RiskMatrix riskMatrix, int? createBy, DateTime? createDate)
        {
            IList<RiskMatrixProject> newRiskMatrixProject = null;
            var projects = _scenarioDetailRepository.GetByScenarioId(scenarioNew.Id).ToList();

            if (scenarioNew != null)
            {
                newRiskMatrixProject = _riskMatrixProjectRepository.GetAllByScenarioId(scenarioNew.Id).ToList();
            }

            if (newRiskMatrixProject.Count == 0)
            {
                if (projects.Count > 0)
                {
                    foreach (var item in projects)
                    {
                        RiskMatrixProject model = new RiskMatrixProject(item.Project, riskMatrix, scenarioNew, item.Project.UserId, createDate);
                        _riskMatrixProjectRepository.Insert(model);
                        _databaseContext.SaveChanges();
                        //add risk matrix project into Correlation Risk Antra Sektor
                        //var correlationRiskAntarSektor = _correlationRiskAntarSektorRepository.GetByProjectIdScenarioId(model.ProjectId, model.ScenarioId);
                        //correlationRiskAntarSektor.AddRiskMatrixProject(model.Id, createBy, createDate);
                        //_correlationRiskAntarSektorRepository.Update(correlationRiskAntarSektor);
                    }
                }
            }
        }

        //private void GenerateCorrelatedProject(Scenario scenarioNewDefault, int? updateBy, DateTime? updateDate)
        private void GenerateCorrelatedProject(Scenario scenarioNewDefault, int? updateBy, DateTime? updateDate)
        {
            var correlationRiskAntarSektor = _correlationRiskAntarSektorRepository.GetByScenarioDefault(scenarioNewDefault.Id).ToList();

            if (correlationRiskAntarSektor.Count > 0)
            {
                var correlatedProject = _correlatedProjectRepository.GetByScenarioId(scenarioNewDefault.Id, null, 0).ToList();
                if (correlatedProject.Count > 0)
                {
                    foreach (var item in correlatedProject)
                    {
                        _correlatedProjectRepository.Delete(item.Id);
                        //lakukan hapus juga pada approval correlatedProject
                        //_approvalRepository.Delete(item.Id, sourceApproval);
                    }
                }

                foreach (var item in correlationRiskAntarSektor)
                {
                    CorrelatedProject model = new CorrelatedProject(scenarioNewDefault, item.ProjectId, item.SektorId, item.Project.UserId, updateDate);
                    _correlatedProjectRepository.Insert(model);
                    // AddApproval(item.Id, role, status, createBy, createDate, sourceApproval);
                }
            }
        }

        private void AddApproval(int idParam, Status status, int? createBy, DateTime? createDate, int nomorUrutStatus)
        {
            var user = _userRepository.Get(createBy.GetValueOrDefault());
            Validate.NotNull(user, "User Create is not found.");

            var userRole = _userRepository.GetRoleUser(user.Id);
            Validate.NotNull(userRole, "User Role Create is not found.");

            string statusApproval = status.StatusDescription + " By " + user.UserName + " Role " + userRole.Name;
            string keterangan = "";
            string sourceApproval = "Scenario";

            Approval model = new Approval(idParam, statusApproval,
            sourceApproval, status, user.Id, createDate, keterangan, nomorUrutStatus);

            _approvalRepository.Insert(model);

        }

        private void RemoveApproval(int id, int deleteBy, DateTime deleteDate, string sourceApproval)
        {
            _approvalRepository.Delete(id, sourceApproval, deleteBy, deleteDate);
            //var approvals = _approvalRepository.GetAllBaseOnSource(id, sourceApproval);
            //if (approvals != null)
            //{
            //    foreach (var item in approvals)
            //    {
            //        //model.RemoveScenarioDetail(item);
            //        _approvalRepository.Delete(item.Id, deleteBy, deleteDate);
            //    }
            //}
        }

        private void SetScenarioInFunctionalRisk(Scenario scenarioNewDefault, Scenario modelOldDefault, int? updateBy, DateTime? updateDate)
        {
            if (modelOldDefault != null)
            {
                var functionalOld = _functionalRiskRepository.GetByScenarioId(modelOldDefault.Id);
                foreach (var item in functionalOld)
                {
                    var model = _functionalRiskRepository.Get(item.Id);
                    Validate.NotNull(model, "Functional Risk tidak ditemukan.");
                    model.Delete(updateBy, updateDate);
                    _functionalRiskRepository.Update(model);
                    //int updatedBy = Convert.ToInt32(updateBy);
                    //DateTime updatedDate = Convert.ToDateTime(updateDate);
                    //_functionalRiskRepository.Delete(model.Id, updatedBy, updatedDate);
                    _databaseContext.SaveChanges();
                }
            }

            var scenario = _scenarioRepository.Get(scenarioNewDefault.Id);
            var matrixAll = _matrixRepository.GetAll().ToList();
            var colorCommentAll = _colorCommentRepository.GetAll().ToList();

            var getByScenarioId = _functionalRiskRepository.GetByScenarioId(scenarioNewDefault.Id).ToList();
            if (getByScenarioId.Count == 0)
            {
                for (int i = 0; i < matrixAll.Count; i++)
                {
                    for (int j = 0; j < colorCommentAll.Count; j++)
                    {
                        var matrix = _matrixRepository.Get(matrixAll[i].Id);
                        var colorComment = _colorCommentRepository.Get(colorCommentAll[j].Id);
                        //var scenario = _scenarioRepository.Get(scenarioId);
                        FunctionalRisk model = new FunctionalRisk(matrix, colorComment, scenario, null, updateBy, updateDate, 0, 0, null);
                        _functionalRiskRepository.Insert(model);
                        _databaseContext.SaveChanges();
                    }
                }
            }
            else
            {
                foreach (var item in getByScenarioId)
                {
                    var model = _functionalRiskRepository.Get(item.Id);
                    model.UnDelete();
                    _functionalRiskRepository.Update(model);
                    _databaseContext.SaveChanges();
                }
            }


        }

        public int Duplicate(ScenarioParam param)
        {

            int id;
            Validate.NotNull(param.NamaScenario, "Nama Scenario is required.");

            var likelihood = _likehoodRepository.Get(param.LikehoodId);
            Validate.NotNull(likelihood, "Likelihood is not found.");

            #region Approval
            Status status = null;
            int nomorUrutStatus = 0;
            //mengecek apakah duplicate dilakukan saat proses approval
            if (param.IsSend == true && param.IsUpdate == null)
            {
                status = _statusRepository.Get(4);
                Validate.NotNull(status, "Status is not found.");
            }

            if (param.IsSend == true && param.IsUpdate == true && param.IsDraftApproval == true)
            {

                status = _statusRepository.Get(4);
                Validate.NotNull(status, "Status is not found.");
            }

            if (param.IsSend == true && param.IsUpdate == true && param.IsDraftApproval == null)
            {
                var userCreate = _userRepository.Get(param.UpdateBy.GetValueOrDefault());
                Validate.NotNull(userCreate, "User is not found.");

                //get role level from user id
                var masterApproval = _masterApprovalScenarioRepository.GetByUserId(userCreate.Id);
                if (masterApproval != null)
                {
                    nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
                }
                if (masterApproval == null && (userCreate.RoleId == 3 || userCreate.RoleId == 4 || userCreate.RoleId == 5))
                {
                    masterApproval = _masterApprovalScenarioRepository.GetByRoleId(userCreate.RoleId);
                    nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
                }
                status = _statusRepository.GetStatus(nomorUrutStatus);

                //status = _statusRepository.Get(1);
                Validate.NotNull(status, "Status is not found.");
            }
            #endregion Approval
            Scenario model;
            if (param.IsUpdate == true)
            {
                model = new Scenario(likelihood, param.NamaScenario, param.UpdateBy, param.UpdateDate, status);
                _scenarioRepository.Insert(model);

            }
            else
            {
                model = new Scenario(likelihood, param.NamaScenario, param.CreateBy, param.CreateDate, status);
                _scenarioRepository.Insert(model);
            }

            var projectAll = _projectRepository.GetAll().ToList();
            IList<ScenarioDetail> scenarioDetailAll = GenerateScenarioDetailAll(projectAll, param, model);
            _scenarioDetailRepository.Insert(scenarioDetailAll);
            _unitOfWork.Commit();

            if (param.IsUpdate == true)
            {
                if (param.IdScenarioLama != null && param.IsScenarioDefault == false)
                {
                    //insert into correlation risk antar sector
                    string cnnString = System.Configuration.ConfigurationManager.ConnectionStrings["HRConnString"].ConnectionString;
                    SqlConnection cnn = new SqlConnection(cnnString);
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.CommandTimeout = 500;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "DuplicateRiskMatrixBasedScenario3";
                    //add any parameters the stored procedure might require
                    List<SqlParameter> prm = new List<SqlParameter>()
                     {
                         new SqlParameter("@scenario1", SqlDbType.Int) {Value = param.IdScenarioLama},
                         new SqlParameter("@scenario2", SqlDbType.Int) {Value = model.Id},
                         new SqlParameter("@createBy", SqlDbType.NVarChar) {Value = param.UpdateBy},
                     };
                    cmd.Parameters.AddRange(prm.ToArray());
                    cnn.Open();
                    object o = cmd.ExecuteScalar();
                    cnn.Close();
                }
                else if (param.IdScenarioLama != null && param.IsScenarioDefault == true)
                {
                    //insert into correlation risk antar sector
                    string cnnString = System.Configuration.ConfigurationManager.ConnectionStrings["HRConnString"].ConnectionString;
                    SqlConnection cnn = new SqlConnection(cnnString);
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.CommandTimeout = 500;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //cmd.CommandText = "DuplicateRiskMatrixBasedScenarioDefault";
                    cmd.CommandText = "DuplicateRiskMatrixBasedScenario3";
                    //add any parameters the stored procedure might require
                    List<SqlParameter> prm = new List<SqlParameter>()
                     {
                         new SqlParameter("@scenario1", SqlDbType.Int) {Value = param.IdScenarioLama},
                         new SqlParameter("@scenario2", SqlDbType.Int) {Value = model.Id},
                         new SqlParameter("@createBy", SqlDbType.NVarChar) {Value = param.UpdateBy},
                     };
                    cmd.Parameters.AddRange(prm.ToArray());
                    cnn.Open();
                    object o = cmd.ExecuteScalar();
                    cnn.Close();
                }
                else
                {
                    //insert into correlation risk antar sector
                    var correlationRiskAntarSektor = GenereateCorrelationRiskAntarSektor(model, scenarioDetailAll, param.UpdateBy, param.UpdateDate);
                    _unitOfWork.Commit();

                    List<int> sektorid = new List<int>();

                    _unitOfWork.Commit();


                    RiskMatrix modelRiskMatrix = new RiskMatrix(model, param.UpdateBy, param.UpdateDate);
                    _riskMatrixRepository.Insert(modelRiskMatrix);
                    _unitOfWork.Commit();

                    var riskMatrix = _riskMatrixRepository.GetByScenarioId(model.Id);
                    Validate.NotNull(riskMatrix, "Risk Matrix is not found.");

                    //setRiskMatrixProject
                    SetRiskMatrixProject(model, riskMatrix, param.UpdateBy, param.UpdateDate);
                    _unitOfWork.Commit();

                    GenerateCorrelatedProject(model, param.UpdateBy, param.UpdateDate);
                    _unitOfWork.Commit();

                    List<int> projectId = new List<int>();
                    //insert into correlated project detail
                    int id2 = modelRiskMatrix.Id;
                }

            }
            else
            {
                //insert into correlation risk antar sector
                var correlationRiskAntarSektor = GenereateCorrelationRiskAntarSektor(model, scenarioDetailAll, param.CreateBy, param.CreateDate);
                _unitOfWork.Commit();

                RiskMatrix modelRiskMatrix = new RiskMatrix(model, param.CreateBy, param.CreateDate);
                _riskMatrixRepository.Insert(modelRiskMatrix);
                _unitOfWork.Commit();

                var riskMatrix = _riskMatrixRepository.GetByScenarioId(model.Id);
                Validate.NotNull(riskMatrix, "Risk Matrix is not found.");

                //setRiskMatrixProject
                SetRiskMatrixProject(model, riskMatrix, param.CreateBy, param.CreateDate);

                //generate correlatedProject
                GenerateCorrelatedProject(model, param.CreateBy, param.CreateDate);
                int id2 = modelRiskMatrix.Id;
                int audit3 = _auditLogService.AddRiskMatrixAudit(model, id2);
            }


            //test get user for approval
            var user = _userRepository.GetAll();

            id = model.Id;

            ////Audit Log ADD 
            //int audit = _auditLogService.AddScenarioAudit(param, id);

            //int audit2 = _auditLogService.AddScenarioDetailAudit(scenarioDetailAll);
            

            //insert into approval
            //if (status != null)
            //{
            //    AddApproval(id, status, model.CreateBy, param.UpdateDate, nomorUrutStatus);
            //}

            _databaseContext.SaveChanges();

            return id;
        }

        public int Update2(int id, ScenarioParam param)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Nama Scenario is not found.");

            var likelihood = _likehoodRepository.Get(param.LikehoodId);
            Validate.NotNull(likelihood, "Likelihood is not found.");

            #region Approval
            bool isSend = param.IsSend.GetValueOrDefault();
            param.IsSend = null;

            Status status = null;
            int nomorUrutStatus = 0;

            //variabel email
            Status statusEmail = null;
            if (param.StatusId != null)
            {
                statusEmail = _statusRepository.Get(param.StatusId.GetValueOrDefault());
                param.StatusId = null;
            }
            int emailDari = param.UpdateBy.GetValueOrDefault();
            int emailKepada = model.CreateBy.GetValueOrDefault();
            List<int> emailCC = new List<int>();

            string typePesan = "";
            string source = "Scenario";
            int requestId = 0;
            string templateNotif = param.TemplateNotif;
            param.TemplateNotif = null;
            string keterangan = param.Keterangan;
            param.Keterangan = null;
            bool isStatusApproval = param.IsStatusApproval.GetValueOrDefault();
            param.IsStatusApproval = null;
            
            if (param.NomorUrutStatus.GetValueOrDefault() != 0)
            {
                nomorUrutStatus = param.NomorUrutStatus.GetValueOrDefault();
                param.NomorUrutStatus = null;
            }
            #endregion Approval

            //int audit = _auditLogService.UpdateScenarioAudit(param, id);

            model.Update(likelihood, param.NamaScenario, param.UpdateBy, param.UpdateDate, status);
            _scenarioRepository.Update(model);

            RemoveOldScenarioDetailUpdate(id, model, param);
            UpdateCorrelatedSektor(model, param.UpdateBy, param.UpdateDate, model.CreateBy);
            _unitOfWork.Commit();

            var riskMatrix = _riskMatrixRepository.GetByScenarioId(id);
            GenerateRiskMatrixProject(model, riskMatrix, param.UpdateBy, param.UpdateDate);

            UpdateCorrelatedProject(model, param.UpdateBy, param.UpdateDate);

            _unitOfWork.Commit();

            int id2 = riskMatrix.Id;
            int audit3 = _auditLogService.AddRiskMatrixAudit(model, id2);

            var scenarioSameName = _scenarioRepository.GetAllByName(model.Id);
            var otherScenarioId = 0;

            List<int> otherScenarioIdStatusTrue = new List<int>();

            bool isFirst = true;
            foreach (var item in scenarioSameName)
            {
                if (item.StatusId == 2 && item.Id != model.Id)
                {
                    isFirst = false;
                }
            }
            var scenarioSameName2 = _scenarioRepository.GetAllByName(model.Id);
            if (isFirst)
            {
                foreach (var item in scenarioSameName2)
                {
                    if (item.StatusId == 1)
                    {
                        otherScenarioId = item.Id;
                    }
                }
            }
            else
            {
                foreach (var item in scenarioSameName2)
                {
                    if (item.StatusId == 2 && item.Id != model.Id)
                    {
                        otherScenarioIdStatusTrue.Add(item.Id);
                    }
                    if (item.StatusId == 1 && item.Id != model.Id && model.CreateBy == item.CreateBy)
                    {
                        otherScenarioId = item.Id;
                        item.Delete(param.UpdateBy.GetValueOrDefault(), param.UpdateDate.GetValueOrDefault());
                    }
                    if (item.StatusId == 1 && item.Id != model.Id && model.CreateBy != item.CreateBy)
                    {
                        item.Delete(param.UpdateBy.GetValueOrDefault(), param.UpdateDate.GetValueOrDefault());
                    }

                }
            }

                if (otherScenarioIdStatusTrue.Count() > 0)
                {
                    foreach (var item in otherScenarioIdStatusTrue)
                    {
                        var scenarioLamaCheckDefault = _scenarioRepository.Get(item);
                        if (scenarioLamaCheckDefault.IsDefault == true)
                        {
                            model.IsDefault = true;
                            scenarioLamaCheckDefault.IsDefault = false;
                            
                            
                    }
                    var scenarioCalculation = _scenarioCalculationRepository.GetByScenarioId(scenarioLamaCheckDefault.Id);

                    SetStatusRiskMatrix(model, scenarioLamaCheckDefault, param.UpdateBy, param.UpdateDate);
                    //riskMatrix.Delete(updateBy, updateDate);

                    //set scenarioId in CorrelatedSektor
                    // TEMPORARY COMMENT BY KIKIW
                    SetScenarioInFunctionalRisk(model, scenarioLamaCheckDefault, param.UpdateBy, param.UpdateDate);
                    if (scenarioCalculation != null)
                    {
                        ScenarioCalculation scenarioCalculationNew = new ScenarioCalculation(model);
                        _scenarioCalculationRepository.Insert(scenarioCalculationNew);
                        _databaseContext.SaveChanges();
                        var listProjectNewScenario = _scenarioDetailRepository.GetByScenarioId(model.Id).Select(x => x.ProjectId);
                        var scenarioProjectCalculation = _projectCalculationRepository.GetByScenarioCalculationId(scenarioCalculation.Id);
                        if (scenarioProjectCalculation != null)
                        {
                            foreach (var itemProject in scenarioProjectCalculation)
                            {
                                if (listProjectNewScenario.Contains(itemProject.ProjectId))
                                {
                                    itemProject.UpdateScenarioCalculationId(scenarioCalculationNew.Id);
                                    _projectCalculationRepository.Update(itemProject);
                                }
                                else
                                {
                                    _projectCalculationRepository.Delete(itemProject);
                                }
                            }
                        }
                        _scenarioCalculationRepository.Delete(scenarioCalculation);

                        
                    }
                    scenarioLamaCheckDefault.Delete(param.UpdateBy.GetValueOrDefault(), param.UpdateDate.GetValueOrDefault());

                    var CorrelatedSectorScenarioLamaDelete = _correlatedSectorRepository.GetByScenarioIdAll(scenarioLamaCheckDefault.Id);
                    foreach (var correlatedSectorDelete in CorrelatedSectorScenarioLamaDelete)
                    {
                        correlatedSectorDelete.Delete(param.UpdateBy.GetValueOrDefault(), param.UpdateDate.GetValueOrDefault());
                    }
                    var CorrelatedProjectScenarioLamaDelete = _correlatedProjectRepository.GetByScenarioId(scenarioLamaCheckDefault.Id, null, 0);
                    foreach (var correlatedProjectDelete in CorrelatedProjectScenarioLamaDelete)
                    {
                        correlatedProjectDelete.Delete(param.UpdateBy.GetValueOrDefault(), param.UpdateDate.GetValueOrDefault());
                    }
                    var RiskMatrixProjectScenarioLamaDelete = _riskMatrixProjectRepository.GetAllByScenarioId(scenarioLamaCheckDefault.Id);
                    foreach (var riskMatrixProjectDelete in RiskMatrixProjectScenarioLamaDelete)
                    {
                        riskMatrixProjectDelete.Delete(param.UpdateBy.GetValueOrDefault(), param.UpdateDate.GetValueOrDefault());
                    }
                    _databaseContext.SaveChanges();
                }
                }

                var RiskMatrixProjectScenarioBaru = _riskMatrixProjectRepository.GetAllByScenarioId(model.Id).Where( x => x.IsDelete == false);
                var RiskMatrixProjectScenarioLama = _riskMatrixProjectRepository.GetAllByScenarioId(otherScenarioId);
                var CorrelatedProjectScenarioBaru = _correlatedProjectRepository.GetByScenarioId(model.Id, null, 0);
                var CorrelatedProjectScenarioLama = _correlatedProjectRepository.GetByScenarioId(otherScenarioId, null, 0);
                var CorrelatedSectorScenarioBaru = _correlatedSectorRepository.GetByScenarioIdAll(model.Id);
                var CorrelatedSectorScenarioLama = _correlatedSectorRepository.GetByScenarioIdAll(otherScenarioId);
                var CorrelatedSectorNeedDelete = _correlatedSectorRepository.GetByScenarioIdAll(0);
                var MatrixProjectCount = RiskMatrixProjectScenarioLama.Count();
                
                if (MatrixProjectCount != 0)
                {
                    foreach (var items1 in RiskMatrixProjectScenarioBaru.ToList())
                    {
                        foreach (var items2 in RiskMatrixProjectScenarioLama.ToList())
                        {
                            if (items1.ProjectId == items2.ProjectId)
                            {
                                items2.RiskMatrixId = items1.RiskMatrixId;
                                items2.ScenarioId = items1.ScenarioId;
                                _riskMatrixProjectRepository.Delete(items1.Id);
                                _unitOfWork.Commit();
                            }
                        }
                    }
                    foreach (var items1 in CorrelatedProjectScenarioBaru.ToList())
                    {
                        foreach (var items2 in CorrelatedProjectScenarioLama.ToList())
                        {
                            if (items1.ProjectId == items2.ProjectId)
                            {
                                items2.ScenarioId = items1.ScenarioId;
                                _correlatedProjectRepository.Delete(items1.Id);
                                _unitOfWork.Commit();
                            }
                        }
                    }
                    foreach (var items1 in CorrelatedSectorScenarioBaru.ToList())
                    {
                        foreach (var items2 in CorrelatedSectorScenarioLama.ToList())
                        {
                            if (items1.SektorId == items2.SektorId)
                            {
                                items2.ScenarioId = items1.ScenarioId;
                                _correlatedSectorRepository.Delete2(items1.Id);
                                _unitOfWork.Commit();
                            }
                        }
                    }
                    foreach (var items1 in CorrelatedSectorNeedDelete.ToList())
                    {
                        _correlatedSectorRepository.Delete2(items1.Id);
                        _unitOfWork.Commit();
                    }
                    _databaseContext.SaveChanges();
                }
            if (otherScenarioId != 0)
            {
                Delete2(otherScenarioId, Convert.ToInt32(param.UpdateBy), Convert.ToDateTime(param.UpdateDate));
            }
            
            foreach (var item in scenarioSameName)
            {
                if (item.StatusId == 1)
                {
                    Delete2(item.Id, Convert.ToInt32(param.UpdateBy), Convert.ToDateTime(param.UpdateDate));
                }
            }
            _databaseContext.SaveChanges();
            #region email
            //untuk notifikasi email approve
            if (isSend == false && isStatusApproval == true)
            {

                if (nomorUrutStatus == 3)
                {
                    var master = _masterApprovalScenarioRepository.GetAll();
                    Validate.NotNull(master, "Master Approval Scenario is not found.");

                    foreach (var item in master)
                    {
                        emailCC.Add(item.UserId.GetValueOrDefault());
                    }
                    //
                    if (!emailCC.Contains(emailKepada))
                    {
                        emailCC.Add(emailKepada);
                    }


                    //
                    typePesan = statusEmail.StatusDescription; ;

                    //
                    requestId = model.Id;

                    //source
                    //code notifikasi email
                    //disini emailCC sebagai emailKepada
                    _emailService.NotifyResetPassword(source, typePesan, requestId, emailCC, emailDari, templateNotif);
                }
            }
            #endregion email
            id = model.Id;

            return id;
        }

        //Validate Correlated Project tidak sedang dalam proses approval
        private void ValidateCorrelatedProject(Scenario model, ScenarioParam param)
        {
            //Cara mengupdate IsDelete = true pada table 
            var correlatedProject = _correlatedProjectRepository.GetByScenarioIdIsDeleteFalse(model.Id).ToList();

            List<int> correlatedProjectWithStatus = new List<int>();

            foreach (var item in correlatedProject)
            {
                if (item.StatusId == 1 || item.StatusId == 2)
                {
                    correlatedProjectWithStatus.Add(item.ProjectId);
                }
            }

            List<int> correlatedProjectWithStatusNew = new List<int>();

            foreach (var item in param.ProjectId)
            {
                correlatedProjectWithStatusNew.Add(Convert.ToInt32(item));
            }

            if (correlatedProjectWithStatus.Count > 0)
            {
                var checkStatusCorrelatedProject = correlatedProjectWithStatus.Except(correlatedProjectWithStatusNew);
                if (checkStatusCorrelatedProject.Count() > 0)
                {
                    throw new ApplicationException(string.Format("Correlated Project tidak bisa diubah karena dalam proses approval"));
                }
            }

        }

        //Validate Correlated Project tidak sedang dalam proses approval
        private void ValidateRiskMatrixProject(Scenario model, ScenarioParam param)
        {
            //Cara mengupdate IsDelete = true pada table 
            var riskMatrixProject = _riskMatrixProjectRepository.GetByScenarioId(model.Id).ToList();

            List<int> riskMatrixProjectWithStatus = new List<int>();

            foreach (var item in riskMatrixProject)
            {
                if (item.StatusId == 1 || item.StatusId == 2)
                {
                    riskMatrixProjectWithStatus.Add(item.ProjectId);
                }
            }

            List<int> riskMatrixProjectWithStatusNew = new List<int>();

            foreach (var item in param.ProjectId)
            {
                riskMatrixProjectWithStatusNew.Add(Convert.ToInt32(item));
            }

            if (riskMatrixProjectWithStatus.Count > 0)
            {
                var checkStatusRiskMatrixProject = riskMatrixProjectWithStatus.Except(riskMatrixProjectWithStatusNew);
                if (checkStatusRiskMatrixProject.Count() > 0)
                {
                    throw new ApplicationException(string.Format("Risk Matrix Project tidak bisa diubah karena dalam proses approval"));
                }
            }

        }

        private void ValidateCorrelatedSektor(Scenario model, ScenarioParam param)
        {
            var correlatedSektor = _correlatedSectorRepository.GetByScenarioIdIsDeleteFalse(model.Id).ToList();

            List<int> correlatedSektorWithStatus = new List<int>();

            List<int> listSektor = new List<int>();

            foreach (var item in correlatedSektor)
            {
                if (item.StatusId == 1 || item.StatusId == 2)
                {
                    correlatedSektorWithStatus.Add(item.SektorId);
                }
            }

            List<int> riskMatrixProjectWithStatusNew = new List<int>();

            foreach (var item in param.ProjectId)
            {
                var project = _projectRepository.Get(Convert.ToInt32(item));
                if (!listSektor.Contains(project.SektorId))
                {
                    listSektor.Add(project.SektorId);

                }
            }

            if (correlatedSektorWithStatus.Count > 0)
            {
                var checkStatusCorrelatedSektor = correlatedSektorWithStatus.Except(listSektor);
                if (checkStatusCorrelatedSektor.Count() > 0)
                {
                    throw new ApplicationException(string.Format("Correlated Sektor tidak bisa diubah karena dalam proses approval"));
                }
            }
        }
        
        public int UpdateScenarioMurni(int id, ScenarioParam param)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Nama Scenario is not found.");

            var likelihood = _likehoodRepository.Get(param.LikehoodId);
            Validate.NotNull(likelihood, "Likelihood is not found.");

            model.Update(likelihood, param.NamaScenario, param.UpdateBy, param.UpdateDate);
            _scenarioRepository.Update(model);

            RemoveOldScenarioDetailUpdate(id, model, param);
            UpdateCorrelatedSektor(model, param.UpdateBy, param.UpdateDate, param.CreateBy);
            _databaseContext.SaveChanges();

            var riskMatrix = _riskMatrixRepository.GetByScenarioId(id);
            GenerateRiskMatrixProject(model, riskMatrix, param.UpdateBy, param.UpdateDate);

            //Update table CorrelatedProject
            UpdateCorrelatedProject(model, param.UpdateBy, param.UpdateDate);
            _databaseContext.SaveChanges();
            id = model.Id;

            return id;
        }


        #endregion Private
    }
}

