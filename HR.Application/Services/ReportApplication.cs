﻿using System;
using System.Drawing;
using HR.Domain;
using System.Collections.Generic;
using System.Linq;
using HR.Common;
using HR.Application.Params;
using HR.Common.Validation;
using HR.Infrastructure;
using ClosedXML.Excel;
using System.Web;
using System.IO;
using Spire.Xls;
using Spire.Xls.Charts;


namespace HR.Application
{
    public class ReportService : IReportService
    {

        private readonly IDatabaseContext _iDatabaseContext;
        private readonly IStageTahunRiskMatrixRepository _stageTahunRiskMatrixRepository;
        private readonly IStageTahunRiskMatrixDetailRepository _stageTahunRiskMatrixDetailRepository;
        private readonly ILikehoodDetailRepository _likehoodDetailRepository;
        private readonly IScenarioRepository _scenarioRepository;
        private readonly IScenarioDetailRepository _scenarioDetailRepository;
        private readonly ICorrelatedSektorDetailRepository _correlatedSektorDetailRepository;
        private readonly IAssetDataRepository _assetDataRepository;
        private readonly IPMNRepository _pmnRepository;

        public ReportService(IDatabaseContext iDatabaseContext, IStageTahunRiskMatrixRepository stageTahunRiskMatrixRepository, IStageTahunRiskMatrixDetailRepository stageTahunRiskMatrixDetailRepository,
            ILikehoodDetailRepository likehoodDetailRepository, IScenarioRepository scenarioRepository, IScenarioDetailRepository scenarioDetailRepository, ICorrelatedSektorDetailRepository correlatedSektorDetailRepository,
            IAssetDataRepository assetDataRepository, IPMNRepository pmnRepository)
        {
            _iDatabaseContext = iDatabaseContext;
            _stageTahunRiskMatrixRepository = stageTahunRiskMatrixRepository;
            _stageTahunRiskMatrixDetailRepository = stageTahunRiskMatrixDetailRepository;
            _likehoodDetailRepository = likehoodDetailRepository;
            _scenarioRepository = scenarioRepository;
            _scenarioDetailRepository = scenarioDetailRepository;
            _correlatedSektorDetailRepository = correlatedSektorDetailRepository;
            _assetDataRepository = assetDataRepository;
            _pmnRepository = pmnRepository;
        }

        #region Reporting

        public void CalculationReport(Calculation calculation)
        {
            var scenarioName = calculation.CalculationResult.NamaScenario;
            var scenarioId = calculation.CalculationResult.ScenarioId;
            var scenario = _scenarioRepository.Get(scenarioId);
            var likelihoodDetail = _likehoodDetailRepository.GetByLikehoodId(scenario.LikehoodId).ToList();
            var proyek = _scenarioDetailRepository.GetByScenarioId(scenarioId).ToList();
            var assetData = _assetDataRepository.GetAll().ToList();
            var pmn = _pmnRepository.GetAll().ToList();
            var currentPMN = pmn.Where(x => x.PMNToModalDasarCap == DateTime.Now.Year).FirstOrDefault();
            var years = calculation.CalculationResult.CollectionYears;

            IList<Sektor> sektors = new List<Sektor>();
            foreach (var item in proyek)
            {
                sektors.Add(item.Project.Sektor);
            }
            var sektor = sektors.Distinct().ToList();

            IList<RiskRegistrasi> riskRegistrasis = new List<RiskRegistrasi>();
            foreach (var item in proyek)
            {
                foreach (var risk in item.Project.ProjectRiskRegistrasi)
                {
                    riskRegistrasis.Add(risk.RiskRegistrasi);
                }
            }
            var riskRegistrasi = riskRegistrasis.Distinct().ToList();


            var workbook = new XLWorkbook();
            workbook.Theme.Background1 = XLColor.White;
            workbook.Theme.Text1 = XLColor.Black;
            workbook.Theme.Background2 = XLColor.Gray;
            workbook.Theme.Text2 = XLColor.BlueGray;
            workbook.Theme.Accent1 = XLColor.Blue;
            workbook.Theme.Accent2 = XLColor.Orange;
            workbook.Theme.Accent3 = XLColor.Gray;
            workbook.Theme.Accent4 = XLColor.Gold;
            workbook.Theme.Accent5 = XLColor.Blue;
            workbook.Theme.Accent6 = XLColor.Green;

            #region DEACTIVE
            #region 0.Inputan
            var worksheet0 = workbook.Worksheets.Add("0.Inputan");


            worksheet0.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            worksheet0.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            worksheet0.Cell("A1").Value = "Inputs";
            worksheet0.Range("A1", "I1").Style.Fill.BackgroundColor = XLColor.FromArgb(84, 130, 53);
            worksheet0.Cell("A1").Style.Font.FontColor = XLColor.FromArgb(230, 230, 230);
            worksheet0.Range("A2", "G2").Style.Fill.BackgroundColor = XLColor.FromArgb(84, 130, 53);
            worksheet0.Cell("A2").Value = "Input data in yellow cells\n\nSummarize the data of Project Risk Likelihood and Company Asset Mix";
            worksheet0.Cell("A2").Style.Fill.BackgroundColor = XLColor.FromArgb(242, 242, 242);
            worksheet0.Range("A2", "G2").Merge().Style.Alignment.WrapText = true;
            worksheet0.Range("A2", "G2").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            worksheet0.Row(2).Height = 40.25;
            worksheet0.Cell("A5").Value = "in Billion IDR";
            worksheet0.Cell("A5").Style.Font.Italic = true;
            worksheet0.Cell("A6").Value = "I. LIKELIHOOD/PARAMETER  ";
            worksheet0.Range("A6", "I6").Style.Fill.BackgroundColor = XLColor.FromArgb(252, 228, 214);
            worksheet0.Range("A1", "A6").Style.Font.Bold = true;
            worksheet0.Range("A1", "A6").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;

            worksheet0.Cell("A7").Value = scenarioName;
            worksheet0.Range("A7", "I7").Style.Fill.BackgroundColor = XLColor.FromArgb(244, 176, 132);
            worksheet0.Range("A7", "E7").Merge();
            worksheet0.Cell("B9").Value = "Definition for likelihood is given here for:\n- average scenario\n- sensitivity testing\n- stress testing";
            worksheet0.Cell("B9").Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);
            worksheet0.Cell("B9").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet0.Range("B9", "B10").Merge().Style.Alignment.WrapText = true;
            worksheet0.Cell("C9").Value = scenarioName;
            worksheet0.Cell("C9").Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);
            worksheet0.Range("C9", "D9").Merge();
            worksheet0.Cell("E9").Value = "Used Scenario";
            worksheet0.Cell("E9").Style.Fill.BackgroundColor = XLColor.FromArgb(252, 228, 214);
            worksheet0.Cell("C10").Value = "Lower";
            worksheet0.Cell("C10").Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);
            worksheet0.Cell("D10").Value = "Upper";
            worksheet0.Cell("D10").Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);
            worksheet0.Cell("E10").Value = "Average";
            worksheet0.Cell("E10").Style.Fill.BackgroundColor = XLColor.FromArgb(252, 228, 214);
            for (int i = 0; i < likelihoodDetail.Count(); i++)
            {
                worksheet0.Cell(11 + i, 2).Value = likelihoodDetail[i].DefinisiLikehood;
                worksheet0.Cell(11 + i, 3).Value = likelihoodDetail[i].Lower;
                worksheet0.Cell(11 + i, 4).Value = likelihoodDetail[i].Upper;
                worksheet0.Cell(11 + i, 5).Value = likelihoodDetail[i].Average;
            }
            worksheet0.Range("B9", "E15").Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet0.Range("B9", "E15").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

            worksheet0.Cell("A18").Value = "Proyek List";
            worksheet0.Range("A18", "I18").Style.Fill.BackgroundColor = XLColor.FromArgb(244, 176, 132);
            worksheet0.Range("A18", "E18").Merge();
            worksheet0.Cell("B20").Value = "NO";
            worksheet0.Range("B20", "B21").Merge();
            worksheet0.Cell("C20").Value = "Nama Proyek";
            worksheet0.Range("C20", "C21").Merge();
            worksheet0.Cell("D20").Value = "Sektor / Proyek Type";
            worksheet0.Range("D20", "D21").Merge();
            worksheet0.Cell("E20").Value = "Risiko Budget";
            worksheet0.Cell("E21").Value = "Minimum";
            worksheet0.Cell("F21").Value = "Maksimum";
            worksheet0.Range("B20", "F21").Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);
            for (int i = 0; i < proyek.Count(); i++)
            {
                worksheet0.Cell(22 + i, 2).Value = i + 1;
                worksheet0.Cell(22 + i, 3).Value = proyek[i].Project.NamaProject;
                worksheet0.Cell(22 + i, 4).Value = proyek[i].Project.Sektor.NamaSektor;
                worksheet0.Cell(22 + i, 5).Value = proyek[i].Project.Sektor.Minimum;
                worksheet0.Cell(22 + i, 6).Value = proyek[i].Project.Sektor.Maximum;

            }
            worksheet0.Range(22, 2, 21 + proyek.Count(), 4).Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 204);
            worksheet0.Range(20, 2, 21 + proyek.Count(), 6).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet0.Range(20, 2, 21 + proyek.Count(), 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

            worksheet0.Cell("H18").Value = "Sektor";
            worksheet0.Range("H18", "K18").Style.Fill.BackgroundColor = XLColor.FromArgb(244, 176, 132);
            worksheet0.Range("H18", "K18").Merge();
            worksheet0.Cell("H20").Value = "NO";
            worksheet0.Range("H20", "H21").Merge();
            worksheet0.Cell("I20").Value = "Nama Sektor";
            worksheet0.Range("I20", "I21").Merge();
            worksheet0.Cell("J20").Value = "Risiko Budget";
            worksheet0.Range("J20", "K20").Merge();
            worksheet0.Cell("J21").Value = "Minimum";
            worksheet0.Cell("K21").Value = "Maksimum";
            worksheet0.Range("H20", "K21").Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);
            for (int i = 0; i < sektor.Count(); i++)
            {
                worksheet0.Cell(22 + i, 8).Value = i + 1;
                worksheet0.Cell(22 + i, 9).Value = sektor[i].NamaSektor;
                worksheet0.Cell(22 + i, 10).Value = sektor[i].Minimum;
                worksheet0.Cell(22 + i, 11).Value = sektor[i].Maximum;
            }
            worksheet0.Range(22, 8, 21 + sektor.Count(), 9).Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 204);
            worksheet0.Range(20, 8, 21 + sektor.Count(), 11).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet0.Range(20, 8, 21 + sektor.Count(), 11).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

            worksheet0.Cell(proyek.Count() + 2 + 22, "A").Value = "Risk Registrasi";
            worksheet0.Range(proyek.Count() + 2 + 22, 1, proyek.Count() + 2 + 22, 9).Style.Fill.BackgroundColor = XLColor.FromArgb(244, 176, 132);
            worksheet0.Range(proyek.Count() + 2 + 22, 1, proyek.Count() + 2 + 22, 5).Merge();
            worksheet0.Cell(proyek.Count() + 4 + 22, "B").Value = "NO";
            worksheet0.Range(proyek.Count() + 4 + 22, 2, proyek.Count() + 4 + 22 + 1, 2).Merge();
            worksheet0.Cell(proyek.Count() + 4 + 22, "C").Value = "Risk Registrasi";
            worksheet0.Range(proyek.Count() + 4 + 22, 3, proyek.Count() + 4 + 22 + 1, 3).Merge();
            worksheet0.Cell(proyek.Count() + 4 + 22, "D").Value = "Risiko Budget";
            worksheet0.Range(proyek.Count() + 4 + 22, 4, proyek.Count() + 4 + 22, 5).Merge();
            worksheet0.Cell(proyek.Count() + 5 + 22, "D").Value = "Minimum";
            worksheet0.Cell(proyek.Count() + 5 + 22, "E").Value = "Maksimum";
            worksheet0.Range(proyek.Count() + 4 + 22, 2, proyek.Count() + 5 + 22, 5).Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);
            for (int i = 0; i < riskRegistrasi.Count(); i++)
            {
                worksheet0.Cell(proyek.Count() + 6 + 22 + i, 2).Value = i + 1;
                worksheet0.Cell(proyek.Count() + 6 + 22 + i, 3).Value = riskRegistrasis[i].KodeMRisk;
                worksheet0.Cell(proyek.Count() + 6 + 22 + i, 4).Value = riskRegistrasis[i].Minimum;
                worksheet0.Cell(proyek.Count() + 6 + 22 + i, 5).Value = riskRegistrasis[i].Maximum;
            }
            worksheet0.Range(proyek.Count() + 6 + 22, 2, proyek.Count() + 5 + 22 + riskRegistrasi.Count(), 3).Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 204);
            worksheet0.Range(proyek.Count() + 4 + 22, 2, proyek.Count() + 5 + 22 + riskRegistrasi.Count(), 5).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet0.Range(proyek.Count() + 4 + 22, 2, proyek.Count() + 5 + 22 + riskRegistrasi.Count(), 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 2, "A").Value = "Asset Data";
            worksheet0.Range(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 2, 1, riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 2, 10).Style.Fill.BackgroundColor = XLColor.FromArgb(244, 176, 132);
            worksheet0.Range(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 2, 1, riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 2, 5).Merge();
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4, "B").Value = "Asset Class";
            worksheet0.Row(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4).Height = 33.75;
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4, "C").Value = "Term";
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4, "D").Value = "Assumed Return";
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4, "E").Value = "Outstanding years";
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4, "F").Value = "Asset value";
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4, "G").Value = "Propotion";
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4, "H").Value = "Assusmed Return-Percentage";
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4, "I").Value = "Assumed Return";
            worksheet0.Range(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4, 2, riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4, 9).Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);
            for (int i = 0; i < assetData.Count(); i++)
            {
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 5 + i, 2).Value = assetData[i].AssetClass;
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 5 + i, 3).Value = assetData[i].TermAwal + "-" + assetData[i].TermAkhir;
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 5 + i, 4).Value = assetData[i].AssumedReturn + "%";
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 5 + i, 5).Value = assetData[i].OutstandingStartYears + "-" + assetData[i].OutstandingEndYears;
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 5 + i, 6).Value = assetData[i].AssetValue;
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 5 + i, 7).Value = assetData[i].Porpotion;
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 5 + i, 8).Value = assetData[i].AssumedReturnPercentage + "%";
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 5 + i, 9).Value = assetData[i].AssumedReturn + "%";
            }
            worksheet0.Range(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 5, 2, riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4 + assetData.Count(), 9).Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 204);
            worksheet0.Range(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4, 2, riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4 + assetData.Count(), 9).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet0.Range(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4, 2, riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4 + assetData.Count(), 9).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4, "K").Value = "PMN To Modal Dasar Capital";
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4, "K").Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);
            worksheet0.Range(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4, 11, riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4, 12).Merge();
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 5, "K").Value = DateTime.Now.Year - 1;
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 5, "K").Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 5, "L").Value = DateTime.Now.Year;
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 5, "L").Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 7, "K").Value = "Recourse Delay";
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 7, "K").Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 7, "L").Value = "Delay Years";
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 7, "L").Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 9, "K").Value = "Opex Growth";
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 9, "K").Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 10, "K").Value = "Opex" + DateTime.Now.Year;
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 10, "K").Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);
            decimal value = 0;

            foreach (var item in pmn)
            {
                if (item.PMNToModalDasarCap == DateTime.Now.Year - 1)
                {
                    value = item.ValuePMNToModalDasarCap;
                }
            }

            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 6, "K").Value = value;
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 6, "K").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 204);
            value = 0;
            foreach (var item in pmn)
            {
                if (item.PMNToModalDasarCap == DateTime.Now.Year)
                {
                    value = item.ValuePMNToModalDasarCap;
                }
            }
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 6, "L").Value = value;
            worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 6, "L").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 204);

            if (currentPMN != null)
            {
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 8, "K").Value = currentPMN.RecourseDelay;
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 8, "K").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 204);
                worksheet0.Range(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 8, 11, riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 8, 12).Merge();
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 8, "L").Value = currentPMN.DelayYears;
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 8, "L").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 204);
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 9, "L").Value = currentPMN.OpexGrowth;
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 9, "L").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 204);
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 10, "L").Value = currentPMN.Opex;
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 10, "L").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 204);
            }
            else
            {
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 8, "K").Value = "0";
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 8, "K").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 204);
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 8, "L").Value = "0";
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 8, "L").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 204);
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 9, "L").Value = "0";
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 9, "L").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 204);
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 10, "L").Value = "0";
                worksheet0.Cell(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 10, "L").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 204);
            }
            worksheet0.Range(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4, 11, riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4 + 6, 12).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet0.Range(riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4, 11, riskRegistrasi.Count() + proyek.Count() + 8 + 22 + 4 + 6, 12).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;


            #region Setting Row Height & Column Width
            worksheet0.Column(1).Width = 8.43;
            worksheet0.Column(2).Width = 37.57;
            worksheet0.Column(3).Width = 31.43;
            worksheet0.Column(4).Width = 34;
            worksheet0.Column(5).Width = 16.57;
            worksheet0.Column(6).Width = 12.14;
            worksheet0.Column(7).Width = 11.29;
            worksheet0.Column(14).Width = 20.29;
            worksheet0.Column(15).Width = 15;
            worksheet0.Column(16).Width = 15.71;

            worksheet0.Row(9).Height = 27.75;
            worksheet0.Row(10).Height = 33.75;
            #endregion
            worksheet0.Columns(4, 12).AdjustToContents();
            //worksheet0.Row(2).AdjustToContents();

            worksheet0.Dispose();
            #endregion 0.Inputan

            #region 1.Risk Registrasi
            var subRiskReport = _iDatabaseContext.SubRiskRegistrasis.Where(x => x.IsDelete == false).OrderBy(x => x.KodeRisk).ToList();
            var worksheet1 = workbook.Worksheets.Add("1.Risk Registrasi");

            worksheet1.Column(1).Width = 3;
            worksheet1.Cell("A1").Value = "Risks Mapping to the RISK REGISTER";
            worksheet1.Cell("B2").Value = "All the risks covered by IIGF should be mapped to the RISK REGISTER below";

            worksheet1.Cell("B7").Value = "Risk ID";
            worksheet1.Cell("C7").Value = "Risk Categories";
            worksheet1.Cell("C8").Value = "In line with the RISK REGISTER";
            worksheet1.Cell("C9").Value = "Risk Category";
            worksheet1.Cell("D9").Value = "Definition";
            worksheet1.Cell("E7").Value = "Risk Event Lead to Claim";
            worksheet1.Cell("F7").Value = "Description of Risk Event Lead to Claim";
            worksheet1.Cell("G7").Value = "Suggested Mitigation";

            #region settingAtas
            worksheet1.Cell("B7").Style.Alignment.WrapText = true;
            worksheet1.Cell("B7").Style.Font.FontSize = 13;
            worksheet1.Range("B7:B9").Column(1).Merge();
            worksheet1.Cell("C7").Style.Font.FontSize = 13;
            worksheet1.Range("C7:D7").Merge();
            worksheet1.Cell("C8").Style.Font.FontSize = 10;
            worksheet1.Range("C8:D8").Merge();
            worksheet1.Cell("C9").Style.Font.FontSize = 10;
            worksheet1.Cell("C9").Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
            worksheet1.Cell("D9").Style.Font.FontSize = 10;
            worksheet1.Cell("D9").Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
            worksheet1.Range("C7:D9").Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
            worksheet1.Cell("E7").Style.Font.FontSize = 13;
            worksheet1.Range("E7:E9").Column(1).Merge();
            worksheet1.Cell("F7").Style.Font.FontSize = 13;
            worksheet1.Range("F7:F9").Column(1).Merge();
            worksheet1.Range("F7:F9").Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
            worksheet1.Cell("G7").Style.Font.FontSize = 13;
            worksheet1.Range("G7:G9").Column(1).Merge();

            worksheet1.Range(1, 1, 1, 7).Merge();
            worksheet1.Range(2, 2, 4, 5).Merge();
            worksheet1.Range(1, 1, 1, 7).Style.Fill.BackgroundColor = XLColor.FromArgb(244, 176, 132);
            worksheet1.Range(1, 1, 1, 7).Style.Font.Bold = true;
            worksheet1.Range(1, 1, 1, 7).Style.Font.FontSize = 16;
            worksheet1.Range(2, 2, 4, 5).Style.Fill.BackgroundColor = XLColor.FromArgb(242, 242, 242);
            worksheet1.Row(1).Height = 29;
            worksheet1.Row(2).Height = 12;
            worksheet1.Row(3).Height = 12;
            worksheet1.Row(4).Height = 12;
            #endregion settingAtas           

            int BatasValue = 9 + subRiskReport.Count();

            worksheet1.Row(9).Height = 22;
            for (int i = 10; i <= BatasValue; i++)
            {
                worksheet1.Row(i).Height = 34;
            }

            for (int i = 0; i < subRiskReport.Count(); i++)
            {
                worksheet1.Cell(10 + i, 2).Value = subRiskReport[i].KodeRisk;
                worksheet1.Cell(10 + i, 2).Style.Alignment.WrapText = true;
                worksheet1.Cell(10 + i, 5).Value = subRiskReport[i].RiskEvenClaim;
                worksheet1.Cell(10 + i, 5).Style.Alignment.WrapText = true;
                worksheet1.Cell(10 + i, 6).Value = subRiskReport[i].DescriptionRiskEvenClaim;
                worksheet1.Cell(10 + i, 6).Style.Alignment.WrapText = true;
                worksheet1.Cell(10 + i, 7).Value = subRiskReport[i].SugestionMigration;
                worksheet1.Cell(10 + i, 7).Style.Alignment.WrapText = true;
            }

            var subRiskReportGroup = subRiskReport.GroupBy(x => x.RiskRegistrasiId).ToList();
            var riskRegistrasiRep = _iDatabaseContext.RiskRegistrasis.Where(x => x.IsDelete == false).OrderBy(x => x.KodeMRisk).ToList();
            var k = 0;

            for (int i = 0; i < subRiskReportGroup.Count(); i++)
            {
                var j = 0;
                j += subRiskReportGroup[i].Count();
                var rangeA = worksheet1.Range(10 + k, 3, 10 + k + j - 1, 3);
                rangeA.Cell(1, 1).Value = riskRegistrasiRep[i].NamaCategoryRisk;
                rangeA.Merge();
                rangeA.Style.Alignment.WrapText = true;

                var rangeB = worksheet1.Range(10 + k, 4, 10 + k + j - 1, 4);
                rangeB.Cell(1, 1).Value = riskRegistrasiRep[i].Definisi;
                rangeB.Merge();
                rangeB.Style.Alignment.WrapText = true;

                worksheet1.Range(10 + k, 2, 10 + k + j - 1, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
                k = k + j;
            }

            #region setting
            worksheet1.Range(7, 2, 9, 7).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            worksheet1.Range(7, 2, 9, 7).Style.Fill.BackgroundColor = XLColor.FromArgb(91, 155, 213);
            worksheet1.Range(7, 2, 9, 7).Style.Font.FontColor = XLColor.GhostWhite;
            worksheet1.Range(7, 2, BatasValue, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
            worksheet1.Range(10, 3, BatasValue, 7).Style.Font.FontSize = 9;
            worksheet1.Range(10, 3, BatasValue, 3).Style.Font.Bold = true;
            worksheet1.Column(2).Width = 5;
            worksheet1.Column(3).Width = 21;
            worksheet1.Column(4).Width = 21;
            worksheet1.Column(5).Width = 35;
            worksheet1.Column(6).Width = 45;
            worksheet1.Column(7).Width = 45;
            worksheet1.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            #endregion setting

            #endregion 1.Risk Registrasi

            #region 2.Risk Matrix
            var scenarioCorrelationDefault = _iDatabaseContext.Scenarios.Where(x => x.IsDelete == false && x.IsDefault == true).SingleOrDefault();
            var riskMatrixProjectReport = _iDatabaseContext.RiskMatrixProjects.Where(x => x.IsDelete == false && x.ScenarioId == scenarioCorrelationDefault.Id && x.StatusId == 2).ToList();
            var pcCnOp = _iDatabaseContext.Stages.Where(x => x.IsDelete == false).ToList();
            var worksheet2 = workbook.Worksheets.Add("2.Risk Matrix");
            worksheet2.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            worksheet2.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            var batasBawahRiskMatrix = riskRegistrasiRep.Count() * 2;

            for (int i = 0; i < riskMatrixProjectReport.Count(); i++)
            {
                var stageTahun = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectId(riskMatrixProjectReport[i].Id).ToList();

                worksheet2.Range(4 + (batasBawahRiskMatrix + 5) * i, 9, batasBawahRiskMatrix + 6 + (batasBawahRiskMatrix + 5) * i, 8 + stageTahun.Count()).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
                worksheet2.Range(5 + (batasBawahRiskMatrix + 5) * i, 9, 5 + (batasBawahRiskMatrix + 5) * i, 8 + stageTahun.Count()).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
                worksheet2.Cell(batasBawahRiskMatrix + 6 + (batasBawahRiskMatrix + 5) * i, 3).Value = "Total Maximum Expose";
                worksheet2.Range(batasBawahRiskMatrix + 6 + (batasBawahRiskMatrix + 5) * i, 3, batasBawahRiskMatrix + 6 + (batasBawahRiskMatrix + 5) * i, 8).Merge();
                worksheet2.Range(4 + (batasBawahRiskMatrix + 5) * i, 2, 5 + (batasBawahRiskMatrix + 5) * i, 8 + stageTahun.Count()).Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);

                for (int s = 0; s < stageTahun.Count(); s++)
                {
                    worksheet2.Cell(4 + (batasBawahRiskMatrix + 5) * i, 9 + s).Value = stageTahun[s].Stage.NamaStage;
                    worksheet2.Cell(5 + (batasBawahRiskMatrix + 5) * i, 9 + s).Value = stageTahun[s].Tahun;
                    worksheet2.Cell(batasBawahRiskMatrix + 6 + (batasBawahRiskMatrix + 5) * i, 9 + s).Value = stageTahun[s].MaximumNilaiExpose;
                    worksheet2.Column(9 + s).Width = 7;

                    var riskMatrixProjectDetailReport = _stageTahunRiskMatrixDetailRepository.GetByStageTahunRiskMatrixId(stageTahun[s].Id).Where(x => x.RiskMatrixProjectId == riskMatrixProjectReport[i].Id).ToList();
                    for (int q = 0; q < riskMatrixProjectDetailReport.Count(); q++)
                    {
                        if (riskMatrixProjectDetailReport[q].LikehoodDetail == null)
                        {
                            worksheet2.Cell(6 + batasBawahRiskMatrix * i + 3 * i + 2 * q + 2 * i, 9 + s).Value = "-";
                            worksheet2.Cell(6 + batasBawahRiskMatrix * i + 3 * i + 2 * q + 1 + 2 * i, 9 + s).Value = "-";
                        }
                        else
                        {
                            worksheet2.Cell(6 + batasBawahRiskMatrix * i + 3 * i + 2 * q + 2 * i, 9 + s).Value = riskMatrixProjectDetailReport[q].NilaiExpose;
                            worksheet2.Cell(6 + batasBawahRiskMatrix * i + 3 * i + 2 * q + 1 + 2 * i, 9 + s).Value = riskMatrixProjectDetailReport[q].LikehoodDetail.DefinisiLikehood;
                        }
                    }
                }

                worksheet2.Range(batasBawahRiskMatrix + 6 + (batasBawahRiskMatrix + 5) * i, 2, batasBawahRiskMatrix + 6 + (batasBawahRiskMatrix + 5) * i, 8).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
                worksheet2.Range(4 + (batasBawahRiskMatrix + 5) * i, 2, batasBawahRiskMatrix + 6 + (batasBawahRiskMatrix + 5) * i, 8).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
                worksheet2.Range(4 + (batasBawahRiskMatrix + 5) * i, 2, 4 + (batasBawahRiskMatrix + 5) * i, 8).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

                worksheet2.Cell(4 + (batasBawahRiskMatrix + 5) * i, 2).Value = "In IDR Billion";
                worksheet2.Range(4 + (batasBawahRiskMatrix + 5) * i, 2, 4 + (batasBawahRiskMatrix + 5) * i, 8).Merge();

                worksheet2.Cell(5 + (batasBawahRiskMatrix + 5) * i, 2).Value = "Project Name";
                worksheet2.Cell(5 + (batasBawahRiskMatrix + 5) * i, 2).Style.Alignment.WrapText = true;
                worksheet2.Cell(5 + (batasBawahRiskMatrix + 5) * i, 2).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
                worksheet2.Row(5 + (batasBawahRiskMatrix + 5) * i).Height = 28;
                worksheet2.Range(5 + (batasBawahRiskMatrix + 5) * i, 3, batasBawahRiskMatrix + 6 + (batasBawahRiskMatrix + 5) * i - 1, 3).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet2.Range(6 + (batasBawahRiskMatrix + 5) * i, 2, batasBawahRiskMatrix + 6 + (batasBawahRiskMatrix + 5) * i - 1, 2).Merge();
                worksheet2.Cell(6 + (batasBawahRiskMatrix + 5) * i, 2).Value = riskMatrixProjectReport[i].Project.NamaProject;
                worksheet2.Cell(6 + (batasBawahRiskMatrix + 5) * i, 2).Style.Alignment.WrapText = true;

                worksheet2.Cell(5 + (batasBawahRiskMatrix + 5) * i, 3).Value = "Period";
                worksheet2.Range(5 + (batasBawahRiskMatrix + 5) * i, 3, 5 + (batasBawahRiskMatrix + 5) * i, 4).Merge();
                worksheet2.Range(5 + (batasBawahRiskMatrix + 5) * i, 3, 5 + (batasBawahRiskMatrix + 5) * i, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
                worksheet2.Range(5 + (batasBawahRiskMatrix + 5) * i, 4, batasBawahRiskMatrix + 6 + (batasBawahRiskMatrix + 5) * i - 1, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                worksheet2.Cell(6 + (batasBawahRiskMatrix + 5) * i, 3).Value = "Start Date";
                worksheet2.Cell(6 + (batasBawahRiskMatrix + 5) * i, 4).Value = "End Date";

                worksheet2.Cell(8 + pcCnOp.Count() + (batasBawahRiskMatrix + 5) * i, 3).Value = "Start Year";
                worksheet2.Cell(8 + pcCnOp.Count() + (batasBawahRiskMatrix + 5) * i, 4).Value = "End year";

                for (int x = 0; x < pcCnOp.Count; x++)
                {
                    var dells = stageTahun.Where(u => u.StageId == pcCnOp[x].Id).ToList();
                    var asd = riskMatrixProjectReport[i].Project.TahunAwalProject;
                    var asd2 = riskMatrixProjectReport[i].Project.TahunAkhirProject;
                    string awB = "'" + asd.ToString("MMM") + "-" + dells[0].Tahun.ToString().Substring(2);
                    string akB = "'" + asd2.ToString("MMM") + "-" + dells[dells.Count() - 1].Tahun.ToString().Substring(2);

                    worksheet2.Cell(7 + x + (batasBawahRiskMatrix + 5) * i, 3).Value = awB;
                    worksheet2.Cell(7 + x + (batasBawahRiskMatrix + 5) * i, 4).Value = akB;
                    worksheet2.Cell(7 + x + (batasBawahRiskMatrix + 5) * i, 6).Value = pcCnOp[x].NamaStage;

                    worksheet2.Cell(9 + pcCnOp.Count() + x + (batasBawahRiskMatrix + 5) * i, 3).Value = dells[0].Tahun;
                    worksheet2.Cell(9 + pcCnOp.Count() + x + (batasBawahRiskMatrix + 5) * i, 4).Value = dells[dells.Count() - 1].Tahun;
                    worksheet2.Cell(9 + pcCnOp.Count() + x + (batasBawahRiskMatrix + 5) * i, 5).Value = dells[0].Tahun;
                    worksheet2.Cell(9 + pcCnOp.Count() + x + (batasBawahRiskMatrix + 5) * i, 6).Value = pcCnOp[x].NamaStage;
                }

                worksheet2.Cell(5 + (batasBawahRiskMatrix + 5) * i, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
                worksheet2.Cell(5 + (batasBawahRiskMatrix + 5) * i, 6).Value = "Stage";
                worksheet2.Cell(5 + (batasBawahRiskMatrix + 5) * i, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
                worksheet2.Range(5 + (batasBawahRiskMatrix + 5) * i, 6, batasBawahRiskMatrix + 6 + (batasBawahRiskMatrix + 5) * i - 1, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                worksheet2.Cell(5 + (batasBawahRiskMatrix + 5) * i, 7).Value = "Risk Registrasi ID";
                worksheet2.Range(5 + (batasBawahRiskMatrix + 5) * i, 8, batasBawahRiskMatrix + 6 + (batasBawahRiskMatrix + 5) * i - 1, 8).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                worksheet2.Range(5 + (batasBawahRiskMatrix + 5) * i, 7, 5 + (batasBawahRiskMatrix + 5) * i, 8).Merge();
                worksheet2.Range(5 + (batasBawahRiskMatrix + 5) * i, 7, 5 + (batasBawahRiskMatrix + 5) * i, 8).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

                for (int j = 0; j < riskRegistrasiRep.Count(); j++)
                {
                    worksheet2.Cell(6 + batasBawahRiskMatrix * i + 3 * i + 2 * j + 2 * i, 7).Value = riskRegistrasiRep[j].KodeMRisk;
                    worksheet2.Cell(6 + batasBawahRiskMatrix * i + 3 * i + 2 * j + 2 * i, 8).Value = "Exposure";
                    worksheet2.Cell(6 + batasBawahRiskMatrix * i + 3 * i + 2 * j + 1 + 2 * i, 7).Value = riskRegistrasiRep[j].KodeMRisk;
                    worksheet2.Cell(6 + batasBawahRiskMatrix * i + 3 * i + 2 * j + 1 + 2 * i, 8).Value = "Likelihood";
                    worksheet2.Range(6 + batasBawahRiskMatrix * i + 3 * i + 2 * j + 2 * i, 7, 6 + batasBawahRiskMatrix * i + 3 * i + 2 * j + 1 + 2 * i, 8 + stageTahun.Count()).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                }
            }

            #region setting
            worksheet2.Column(1).Width = 3;
            worksheet2.Column(2).Width = 14;
            worksheet2.Column(3).Width = 10;
            worksheet2.Column(4).Width = 10;
            worksheet2.Column(5).Width = 10;
            worksheet2.Column(6).Width = 10;
            worksheet2.Column(7).Width = 4;
            worksheet2.Column(8).Width = 12;

            worksheet2.Cell("A1").Value = "IIGF Project Risk Matrix";
            worksheet2.Cell("B2").Value = "Summarize the exposure and risk likelihood in every time horizon and every current IIGF project";

            worksheet2.Range(1, 1, 1, 30).Merge();
            worksheet2.Range(2, 2, 2, 8).Merge();

            worksheet2.Cell(1, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet2.Cell(2, 2).Style.Alignment.WrapText = true;
            worksheet2.Row(2).Height = 35;
            worksheet2.Range(1, 1, 1, 30).Style.Fill.BackgroundColor = XLColor.FromArgb(244, 176, 132);
            worksheet2.Range(1, 1, 1, 30).Style.Font.Bold = true;
            worksheet2.Range(1, 1, 1, 7).Style.Font.FontSize = 16;
            worksheet2.Range(2, 2, 2, 8).Style.Fill.BackgroundColor = XLColor.FromArgb(242, 242, 242);
            #endregion setting

            #endregion 2.Risk Matrix

            #region 3.Correlation Sektor
            var CorrelationMatrixReport = _iDatabaseContext.CorrelationMatrixs.Where(x => x.IsDelete == false).ToList();
            var correlationSectorReport = _iDatabaseContext.CorrelatedSektors.Where(x => x.IsDelete == false && x.StatusId == 2 && x.ScenarioId == scenarioCorrelationDefault.Id).ToList();
            var worksheet3 = workbook.Worksheets.Add("3.Correlation Sektor");

            worksheet3.Column(1).Width = 3;
            worksheet3.Cell("A1").Value = "Correlation Matrix";
            worksheet3.Cell("B2").Value = "Correlations within project, between risk categories, across projects and sectors";

            worksheet3.Range(1, 1, 1, 3 + riskRegistrasiRep.Count() * correlationSectorReport.Count()).Merge();
            worksheet3.Range(2, 2, 3, 5).Merge();

            worksheet3.Cell(1, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet3.Cell(2, 2).Style.Alignment.WrapText = true;
            worksheet3.Row(2).Height = 35;
            worksheet3.Range(1, 1, 1, 3 + riskRegistrasiRep.Count() * correlationSectorReport.Count()).Style.Fill.BackgroundColor = XLColor.FromArgb(84, 130, 53);
            worksheet3.Range(1, 1, 1, 3 + riskRegistrasiRep.Count() * correlationSectorReport.Count()).Style.Font.Bold = true;
            worksheet3.Range(1, 1, 1, 7).Style.Font.FontSize = 16;
            worksheet3.Range(2, 2, 3, 5).Style.Fill.BackgroundColor = XLColor.FromArgb(242, 242, 242);

            worksheet3.Row(1).Height = 29;
            worksheet3.Row(2).Height = 18;
            worksheet3.Row(3).Height = 18;
            worksheet3.Row(4).Height = 12;

            worksheet3.Cell("B5").Value = "Nama Matrix";
            worksheet3.Cell("B5").Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);
            worksheet3.Cell("B5").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            worksheet3.Range(5, 2, 5, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
            worksheet3.Range(5, 2, 5, 4).Merge();
            worksheet3.Cell("E5").Value = "Nilai";
            worksheet3.Cell("E5").Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);
            worksheet3.Cell("E5").Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
            worksheet3.Range(5, 2, 5 + CorrelationMatrixReport.Count(), 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
            worksheet3.Range(5, 5, 5 + CorrelationMatrixReport.Count(), 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

            for (int i = 0; i < CorrelationMatrixReport.Count(); i++)
            {
                worksheet3.Cell(6 + i, 2).Value = CorrelationMatrixReport[i].NamaCorrelationMatrix;
                worksheet3.Cell(6 + i, 5).Value = CorrelationMatrixReport[i].Nilai;
                worksheet3.Range(6 + i, 2, 6 + i, 4).Merge();
            }

            var checkPlus = CorrelationMatrixReport.Count() - 4;

            worksheet3.Cell(11 + checkPlus, 2).Value = "Risk Categories Id";
            worksheet3.Cell(11 + checkPlus, 3).Value = "Risk Categories";
            worksheet3.Cell(11 + checkPlus, 4).Value = "Sektor";

            worksheet3.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            worksheet3.Cell(11 + checkPlus, 2).Style.Alignment.WrapText = true;
            worksheet3.Range(11 + checkPlus, 2, 13 + checkPlus, 2).Merge();
            worksheet3.Range(11 + checkPlus, 3, 13 + checkPlus, 3).Merge();
            worksheet3.Range(11 + checkPlus, 4, 11 + checkPlus, 3 + riskRegistrasiRep.Count() * correlationSectorReport.Count()).Merge();
            worksheet3.Range(11 + checkPlus, 2, 13 + checkPlus, 2).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
            worksheet3.Range(11 + checkPlus, 3, 13 + checkPlus, 3).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
            worksheet3.Range(14 + checkPlus, 2, 13 + checkPlus + riskRegistrasiRep.Count(), 3).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
            worksheet3.Range(11 + checkPlus, 4, 11 + checkPlus, 3 + riskRegistrasiRep.Count() * correlationSectorReport.Count()).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
            worksheet3.Range(11 + checkPlus, 2, 11 + checkPlus, 4).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            worksheet3.Range(14 + checkPlus, 2, 13 + checkPlus + riskRegistrasiRep.Count(), 2).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            worksheet3.Range(14 + checkPlus, 2, 13 + checkPlus + riskRegistrasiRep.Count(), 2).Style.Font.Bold = true;
            worksheet3.Range(11 + checkPlus, 2, 13 + checkPlus, 3 + riskRegistrasiRep.Count() * correlationSectorReport.Count()).Style.Font.Bold = true;
            worksheet3.Range(11 + checkPlus, 2, 12 + checkPlus, 3 + riskRegistrasiRep.Count() * correlationSectorReport.Count()).Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);

            worksheet3.Column(1).Width = 3;
            worksheet3.Column(2).Width = 14;
            worksheet3.Column(3).Width = 38;

            for (int i = 0; i < riskRegistrasiRep.Count() + 3; i++)
            {
                worksheet3.Row(11 + i).Height = 27;
            }

            for (int i = 0; i < riskRegistrasiRep.Count() * correlationSectorReport.Count(); i++)
            {
                worksheet3.Column(4 + i).Width = 4;
            }

            for (int i = 0; i < riskRegistrasiRep.Count(); i++)
            {
                worksheet3.Cell(14 + i, 2).Value = riskRegistrasiRep[i].KodeMRisk;
                worksheet3.Cell(14 + i, 3).Value = riskRegistrasiRep[i].NamaCategoryRisk;
            }

            var y = 4;
            for (int i = 0; i < correlationSectorReport.Count(); i++)
            {
                var correlationSektorReportList = _correlatedSektorDetailRepository.GetByCorrelatedSektorId(correlationSectorReport[i].Id).Where(x => x.IsDelete == false).ToList();
                worksheet3.Cell(12, y + i).Value = correlationSectorReport[i].NamaSektor;
                worksheet3.Range(12, y + i, 12, y + i + riskRegistrasiRep.Count() - 1).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
                worksheet3.Range(13, y + i, 13, y + i + riskRegistrasiRep.Count() - 1).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

                for (int j = 0; j < riskRegistrasiRep.Count(); j++)
                {
                    worksheet3.Cell(13, y + j + i).Value = riskRegistrasiRep[j].KodeMRisk;
                }

                var b = 0;
                var c = 0;
                foreach (var item in correlationSektorReportList)
                {
                    if (c == riskRegistrasiRep.Count())
                    {
                        c = 0;
                        b++;
                    }
                    worksheet3.Cell(14 + b, y + c + i).Value = item.CorrelationMatrix.Nilai;
                    c++;
                }
                worksheet3.Range(12, y + i, 12, y + i + riskRegistrasiRep.Count() - 1).Merge();
                worksheet3.Range(12, y + i, 13 + riskRegistrasiRep.Count(), y + i + riskRegistrasiRep.Count() - 1).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
                worksheet3.Range(12, y + i, 13 + riskRegistrasiRep.Count(), y + i + riskRegistrasiRep.Count() - 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                y = y + riskRegistrasiRep.Count() - 1;

            }

            #endregion 3.Correlation Sektor

            #region 4.Correlation Proyek            
            var correlationProjectReport = _iDatabaseContext.CorrelatedProjects.Where(x => x.IsDelete == false && x.IsApproved == true && x.ScenarioId == scenarioCorrelationDefault.Id).SingleOrDefault();
            var correlationProjectReportList = _iDatabaseContext.CorrelatedProjectDetails.Where(x => x.IsDelete == false && x.CorrelatedProjectId == correlationProjectReport.Id).ToList();
            var projectInScenariodetails = _iDatabaseContext.ScenarioDetails.Where(x => x.IsDelete == false && x.ScenarioId == scenarioCorrelationDefault.Id).ToList();
            var worksheet4 = workbook.Worksheets.Add("4.Correlation Proyek");

            worksheet4.Cell("A1").Value = "Correlation Matrix Between Project";
            worksheet4.Cell("B2").Value = "Correlations within project, between risk categories, across projects and sectors";

            worksheet4.Range(1, 1, 1, 3 + riskRegistrasiRep.Count() * correlationSectorReport.Count()).Merge();
            worksheet4.Range(2, 2, 3, 5).Merge();

            worksheet4.Cell(1, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet4.Cell(2, 2).Style.Alignment.WrapText = true;
            worksheet4.Row(2).Height = 35;
            worksheet4.Range(1, 1, 1, 3 + riskRegistrasiRep.Count() * correlationSectorReport.Count()).Style.Fill.BackgroundColor = XLColor.FromArgb(84, 130, 53);
            worksheet4.Range(1, 1, 1, 3 + riskRegistrasiRep.Count() * correlationSectorReport.Count()).Style.Font.Bold = true;
            worksheet4.Range(1, 1, 1, 7).Style.Font.FontSize = 16;
            worksheet4.Range(2, 2, 3, 5).Style.Fill.BackgroundColor = XLColor.FromArgb(242, 242, 242);

            worksheet4.Row(1).Height = 29;
            worksheet4.Row(2).Height = 18;
            worksheet4.Row(3).Height = 18;
            worksheet4.Row(4).Height = 12;
            worksheet4.Column(1).Width = 3;

            worksheet4.Cell("B5").Value = "Nama Matrix";
            worksheet4.Cell("B5").Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);
            worksheet4.Cell("B5").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            worksheet4.Range(5, 2, 5, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
            worksheet4.Range(5, 2, 5, 4).Merge();
            worksheet4.Cell("E5").Value = "Nilai";
            worksheet4.Cell("E5").Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);
            worksheet4.Cell("E5").Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
            worksheet4.Range(5, 2, 5 + CorrelationMatrixReport.Count(), 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
            worksheet4.Range(5, 5, 5 + CorrelationMatrixReport.Count(), 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

            for (int i = 0; i < CorrelationMatrixReport.Count(); i++)
            {
                worksheet4.Cell(6 + i, 2).Value = CorrelationMatrixReport[i].NamaCorrelationMatrix;
                worksheet4.Cell(6 + i, 5).Value = CorrelationMatrixReport[i].Nilai;
                worksheet4.Range(6 + i, 2, 6 + i, 4).Merge();
            }

            worksheet4.Cell(12 + checkPlus, 2).Value = "Project Type";
            worksheet4.Cell(12 + checkPlus, 3).Value = "Project Name";

            int WorkSheet4Samping = 3 + projectInScenariodetails.Count();
            int WorkSheet4Atas = 12 + checkPlus + projectInScenariodetails.Count();

            for (int i = 0; i < projectInScenariodetails.Count(); i++)
            {
                worksheet4.Cell(13 + checkPlus + i, 2).Value = projectInScenariodetails[i].Project.Sektor.NamaSektor;
                worksheet4.Cell(13 + checkPlus + i, 2).Style.Alignment.WrapText = true;
                worksheet4.Cell(13 + checkPlus + i, 3).Value = projectInScenariodetails[i].Project.NamaProject;
                worksheet4.Cell(13 + checkPlus + i, 3).Style.Alignment.WrapText = true;
                worksheet4.Cell(12 + checkPlus, 4 + i).Value = projectInScenariodetails[i].Project.NamaProject;
                worksheet4.Cell(12 + checkPlus, 4 + i).Style.Alignment.WrapText = true;
            }

            var m = 0;
            var n = 0;

            foreach (var item in correlationProjectReportList)
            {
                if (n == projectInScenariodetails.Count())
                {
                    n = 0;
                    m++;
                }
                worksheet4.Cell(13 + checkPlus + m, 4 + n).Value = item.CorrelationMatrix.Nilai;
                n++;
            }

            #region setting
            worksheet4.Cell(12 + checkPlus, 2).Style.Fill.BackgroundColor = XLColor.FromArgb(91, 155, 213);
            worksheet4.Cell(12 + checkPlus, 2).Style.Font.Bold = true;
            worksheet4.Cell(12 + checkPlus, 3).Style.Fill.BackgroundColor = XLColor.FromArgb(91, 155, 213);
            worksheet4.Cell(12 + checkPlus, 3).Style.Font.Bold = true;
            worksheet4.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;

            worksheet4.Range(12 + checkPlus, 2, WorkSheet4Atas, 3).Style.Font.FontSize = 9;
            worksheet4.Range(12 + checkPlus, 2, WorkSheet4Atas, 3).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
            worksheet4.Range(12 + checkPlus, 2, WorkSheet4Atas, 2).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            worksheet4.Range(13 + checkPlus, 2, WorkSheet4Atas, 2).Style.Font.Bold = true;
            worksheet4.Range(13 + checkPlus, 2, WorkSheet4Atas, 2).Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);

            worksheet4.Range(12 + checkPlus, 2, 12 + checkPlus, WorkSheet4Samping).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            worksheet4.Range(12 + checkPlus, 2, 12 + checkPlus, WorkSheet4Samping).Style.Font.FontSize = 9;
            worksheet4.Range(12 + checkPlus, 4, 12 + checkPlus, WorkSheet4Samping).Style.Fill.BackgroundColor = XLColor.FromArgb(189, 215, 238);
            worksheet4.Range(12 + checkPlus, 2, 12 + checkPlus, WorkSheet4Samping).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;


            worksheet4.Range(12 + checkPlus, 2, WorkSheet4Atas, WorkSheet4Samping).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
            worksheet4.Range(13 + checkPlus, 4, WorkSheet4Atas, WorkSheet4Samping).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            for (int i = 12; i <= WorkSheet4Atas; i++)
            {
                worksheet4.Row(i).Height = 40;
            }

            for (int i = 2; i <= WorkSheet4Samping; i++)
            {
                worksheet4.Column(i).Width = 16;
            }
            #endregion setting

            #endregion 4.Correlation Proyek

            #region 5. KalkulasiProjectDetail
            var worksheet5 = workbook.Worksheets.Add("5.KalkulasiProjectDetail");
            worksheet5.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            worksheet5.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            worksheet5.Cell("B1").Value = "Risk-Capital Calculcation";
            worksheet5.Cell("B1").Style.Font.Bold = true;
            worksheet5.Cell("B1").Style.Font.FontColor = XLColor.FromArgb(231, 230, 230);
            worksheet5.Cell("B1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet5.Range(1, 1, 1, 25).Style.Fill.BackgroundColor = XLColor.FromArgb(84, 130, 53);
            worksheet5.Cell("B2").Value = "Detail calculation of capital required (undiversified and diversified) each year by :\n- Risk in every particular project\n- Aggregation of every project\n- Agrregation of each risk\nCalculation of Asset projection each year up until project year end";
            worksheet5.Cell("B2").Style.Alignment.WrapText = true;
            worksheet5.Cell("B2").Style.Fill.BackgroundColor = XLColor.FromArgb(242, 242, 242);
            worksheet5.Range("B2", "F2").Merge();
            worksheet5.Row(2).Height = 90;
            worksheet5.Cell("B2").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet5.Cell("B4").Value = "Project Detail";
            worksheet5.Cell("B4").Style.Font.Bold = true;
            worksheet5.Cell("B4").Style.Font.Underline = XLFontUnderlineValues.Single;
            worksheet5.Cell("B4").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet5.Range(4, 1, 4, 7 + 25).Style.Fill.BackgroundColor = XLColor.FromArgb(248, 203, 173);

            var projectDetail = calculation.CalculationResult.ProjectDetail;
            for (int i = 0; i < calculation.CalculationResult.ProjectDetail.Length; i++)
            {
                worksheet5.Cell(5 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "B").Value = projectDetail[i].NamaProject;
                worksheet5.Cell(5 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "B").Style.Font.Bold = true;
                worksheet5.Cell(5 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "B").Style.Font.Italic = true;
                worksheet5.Range(5 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 2, 5 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 25).Style.Fill.BackgroundColor = XLColor.FromArgb(244, 176, 132);
                worksheet5.Range(5 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 2, 5 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 24).Merge();
                worksheet5.Cell(7 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "B").Value = "Matrix Choice";
                worksheet5.Cell(8 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "B").Value = projectDetail[i].NamaSektor;
                worksheet5.Range(7 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 2, 8 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 2).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                worksheet5.Range(7 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 2, 8 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 2).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet5.Cell(6 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "F").Value = "Correlation Matrix between Risk Categories";
                worksheet5.Cell(6 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "F").Style.Font.Bold = true;
                worksheet5.Cell(6 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "F").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                for (int j = 0; j < projectDetail[i].RiskRegistrasi.Length; j++)
                {
                    worksheet5.Cell(7 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 7 + j).Value = projectDetail[i].RiskRegistrasi[j].KodeMRisk;
                    worksheet5.Cell(7 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 7 + j).Style.Font.Bold = true;
                    worksheet5.Cell(8 + j + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 6).Value = projectDetail[i].RiskRegistrasi[j].KodeMRisk;
                    worksheet5.Cell(8 + j + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 6).Style.Font.Bold = true;
                    var riskRegistrasiCol = projectDetail[i].RiskRegistrasi[j].Id;
                    for (int z = 0; z < projectDetail[i].RiskRegistrasi.Length; z++)
                    {
                        var riskRegistrasiRow = projectDetail[i].RiskRegistrasi[z].Id;
                        worksheet5.Cell(z + 8 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 7 + j).Value = projectDetail[i].CorrelatedSektorDetail.Where(x => x.RiskRegistrasiIdCol == riskRegistrasiCol && x.RiskRegistrasiIdRow == riskRegistrasiRow).FirstOrDefault().CorrelationMatrixValue;
                    }

                    worksheet5.Cell(projectDetail[i].RiskRegistrasi.Length + 12 + j + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "B").Value = projectDetail[i].RiskRegistrasi[j].KodeMRisk;
                    worksheet5.Cell(projectDetail[i].RiskRegistrasi.Length + 12 + j + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "B").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 189);
                    worksheet5.Cell(projectDetail[i].RiskRegistrasi.Length + 12 + j + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "C").Value = projectDetail[i].RiskRegistrasi[j].NamaCategoryRisk;
                    worksheet5.Cell(projectDetail[i].RiskRegistrasi.Length + 12 + j + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "C").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 189);
                    worksheet5.Range(projectDetail[i].RiskRegistrasi.Length + 12 + j + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 3, projectDetail[i].RiskRegistrasi.Length + 12 + j + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 7).Merge();
                    worksheet5.Range(projectDetail[i].RiskRegistrasi.Length + 12 + j + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 3, projectDetail[i].RiskRegistrasi.Length + 12 + j + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 7).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 2) + 18 + j + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "B").Value = projectDetail[i].RiskRegistrasi[j].KodeMRisk;
                    worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 2) + 18 + j + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "B").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 189);
                    worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 2) + 18 + j + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "C").Value = projectDetail[i].RiskRegistrasi[j].NamaCategoryRisk;
                    worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 2) + 18 + j + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "C").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 189);
                    worksheet5.Range((projectDetail[i].RiskRegistrasi.Length * 2) + 18 + j + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 3, (projectDetail[i].RiskRegistrasi.Length * 2) + 18 + j + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 7).Merge();
                    worksheet5.Range((projectDetail[i].RiskRegistrasi.Length * 2) + 18 + j + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 3, (projectDetail[i].RiskRegistrasi.Length * 2) + 18 + j + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 7).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                }
                worksheet5.Range(7 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 6, 7 + projectDetail[i].RiskRegistrasi.Length + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 6 + projectDetail[i].RiskRegistrasi.Length).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                worksheet5.Range(7 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 6, 7 + projectDetail[i].RiskRegistrasi.Length + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 6 + projectDetail[i].RiskRegistrasi.Length).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;



                worksheet5.Cell(projectDetail[i].RiskRegistrasi.Length + 10 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "B").Value = "Risk Categories";
                worksheet5.Cell(projectDetail[i].RiskRegistrasi.Length + 10 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "B").Style.Font.Bold = true;
                worksheet5.Cell(projectDetail[i].RiskRegistrasi.Length + 10 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "B").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
                worksheet5.Range(projectDetail[i].RiskRegistrasi.Length + 10 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 2, projectDetail[i].RiskRegistrasi.Length + 11 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 2).Merge();
                worksheet5.Cell(projectDetail[i].RiskRegistrasi.Length + 10 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "C").Value = "Risk Category Description";
                worksheet5.Cell(projectDetail[i].RiskRegistrasi.Length + 10 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "C").Style.Font.Bold = true;
                worksheet5.Cell(projectDetail[i].RiskRegistrasi.Length + 10 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "C").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
                worksheet5.Range(projectDetail[i].RiskRegistrasi.Length + 10 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 3, projectDetail[i].RiskRegistrasi.Length + 11 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 7).Merge();
                worksheet5.Cell(projectDetail[i].RiskRegistrasi.Length + 10 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "H").Value = "Undiversified Risk Capital";
                worksheet5.Cell(projectDetail[i].RiskRegistrasi.Length + 10 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "H").Style.Font.Bold = true;
                worksheet5.Cell(projectDetail[i].RiskRegistrasi.Length + 10 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "H").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                worksheet5.Cell(projectDetail[i].RiskRegistrasi.Length + 10 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "H").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
                worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 2) + 12 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "B").Value = "Total Undiversified Risk Capital";
                worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 2) + 12 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "B").Style.Font.Bold = true;
                worksheet5.Range((projectDetail[i].RiskRegistrasi.Length * 2) + 12 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 2, (projectDetail[i].RiskRegistrasi.Length * 2) + 13 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 7).Merge();
                worksheet5.Range((projectDetail[i].RiskRegistrasi.Length * 2) + 12 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 2, (projectDetail[i].RiskRegistrasi.Length * 2) + 13 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 7).Style.Fill.BackgroundColor = XLColor.FromArgb(217, 217, 217);

                worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 2) + 16 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "B").Value = "Risk Categories";
                worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 2) + 16 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "B").Style.Font.Bold = true;
                worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 2) + 16 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "B").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
                worksheet5.Range((projectDetail[i].RiskRegistrasi.Length * 2) + 16 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 2, (projectDetail[i].RiskRegistrasi.Length * 2) + 17 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 2).Merge();
                worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 2) + 16 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "C").Value = "Risk Category Description";
                worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 2) + 16 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "C").Style.Font.Bold = true;
                worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 2) + 16 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "C").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
                worksheet5.Range((projectDetail[i].RiskRegistrasi.Length * 2) + 16 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 3, (projectDetail[i].RiskRegistrasi.Length * 2) + 17 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 7).Merge();
                worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 2) + 16 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "H").Value = "Diversified Risk Capital";
                worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 2) + 16 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "H").Style.Font.Bold = true;
                worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 2) + 16 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "H").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 2) + 16 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "H").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
                worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 3) + 18 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "B").Value = "Total Diversified Risk Capital";
                worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 3) + 18 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), "B").Style.Font.Bold = true;
                worksheet5.Range((projectDetail[i].RiskRegistrasi.Length * 3) + 18 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 2, (projectDetail[i].RiskRegistrasi.Length * 3) + 19 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 7).Merge();
                worksheet5.Range((projectDetail[i].RiskRegistrasi.Length * 3) + 18 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 2, (projectDetail[i].RiskRegistrasi.Length * 3) + 19 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 7).Style.Fill.BackgroundColor = XLColor.FromArgb(217, 217, 217);

                for (int j = 0; j < projectDetail[i].UndiversifiedYearCollection.Length; j++)
                {
                    worksheet5.Cell(projectDetail[i].RiskRegistrasi.Length + 11 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 8 + j).Value = projectDetail[i].UndiversifiedYearCollection[j].Year;
                    worksheet5.Cell(projectDetail[i].RiskRegistrasi.Length + 11 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 8 + j).Style.Font.Bold = true;
                    worksheet5.Cell(projectDetail[i].RiskRegistrasi.Length + 29 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 8 + j).Value = projectDetail[i].DiversifiedRiskCapitalCollection[j].Year;
                    worksheet5.Cell(projectDetail[i].RiskRegistrasi.Length + 29 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 8 + j).Style.Font.Bold = true;

                    for (int z = 0; z < projectDetail[i].UndiversifiedYearCollection[j].YearValue.Length; z++)
                    {
                        worksheet5.Cell(z + projectDetail[i].RiskRegistrasi.Length + 12 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 8 + j).Value = projectDetail[i].UndiversifiedYearCollection[j].YearValue[z].ValueUndiversified;
                        worksheet5.Cell(z + (projectDetail[i].RiskRegistrasi.Length * 2) + 18 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 8 + j).Value = projectDetail[i].DiversifiedRiskCapitalCollection[j].RiskValueCollection[z].Value;
                    }

                    worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 2) + 12 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 8 + j).Value = projectDetail[i].UndiversifiedYearCollection[j].Total;
                    worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 2) + 12 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 8 + j).Style.Fill.BackgroundColor = XLColor.FromArgb(217, 217, 217);
                    worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 2) + 12 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 8 + j).Style.Font.Bold = true;
                    worksheet5.Range((projectDetail[i].RiskRegistrasi.Length * 2) + 12 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 8 + j, (projectDetail[i].RiskRegistrasi.Length * 2) + 13 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 8 + j).Merge();
                    worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 3) + 18 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 8 + j).Value = projectDetail[i].DiversifiedRiskCapitalCollection[j].Total;
                    worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 3) + 18 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 8 + j).Style.Fill.BackgroundColor = XLColor.FromArgb(217, 217, 217);
                    worksheet5.Cell((projectDetail[i].RiskRegistrasi.Length * 3) + 18 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 8 + j).Style.Font.Bold = true;
                    worksheet5.Range((projectDetail[i].RiskRegistrasi.Length * 3) + 18 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 8 + j, (projectDetail[i].RiskRegistrasi.Length * 3) + 19 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 8 + j).Merge();
                }

                worksheet5.Range(projectDetail[i].RiskRegistrasi.Length + 10 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 8, projectDetail[i].RiskRegistrasi.Length + 10 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 7 + projectDetail[i].UndiversifiedYearCollection.Length).Merge();
                worksheet5.Range((projectDetail[i].RiskRegistrasi.Length * 2) + 16 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 8, (projectDetail[i].RiskRegistrasi.Length * 2) + 16 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 7 + projectDetail[i].UndiversifiedYearCollection.Length).Merge();
                worksheet5.Range(projectDetail[i].RiskRegistrasi.Length + 10 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 2, (projectDetail[i].RiskRegistrasi.Length * 2) + 13 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 7 + projectDetail[i].UndiversifiedYearCollection.Length).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                worksheet5.Range(projectDetail[i].RiskRegistrasi.Length + 10 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 2, (projectDetail[i].RiskRegistrasi.Length * 2) + 13 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 7 + projectDetail[i].UndiversifiedYearCollection.Length).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

                worksheet5.Range((projectDetail[i].RiskRegistrasi.Length * 2) + 16 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 2, (projectDetail[i].RiskRegistrasi.Length * 3) + 19 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 7 + projectDetail[i].UndiversifiedYearCollection.Length).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                worksheet5.Range((projectDetail[i].RiskRegistrasi.Length * 2) + 16 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 2, (projectDetail[i].RiskRegistrasi.Length * 3) + 19 + (i * (projectDetail[i].RiskRegistrasi.Length * 3 + 18)), 7 + projectDetail[i].UndiversifiedYearCollection.Length).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

            }
            worksheet5.Columns().AdjustToContents();
            worksheet5.Column(7).Width = 18;
            worksheet5.Dispose();
            #endregion 5. Kalkulasi Project Detail

            #region 6.KalkulasiAggregationOfProject
            var worksheet6 = workbook.Worksheets.Add("KalkulasiAggregationOfProject");

            worksheet6.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            worksheet6.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            var undiv = calculation.CalculationResult.AggregationOfProject.AggregationUndiversifiedProjectCapital;
            var intraDiv = calculation.CalculationResult.AggregationOfProject.AggregationIntraDiversifiedProjectCapital;
            var interDiv = calculation.CalculationResult.AggregationOfProject.AggregationInterDiversifiedProjectCapital;
            var projects = calculation.CalculationResult.AggregationOfProject.AggregationOfProjectCollection;

            worksheet6.Cell("B1").Value = "Risk-Capital Calculcation";
            worksheet6.Cell("B1").Style.Font.Bold = true;
            worksheet6.Cell("B1").Style.Font.FontColor = XLColor.FromArgb(231, 230, 230);
            worksheet6.Cell("B1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet6.Range(1, 1, 1, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(84, 130, 53);
            worksheet6.Cell("B2").Value = "Detail calculation of capital required (undiversified and diversified) each year by :\n- Risk in every particular project\n- Aggregation of every project\n- Agrregation of each risk\nCalculation of Asset projection each year up until project year end";
            worksheet6.Cell("B2").Style.Alignment.WrapText = true;
            worksheet6.Cell("B2").Style.Fill.BackgroundColor = XLColor.FromArgb(242, 242, 242);
            worksheet6.Range("B2", "F2").Merge();
            worksheet6.Row(2).Height = 90;
            worksheet6.Cell("B2").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet6.Cell("B4").Value = "Aggregation of Projects";
            worksheet6.Cell("B4").Style.Font.Bold = true;
            worksheet6.Cell("B4").Style.Font.Underline = XLFontUnderlineValues.Single;
            worksheet6.Range(4, 1, 4, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(248, 203, 173);


            worksheet6.Range(8 + projects.Length, 2, 9 + projects.Length, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(191, 191, 191);
            worksheet6.Cell(6, "B").Value = "Project Name";
            worksheet6.Cell(6, "B").Style.Font.Bold = true;
            worksheet6.Cell(6, "B").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet6.Range(6, 2, 7, 3).Merge();
            worksheet6.Cell(6, "D").Value = "Project Type";
            worksheet6.Cell(6, "D").Style.Font.Bold = true;
            worksheet6.Cell(6, "D").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet6.Range(6, 4, 7, 7).Merge();
            worksheet6.Cell(6, "H").Value = "Undiversified Project Capital";
            worksheet6.Cell(6, "H").Style.Font.Bold = true;
            worksheet6.Cell(6, "H").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet6.Cell(6, "H").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet6.Range(6, 8, 6, 7 + years.Length).Merge();
            worksheet6.Cell(8 + projects.Length, "B").Value = "Total Undiversified Project Capital";
            worksheet6.Cell(8 + projects.Length, "B").Style.Font.Bold = true;
            worksheet6.Range(8 + projects.Length, 2, 9 + projects.Length, 7).Merge();


            worksheet6.Range(13 + projects.Length * 2, 2, 14 + projects.Length * 2, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(191, 191, 191);
            worksheet6.Cell(11 + projects.Length, "B").Value = "Project Name";
            worksheet6.Cell(11 + projects.Length, "B").Style.Font.Bold = true;
            worksheet6.Cell(11 + projects.Length, "B").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet6.Range(11 + projects.Length, 2, 12 + projects.Length, 3).Merge();
            worksheet6.Cell(11 + projects.Length, "D").Value = "Project Type";
            worksheet6.Cell(11 + projects.Length, "D").Style.Font.Bold = true;
            worksheet6.Cell(11 + projects.Length, "D").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet6.Range(11 + projects.Length, 4, 12 + projects.Length, 7).Merge();
            worksheet6.Cell(11 + projects.Length, "H").Value = "Intra-Project Diversified Project Capital";
            worksheet6.Cell(11 + projects.Length, "H").Style.Font.Bold = true;
            worksheet6.Cell(11 + projects.Length, "H").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet6.Cell(11 + projects.Length, "H").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet6.Range(11 + projects.Length, 8, 11 + projects.Length, 7 + years.Length).Merge();
            worksheet6.Cell(13 + projects.Length * 2, "B").Value = "Total Intra-Project Diversified Project Capital";
            worksheet6.Cell(13 + projects.Length * 2, "B").Style.Font.Bold = true;
            worksheet6.Range(13 + projects.Length * 2, 2, 14 + projects.Length * 2, 7).Merge();


            worksheet6.Range(18 + projects.Length * 3, 2, 19 + projects.Length * 3, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(191, 191, 191);
            worksheet6.Cell(16 + projects.Length * 2, "B").Value = "Project Name";
            worksheet6.Cell(16 + projects.Length * 2, "B").Style.Font.Bold = true;
            worksheet6.Cell(16 + projects.Length * 2, "B").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet6.Range(16 + projects.Length * 2, 2, 17 + projects.Length * 2, 3).Merge();
            worksheet6.Cell(16 + projects.Length * 2, "D").Value = "Project Type";
            worksheet6.Cell(16 + projects.Length * 2, "D").Style.Font.Bold = true;
            worksheet6.Cell(16 + projects.Length * 2, "D").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet6.Range(16 + projects.Length * 2, 4, 17 + projects.Length * 2, 7).Merge();
            worksheet6.Cell(16 + projects.Length * 2, "H").Value = "Inter-Project Diversified Project Capital";
            worksheet6.Cell(16 + projects.Length * 2, "H").Style.Font.Bold = true;
            worksheet6.Cell(16 + projects.Length * 2, "H").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet6.Cell(16 + projects.Length * 2, "H").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet6.Range(16 + projects.Length * 2, 8, 16 + projects.Length * 2, 7 + years.Length).Merge();
            worksheet6.Cell(18 + projects.Length * 3, "B").Value = "Total Inter-Project Diversified Project Capital";
            worksheet6.Cell(18 + projects.Length * 3, "B").Style.Font.Bold = true;
            worksheet6.Range(18 + projects.Length * 3, 2, 19 + projects.Length * 3, 7).Merge();

            for (int i = 0; i < projects.Length; i++)
            {
                worksheet6.Cell(8 + i, "B").Value = projects[i].NamaProject;
                worksheet6.Cell(8 + i, "B").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 189);
                worksheet6.Range(8 + i, 2, 8 + i, 3).Merge();
                worksheet6.Cell(8 + i, "D").Value = projects[i].NamaSektor;
                worksheet6.Cell(8 + i, "D").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 189);
                worksheet6.Range(8 + i, 4, 8 + i, 7).Merge();

                worksheet6.Cell(13 + i + projects.Length, "B").Value = projects[i].NamaProject;
                worksheet6.Cell(13 + i + projects.Length, "B").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 189);
                worksheet6.Range(13 + i + projects.Length, 2, 13 + i + projects.Length, 3).Merge();
                worksheet6.Cell(13 + i + projects.Length, "D").Value = projects[i].NamaSektor;
                worksheet6.Cell(13 + i + projects.Length, "D").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 189);
                worksheet6.Range(13 + i + projects.Length, 4, 13 + i + projects.Length, 7).Merge();

                worksheet6.Cell(18 + i + projects.Length * 2, "B").Value = projects[i].NamaProject;
                worksheet6.Cell(18 + i + projects.Length * 2, "B").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 189);
                worksheet6.Range(18 + i + projects.Length * 2, 2, 18 + i + projects.Length * 2, 3).Merge();
                worksheet6.Cell(18 + i + projects.Length * 2, "D").Value = projects[i].NamaSektor;
                worksheet6.Cell(18 + i + projects.Length * 2, "D").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 189);
                worksheet6.Range(18 + i + projects.Length * 2, 4, 18 + i + projects.Length * 2, 7).Merge();
            }

            for (int i = 0; i < years.Length; i++)
            {
                worksheet6.Cell(7, 8 + i).Value = years[i];
                worksheet6.Cell(7, 8 + i).Style.Font.Bold = true;
                worksheet6.Cell(12 + projects.Length, 8 + i).Value = years[i];
                worksheet6.Cell(12 + projects.Length, 8 + i).Style.Font.Bold = true;
                worksheet6.Cell(17 + projects.Length * 2, 8 + i).Value = years[i];
                worksheet6.Cell(17 + projects.Length * 2, 8 + i).Style.Font.Bold = true;

                for (int j = 0; j < projects.Length; j++)
                {
                    worksheet6.Cell(8 + j, 8 + i).Value = undiv[i].AggregationUndiversifiedProjectCollection[j].Total;
                    worksheet6.Cell(13 + j + projects.Length, 8 + i).Value = intraDiv[i].AggregationIntraDiversifiedProjectCollection[j].Total;
                    worksheet6.Cell(18 + j + projects.Length * 2, 8 + i).Value = interDiv[i].AggregationInterDiversifiedProjectCollection[j].Total;
                }

                worksheet6.Cell(8 + projects.Length, 8 + i).Value = undiv[i].TotalPerYear;
                worksheet6.Cell(8 + projects.Length, 8 + i).Style.Font.Bold = true;
                worksheet6.Range(8 + projects.Length, 8 + i, 9 + projects.Length, 8 + i).Merge();
                worksheet6.Cell(13 + projects.Length * 2, 8 + i).Value = intraDiv[i].TotalPerYear;
                worksheet6.Cell(13 + projects.Length * 2, 8 + i).Style.Font.Bold = true;
                worksheet6.Range(13 + projects.Length * 2, 8 + i, 14 + projects.Length * 2, 8 + i).Merge();
                worksheet6.Cell(18 + projects.Length * 3, 8 + i).Value = interDiv[i].TotalPerYear;
                worksheet6.Cell(18 + projects.Length * 3, 8 + i).Style.Font.Bold = true;
                worksheet6.Range(18 + projects.Length * 3, 8 + i, 19 + projects.Length * 3, 8 + i).Merge();
            }

            worksheet6.Range(6, 2, 9 + projects.Length, 7 + years.Length).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet6.Range(6, 2, 9 + projects.Length, 7 + years.Length).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

            worksheet6.Range(11 + projects.Length, 2, 14 + projects.Length * 2, 7 + years.Length).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet6.Range(11 + projects.Length, 2, 14 + projects.Length * 2, 7 + years.Length).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

            worksheet6.Range(16 + projects.Length * 2, 2, 19 + projects.Length * 3, 7 + years.Length).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet6.Range(16 + projects.Length * 2, 2, 19 + projects.Length * 3, 7 + years.Length).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

            worksheet6.Columns().AdjustToContents();
            worksheet6.Dispose();
            #endregion 6.KalkulasiAggregationOfProject

            #region 7.KalkulasiAggregationOfSector
            var worksheet7 = workbook.Worksheets.Add("KalkulasiAggregationOfSector");

            worksheet7.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            worksheet7.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            var undivSector = calculation.CalculationResult.AggregationSektor.UndiversifiedAggregationSektor;
            var intraDivSector = calculation.CalculationResult.AggregationSektor.IntraProjectDiversifiedAggregationSektor;
            var interDivSector = calculation.CalculationResult.AggregationSektor.InterProjectDiversifiedAggregationSektor;
            var sectorId = calculation.CalculationResult.AggregationSektor.UndiversifiedAggregationSektor.YearCollection[1].YearValues;
            var sectors = calculation.CalculationResult.AggregationSektor.Sektor;

            //IList<Sektor> sectors = new List<Sektor>();
            //foreach (var item in sectorId)
            //{
            //    var sector = _sektorRepository.Get(item.SektorId);
            //    sectors.Add(sector);
            //}

            worksheet7.Cell("B1").Value = "Risk-Capital Calculcation";
            worksheet7.Cell("B1").Style.Font.Bold = true;
            worksheet7.Cell("B1").Style.Font.FontColor = XLColor.FromArgb(231, 230, 230);
            worksheet7.Cell("B1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet7.Range(1, 1, 1, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(84, 130, 53);
            worksheet7.Cell("B2").Value = "Detail calculation of capital required (undiversified and diversified) each year by :\n- Risk in every particular project\n- Aggregation of every project\n- Agrregation of each risk\nCalculation of Asset projection each year up until project year end";
            worksheet7.Cell("B2").Style.Alignment.WrapText = true;
            worksheet7.Cell("B2").Style.Fill.BackgroundColor = XLColor.FromArgb(242, 242, 242);
            worksheet7.Range("B2", "F2").Merge();
            worksheet7.Row(2).Height = 90;
            worksheet7.Cell("B2").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet7.Cell("B4").Value = "Aggregation of Sector";
            worksheet7.Cell("B4").Style.Font.Bold = true;
            worksheet7.Cell("B4").Style.Font.Underline = XLFontUnderlineValues.Single;
            worksheet7.Range(4, 1, 4, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(248, 203, 173);


            worksheet7.Range(8 + sectors.Length, 2, 9 + sectors.Length, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(191, 191, 191);
            worksheet7.Cell(6, "B").Value = "Sector";
            worksheet7.Cell(6, "B").Style.Font.Bold = true;
            worksheet7.Cell(6, "B").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet7.Range(6, 2, 7, 3).Merge();
            worksheet7.Cell(6, "D").Value = "Undiversified Project Capital";
            worksheet7.Cell(6, "D").Style.Font.Bold = true;
            worksheet7.Cell(6, "D").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet7.Cell(6, "D").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet7.Range(6, 4, 6, 3 + years.Length).Merge();
            worksheet7.Cell(8 + sectors.Length, "B").Value = "Total";
            worksheet7.Cell(8 + sectors.Length, "B").Style.Font.Bold = true;
            worksheet7.Range(8 + sectors.Length, 2, 9 + sectors.Length, 3).Merge();


            worksheet7.Range(13 + sectors.Length * 2, 2, 14 + sectors.Length * 2, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(191, 191, 191);
            worksheet7.Cell(11 + sectors.Length, "B").Value = "Sector";
            worksheet7.Cell(11 + sectors.Length, "B").Style.Font.Bold = true;
            worksheet7.Cell(11 + sectors.Length, "B").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet7.Range(11 + sectors.Length, 2, 12 + sectors.Length, 3).Merge();
            worksheet7.Cell(11 + sectors.Length, "D").Value = "Intra-Project Diversified Project Capital";
            worksheet7.Cell(11 + sectors.Length, "D").Style.Font.Bold = true;
            worksheet7.Cell(11 + sectors.Length, "D").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet7.Cell(11 + sectors.Length, "D").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet7.Range(11 + sectors.Length, 4, 11 + sectors.Length, 3 + years.Length).Merge();
            worksheet7.Cell(13 + sectors.Length * 2, "B").Value = "Total";
            worksheet7.Cell(13 + sectors.Length * 2, "B").Style.Font.Bold = true;
            worksheet7.Range(13 + sectors.Length * 2, 2, 14 + sectors.Length * 2, 3).Merge();


            worksheet7.Range(18 + sectors.Length * 3, 2, 19 + sectors.Length * 3, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(191, 191, 191);
            worksheet7.Cell(16 + sectors.Length * 2, "B").Value = "Sector";
            worksheet7.Cell(16 + sectors.Length * 2, "B").Style.Font.Bold = true;
            worksheet7.Cell(16 + sectors.Length * 2, "B").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet7.Range(16 + sectors.Length * 2, 2, 17 + sectors.Length * 2, 3).Merge();
            worksheet7.Cell(16 + sectors.Length * 2, "D").Value = "Inter-Project Diversified Project Capital";
            worksheet7.Cell(16 + sectors.Length * 2, "D").Style.Font.Bold = true;
            worksheet7.Cell(16 + sectors.Length * 2, "D").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet7.Cell(16 + sectors.Length * 2, "D").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet7.Range(16 + sectors.Length * 2, 4, 16 + sectors.Length * 2, 3 + years.Length).Merge();
            worksheet7.Cell(18 + sectors.Length * 3, "B").Value = "Total";
            worksheet7.Cell(18 + sectors.Length * 3, "B").Style.Font.Bold = true;
            worksheet7.Range(18 + sectors.Length * 3, 2, 19 + sectors.Length * 3, 3).Merge();

            for (int i = 0; i < sectors.Length; i++)
            {
                worksheet7.Cell(8 + i, "B").Value = sectors[i].NamaSektor;
                worksheet7.Cell(8 + i, "B").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 189);
                worksheet7.Range(8 + i, 2, 8 + i, 3).Merge();

                worksheet7.Cell(13 + i + sectors.Length, "B").Value = sectors[i].NamaSektor;
                worksheet7.Cell(13 + i + sectors.Length, "B").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 189);
                worksheet7.Range(13 + i + sectors.Length, 2, 13 + i + sectors.Length, 3).Merge();

                worksheet7.Cell(18 + i + sectors.Length * 2, "B").Value = sectors[i].NamaSektor;
                worksheet7.Cell(18 + i + sectors.Length * 2, "B").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 189);
                worksheet7.Range(18 + i + sectors.Length * 2, 2, 18 + i + sectors.Length * 2, 3).Merge();
            }

            for (int i = 0; i < years.Length; i++)
            {
                worksheet7.Cell(7, 4 + i).Value = years[i];
                worksheet7.Cell(7, 4 + i).Style.Font.Bold = true;
                worksheet7.Cell(12 + sectors.Length, 4 + i).Value = years[i];
                worksheet7.Cell(12 + sectors.Length, 4 + i).Style.Font.Bold = true;
                worksheet7.Cell(17 + sectors.Length * 2, 4 + i).Value = years[i];
                worksheet7.Cell(17 + sectors.Length * 2, 4 + i).Style.Font.Bold = true;

                for (int j = 0; j < sectors.Length; j++)
                {
                    worksheet7.Cell(8 + j, 4 + i).Value = "0";
                    worksheet7.Cell(13 + j + sectors.Length, 4 + i).Value = "0";
                    worksheet7.Cell(18 + j + sectors.Length * 2, 4 + i).Value = "0";
                }

                for (int j = 0; j < sektor.Count; j++)
                {
                    worksheet7.Cell(8 + j, 4 + i).Value = undivSector.YearCollection[i].YearValues[j].Value;
                    worksheet7.Cell(13 + j + sectors.Length, 4 + i).Value = intraDivSector.YearCollection[i].YearValues[j].Value;
                    worksheet7.Cell(18 + j + sectors.Length * 2, 4 + i).Value = interDivSector.YearCollection[i].YearValues[j].Value;
                }

                worksheet7.Cell(8 + sectors.Length, 4 + i).Value = undivSector.YearCollection[i].Total;
                worksheet7.Cell(8 + sectors.Length, 4 + i).Style.Font.Bold = true;
                worksheet7.Range(8 + sectors.Length, 4 + i, 9 + sectors.Length, 4 + i).Merge();
                worksheet7.Cell(13 + sectors.Length * 2, 4 + i).Value = intraDivSector.YearCollection[i].Total;
                worksheet7.Cell(13 + sectors.Length * 2, 4 + i).Style.Font.Bold = true;
                worksheet7.Range(13 + sectors.Length * 2, 4 + i, 14 + sectors.Length * 2, 4 + i).Merge();
                worksheet7.Cell(18 + sectors.Length * 3, 4 + i).Value = interDivSector.YearCollection[i].Total;
                worksheet7.Cell(18 + sectors.Length * 3, 4 + i).Style.Font.Bold = true;
                worksheet7.Range(18 + sectors.Length * 3, 4 + i, 19 + sectors.Length * 3, 4 + i).Merge();
            }

            worksheet7.Range(6, 2, 9 + sectors.Length, 3 + years.Length).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet7.Range(6, 2, 9 + sectors.Length, 3 + years.Length).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

            worksheet7.Range(11 + sectors.Length, 2, 14 + sectors.Length * 2, 3 + years.Length).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet7.Range(11 + sectors.Length, 2, 14 + sectors.Length * 2, 3 + years.Length).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

            worksheet7.Range(16 + sectors.Length * 2, 2, 19 + sectors.Length * 3, 3 + years.Length).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet7.Range(16 + sectors.Length * 2, 2, 19 + sectors.Length * 3, 3 + years.Length).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

            worksheet7.Columns().AdjustToContents();
            worksheet7.Column(3).Width = 20;
            worksheet7.Dispose();
            #endregion 7.KalkulasiAggregationOfSector

            #region 8.KalkulasiAggregationOfRisk
            var worksheet8 = workbook.Worksheets.Add("KalkulasiAggregationOfRisk");

            worksheet8.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            worksheet8.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            var undivRisk = calculation.CalculationResult.AggregationRisk.UndiversifiedAggregationRisk;
            var intraDivRisk = calculation.CalculationResult.AggregationRisk.IntraProjectDiversifiedAggregationRisk;
            var interDivRisk = calculation.CalculationResult.AggregationRisk.InterProjectDiversifiedAggregationRisk;
            var riskId = calculation.CalculationResult.AggregationRisk.UndiversifiedAggregationRisk.RiskYearCollection[1].RiskYearValue;
            var risks = calculation.CalculationResult.AggregationRisk.RiskRegistrasi;

            worksheet8.Cell("B1").Value = "Risk-Capital Calculcation";
            worksheet8.Cell("B1").Style.Font.Bold = true;
            worksheet8.Cell("B1").Style.Font.FontColor = XLColor.FromArgb(231, 230, 230);
            worksheet8.Cell("B1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet8.Range(1, 1, 1, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(84, 130, 53);
            worksheet8.Cell("B2").Value = "Detail calculation of capital required (undiversified and diversified) each year by :\n- Risk in every particular project\n- Aggregation of every project\n- Agrregation of each risk\nCalculation of Asset projection each year up until project year end";
            worksheet8.Cell("B2").Style.Alignment.WrapText = true;
            worksheet8.Cell("B2").Style.Fill.BackgroundColor = XLColor.FromArgb(242, 242, 242);
            worksheet8.Range("B2", "F2").Merge();
            worksheet8.Row(2).Height = 90;
            worksheet8.Cell("B2").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet8.Cell("B4").Value = "Aggregation of Risk";
            worksheet8.Cell("B4").Style.Font.Bold = true;
            worksheet8.Cell("B4").Style.Font.Underline = XLFontUnderlineValues.Single;
            worksheet8.Range(4, 1, 4, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(248, 203, 173);


            worksheet8.Range(8 + risks.Length, 2, 9 + risks.Length, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(191, 191, 191);
            worksheet8.Cell(6, "B").Value = "Risk Categories";
            worksheet8.Cell(6, "B").Style.Font.Bold = true;
            worksheet8.Cell(6, "B").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet8.Range(6, 2, 7, 2).Merge();
            worksheet8.Cell(6, "C").Value = "Risk Category Description";
            worksheet8.Cell(6, "C").Style.Font.Bold = true;
            worksheet8.Cell(6, "C").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet8.Range(6, 3, 7, 7).Merge();
            worksheet8.Cell(6, "H").Value = "Undiversified Project Capital";
            worksheet8.Cell(6, "H").Style.Font.Bold = true;
            worksheet8.Cell(6, "H").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet8.Cell(6, "H").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet8.Range(6, 8, 6, 7 + years.Length).Merge();
            worksheet8.Cell(8 + risks.Length, "B").Value = "Total Undiversified Risk Capital";
            worksheet8.Cell(8 + risks.Length, "B").Style.Font.Bold = true;
            worksheet8.Range(8 + risks.Length, 2, 9 + risks.Length, 7).Merge();


            worksheet8.Range(13 + risks.Length * 2, 2, 14 + risks.Length * 2, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(191, 191, 191);
            worksheet8.Cell(11 + risks.Length, "B").Value = "Risk Categories";
            worksheet8.Cell(11 + risks.Length, "B").Style.Font.Bold = true;
            worksheet8.Cell(11 + risks.Length, "B").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet8.Range(11 + risks.Length, 2, 12 + risks.Length, 2).Merge();
            worksheet8.Cell(11 + risks.Length, "C").Value = "Risk Category Description";
            worksheet8.Cell(11 + risks.Length, "C").Style.Font.Bold = true;
            worksheet8.Cell(11 + risks.Length, "C").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet8.Range(11 + risks.Length, 3, 12 + risks.Length, 7).Merge();
            worksheet8.Cell(11 + risks.Length, "H").Value = "Intra-Project Diversified Project Capital";
            worksheet8.Cell(11 + risks.Length, "H").Style.Font.Bold = true;
            worksheet8.Cell(11 + risks.Length, "H").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet8.Cell(11 + risks.Length, "H").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet8.Range(11 + risks.Length, 8, 11 + risks.Length, 7 + years.Length).Merge();
            worksheet8.Cell(13 + risks.Length * 2, "B").Value = "Total Intra-Project Diversified Risk Capital";
            worksheet8.Cell(13 + risks.Length * 2, "B").Style.Font.Bold = true;
            worksheet8.Range(13 + risks.Length * 2, 2, 14 + risks.Length * 2, 7).Merge();


            worksheet8.Range(18 + risks.Length * 3, 2, 19 + risks.Length * 3, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(191, 191, 191);
            worksheet8.Cell(16 + risks.Length * 2, "B").Value = "Risk Categories";
            worksheet8.Cell(16 + risks.Length * 2, "B").Style.Font.Bold = true;
            worksheet8.Cell(16 + risks.Length * 2, "B").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet8.Range(16 + risks.Length * 2, 2, 17 + risks.Length * 2, 2).Merge();
            worksheet8.Cell(16 + risks.Length * 2, "C").Value = "Risk Category Description";
            worksheet8.Cell(16 + risks.Length * 2, "C").Style.Font.Bold = true;
            worksheet8.Cell(16 + risks.Length * 2, "C").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet8.Range(16 + risks.Length * 2, 3, 17 + risks.Length * 2, 7).Merge();
            worksheet8.Cell(16 + risks.Length * 2, "H").Value = "Inter-Project Diversified Project Capital";
            worksheet8.Cell(16 + risks.Length * 2, "H").Style.Font.Bold = true;
            worksheet8.Cell(16 + risks.Length * 2, "H").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet8.Cell(16 + risks.Length * 2, "H").Style.Fill.BackgroundColor = XLColor.FromArgb(155, 194, 230);
            worksheet8.Range(16 + risks.Length * 2, 8, 16 + risks.Length * 2, 7 + years.Length).Merge();
            worksheet8.Cell(18 + risks.Length * 3, "B").Value = "Total Inter-Project Diversified Risk Capital";
            worksheet8.Cell(18 + risks.Length * 3, "B").Style.Font.Bold = true;
            worksheet8.Range(18 + risks.Length * 3, 2, 19 + risks.Length * 3, 7).Merge();

            for (int i = 0; i < risks.Length; i++)
            {
                worksheet8.Cell(8 + i, "B").Value = risks[i].KodeMRisk;
                worksheet8.Cell(8 + i, "B").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 189);
                worksheet8.Range(8 + i, 2, 8 + i, 2).Merge();
                worksheet8.Cell(8 + i, "C").Value = risks[i].NamaCategoryRisk;
                worksheet8.Cell(8 + i, "C").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 189);
                worksheet8.Range(8 + i, 3, 8 + i, 7).Merge();

                worksheet8.Cell(13 + i + risks.Length, "B").Value = risks[i].KodeMRisk;
                worksheet8.Cell(13 + i + risks.Length, "B").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 189);
                worksheet8.Range(13 + i + risks.Length, 2, 13 + i + risks.Length, 2).Merge();
                worksheet8.Cell(13 + i + risks.Length, "C").Value = risks[i].NamaCategoryRisk;
                worksheet8.Cell(13 + i + risks.Length, "C").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 189);
                worksheet8.Range(13 + i + risks.Length, 3, 13 + i + risks.Length, 7).Merge();

                worksheet8.Cell(18 + i + risks.Length * 2, "B").Value = risks[i].KodeMRisk;
                worksheet8.Cell(18 + i + risks.Length * 2, "B").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 189);
                worksheet8.Range(18 + i + risks.Length * 2, 2, 18 + i + risks.Length * 2, 2).Merge();
                worksheet8.Cell(18 + i + risks.Length * 2, "C").Value = risks[i].NamaCategoryRisk;
                worksheet8.Cell(18 + i + risks.Length * 2, "C").Style.Fill.BackgroundColor = XLColor.FromArgb(255, 255, 189);
                worksheet8.Range(18 + i + risks.Length * 2, 3, 18 + i + risks.Length * 2, 7).Merge();
            }

            for (int i = 0; i < years.Length; i++)
            {
                worksheet8.Cell(7, 8 + i).Value = years[i];
                worksheet8.Cell(7, 8 + i).Style.Font.Bold = true;
                worksheet8.Cell(12 + risks.Length, 8 + i).Value = years[i];
                worksheet8.Cell(12 + risks.Length, 8 + i).Style.Font.Bold = true;
                worksheet8.Cell(17 + risks.Length * 2, 8 + i).Value = years[i];
                worksheet8.Cell(17 + risks.Length * 2, 8 + i).Style.Font.Bold = true;

                for (int j = 0; j < risks.Length; j++)
                {
                    worksheet8.Cell(8 + j, 8 + i).Value = "0";
                    worksheet8.Cell(13 + j + risks.Length, 8 + i).Value = "0";
                    worksheet8.Cell(18 + j + risks.Length * 2, 8 + i).Value = "0";
                }

                for (int j = 0; j < risks.Length; j++)
                {
                    worksheet8.Cell(8 + j, 8 + i).Value = undivRisk.RiskYearCollection[i].RiskYearValue[j].Value;
                    worksheet8.Cell(13 + j + risks.Length, 8 + i).Value = intraDivRisk.RiskYearCollection[i].RiskYearValue[j].Value;
                    worksheet8.Cell(18 + j + risks.Length * 2, 8 + i).Value = intraDivRisk.RiskYearCollection[i].RiskYearValue[j].Value;
                }
                worksheet8.Cell(8 + risks.Length, 8 + i).Value = undiv[i].TotalPerYear;
                worksheet8.Cell(8 + risks.Length, 8 + i).Style.Font.Bold = true;
                worksheet8.Range(8 + risks.Length, 8 + i, 9 + risks.Length, 8 + i).Merge();
                worksheet8.Cell(13 + risks.Length * 2, 8 + i).Value = intraDiv[i].TotalPerYear;
                worksheet8.Cell(13 + risks.Length * 2, 8 + i).Style.Font.Bold = true;
                worksheet8.Range(13 + risks.Length * 2, 8 + i, 14 + risks.Length * 2, 8 + i).Merge();
                worksheet8.Cell(18 + risks.Length * 3, 8 + i).Value = interDiv[i].TotalPerYear;
                worksheet8.Cell(18 + risks.Length * 3, 8 + i).Style.Font.Bold = true;
                worksheet8.Range(18 + risks.Length * 3, 8 + i, 19 + risks.Length * 3, 8 + i).Merge();
            }

            worksheet8.Range(6, 2, 9 + risks.Length, 7 + years.Length).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet8.Range(6, 2, 9 + risks.Length, 7 + years.Length).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

            worksheet8.Range(11 + risks.Length, 2, 14 + risks.Length * 2, 7 + years.Length).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet8.Range(11 + risks.Length, 2, 14 + risks.Length * 2, 7 + years.Length).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

            worksheet8.Range(16 + risks.Length * 2, 2, 19 + risks.Length * 3, 7 + years.Length).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet8.Range(16 + risks.Length * 2, 2, 19 + risks.Length * 3, 7 + years.Length).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

            worksheet8.Columns().AdjustToContents();
            worksheet8.Dispose();
            #endregion 8.KalkulasiAggregationOfRisk

            #region 9.KalkulasiAssetProjection
            var assetProjection = calculation.CalculationResult.AssetProjectionLiquidity;

            var worksheet9 = workbook.Worksheets.Add("KalkulasiAssetProjection");

            worksheet9.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            worksheet9.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            worksheet9.Cell("B1").Value = "Risk-Capital Calculcation";
            worksheet9.Cell("B1").Style.Font.Bold = true;
            worksheet9.Cell("B1").Style.Font.FontColor = XLColor.FromArgb(231, 230, 230);
            worksheet9.Cell("B1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet9.Range(1, 1, 1, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(84, 130, 53);
            worksheet9.Cell("B2").Value = "Detail calculation of capital required (undiversified and diversified) each year by :\n- Risk in every particular project\n- Aggregation of every project\n- Agrregation of each risk\nCalculation of Asset projection each year up until project year end";
            worksheet9.Cell("B2").Style.Alignment.WrapText = true;
            worksheet9.Cell("B2").Style.Fill.BackgroundColor = XLColor.FromArgb(242, 242, 242);
            worksheet9.Range("B2", "F2").Merge();
            worksheet9.Row(2).Height = 90;
            worksheet9.Cell("B2").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet9.Cell("B4").Value = "Asset Projection";
            worksheet9.Cell("B4").Style.Font.Bold = true;
            worksheet9.Cell("B4").Style.Font.Underline = XLFontUnderlineValues.Single;
            worksheet9.Cell("B4").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet9.Range(4, 1, 4, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(248, 203, 173);
            worksheet9.Cell("B5").Value = "Investment strategy - to to keep the balance of assets between categories";
            worksheet9.Cell("B5").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet9.Range("B5", "I5").Merge();


            worksheet9.Cell("B8").Value = "Asset Class";
            worksheet9.Cell("C8").Value = "Proportion";
            worksheet9.Cell("D8").Value = "Term";
            worksheet9.Cell("E8").Value = "Assumed Return";
            worksheet9.Cell("F8").Value = "Outstanding Years";
            worksheet9.Cell("G8").Value = "Asset Value";
            worksheet9.Range("B8", "G8").Style.Font.Bold = true;

            for (int i = 0; i < years.Length; i++)
            {
                worksheet9.Cell(8, 8 + i).Value = years[i];
                worksheet9.Cell(8, 8 + i).Style.Font.Bold = true;
                worksheet9.Cell(22, 8 + i).Value = years[i];
                worksheet9.Cell(22, 8 + i).Style.Font.Bold = true;
            }

            for (int i = 0; i < assetProjection.AssetClassDefault.Length; i++)
            {
                worksheet9.Cell(9 + i, 2).Value = assetProjection.AssetClassDefault[i].AssetClass;
                worksheet9.Cell(9 + i, 2).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                worksheet9.Cell(9 + i, 3).Value = assetProjection.AssetClassDefault[i].Proportion;
                worksheet9.Cell(9 + i, 3).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                worksheet9.Cell(9 + i, 4).Value = assetProjection.AssetClassDefault[i].TermAwal + "-" + assetProjection.AssetClassDefault[i].TermAkhir;
                worksheet9.Cell(9 + i, 5).Value = assetProjection.AssetClassDefault[i].AssumedReturn;
                worksheet9.Cell(9 + i, 5).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                worksheet9.Cell(9 + i, 6).Value = assetProjection.AssetClassDefault[i].OutstandingYearAwal + "-" + assetProjection.AssetClassDefault[i].OutstandingYearAkhir;
                worksheet9.Cell(9 + i, 7).Value = assetProjection.AssetClassDefault[i].AssetValue;
                worksheet9.Cell(9 + i, 7).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
            }

            for (int i = 0; i < assetProjection.AssetClassProjectionCollection.Length; i++)
            {
                for (int j = 0; j < assetProjection.AssetClassProjectionCollection[i].AssetList.Length; j++)
                {
                    worksheet9.Cell(9 + j, 8 + i).Value = assetProjection.AssetClassProjectionCollection[i].AssetList[j].Value;
                    worksheet9.Cell(9 + j, 8 + i).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                }
            }

            worksheet9.Cell("B15").Value = "Available capital Projected (allowing for reinvestment)";
            worksheet9.Range("B15", "F15").Merge();
            worksheet9.Cell("G15").Value = assetProjection.AssetSummaryDefault.AvailableCapitalProjected;
            worksheet9.Cell("G15").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
            worksheet9.Cell("B16").Value = "Total Undiversified Capital";
            worksheet9.Range("B16", "F16").Merge();
            worksheet9.Cell("B17").Value = "Total Diversified Capital (Risk Capital)";
            worksheet9.Range("B17", "F17").Merge();
            worksheet9.Cell("B18").Value = "Available capital Projected before opex, claim and recourse";
            worksheet9.Range("B18", "F18").Merge();
            worksheet9.Cell("B19").Value = "Operating expenses (annually growth)";
            worksheet9.Range("B19", "F19").Merge();
            worksheet9.Cell("G19").Value = assetProjection.AssetSummaryDefault.OperatingExpenses;
            worksheet9.Cell("G19").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
            worksheet9.Cell("B20").Value = "Available capital Projected before claim and recourse";
            worksheet9.Range("B20", "F20").Merge();
            worksheet9.Cell("G20").Value = assetProjection.AssetSummaryDefault.AvailableCapitalProjectedBeforeClaim;
            worksheet9.Cell("G20").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
            worksheet9.Range("B15", "F20").Style.Font.Bold = true;
            worksheet9.Range("B15", "F20").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;

            for (int i = 0; i < assetProjection.AssetSummaryCollection.Length; i++)
            {
                worksheet9.Cell(15, 8 + i).Value = assetProjection.AssetSummaryCollection[i].AvailableCapitalProjected;
                worksheet9.Cell(15, 8 + i).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                worksheet9.Cell(16, 8 + i).Value = assetProjection.AssetSummaryCollection[i].TotalUndiversifiedCapital;
                worksheet9.Cell(16, 8 + i).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                worksheet9.Cell(17, 8 + i).Value = assetProjection.AssetSummaryCollection[i].TotalDiversifiedCapital;
                worksheet9.Cell(17, 8 + i).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                worksheet9.Cell(18, 8 + i).Value = assetProjection.AssetSummaryCollection[i].AvailableCapitalProjectedBeforeOpex;
                worksheet9.Cell(18, 8 + i).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                worksheet9.Cell(19, 8 + i).Value = assetProjection.AssetSummaryCollection[i].OperatingExpenses;
                worksheet9.Cell(19, 8 + i).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                worksheet9.Cell(20, 8 + i).Value = assetProjection.AssetSummaryCollection[i].AvailableCapitalProjectedBeforeClaim;
                worksheet9.Cell(20, 8 + i).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
            }
            worksheet9.Range(8, 2, 20, 7 + years.Length).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet9.Range(8, 2, 20, 7 + years.Length).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;


            worksheet9.Range("B22", "G22").Merge();
            worksheet9.Cell("B23").Value = "Operating expenses (annually growth)";
            worksheet9.Range("B23", "G23").Merge();
            worksheet9.Cell("B24").Value = "Available capital Projected before claim and recourse";
            worksheet9.Range("B24", "G24").Merge();
            worksheet9.Range("B23", "G24").Style.Font.Bold = true;
            worksheet9.Range("B23", "G24").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;

            for (int i = 0; i < assetProjection.AssetProjectionIlustration.Length; i++)
            {
                worksheet9.Cell(23, 8 + i).Value = assetProjection.AssetProjectionIlustration[i].RecoursePayment;
                worksheet9.Cell(23, 8 + i).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                worksheet9.Cell(24, 8 + i).Value = assetProjection.AssetProjectionIlustration[i].AvailableCapitalRecourse;
                worksheet9.Cell(24, 8 + i).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
            }
            worksheet9.Range(22, 2, 24, 7 + years.Length).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet9.Range(22, 2, 24, 7 + years.Length).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

            worksheet9.Columns().AdjustToContents();
            worksheet9.Dispose();

            #endregion 9.KalkulasiAssetProjection

            #region 10.KalkulasiLiquidity 

            var worksheet10 = workbook.Worksheets.Add("KalkulasiLiquidity");

            worksheet10.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            worksheet10.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            worksheet10.Cell("B1").Value = "Risk-Capital Calculcation";
            worksheet10.Cell("B1").Style.Font.Bold = true;
            worksheet10.Cell("B1").Style.Font.FontColor = XLColor.FromArgb(231, 230, 230);
            worksheet10.Cell("B1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet10.Range(1, 1, 1, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(84, 130, 53);
            worksheet10.Cell("B2").Value = "Detail calculation of capital required (undiversified and diversified) each year by :\n- Risk in every particular project\n- Aggregation of every project\n- Agrregation of each risk\nCalculation of Asset projection each year up until project year end";
            worksheet10.Cell("B2").Style.Alignment.WrapText = true;
            worksheet10.Cell("B2").Style.Fill.BackgroundColor = XLColor.FromArgb(242, 242, 242);
            worksheet10.Range("B2", "F2").Merge();
            worksheet10.Row(2).Height = 90;
            worksheet10.Cell("B2").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet10.Cell("B4").Value = "Liquidity";
            worksheet10.Cell("B4").Style.Font.Bold = true;
            worksheet10.Cell("B4").Style.Font.Underline = XLFontUnderlineValues.Single;
            worksheet10.Cell("B4").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet10.Range(4, 1, 4, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(248, 203, 173);

            worksheet10.Cell("B6").Value = "Asset Class";
            worksheet10.Cell("C6").Value = "Proportion";
            worksheet10.Cell("D6").Value = "Term";
            worksheet10.Cell("E6").Value = "Assumed Return";
            worksheet10.Cell("F6").Value = "Outstanding Years";
            worksheet10.Cell("G6").Value = "Asset Value";
            worksheet10.Range("B6", "G6").Style.Font.Bold = true;

            for (int i = 0; i < years.Length; i++)
            {
                worksheet10.Cell(6, 8 + i).Value = years[i];
                worksheet10.Cell(6, 8 + i).Style.Font.Bold = true;
            }

            for (int i = 0; i < assetProjection.AssetClassDefault.Length; i++)
            {
                worksheet10.Cell(7 + i, 2).Value = assetProjection.AssetClassDefault[i].AssetClass;
                worksheet10.Cell(7 + i, 2).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                worksheet10.Cell(7 + i, 3).Value = assetProjection.AssetClassDefault[i].Proportion;
                worksheet10.Cell(7 + i, 3).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                worksheet10.Cell(7 + i, 4).Value = assetProjection.AssetClassDefault[i].TermAwal + "-" + assetProjection.AssetClassDefault[i].TermAkhir;
                worksheet10.Cell(7 + i, 5).Value = assetProjection.AssetClassDefault[i].AssumedReturn;
                worksheet10.Cell(7 + i, 5).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                worksheet10.Cell(7 + i, 6).Value = assetProjection.AssetClassDefault[i].OutstandingYearAwal + "-" + assetProjection.AssetClassDefault[i].OutstandingYearAkhir;
                worksheet10.Cell(7 + i, 7).Value = assetProjection.AssetClassDefault[i].AssetValue;
                worksheet10.Cell(7 + i, 7).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
            }

            for (int i = 0; i < assetProjection.AssetClassLiquidityCollection.Length; i++)
            {
                for (int j = 0; j < assetProjection.AssetClassLiquidityCollection[i].AssetList.Length; j++)
                {
                    worksheet10.Cell(7 + j, 8 + i).Value = assetProjection.AssetClassLiquidityCollection[i].AssetList[j].Value;
                    worksheet10.Cell(7 + j, 8 + i).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                }
            }

            worksheet10.Cell("B12").Value = "Liquid Assets";
            worksheet10.Range("B12", "G12").Merge();
            for (int i = 0; i < years.Length; i++)
            {
                worksheet10.Cell(12, 8 + i).Value = assetProjection.LiquidAsset[i].Value;
                worksheet10.Cell(12, 8 + i).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
            }

            worksheet10.Range(6, 2, 12, 7 + years.Length).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet10.Range(6, 2, 12, 7 + years.Length).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

            worksheet10.Columns().AdjustToContents();
            worksheet10.Dispose();

            #endregion 10.KalkulasiLiquidity

            #region 11.KalkulasiScenarioTesting 
            var scenarioTesting = calculation.ScenarioTesting;
            var worksheet11 = workbook.Worksheets.Add("KalkulasiScenarioTesting");

            worksheet11.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            worksheet11.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            worksheet11.Cell("B1").Value = "Risk-Capital Calculcation";
            worksheet11.Cell("B1").Style.Font.Bold = true;
            worksheet11.Cell("B1").Style.Font.FontColor = XLColor.FromArgb(231, 230, 230);
            worksheet11.Cell("B1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet11.Range(1, 1, 1, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(84, 130, 53);
            worksheet11.Cell("B2").Value = "Detail calculation of capital required (undiversified and diversified) each year by :\n- Risk in every particular project\n- Aggregation of every project\n- Agrregation of each risk\nCalculation of Asset projection each year up until project year end";
            worksheet11.Cell("B2").Style.Alignment.WrapText = true;
            worksheet11.Cell("B2").Style.Fill.BackgroundColor = XLColor.FromArgb(242, 242, 242);
            worksheet11.Range("B2", "F2").Merge();
            worksheet11.Row(2).Height = 90;
            worksheet11.Cell("B2").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet11.Cell("B4").Value = "Skenario Testing";
            worksheet11.Cell("B4").Style.Font.Bold = true;
            worksheet11.Cell("B4").Style.Font.Underline = XLFontUnderlineValues.Single;
            worksheet11.Cell("B4").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            worksheet11.Range(4, 1, 4, 7 + years.Length).Style.Fill.BackgroundColor = XLColor.FromArgb(248, 203, 173);
            worksheet11.Cell("B5").Value = "To calculate the scenario you need to\nRun a model under sertain likelihood\ncorresponding to values under the\nscenario";
            worksheet11.Cell("B5").Style.Alignment.WrapText = true;
            worksheet11.Row(5).Height = 60;

            worksheet11.Cell("B7").Value = "Nama Skenario";
            worksheet11.Cell("C7").Value = "#";
            for (int i = 0; i < years.Length; i++)
            {
                worksheet11.Cell(7, 4 + i).Value = years[i];
                worksheet11.Cell(11 + scenarioTesting.ScenarioTestingCollection.Length * 2, 5 + i).Value = years[i];
            }
            for (int i = 0; i < scenarioTesting.ScenarioTestingCollection.Length; i++)
            {
                worksheet11.Cell(8 + i * 2, 2).Value = scenarioTesting.ScenarioTestingCollection[i].NamaScenario;
                worksheet11.Range(8 + i * 2, 2, 9 + i * 2, 2).Merge();
                worksheet11.Cell(8 + i * 2, 3).Value = "Total Undiversified Capital";
                worksheet11.Cell(9 + i * 2, 3).Value = "Total Diversified Capital (Risk Capital)";
                for (int j = 0; j < scenarioTesting.ScenarioTestingCollection[i].ScenarioTestingYearCollection.Length; j++)
                {
                    worksheet11.Cell(8 + i * 2, 4 + j).Value = scenarioTesting.ScenarioTestingCollection[i].ScenarioTestingYearCollection[j].TotalUndiversifiedCapital;
                    worksheet11.Cell(9 + i * 2, 4 + j).Value = scenarioTesting.ScenarioTestingCollection[i].ScenarioTestingYearCollection[j].TotalDiversifiedCapital;
                }
            }

            worksheet11.Range(7, 2, 7 + scenarioTesting.ScenarioTestingCollection.Length * 2, 3 + years.Length).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet11.Range(7, 2, 7 + scenarioTesting.ScenarioTestingCollection.Length * 2, 3 + years.Length).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;


            worksheet11.Cell(9 + scenarioTesting.ScenarioTestingCollection.Length * 2, 2).Value = "Sensitivity";

            worksheet11.Cell(11 + scenarioTesting.ScenarioTestingCollection.Length * 2, 2).Value = "Diversivied/Undiversified";
            worksheet11.Cell(11 + scenarioTesting.ScenarioTestingCollection.Length * 2, 3).Value = "Scenario";
            worksheet11.Cell(11 + scenarioTesting.ScenarioTestingCollection.Length * 2, 4).Value = "Scenario Description";

            worksheet11.Cell(12 + scenarioTesting.ScenarioTestingCollection.Length * 2, 2).Value = "Undiversified Risk Capital";
            worksheet11.Range(12 + scenarioTesting.ScenarioTestingCollection.Length * 2, 2, 11 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 3, 2).Merge();

            worksheet11.Cell(12 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 3, 2).Value = "Diversified";
            worksheet11.Range(12 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 3, 2, 11 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6, 2).Merge();

            for (int i = 0; i < scenarioTesting.Sensitivity.Undiversified.Length; i++)
            {
                worksheet11.Cell(12 + scenarioTesting.ScenarioTestingCollection.Length * 2 + i * 3, 3).Value = scenarioTesting.Sensitivity.Undiversified[i].NamaScenario;
                worksheet11.Range(12 + scenarioTesting.ScenarioTestingCollection.Length * 2 + i * 3, 3, 14 + scenarioTesting.ScenarioTestingCollection.Length * 2 + i * 3, 3).Merge();

                worksheet11.Cell(12 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 3 + i * 3, 3).Value = scenarioTesting.Sensitivity.Undiversified[i].NamaScenario;
                worksheet11.Range(12 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 3 + i * 3, 3, 14 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 3 + i * 3, 3).Merge();

                worksheet11.Cell(12 + scenarioTesting.ScenarioTestingCollection.Length * 2 + i * 3, 4).Value = "Upper";
                worksheet11.Cell(13 + scenarioTesting.ScenarioTestingCollection.Length * 2 + i * 3, 4).Value = "Middle";
                worksheet11.Cell(14 + scenarioTesting.ScenarioTestingCollection.Length * 2 + i * 3, 4).Value = "Lower";

                worksheet11.Cell(12 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 3 + i * 3, 4).Value = "Upper";
                worksheet11.Cell(13 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 3 + i * 3, 4).Value = "Middle";
                worksheet11.Cell(14 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 3 + i * 3, 4).Value = "Lower";

                for (int j = 0; j < years.Length; j++)
                {
                    worksheet11.Cell(12 + scenarioTesting.ScenarioTestingCollection.Length * 2 + i * 3, 5 + j).Value = scenarioTesting.Sensitivity.Undiversified[i].UpperValue[j].Value;
                    worksheet11.Cell(13 + scenarioTesting.ScenarioTestingCollection.Length * 2 + i * 3, 5 + j).Value = scenarioTesting.Sensitivity.Undiversified[i].MidValue[j].Value;
                    worksheet11.Cell(14 + scenarioTesting.ScenarioTestingCollection.Length * 2 + i * 3, 5 + j).Value = scenarioTesting.Sensitivity.Undiversified[i].LowerValue[j].Value;

                    worksheet11.Cell(12 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 3 + i * 3, 5 + j).Value = scenarioTesting.Sensitivity.Diversified[i].UpperValue[j].Value;
                    worksheet11.Cell(13 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 3 + i * 3, 5 + j).Value = scenarioTesting.Sensitivity.Diversified[i].MidValue[j].Value;
                    worksheet11.Cell(14 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 3 + i * 3, 5 + j).Value = scenarioTesting.Sensitivity.Diversified[i].LowerValue[j].Value;
                }
            }
            worksheet11.Range(11 + scenarioTesting.ScenarioTestingCollection.Length * 2, 2, 11 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6, 4 + years.Length).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet11.Range(11 + scenarioTesting.ScenarioTestingCollection.Length * 2, 2, 11 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6, 4 + years.Length).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

            worksheet11.Cell(13 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6, 2).Value = "Stress Testing";

            worksheet11.Cell(15 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6, 2).Value = "Diversivied/Undiversified";
            worksheet11.Cell(15 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6, 3).Value = "Scenario";

            for (int i = 0; i < years.Length; i++)
            {
                worksheet11.Cell(15 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6, 4 + i).Value = years[i];
            }

            worksheet11.Cell(16 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6, 2).Value = "Risk Capital";
            worksheet11.Range(16 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6, 2, 16 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6 + scenarioTesting.StressTesting.RiskCapital.Length - 1, 2).Merge();

            worksheet11.Cell(16 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6 + scenarioTesting.StressTesting.RiskCapital.Length, 2).Value = "Available Capital";
            worksheet11.Range(16 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6 + scenarioTesting.StressTesting.RiskCapital.Length, 2, 16 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6 + scenarioTesting.StressTesting.RiskCapital.Length * 2 - 1, 2).Merge();

            worksheet11.Cell(16 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6 + scenarioTesting.StressTesting.RiskCapital.Length * 2, 2).Value = "Total Liquidity";
            worksheet11.Range(16 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6 + scenarioTesting.StressTesting.RiskCapital.Length * 2, 2, 16 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6 + scenarioTesting.StressTesting.RiskCapital.Length * 3 - 1, 2).Merge();

            for (int i = 0; i < scenarioTesting.StressTesting.RiskCapital.Length; i++)
            {
                worksheet11.Cell(16 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6 + i, 3).Value = scenarioTesting.StressTesting.RiskCapital[i].NamaScenario;
                worksheet11.Cell(16 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6 + scenarioTesting.StressTesting.RiskCapital.Length + i, 3).Value = scenarioTesting.StressTesting.AvailableCapital[i].NamaScenario;
                worksheet11.Cell(16 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6 + scenarioTesting.StressTesting.RiskCapital.Length * 2 + i, 3).Value = scenarioTesting.StressTesting.TotalLiquidity[i].NamaScenario;
                for (int j = 0; j < years.Length; j++)
                {
                    worksheet11.Cell(16 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6 + i, 4 + j).Value = scenarioTesting.StressTesting.RiskCapital[i].ValuePerYear[j].Value;
                    worksheet11.Cell(16 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6 + scenarioTesting.StressTesting.RiskCapital.Length + i, 4 + j).Value = scenarioTesting.StressTesting.AvailableCapital[i].ValuePerYear[j].Value;
                    worksheet11.Cell(16 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6 + scenarioTesting.StressTesting.RiskCapital.Length * 2 + i, 4 + j).Value = scenarioTesting.StressTesting.TotalLiquidity[i].ValuePerYear[j].Value;
                }
            }

            worksheet11.Range(15 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6, 2, 15 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6 + scenarioTesting.StressTesting.RiskCapital.Length * 3, 3 + years.Length).Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            worksheet11.Range(15 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6, 2, 15 + scenarioTesting.ScenarioTestingCollection.Length * 2 + scenarioTesting.Sensitivity.Undiversified.Length * 6 + scenarioTesting.StressTesting.RiskCapital.Length * 3, 3 + years.Length).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;


            worksheet11.Columns().AdjustToContents();
            worksheet11.Dispose();

            #endregion 11.KalkulasiScenarioTesting

            #endregion DEACTIVE

            //string exportDate;
            //exportDate = DateTime.Now.ToString("MMM d, yyyy hh-mm-ss");
            //var fileName = "RBC_PII_" + exportDate + ".xlsx";
            var fileName = "RBC_PII_Calculation.xlsx";
            string path = "~\\Temp\\Reports\\Calculation\\";
            if (!System.IO.Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath(path)))
            {
                System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(path));
            }
            var pathFile = System.Web.Hosting.HostingEnvironment.MapPath(path);
            DirectoryInfo directory = new DirectoryInfo(pathFile);
            //delete files:
            directory.GetFiles().ToList().ForEach(f => f.Delete());
            workbook.SaveAs(pathFile + fileName);

            workbook.Dispose();
        }
        public void ResultReport(Result result)
        {

            var years = result.LeverageRatio.YearCollectionLeverage;

            //Initialize a new Workboook object
            Workbook workbook = new Workbook();
            //Get the first worksheet

            Worksheet worksheet12 = workbook.Worksheets[0];
            workbook.Worksheets[1].Remove();
            workbook.Worksheets[1].Remove();

            worksheet12.Name = "Result";
            
            worksheet12.AllocatedRange.Style.Font.Size = 10;

            #region 1. Diversified Benefit Achieved

            var L1 = result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length;
            var L2 = result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedByProject.ProjectDiversifiedBenefitAchieved.Length;

            worksheet12.Range["B1"].Value = "Results";
            worksheet12.Range["B1"].Style.Font.IsBold = true;
            worksheet12.Range["B1"].Style.Font.Color = Color.FromArgb(255, 255, 255);
            worksheet12.Range["B1"].Style.HorizontalAlignment = HorizontalAlignType.Left;
            worksheet12.Range[1, 1, 1, 7 + years.Length].Style.Color = Color.FromArgb(163, 32, 32);
            worksheet12.Range["B2"].Value = "Summary of key results from the Risk-based capital framework";
            worksheet12.Range["B2"].Style.WrapText = true;
            worksheet12.Range[2, 2, 2, 4].Style.Color = Color.FromArgb(242, 242, 242);
            worksheet12.Range[2, 2, 2, 4].Merge();
            worksheet12.SetRowHeight(2, 38);
            worksheet12.Range["B2"].Style.HorizontalAlignment = HorizontalAlignType.Left;
            worksheet12.Range["B4"].Value = "1. Diversified Benefit Achieved";
            worksheet12.Range["B4"].Style.Font.Color = Color.FromArgb(255, 255, 255);
            worksheet12.Range["B4"].Style.Font.IsBold = true;
            worksheet12.Range["B4"].Style.HorizontalAlignment = HorizontalAlignType.Left;
            worksheet12.Range[4, 1, 4, 7 + years.Length].Style.Color = Color.FromArgb(200, 105, 0);

            worksheet12.Range["B6"].Value = "Sector";
            worksheet12.Range["B6"].Style.Font.IsBold = true;

            for (int i = 0; i < years.Length; i++)
            {
                worksheet12.Range[6, i + 3].Value2 = years[i].Year;
                worksheet12.Range[6, i + 3].Style.Font.IsBold = true;
                worksheet12.Range[7 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length, i + 3].Value2 = result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.YearSektorCollection[i].Total;
                worksheet12.Range[7 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length, i + 3].Style.Font.IsBold = true;


                worksheet12.Range[10 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length, i + 3].Value2 = years[i].Year;
                worksheet12.Range[10 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length, i + 3].Style.Font.IsBold = true;
                worksheet12.Range[11 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedByProject.ProjectDiversifiedBenefitAchieved.Length, i + 3].Value2 = result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedByProject.YearProjectCollection[i].Total;
                worksheet12.Range[11 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedByProject.ProjectDiversifiedBenefitAchieved.Length, i + 3].Style.Font.IsBold = true;

            }

            for (int i = 0; i < result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length; i++)
            {
                worksheet12.Range[7 + i, 2].Value = result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved[i].NamaSektor;
                for (int j = 0; j < result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.YearSektorCollection.Length; j++)
                {
                    worksheet12.Range[7 + i, 3 + j].Value2 = result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.YearSektorCollection[j].YearSektorValue[i].Value;

                }
            }
            worksheet12.Range[7 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length, 2].Value = "Diversification Benefit Achieved";
            worksheet12.Range[7 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length, 2].Style.Font.IsBold = true;

            worksheet12.Range[6, 2, 7 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length, 2 + years.Length].BorderAround(LineStyleType.Medium, Color.Black);
            worksheet12.Range[6, 2, 7 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length, 2 + years.Length].BorderInside(LineStyleType.Thin, Color.Black);


            worksheet12.Range[10 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length, 2].Value = "Project Name";
            worksheet12.Range[10 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length, 2].Style.Font.IsBold = true;

            for (int i = 0; i < result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedByProject.ProjectDiversifiedBenefitAchieved.Length; i++)
            {
                worksheet12.Range[11 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length + i, 2].Value = result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedByProject.ProjectDiversifiedBenefitAchieved[i].NamaProject;
                for (int j = 0; j < result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedByProject.YearProjectCollection.Length; j++)
                {
                    worksheet12.Range[11 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length + i, 3 + j].Value2 = result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedByProject.YearProjectCollection[j].YearProjectValue[i].Value;

                }
            }
            worksheet12.Range[11 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedByProject.ProjectDiversifiedBenefitAchieved.Length, 2].Value = "Diversification Benefit Achieved";
            worksheet12.Range[11 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedByProject.ProjectDiversifiedBenefitAchieved.Length, 2].Style.Font.IsBold = true;

            worksheet12.Range[10 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length, 2, 11 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedByProject.ProjectDiversifiedBenefitAchieved.Length, 2 + years.Length].BorderAround(LineStyleType.Medium, Color.Black);
            worksheet12.Range[10 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length, 2, 11 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedByProject.ProjectDiversifiedBenefitAchieved.Length, 2 + years.Length].BorderInside(LineStyleType.Thin, Color.Black);


            //Add chart and set chart data range  
            Chart chartDBA1 = worksheet12.Charts.Add(ExcelChartType.ColumnStacked);
            chartDBA1.DataRange = worksheet12.Range[7, 2, 6 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length, 2 + years.Length];
            chartDBA1.SeriesDataFromRange = true;

            for (int i = 0; i < result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length; i++)
            {
                ChartSerie csDBA1 = chartDBA1.Series[i];
                csDBA1.CategoryLabels = worksheet12.Range[6, 3, 6, 2 + years.Length];
            }
            
            chartDBA1.PlotArea.ForeGroundColor = Color.Transparent;
            //Chart position  
            chartDBA1.LeftColumn = 2;
            chartDBA1.TopRow = 13 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedByProject.ProjectDiversifiedBenefitAchieved.Length;
            chartDBA1.RightColumn = 9;
            chartDBA1.BottomRow = 28 + 12 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedByProject.ProjectDiversifiedBenefitAchieved.Length;
            //Chart title  
            chartDBA1.ChartTitle = "Diversification Achieved : Sector";
            chartDBA1.ChartTitleArea.Font.FontName = "Calibri";
            chartDBA1.ChartTitleArea.Font.Size = 13;
            chartDBA1.ChartTitleArea.Font.IsBold = true;
            //Chart axis  
            chartDBA1.PrimaryValueAxis.Title = "IN IDR BIO";
            chartDBA1.PrimaryValueAxis.Font.FontName = "Calibri";
            chartDBA1.PrimaryValueAxis.HasMinorGridLines = false;
            chartDBA1.PrimaryValueAxis.HasMajorGridLines = false;
            chartDBA1.PrimaryValueAxis.MaxValue = (double)Math.Ceiling(result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.YearSektorCollection.Max(x => x.Total) / 1000) * 1000;
            chartDBA1.PrimaryValueAxis.TitleArea.TextRotationAngle = -90;
            chartDBA1.PrimaryValueAxis.MinValue = 0;
            //Chart legend  
            chartDBA1.Legend.Position = LegendPositionType.Bottom;

            //Add chart and set chart data range  
            Chart chartDBA2 = worksheet12.Charts.Add(ExcelChartType.ColumnStacked);
            chartDBA2.DataRange = worksheet12.Range[11 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length, 2, 10 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedByProject.ProjectDiversifiedBenefitAchieved.Length, 2 + years.Length];
            chartDBA2.SeriesDataFromRange = true;

            for (int i = 0; i < result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedByProject.ProjectDiversifiedBenefitAchieved.Length; i++)
            {
                ChartSerie csDBA2 = chartDBA2.Series[i];
                csDBA2.CategoryLabels = worksheet12.Range[10 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length, 3, 10 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length, 2 + years.Length];
            }

            chartDBA2.PlotArea.ForeGroundColor = Color.Transparent;
            //Chart position  
            chartDBA2.LeftColumn = 10;
            chartDBA2.TopRow = 13 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedByProject.ProjectDiversifiedBenefitAchieved.Length;
            chartDBA2.RightColumn = 17;
            chartDBA2.BottomRow = 28 + 12 + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedByProject.ProjectDiversifiedBenefitAchieved.Length;
            //Chart title  
            chartDBA2.ChartTitle = "Diversification Achieved : Project";
            chartDBA2.ChartTitleArea.Font.FontName = "Calibri";
            chartDBA2.ChartTitleArea.Font.Size = 13;
            chartDBA2.ChartTitleArea.Font.IsBold = true;
            //Chart axis  
            chartDBA2.PrimaryValueAxis.Title = "IN IDR BIO";
            chartDBA2.PrimaryValueAxis.Font.FontName = "Calibri";
            chartDBA2.PrimaryValueAxis.HasMinorGridLines = false;
            chartDBA2.PrimaryValueAxis.HasMajorGridLines = false;
            chartDBA2.PrimaryValueAxis.MaxValue = (double)Math.Ceiling(result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedByProject.YearProjectCollection.Max(x => x.Total.Value) / 1000) * 1000;
            chartDBA2.PrimaryValueAxis.TitleArea.TextRotationAngle = -90;
            chartDBA2.PrimaryValueAxis.MinValue = 0;
            //Chart legend  
            chartDBA2.Legend.Position = LegendPositionType.Bottom;

            for (int i = 0; i < years.Length; i++)
            {
                worksheet12.Range[41 + L1 + L2, 4 * (i + 1)].Value2 = years[i].Year;
                worksheet12.Range[41 + L1 + L2, 4 * (i + 1) + 1].Value2 = years[i].Year;
                worksheet12.Range[41 + L1 + L2, 4 * (i + 1) + 2].Value2 = years[i].Year;

                worksheet12.Range[42 + L1 + L2, 4 * (i + 1)].Value2 = result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedCollection.UndiversifiedBenefitAchieved[i].Total;
                worksheet12.Range[43 + L1 + L2, 4 * (i + 1) + 1].Value2 = result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedCollection.DiversifiedBenefitAchieved[i].Total;
                worksheet12.Range[44 + L1 + L2, 4 * (i + 1) + 1].Value2 = result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedCollection.InterProjectBenefit[i].Total;
                worksheet12.Range[45 + L1 + L2, 4 * (i + 1) + 2].Value2 = result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedCollection.DiversifiedBenefitAchieved[i].Total;
            }
            worksheet12.Range[42 + L1 + L2, 2].Value = "Undiversified Risk Capital";
            worksheet12.Range[43 + L1 + L2, 2].Value = "Diversified Risk Capital";
            worksheet12.Range[44 + L1 + L2, 2].Value = "Inter-Project DB (Risk Capital)";
            worksheet12.Range[45 + L1 + L2, 2].Value = "Diversified";

            worksheet12.Range[41 + L1 + L2, 2, 45 + L1 + L2, 2 + years.Length * 4].BorderAround(LineStyleType.Medium, Color.Black);
            worksheet12.Range[41 + L1 + L2, 2, 45 + L1 + L2, 2 + years.Length * 4].BorderInside(LineStyleType.Thin, Color.Black);

            //Add chart and set chart data range  
            Chart chartDBA3 = worksheet12.Charts.Add(ExcelChartType.ColumnStacked);
            chartDBA3.DataRange = worksheet12.Range[42 + L1 + L2, 2, 45 + L1 + L2, 2 + years.Length * 4];
            chartDBA3.SeriesDataFromRange = true;

            for (int i = 0; i < 4; i++)
            {
                ChartSerie csDBA3 = chartDBA3.Series[i];
                csDBA3.CategoryLabels = worksheet12.Range[41 + L1 + L2, 3, 41 + L1 + L2, 2 + years.Length * 4];
            }

            chartDBA3.PlotArea.ForeGroundColor = Color.Transparent;
            //Chart position  
            chartDBA3.LeftColumn = 2;
            chartDBA3.TopRow = 47 + L1 + L2;
            chartDBA3.RightColumn = 9;
            chartDBA3.BottomRow = 72 + L1 + L2;
            //Chart title  
            chartDBA3.ChartTitle = "Diversification Benefit Achieved";
            chartDBA3.ChartTitleArea.Font.FontName = "Calibri";
            chartDBA3.ChartTitleArea.Font.Size = 13;
            chartDBA3.ChartTitleArea.Font.IsBold = true;
            //Chart axis  
            chartDBA3.PrimaryValueAxis.Title = "RISK CAPITAL";
            chartDBA3.PrimaryValueAxis.Font.FontName = "Calibri";
            chartDBA3.PrimaryValueAxis.HasMinorGridLines = false;
            chartDBA3.PrimaryValueAxis.HasMajorGridLines = false;
            chartDBA3.PrimaryValueAxis.MaxValue = (double)Math.Ceiling(result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedCollection.UndiversifiedBenefitAchieved.Max(x => x.Total.Value) / 1000) * 1000;
            chartDBA3.PrimaryValueAxis.TitleArea.TextRotationAngle = -90;
            chartDBA3.PrimaryValueAxis.MinValue = 0;
            //Chart legend  
            chartDBA3.Legend.Position = LegendPositionType.Corner;

            #endregion 1. Diversified Benefit Achieved

            #region 2. Leverage Ratios

            worksheet12.Range[74 + L1 + L2, 2].Value = "2. Leverage Ratios";
            worksheet12.Range[74 + L1 + L2, 2].Style.Font.Color = Color.FromArgb(255, 255, 255);
            worksheet12.Range[74 + L1 + L2, 2].Style.Font.IsBold = true;
            worksheet12.Range[74 + L1 + L2, 2].Style.HorizontalAlignment = HorizontalAlignType.Left;
            worksheet12.Range[74 + L1 + L2, 1, 74 + L1 + L2, 7 + years.Length].Style.Color = Color.FromArgb(200, 105, 0);

            worksheet12.Range[77 + L1 + L2, 2].Value = "Total Undiversified Risk Capital";
            worksheet12.Range[78 + L1 + L2, 2].Value = "Total Diversified Risk Capital";
            worksheet12.Range[79 + L1 + L2, 2].Value = "Available Capital";
            worksheet12.Range[80 + L1 + L2, 2].Value = "Total Maximum Exposure*";
            worksheet12.Range[81 + L1 + L2, 2].Value = "Diversification Ratio";
            worksheet12.Range[82 + L1 + L2, 2].Value = "Mitigation Effectiveness";
            worksheet12.Range[83 + L1 + L2, 2].Value = "Leverage Ratio";

            for (int i = 0; i < years.Length; i++)
            {
                worksheet12.Range[76 + L1 + L2, 3 + i].Value2 = years[i].Year;
                for (int j = 0; j < result.LeverageRatio.LeverageItem.Length; j++)
                {
                    switch (result.LeverageRatio.LeverageItem[j].Name)
                    {
                        case "Total Undiversified Risk Capital":
                            worksheet12.Range[77 + L1 + L2, 3 + i].Value2 = result.LeverageRatio.LeverageItem[j].Values[i].Value;
                            break;
                        case "Total Diversified Risk Capital":
                            worksheet12.Range[78 + L1 + L2, 3 + i].Value2 = result.LeverageRatio.LeverageItem[j].Values[i].Value;
                            break;
                        case "Available Capital":
                            worksheet12.Range[79 + L1 + L2, 3 + i].Value2 = result.LeverageRatio.LeverageItem[j].Values[i].Value;
                            break;
                        case "Total Maximum Exposure":
                            worksheet12.Range[80 + L1 + L2, 3 + i].Value2 = result.LeverageRatio.LeverageItem[j].Values[i].Value;
                            break;
                        case "Diversification Ratio":
                            worksheet12.Range[81 + L1 + L2, 3 + i].Value2 = result.LeverageRatio.LeverageItem[j].Values[i].Value;
                            break;
                        case "Mitigation Effectiveness":
                            worksheet12.Range[82 + L1 + L2, 3 + i].Value2 = result.LeverageRatio.LeverageItem[j].Values[i].Value;
                            break;
                        case "Leveratio Ratio":
                            worksheet12.Range[83 + L1 + L2, 3 + i].Value2 = result.LeverageRatio.LeverageItem[j].Values[i].Value;
                            break;
                    }
                }
            }

            worksheet12.Range[76 + L1 + L2, 2, 83 + L1 + L2, 2 + years.Length].BorderAround(LineStyleType.Medium, Color.Black);
            worksheet12.Range[76 + L1 + L2, 2, 83 + L1 + L2, 2 + years.Length].BorderInside(LineStyleType.Thin, Color.Black);
            worksheet12.Range[84 + L1 + L2, 2].Value = "* curently Total exposure = maximum exposure (not allowing for cases when where is a need to pay for several events over one year)";
            worksheet12.Range[84 + L1 + L2, 2].Style.Font.Color = Color.Red;
            worksheet12.Range[84 + L1 + L2, 2, 84 + L1 + L2, 6].Merge();

            //Add chart and set chart data range  
            Chart chartDBA4 = worksheet12.Charts.Add(ExcelChartType.Area);
            chartDBA4.DataRange = worksheet12.Range[77 + L1 + L2, 3, 78 + L1 + L2, 3 + years.Length];
            chartDBA4.SeriesDataFromRange = true;

            for (int i = 0; i < 2; i++)
            {
                ChartSerie csDBA4 = chartDBA4.Series[0];
                csDBA4.CategoryLabels = worksheet12.Range[76 + L1 + L2, 3, 76 + L1 + L2, 3 + years.Length];
            }

            chartDBA4.PlotArea.ForeGroundColor = Color.Transparent;
            //Chart position  
            chartDBA4.LeftColumn = 10;
            chartDBA4.TopRow = 47 + L1 + L2;
            chartDBA4.RightColumn = 17;
            chartDBA4.BottomRow = 72 + L1 + L2;
            //Chart title  
            chartDBA4.ChartTitle = "Diversification Benefit Achieved";
            chartDBA4.ChartTitleArea.Font.FontName = "Calibri";
            chartDBA4.ChartTitleArea.Font.Size = 13;
            chartDBA4.ChartTitleArea.Font.IsBold = true;
            //Chart axis  
            chartDBA4.PrimaryValueAxis.Title = "RISK CAPITAL";
            chartDBA4.PrimaryValueAxis.Font.FontName = "Calibri";
            chartDBA4.PrimaryValueAxis.HasMajorGridLines = false;
            chartDBA4.PrimaryValueAxis.MaxValue = (double)Math.Ceiling(result.LeverageRatio.LeverageItem.Where(y => y.Name == "Total Undiversified Risk Capital").Single().Values.Max(x => x.Value) / 1000) * 1000;
            chartDBA4.PrimaryValueAxis.TitleArea.TextRotationAngle = -90;
            chartDBA4.PrimaryValueAxis.MinValue = 0;

            //Add chart and set chart data range  
            Chart chartDR = worksheet12.Charts.Add(ExcelChartType.LineMarkers);
            chartDR.DataRange = worksheet12.Range[81 + L1 + L2, 3, 81 + L1 + L2, 3 + years.Length];
            chartDR.SeriesDataFromRange = true;

            ChartSerie csDR = chartDR.Series[0];
            csDR.CategoryLabels = worksheet12.Range[76 + L1 + L2, 3, 76 + L1 + L2, 3 + years.Length];

            chartDR.PlotArea.ForeGroundColor = Color.Transparent;
            //Chart position  
            chartDR.LeftColumn = 2;
            chartDR.TopRow = 86 + L1 + L2;
            chartDR.RightColumn = 9;
            chartDR.BottomRow = 106 + L1 + L2;
            //Chart title  
            chartDR.ChartTitle = "Diversification Ratio";
            chartDR.ChartTitleArea.Font.FontName = "Calibri";
            chartDR.ChartTitleArea.Font.Size = 13;
            chartDR.ChartTitleArea.Font.IsBold = true;
            chartDR.PrimaryValueAxis.HasMajorGridLines = false;
            chartDR.PrimaryValueAxis.Deleted = true;
            chartDR.HasLegend = false;

            //Add chart and set chart data range  
            Chart chartLR = worksheet12.Charts.Add(ExcelChartType.LineMarkers);
            chartLR.DataRange = worksheet12.Range[83 + L1 + L2, 3, 83 + L1 + L2, 3 + years.Length];
            chartLR.SeriesDataFromRange = true;

            ChartSerie csLR = chartLR.Series[0];
            csLR.CategoryLabels = worksheet12.Range[76 + L1 + L2, 3, 76 + L1 + L2, 3 + years.Length];

            chartLR.PlotArea.ForeGroundColor = Color.Transparent;
            //Chart position  
            chartLR.LeftColumn = 10;
            chartLR.TopRow = 86 + L1 + L2;
            chartLR.RightColumn = 17;
            chartLR.BottomRow = 106 + L1 + L2;
            //Chart title  
            chartLR.ChartTitle = "Leverage Ratio";
            chartLR.ChartTitleArea.Font.FontName = "Calibri";
            chartLR.ChartTitleArea.Font.Size = 13;
            chartLR.ChartTitleArea.Font.IsBold = true;
            chartLR.PrimaryValueAxis.HasMajorGridLines = false;
            chartLR.PrimaryValueAxis.Deleted = true;
            chartLR.HasLegend = false;

            //Add chart and set chart data range  
            Chart chartME = worksheet12.Charts.Add(ExcelChartType.LineMarkers);
            chartME.DataRange = worksheet12.Range[82 + L1 + L2, 3, 82 + L1 + L2, 3 + years.Length];
            chartME.SeriesDataFromRange = true;

            ChartSerie csME = chartME.Series[0];
            csME.CategoryLabels = worksheet12.Range[76 + L1 + L2, 3, 76 + L1 + L2, 3 + years.Length];

            chartME.PlotArea.ForeGroundColor = Color.Transparent;
            //Chart position  
            chartME.LeftColumn = 2;
            chartME.TopRow = 107 + L1 + L2;
            chartME.RightColumn = 9;
            chartME.BottomRow = 127 + L1 + L2;
            //Chart title  
            chartME.ChartTitle = "Mitigation Effectiveness";
            chartME.ChartTitleArea.Font.FontName = "Calibri";
            chartME.ChartTitleArea.Font.Size = 13;
            chartME.ChartTitleArea.Font.IsBold = true;
            chartME.PrimaryValueAxis.HasMajorGridLines = false;
            chartME.PrimaryValueAxis.Deleted = true;
            chartME.HasLegend = false;
            #endregion 2. Leverage Ratios 

            #region 3. Available Capital vs Risk Capital Projection
            worksheet12.Range[129 + L1 + L2, 2].Value = "3. Available Capital vs Risk Capital Projection";
            worksheet12.Range[129 + L1 + L2, 2].Style.Font.Color = Color.FromArgb(255, 255, 255);
            worksheet12.Range[129 + L1 + L2, 2].Style.Font.IsBold = true;
            worksheet12.Range[129 + L1 + L2, 2].Style.HorizontalAlignment = HorizontalAlignType.Left;
            worksheet12.Range[129 + L1 + L2, 1, 129 + L1 + L2, 7 + years.Length].Style.Color = Color.FromArgb(200, 105, 0);

            worksheet12.Range[132 + L1 + L2, 2].Value = "Available Capital";
            worksheet12.Range[133 + L1 + L2, 2].Value = "Undiversified Risk Capital";
            worksheet12.Range[134 + L1 + L2, 2].Value = "Diversified Risk Capital";
            worksheet12.Range[135 + L1 + L2, 2].Value = "Total Maximum Exposure";

            for (int i = 0; i < years.Length; i++)
            {
                worksheet12.Range[131 + L1 + L2, 3 + i].Value2 = years[i].Year;
                for (int j = 0; j < result.AvailableVSRiskCapital.AvailableVSRiskCapitalCollection.Length; j++)
                {
                    switch (result.AvailableVSRiskCapital.AvailableVSRiskCapitalCollection[j].Name)
                    {
                        case "Available Capital":
                            worksheet12.Range[132 + L1 + L2, 3 + i].Value2 = result.AvailableVSRiskCapital.AvailableVSRiskCapitalCollection[j].Values[i].Value;
                            break;
                        case "Undiversified Risk Capital":
                            worksheet12.Range[133 + L1 + L2, 3 + i].Value2 = result.AvailableVSRiskCapital.AvailableVSRiskCapitalCollection[j].Values[i].Value;
                            break;
                        case "Diversified Risk Capital":
                            worksheet12.Range[134 + L1 + L2, 3 + i].Value2 = result.AvailableVSRiskCapital.AvailableVSRiskCapitalCollection[j].Values[i].Value;
                            break;
                        case "Total Maximum Exposure":
                            worksheet12.Range[135 + L1 + L2, 3 + i].Value2 = result.AvailableVSRiskCapital.AvailableVSRiskCapitalCollection[j].Values[i].Value;
                            break;
                    }
                }
            }

            worksheet12.Range[131 + L1 + L2, 2, 135 + L1 + L2, 2 + years.Length].BorderAround(LineStyleType.Medium, Color.Black);
            worksheet12.Range[131 + L1 + L2, 2, 135 + L1 + L2, 2 + years.Length].BorderInside(LineStyleType.Thin, Color.Black);

            //Add chart and set chart data range  
            Chart chartARC = worksheet12.Charts.Add();
            chartARC.DataRange = worksheet12.Range[132 + L1 + L2, 2, 135 + L1 + L2, 2 + years.Length];
            
            var csARC1 = chartARC.Series[0];
            csARC1.SerieType = ExcelChartType.LineStacked;
            csARC1.CategoryLabels = worksheet12.Range[131 + L1 + L2, 3, 131 + L1 + L2, 2 + years.Length];

            var csARC2 = chartARC.Series[2];
            csARC2.SerieType = ExcelChartType.ColumnClustered;
            csARC2.CategoryLabels = worksheet12.Range[131 + L1 + L2, 3, 131 + L1 + L2, 2 + years.Length];

            var csARC3 = chartARC.Series[3];
            csARC3.SerieType = ExcelChartType.ColumnClustered;
            csARC3.CategoryLabels = worksheet12.Range[131 + L1 + L2, 3, 131 + L1 + L2, 2 + years.Length];

            chartARC.Series.RemoveAt(1);

            chartARC.SeriesDataFromRange = true;
            chartARC.PlotArea.ForeGroundColor = Color.Transparent;
            //Chart position  
            chartARC.LeftColumn = 2;
            chartARC.TopRow = 137 + L1 + L2;
            chartARC.RightColumn = 9;
            chartARC.BottomRow = 157 + L1 + L2;
            //Chart title  
            chartARC.ChartTitle = "Available vs Risk Capital";
            chartARC.ChartTitleArea.Font.FontName = "Calibri";
            chartARC.ChartTitleArea.Font.Size = 13;
            chartARC.ChartTitleArea.Font.IsBold = true;
            ////Chart axis  
            chartARC.PrimaryValueAxis.HasMajorGridLines = false;
            chartARC.PrimaryValueAxis.HasMinorGridLines = false;
            chartARC.PrimaryValueAxis.MaxValue = (double)Math.Ceiling(result.AvailableVSRiskCapital.AvailableVSRiskCapitalCollection.Where(y => y.Name == "Total Maximum Exposure").Single().Values.Max(x => x.Value) / 1000) * 1000;
            chartARC.PrimaryValueAxis.MinValue = 0;
            //Chart legend  
            chartARC.Legend.Position = LegendPositionType.Bottom;

            #endregion 3. Available Capital vs Risk Capital Projection

            #region 4. Liquidity
            worksheet12.Range[159 + L1 + L2, 2].Value = "4. Liquidity";
            worksheet12.Range[159 + L1 + L2, 2].Style.Font.Color = Color.FromArgb(255, 255, 255);
            worksheet12.Range[159 + L1 + L2, 2].Style.Font.IsBold = true;
            worksheet12.Range[159 + L1 + L2, 2].Style.HorizontalAlignment = HorizontalAlignType.Left;
            worksheet12.Range[159 + L1 + L2, 1, 159 + L1 + L2, 7 + years.Length].Style.Color = Color.FromArgb(200, 105, 0);

            worksheet12.Range[162 + L1 + L2, 2].Value = "Risk Capital";
            worksheet12.Range[163 + L1 + L2, 2].Value = "Liquidity";

            for (int i = 0; i < years.Length; i++)
            {
                worksheet12.Range[161 + L1 + L2, 3 + i].Value2 = years[i].Year;
                for (int j = 0; j < result.Liquidity.LiquidityCollection.Length; j++)
                {
                    switch (result.Liquidity.LiquidityCollection[j].Name)
                    {
                        case "Risk Capital":
                            worksheet12.Range[162 + L1 + L2, 3 + i].Value2 = result.Liquidity.LiquidityCollection[j].Values[i].Value;
                            break;
                        case "Liquidity":
                            worksheet12.Range[163 + L1 + L2, 3 + i].Value2 = result.Liquidity.LiquidityCollection[j].Values[i].Value;
                            break;
                    }
                }
            }

            worksheet12.Range[161 + L1 + L2, 2, 163 + L1 + L2, 2 + years.Length].BorderAround(LineStyleType.Medium, Color.Black);
            worksheet12.Range[161 + L1 + L2, 2, 163 + L1 + L2, 2 + years.Length].BorderInside(LineStyleType.Thin, Color.Black);

            //Add chart and set chart data range  
            Chart chartL = worksheet12.Charts.Add();
            chartL.DataRange = worksheet12.Range[162 + L1 + L2, 2, 163 + L1 + L2, 2 + years.Length];
            chartL.SeriesDataFromRange = true;

            ChartSerie csL1 = chartL.Series[0];
            csL1.SerieType = ExcelChartType.ColumnClustered;
            csL1.CategoryLabels = worksheet12.Range[161 + L1 + L2, 3, 161 + L1 + L2, 2 + years.Length];

            ChartSerie csL2 = chartL.Series[1];
            csL2.SerieType = ExcelChartType.LineStacked;
            csL2.CategoryLabels = worksheet12.Range[161 + L1 + L2, 3, 161 + L1 + L2, 2 + years.Length];

            chartL.PlotArea.ForeGroundColor = Color.Transparent;
            //Chart position  
            chartL.LeftColumn = 2;
            chartL.TopRow = 165 + L1 + L2;
            chartL.RightColumn = 9;
            chartL.BottomRow = 185 + L1 + L2;
            //Chart title  
            chartL.ChartTitle = "Risk Capital vs Liquidity";
            chartL.ChartTitleArea.Font.FontName = "Calibri";
            chartL.ChartTitleArea.Font.Size = 13;
            chartL.ChartTitleArea.Font.IsBold = true;
            //Chart axis  
            chartL.PrimaryValueAxis.HasMajorGridLines = false;
            chartL.PrimaryValueAxis.HasMinorGridLines = false;
            chartL.PrimaryValueAxis.MaxValue = (double)Math.Ceiling(result.Liquidity.LiquidityCollection.Where(y => y.Name == "Liquidity").Single().Values.Max(x => x.Value) / 1000) * 1000;
            chartL.PrimaryValueAxis.MinValue = 0;
            //Chart legend  
            chartL.Legend.Position = LegendPositionType.Bottom;
            #endregion 4. Liquidity

            #region 5. Sensitivity
            worksheet12.Range[187 + L1 + L2, 2].Value = "5. Sensitivity";
            worksheet12.Range[187 + L1 + L2, 2].Style.Font.Color = Color.FromArgb(255, 255, 255);
            worksheet12.Range[187 + L1 + L2, 2].Style.Font.IsBold = true;
            worksheet12.Range[187 + L1 + L2, 2].Style.HorizontalAlignment = HorizontalAlignType.Left;
            worksheet12.Range[187 + L1 + L2, 1, 187 + L1 + L2, 7 + years.Length].Style.Color = Color.FromArgb(200, 105, 0);

            worksheet12.Range[189 + L1 + L2, 3].Value = "Risk Capital";
            worksheet12.Range[189 + L1 + L2, 3].Style.Font.IsBold = true;

            for (int i = 0; i < result.SensitivityStressTesting.SensitivityResult.SensitivityStressScenarioCollection.Length; i++)
            {
                worksheet12.Range[191 + L1 + L2 + i, 2].Value = result.SensitivityStressTesting.SensitivityResult.SensitivityStressScenarioCollection[i].NamaScenario;
            }

            var L3 = result.SensitivityStressTesting.SensitivityResult.SensitivityStressScenarioCollection.Length;

            worksheet12.Range[191 + L1 + L2 + L3, 2].Value = "Available Capital";
            worksheet12.Range[192 + L1 + L2 + L3, 2].Value = "Liquidity";

            for (int i = 0; i < years.Length; i++)
            {
                worksheet12.Range[190 + L1 + L2, 3 + i].Value2 = years[i].Year;
                for (int j = 0; j < L3; j++)
                {
                    worksheet12.Range[191 + L1 + L2 + j, 3 + i].Value2 = result.SensitivityStressTesting.SensitivityResult.SensitivityStressScenarioCollection[j].Values[i].Value;
                }
                for (int j = 0; j < result.SensitivityStressTesting.SensitivityResult.SensitivityStressCollection.Length; j++)
                {
                    switch (result.SensitivityStressTesting.SensitivityResult.SensitivityStressCollection[j].Name)
                    {
                        case "Available Capital":
                            worksheet12.Range[191 + L1 + L2 + L3, 3 + i].Value2 = result.SensitivityStressTesting.SensitivityResult.SensitivityStressCollection[j].Values[i].Value;
                            break;
                        case "Liquidity":
                            worksheet12.Range[192 + L1 + L2 + L3, 3 + i].Value2 = result.SensitivityStressTesting.SensitivityResult.SensitivityStressCollection[j].Values[i].Value;
                            break;
                    }
                }
            }

            worksheet12.Range[190 + L1 + L2, 2, 192 + L1 + L2 + L3, 2 + years.Length].BorderAround(LineStyleType.Medium, Color.Black);
            worksheet12.Range[190 + L1 + L2, 2, 192 + L1 + L2 + L3, 2 + years.Length].BorderInside(LineStyleType.Thin, Color.Black);

            for (int i = 0; i < L3; i++)
            {
                //Add chart and set chart data range  
                Chart chartS = worksheet12.Charts.Add();
                chartS.DataRange = worksheet12.Range[191 + L1 + L2, 2, 192 + L1 + L2 + L3, 2 + years.Length];
                chartS.SeriesDataFromRange = true;
                
                ChartSerie csS1 = chartS.Series[i];
                csS1.SerieType = ExcelChartType.ColumnClustered;
                csS1.CategoryLabels = worksheet12.Range[190 + L1 + L2, 3, 190 + L1 + L2, 2 + years.Length];
                csS1.Name = "Risk Capital";

                ChartSerie csS2 = chartS.Series[L3 + 1];
                csS2.SerieType = ExcelChartType.Line;
                csS2.CategoryLabels = worksheet12.Range[190 + L1 + L2, 3, 190 + L1 + L2, 2 + years.Length];
                csS2.Name = "Liquidity";

                chartS.PlotArea.ForeGroundColor = Color.Transparent;//Chart position  
                chartS.LeftColumn = 2 + i * 4;
                chartS.TopRow = 194 + L1 + L2 + L3;
                chartS.RightColumn = 5 + i * 4;
                chartS.BottomRow = 213 + L1 + L2 + L3;
                //Chart title  
                chartS.ChartTitle = result.SensitivityStressTesting.SensitivityResult.SensitivityStressScenarioCollection[i].NamaScenario;
                chartS.ChartTitleArea.Font.FontName = "Calibri";
                chartS.ChartTitleArea.Font.Size = 13;
                chartS.ChartTitleArea.Font.IsBold = true;
                //Chart axis  
                chartS.PrimaryValueAxis.HasMajorGridLines = false;
                chartS.PrimaryValueAxis.HasMinorGridLines = false;
                chartS.PrimaryValueAxis.MaxValue = (double)Math.Ceiling(result.SensitivityStressTesting.SensitivityResult.SensitivityStressCollection.Where(y => y.Name == "Liquidity").Single().Values.Max(x => x.Value) / 1000) * 1000;
                chartS.PrimaryValueAxis.MinValue = 0;
                //Chart legend  
                chartS.Legend.Position = LegendPositionType.Bottom;

                if (L3 == 3)
                {
                    switch (i)
                    {
                        case 0:
                            chartS.Series.RemoveAt(1);
                            chartS.Series.RemoveAt(1);
                            chartS.Series.RemoveAt(1);
                            break;
                        case 1:
                            chartS.Series.RemoveAt(0);
                            chartS.Series.RemoveAt(1);
                            chartS.Series.RemoveAt(1);
                            break;
                        case 2:
                            chartS.Series.RemoveAt(0);
                            chartS.Series.RemoveAt(0);
                            chartS.Series.RemoveAt(1);
                            break;
                    }
                }
                if (L3 == 4)
                {
                    switch (i)
                    {
                        case 0:
                            chartS.Series.RemoveAt(1);
                            chartS.Series.RemoveAt(1);
                            chartS.Series.RemoveAt(1);
                            chartS.Series.RemoveAt(1);
                            break;
                        case 1:
                            chartS.Series.RemoveAt(0);
                            chartS.Series.RemoveAt(1);
                            chartS.Series.RemoveAt(1);
                            chartS.Series.RemoveAt(1);
                            break;
                        case 2:
                            chartS.Series.RemoveAt(0);
                            chartS.Series.RemoveAt(0);
                            chartS.Series.RemoveAt(1);
                            chartS.Series.RemoveAt(1);
                            break;
                        case 3:
                            chartS.Series.RemoveAt(0);
                            chartS.Series.RemoveAt(0);
                            chartS.Series.RemoveAt(0);
                            chartS.Series.RemoveAt(1);
                            break;
                    }
                }
            }

            #endregion 5. Sensitivity

            #region 5. Stress Testing
            worksheet12.Range[215 + L1 + L2 + L3, 2].Value = "5. Stress Testing";
            worksheet12.Range[215 + L1 + L2 + L3, 2].Style.Font.Color = Color.FromArgb(255, 255, 255);
            worksheet12.Range[215 + L1 + L2 + L3, 2].Style.Font.IsBold = true;
            worksheet12.Range[215 + L1 + L2 + L3, 2].Style.HorizontalAlignment = HorizontalAlignType.Left;
            worksheet12.Range[215 + L1 + L2 + L3, 1, 215 + L1 + L2 + L3, 7 + years.Length].Style.Color = Color.FromArgb(200, 105, 0);

            for (int i = 0; i < L3; i++)
            {
                worksheet12.Range[218 + L1 + L2 + L3 + i, 3].Value = result.SensitivityStressTesting.StressTestingResult.RiskCapitalCollection[i].NamaScenario;
                worksheet12.Range[218 + L1 + L2 + L3 * 2 + i, 3].Value = result.SensitivityStressTesting.StressTestingResult.RiskCapitalCollection[i].NamaScenario;
            }

            worksheet12.Range[218 + L1 + L2 + L3, 2].Value = "Risk Capital";
            worksheet12.Range[218 + L1 + L2 + L3 * 2, 2].Value = "Available Capital";
            worksheet12.Range[218 + L1 + L2 + L3 * 3, 2].Value = "Liquidity";

            IList<decimal> avcValues = new List<decimal>();
            for (int i = 0; i < years.Length; i++)
            {
                worksheet12.Range[217 + L1 + L2 + L3, 4 + i].Value2 = years[i].Year;
                for (int j = 0; j < L3; j++)
                {
                    worksheet12.Range[218 + L1 + L2 + L3 + j, 4 + i].Value2 = result.SensitivityStressTesting.StressTestingResult.RiskCapitalCollection[j].Values[i].Value;
                    worksheet12.Range[218 + L1 + L2 + L3 * 2 + j, 4 + i].Value2 = result.SensitivityStressTesting.StressTestingResult.AvailableCapitalCollection[j].Values[i].Value;
                    avcValues.Add(result.SensitivityStressTesting.StressTestingResult.AvailableCapitalCollection[j].Values[i].Value);
                }
                
                for (int j = 0; j < result.SensitivityStressTesting.StressTestingResult.SensitivityStressCollection.Length; j++)
                {
                    switch (result.SensitivityStressTesting.StressTestingResult.SensitivityStressCollection[j].Name)
                    {
                        case "Liquidity":
                            worksheet12.Range[218 + L1 + L2 + L3 * 3, 4 + i].Value2 = result.SensitivityStressTesting.StressTestingResult.SensitivityStressCollection[j].Values[i].Value;
                            break;
                    }
                }
            }

            worksheet12.Range[217 + L1 + L2 + L3, 2, 218 + L1 + L2 + L3 * 3, 3 + years.Length].BorderAround(LineStyleType.Medium, Color.Black);
            worksheet12.Range[217 + L1 + L2 + L3, 2, 218 + L1 + L2 + L3 * 3, 3 + years.Length].BorderInside(LineStyleType.Thin, Color.Black);

            for (int i = 0; i < L3; i++)
            {
                //Add chart and set chart data range  
                Chart chartST = worksheet12.Charts.Add();
                chartST.DataRange = worksheet12.Range[218 + L1 + L2 + L3, 4, 218 + L1 + L2 + L3 * 3, 3 + years.Length];
                chartST.SeriesDataFromRange = true;

                ChartSerie csS1 = chartST.Series[i];
                csS1.SerieType = ExcelChartType.ColumnClustered;
                csS1.CategoryLabels = worksheet12.Range[217 + L1 + L2 + L3, 4, 217 + L1 + L2 + L3, 3 + years.Length];
                csS1.Name = "Risk Capital";

                ChartSerie csS2 = chartST.Series[L3 + i];
                csS2.SerieType = ExcelChartType.Line;
                csS2.CategoryLabels = worksheet12.Range[217 + L1 + L2 + L3, 4, 217 + L1 + L2 + L3, 3 + years.Length];
                csS2.Name = "Available Capital";

                ChartSerie csS3 = chartST.Series[L3 * 2];
                csS3.SerieType = ExcelChartType.Line;
                csS3.CategoryLabels = worksheet12.Range[217 + L1 + L2 + L3, 4, 217 + L1 + L2 + L3, 3 + years.Length];
                csS3.Name = "Liquidity";

                chartST.PlotArea.ForeGroundColor = Color.Transparent;
                //Chart position  
                chartST.LeftColumn = 2 + i * 4;
                chartST.TopRow = 220 + L1 + L2 + L3 * 3;
                chartST.RightColumn = 5 + i * 4;
                chartST.BottomRow = 240 + L1 + L2 + L3;
                //Chart title  
                chartST.ChartTitle = result.SensitivityStressTesting.SensitivityResult.SensitivityStressScenarioCollection[i].NamaScenario;
                chartST.ChartTitleArea.Font.FontName = "Calibri";
                chartST.ChartTitleArea.Font.Size = 13;
                chartST.ChartTitleArea.Font.IsBold = true;
                //Chart axis  
                chartST.PrimaryValueAxis.HasMajorGridLines = false;
                chartST.PrimaryValueAxis.HasMinorGridLines = false;
                chartST.PrimaryValueAxis.MaxValue = (double)Math.Ceiling(avcValues.Max() / 1000) * 1000;
                chartST.PrimaryValueAxis.MinValue = 0;
                //Chart legend  
                chartST.Legend.Position = LegendPositionType.Bottom;

                if (L3 == 3)
                {
                    switch (i)
                    {
                        case 0:
                            chartST.Series.RemoveAt(1);
                            chartST.Series.RemoveAt(1);
                            chartST.Series.RemoveAt(2);
                            chartST.Series.RemoveAt(2);
                            break;
                        case 1:
                            chartST.Series.RemoveAt(0);
                            chartST.Series.RemoveAt(1);
                            chartST.Series.RemoveAt(1);
                            chartST.Series.RemoveAt(2);
                            break;
                        case 2:
                            chartST.Series.RemoveAt(0);
                            chartST.Series.RemoveAt(0);
                            chartST.Series.RemoveAt(1);
                            chartST.Series.RemoveAt(1);
                            break;
                    }
                }
                if (L3 == 4)
                {
                    switch (i)
                    {
                        case 0:
                            chartST.Series.RemoveAt(1);
                            chartST.Series.RemoveAt(1);
                            chartST.Series.RemoveAt(1);
                            chartST.Series.RemoveAt(2);
                            chartST.Series.RemoveAt(2);
                            chartST.Series.RemoveAt(2);
                            break;
                        case 1:
                            chartST.Series.RemoveAt(0);
                            chartST.Series.RemoveAt(1);
                            chartST.Series.RemoveAt(1);
                            chartST.Series.RemoveAt(1);
                            chartST.Series.RemoveAt(2);
                            chartST.Series.RemoveAt(2);
                            break;
                        case 2:
                            chartST.Series.RemoveAt(0);
                            chartST.Series.RemoveAt(0);
                            chartST.Series.RemoveAt(1);
                            chartST.Series.RemoveAt(1);
                            chartST.Series.RemoveAt(1);
                            chartST.Series.RemoveAt(2);
                            break;
                        case 3:
                            chartST.Series.RemoveAt(0);
                            chartST.Series.RemoveAt(0);
                            chartST.Series.RemoveAt(0);
                            chartST.Series.RemoveAt(1);
                            chartST.Series.RemoveAt(1);
                            chartST.Series.RemoveAt(1);
                            break;
                    }
                }
            }

            #endregion 5. Stress Testing

            #region 6. Risk Budget
            worksheet12.Range[242 + L1 + L2 + L3, 2].Value = "6. Risk Budget";
            worksheet12.Range[242 + L1 + L2 + L3, 2].Style.Font.Color = Color.FromArgb(255, 255, 255);
            worksheet12.Range[242 + L1 + L2 + L3, 2].Style.Font.IsBold = true;
            worksheet12.Range[242 + L1 + L2 + L3, 2].Style.HorizontalAlignment = HorizontalAlignType.Left;
            worksheet12.Range[242 + L1 + L2 + L3, 1, 242 + L1 + L2 + L3, 7 + years.Length].Style.Color = Color.FromArgb(200, 105, 0);

            worksheet12.Range[243 + L1 + L2 + L3, 2].Value = "Risk budget reflect the proportion of total risk capital allocated to certain project or sector";
            worksheet12.Range[243 + L1 + L2 + L3, 2, 243 + L1 + L2 + L3, 7].Merge();
            worksheet12.Range[243 + L1 + L2 + L3, 2].Style.HorizontalAlignment = HorizontalAlignType.Left;

            worksheet12.Range[244 + L1 + L2 + L3, 2].Value = "by risk category";
            worksheet12.Range[244 + L1 + L2 + L3, 2].Style.Font.IsBold = true;
            worksheet12.Range[250 + L1 + L2 + L3, 2].Value = "by project";
            worksheet12.Range[250 + L1 + L2 + L3, 2].Style.Font.IsBold = true;
            worksheet12.Range[256 + L1 + L2 + L3, 2].Value = "by project sector";
            worksheet12.Range[256 + L1 + L2 + L3, 2].Style.Font.IsBold = true;

            worksheet12.Range[245 + L1 + L2 + L3, 2].Value = "Risk";
            worksheet12.Range[245 + L1 + L2 + L3, 2].Style.Font.Color = Color.Red;
            worksheet12.Range[245 + L1 + L2 + L3, 2].Style.Font.IsBold = true;
            worksheet12.Range[251 + L1 + L2 + L3, 2].Value = "Project";
            worksheet12.Range[251 + L1 + L2 + L3, 2].Style.Font.Color = Color.Red;
            worksheet12.Range[251 + L1 + L2 + L3, 2].Style.Font.IsBold = true;
            worksheet12.Range[257 + L1 + L2 + L3, 2].Value = "Sector";
            worksheet12.Range[257 + L1 + L2 + L3, 2].Style.Font.Color = Color.Red;
            worksheet12.Range[257 + L1 + L2 + L3, 2].Style.Font.IsBold = true;

            for (int i = 0; i < 3; i++)
            {
                worksheet12.Range[246 + L1 + L2 + L3 + i, 2].Value = result.RiskBudget.RiskBudgetByRisk.RiskBudgetCollectionByRisk[i].Name;
                worksheet12.Range[252 + L1 + L2 + L3 + i, 2].Value = result.RiskBudget.RiskBudgetByProject.RiskBudgetCollection[i].Name;
                worksheet12.Range[258 + L1 + L2 + L3 + i, 2].Value = result.RiskBudget.RiskBudgetBySektor.RiskBudgetCollectionBySektor[i].Name;
            }
            IList<decimal> rbValues = new List<decimal>();
            for (int i = 0; i < years.Length; i++)
            {
                worksheet12.Range[245 + L1 + L2 + L3, 3 + i].Value2 = years[i].Year;
                worksheet12.Range[251 + L1 + L2 + L3, 3 + i].Value2 = years[i].Year;
                worksheet12.Range[257 + L1 + L2 + L3, 3 + i].Value2 = years[i].Year;

                for (int j = 0; j < 3; j++)
                {
                    worksheet12.Range[246 + L1 + L2 + L3 + j, 3 + i].Value2 = result.RiskBudget.RiskBudgetByRisk.RiskBudgetCollectionByRisk[j].Values[i].Value;
                    worksheet12.Range[252 + L1 + L2 + L3 + j, 3 + i].Value2 = result.RiskBudget.RiskBudgetByProject.RiskBudgetCollection[j].Values[i].Value;
                    worksheet12.Range[258 + L1 + L2 + L3 + j, 3 + i].Value2 = result.RiskBudget.RiskBudgetBySektor.RiskBudgetCollectionBySektor[j].Values[i].Value;

                    rbValues.Add(result.RiskBudget.RiskBudgetByRisk.RiskBudgetCollectionByRisk[j].Values[i].Value);
                    rbValues.Add(result.RiskBudget.RiskBudgetByProject.RiskBudgetCollection[j].Values[i].Value);
                    rbValues.Add(result.RiskBudget.RiskBudgetBySektor.RiskBudgetCollectionBySektor[j].Values[i].Value);
                }
            }

            worksheet12.Range[245 + L1 + L2 + L3, 2, 248 + L1 + L2 + L3, 2 + years.Length].BorderAround(LineStyleType.Medium, Color.Black);
            worksheet12.Range[245 + L1 + L2 + L3, 2, 248 + L1 + L2 + L3, 2 + years.Length].BorderInside(LineStyleType.Thin, Color.Black);

            worksheet12.Range[251 + L1 + L2 + L3, 2, 254 + L1 + L2 + L3, 2 + years.Length].BorderAround(LineStyleType.Medium, Color.Black);
            worksheet12.Range[251 + L1 + L2 + L3, 2, 254 + L1 + L2 + L3, 2 + years.Length].BorderInside(LineStyleType.Thin, Color.Black);

            worksheet12.Range[257 + L1 + L2 + L3, 2, 260 + L1 + L2 + L3, 2 + years.Length].BorderAround(LineStyleType.Medium, Color.Black);
            worksheet12.Range[257 + L1 + L2 + L3, 2, 260 + L1 + L2 + L3, 2 + years.Length].BorderInside(LineStyleType.Thin, Color.Black);

            for (int i = 0; i < 3; i++)
            {
                //Add chart and set chart data range  
                Chart chartRBR = worksheet12.Charts.Add();
                chartRBR.DataRange = worksheet12.Range[246 + L1 + L2 + L3 + i * 6, 2, 248 + L1 + L2 + L3 + i * 6, 2 + years.Length];
                chartRBR.SeriesDataFromRange = true;

                ChartSerie csRBR1 = chartRBR.Series[0];
                csRBR1.SerieType = ExcelChartType.ColumnClustered;
                csRBR1.CategoryLabels = worksheet12.Range[245 + L1 + L2 + L3 + i * 6, 3, 245 + L1 + L2 + L3 + i * 6, 2 + years.Length];

                ChartSerie csRBR2 = chartRBR.Series[1];
                csRBR2.SerieType = ExcelChartType.Line;
                csRBR2.CategoryLabels = worksheet12.Range[245 + L1 + L2 + L3 + i * 6, 3, 245 + L1 + L2 + L3 + i * 6, 2 + years.Length];

                ChartSerie csRBR3 = chartRBR.Series[2];
                csRBR3.SerieType = ExcelChartType.Line;
                csRBR3.CategoryLabels = worksheet12.Range[245 + L1 + L2 + L3 + i * 6, 3, 245 + L1 + L2 + L3 + i * 6, 2 + years.Length];

                chartRBR.PlotArea.ForeGroundColor = Color.Transparent;
                //Chart position  
                chartRBR.LeftColumn = 2 + i * 4;
                chartRBR.TopRow = 262 + L1 + L2 + L3;
                chartRBR.RightColumn = 5 + i * 4;
                chartRBR.BottomRow = 282 + L1 + L2 + L3;
                //Chart title  
                chartRBR.ChartTitleArea.Font.FontName = "Calibri";
                chartRBR.ChartTitleArea.Font.Size = 13;
                chartRBR.ChartTitleArea.Font.IsBold = true;
                //Chart axis  
                chartRBR.PrimaryValueAxis.HasMajorGridLines = false;
                chartRBR.PrimaryValueAxis.HasMinorGridLines = false;
                chartRBR.PrimaryValueAxis.MaxValue = (double)Math.Ceiling(rbValues.Max() / 10) * 10 + 50;
                chartRBR.PrimaryValueAxis.MinValue = 0;
                //Chart legend  
                chartRBR.Legend.Position = LegendPositionType.Bottom;
                switch (i)
                {
                    case 0:
                        chartRBR.ChartTitle = "Risk Budget : Risk Category";
                        break;
                    case 1:
                        chartRBR.ChartTitle = "Risk Budget : Project";
                        break;
                    case 2:
                        chartRBR.ChartTitle = "Risk Budget : Project Sector";
                        break;
                }
            }
            #endregion 6. Risk Budget

            worksheet12.AllocatedRange.AutoFitColumns();
            worksheet12.AllocatedRange.Style.Font.FontName = "Calibri";
            worksheet12.AllocatedRange.Style.Font.Size = 10;

            //Save workbook to disk
            var fileName = "RBC_PII_Result.xlsx";
            var fileNamePDF = "RBC_PII_Result.pdf";
            string path = "~\\Temp\\Reports\\Result\\";
            if (!Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath(path)))
            {
                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(path));
            }
            var pathFile = System.Web.Hosting.HostingEnvironment.MapPath(path);
            DirectoryInfo directory = new DirectoryInfo(pathFile);
            //delete files:
            directory.GetFiles().ToList().ForEach(f => f.Delete());
            workbook.SaveToFile(pathFile + fileName, ExcelVersion.Version2010);
            workbook.SaveToFile(pathFile + fileNamePDF, FileFormat.PDF);

        }
        public void DashboardReport(ReportMainDashboard mainDashboardResult)
        {
            var colorDefault = _iDatabaseContext.ColorComments.Where(x => x.IsDefault == true).SingleOrDefault();
            List<Comments> comments = new List<Comments>();
            OverAllComments overallComments = new OverAllComments();
            if(colorDefault!= null)
            {
                comments = _iDatabaseContext.Commentss.Where(x => x.ColorCommentId == colorDefault.Id).ToList();
                overallComments = _iDatabaseContext.OverAllCommentss.Where(x => x.ColorCommentId == colorDefault.Id).SingleOrDefault();
            }

            var years = mainDashboardResult.Result.AvailableVSRiskCapital.AvailableVSRiskCapitalYearCollection;
            var projects = mainDashboardResult.Project;

            //Initialize a new Workboook object
            Workbook workbook = new Workbook();

            //Get the first worksheet
            Worksheet worksheetDashb = workbook.Worksheets[0];
            worksheetDashb.Name = "Main Dashboard";

            worksheetDashb.AllocatedRange.Style.Font.Size = 10;

            #region 0.Dashboard
            
            worksheetDashb.SetColumnWidth(1, 3);
            worksheetDashb.SetColumnWidth(2, 17);
            worksheetDashb.SetColumnWidth(3, 19);
            worksheetDashb.SetColumnWidth(4, 15);
            for (int i = 0; i < 9; i++)
            {
                worksheetDashb.SetColumnWidth(5 + i, 17);
            }
            for (int i = 0; i < mainDashboardResult.FunctionalRiskMainDashboard.Count(); i++)
            {
                worksheetDashb.SetRowHeight(10 + i, 35);
            }
            worksheetDashb.Range["B1"].Value = "Dashboard";
            worksheetDashb.Range["B1"].Style.Font.IsBold = true;
            worksheetDashb.Range["B1"].Style.Font.Color = Color.White;
            worksheetDashb.Range["B1"].Style.HorizontalAlignment = HorizontalAlignType.Left;
            worksheetDashb.Range[1, 1, 1, 7 + years.Length].Style.Color = Color.FromArgb(163, 32, 32);
            worksheetDashb.Range["B2"].Value = "Summarises\n(1) Risk apperite and risk limts through all dimentions\n(2) The key results against(1)";
            worksheetDashb.Range["B2"].Style.WrapText = true;
            worksheetDashb.Range[2, 2, 2, 4].Style.Color = Color.FromArgb(242, 242, 242);
            worksheetDashb.Range[2, 2, 3, 4].Merge();

            worksheetDashb.Range[5, 2].Value = "Functional Risk Limits";
            worksheetDashb.Range[5, 2].Style.Font.Color = Color.White;
            worksheetDashb.Range[5, 2, 5, 13].Merge();
            worksheetDashb.Range[5, 2, 5, 13].Style.Color = Color.Black;
            worksheetDashb.Range[8, 2].Value = "Metrices";
            worksheetDashb.Range[8, 2, 9, 2].Merge();
            worksheetDashb.Range[8, 3].Value = "Formula";
            worksheetDashb.Range[8, 3, 9, 3].Merge();
            worksheetDashb.Range[8, 4].Value = "How It Works";
            worksheetDashb.Range[8, 4, 9, 4].Merge();
            worksheetDashb.Range[8, 5].Value = "Comforts Green Zone";
            worksheetDashb.Range[8, 5, 8, 7].Merge();
            worksheetDashb.Range[8, 5, 8, 7].BorderAround(LineStyleType.Thin, Color.Black);
            worksheetDashb.Range[8, 5, 9, 7].Style.Color = Color.FromArgb(204, 255, 204);
            worksheetDashb.Range[8, 8].Value = "Alert Yellow Zone";
            worksheetDashb.Range[8, 8, 8, 10].Merge();
            worksheetDashb.Range[8, 8, 8, 10].BorderAround(LineStyleType.Thin, Color.Black);
            worksheetDashb.Range[8, 8, 9, 10].Style.Color = Color.FromArgb(255, 255, 204);
            worksheetDashb.Range[8, 11].Value = "Limit Red Zone";
            worksheetDashb.Range[8, 11, 8, 13].Merge();
            worksheetDashb.Range[8, 11, 8, 13].BorderAround(LineStyleType.Thin, Color.Black);
            worksheetDashb.Range[8, 11, 9, 13].Style.Color = Color.FromArgb(254, 190, 190);

            for (int i = 0; i < 3; i++)
            {
                worksheetDashb.Range[9, 5 + i].Value = mainDashboardResult.FunctionalRiskMainDashboard[0].Colors[0].Scenarios[i].Scenario;
                worksheetDashb.Range[9, 8 + i].Value = mainDashboardResult.FunctionalRiskMainDashboard[0].Colors[0].Scenarios[i].Scenario;
                worksheetDashb.Range[9, 11 + i].Value = mainDashboardResult.FunctionalRiskMainDashboard[0].Colors[0].Scenarios[i].Scenario;
            }

            for (int i = 0; i < mainDashboardResult.FunctionalRiskMainDashboard.Count(); i++)
            {
                worksheetDashb.Range[10 + i, 2].Value = mainDashboardResult.FunctionalRiskMainDashboard[i].NamaMatrix;
                worksheetDashb.Range[10 + i, 2].Style.WrapText = true;
                worksheetDashb.Range[10 + i, 3].Value = mainDashboardResult.FunctionalRiskMainDashboard[i].NamaFormula;
                worksheetDashb.Range[10 + i, 3].Style.WrapText = true;
                worksheetDashb.Range[10 + i, 4].Value = "TBC";
                worksheetDashb.Range[10 + i, 5].Value = mainDashboardResult.FunctionalRiskMainDashboard[i].Colors[0].Scenarios[0].Definisi;
                worksheetDashb.Range[10 + i, 5].Style.WrapText = true;
                worksheetDashb.Range[10 + i, 6].Value = mainDashboardResult.FunctionalRiskMainDashboard[i].Colors[0].Scenarios[1].Definisi;
                worksheetDashb.Range[10 + i, 6].Style.WrapText = true;
                worksheetDashb.Range[10 + i, 7].Value = mainDashboardResult.FunctionalRiskMainDashboard[i].Colors[0].Scenarios[2].Definisi;
                worksheetDashb.Range[10 + i, 7].Style.WrapText = true;
                worksheetDashb.Range[10 + i, 8].Value = mainDashboardResult.FunctionalRiskMainDashboard[i].Colors[1].Scenarios[0].Definisi;
                worksheetDashb.Range[10 + i, 8].Style.WrapText = true;
                worksheetDashb.Range[10 + i, 9].Value = mainDashboardResult.FunctionalRiskMainDashboard[i].Colors[1].Scenarios[1].Definisi;
                worksheetDashb.Range[10 + i, 9].Style.WrapText = true;
                worksheetDashb.Range[10 + i, 10].Value = mainDashboardResult.FunctionalRiskMainDashboard[i].Colors[1].Scenarios[2].Definisi;
                worksheetDashb.Range[10 + i, 10].Style.WrapText = true;
                worksheetDashb.Range[10 + i, 11].Value = mainDashboardResult.FunctionalRiskMainDashboard[i].Colors[2].Scenarios[0].Definisi;
                worksheetDashb.Range[10 + i, 11].Style.WrapText = true;
                worksheetDashb.Range[10 + i, 12].Value = mainDashboardResult.FunctionalRiskMainDashboard[i].Colors[2].Scenarios[1].Definisi;
                worksheetDashb.Range[10 + i, 12].Style.WrapText = true;
                worksheetDashb.Range[10 + i, 13].Value = mainDashboardResult.FunctionalRiskMainDashboard[i].Colors[2].Scenarios[2].Definisi;
                worksheetDashb.Range[10 + i, 13].Style.WrapText = true;
            }

            worksheetDashb.Range[8, 2, 9 + mainDashboardResult.FunctionalRiskMainDashboard.Count(), 13].BorderAround(LineStyleType.Thin, Color.Black);
            worksheetDashb.Range[8, 2, 9 + mainDashboardResult.FunctionalRiskMainDashboard.Count(), 13].BorderInside(LineStyleType.Thin, Color.Black);


            //Table 2
            var awalTbl2 = 15 + mainDashboardResult.FunctionalRiskMainDashboard.Count();
            worksheetDashb.Range[12 + mainDashboardResult.FunctionalRiskMainDashboard.Count(), 2].Value = "Results";
            worksheetDashb.Range[12 + mainDashboardResult.FunctionalRiskMainDashboard.Count(), 2].Style.Font.Color = Color.White;
            worksheetDashb.Range[12 + mainDashboardResult.FunctionalRiskMainDashboard.Count(), 2, 12 + mainDashboardResult.FunctionalRiskMainDashboard.Count(), 13].Merge();
            worksheetDashb.Range[12 + mainDashboardResult.FunctionalRiskMainDashboard.Count(), 2, 12 + mainDashboardResult.FunctionalRiskMainDashboard.Count(), 13].Style.Color = Color.Black;
            worksheetDashb.Range[awalTbl2, 2].Value = "Metrices";
            worksheetDashb.Range[awalTbl2, 2, 16 + mainDashboardResult.FunctionalRiskMainDashboard.Count(), 2].Merge();
            worksheetDashb.Range[awalTbl2, 3].Value = "Formula";
            worksheetDashb.Range[awalTbl2, 3, awalTbl2 + 1, 3].Merge();
            worksheetDashb.Range[awalTbl2, 4].Value = "How It Works";
            worksheetDashb.Range[awalTbl2, 4, awalTbl2 + 1, 4].Merge();
            worksheetDashb.Range[awalTbl2, 5].Value = mainDashboardResult.FunctionalRiskResultTableResults[0].Scenarios[0].Scenario;
            worksheetDashb.Range[awalTbl2, 5, awalTbl2, 7].Merge();
            worksheetDashb.Range[awalTbl2, 8].Value = mainDashboardResult.FunctionalRiskResultTableResults[0].Scenarios[2].Scenario;
            worksheetDashb.Range[awalTbl2, 8, awalTbl2, 10].Merge();
            worksheetDashb.Range[awalTbl2, 11].Value = mainDashboardResult.FunctionalRiskResultTableResults[0].Scenarios[1].Scenario;
            worksheetDashb.Range[awalTbl2, 11, awalTbl2, 13].Merge();

            worksheetDashb.Range[awalTbl2 + 1, 5].Value = "Calculations";
            worksheetDashb.Range[awalTbl2 + 1, 6].Value = "Comments";
            worksheetDashb.Range[awalTbl2 + 1, 6, awalTbl2 + 1, 7].Merge();
            worksheetDashb.Range[awalTbl2 + 1, 8].Value = "Calculations";
            worksheetDashb.Range[awalTbl2 + 1, 9].Value = "Comments";
            worksheetDashb.Range[awalTbl2 + 1, 9, awalTbl2 + 1, 10].Merge();
            worksheetDashb.Range[awalTbl2 + 1, 11].Value = "Calculations";
            worksheetDashb.Range[awalTbl2 + 1, 12].Value = "Comments";
            worksheetDashb.Range[awalTbl2 + 1, 12, awalTbl2 + 1, 13].Merge();

            for (int i = 0; i < mainDashboardResult.FunctionalRiskMainDashboard.Count(); i++)
            {
                worksheetDashb.Range[awalTbl2 + 2 + i, 2].Value = mainDashboardResult.FunctionalRiskMainDashboard[i].NamaMatrix;
                worksheetDashb.Range[awalTbl2 + 2 + i, 2].Style.WrapText = true;
                worksheetDashb.Range[awalTbl2 + 2 + i, 3].Value = mainDashboardResult.FunctionalRiskMainDashboard[i].NamaFormula;
                worksheetDashb.Range[awalTbl2 + 2 + i, 3].Style.WrapText = true;
                worksheetDashb.Range[awalTbl2 + 2 + i, 4].Value = "TBC";
                worksheetDashb.Range[awalTbl2 + 2 + i, 5].Value = mainDashboardResult.FunctionalRiskResultTableResults[i].Scenarios[0].Calculations;
                worksheetDashb.Range[awalTbl2 + 2 + i, 6].Value = mainDashboardResult.FunctionalRiskResultTableResults[i].Scenarios[0].Comment;
                worksheetDashb.Range[awalTbl2 + 2 + i, 6].Style.WrapText = true;
                worksheetDashb.Range[awalTbl2 + 2 + i, 6, awalTbl2 + 2 + i, 7].Merge();
                worksheetDashb.Range[awalTbl2 + 2 + i, 8].Value = mainDashboardResult.FunctionalRiskResultTableResults[i].Scenarios[2].Calculations;
                worksheetDashb.Range[awalTbl2 + 2 + i, 9].Value = mainDashboardResult.FunctionalRiskResultTableResults[i].Scenarios[2].Comment;
                worksheetDashb.Range[awalTbl2 + 2 + i, 9].Style.WrapText = true;
                worksheetDashb.Range[awalTbl2 + 2 + i, 9, awalTbl2 + 2 + i, 10].Merge();
                worksheetDashb.Range[awalTbl2 + 2 + i, 11].Value = mainDashboardResult.FunctionalRiskResultTableResults[i].Scenarios[1].Calculations;
                worksheetDashb.Range[awalTbl2 + 2 + i, 12].Value = mainDashboardResult.FunctionalRiskResultTableResults[i].Scenarios[1].Comment;
                worksheetDashb.Range[awalTbl2 + 2 + i, 12].Style.WrapText = true;
                worksheetDashb.Range[awalTbl2 + 2 + i, 12, awalTbl2 + 2 + i, 13].Merge();
                worksheetDashb.SetRowHeight(awalTbl2 + 2 + i, 55);
            }

            worksheetDashb.Range[awalTbl2, 2, awalTbl2 + 1 + mainDashboardResult.FunctionalRiskMainDashboard.Count(), 13].BorderAround(LineStyleType.Thin, Color.Black);
            worksheetDashb.Range[awalTbl2, 2, awalTbl2 + 1 + mainDashboardResult.FunctionalRiskMainDashboard.Count(), 13].BorderInside(LineStyleType.Thin, Color.Black);

            #endregion 0.Dashboard

            var L1 = awalTbl2 + 1 + mainDashboardResult.FunctionalRiskMainDashboard.Count();


            Worksheet worksheetResult = workbook.Worksheets[1];

            var result = mainDashboardResult.Result;


            #region Leverage Ratios

            worksheetResult.Range[1, 2].Value = "2. Leverage Ratios";
            worksheetResult.Range[1, 2].Style.Font.Color = Color.FromArgb(255, 255, 255);
            worksheetResult.Range[1, 2].Style.Font.IsBold = true;
            worksheetResult.Range[1, 2].Style.HorizontalAlignment = HorizontalAlignType.Left;
            worksheetResult.Range[1, 1, 1, 7 + years.Length].Style.Color = Color.FromArgb(200, 105, 0);

            worksheetResult.Range[4, 2].Value = "Total Undiversified Risk Capital";
            worksheetResult.Range[5, 2].Value = "Total Diversified Risk Capital";
            worksheetResult.Range[6, 2].Value = "Available Capital";
            worksheetResult.Range[7, 2].Value = "Total Maximum Exposure*";
            worksheetResult.Range[8, 2].Value = "Diversification Ratio";
            worksheetResult.Range[9, 2].Value = "Mitigation Effectiveness";
            worksheetResult.Range[10, 2].Value = "Leverage Ratio";

            for (int i = 0; i < years.Length; i++)
            {
                worksheetResult.Range[3, 3 + i].Value2 = years[i].Year;
                for (int j = 0; j < result.LeverageRatio.LeverageItem.Length; j++)
                {
                    switch (result.LeverageRatio.LeverageItem[j].Name)
                    {
                        case "Total Undiversified Risk Capital":
                            worksheetResult.Range[4, 3 + i].Value2 = result.LeverageRatio.LeverageItem[j].Values[i].Value;
                            break;
                        case "Total Diversified Risk Capital":
                            worksheetResult.Range[5, 3 + i].Value2 = result.LeverageRatio.LeverageItem[j].Values[i].Value;
                            break;
                        case "Available Capital":
                            worksheetResult.Range[6, 3 + i].Value2 = result.LeverageRatio.LeverageItem[j].Values[i].Value;
                            break;
                        case "Total Maximum Exposure":
                            worksheetResult.Range[7, 3 + i].Value2 = result.LeverageRatio.LeverageItem[j].Values[i].Value;
                            break;
                        case "Diversification Ratio":
                            worksheetResult.Range[8, 3 + i].Value2 = result.LeverageRatio.LeverageItem[j].Values[i].Value;
                            break;
                        case "Mitigation Effectiveness":
                            worksheetResult.Range[9, 3 + i].Value2 = result.LeverageRatio.LeverageItem[j].Values[i].Value;
                            break;
                        case "Leveratio Ratio":
                            worksheetResult.Range[10, 3 + i].Value2 = result.LeverageRatio.LeverageItem[j].Values[i].Value;
                            break;
                    }
                }
            }

            worksheetResult.Range[3, 2, 10, 2 + years.Length].BorderAround(LineStyleType.Medium, Color.Black);
            worksheetResult.Range[3, 2, 10, 2 + years.Length].BorderInside(LineStyleType.Thin, Color.Black);

            //Add chart and set chart data range  
            Chart chartDR = worksheetDashb.Charts.Add(ExcelChartType.LineMarkers);
            chartDR.DataRange = worksheetResult.Range[8, 3, 8, 3 + years.Length];
            chartDR.SeriesDataFromRange = true;

            ChartSerie csDR = chartDR.Series[0];
            csDR.CategoryLabels = worksheetResult.Range[3, 3, 3, 3 + years.Length];

            chartDR.PlotArea.ForeGroundColor = Color.Transparent;
            //Chart position  
            chartDR.LeftColumn = 2;
            chartDR.TopRow = L1 + 2;
            chartDR.RightColumn = 5;
            chartDR.BottomRow = L1 + 12;
            //Chart title  
            chartDR.ChartTitle = "Diversification Ratio";
            chartDR.ChartTitleArea.Font.FontName = "Calibri";
            chartDR.ChartTitleArea.Font.Size = 13;
            chartDR.ChartTitleArea.Font.IsBold = true;
            chartDR.PrimaryValueAxis.HasMajorGridLines = false;
            chartDR.PrimaryValueAxis.Deleted = true;
            chartDR.HasLegend = false;

            //Add chart and set chart data range  
            Chart chartLR = worksheetDashb.Charts.Add(ExcelChartType.LineMarkers);
            chartLR.DataRange = worksheetResult.Range[10, 3, 10, 3 + years.Length];
            chartLR.SeriesDataFromRange = true;

            ChartSerie csLR = chartLR.Series[0];
            csLR.CategoryLabels = worksheetResult.Range[3, 3, 3, 3 + years.Length];

            chartLR.PlotArea.ForeGroundColor = Color.Transparent;
            //Chart position  
            chartLR.LeftColumn = 6;
            chartLR.TopRow = L1 + 2;
            chartLR.RightColumn = 9;
            chartLR.BottomRow = L1 + 12;
            //Chart title  
            chartLR.ChartTitle = "Leverage Ratio";
            chartLR.ChartTitleArea.Font.FontName = "Calibri";
            chartLR.ChartTitleArea.Font.Size = 13;
            chartLR.ChartTitleArea.Font.IsBold = true;
            chartLR.PrimaryValueAxis.HasMajorGridLines = false;
            chartLR.PrimaryValueAxis.Deleted = true;
            chartLR.HasLegend = false;

            //Add chart and set chart data range  
            Chart chartME = worksheetDashb.Charts.Add(ExcelChartType.LineMarkers);
            chartME.DataRange = worksheetResult.Range[9, 3, 9, 3 + years.Length];
            chartME.SeriesDataFromRange = true;

            ChartSerie csME = chartME.Series[0];
            csME.CategoryLabels = worksheetResult.Range[3, 3, 3, 3 + years.Length];

            chartME.PlotArea.ForeGroundColor = Color.Transparent;
            //Chart position  
            chartME.LeftColumn = 10;
            chartME.TopRow = L1 + 2;
            chartME.RightColumn = 13;
            chartME.BottomRow = L1 + 12;
            //Chart title  
            chartME.ChartTitle = "Mitigation Effectiveness";
            chartME.ChartTitleArea.Font.FontName = "Calibri";
            chartME.ChartTitleArea.Font.Size = 13;
            chartME.ChartTitleArea.Font.IsBold = true;
            chartME.PrimaryValueAxis.HasMajorGridLines = false;
            chartME.PrimaryValueAxis.Deleted = true;
            chartME.HasLegend = false;
            #endregion Leverage Ratios 

            #region  Aggregation of Projects

            Worksheet worksheetAOP = workbook.Worksheets[1];

            var aggregationOfProjects = mainDashboardResult.AggregationInterDiversifiedProjectCapital;
            

            worksheetAOP.Range[13, 2].Value = "Aggregation Of Project";
            worksheetAOP.Range[13, 2].Style.Font.Color = Color.FromArgb(255, 255, 255);
            worksheetAOP.Range[13, 2].Style.Font.IsBold = true;
            worksheetAOP.Range[13, 2].Style.HorizontalAlignment = HorizontalAlignType.Left;
            worksheetAOP.Range[13, 1, 13, 7 + years.Length].Style.Color = Color.FromArgb(200, 105, 0);
            worksheetAOP.Range[15, 2].Value = "Project Name";
            worksheetAOP.Range[15, 2].Style.Font.IsBold = true;
            worksheetAOP.Range[15, 3].Value = "Inter-Project Diversified Project Capital";
            worksheetAOP.Range[15, 3].Style.Font.IsBold = true;
            worksheetAOP.Range[15, 3].Style.HorizontalAlignment = HorizontalAlignType.Left;
            worksheetAOP.Range[15, 3, 15, 2 + years.Length].Merge();

            for (int i = 0; i < projects.Length; i++)
            {

                worksheetAOP.Range[17 + i, 2].Value = projects[i].NamaProject;
            }

            for (int i = 0; i < years.Length; i++)
            {
                worksheetAOP.Range[16, 3 + i].Value2 = years[i].Year;
                worksheetAOP.Range[16, 3 + i].Style.Font.IsBold = true;

                for (int j = 0; j < projects.Length; j++)
                {
                    worksheetAOP.Range[17 + j, 3 + i].Value2 = aggregationOfProjects[i].AggregationInterDiversifiedProjectCollection[j].Total;
                }
            }

            worksheetResult.Range[15, 2, 16 + projects.Length, 2 + years.Length].BorderAround(LineStyleType.Medium, Color.Black);
            worksheetResult.Range[15, 2, 16 + projects.Length, 2 + years.Length].BorderInside(LineStyleType.Thin, Color.Black);

            int col = 0;
            for (int i = 0; i < projects.Length; i++)
            {
                if (col == 5)
                {
                    col = 0;
                }

                //Add chart and set chart data range  
                Chart chartAOP = worksheetDashb.Charts.Add(ExcelChartType.LineMarkers);
                chartAOP.DataRange = worksheetResult.Range[17 + i, 3, 17 + i, 3 + years.Length];
                chartAOP.SeriesDataFromRange = true;

                ChartSerie csAOP = chartAOP.Series[0];
                csAOP.CategoryLabels = worksheetResult.Range[16, 3, 16, 3 + years.Length];

                chartAOP.PlotArea.ForeGroundColor = Color.Transparent;
                //Chart position  
                
                chartAOP.LeftColumn = 2 + col * 3;
                chartAOP.TopRow = L1 + 14 + (int)Math.Floor((decimal)i / 5) * 11;
                chartAOP.RightColumn = 4 + col * 3;
                chartAOP.BottomRow = L1 + 24 + (int)Math.Floor((decimal)i / 5) * 11;
                //Chart title  
                chartAOP.ChartTitle = projects[i].NamaProject;
                chartAOP.ChartTitleArea.Font.FontName = "Calibri";
                chartAOP.ChartTitleArea.Font.Size = 13;
                chartAOP.ChartTitleArea.Font.IsBold = true;
                chartAOP.PrimaryValueAxis.HasMajorGridLines = false;
                chartAOP.PrimaryValueAxis.MaxValue = 200;
                chartAOP.PrimaryValueAxis.MinValue = 0;
                chartAOP.HasLegend = false;
                
                col += 1;
            }

            var space1 = L1 + 18 + (int)Math.Ceiling((decimal)projects.Length / 5) * 10 + 2;

            #endregion  Aggregation of Projects

            #region Diversified Benefit Achieved

            worksheetResult.Range[18 + projects.Length, 2].Value = "Diversified Benefit Achieved";
            worksheetResult.Range[18 + projects.Length, 2].Style.Font.Color = Color.FromArgb(255, 255, 255);
            worksheetResult.Range[18 + projects.Length, 2].Style.Font.IsBold = true;
            worksheetResult.Range[18 + projects.Length, 2].Style.HorizontalAlignment = HorizontalAlignType.Left;
            worksheetResult.Range[18 + projects.Length, 1, 18 + projects.Length, 7 + years.Length].Style.Color = Color.FromArgb(200, 105, 0);

            worksheetResult.Range[20 + projects.Length, 2].Value = "Sector";
            worksheetResult.Range[20 + projects.Length, 2].Style.Font.IsBold = true;

            for (int i = 0; i < years.Length; i++)
            {
                worksheetResult.Range[20 + projects.Length, i + 3].Value2 = years[i].Year;
                worksheetResult.Range[20 + projects.Length, i + 3].Style.Font.IsBold = true;
            }

            for (int i = 0; i < result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length; i++)
            {
                worksheetResult.Range[21 + projects.Length + i, 2].Value = result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved[i].NamaSektor;
                for (int j = 0; j < result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.YearSektorCollection.Length; j++)
                {
                    worksheetResult.Range[21 + projects.Length + i, 3 + j].Value2 = result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.YearSektorCollection[j].YearSektorValue[i].Value;
                }
            }
            worksheetResult.Range[20 + projects.Length, 2, 20 + projects.Length + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length, 2 + years.Length].BorderAround(LineStyleType.Medium, Color.Black);
            worksheetResult.Range[20 + projects.Length, 2, 20 + projects.Length + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length, 2 + years.Length].BorderInside(LineStyleType.Thin, Color.Black);
            

            #endregion Diversified Benefit Achieved

            #region Aggregation Of Sector
            var L2 = 20 + projects.Length + result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length;

            var interAOS = mainDashboardResult.InterProjectDiversifiedAggregationSektor;

            worksheetResult.Range[L2 + 3, 2].Value = "Aggregation Of Sector (Inter Project Diversified)";
            worksheetResult.Range[L2 + 3, 2].Style.Font.Color = Color.FromArgb(255, 255, 255);
            worksheetResult.Range[L2 + 3, 2].Style.Font.IsBold = true;
            worksheetResult.Range[L2 + 3, 2].Style.HorizontalAlignment = HorizontalAlignType.Left;
            worksheetResult.Range[L2 + 3, 1, L2 + 3, 7 + years.Length].Style.Color = Color.FromArgb(200, 105, 0);

            worksheetResult.Range[L2 + 5, 2].Value = "Sector";
            worksheetResult.Range[L2 + 5, 2].Style.Font.IsBold = true;

            for (int i = 0; i < years.Length; i++)
            {
                worksheetResult.Range[L2 + 5, 3].Value2 = years[i].Year;
                worksheetResult.Range[L2 + 5, 3].Style.Font.IsBold = true;
            }

            for (int i = 0; i < interAOS.SektorInterDiversified.Length; i++)
            {
                worksheetResult.Range[L2 + 6 + i, 2].Value = interAOS.SektorInterDiversified[i].NamaSektor;
                for (int j = 0; j < interAOS.YearCollection.Length; j++)
                {
                    worksheetResult.Range[L2 + 6 + i, 3 + j].Value2 = interAOS.YearCollection[j].YearValues[i].Value;

                }
            }
            worksheetResult.Range[L2 + 5, 2, L2 + 5 + interAOS.SektorInterDiversified.Length, 2 + years.Length].BorderAround(LineStyleType.Medium, Color.Black);
            worksheetResult.Range[L2 + 5, 2, L2 + 5 + interAOS.SektorInterDiversified.Length, 2 + years.Length].BorderInside(LineStyleType.Thin, Color.Black);

            //Add chart and set chart data range  
            Chart chartDBA = worksheetDashb.Charts.Add();
            chartDBA.ChartType = ExcelChartType.ScatterMarkers;
            chartDBA.DataRange = worksheetResult.Range[L2 + 6, 2, L2 + 6 + interAOS.SektorInterDiversified.Length, 2 + years.Length];
            chartDBA.SeriesDataFromRange = true;

            for (int i = 0; i < result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved.Length; i++)
            {
                ChartSerie csDBA = chartDBA.Series[i];
                csDBA.CategoryLabels = worksheetResult.Range[20 + projects.Length, 3, 20 + projects.Length, 3 + years.Length];
                csDBA.Name = result.DiversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved[i].NamaSektor;
                csDBA.Values = worksheetResult.Range[L2 + 6 + i, 3, L2 + 6 + i, 3 + years.Length];
                csDBA.Bubbles = worksheetResult.Range[21 + projects.Length + i, 2 + years.Length];
            }

            chartDBA.PlotArea.ForeGroundColor = Color.Transparent;
            //Chart position  

            chartDBA.LeftColumn = 2;
            chartDBA.TopRow = space1;
            chartDBA.RightColumn = 10;
            chartDBA.BottomRow = space1 + 13;
            //Chart title  
            chartDBA.ChartTitle = "Diversification Achieved vs Risk Capital";
            chartDBA.ChartTitleArea.Font.FontName = "Calibri";
            chartDBA.ChartTitleArea.Font.Size = 13;
            chartDBA.ChartTitleArea.Font.IsBold = true;
            chartDBA.PrimaryValueAxis.HasMajorGridLines = false;
            chartDBA.PrimaryValueAxis.MaxValue = 2000;
            chartDBA.PrimaryValueAxis.MinValue = 0;
            chartDBA.PrimaryCategoryAxis.MinValue = years.Min(x=> x.Year);
            chartDBA.PrimaryCategoryAxis.MaxValue = years.Max(x => x.Year);
            chartDBA.PrimaryCategoryAxis.MinorUnit = 5;
            chartDBA.HasLegend = true;

            #endregion Aggregation Of Sector

            #region Asset Allocation
            var L3 = L2 + interAOS.SektorInterDiversified.Length + 5;

            worksheetResult.Range[L3 + 3, 2].Value = "Asset Allocation (Strategic)";
            worksheetResult.Range[L3 + 3, 2].Style.Font.Color = Color.FromArgb(255, 255, 255);
            worksheetResult.Range[L3 + 3, 2].Style.Font.IsBold = true;
            worksheetResult.Range[L3 + 3, 2].Style.HorizontalAlignment = HorizontalAlignType.Left;
            worksheetResult.Range[L3 + 3, 1, L3 + 3, 7 + years.Length].Style.Color = Color.FromArgb(200, 105, 0);

            worksheetResult.Range[L3 + 5, 2].Value = "Asset Class";
            worksheetResult.Range[L3 + 5, 2].Style.Font.IsBold = true;

            worksheetResult.Range[L3 + 5, 3].Value = "Proportion";
            worksheetResult.Range[L3 + 5, 3].Style.Font.IsBold = true;

            var assetClass = mainDashboardResult.AssetClassDefault;
            for (int i = 0; i < assetClass.Length; i++)
            {
                worksheetResult.Range[L3 + 6 + i, 2].Value = assetClass[i].AssetClass;
                worksheetResult.Range[L3 + 6 + i, 3].Value2 = assetClass[i].Proportion;
            }

            worksheetResult.Range[L3 + 5, 2, L3 + 5 + assetClass.Length, 3].BorderAround(LineStyleType.Medium, Color.Black);
            worksheetResult.Range[L3 + 5, 2, L3 + 5 + assetClass.Length, 3].BorderInside(LineStyleType.Thin, Color.Black);

            //Add chart and set chart data range  
            Chart chartAA = worksheetDashb.Charts.Add(ExcelChartType.Doughnut);
            chartAA.DataRange = worksheetResult.Range[L3 + 5, 2, L3 + 5 + assetClass.Length, 3];
            chartAA.SeriesDataFromRange = false;
            
            chartAA.PlotArea.ForeGroundColor = Color.Transparent;
            //Chart position  

            chartAA.LeftColumn = 11;
            chartAA.TopRow = space1;
            chartAA.RightColumn = 15;
            chartAA.BottomRow = space1 + 13;

            foreach (ChartSerie csAA in chartAA.Series)
            {
                csAA.DataPoints.DefaultDataPoint.DataLabels.HasPercentage = true;
            }

            //Chart title  
            chartAA.ChartTitle = "Asset allocation (strategic)";
            chartAA.ChartTitleArea.Font.FontName = "Calibri";
            chartAA.ChartTitleArea.Font.Size = 13;
            chartAA.ChartTitleArea.Font.IsBold = true;
            chartAA.HasLegend = true;
            chartAA.Legend.Position = LegendPositionType.Right;

            #endregion Asset Allocation

            #region Overall Risk
            var L4 = space1 + 14;

            worksheetDashb.Range[L4, 2].Value = "Overall Risk";
            worksheetDashb.Range[L4, 2].Style.Font.IsBold = true;
            worksheetDashb.Range[L4 + 1, 2].Value = colorDefault.Warna;
            worksheetDashb.Range[L4 + 1, 2].Style.Font.IsBold = true;
            worksheetDashb.Range[L4 + 1, 2].Style.Font.Color = Color.White;
            switch (colorDefault.Warna)
            {
                case "Red":
                    worksheetDashb.Range[L4 + 1, 2].Style.Color = Color.Red;
                    break;
                case "Green":
                    worksheetDashb.Range[L4 + 1, 2].Style.Color = Color.Green;
                    break;
                case "Yellow":
                    worksheetDashb.Range[L4 + 1, 2].Style.Color = Color.Yellow;
                    break;
            }
            worksheetDashb.Range[L4 + 1, 2].BorderAround(LineStyleType.Medium, Color.Black);

            worksheetDashb.Range[L4 + 3, 2].Value = "Metrices";
            worksheetDashb.Range[L4 + 3, 2].Style.Font.IsBold = true;
            worksheetDashb.Range[L4 + 3, 3].Value = "Comments";
            worksheetDashb.Range[L4 + 3, 3].Style.Font.IsBold = true;
            worksheetDashb.Range[L4 + 3, 3, L4 + 3, 9].Merge();
            worksheetDashb.Range[L4 + 3, 10].Value = "Action points";
            worksheetDashb.Range[L4 + 3, 10].Style.Font.IsBold = true;
            worksheetDashb.Range[L4 + 3, 10, L4 + 3, 16].Merge();

            for (int i = 0; i < comments.Count; i++)
            {
                worksheetDashb.Range[L4 + 4 + i, 2].Value = comments[i].Matrix.NamaMatrix;
                worksheetDashb.Range[L4 + 4 + i, 3].Value = comments[i].Comment;
                worksheetDashb.Range[L4 + 4 + i, 3, L4 + 4 + i, 9].Merge();
                worksheetDashb.Range[L4 + 4 + i, 10].Value = comments[i].ActionPoint;
                worksheetDashb.Range[L4 + 4 + i, 10, L4 + 4 + i, 16].Merge();
            }

            worksheetDashb.Range[L4 + 3, 2, L4 + 3 + comments.Count, 16].BorderAround(LineStyleType.Medium, Color.Black);
            worksheetDashb.Range[L4 + 3, 2, L4 + 3 + comments.Count, 16].BorderInside(LineStyleType.Thin, Color.Black);

            worksheetDashb.Range[L4 + 3 + comments.Count + 3, 2].Value = "Overall Comment";
            worksheetDashb.Range[L4 + 3 + comments.Count + 3, 2].Style.Font.IsBold = true;
            worksheetDashb.Range[L4 + 3 + comments.Count + 4, 2].Value = overallComments.OverAllComment;
            worksheetDashb.Range[L4 + 3 + comments.Count + 4, 2, L4 + comments.Count + 17, 15].Merge();
            worksheetDashb.Range[L4 + 3 + comments.Count + 4, 2, L4 + comments.Count + 17, 15].BorderAround(LineStyleType.Medium, Color.Black);


            #endregion Overall Risk

            worksheetDashb.AllocatedRange.AutoFitColumns();
            worksheetDashb.AllocatedRange.Style.Font.FontName = "Calibri";
            worksheetDashb.AllocatedRange.Style.Font.Size = 10;
            worksheetDashb.AllocatedRange.Style.VerticalAlignment = VerticalAlignType.Center;
            worksheetDashb.AllocatedRange.Style.HorizontalAlignment = HorizontalAlignType.Center;

            worksheetDashb.SetColumnWidth(1, 1);
            worksheetDashb.SetColumnWidth(2, 24);
            worksheetDashb.SetColumnWidth(3, 21);
            worksheetDashb.SetColumnWidth(4, 11);

            for (int i = 0; i < 11; i++)
            {
                worksheetDashb.SetColumnWidth(5 + i, 14.3);
            }


            //Save workbook to disk
            var fileName = "RBC_PII_Main_Dashboard.xlsx";
            var fileNamePDF = "RBC_PII_Main_Dashboard.pdf";
            string path = "~\\Temp\\Reports\\Main_Dashboard\\";
            if (!Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath(path)))
            {
                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(path));
            }
            var pathFile = System.Web.Hosting.HostingEnvironment.MapPath(path);
            DirectoryInfo directory = new DirectoryInfo(pathFile);
            //delete files:
            directory.GetFiles().ToList().ForEach(f => f.Delete());
            workbook.SaveToFile(pathFile + fileName, ExcelVersion.Version2010);
            workbook.SaveToFile(pathFile + fileNamePDF, FileFormat.PDF);
        }
        #endregion Reporting

    }
}
