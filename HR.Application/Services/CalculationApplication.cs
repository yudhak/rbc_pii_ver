﻿using System;
using HR.Domain;
using System.Collections.Generic;
using System.Linq;
using HR.Common;
using HR.Application.Params;
using HR.Common.Validation;
using HR.Infrastructure;
using System.Data.SqlClient;
using System.Data;
using System.Collections;

namespace HR.Application
{
    public class CalculationService : ICalculationService
    {
        private readonly IDatabaseContext _iDatabaseContext;
        private readonly IRiskMatrixProjectRepository _riskMatrixProjectRepository;
        private readonly IStageTahunRiskMatrixRepository _stageTahunRiskMatrixRepository;
        private readonly IStageTahunRiskMatrixDetailRepository _stageTahunRiskMatrixDetailRepository;
        private readonly IRiskRegistrasiRepository _riskRegistrasiRepository;
        private readonly ILikehoodDetailRepository _likehoodDetailRepository;
        private readonly IScenarioRepository _scenarioRepository;
        private readonly ICorrelatedSektorRepository _correlatedSektorRepository;
        private readonly ICorrelatedProjectRepository _correlatedProjectRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly IScenarioDetailRepository _scenarioDetailRepository;
        private readonly ICorrelatedSektorDetailRepository _correlatedSektorDetailRepository;
        private readonly ICorrelatedProjectDetailRepository _correlatedProjectDetailRepository;
        private readonly ISektorRepository _sektorRepository;
        private readonly IAssetDataRepository _assetDataRepository;
        private readonly IPMNRepository _pmnRepository;
        private readonly IScenarioCalculationRepository _scenarioCalculationRepository;
        private readonly IAvailableCapitalProjectedRepository _availableCapitalProjectedRepository;
        private readonly IProjectCalculationRepository _projectCalculationRepository;
        private readonly IProjectRiskRegistrasiRepository _projectRiskRegistrasiRepository;


        public CalculationService(IRiskMatrixProjectRepository riskMatrixProjectRepository, IStageTahunRiskMatrixRepository stageTahunRiskMatrixRepository, IStageTahunRiskMatrixDetailRepository stageTahunRiskMatrixDetailRepository,
            IRiskRegistrasiRepository riskRegistrasiRepository, ILikehoodDetailRepository likehoodDetailRepository, IScenarioRepository scenarioRepository, ICorrelatedSektorRepository correlatedSektorRepository, ICorrelatedProjectRepository correlatedProjectRepository,
            IProjectRepository projectRepository, IScenarioDetailRepository scenarioDetailRepository, ICorrelatedSektorDetailRepository correlatedSektorDetailRepository, ICorrelatedProjectDetailRepository correlatedProjectDetailRepository, ISektorRepository sektorRepository,
            IAssetDataRepository assetDataRepository, IPMNRepository pmnRepository, IScenarioCalculationRepository scenarioCalculationRepository, IDatabaseContext iDatabaseContext, IAvailableCapitalProjectedRepository availableCapitalProjectedRepository, IProjectCalculationRepository projectCalculationRepository,
            IProjectRiskRegistrasiRepository projectRiskRegistrasiRepository)
        {
            _iDatabaseContext = iDatabaseContext;
            _riskMatrixProjectRepository = riskMatrixProjectRepository;
            _stageTahunRiskMatrixRepository = stageTahunRiskMatrixRepository;
            _stageTahunRiskMatrixDetailRepository = stageTahunRiskMatrixDetailRepository;
            _riskRegistrasiRepository = riskRegistrasiRepository;
            _likehoodDetailRepository = likehoodDetailRepository;
            _scenarioRepository = scenarioRepository;
            _correlatedSektorRepository = correlatedSektorRepository;
            _correlatedProjectRepository = correlatedProjectRepository;
            _projectRepository = projectRepository;
            _scenarioDetailRepository = scenarioDetailRepository;
            _correlatedSektorDetailRepository = correlatedSektorDetailRepository;
            _correlatedProjectDetailRepository = correlatedProjectDetailRepository;
            _sektorRepository = sektorRepository;
            _assetDataRepository = assetDataRepository;
            _pmnRepository = pmnRepository;
            _scenarioCalculationRepository = scenarioCalculationRepository;
            _availableCapitalProjectedRepository = availableCapitalProjectedRepository;
            _projectCalculationRepository = projectCalculationRepository;
            _projectRiskRegistrasiRepository = projectRiskRegistrasiRepository;
        }
        public Calculation GetAllDataCalculationByScenarioId()
        {
            var scenarioCalculation = _scenarioCalculationRepository.GetAll().ToList();
            var scenarioDefault = _scenarioRepository.GetDefault();
            Calculation calculation = new Calculation();
            IList<CalculationResult> calculationResults = new List<CalculationResult>();
            ScenarioTesting scenarioTesting = new ScenarioTesting();
            IList<int> tahun = new List<int>();


            IList<ScenarioProject> scenarioProjects = new List<ScenarioProject>();

            var scenarioDefaultExist = scenarioCalculation.Where(x => x.ScenarioId == scenarioDefault.Id).SingleOrDefault();

            if (scenarioDefaultExist == null)
            {
                var projects = _projectRepository.GetAll().Where(x => x.IsActive == true && x.IsDelete == false)    // your starting point - table in the "from" statement
                    .Join(scenarioDefault.ScenarioDetail.Where(x => x.IsDelete == false), // the source table of the inner join
                    proj => proj.Id,        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
                    scdet => scdet.ProjectId,   // Select the foreign key (the second part of the "on" clause)
                    (proj, scdet) => scdet.ProjectId).ToArray(); // selection
                ScenarioProject scenarioProject = new ScenarioProject();
                scenarioProject.ScenarioId = scenarioDefault.Id;
                scenarioProject.ProjectId = projects;
                scenarioProjects.Add(scenarioProject);
            }

            for (int i = 0; i < scenarioCalculation.Count(); i++)
            {
                ScenarioProject scenarioProject = new ScenarioProject();
                scenarioProject.ScenarioId = scenarioCalculation[i].ScenarioId;
                var projectCalculation = _projectCalculationRepository.GetByScenarioCalculationId(scenarioCalculation[i].Id).Select(x => x.ProjectId).ToArray();
                if (projectCalculation.Length == 0)
                {
                    throw new ApplicationException(string.Format("Data proyek kalkulasi pada tabel tblProjectCalculations tidak ditemukan. Silakan lakukan proses kalkulasi di menu Kalkulasi terlebih dahulu."));
                }
                else
                {
                    scenarioProject.ProjectId = projectCalculation;
                    scenarioProjects.Add(scenarioProject);
                }
            }

            foreach (var scenarios in scenarioProjects)
            {
                if (scenarios != null)
                {
                    var scenario = _scenarioRepository.Get(scenarios.ScenarioId);
                    Validate.NotNull(scenario, "Skenario id {0} tidak ditemukan.", scenario.Id);
                    var likelihoodDetail = scenario.Likehood.LikehoodDetails;
                    var riskRegistrasi = _riskRegistrasiRepository.GetAll().ToList();
                    var sektors = _sektorRepository.GetAll().ToList();
                    //var projects = scenario.ScenarioDetail.Where(x => x.IsDelete == false).ToList();
                    var proyek = _projectRepository.GetAll().Where(x => x.IsActive == true && x.IsDelete == false)    // your starting point - table in the "from" statement
                    .Join(scenario.ScenarioDetail.Where(x => x.IsDelete == false), // the source table of the inner join
                    proj => proj.Id,        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
                    scdet => scdet.ProjectId,   // Select the foreign key (the second part of the "on" clause)
                    (proj, scdet) => scdet).ToList(); // selection

                    var projects = proyek    // your starting point - table in the "from" statement
                    .Join(scenarios.ProjectId, // the source table of the inner join
                    proj => proj.ProjectId,        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
                    scdet => scdet,   // Select the foreign key (the second part of the "on" clause)
                    (proj, scdet) => proj).ToList(); // selection

                    CalculationResult calculationResult = new CalculationResult();

                    var generateYear = GenerateYear(scenarios);
                    calculationResult.CollectionYears = generateYear.ToArray();

                    IList<UndiversifiedRiskCapitalProjectCollection> undiversifiedRiskCapitalProjectCollection = new List<UndiversifiedRiskCapitalProjectCollection>();
                    AggregationOfProject aggregationOfProject = new AggregationOfProject();

                    IList<AggregationOfProjectCollection> aggregationOfProjectCollections = new List<AggregationOfProjectCollection>();
                    if (projects.Count > 0)
                    {
                        foreach (var item in projects)
                        {
                            IList<UndiversifiedYearCollection> undiversifiedYearCollection = new List<UndiversifiedYearCollection>();

                            var riskMatrixProject = _riskMatrixProjectRepository.GetByScenarioIdProjectId(scenario.Id, item.ProjectId);
                            Validate.NotNull(riskMatrixProject, "Risk Matrix untuk proyek {0} pada skenario {1} tidak ditemukan / belum diisi.", item.Project.NamaProject, scenario.NamaScenario);

                            var project = item.Project;
                            var stageTahunRiskMatrix = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectId(riskMatrixProject.Id).ToList();

                            if (stageTahunRiskMatrix.Count > 0)
                            {
                                var projectDetail = GetUndiversifiedByProject(item.ProjectId, scenario.Id);
                                undiversifiedRiskCapitalProjectCollection.Add(projectDetail);
                                UndiversifiedRiskCapitalProjectCollection undiversifiedRiskCapitalProject = new UndiversifiedRiskCapitalProjectCollection();
                                undiversifiedRiskCapitalProject.ProjectId = project.Id;
                                undiversifiedRiskCapitalProject.NamaProject = project.NamaProject;
                                undiversifiedRiskCapitalProject.SektorId = project.SektorId;
                                undiversifiedRiskCapitalProject.NamaSektor = project.Sektor.NamaSektor;
                                undiversifiedRiskCapitalProject.UndiversifiedYearCollection = undiversifiedYearCollection.ToArray();
                                undiversifiedRiskCapitalProject.RiskRegistrasi = riskRegistrasi.ToArray();
                            }
                            else
                            {
                                throw new ApplicationException(string.Format("Risk Matrix untuk proyek {0} pada skenario {1} tidak ditemukan / belum diisi.", item.Project.NamaProject, scenario.NamaScenario));
                            }
                            AggregationOfProjectCollection aggregationOfProjectCollection = new AggregationOfProjectCollection();
                            aggregationOfProjectCollection.NamaProject = item.Project.NamaProject;
                            aggregationOfProjectCollection.ProjectId = item.Project.Id;
                            aggregationOfProjectCollection.NamaSektor = item.Project.Sektor.NamaSektor;
                            aggregationOfProjectCollection.SektorId = item.Project.SektorId;
                            aggregationOfProjectCollections.Add(aggregationOfProjectCollection);
                        }
                        aggregationOfProject.AggregationOfProjectCollection = aggregationOfProjectCollections.ToArray();
                    }

                    calculationResult.ScenarioId = scenario.Id;
                    calculationResult.NamaScenario = scenario.NamaScenario;
                    calculationResult.ProjectDetail = undiversifiedRiskCapitalProjectCollection.ToArray();

                    #region Generate AggregationOfProject
                    //get aggregation project undiversified
                    var aggregationUndiversifiedProjectCapital = GetAggregationUndiversifiedProjectCapital(generateYear, calculationResult.ProjectDetail);
                    aggregationOfProject.AggregationUndiversifiedProjectCapital = aggregationUndiversifiedProjectCapital.ToArray();
                    //get aggregation project intra-diversified
                    var aggregationIntraDiversifiedProjectCapital = GetAggregationIntraDiversifiedProjectCapital(generateYear, calculationResult.ProjectDetail);
                    aggregationOfProject.AggregationIntraDiversifiedProjectCapital = aggregationIntraDiversifiedProjectCapital.ToArray();
                    //get aggregation project inter-diversified
                    var aggregationInterDiversifiedProjectCapital = GetAggregationInterDiversifiedProjectCapital(scenario.Id, generateYear, aggregationOfProject);
                    aggregationOfProject.AggregationInterDiversifiedProjectCapital = aggregationInterDiversifiedProjectCapital.ToArray();
                    calculationResult.AggregationOfProject = aggregationOfProject;
                    #endregion Generate AggregationOfProject

                    #region Generate AggregationOfSektor
                    AggregationSektor aggregationSektor = new AggregationSektor();
                    //Undiversified
                    var aggregationOfSektor = GetUndiversifiedAggregationOfSector(scenario.Id, aggregationOfProject);
                    //IntraDiversified
                    var intraDiversified = GenerateIntraProjectDiversified(scenario, aggregationOfSektor.Sektor, aggregationOfProjectCollections, aggregationIntraDiversifiedProjectCapital);
                    //InterDiversified
                    var interDiversified = GenerateInterProjectDiversified(scenario, aggregationOfSektor.Sektor, aggregationOfProjectCollections, aggregationInterDiversifiedProjectCapital);
                    #endregion Generate AggregationOfSektor

                    #region Generate AggregationOfRisk
                    AggregationRisk aggregationRisk = new AggregationRisk();
                    //get aggregation risk registrasi
                    aggregationRisk.RiskRegistrasi = riskRegistrasi.ToArray();
                    //get aggregation risk undiversified
                    var undiversifiedAggregationRisk = GetUndiversifiedAggregationOfRisk(riskRegistrasi, generateYear, calculationResult.ProjectDetail);
                    aggregationRisk.UndiversifiedAggregationRisk = undiversifiedAggregationRisk;
                    //get aggregation risk intra-diversified 
                    var intraProjectDiversifiedAggregationRisk = GetIntraProjectDiversifiedAggregationOfRisk(riskRegistrasi, generateYear, calculationResult.ProjectDetail);
                    aggregationRisk.IntraProjectDiversifiedAggregationRisk = intraProjectDiversifiedAggregationRisk;
                    //get aggregation risk intra-diversified 
                    var interProjectDiversifiedAggregationRisk = GetInterProjectDiversifiedAggregationOfRisk(riskRegistrasi, generateYear, intraProjectDiversifiedAggregationRisk.RiskYearCollection, aggregationIntraDiversifiedProjectCapital, aggregationInterDiversifiedProjectCapital);
                    aggregationRisk.InterProjectDiversifiedAggregationRisk = interProjectDiversifiedAggregationRisk;

                    calculationResult.AggregationRisk = aggregationRisk;
                    #endregion Generate AggregationOfRisk

                    aggregationSektor.UndiversifiedAggregationSektor = aggregationOfSektor.UndiversifiedAggregationSektor;
                    aggregationSektor.Sektor = aggregationOfSektor.Sektor;
                    aggregationSektor.IntraProjectDiversifiedAggregationSektor = intraDiversified;
                    aggregationSektor.InterProjectDiversifiedAggregationSektor = interDiversified;

                    calculationResult.AggregationSektor = aggregationSektor;

                    var assetProjection = GenerateAssetProjectionInvestmentStrategy(aggregationOfProject.AggregationInterDiversifiedProjectCapital, generateYear, scenario, aggregationOfProject);
                    calculationResult.AssetProjectionLiquidity = assetProjection;

                    #region Monitoring
                    var projectMonitoring = _projectRepository.GetByTahapan(projects);

                    var sektorMonitoring = projectMonitoring.Select(x => x.SektorId).Distinct().ToArray();

                    var riskRegistrasiMonitoring = _projectRiskRegistrasiRepository.GetByProjectId(projectMonitoring.Select(x => x.Id).ToArray()).ToArray();


                    calculationResult.ProjectMonitoring = projectMonitoring.Select(x => x.Id).ToArray();
                    calculationResult.SektorMonitoring = sektorMonitoring;
                    calculationResult.RiskRegistrasiMonitoring = riskRegistrasiMonitoring;
                    #endregion Monitoring

                    calculationResults.Add(calculationResult);

                    //Generate tahun Scenario Testing
                    var years = generateYear.Except(tahun);
                    foreach (var yearValue in years)
                    {
                        tahun.Add(yearValue);
                    }

                }
            }
            foreach (var calculationResult in calculationResults)
            {
                if (calculationResult.ScenarioId == scenarioDefault.Id)
                {
                    calculation.CalculationResult = calculationResult;
                }
            }

            var arrayTahun = tahun.ToArray();
            Array.Sort(arrayTahun);
            scenarioTesting.Year = arrayTahun;
            calculation.ScenarioTesting = scenarioTesting;

            var scenarioTestingCollection = GetScenarioTestingCollection(arrayTahun, calculationResults);
            var sensitivity = GetSensitivity(arrayTahun, calculationResults);
            var stressTesting = GetStressTesting(arrayTahun, calculationResults);

            //Calculation Scenario Testing
            calculation.ScenarioTesting.ScenarioTestingCollection = scenarioTestingCollection.ToArray();
            calculation.ScenarioTesting.Sensitivity = sensitivity;
            calculation.ScenarioTesting.StressTesting = stressTesting;

            return calculation;
        }

        public void GenerteScenarioCalculationRisk(ScenarioCollection param)
        {
            _scenarioCalculationRepository.Delete();
            _projectCalculationRepository.Delete();

            for (int i = 0; i < param.ScenarioProject.Length; i++)
            {
                var scenario = _scenarioRepository.Get(param.ScenarioProject[i].ScenarioId);
                ScenarioCalculation modelScenario = new ScenarioCalculation(scenario);
                _scenarioCalculationRepository.Insert(modelScenario);
                _iDatabaseContext.SaveChanges();

                var scenarioCalculation = _scenarioCalculationRepository.Get(modelScenario.Id);
                for (int j = 0; j < param.ScenarioProject[i].ProjectId.Length; j++)
                {
                    var project = _projectRepository.Get(param.ScenarioProject[i].ProjectId[j]);
                    ProjectCalculation modelProject = new ProjectCalculation(project, scenarioCalculation);
                    _projectCalculationRepository.Insert(modelProject);
                    _iDatabaseContext.SaveChanges();
                }
            }
        }
        public IList<int> GenerateYear(ScenarioProject scenarios)
        {
            var scenario = _scenarioRepository.Get(scenarios.ScenarioId);
            var likelihoodDetail = scenario.Likehood.LikehoodDetails;
            var riskRegistrasi = _riskRegistrasiRepository.GetAll().ToList();
            //var projects = scenario.ScenarioDetail.Where(x => x.IsDelete == false).ToList();
            var proyek = _projectRepository.GetAll().Where(x => x.IsActive == true)    // your starting point - table in the "from" statement
            .Join(scenario.ScenarioDetail.Where(x => x.IsDelete == false), // the source table of the inner join
            proj => proj.Id,        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
            scdet => scdet.ProjectId,   // Select the foreign key (the second part of the "on" clause)
            (proj, scdet) => scdet).ToList(); // selection

            var projects = proyek    // your starting point - table in the "from" statement
            .Join(scenarios.ProjectId, // the source table of the inner join
            proj => proj.ProjectId,        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
            scdet => scdet,   // Select the foreign key (the second part of the "on" clause)
            (proj, scdet) => proj).ToList(); // selection

            List<int> tahunAwal = new List<int>();
            List<int> tahunAkhir = new List<int>();

            if (projects.Count > 0)
            {
                foreach (var item in projects)
                {
                    var riskMatrixProject = _riskMatrixProjectRepository.GetByScenarioIdProjectId(scenario.Id, item.ProjectId);
                    Validate.NotNull(riskMatrixProject, "Risk Matrix untuk proyek {0} pada skenario {1} belum disetujui.", item.Project.NamaProject, scenario.NamaScenario);

                    var project = item.Project;
                    var stageTahunRiskMatrix = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectId(riskMatrixProject.Id).ToList();
                    if (stageTahunRiskMatrix.Count > 0)
                    {
                        foreach (var dataStage in stageTahunRiskMatrix)
                        {
                            decimal? totalPerYear = 0;

                            List<decimal?> Total = new List<decimal?>();

                            var stageTahunDetail = _stageTahunRiskMatrixDetailRepository.GetByStageTahunRiskMatrixId(dataStage.Id).ToList();

                            Total.Add(totalPerYear);
                        }
                        tahunAwal.Add(project.TahunAwalProject.Year);
                        tahunAkhir.Add(project.TahunAkhirProject.Year);
                    }
                    else
                    {
                        throw new ApplicationException(string.Format("Tahun Stage pada proyek {0} dengan skenario {1} tidak ditemukan / belum diisi. Silakan cek di menu Riskio Matriks - Tambah Risiko Matriks dan pilih proyek {0}.", project.NamaProject, scenario.NamaScenario));
                    }
                }
            }
            //int thnAwal = tahunAwal.Min();
            int thnAwal = 2017;
            int thnAkhir = tahunAkhir.Max();
            int interval = thnAkhir - thnAwal;
            List<int> tahun = new List<int>();
            for (int i = 0; i <= interval; i++)
            {
                tahun.Add(i + thnAwal);
            }
            return tahun;
        }
        

        #region 1.Project Detail

        public UndiversifiedRiskCapitalProjectCollection GetUndiversifiedByProject(int projectId, int scenarioId)
        {
            var scenario = _scenarioRepository.Get(scenarioId);
            var likelihoodDetail = scenario.Likehood.LikehoodDetails;
            var project = _projectRepository.Get(projectId);
            var riskRegistrasi = _riskRegistrasiRepository.GetAll().ToList();

            IList<UndiversifiedYearCollection> undiversifiedYearCollection = new List<UndiversifiedYearCollection>();
            UndiversifiedRiskCapitalProjectCollection result = new UndiversifiedRiskCapitalProjectCollection();

            if (project != null)
            {
                var riskMatrixProject = _riskMatrixProjectRepository.GetByScenarioIdProjectId(scenario.Id, projectId);
                var stageTahunRiskMatrix = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectId(riskMatrixProject.Id).ToList();

                if (stageTahunRiskMatrix.Count > 0)
                {
                    foreach (var dataStage in stageTahunRiskMatrix)
                    {
                        decimal? totalPerYearAverage = 0;
                        decimal? totalPerYearUpper = 0;
                        decimal? totalPerYearLower = 0;

                        UndiversifiedYearCollection undiversifiedYear = new UndiversifiedYearCollection();
                        undiversifiedYear.Year = dataStage.Tahun;

                        IList<YearValue> yearValues = new List<YearValue>();

                        var stageTahunDetail = _stageTahunRiskMatrixDetailRepository.GetByStageTahunRiskMatrixId(dataStage.Id).ToList();
                        if (stageTahunDetail.Count > 0)
                        {
                            foreach (var detailItem in stageTahunDetail)
                            {
                                decimal? valAverage = 0;
                                decimal? valLower = 0;
                                decimal? valUpper = 0;
                                var likehoodFiltered = likelihoodDetail.Where(x => x.Id == detailItem.LikehoodDetailId).FirstOrDefault();
                                if (likehoodFiltered != null)
                                {
                                    var nilaiAverage = decimal.Round(likehoodFiltered.Average / 100, 28);
                                    var nilaiLower = decimal.Round(likehoodFiltered.Lower / 100, 28);
                                    var nilaiUpper = decimal.Round(likehoodFiltered.Upper / 100, 28);
                                    var nilaiExpose = decimal.Round(detailItem.NilaiExpose.Value, 28);
                                    Validate.NotNull(nilaiExpose, "Nilai expose pada proyek {1} tahun {2} dengan skenario {0} bernilai null/belum diisi.", scenario.NamaScenario, detailItem.StageTahunRiskMatrix.RiskMatrixProject.Project.NamaProject, detailItem.StageTahunRiskMatrix.Tahun);

                                    valAverage = nilaiExpose * nilaiAverage;
                                    valLower = nilaiExpose * nilaiLower;
                                    valUpper = nilaiExpose * nilaiUpper;
                                }

                                YearValue year = new YearValue();
                                year.ScenarioId = scenario.Id;
                                year.RiskRegistrasiId = detailItem.RiskRegistrasiId;
                                year.LikehoodId = scenario.LikehoodId;
                                year.ValueUndiversified = valAverage;
                                year.ValueUndiversifiedLower = valLower;
                                year.ValueUndiversifiedUpper = valUpper;

                                totalPerYearAverage += valAverage;
                                totalPerYearLower += valLower;
                                totalPerYearUpper += valUpper;

                                yearValues.Add(year);
                            }
                        }
                        else
                        {
                            throw new ApplicationException(string.Format("Tahun {0} untuk proyek {1} dengan skenario {2} di Risk Matrix tidak ditemukan / belum diisi.", dataStage.Tahun, project.NamaProject, scenario.NamaScenario));
                        }

                        undiversifiedYear.YearValue = yearValues.ToArray();
                        undiversifiedYearCollection.Add(undiversifiedYear);
                        undiversifiedYear.Total = totalPerYearAverage;
                        undiversifiedYear.TotalLower = totalPerYearLower;
                        undiversifiedYear.TotalUpper = totalPerYearUpper;
                    }

                    result.ScenarioId = scenario.Id;
                    result.NamaScenario = scenario.NamaScenario;
                    result.ProjectId = project.Id;
                    result.NamaProject = project.NamaProject;
                    result.SektorId = project.SektorId;
                    result.NamaSektor = project.Sektor.NamaSektor;
                    result.UndiversifiedYearCollection = undiversifiedYearCollection.ToArray();
                    result.RiskRegistrasi = riskRegistrasi.ToArray();
                }
            }

            //Get CorrelatedSektorDetail
            var correlatedSektor = _correlatedSektorRepository.GetByScenarioIdSektorIdCst(scenario.Id, result.SektorId);
            Validate.NotNull(correlatedSektor, "Korelasi Sektor {1} pada skenario {1} tidak ditemukan.", scenario.NamaScenario, result.NamaSektor);
            var correlatedSektorDetail = _correlatedSektorDetailRepository.GetByCorrelatedSektorIdLite(correlatedSektor).ToList();

            //Get Diversified
            var diversified = GetDiversifiedRiskCapital(result, correlatedSektorDetail);
            result.DiversifiedRiskCapitalCollection = diversified;
            result.CorrelatedSektorDetail = correlatedSektorDetail;

            return result;
        }

        public IList<DiversifiedRiskCapitalCollection> GetDiversifiedRiskCapital(UndiversifiedRiskCapitalProjectCollection undiversified, IList<CorrelatedSektorDetailLite> correlatedSektorDetail)
        {
            IList<DiversifiedRiskCapital> diversifiedRiskCapitalList = new List<DiversifiedRiskCapital>();

            //temp year total only
            IList<YearTotalDiversified> yearTotalList = new List<YearTotalDiversified>();

            for (int j = 0; j < undiversified.UndiversifiedYearCollection.Length; j++)
            {
                DiversifiedRiskCapital diversifiedRiskCapital = new DiversifiedRiskCapital();

                //temp year total only
                YearTotalDiversified yearTotal = new YearTotalDiversified();

                var undiversifiedYear = undiversified.UndiversifiedYearCollection[j].YearValue;
                if (undiversifiedYear.Length > 0)
                {
                    if (undiversified.RiskRegistrasi.Count() > 0)
                    {
                        DiversifiedRiskCapitalCollection diversifiedRiskCapitalCollection = new DiversifiedRiskCapitalCollection();

                        //temp
                        IList<decimal?> valuePerColumnAverage = new List<decimal?>();
                        IList<decimal?> valuePerColumnLower = new List<decimal?>();
                        IList<decimal?> valuePerColumnUpper = new List<decimal?>();
                        for (int i = 0; i < undiversified.RiskRegistrasi.Length; i++)
                        {
                            IList<decimal?> mmultAverage = new List<decimal?>();
                            IList<decimal?> mmultLower = new List<decimal?>();
                            IList<decimal?> mmultUpper = new List<decimal?>();
                            int u = 0;
                            var correlatedSektorDetailByRisk = correlatedSektorDetail.Where(x => x.RiskRegistrasiIdCol == undiversified.RiskRegistrasi[i].Id && x.IsDelete == false).ToList();
                            if (correlatedSektorDetailByRisk == null || correlatedSektorDetailByRisk.Count == 0)
                            {
                                throw new ApplicationException(string.Format("Risk Registrasi {0} pada Project {1} di Scenario {2} tidak ada pada Correlated Sektor {3}", undiversified.RiskRegistrasi[i].NamaCategoryRisk, undiversified.NamaProject, undiversified.NamaScenario, undiversified.NamaSektor));
                            }
                            else if (undiversifiedYear.Count() != correlatedSektorDetailByRisk.Count())
                            {
                                throw new ApplicationException(string.Format("Jumlah Risk Registrasi pada Project {0} di Scenario {1} tidak sama dengan jumlah Risk Registrasi pada Correlated Sektor {2} ", undiversified.NamaProject, undiversified.NamaScenario, undiversified.NamaSektor));
                            }
                            else
                            {
                                for (int c = 0; c < correlatedSektorDetailByRisk.Count; c++)
                                {
                                    var resultAverage = decimal.Round(undiversifiedYear[u].ValueUndiversified.Value, 28) * decimal.Round(correlatedSektorDetailByRisk[c].CorrelationMatrixValue, 28);
                                    var resultLower = decimal.Round(undiversifiedYear[u].ValueUndiversifiedLower.Value, 28) * decimal.Round(correlatedSektorDetailByRisk[c].CorrelationMatrixValue, 28);
                                    var resultUpper = decimal.Round(undiversifiedYear[u].ValueUndiversifiedUpper.Value, 28) * decimal.Round(correlatedSektorDetailByRisk[c].CorrelationMatrixValue, 28);
                                    mmultAverage.Add(resultAverage);
                                    mmultLower.Add(resultLower);
                                    mmultUpper.Add(resultUpper);

                                    u += 1;
                                }
                            }
                            valuePerColumnAverage.Add(mmultAverage.Sum());
                            valuePerColumnLower.Add(mmultLower.Sum());
                            valuePerColumnUpper.Add(mmultUpper.Sum());
                        }

                        IList<decimal?> mmultAverageExt = new List<decimal?>();
                        IList<decimal?> mmultLowerExt = new List<decimal?>();
                        IList<decimal?> mmultUpperExt = new List<decimal?>();
                        for (int i = 0; i < valuePerColumnAverage.Count; i++)
                        {
                            var mmultAverage = decimal.Round(undiversifiedYear[i].ValueUndiversified.Value, 28) * decimal.Round(valuePerColumnAverage[i].Value, 28);
                            var mmultLower = decimal.Round(undiversifiedYear[i].ValueUndiversifiedLower.Value, 28) * decimal.Round(valuePerColumnLower[i].Value, 28);
                            var mmultUpper = decimal.Round(undiversifiedYear[i].ValueUndiversifiedUpper.Value, 28) * decimal.Round(valuePerColumnUpper[i].Value, 28);
                            mmultAverageExt.Add(mmultAverage);
                            mmultLowerExt.Add(mmultLower);
                            mmultUpperExt.Add(mmultUpper);
                        }
                        yearTotal.Year = undiversified.UndiversifiedYearCollection[j].Year;
                        yearTotal.Total = (decimal)Math.Sqrt(Math.Abs((double)mmultAverageExt.Sum()));
                        yearTotal.TotalLower = (decimal)Math.Sqrt(Math.Abs((double)mmultLowerExt.Sum()));
                        yearTotal.TotalUpper = (decimal)Math.Sqrt(Math.Abs((double)mmultUpperExt.Sum()));

                        yearTotalList.Add(yearTotal);
                    }
                }
                else
                {
                    throw new ApplicationException(string.Format("Nilai Undiversified per Year {0} tidak ditemukan."));
                }
            }

            //Risk Value per Year calculation
            IList<DiversifiedRiskCapitalCollection> finalDiversifiedRiskCapitalCollection = new List<DiversifiedRiskCapitalCollection>();

            if (yearTotalList.Count > 0)
            {
                DiversifiedRiskCapital result = new DiversifiedRiskCapital();
                for (int y = 0; y < yearTotalList.Count; y++)
                {
                    var yr = yearTotalList[y].Year;
                    if (undiversified.RiskRegistrasi.Count() > 0)
                    {
                        DiversifiedRiskCapitalCollection diversifiedRiskCapital = new DiversifiedRiskCapitalCollection();
                        IList<RiskValueCollection> riskValueCollection = new List<RiskValueCollection>();
                        for (int r = 0; r < undiversified.RiskRegistrasi.Length; r++)
                        {
                            RiskValueCollection riskValue = new RiskValueCollection();
                            var undiversifiedList = undiversified.UndiversifiedYearCollection;
                            if (undiversifiedList.Length > 0)
                            {
                                var undiversifiedRisk = undiversifiedList.FirstOrDefault(x => x.Year == yr);
                                if (undiversifiedRisk != null)
                                {
                                    var undiVal = undiversifiedRisk.YearValue.SingleOrDefault(x => x.RiskRegistrasiId == undiversified.RiskRegistrasi[r].Id);
                                    var totUndiversifiedPerYearDecimal = undiversifiedRisk.Total;
                                    var totUndiversifiedPerYearDecimalLower = undiversifiedRisk.TotalLower;
                                    var totUndiversifiedPerYearDecimalUpper = undiversifiedRisk.TotalUpper;

                                    var gettingRiskValPerYear = decimal.Round(undiVal.ValueUndiversified.Value, 28) * decimal.Round(yearTotalList[y].Total.Value, 28);
                                    var gettingRiskValPerYearLower = decimal.Round(undiVal.ValueUndiversifiedLower.Value, 28) * decimal.Round(yearTotalList[y].TotalLower.Value, 28);
                                    var gettingRiskValPerYearUpper = decimal.Round(undiVal.ValueUndiversifiedUpper.Value, 28) * decimal.Round(yearTotalList[y].TotalUpper.Value, 28);

                                    var getTwoDigit = totUndiversifiedPerYearDecimal;
                                    var getTwoDigitLower = totUndiversifiedPerYearDecimalLower;
                                    var getTwoDigitUpper = totUndiversifiedPerYearDecimalUpper;

                                    decimal? riskValPerYear;
                                    if (getTwoDigit == 0)
                                    {
                                        riskValPerYear = 0;
                                    }
                                    else
                                    {
                                        riskValPerYear = decimal.Round(gettingRiskValPerYear, 28) / decimal.Round(totUndiversifiedPerYearDecimal.Value, 28);
                                    }

                                    decimal? riskValPerYearLower;
                                    if (getTwoDigitLower == 0)
                                    {
                                        riskValPerYearLower = 0;
                                    }
                                    else
                                    {
                                        riskValPerYearLower = decimal.Round(gettingRiskValPerYearLower, 28) / decimal.Round(totUndiversifiedPerYearDecimalLower.Value, 28);
                                    }

                                    decimal? riskValPerYearUpper;
                                    if (getTwoDigit == 0)
                                    {
                                        riskValPerYearUpper = 0;
                                    }
                                    else
                                    {
                                        riskValPerYearUpper = decimal.Round(gettingRiskValPerYearUpper, 28) / decimal.Round(totUndiversifiedPerYearDecimalUpper.Value, 28);
                                    }
                                    //var valDecimal = decimal.Round(riskValPerYear.Value, 2);

                                    riskValue.RiskRegistrasiId = undiversified.RiskRegistrasi[r].Id;
                                    //riskValue.Value = valDecimal;
                                    riskValue.Value = riskValPerYear.Value;
                                    riskValue.ValueLower = riskValPerYearLower;
                                    riskValue.ValueUpper = riskValPerYearUpper;
                                    riskValueCollection.Add(riskValue);
                                }
                            }
                        }
                        //var getTwoDigitDiversifiedRiskCapitalTotalDecimal = decimal.Round(yearTotalList[y].Total.Value, 2);

                        diversifiedRiskCapital.Year = yearTotalList[y].Year;
                        //diversifiedRiskCapital.Total = getTwoDigitDiversifiedRiskCapitalTotalDecimal;
                        diversifiedRiskCapital.Total = yearTotalList[y].Total.Value;
                        diversifiedRiskCapital.TotalLower = yearTotalList[y].TotalLower;
                        diversifiedRiskCapital.TotalUpper = yearTotalList[y].TotalUpper;
                        diversifiedRiskCapital.RiskValueCollection = riskValueCollection.ToArray();

                        finalDiversifiedRiskCapitalCollection.Add(diversifiedRiskCapital);
                    }
                }
            }

            return finalDiversifiedRiskCapitalCollection;
        }
        #endregion 1.Project Detail

        #region 2.AggregationOfProject
        public IList<AggregationUndiversifiedProjectCapital> GetAggregationUndiversifiedProjectCapital(IList<int> generateYear, IList<UndiversifiedRiskCapitalProjectCollection> undiversifiedRiskCapitalProjectCollection)
        {
            //AggregationUndiversifiedProjectCapital aggregationUndiversifiedProjectCapital = new AggregationUndiversifiedProjectCapital();
            IList<AggregationUndiversifiedProjectCapital> aggregationUndiversifiedProjectCapitals = new List<AggregationUndiversifiedProjectCapital>();

            if (undiversifiedRiskCapitalProjectCollection.Count > 0)
            {
                foreach (var th in generateYear)
                {

                    IList<AggregationUndiversifiedProjectCollection> aggregationUndiversifiedProjectCollections = new List<AggregationUndiversifiedProjectCollection>();
                    IList<decimal?> TotalLower = new List<decimal?>();
                    IList<decimal?> TotalUpper = new List<decimal?>();
                    foreach (var item in undiversifiedRiskCapitalProjectCollection)
                    {
                        AggregationUndiversifiedProjectCollection aggregationUndiversifiedProjectCollection = new AggregationUndiversifiedProjectCollection();
                        aggregationUndiversifiedProjectCollection.ProjectId = item.ProjectId;
                        aggregationUndiversifiedProjectCollection.SektorId = item.SektorId;
                        aggregationUndiversifiedProjectCollection.Year = th;
                        aggregationUndiversifiedProjectCollection.Total = 0;


                        foreach (var val in item.UndiversifiedYearCollection)
                        {
                            if (val.Year == th)
                            {
                                aggregationUndiversifiedProjectCollection.Total = val.Total;
                                TotalLower.Add(val.TotalLower);
                                TotalUpper.Add(val.TotalUpper);
                            }
                        }
                        aggregationUndiversifiedProjectCollections.Add(aggregationUndiversifiedProjectCollection);
                    }
                    var totalPerYear = aggregationUndiversifiedProjectCollections.Sum(x => x.Total);
                    var finalValuePerYear = totalPerYear.Value;
                    var totalPerYearLower = (TotalLower.Sum()).Value;
                    var totalPerYearUpper = (TotalUpper.Sum()).Value;

                    AggregationUndiversifiedProjectCapital aggregationUndiversifiedProjectCapital = new AggregationUndiversifiedProjectCapital();
                    aggregationUndiversifiedProjectCapital.AggregationUndiversifiedProjectCollection = aggregationUndiversifiedProjectCollections.ToArray();
                    aggregationUndiversifiedProjectCapital.aggregationYears = th;
                    aggregationUndiversifiedProjectCapital.TotalPerYear = finalValuePerYear;
                    aggregationUndiversifiedProjectCapital.TotalPerYearLower = totalPerYearLower;
                    aggregationUndiversifiedProjectCapital.TotalPerYearUpper = totalPerYearUpper;
                    aggregationUndiversifiedProjectCapitals.Add(aggregationUndiversifiedProjectCapital);
                }
            }

            return aggregationUndiversifiedProjectCapitals;
        }

        public IList<AggregationIntraDiversifiedProjectCapital> GetAggregationIntraDiversifiedProjectCapital(IList<int> generateYear, IList<UndiversifiedRiskCapitalProjectCollection> undiversifiedRiskCapitalProjectCollection)
        {
            IList<AggregationIntraDiversifiedProjectCapital> aggregationIntraDiversifiedProjectCapitals = new List<AggregationIntraDiversifiedProjectCapital>();
            IList<AggregationIntraDiversifiedProjectCollection> aggregationIntraDiversifiedProjectCollections = new List<AggregationIntraDiversifiedProjectCollection>();

            if (undiversifiedRiskCapitalProjectCollection.Count > 0)
            {
                foreach (var th in generateYear)
                {
                    foreach (var item in undiversifiedRiskCapitalProjectCollection)
                    {
                        decimal? totalPerProject = 0;
                        decimal? totalPerProjectLower = 0;
                        decimal? totalPerProjectUpper = 0;

                        foreach (var thn in item.DiversifiedRiskCapitalCollection)
                        {
                            if (th == thn.Year)
                            {
                                totalPerProject += thn.Total;
                                totalPerProjectLower += thn.TotalLower;
                                totalPerProjectUpper += thn.TotalUpper;
                            }
                        }
                        AggregationIntraDiversifiedProjectCollection aggregationIntraDiversifiedProjectCollection = new AggregationIntraDiversifiedProjectCollection();
                        aggregationIntraDiversifiedProjectCollection.ProjectId = item.ProjectId;
                        aggregationIntraDiversifiedProjectCollection.SektorId = item.SektorId;
                        aggregationIntraDiversifiedProjectCollection.Year = th;
                        aggregationIntraDiversifiedProjectCollection.Total = totalPerProject.Value;
                        aggregationIntraDiversifiedProjectCollection.TotalLower = totalPerProjectLower;
                        aggregationIntraDiversifiedProjectCollection.TotalUpper = totalPerProjectUpper;
                        aggregationIntraDiversifiedProjectCollections.Add(aggregationIntraDiversifiedProjectCollection);
                    }
                    var totalPerYear = aggregationIntraDiversifiedProjectCollections.Sum(x => x.Total);

                    AggregationIntraDiversifiedProjectCapital aggregationIntraDiversifiedProjectCapital = new AggregationIntraDiversifiedProjectCapital();
                    aggregationIntraDiversifiedProjectCapital.AggregationIntraDiversifiedProjectCollection = aggregationIntraDiversifiedProjectCollections.ToArray();
                    aggregationIntraDiversifiedProjectCapital.aggregationYears = th;
                    aggregationIntraDiversifiedProjectCapital.TotalPerYear = totalPerYear;
                    aggregationIntraDiversifiedProjectCapitals.Add(aggregationIntraDiversifiedProjectCapital);
                    aggregationIntraDiversifiedProjectCollections.Clear();
                }
            }

            return aggregationIntraDiversifiedProjectCapitals;
        }

        public IList<AggregationInterDiversifiedProjectCapital> GetAggregationInterDiversifiedProjectCapital(int scenarioId, IList<int> generateYear, AggregationOfProject aggregationOfProject)
        {
            //Get CorrelatedSektorDetail
            var correlatedProject = _correlatedProjectRepository.GetByScenarioIdProjectId(scenarioId);
            //Validate.NotNull(correlatedProject, "Korelasi Project {1} pada skenario {1} tidak ditemukan.", correlatedProject.ProjectId, correlatedProject.ScenarioId);

            IList<AggregationInterDiversifiedProjectCapital> aggregationInterDiversifiedProjectCapitals = new List<AggregationInterDiversifiedProjectCapital>();

            if (correlatedProject != null)
            {
                var correlatedProjectDetail = _correlatedProjectDetailRepository.GetByCorrelatedProjectId(correlatedProject.Id).ToList();

                //IList<DiversifiedRiskCapital> diversifiedRiskCapitalList = new List<DiversifiedRiskCapital>();


                //temp year total only
                IList<YearTotalAggregationInterDiversified> yearTotalList = new List<YearTotalAggregationInterDiversified>();

                for (int j = 0; j < aggregationOfProject.AggregationIntraDiversifiedProjectCapital.Length; j++)
                {
                    //DiversifiedRiskCapital diversifiedRiskCapital = new DiversifiedRiskCapital();

                    //temp year total only
                    //YearTotalDiversified yearTotal = new YearTotalDiversified();
                    YearTotalAggregationInterDiversified yearTotal = new YearTotalAggregationInterDiversified();

                    var undiversifiedYear = aggregationOfProject.AggregationIntraDiversifiedProjectCapital[j].AggregationIntraDiversifiedProjectCollection;
                    if (undiversifiedYear.Length > 0)
                    {
                        if (aggregationOfProject.AggregationOfProjectCollection.Count() > 0)
                        {
                            //DiversifiedRiskCapitalCollection diversifiedRiskCapitalCollection = new DiversifiedRiskCapitalCollection();

                            //temp
                            IList<decimal?> valuePerColumnAverage = new List<decimal?>();
                            IList<decimal?> valuePerColumnLower = new List<decimal?>();
                            IList<decimal?> valuePerColumnUpper = new List<decimal?>();
                            for (int i = 0; i < aggregationOfProject.AggregationOfProjectCollection.Length; i++)
                            {
                                IList<decimal?> mmultAverage = new List<decimal?>();
                                IList<decimal?> mmultLower = new List<decimal?>();
                                IList<decimal?> mmultUpper = new List<decimal?>();
                                int u = 0;
                                //var correlatedProjectDetailById = correlatedProjectDetail.Where(x => x.ProjectIdCol == aggregationOfProject.AggregationOfProjectCollection[i].ProjectId).ToList();

                                var correlatedProjectDetailById = correlatedProjectDetail.Where(x => x.ProjectIdCol == aggregationOfProject.AggregationOfProjectCollection[i].ProjectId)    // your starting point - table in the "from" statement
                                .Join(aggregationOfProject.AggregationOfProjectCollection, // the source table of the inner join
                                proj => proj.ProjectIdRow,        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
                                scdet => scdet.ProjectId,   // Select the foreign key (the second part of the "on" clause)
                                (proj, scdet) => proj).ToList(); // selection

                                if (correlatedProjectDetailById == null || correlatedProjectDetailById.Count == 0)
                                {
                                    throw new ApplicationException(string.Format("Project {0} di Scenario {1} tidak ada pada Correlated Project", aggregationOfProject.AggregationOfProjectCollection[i].NamaProject, _scenarioRepository.Get(scenarioId).NamaScenario));
                                }
                                else if (undiversifiedYear.Count() != correlatedProjectDetailById.Count())
                                {
                                    throw new ApplicationException(string.Format("Jumlah Project {0} di Scenario {1} tidak sama dengan jumlah Project pada Correlated Project", aggregationOfProject.AggregationOfProjectCollection[i].NamaProject, _scenarioRepository.Get(scenarioId).NamaScenario));
                                }
                                else
                                {
                                    for (int c = 0; c < correlatedProjectDetailById.Count; c++)
                                    {
                                        var resultAverage = decimal.Round(undiversifiedYear[u].Total.Value, 28) * decimal.Round(correlatedProjectDetailById[c].CorrelationMatrix.Nilai, 28);
                                        var resultLower = decimal.Round(undiversifiedYear[u].TotalLower.Value, 28) * decimal.Round(correlatedProjectDetailById[c].CorrelationMatrix.Nilai, 28);
                                        var resultUpper = decimal.Round(undiversifiedYear[u].TotalUpper.Value, 28) * decimal.Round(correlatedProjectDetailById[c].CorrelationMatrix.Nilai, 28);
                                        mmultAverage.Add(resultAverage);
                                        mmultLower.Add(resultLower);
                                        mmultUpper.Add(resultUpper);

                                        u += 1;
                                    }
                                }
                                valuePerColumnAverage.Add(mmultAverage.Sum());
                                valuePerColumnLower.Add(mmultLower.Sum());
                                valuePerColumnUpper.Add(mmultUpper.Sum());
                            }
                            IList<decimal?> mmultAverageExt = new List<decimal?>();
                            IList<decimal?> mmultLowerExt = new List<decimal?>();
                            IList<decimal?> mmultUpperExt = new List<decimal?>();
                            for (int i = 0; i < valuePerColumnAverage.Count; i++)
                            {
                                var mmultAverage = decimal.Round(undiversifiedYear[i].Total.Value, 28) * decimal.Round(valuePerColumnAverage[i].Value, 28);
                                var mmultLower = decimal.Round(undiversifiedYear[i].TotalLower.Value, 28) * decimal.Round(valuePerColumnLower[i].Value, 28);
                                var mmultUpper = decimal.Round(undiversifiedYear[i].TotalUpper.Value, 28) * decimal.Round(valuePerColumnUpper[i].Value, 28);
                                mmultAverageExt.Add(mmultAverage);
                                mmultLowerExt.Add(mmultLower);
                                mmultUpperExt.Add(mmultUpper);
                            }
                            yearTotal.Year = aggregationOfProject.AggregationIntraDiversifiedProjectCapital[j].aggregationYears;
                            yearTotal.Total = (decimal)Math.Sqrt(Math.Abs((double)mmultAverageExt.Sum()));
                            yearTotal.TotalLower = (decimal)Math.Sqrt(Math.Abs((double)mmultLowerExt.Sum()));
                            yearTotal.TotalUpper = (decimal)Math.Sqrt(Math.Abs((double)mmultUpperExt.Sum()));

                            yearTotalList.Add(yearTotal);
                        }
                    }
                    else
                    {
                        throw new ApplicationException(string.Format("Korelasi Project pada skenario {0} tidak ditemukan.", scenarioId));
                    }
                }

                //Risk Value per Year calculation
                if (yearTotalList.Count > 0)
                {
                    DiversifiedRiskCapital result = new DiversifiedRiskCapital();
                    for (int y = 0; y < yearTotalList.Count; y++)
                    {
                        var yr = yearTotalList[y].Year;
                        if (aggregationOfProject.AggregationOfProjectCollection.Count() > 0)
                        {
                            AggregationInterDiversifiedProjectCapital aggregationInterDiversifiedProjectCapital = new AggregationInterDiversifiedProjectCapital();
                            IList<AggregationInterDiversifiedProjectCollection> aggregationInterDiversifiedProjectCollections = new List<AggregationInterDiversifiedProjectCollection>();
                            for (int r = 0; r < aggregationOfProject.AggregationOfProjectCollection.Length; r++)
                            {
                                AggregationInterDiversifiedProjectCollection aggregationInterDiversifiedProjectCollection = new AggregationInterDiversifiedProjectCollection();
                                var undiversifiedList = aggregationOfProject.AggregationIntraDiversifiedProjectCapital;
                                if (undiversifiedList.Length > 0)
                                {
                                    var undiversifiedRisk = undiversifiedList.FirstOrDefault(x => x.aggregationYears == yr);
                                    if (undiversifiedRisk != null)
                                    {
                                        var undiVal = undiversifiedRisk.AggregationIntraDiversifiedProjectCollection.SingleOrDefault(x => x.ProjectId == aggregationOfProject.AggregationOfProjectCollection[r].ProjectId);
                                        var totUndiversifiedPerYearDecimal = undiversifiedRisk.TotalPerYear;
                                        var gettingRiskValPerYear = decimal.Round(undiVal.Total.Value, 28) * decimal.Round(yearTotalList[y].Total.Value, 28);
                                        var getTwoDigit = totUndiversifiedPerYearDecimal;
                                        var totUndiversifiedPerYear = getTwoDigit;

                                        decimal? riskValPerYear;
                                        if (getTwoDigit == 0)
                                        {
                                            riskValPerYear = 0;
                                        }
                                        else
                                        {
                                            riskValPerYear = decimal.Round(gettingRiskValPerYear, 28) / decimal.Round(totUndiversifiedPerYear.Value, 28);
                                        }
                                        aggregationInterDiversifiedProjectCollection.ProjectId = aggregationOfProject.AggregationOfProjectCollection[r].ProjectId;
                                        aggregationInterDiversifiedProjectCollection.SektorId = aggregationOfProject.AggregationOfProjectCollection[r].SektorId;
                                        aggregationInterDiversifiedProjectCollection.Total = riskValPerYear.Value;
                                        aggregationInterDiversifiedProjectCollection.Year = yearTotalList[y].Year;
                                        aggregationInterDiversifiedProjectCollections.Add(aggregationInterDiversifiedProjectCollection);
                                    }
                                }
                            }
                            var getTwoDigitDiversifiedRiskCapitalTotalDecimal = (yearTotalList[y].Total.Value);
                            var TotalLower = (yearTotalList[y].TotalLower.Value);
                            var TotalUpper = (yearTotalList[y].TotalUpper.Value);

                            aggregationInterDiversifiedProjectCapital.aggregationYears = yearTotalList[y].Year;
                            aggregationInterDiversifiedProjectCapital.TotalPerYear = getTwoDigitDiversifiedRiskCapitalTotalDecimal;
                            aggregationInterDiversifiedProjectCapital.TotalPerYearLower = TotalLower;
                            aggregationInterDiversifiedProjectCapital.TotalPerYearUpper = TotalUpper;
                            aggregationInterDiversifiedProjectCapital.AggregationInterDiversifiedProjectCollection = aggregationInterDiversifiedProjectCollections.ToArray();

                            aggregationInterDiversifiedProjectCapitals.Add(aggregationInterDiversifiedProjectCapital);
                        }
                    }
                }
            }

            else
            {
                var scenario = _scenarioRepository.Get(scenarioId);
                throw new ApplicationException(string.Format("Korelasi Proyek pada Scenario {0} belum ada yang disetujui.", scenario.NamaScenario));
            }

            return aggregationInterDiversifiedProjectCapitals;
        }

        #endregion 2.AggregationOfProject

        #region 3.AggregationOfSector
        public AggregationSektor GetUndiversifiedAggregationOfSector(int scenarioId, AggregationOfProject aggregationOfProject)
        {
            AggregationSektor result = new AggregationSektor();
            UndiversifiedAggregationSektor undiversified = new UndiversifiedAggregationSektor();

            //var sektors = _sektorRepository.GetAll();
            var sektors = aggregationOfProject.AggregationOfProjectCollection.Select(x=> new SektorLite {SektorId = x.SektorId, NamaSektor = x.NamaSektor }).GroupBy(x=> new {x.SektorId ,x.NamaSektor }).Select(x=> x.FirstOrDefault());
            var projects = aggregationOfProject.AggregationOfProjectCollection;
            var years = aggregationOfProject.AggregationUndiversifiedProjectCapital;
            IList<YearCollection> yearList = new List<YearCollection>();

            for (int y = 0; y < years.Length; y++)
            {
                IList<YearValues> yearValues = new List<YearValues>();
                YearCollection year = new YearCollection();

                foreach (var itemSektor in sektors)
                {
                    YearValues value = new YearValues();

                    var listValueByYear = years[y].AggregationUndiversifiedProjectCollection.Where(x => x.SektorId == itemSektor.SektorId).ToList();
                    if (listValueByYear.Count > 0)
                    {
                        var totalYearThisSektor = listValueByYear.Sum(x => x.Total);
                        var finalValuePerYearPerSektor = totalYearThisSektor.Value;

                        year.Year = listValueByYear[0].Year;

                        value.SektorId = itemSektor.SektorId;
                        value.Value = finalValuePerYearPerSektor;

                        yearValues.Add(value);
                    }
                    else
                    {
                        value.SektorId = itemSektor.SektorId;
                        value.Value = 0;
                        yearValues.Add(value);
                    }
                }
                var grandTotalPerYear = yearValues.Sum(x => x.Value);
                year.Total = grandTotalPerYear;
                year.YearValues = yearValues.ToArray();
                yearList.Add(year);
            }

            undiversified.YearCollection = yearList.ToArray();

            #region set Sektor
            if (sektors.Count() > 0)
            {
                result.Sektor = sektors.ToArray();
            }
            else
            {
                throw new ApplicationException(string.Format("Daftar Sektor tidak ditemukan."));
            }
            #endregion set Sektor

            
            result.UndiversifiedAggregationSektor = undiversified;

            return result;
        }

        //Generate IntraProjectDiversified Aggregation of Sector
        public IntraProjectDiversifiedAggregationSektor GenerateIntraProjectDiversified(Scenario scenario, IList<SektorLite> sektorCollection,
            IList<AggregationOfProjectCollection> projectCollection, IList<AggregationIntraDiversifiedProjectCapital> aggregationIntraCollection)
        {
            IntraProjectDiversifiedAggregationSektor result = new IntraProjectDiversifiedAggregationSektor();
            var years = aggregationIntraCollection.ToList();
            IList<YearCollection> yearList = new List<YearCollection>();

            if (years.Count > 0)
            {
                for (int i = 0; i < years.Count; i++)
                {
                    IList<YearValues> yearValues = new List<YearValues>();
                    YearCollection year = new YearCollection();

                    if (sektorCollection.Count > 0)
                    {
                        foreach (var item in sektorCollection)
                        {
                            YearValues value = new YearValues();
                            var listValueByYear = years[i].AggregationIntraDiversifiedProjectCollection.Where(x => x.SektorId == item.SektorId).ToList();
                            if (listValueByYear.Count > 0)
                            {
                                //fomating value
                                var totalYearThisSektor = listValueByYear.Sum(x => x.Total);
                                var finalValuePerYearPerSektor = totalYearThisSektor.Value;

                                year.Year = listValueByYear[0].Year;

                                value.SektorId = item.SektorId;
                                value.Value = finalValuePerYearPerSektor;

                                yearValues.Add(value);
                            }
                            else
                            {
                                value.SektorId = item.SektorId;
                                value.Value = 0;
                                yearValues.Add(value);
                            }
                        }
                        var grandTotalPerYear = yearValues.Sum(x => x.Value);
                        year.Total = grandTotalPerYear;
                        year.YearValues = yearValues.ToArray();
                        yearList.Add(year);
                    }
                    else
                    {
                        throw new ApplicationException(string.Format("Daftar Sektor pada master data tidak ditemukan."));
                    }
                }

                result.SektorIntraDiversified = sektorCollection.ToArray();
                result.YearCollection = yearList.ToArray();
            }
            else
            {
                throw new ApplicationException(string.Format("Data Intra-Project Diversified Aggregation of Project tidak ditemukan."));
            }
            return result;
        }

        public InterProjectDiversifiedAggregationSektor GenerateInterProjectDiversified(Scenario scenario, IList<SektorLite> sektorCollection,
            IList<AggregationOfProjectCollection> projectCollection, IList<AggregationInterDiversifiedProjectCapital> aggregationInterCollection)
        {
            InterProjectDiversifiedAggregationSektor result = new InterProjectDiversifiedAggregationSektor();
            var years = aggregationInterCollection.ToList();
            IList<YearCollection> yearList = new List<YearCollection>();

            if (years.Count > 0)
            {
                for (int i = 0; i < years.Count; i++)
                {
                    IList<YearValues> yearValues = new List<YearValues>();
                    YearCollection year = new YearCollection();

                    if (sektorCollection.Count > 0)
                    {
                        foreach (var item in sektorCollection)
                        {
                            YearValues value = new YearValues();
                            var listValueByYear = years[i].AggregationInterDiversifiedProjectCollection.Where(x => x.SektorId == item.SektorId).ToList();
                            if (listValueByYear.Count > 0)
                            {
                                var totalYearThisSektor = listValueByYear.Sum(x => x.Total);
                                var finalValuePerYearPerSektor = totalYearThisSektor.Value;
                                year.Year = listValueByYear[0].Year;
                                value.SektorId = item.SektorId;
                                value.Value = finalValuePerYearPerSektor;
                                yearValues.Add(value);
                            }
                            else
                            {
                                value.SektorId = item.SektorId;
                                value.Value = 0;
                                yearValues.Add(value);
                            }
                        }
                        var grandTotalPerYear = yearValues.Sum(x => x.Value);
                        year.Total = grandTotalPerYear;
                        year.YearValues = yearValues.ToArray();
                        yearList.Add(year);
                    }
                    else
                    {
                        throw new ApplicationException(string.Format("Daftar Sektor pada master data tidak ditemukan."));
                    }
                }

                result.SektorInterDiversified = sektorCollection.ToArray();
                result.YearCollection = yearList.ToArray();
            }
            else
            {
                throw new ApplicationException(string.Format("Data Inter-Project Diversified Aggregation of Project tidak ditemukan."));
            }

            return result;
        }
        #endregion 3.AggregationOfSector

        #region 4. AggregationOfRisk
        public UndiversifiedAggregationRisk GetUndiversifiedAggregationOfRisk(IList<RiskRegistrasi> risk, IList<int> generateYear, IList<UndiversifiedRiskCapitalProjectCollection> undiversified)
        {
            UndiversifiedAggregationRisk undiversifiedAggregationRisk = new UndiversifiedAggregationRisk();
            IList<RiskYearCollection> riskYearCollections = new List<RiskYearCollection>();
            if (risk.Count > 0)
            {
                foreach (var itemYear in generateYear)
                {
                    IList<RiskYearValue> riskYearValues = new List<RiskYearValue>();
                    foreach (var itemRisk in risk)
                    {
                        decimal? valueUndiversified = 0;
                        foreach (var itemProject in undiversified)
                        {
                            foreach (var itemProjectYear in itemProject.UndiversifiedYearCollection)
                            {
                                if (itemProjectYear.Year == itemYear)
                                {
                                    foreach (var itemProjectRisk in itemProjectYear.YearValue)
                                    {
                                        if (itemProjectRisk.RiskRegistrasiId == itemRisk.Id)
                                        {
                                            valueUndiversified += itemProjectRisk.ValueUndiversified;
                                        }
                                    }
                                }
                            }
                        }
                        RiskYearValue riskYearValue = new RiskYearValue();
                        riskYearValue.RiskRegistrasiId = itemRisk.Id;
                        riskYearValue.Value = valueUndiversified.Value;
                        riskYearValues.Add(riskYearValue);
                    }
                    var totalYearValue = riskYearValues.Sum(x => x.Value);
                    RiskYearCollection riskYearCollection = new RiskYearCollection();
                    riskYearCollection.Year = itemYear;
                    riskYearCollection.RiskYearValue = riskYearValues.ToArray();
                    riskYearCollection.Total = totalYearValue.Value;
                    riskYearCollections.Add(riskYearCollection);
                }
                undiversifiedAggregationRisk.RiskYearCollection = riskYearCollections.ToArray();
            }
            else
            {
                throw new ApplicationException(string.Format("Daftar Risk Registrasi pada master data tidak ditemukan."));
            }
            return undiversifiedAggregationRisk;
        }

        public IntraProjectDiversifiedAggregationRisk GetIntraProjectDiversifiedAggregationOfRisk(IList<RiskRegistrasi> risk, IList<int> generateYear, IList<UndiversifiedRiskCapitalProjectCollection> diversified)
        {
            IntraProjectDiversifiedAggregationRisk intraProjectDiversifiedAggregationRisk = new IntraProjectDiversifiedAggregationRisk();
            IList<RiskYearCollection> riskYearCollections = new List<RiskYearCollection>();
            if (risk.Count > 0)
            {
                foreach (var itemYear in generateYear)
                {
                    IList<RiskYearValue> riskYearValues = new List<RiskYearValue>();
                    foreach (var itemRisk in risk)
                    {
                        decimal? valueDiversified = 0;
                        foreach (var itemProject in diversified)
                        {
                            foreach (var itemProjectYear in itemProject.DiversifiedRiskCapitalCollection)
                            {

                                if (itemProjectYear.Year == itemYear)
                                {
                                    foreach (var itemProjectRisk in itemProjectYear.RiskValueCollection)
                                    {
                                        if (itemProjectRisk.RiskRegistrasiId == itemRisk.Id)
                                        {
                                            valueDiversified += itemProjectRisk.Value;
                                        }
                                    }
                                }
                            }
                        }
                        RiskYearValue riskYearValue = new RiskYearValue();
                        riskYearValue.RiskRegistrasiId = itemRisk.Id;
                        riskYearValue.Value = valueDiversified.Value;
                        riskYearValues.Add(riskYearValue);
                    }
                    var totalYearValue = riskYearValues.Sum(x => x.Value);
                    RiskYearCollection riskYearCollection = new RiskYearCollection();
                    riskYearCollection.Year = itemYear;
                    riskYearCollection.RiskYearValue = riskYearValues.ToArray();
                    riskYearCollection.Total = totalYearValue.Value;
                    riskYearCollections.Add(riskYearCollection);
                }
                intraProjectDiversifiedAggregationRisk.RiskYearCollection = riskYearCollections.ToArray();
            }
            else
            {
                throw new ApplicationException(string.Format("Daftar Risk Registrasi pada master data tidak ditemukan."));
            }
            return intraProjectDiversifiedAggregationRisk;
        }

        public InterProjectDiversifiedAggregationRisk GetInterProjectDiversifiedAggregationOfRisk(IList<RiskRegistrasi> risk, IList<int> generateYear, IList<RiskYearCollection> intraDiversifiedRisk, IList<AggregationIntraDiversifiedProjectCapital> intraProjectDiversified, IList<AggregationInterDiversifiedProjectCapital> interProjectDiversified)
        {
            InterProjectDiversifiedAggregationRisk interProjectDiversifiedAggregationRisk = new InterProjectDiversifiedAggregationRisk();
            IList<RiskYearCollection> riskYearCollections = new List<RiskYearCollection>();

            if (risk.Count > 0)
            {
                foreach (var itemYear in generateYear)
                {
                    IList<RiskYearValue> riskYearValues = new List<RiskYearValue>();
                    foreach (var itemRisk in risk)
                    {
                        decimal? valueRisk = 0;
                        foreach (var itemYearIntra in intraDiversifiedRisk)
                        {
                            if (itemYear == itemYearIntra.Year)
                            {
                                foreach (var itemRiskIntra in itemYearIntra.RiskYearValue)
                                {
                                    if (itemRisk.Id == itemRiskIntra.RiskRegistrasiId)
                                    {
                                        foreach (var itemProjectInter in interProjectDiversified)
                                        {
                                            if (itemYear == itemProjectInter.aggregationYears)
                                            {
                                                foreach (var itemProjectIntra in intraProjectDiversified)
                                                {
                                                    if (itemYear == itemProjectIntra.aggregationYears && itemProjectIntra.TotalPerYear != 0)
                                                    {
                                                        valueRisk = itemRiskIntra.Value * itemProjectInter.TotalPerYear / itemProjectIntra.TotalPerYear;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        RiskYearValue riskYearValue = new RiskYearValue();
                        riskYearValue.RiskRegistrasiId = itemRisk.Id;
                        riskYearValue.Value = valueRisk.Value;
                        riskYearValues.Add(riskYearValue);
                    }
                    var totalYearValue = riskYearValues.Sum(x => x.Value);
                    RiskYearCollection riskYearCollection = new RiskYearCollection();
                    riskYearCollection.Year = itemYear;
                    riskYearCollection.Total = totalYearValue.Value;
                    riskYearCollection.RiskYearValue = riskYearValues.ToArray();
                    riskYearCollections.Add(riskYearCollection);
                }
                interProjectDiversifiedAggregationRisk.RiskYearCollection = riskYearCollections.ToArray();
            }
            else
            {
                throw new ApplicationException(string.Format("Daftar Risk Registrasi pada master data tidak ditemukan."));
            }
            return interProjectDiversifiedAggregationRisk;
        }

        #endregion 4. AggregationOfRisk

        #region Add Available Capital
        public AvailableCapitalProjected Get(int id)
        {
            return _availableCapitalProjectedRepository.Get(id);
        }
        public AvailableCapitalProjected GetByScenarioIdYear(int scenarioId, int year, string source)
        {
            return _availableCapitalProjectedRepository.GetByScenarioIdYear(scenarioId, year, source);
        }
        public int AddAvailableCapitalProjected(AvailableCapitalProjectedParam param)
        {
            string source;
            var scenario = _scenarioRepository.GetDefault();

            if (param.ListChanged.DataCapital != null)
            {
                foreach (var item in param.ListChanged.DataCapital)
                {
                    source = "Available Capital";
                    var isExist = _availableCapitalProjectedRepository.GetByScenarioIdYear(scenario.Id, item.Year, source);
                    if (isExist == null)
                    {
                        AvailableCapitalProjected model = new AvailableCapitalProjected(scenario, item.Year, item.Value, source);
                        _availableCapitalProjectedRepository.Insert(model);
                        _iDatabaseContext.SaveChanges();
                    }
                    else
                    {
                        var model = this.Get(isExist.Id);
                        model.Update(item.Value);
                        _availableCapitalProjectedRepository.Update(model);
                        _iDatabaseContext.SaveChanges();
                    }
                }
            }

            if (param.ListChanged.DataOpex != null)
            {
                foreach (var item in param.ListChanged.DataOpex)
                {
                    source = "Opex";
                    var isExist = _availableCapitalProjectedRepository.GetByScenarioIdYear(scenario.Id, item.Year, source);
                    if (isExist == null)
                    {
                        AvailableCapitalProjected model = new AvailableCapitalProjected(scenario, item.Year, item.Value, source);
                        _availableCapitalProjectedRepository.Insert(model);
                        _iDatabaseContext.SaveChanges();
                    }
                    else
                    {
                        var model = this.Get(isExist.Id);
                        model.Update(item.Value);
                        _availableCapitalProjectedRepository.Update(model);
                        _iDatabaseContext.SaveChanges();
                    }
                }
            }
            return 0;
        }
        #endregion Add Available Capital

        #region 5. AssetProjection
        public AssetProjectionLiquidity GenerateAssetProjectionInvestmentStrategy(IList<AggregationInterDiversifiedProjectCapital> interProjectDiversified, IList<int> years, Scenario scenario, AggregationOfProject aggregationOfProject)
        {
            AssetProjectionLiquidity assetProjectionLiquidity = new AssetProjectionLiquidity();

            #region AssetClassDefault
            var PMNCurrentYear = (_pmnRepository.GetAll().ToList()).Where(x => x.PMNToModalDasarCap == 2017).FirstOrDefault();
            decimal valuePMNCurrent;
            if (PMNCurrentYear != null)
            {
                valuePMNCurrent = PMNCurrentYear.ValuePMNToModalDasarCap;
            }
            else
            {
                throw new ApplicationException(string.Format("Tahun PMN untuk modal dasar cap {0} pada master data tidak ditemukan.", DateTime.Now.Year));
            }

            IList<AssetClassDefault> assetClassDefaults = new List<AssetClassDefault>();

            var asset = _assetDataRepository.GetAll();
            if (asset.Count() > 0)
            {
                foreach (var item in asset)
                {
                    AssetClassDefault assetClassDefault = new AssetClassDefault();
                    assetClassDefault.AssetClass = item.AssetClass;
                    assetClassDefault.Proportion = item.Porpotion;
                    assetClassDefault.TermAwal = item.TermAwal;
                    assetClassDefault.TermAkhir = item.TermAkhir;
                    assetClassDefault.AssumedReturn = item.AssumedReturn;
                    assetClassDefault.OutstandingYearAwal = item.OutstandingStartYears;
                    assetClassDefault.OutstandingYearAkhir = item.OutstandingEndYears;
                    //calculate AssetValue
                    assetClassDefault.AssetValue = item.AssetValue + valuePMNCurrent * item.Porpotion / 100;
                    assetClassDefaults.Add(assetClassDefault);
                }
            }
            assetProjectionLiquidity.AssetClassDefault = assetClassDefaults.ToArray();
            #endregion AssetClassDefault

            #region AssetSummaryDefault
            AssetSummaryDefault assetSummaryDefault = new AssetSummaryDefault();
            assetSummaryDefault.AvailableCapitalProjected = assetClassDefaults.Sum(x => x.AssetValue);
            assetSummaryDefault.OperatingExpenses = PMNCurrentYear.OpexGrowth;
            assetSummaryDefault.AvailableCapitalProjectedBeforeClaim = assetClassDefaults.Sum(x => x.AssetValue);
            assetProjectionLiquidity.AssetSummaryDefault = assetSummaryDefault;
            #endregion AssetSummaryDefault


            IList<AssetSummaryCollection> assetSummaryCollections = new List<AssetSummaryCollection>();
            IList<AssetProjectionIlustration> assetProjectionIlustrations = new List<AssetProjectionIlustration>();
            IList<AssetClassCollection> assetClassProjectionCollections = new List<AssetClassCollection>();
            IList<AssetClassCollection> assetClassLiquidityCollections = new List<AssetClassCollection>();
            IList<YearList> liquidAssets = new List<YearList>();
            int currentYear = 2017;
            foreach (var year in years)
            {
                #region AssetSummaryCollection & Ilustration
                AssetSummaryCollection assetSummaryCollection = new AssetSummaryCollection();
                AssetProjectionIlustration assetProjectionIlustration = new AssetProjectionIlustration();
                assetSummaryCollection.Year = year;
                assetProjectionIlustration.Year = year;
                // Total Undiversified Capital
                var totalUndiversifiedCapital = aggregationOfProject.AggregationUndiversifiedProjectCapital.Where(x => x.aggregationYears == year).FirstOrDefault();
                assetSummaryCollection.TotalUndiversifiedCapital = totalUndiversifiedCapital.TotalPerYear.Value;
                // Total Diversified Capital
                var totalDiversifiedCapital = (aggregationOfProject.AggregationInterDiversifiedProjectCapital.Where(x => x.aggregationYears == year).FirstOrDefault()).TotalPerYear.Value;
                assetSummaryCollection.TotalDiversifiedCapital = totalDiversifiedCapital;

                decimal operatingExpenses = 0;
                decimal availableCapitalProjectedBeforeOpex = 0;
                bool isEditedOpex = false;

                if (year == currentYear)
                {
                    // Available capital Projected before opex
                    foreach (var item in assetClassDefaults)
                    {
                        var value = decimal.Round(item.AssetValue, 28) * decimal.Round((1 + item.AssumedReturn / 100), 28);
                        availableCapitalProjectedBeforeOpex += value;
                    }



                    var editOpex = _availableCapitalProjectedRepository.GetByScenarioIdYear(scenario.Id, year, "Opex");
                    if (editOpex != null && editOpex.Value != null)
                    {
                        operatingExpenses = editOpex.Value.Value;
                        isEditedOpex = true;
                    }
                    else
                    {
                        // Operating expenses
                        operatingExpenses = PMNCurrentYear.Opex;
                    }                    
                }
                else
                {
                    // Available capital Projected before opex
                    var itemACC = assetClassProjectionCollections.Where(x => x.Year == year - 1).FirstOrDefault();
                    foreach (var item in itemACC.AssetList)
                    {
                        foreach (var itemACD in assetClassDefaults)
                        {
                            if (itemACD.AssetClass == item.AssetClass)
                            {
                                var value = decimal.Round(item.Value, 28) * decimal.Round((1 + itemACD.AssumedReturn / 100), 28);
                                availableCapitalProjectedBeforeOpex += value;
                            }

                        }
                    }
                    // Operating expenses
                    var itemASC = assetSummaryCollections.Where(x => x.Year == year - 1).FirstOrDefault();

                    var editOpex = _availableCapitalProjectedRepository.GetByScenarioIdYear(scenario.Id, year, "Opex");
                    if (editOpex != null && editOpex.Value != null)
                    {
                        operatingExpenses = editOpex.Value.Value;
                        isEditedOpex = true;
                    }
                    else
                    {
                        operatingExpenses = decimal.Round(itemASC.OperatingExpenses, 28) * decimal.Round((assetSummaryDefault.OperatingExpenses / 100 + 1), 28);
                    }
                    
                    assetSummaryCollection.OperatingExpenses = operatingExpenses;
                }
                assetSummaryCollection.AvailableCapitalProjectedBeforeOpex = availableCapitalProjectedBeforeOpex;
                assetSummaryCollection.OperatingExpenses = operatingExpenses;
                assetSummaryCollection.isEditedOpex = isEditedOpex;

                //Available capital Projected before claim
                var availableCapitalProjectedBeforeClaim = availableCapitalProjectedBeforeOpex - operatingExpenses;
                assetSummaryCollection.AvailableCapitalProjectedBeforeClaim = availableCapitalProjectedBeforeClaim;

                // Ilustration (RecoursePayment)
                decimal recoursePayment = 0;
                var itemRecouseDelay = aggregationOfProject.AggregationInterDiversifiedProjectCapital.Where(x => x.aggregationYears == year - (int)PMNCurrentYear.RecourseDelay).FirstOrDefault();
                if (itemRecouseDelay != null)
                {
                    recoursePayment = itemRecouseDelay.TotalPerYear.Value;
                    assetProjectionIlustration.RecoursePayment = recoursePayment;
                }
                else
                {
                    assetProjectionIlustration.RecoursePayment = recoursePayment;
                }

                // Ilustration (Available capital after Recourse)
                var edit = _availableCapitalProjectedRepository.GetByScenarioIdYear(scenario.Id, year, "Available Capital");
                decimal availableCapitalAfterRecourse = 0;
                bool isEditedAvailableCapital = false;
                if (edit != null && edit.Value != null)
                {
                    availableCapitalAfterRecourse = edit.Value.Value;
                    isEditedAvailableCapital = true;
                }
                else
                {
                    availableCapitalAfterRecourse = availableCapitalProjectedBeforeClaim - totalDiversifiedCapital + recoursePayment;
                }
                assetProjectionIlustration.isEditedAvailableCapital = isEditedAvailableCapital;
                assetProjectionIlustration.AvailableCapitalRecourse = availableCapitalAfterRecourse;

                // Available capital Projected
                assetSummaryCollection.AvailableCapitalProjected = availableCapitalAfterRecourse;
                assetSummaryCollection.isEditedAvailableCapital = isEditedAvailableCapital;

                assetProjectionIlustrations.Add(assetProjectionIlustration);
                assetSummaryCollections.Add(assetSummaryCollection);
                #endregion AssetSummaryCollection

                #region AssetClassProjectionCollection
                AssetClassCollection assetClassCollection = new AssetClassCollection();
                assetClassCollection.Year = year;

                IList<AssetList> assetLists = new List<AssetList>();

                var PMNNextYear = (_pmnRepository.GetAll().ToList()).Where(x => x.PMNToModalDasarCap == year + 1).FirstOrDefault();
                if (year == currentYear)
                {
                    foreach (var item in assetClassDefaults)
                    {
                        AssetList assetList = new AssetList();
                        assetList.AssetClass = item.AssetClass;
                        assetList.Value = (availableCapitalAfterRecourse + PMNNextYear.ValuePMNToModalDasarCap) * item.Proportion / 100;

                        assetLists.Add(assetList);
                    }
                }

                else
                {
                    foreach (var item in assetClassDefaults)
                    {
                        AssetList assetList = new AssetList();
                        assetList.AssetClass = item.AssetClass;
                        assetList.Value = decimal.Round(availableCapitalAfterRecourse, 28) * decimal.Round(item.Proportion / 100, 28);

                        assetLists.Add(assetList);
                    }
                }
                assetClassCollection.AssetList = assetLists.ToArray();
                assetClassProjectionCollections.Add(assetClassCollection);
                #endregion AssetClassProjectionCollection

                #region Liquidity (Collection)
                AssetClassCollection assetClassLiquidityCollection = new AssetClassCollection();
                assetClassLiquidityCollection.Year = year;
                IList<AssetList> assetLiquidityLists = new List<AssetList>();
                if (year == currentYear)
                {
                    foreach (var itemACPC in assetLists)
                    {
                        foreach (var item in assetClassDefaults)
                        {
                            if (itemACPC.AssetClass == item.AssetClass && item.AssetClass == "Cash - Deposit")
                            {
                                AssetList assetList = new AssetList();
                                assetList.AssetClass = item.AssetClass;
                                assetList.Value = itemACPC.Value;
                                assetLiquidityLists.Add(assetList);
                            }
                            else if (itemACPC.AssetClass == item.AssetClass)
                            {
                                AssetList assetList = new AssetList();
                                assetList.AssetClass = item.AssetClass;
                                assetList.Value = itemACPC.Value - item.AssetValue;
                                assetLiquidityLists.Add(assetList);
                            }
                        }
                    }
                }
                else
                {
                    var itemBefore = assetClassProjectionCollections.Where(x => x.Year == year - 1).FirstOrDefault();
                    var itemCurrent = assetClassProjectionCollections.Where(x => x.Year == year).FirstOrDefault();
                    foreach (var itemBfr in itemBefore.AssetList)
                    {
                        foreach (var itemCrn in itemCurrent.AssetList)
                        {
                            if (itemBfr.AssetClass == itemCrn.AssetClass && itemCrn.AssetClass == "Cash - Deposit")
                            {
                                AssetList assetList = new AssetList();
                                assetList.AssetClass = itemBfr.AssetClass;
                                assetList.Value = itemCrn.Value;
                                assetLiquidityLists.Add(assetList);
                            }
                            else if (itemBfr.AssetClass == itemCrn.AssetClass)
                            {
                                AssetList assetList = new AssetList();
                                assetList.AssetClass = itemBfr.AssetClass;
                                assetList.Value = itemCrn.Value - itemBfr.Value;
                                assetLiquidityLists.Add(assetList);
                            }
                        }
                    }
                }
                assetClassLiquidityCollection.AssetList = assetLiquidityLists.ToArray();
                assetClassLiquidityCollections.Add(assetClassLiquidityCollection);
                #endregion Liquidity (Collection)

                #region Liquid Asset
                YearList liquidAsset = new YearList();
                liquidAsset.Year = year;
                liquidAsset.Value = assetLiquidityLists.Sum(x => x.Value);
                liquidAssets.Add(liquidAsset);
                #endregion Liquid Asset
            }
            assetProjectionLiquidity.AssetSummaryCollection = assetSummaryCollections.ToArray();
            assetProjectionLiquidity.AssetClassProjectionCollection = assetClassProjectionCollections.ToArray();
            assetProjectionLiquidity.AssetClassLiquidityCollection = assetClassLiquidityCollections.ToArray();
            assetProjectionLiquidity.AssetProjectionIlustration = assetProjectionIlustrations.ToArray();
            assetProjectionLiquidity.LiquidAsset = liquidAssets.ToArray();

            return assetProjectionLiquidity;
        }
        #endregion 5. AssetProjection

        #region 6. ScenarioTesting
        public IList<ScenarioTestingCollection> GetScenarioTestingCollection(int[] year, IList<CalculationResult> calculationResult)
        {

            IList<ScenarioTestingCollection> scenarioTestingCollections = new List<ScenarioTestingCollection>();
            if (calculationResult != null && year != null)
            {
                foreach (var calculationCollection in calculationResult)
                {
                    ScenarioTestingCollection scenarioTestingCollection = new ScenarioTestingCollection();
                    scenarioTestingCollection.ScenarioId = calculationCollection.ScenarioId;
                    scenarioTestingCollection.NamaScenario = calculationCollection.NamaScenario;

                    IList<ScenarioTestingYearCollection> scenarioTestingYearCollections = new List<ScenarioTestingYearCollection>();
                    foreach (var tahun in year)
                    {
                        ScenarioTestingYearCollection scenarioTestingYearCollection = new ScenarioTestingYearCollection();
                        scenarioTestingYearCollection.Year = tahun;
                        scenarioTestingYearCollection.TotalUndiversifiedCapital = 0;
                        scenarioTestingYearCollection.TotalDiversifiedCapital = 0;
                        foreach (var aggregationUndiv in calculationCollection.AggregationOfProject.AggregationUndiversifiedProjectCapital)
                        {
                            if (aggregationUndiv.aggregationYears == tahun)
                            {
                                scenarioTestingYearCollection.TotalUndiversifiedCapital = aggregationUndiv.TotalPerYear;
                            }
                        }
                        foreach (var aggregationDiv in calculationCollection.AggregationOfProject.AggregationInterDiversifiedProjectCapital)
                        {
                            if (aggregationDiv.aggregationYears == tahun)
                            {
                                scenarioTestingYearCollection.TotalDiversifiedCapital = aggregationDiv.TotalPerYear;
                            }
                        }
                        scenarioTestingYearCollections.Add(scenarioTestingYearCollection);
                    }
                    scenarioTestingCollection.ScenarioTestingYearCollection = scenarioTestingYearCollections.ToArray();
                    scenarioTestingCollections.Add(scenarioTestingCollection);
                }
            }
            return scenarioTestingCollections;
        }

        public Sensitivity GetSensitivity(int[] year, IList<CalculationResult> calculationResult)
        {
            Sensitivity sensitivity = new Sensitivity();
            IList<Undiversified> undiversifieds = new List<Undiversified>();
            IList<Diversified> diversifieds = new List<Diversified>();
            if (calculationResult != null && year != null)
            {
                foreach (var scenario in calculationResult)
                {
                    Undiversified undiversified = new Undiversified();
                    Diversified diversified = new Diversified();
                    undiversified.NamaScenario = scenario.NamaScenario;
                    undiversified.ScenarioId = scenario.ScenarioId;
                    diversified.NamaScenario = scenario.NamaScenario;
                    diversified.ScenarioId = scenario.ScenarioId;
                    IList<ValuePerYear> lowerValueUndivs = new List<ValuePerYear>();
                    IList<ValuePerYear> midValueUndivs = new List<ValuePerYear>();
                    IList<ValuePerYear> upperValueUndivs = new List<ValuePerYear>();
                    IList<ValuePerYear> lowerValueDivs = new List<ValuePerYear>();
                    IList<ValuePerYear> midValueDivs = new List<ValuePerYear>();
                    IList<ValuePerYear> upperValueDivs = new List<ValuePerYear>();

                    foreach (var tahun in year)
                    {
                        foreach (var undiv in scenario.AggregationOfProject.AggregationUndiversifiedProjectCapital)
                        {
                            ValuePerYear lowerValueUndiv = new ValuePerYear();
                            ValuePerYear midValueUndiv = new ValuePerYear();
                            ValuePerYear upperValueUndiv = new ValuePerYear();
                            if (undiv.aggregationYears == tahun)
                            {
                                lowerValueUndiv.Value = undiv.TotalPerYearLower;
                                midValueUndiv.Value = undiv.TotalPerYear;
                                upperValueUndiv.Value = undiv.TotalPerYearUpper;
                                lowerValueUndiv.Year = tahun;
                                midValueUndiv.Year = tahun;
                                upperValueUndiv.Year = tahun;
                                lowerValueUndivs.Add(lowerValueUndiv);
                                midValueUndivs.Add(midValueUndiv);
                                upperValueUndivs.Add(upperValueUndiv);
                            }
                        }
                        foreach (var div in scenario.AggregationOfProject.AggregationInterDiversifiedProjectCapital)
                        {
                            ValuePerYear lowerValueDiv = new ValuePerYear();
                            ValuePerYear midValueDiv = new ValuePerYear();
                            ValuePerYear upperValueDiv = new ValuePerYear();

                            if (div.aggregationYears == tahun)
                            {
                                lowerValueDiv.Value = div.TotalPerYearLower;
                                midValueDiv.Value = div.TotalPerYear;
                                upperValueDiv.Value = div.TotalPerYearUpper;
                                lowerValueDiv.Year = tahun;
                                midValueDiv.Year = tahun;
                                upperValueDiv.Year = tahun;
                                lowerValueDivs.Add(lowerValueDiv);
                                midValueDivs.Add(midValueDiv);
                                upperValueDivs.Add(upperValueDiv);
                            }
                        }
                    }

                    undiversified.LowerValue = lowerValueUndivs.ToArray();
                    undiversified.MidValue = midValueUndivs.ToArray();
                    undiversified.UpperValue = upperValueUndivs.ToArray();
                    diversified.LowerValue = lowerValueDivs.ToArray();
                    diversified.MidValue = midValueDivs.ToArray();
                    diversified.UpperValue = upperValueDivs.ToArray();
                    undiversifieds.Add(undiversified);
                    diversifieds.Add(diversified);
                }
            }
            sensitivity.Undiversified = undiversifieds.ToArray();
            sensitivity.Diversified = diversifieds.ToArray();

            return sensitivity;
        }

        public StressTesting GetStressTesting(int[] year, IList<CalculationResult> calculationResult)
        {
            StressTesting stressTesting = new StressTesting();

            IList<RiskCapital> riskCapitals = new List<RiskCapital>();
            IList<AvailableCapital> availableCapitals = new List<AvailableCapital>();
            IList<TotalLiquidity> totalLiquiditys = new List<TotalLiquidity>();

            if (calculationResult != null && year != null)
            {
                foreach (var scenario in calculationResult)
                {
                    RiskCapital riskCapital = new RiskCapital();
                    riskCapital.ScenarioId = scenario.ScenarioId;
                    riskCapital.NamaScenario = scenario.NamaScenario;

                    IList<ValuePerYear> valuePerYears = new List<ValuePerYear>();
                    foreach (var tahun in year)
                    {
                        ValuePerYear valuePerYear = new ValuePerYear();
                        valuePerYear.Year = tahun;
                        valuePerYear.Value = 0;

                        foreach (var diversifiedRiskCapital in scenario.AggregationOfProject.AggregationInterDiversifiedProjectCapital)
                        {
                            if (diversifiedRiskCapital.aggregationYears == tahun)
                            {
                                valuePerYear.Value = diversifiedRiskCapital.TotalPerYear;
                            }
                        }
                        valuePerYears.Add(valuePerYear);
                    }
                    riskCapital.ValuePerYear = valuePerYears.ToArray();
                    riskCapitals.Add(riskCapital);
                }

                foreach (var scenario in calculationResult)
                {
                    AvailableCapital availableCapital = new AvailableCapital();
                    TotalLiquidity totalLiquidity = new TotalLiquidity();
                    availableCapital.ScenarioId = scenario.ScenarioId;
                    availableCapital.NamaScenario = scenario.NamaScenario;
                    totalLiquidity.ScenarioId = scenario.ScenarioId;
                    totalLiquidity.NamaScenario = scenario.NamaScenario;

                    IList<ValuePerYear> availableCapitalValues = new List<ValuePerYear>();
                    IList<ValuePerYear> totalLiquidityValues = new List<ValuePerYear>();
                    foreach (var tahun in year)
                    {
                        ValuePerYear availableCapitalValue = new ValuePerYear();
                        ValuePerYear totalLiquidityValue = new ValuePerYear();
                        availableCapitalValue.Year = tahun;
                        availableCapitalValue.Value = 0;
                        totalLiquidityValue.Year = tahun;
                        totalLiquidityValue.Value = 0;

                        foreach (var availableCapitalProjected in scenario.AssetProjectionLiquidity.AssetSummaryCollection)
                        {
                            if (availableCapitalProjected.Year == tahun)
                            {
                                availableCapitalValue.Value = availableCapitalProjected.AvailableCapitalProjected;
                            }
                        }
                        foreach (var totalYearLiquidity in scenario.AssetProjectionLiquidity.LiquidAsset)
                        {
                            if (totalYearLiquidity.Year == tahun)
                            {
                                totalLiquidityValue.Value = totalYearLiquidity.Value;
                            }
                        }
                        availableCapitalValues.Add(availableCapitalValue);
                        totalLiquidityValues.Add(totalLiquidityValue);
                    }
                    availableCapital.ValuePerYear = availableCapitalValues.ToArray();
                    availableCapitals.Add(availableCapital);
                    totalLiquidity.ValuePerYear = totalLiquidityValues.ToArray();
                    totalLiquiditys.Add(totalLiquidity);
                }

            }
            stressTesting.RiskCapital = riskCapitals.ToArray();
            stressTesting.AvailableCapital = availableCapitals.ToArray();
            stressTesting.TotalLiquidity = totalLiquiditys.ToArray();
            return stressTesting;
        }

        #endregion 6. ScenarioTesting
    }
}
