﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;
using HR.Infrastructure;

namespace HR.Application
{
    public class CorrelatedSektorDetailService : ICorrelatedSektorDetailService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICorrelatedSektorDetailRepository _correlatedSektorDetailRepository;
        private readonly ICorrelatedSektorRepository _correlatedSektorRepository;
        private readonly IRiskRegistrasiRepository _riskRegistrasiRepository;
        private readonly ICorrelationMatrixRepository _correlationMatrixRepository;
        private readonly IAuditLogService _auditLogService;
        private readonly IUserRepository _userRepository;
        private readonly IStatusRepository _statusRepository;
        private readonly IApprovalRepository _approvalRepository;
        private readonly IMasterApprovalCorrelatedSektorRepository _masterApprovalCorrelatedSektorRepository;
        private readonly IScenarioRepository _scenarioRepository;
        private readonly IEmailService _emailService;
        private readonly IUserService _userService;

        private readonly IDatabaseContext _databaseContext;

        public CorrelatedSektorDetailService(IUnitOfWork unitOfWork, ICorrelatedSektorDetailRepository correlatedSektorDetailRepository, ICorrelatedSektorRepository correlatedSektorRepository, 
            IRiskRegistrasiRepository riskRegistrasiRepository, ICorrelationMatrixRepository correlationMatrixRepository, 
            IAuditLogService auditLogService, IUserRepository userRepository, IStatusRepository statusRepository, IApprovalRepository approvalRepository
            , IMasterApprovalCorrelatedSektorRepository masterApprovalCorrelatedSektorRepository, IDatabaseContext databaseContext, IScenarioRepository scenarioRepository,
            IEmailService emailService, IUserService userService)
        {
            _unitOfWork = unitOfWork;
            _correlatedSektorDetailRepository = correlatedSektorDetailRepository;
            _correlatedSektorRepository = correlatedSektorRepository;
            _riskRegistrasiRepository = riskRegistrasiRepository;
            _correlationMatrixRepository = correlationMatrixRepository;
            _auditLogService = auditLogService;
            _userRepository = userRepository;
            _statusRepository = statusRepository;
            _approvalRepository = approvalRepository;
            _masterApprovalCorrelatedSektorRepository = masterApprovalCorrelatedSektorRepository;
            _databaseContext = databaseContext;
            _scenarioRepository = scenarioRepository;
            _emailService = emailService;
            _userService = userService;
        }

        public CorrelatedSektorDetailCollectionParam GetByCorrelatedSektorId(int correlatedSektorId)
        {
            CorrelatedSektorDetailCollectionParam param = new CorrelatedSektorDetailCollectionParam();
            IList<CorrelatedSektorDetailCollection> dataCollection = new List<CorrelatedSektorDetailCollection>();

            var data = _correlatedSektorDetailRepository.GetByCorrelatedSektorId(correlatedSektorId).ToList();
            if(data.Count > 0)
            {
                var riskRegistrasi = _riskRegistrasiRepository.GetAll().ToList();

                foreach (var itemRisk in riskRegistrasi)
                {
                    CorrelatedSektorDetailCollection detail = new CorrelatedSektorDetailCollection();
                    IList<RiskRegistrasiValues> detailList = new List<RiskRegistrasiValues>();

                    var dataDetail = data.Where(x => x.RiskRegistrasiIdRow == itemRisk.Id).ToList();

                    foreach (var item in dataDetail)
                    {
                        RiskRegistrasiValues detailValue = new RiskRegistrasiValues();
                        detailValue.RiskRegistrasiIdRow = item.RiskRegistrasiIdRow;
                        detailValue.RiskRegistrasiIdCol = item.RiskRegistrasiIdCol;
                        detailValue.CorrelationMatrixId = item.CorrelationMatrixId;

                        detailList.Add(detailValue);
                    }
                    detail.RiskRegistrasiId = itemRisk.Id;
                    detail.RiskRegistrasiValues = detailList.ToArray();

                    dataCollection.Add(detail);
                }

                param.CorrelatedSektorId = data[0].CorrelatedSektorId;
                param.CorrelatedSektorDetailCollection = dataCollection.ToArray();
                param.CreateBy = data[0].CreateBy;
                param.CreateDate = data[0].CreateDate;
                param.UpdateBy = data[0].UpdateBy;
                param.UpdateDate = data[0].UpdateDate;
                param.IsDelete = data[0].IsDelete;
            }

            return param;
        }

        public void IsExistOnAdding(int correlataedSektor, int riskRegistrasiIdRow, int riskRegistrasiIdCol)
        {

        }

        public int Add(CorrelatedSektorDetailCollectionParam param)
        {
            int id = 0;
            CorrelatedSektor correlatedSektor = _correlatedSektorRepository.Get(param.CorrelatedSektorId);
            Validate.NotNull(correlatedSektor, "Correlated Sektor tidak ditemukan.");

            //variabel untuk email
            int emailDari = 0;
            int emailKepada = 0;
            List<int> emailCC = new List<int>();
            string source = "CorrelatedSektor";
            string typePesan = "";
            Status statusEmail = null;
            string templateNotif;
            int requestId = 0;
            string keterangan = "";
            var userCreateNew = _userService.Get(param.CreateBy.GetValueOrDefault());
            if (correlatedSektor.StatusId == 2)
            {
                var PerluExistingName = _correlatedSektorRepository.GetByScenarioIdSektorIdList2(correlatedSektor.ScenarioId, correlatedSektor.SektorId, param.CreateBy.GetValueOrDefault()).Where(x => x.StatusId != 2);
                int ValidateName = PerluExistingName.Count();
                if (ValidateName >= 1)
                {
                    string dodol = null;
                    Validate.NotNull(dodol, "User tidak boleh mempunyai 2 draft dengan Korelasi Kategori Risiko yang sama.");
                    return 0;
                }
                else
                {
                    var checkMasterCorrelatedSektor = _masterApprovalCorrelatedSektorRepository.GetAll();
                    var checkoutMaster = checkMasterCorrelatedSektor.Count();
                    if (checkoutMaster == 0)
                    {
                        Validate.NotNull(null, "User harus mengatur Approval untuk Korelasi Kategori Risiko terlebih dahulu.");
                    }
                    int a = 0;
                    foreach (var riskItem in param.CorrelatedSektorDetailCollection)
                    {
                        RiskRegistrasi riskRegistrasi = _riskRegistrasiRepository.Get(riskItem.RiskRegistrasiId);
                        Validate.NotNull(riskRegistrasi, "Risk Kategori tidak ditemukan.");

                        foreach (var item in riskItem.RiskRegistrasiValues)
                        {
                            CorrelationMatrix correlationMatrix = _correlationMatrixRepository.Get(item.CorrelationMatrixId);
                            RiskRegistrasi riskRegistrasiRow = _riskRegistrasiRepository.Get(item.RiskRegistrasiIdRow);
                            RiskRegistrasi riskRegistrasiCol = _riskRegistrasiRepository.Get(item.RiskRegistrasiIdCol);
                            var isExist = _correlatedSektorDetailRepository.IsExisitOnAdding(correlatedSektor.Id, riskRegistrasiRow.Id, riskRegistrasiCol.Id);

                            if (isExist == null)
                            {
                                CorrelatedSektorDetail model = new CorrelatedSektorDetail(correlatedSektor, riskRegistrasiRow.Id, riskRegistrasiCol.Id, correlationMatrix, param.CreateBy, param.CreateDate);
                                _correlatedSektorDetailRepository.Insert(model);
                                a++;
                            }
                            else
                            {
                                int audit = _auditLogService.CorrelationSektorDetailAudit(isExist, correlationMatrix, Convert.ToInt32(param.CreateBy));
                                isExist.Update(correlationMatrix, param.CreateBy, param.CreateDate);
                                _correlatedSektorDetailRepository.Update(isExist);
                            }
                        }
                    }

                    if (a > 0)
                    {
                        int audit = _auditLogService.AddCorrelationSektorDetailAudit(correlatedSektor.Id, Convert.ToInt32(param.CreateBy));
                    }   

                    int nomorUrutStatus = 0;
                    int dodol = Convert.ToInt32(param.CreateBy);
                    var masterApproval = _masterApprovalCorrelatedSektorRepository.GetByUserId(dodol);

                    if (masterApproval != null)
                    {
                        nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
                    }
                    if (masterApproval == null && (userCreateNew.RoleId == 3 || userCreateNew.RoleId == 4 || userCreateNew.RoleId == 5))
                    {
                        masterApproval = _masterApprovalCorrelatedSektorRepository.GetByRoleId(userCreateNew.RoleId);
                        nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
                    }
                    if (nomorUrutStatus != 0)
                    {
                        param.IsUpdate = true;
                        var duplicate = Duplicate(param);

                        var status = _statusRepository.GetStatus(nomorUrutStatus);
                        if (param.IsSend == true && status.Id != 2)
                        {
                            param.IsDraftApproval = true;
                            int idDuplicate = Duplicate(param);

                            AddApproval(idDuplicate, status, param.CreateBy, param.CreateDate, nomorUrutStatus);
                            _databaseContext.SaveChanges();
                            #region email
                            if (param.IsSend == true && param.IsStatusApproval == false || param.IsStatusApproval == null)
                            {
                                if (nomorUrutStatus != 3)
                                {
                                    emailDari = param.CreateBy.GetValueOrDefault();

                                    var master = _masterApprovalCorrelatedSektorRepository.GetByNomorUrut(nomorUrutStatus + 1);
                                    Validate.NotNull(master, "Master Approval Correlated Sektor is not found.");
                                    emailKepada = master.UserId.GetValueOrDefault();

                                    typePesan = status.StatusDescription;

                                    requestId = correlatedSektor.Id;

                                    if (nomorUrutStatus < 2)
                                    {
                                        master = _masterApprovalCorrelatedSektorRepository.GetByNomorUrut(nomorUrutStatus + 2);
                                        Validate.NotNull(master, "Master Approval Correlated Sektor is not found.");

                                        emailCC.Add(master.UserId.GetValueOrDefault());

                                        //source
                                         _emailService.NotifyResetPassword(source, typePesan, requestId, emailKepada, emailCC, emailDari, param.TemplateNotif);

                                    }
                                    else
                                    {
                                        emailCC.Add(master.UserId.GetValueOrDefault());
                                        _emailService.NotifyResetPassword(source, typePesan, requestId, emailCC, emailDari, param.TemplateNotif);
                                    }

                                }
                            }
                            #endregion email
                        }

                        if (status.Id == 2)
                        {
                            if (param.IsSend == true)
                            {
                                //var deleteScenario = Delete2(id, Convert.ToInt32(param.UpdateBy), Convert.ToDateTime(param.UpdateDate));
                                var deleteCorrelatedSektor = Update2(duplicate, param);
                            }
                        }
                        return duplicate;
                    }
                    else
                    {
                        param.IsUpdate = true;
                        var duplicate = Duplicate(param);
                        var status = _statusRepository.GetStatus(nomorUrutStatus);
                        if (param.IsSend == true)
                        {
                            param.IsDraftApproval = true;
                            int idDuplicate = Duplicate(param);

                            AddApproval(idDuplicate, status, param.CreateBy, param.CreateDate, nomorUrutStatus);
                            _databaseContext.SaveChanges();
                            #region email
                            if (param.IsSend == true && param.IsStatusApproval == false || param.IsStatusApproval == null)
                            {
                                if (nomorUrutStatus != 3)
                                {
                                    emailDari = param.CreateBy.GetValueOrDefault();

                                    var master = _masterApprovalCorrelatedSektorRepository.GetByNomorUrut(nomorUrutStatus + 1);
                                    Validate.NotNull(master, "Master Approval Correlated Sektor is not found.");
                                    emailKepada = master.UserId.GetValueOrDefault();

                                    typePesan = status.StatusDescription;

                                    requestId = correlatedSektor.Id;

                                    if (nomorUrutStatus < 2)
                                    {
                                        master = _masterApprovalCorrelatedSektorRepository.GetByNomorUrut(nomorUrutStatus + 2);
                                        Validate.NotNull(master, "Master Approval Correlated Sektor is not found.");

                                        emailCC.Add(master.UserId.GetValueOrDefault());
                                        //source
                                         _emailService.NotifyResetPassword(source, typePesan, requestId, emailKepada, emailCC, emailDari, param.TemplateNotif);

                                    }
                                    else
                                    {
                                        emailCC.Add(master.UserId.GetValueOrDefault());
                                        _emailService.NotifyResetPassword(source, typePesan, requestId, emailCC, emailDari, param.TemplateNotif);
                                    }

                                }
                            }
                            #endregion email
                        }
                        return duplicate;
                    }
                }
            }
            else
            {
                #region Approval

                bool isSend;
                bool isStatusApproval;

                Status status = null;
                int nomorUrutStatus = 0;
                if (correlatedSektor.StatusId != 2)
                {
                    //Jika Correlated Sektor saat ini statusnya 1/submit, tidak bisa diedit
                    if (correlatedSektor.StatusId == 1 && param.IsStatusApproval == null)
                    {
                        throw new ApplicationException(string.Format("Correlated Sektor tidak bisa diedit karena dalam proses approval"));
                    }
                    //Jika CorrelatedSektor saat ini statusnya 3/reject, maka akan kirim kembali ke Approval 
                    //atau jika user menekan tombol kirim, maka akan kirim ke tabel Approval
                    else if ((correlatedSektor.StatusId == 3 && param.IsSend == true) && param.IsStatusApproval == null || param.IsSend == true && param.IsStatusApproval == null)
                    {
                        var checkMasterCorrelatedSektor = _masterApprovalCorrelatedSektorRepository.GetAll();
                        var checkoutMaster = checkMasterCorrelatedSektor.Count();
                        if (checkoutMaster == 0)
                        {
                            Validate.NotNull(null, "User must setting Approval for Correlation Sector first.");
                        }
                        var userCreate = _userRepository.Get(param.CreateBy.GetValueOrDefault());
                        Validate.NotNull(userCreate, "User is not found.");

                        //get role level from user id
                        var masterApproval = _masterApprovalCorrelatedSektorRepository.GetByUserId(userCreate.Id);
                        if (masterApproval != null)
                        {
                            nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
                        }
                        if (masterApproval == null && (userCreate.RoleId == 3 || userCreate.RoleId == 4 || userCreate.RoleId == 5))
                        {
                            masterApproval = _masterApprovalCorrelatedSektorRepository.GetByRoleId(userCreate.RoleId);
                            nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
                        }
                        status = _statusRepository.GetStatus(nomorUrutStatus);
                        Validate.NotNull(status, "Status is not found.");

                        //insert into approval
                        if (status != null && status.Id != 2)
                        {
                            param.IsUpdate = true;
                            param.IsDraftApproval = true;
                            int idDuplicate = Duplicate(param);

                            //insert into approval
                            AddApproval(idDuplicate, status, param.CreateBy, param.CreateDate, nomorUrutStatus);
                            _databaseContext.SaveChanges();
                        }
                        //_unitOfWork.Commit();
                        correlatedSektor.UpdateStatus(status, param.CreateBy, param.CreateDate);
                    }
                    //jika user menekan tombol kirim, maka akan kirim ke tabel Approval
                }
               
                emailKepada = correlatedSektor.UpdateBy.GetValueOrDefault();
                isStatusApproval = param.IsStatusApproval.GetValueOrDefault();
                param.IsStatusApproval = null;
                isSend = param.IsSend.GetValueOrDefault();
                param.IsSend = null;
                if (param.NomorUrutStatus.GetValueOrDefault() != 0)
                {
                    nomorUrutStatus = param.NomorUrutStatus.GetValueOrDefault();
                    param.NomorUrutStatus = null;
                }
                if (param.StatusId != null)
                {
                    statusEmail = _statusRepository.Get(param.StatusId.GetValueOrDefault());
                    param.StatusId = null;
                }
                templateNotif = param.TemplateNotif;
                param.TemplateNotif = null;
                param.IsUpdate = null;
                param.IsDraftApproval = null;
                keterangan = param.Keterangan;
                param.Keterangan = null;
                #endregion Approval

                using (_unitOfWork)
                {
                    var a = 0;
                    foreach (var riskItem in param.CorrelatedSektorDetailCollection)
                    {
                        RiskRegistrasi riskRegistrasi = _riskRegistrasiRepository.Get(riskItem.RiskRegistrasiId);
                        Validate.NotNull(riskRegistrasi, "Risk Kategori tidak ditemukan.");

                        foreach (var item in riskItem.RiskRegistrasiValues)
                        {
                            CorrelationMatrix correlationMatrix = _correlationMatrixRepository.Get(item.CorrelationMatrixId);
                            Validate.NotNull(correlationMatrix, "Correlation Matrix tidak ditemukan.");

                            RiskRegistrasi riskRegistrasiRow = _riskRegistrasiRepository.Get(item.RiskRegistrasiIdRow);
                            Validate.NotNull(riskRegistrasiRow, "Risk Kategori (row) tidak ditemukan.");

                            RiskRegistrasi riskRegistrasiCol = _riskRegistrasiRepository.Get(item.RiskRegistrasiIdCol);
                            Validate.NotNull(riskRegistrasiCol, "Risk Kategori (col) tidak ditemukan.");

                            var isExist = _correlatedSektorDetailRepository.IsExisitOnAdding(correlatedSektor.Id, riskRegistrasiRow.Id, riskRegistrasiCol.Id);

                            if (isExist == null)
                            {
                                CorrelatedSektorDetail model = new CorrelatedSektorDetail(correlatedSektor, riskRegistrasiRow.Id, riskRegistrasiCol.Id, correlationMatrix, param.CreateBy, param.CreateDate);
                                _correlatedSektorDetailRepository.Insert(model);
                                a++;
                            }
                            else
                            {
                                //Audit Log Update 
                                int audit = _auditLogService.CorrelationSektorDetailAudit(isExist, correlationMatrix, Convert.ToInt32(param.CreateBy));

                                isExist.Update(correlationMatrix, param.CreateBy, param.CreateDate);
                                _correlatedSektorDetailRepository.Update(isExist);
                            }

                        }
                    }

                    if (a > 0)
                    {
                        int audit = _auditLogService.AddCorrelationSektorDetailAudit(correlatedSektor.Id, Convert.ToInt32(param.CreateBy));
                    }
                    //comparation old data and new data risk registrasi
                    #region comparation old data and new data risk registrasi
                    #region newRiskList
                    IList<int> newRisks = new List<int>();
                    foreach (var item in param.CorrelatedSektorDetailCollection)
                    {
                        newRisks.Add(item.RiskRegistrasiId);
                    }
                    #endregion newRiskList

                    #region oldRiskList
                    IList<int> oldRisks = new List<int>();
                    var oldData = _correlatedSektorDetailRepository.GetOldData(correlatedSektor.Id).ToList();
                    var oldRiskList = oldData.GroupBy(p => p.RiskRegistrasiIdRow).ToList();
                    if (oldRiskList.Count > 0)
                    {
                        foreach (var item in oldRiskList)
                        {
                            oldRisks.Add(item.Key);
                        }
                    }
                    #endregion oldRiskList

                    //Disable old data
                    var differences = oldRisks.Except(newRisks);
                    if (differences != null || differences.Count() > 0)
                    {
                        foreach (var item in differences)
                        {
                            var riskRegistrasiId = item;
                            var dataRow = _correlatedSektorDetailRepository.GetByCorrelatedSectorRiskRegistrasiRow(correlatedSektor.Id, item).ToList();
                            foreach (var itemRisk in dataRow)
                            {
                                itemRisk.SetDisable(param.CreateBy, param.CreateDate);
                                _correlatedSektorDetailRepository.Update(itemRisk);
                            }

                            var dataCol = _correlatedSektorDetailRepository.GetByCorrelatedSectorRiskRegistrasiCol(correlatedSektor.Id, item).ToList();
                            foreach (var itemRisk in dataCol)
                            {
                                itemRisk.SetDisable(param.CreateBy, param.CreateDate);
                                _correlatedSektorDetailRepository.Update(itemRisk);
                            }
                        }
                    }
                    if (nomorUrutStatus == 3)
                    {
                        var deleteCorrelatedSektor = Update2(correlatedSektor.Id, param);
                    }
                    #endregion comparation old data and new data risk registrasi
                    _databaseContext.SaveChanges();
                    #region email
                    //Untuk notifikasi email submit
                    if (isSend == true && isStatusApproval == false)
                    {
                        //AddApproval(id, status, param.UpdateBy, param.UpdateDate, nomorUrutStatus);

                        if (nomorUrutStatus != 3)
                        {
                            emailDari = param.CreateBy.GetValueOrDefault();

                            var master = _masterApprovalCorrelatedSektorRepository.GetByNomorUrut(nomorUrutStatus + 1);
                            Validate.NotNull(master, "Master Approval Correlated Sektor tidak ditemukan.");

                            //
                            emailKepada = master.UserId.GetValueOrDefault();

                            Validate.NotNull(status, "Status description submit Correlated Sektor tidak ditemukan.");
                            //
                            typePesan = status.StatusDescription;

                            //
                            requestId = correlatedSektor.Id;

                            if (nomorUrutStatus < 2)
                            {
                                master = _masterApprovalCorrelatedSektorRepository.GetByNomorUrut(nomorUrutStatus + 2);
                                Validate.NotNull(master, "Master Approval Correlated Sektor is not found.");

                                emailCC.Add(master.UserId.GetValueOrDefault());
                                //source
                                 _emailService.NotifyResetPassword(source, typePesan, requestId, emailKepada, emailCC, emailDari, templateNotif);
                            }
                            else
                            {
                                emailCC.Add(master.UserId.GetValueOrDefault());
                                _emailService.NotifyResetPassword(source, typePesan, requestId, emailCC, emailDari, templateNotif);
                            }



                        }
                    }
                    //untuk notifikasi email approve
                    if (isSend == false && isStatusApproval == true)
                    {

                        if (nomorUrutStatus != 3)
                        {
                            emailDari = param.UpdateBy.GetValueOrDefault();

                            var master2 = _masterApprovalCorrelatedSektorRepository.GetByNomorUrut(nomorUrutStatus + 1);
                            emailKepada = master2.UserId.GetValueOrDefault();

                            var master = _masterApprovalCorrelatedSektorRepository.GetAll();
                            Validate.NotNull(master, "Master Approval Correlated Sektor tidak ditemukan.");

                            foreach (var item in master)
                            {
                                emailCC.Add(item.UserId.GetValueOrDefault());
                            }
                            //
                            emailCC.Add(correlatedSektor.CreateBy.GetValueOrDefault());

                            Validate.NotNull(statusEmail, "Status description approve Correlated Sektor tidak ditemukan.");
                            //
                            typePesan = statusEmail.StatusDescription;

                            //
                            requestId = correlatedSektor.Id;

                            //source
                             _emailService.NotifyResetPassword(source, typePesan, requestId, emailKepada, emailCC, emailDari, templateNotif);

                        }
                    }
                    #endregion email
                    _unitOfWork.Commit();
                    //id = model.Id;
                }
            }
            
            return id;
        }

        private void AddApproval(int idParam, Status status, int? createBy, DateTime? createDate, int nomorUrutStatus)
        {
            var user = _userRepository.Get(createBy.GetValueOrDefault());
            Validate.NotNull(user, "User Create is not found.");

            var userRole = _userRepository.GetRoleUser(user.Id);
            Validate.NotNull(userRole, "User Role Create is not found.");

            string statusApproval = status.StatusDescription + " By " + user.UserName + " Role " + userRole.Name;
            string keterangan = "";
            string sourceApproval = "CorrelatedSektor";

            Approval model = new Approval(idParam, statusApproval,
            sourceApproval, status, user.Id, createDate, keterangan, nomorUrutStatus);

            _approvalRepository.Insert(model);

        }
        
        public int Duplicate(CorrelatedSektorDetailCollectionParam param)
        {

            int id;
            //Validate.NotNull(param.NamaScenario, "Nama Scenario is required.");

            CorrelatedSektor correlatedSektor = _correlatedSektorRepository.Get(param.CorrelatedSektorId);
            Validate.NotNull(correlatedSektor, "Correlated Sector tidak ditemukan.");

            var scenario = _scenarioRepository.Get(correlatedSektor.ScenarioId);
            //  correlatedProject.ScenarioId;

            #region Approval
            Status status = null;
            int nomorUrutStatus = 0;

            if (param.IsSend == true && param.IsUpdate == null)
            {
                status = _statusRepository.Get(4);
                Validate.NotNull(status, "Status is not found.");
            }else
            if (param.IsSend == true && param.IsUpdate == true && param.IsDraftApproval == true)
            {
                status = _statusRepository.Get(4);
                Validate.NotNull(status, "Status is not found.");
            }else

            if (param.IsSend == true && param.IsUpdate == true && param.IsDraftApproval == null)
            {
                var checkMasterCorrelatedSektor = _masterApprovalCorrelatedSektorRepository.GetAll();
                var checkoutMaster = checkMasterCorrelatedSektor.Count();
                if (checkoutMaster == 0)
                {
                    Validate.NotNull(null, "User must setting Approval for Correlated Sector first.");
                }
                var userCreate = _userRepository.Get(param.CreateBy.GetValueOrDefault());
                Validate.NotNull(userCreate, "User is not found.");

                //get role level from user id
                var masterApproval = _masterApprovalCorrelatedSektorRepository.GetByUserId(userCreate.Id);
                if (masterApproval != null)
                {
                    nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
                }
                if (masterApproval == null && (userCreate.RoleId == 3 || userCreate.RoleId == 4 || userCreate.RoleId == 5))
                {
                    masterApproval = _masterApprovalCorrelatedSektorRepository.GetByRoleId(userCreate.RoleId);
                    nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
                }
                status = _statusRepository.GetStatus(nomorUrutStatus);
                //status = _statusRepository.Get(1);
                Validate.NotNull(status, "Status is not found.");
            }

            #endregion Approval

            CorrelatedSektor model = new CorrelatedSektor(correlatedSektor.SektorId, correlatedSektor.NamaSektor, scenario,  param.CreateBy, param.CreateDate, status);
            _correlatedSektorRepository.Insert(model);
            //test get user for approval
            var user = _userRepository.GetAll();
            _databaseContext.SaveChanges();

            foreach (var riskItem in param.CorrelatedSektorDetailCollection)
            {
                RiskRegistrasi riskRegistrasi = _riskRegistrasiRepository.Get(riskItem.RiskRegistrasiId);
                Validate.NotNull(riskRegistrasi, "Risk Kategori tidak ditemukan.");

                foreach (var item in riskItem.RiskRegistrasiValues)
                {
                    CorrelationMatrix correlationMatrix = _correlationMatrixRepository.Get(item.CorrelationMatrixId);
                    Validate.NotNull(correlationMatrix, "Correlation Matrix tidak ditemukan.");

                    RiskRegistrasi riskRegistrasiRow = _riskRegistrasiRepository.Get(item.RiskRegistrasiIdRow);
                    Validate.NotNull(riskRegistrasiRow, "Risk Kategori (row) tidak ditemukan.");

                    RiskRegistrasi riskRegistrasiCol = _riskRegistrasiRepository.Get(item.RiskRegistrasiIdCol);
                    Validate.NotNull(riskRegistrasiCol, "Risk Kategori (col) tidak ditemukan.");

                    var isExist = _correlatedSektorDetailRepository.IsExisitOnAdding(model.Id, riskRegistrasiRow.Id, riskRegistrasiCol.Id);

                    if (isExist == null)
                    {
                        CorrelatedSektorDetail model2 = new CorrelatedSektorDetail(model, riskRegistrasiRow.Id, riskRegistrasiCol.Id, correlationMatrix, param.CreateBy, param.CreateDate);
                        _correlatedSektorDetailRepository.Insert(model2);

                    }
                    else
                    {
                        isExist.Update(correlationMatrix, param.CreateBy, param.CreateDate);
                        _correlatedSektorDetailRepository.Update(isExist);
                    }

                }
            }


            //comparation old data and new data risk registrasi
            #region comparation old data and new data risk registrasi
            #region newRiskList
            IList<int> newRisks = new List<int>();
            foreach (var item in param.CorrelatedSektorDetailCollection)
            {
                newRisks.Add(item.RiskRegistrasiId);
            }
            #endregion newRiskList

            #region oldRiskList
            IList<int> oldRisks = new List<int>();
            var oldData = _correlatedSektorDetailRepository.GetOldData(model.Id).ToList();
            var oldRiskList = oldData.GroupBy(p => p.RiskRegistrasiIdRow).ToList();
            if (oldRiskList.Count > 0)
            {
                foreach (var item in oldRiskList)
                {
                    oldRisks.Add(item.Key);
                }
            }
            #endregion oldRiskList

            //Disable old data
            var differences = oldRisks.Except(newRisks);
            if (differences != null || differences.Count() > 0)
            {
                foreach (var item in differences)
                {
                    var riskRegistrasiId = item;
                    var dataRow = _correlatedSektorDetailRepository.GetByCorrelatedSectorRiskRegistrasiRow(model.Id, item).ToList();
                    foreach (var itemRisk in dataRow)
                    {
                        itemRisk.SetDisable(param.CreateBy, param.CreateDate);
                        _correlatedSektorDetailRepository.Update(itemRisk);
                    }

                    var dataCol = _correlatedSektorDetailRepository.GetByCorrelatedSectorRiskRegistrasiCol(model.Id, item).ToList();
                    foreach (var itemRisk in dataCol)
                    {
                        itemRisk.SetDisable(param.CreateBy, param.CreateDate);
                        _correlatedSektorDetailRepository.Update(itemRisk);
                    }
                }
            }
            #endregion comparation old data and new data risk registrasi
            //insert into approval
            //if (status != null)
            //{
            //    AddApproval(model.Id, status, param.CreateBy, param.CreateDate, nomorUrutStatus);
            //}
            _databaseContext.SaveChanges();
            return model.Id;
        }

        public int Update2(int id, CorrelatedSektorDetailCollectionParam param)
        {
            CorrelatedSektor model = _correlatedSektorRepository.Get(id);
            Validate.NotNull(model, "Correlated Secktor tidak ditemukan.");

            var PerluExistingName = _correlatedSektorRepository.GetByScenarioIdSektorIdList(model.ScenarioId,  model.SektorId);
            int ValidateName = PerluExistingName.Count();


            #region Approval
            bool isSend = param.IsSend.GetValueOrDefault();
            param.IsSend = null;

            int nomorUrutStatus = 0;

            //variabel email
            Status statusEmail = null;
            if (param.StatusId != null)
            {
                statusEmail = _statusRepository.Get(param.StatusId.GetValueOrDefault());
                param.StatusId = null;
            }

            int emailDari = param.UpdateBy.GetValueOrDefault();
            int emailKepada = model.CreateBy.GetValueOrDefault();
            List<int> emailCC = new List<int>();

            string typePesan = "";
            string source = "CorrelatedSektor";
            int requestId = 0;
            string templateNotif = param.TemplateNotif;
            param.TemplateNotif = null;
            string keterangan = param.Keterangan;
            param.Keterangan = null;
            bool isStatusApproval = param.IsStatusApproval.GetValueOrDefault();
            param.IsStatusApproval = null;

            if (param.NomorUrutStatus.GetValueOrDefault() != 0)
            {
                nomorUrutStatus = param.NomorUrutStatus.GetValueOrDefault();
                param.NomorUrutStatus = null;
            }
            #endregion Approval

            // int audit = _auditLogService.UpdateScenarioAudit(param, id);

            

            _databaseContext.SaveChanges();


            var correlatedSectorSameName = _correlatedSektorRepository.GetByScenarioIdSektorIdList2(model.ScenarioId, model.SektorId, param.CreateBy.GetValueOrDefault());
            var otherCorrelatedSektorId = 0;

            bool IsFirst = true;
            foreach (var item in correlatedSectorSameName)
            {
                if (item.StatusId == 2 && item.Id != model.Id)
                {
                    IsFirst = false;
                }
            }

            if (IsFirst)
            {
                foreach (var item in correlatedSectorSameName)
                {
                    if (item.StatusId == 1)
                    {
                        Delete2(item.Id, Convert.ToInt32(param.CreateBy), Convert.ToDateTime(param.CreateDate));
                    }
                }
            }
            else
            {
                foreach (var item in correlatedSectorSameName)
                {
                    if (item.StatusId == 1)
                    {
                        //item.SetNullStatus(Convert.ToInt32(param.CreateBy), Convert.ToDateTime(param.CreateDate));
                        Delete2(item.Id, Convert.ToInt32(param.CreateBy), Convert.ToDateTime(param.CreateDate));
                    }
                    if (item.StatusId == 2 && item.Id != model.Id)
                    {
                        Delete2(item.Id, Convert.ToInt32(param.CreateBy), Convert.ToDateTime(param.CreateDate));
                    }
                }
            }

            _databaseContext.SaveChanges();
            var correlatedSectorSame = _correlatedSektorRepository.GetByScenarioIdSektorIdList(model.ScenarioId, model.SektorId);
            foreach (var item in correlatedSectorSame)
            {
                if (item.StatusId == 2 && item.Id != model.Id)
                {
                    Delete2(item.Id, Convert.ToInt32(param.CreateBy), Convert.ToDateTime(param.CreateDate));
                }
                if (item.StatusId == 1 && item.Id != model.Id)
                {
                    Delete2(item.Id, Convert.ToInt32(param.CreateBy), Convert.ToDateTime(param.CreateDate));
                }
            }
            id = model.Id;
            _databaseContext.SaveChanges();
            #region email
            //untuk notifikasi email approve
            if (isSend == false && isStatusApproval == true)
            {
                if (nomorUrutStatus == 3)
                {

                    var master = _masterApprovalCorrelatedSektorRepository.GetAll();
                    Validate.NotNull(master, "Master Approval Correlated Correlated tidak ditemukan.");

                    foreach (var item in master)
                    {
                        emailCC.Add(item.UserId.GetValueOrDefault());
                    }
                    //
                    if (!emailCC.Contains(emailKepada))
                    {
                        emailCC.Add(emailKepada);
                    }

                    //
                    typePesan = statusEmail.StatusDescription; ;

                    //
                    requestId = model.Id;

                    //source
                    _emailService.NotifyResetPassword(source, typePesan, requestId, emailCC, emailDari, templateNotif);
                }
            }
            #endregion email
            return id;

        }

        public int Delete2(int id, int deleteBy, DateTime deleteDate)
        {

            CorrelatedSektor correlatedSektor = _correlatedSektorRepository.Get(id);
            Validate.NotNull(correlatedSektor, "Correlated Sector tidak ditemukan.");

            correlatedSektor.Delete(deleteBy, deleteDate);
            _correlatedSektorRepository.Update(correlatedSektor);


            _databaseContext.SaveChanges();
            return id;
        }

        public int AddMurni(CorrelatedSektorDetailCollectionParam param)
        {
            int id = 0;
            CorrelatedSektor correlatedSektor = _correlatedSektorRepository.Get(param.CorrelatedSektorId);
            Validate.NotNull(correlatedSektor, "Correlated Sektor tidak ditemukan.");
           
                foreach (var riskItem in param.CorrelatedSektorDetailCollection)
                {
                    RiskRegistrasi riskRegistrasi = _riskRegistrasiRepository.Get(riskItem.RiskRegistrasiId);
                    Validate.NotNull(riskRegistrasi, "Risk Kategori tidak ditemukan.");

                    foreach (var item in riskItem.RiskRegistrasiValues)
                    {
                        CorrelationMatrix correlationMatrix = _correlationMatrixRepository.Get(item.CorrelationMatrixId);
                        Validate.NotNull(correlationMatrix, "Correlation Matrix tidak ditemukan.");

                        RiskRegistrasi riskRegistrasiRow = _riskRegistrasiRepository.Get(item.RiskRegistrasiIdRow);
                        Validate.NotNull(riskRegistrasiRow, "Risk Kategori (row) tidak ditemukan.");

                        RiskRegistrasi riskRegistrasiCol = _riskRegistrasiRepository.Get(item.RiskRegistrasiIdCol);
                        Validate.NotNull(riskRegistrasiCol, "Risk Kategori (col) tidak ditemukan.");

                        var isExist = _correlatedSektorDetailRepository.IsExisitOnAdding(correlatedSektor.Id, riskRegistrasiRow.Id, riskRegistrasiCol.Id);

                        if (isExist == null)
                        {
                            CorrelatedSektorDetail model = new CorrelatedSektorDetail(correlatedSektor, riskRegistrasiRow.Id, riskRegistrasiCol.Id, correlationMatrix, correlatedSektor.CreateBy, param.CreateDate);
                            _correlatedSektorDetailRepository.Insert(model);

                        }
                        else
                        {
                            //Audit Log Update 
                            int audit = _auditLogService.CorrelationSektorDetailAudit(isExist, correlationMatrix, Convert.ToInt32(param.UpdateBy));

                            isExist.Update(correlationMatrix, param.CreateBy, param.CreateDate);
                            _correlatedSektorDetailRepository.Update(isExist);
                        }

                    }
                }


                //comparation old data and new data risk registrasi
                #region comparation old data and new data risk registrasi
                #region newRiskList
                IList<int> newRisks = new List<int>();
                foreach (var item in param.CorrelatedSektorDetailCollection)
                {
                    newRisks.Add(item.RiskRegistrasiId);
                }
                #endregion newRiskList

                #region oldRiskList
                IList<int> oldRisks = new List<int>();
                var oldData = _correlatedSektorDetailRepository.GetOldData(correlatedSektor.Id).ToList();
                var oldRiskList = oldData.GroupBy(p => p.RiskRegistrasiIdRow).ToList();
                if (oldRiskList.Count > 0)
                {
                    foreach (var item in oldRiskList)
                    {
                        oldRisks.Add(item.Key);
                    }
                }
                #endregion oldRiskList

                //Disable old data
                var differences = oldRisks.Except(newRisks);
                if (differences != null || differences.Count() > 0)
                {
                    foreach (var item in differences)
                    {
                        var riskRegistrasiId = item;
                        var dataRow = _correlatedSektorDetailRepository.GetByCorrelatedSectorRiskRegistrasiRow(correlatedSektor.Id, item).ToList();
                        foreach (var itemRisk in dataRow)
                        {
                            itemRisk.SetDisable(param.CreateBy, param.CreateDate);
                            _correlatedSektorDetailRepository.Update(itemRisk);
                        }

                        var dataCol = _correlatedSektorDetailRepository.GetByCorrelatedSectorRiskRegistrasiCol(correlatedSektor.Id, item).ToList();
                        foreach (var itemRisk in dataCol)
                        {
                            itemRisk.SetDisable(param.CreateBy, param.CreateDate);
                            _correlatedSektorDetailRepository.Update(itemRisk);
                        }
                    }
                }

            #endregion comparation old data and new data risk registrasi


            _databaseContext.SaveChanges();
                //id = model.Id;
            
          
            return id;
        }
    }   
}
