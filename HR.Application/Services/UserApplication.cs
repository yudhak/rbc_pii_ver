﻿using HR.Core;
using HR.Domain;
using System;
using System.Collections.Generic;
using HR.Application.DTO;
using HR.Common.Validation;
using HR.Application.Params;

namespace HR.Application
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserRepository _userRepository;
        private readonly IUserRoleRepository _userRoleRepository;
        public UserService(IUnitOfWork unitOfWork, IUserRepository userRepository, IUserRoleRepository userRoleRepository)
        {
            _unitOfWork = unitOfWork;
            _userRepository = userRepository;
            _userRoleRepository = userRoleRepository;
        }

        public void AssertIdNotAlreadyExist(string userName)
        {
            throw new NotImplementedException();
        }

        public void AssertIdNotAlreadyExistUpdate(int id, string userName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetAll(string keyword)
        {
            return _userRepository.GetAll(keyword);
        }

        public IEnumerable<User> GetAll(string keyword,int field)
        {
            return _userRepository.GetAll(keyword, field);
        }

        public User Get(int id)
        {
            return _userRepository.Get(id);
        }

        public User GetByUserName(string userName)
        {
            return _userRepository.Find(userName);
        }

        public bool isExistUserName(string userName)
        {
            return _userRepository.isExist(userName);
        }

        public UserRoleDTO GetUserRole(int userId)
        {
            var result = _userRoleRepository.GetByUserId(userId);
            return UserRoleDTO.From(result);
        }

        public RoleDTO GetRole(int userId)
        {
            var result = _userRepository.GetRoleUser(userId);
            return RoleDTO.From(result);
        }

        public IEnumerable<User> GetAllFilter()
        {
            var user = _userRepository.GetAllFilter();
            return user;
        }

        
        public void IsExistOnEditing(int id, string userName, string password, string email, int roleId, bool status)
        {
            if (_userRepository.IsExist(id, userName))
            {
                throw new ApplicationException(string.Format("Username {0} sudah ada.", userName));
            }
        }

        public void isExistOnAdding(string userName)
        {
            if (_userRepository.IsExist(userName))
            {
                throw new ApplicationException(string.Format("UserName {0} sudah ada.", userName));
            }
        }

        #region Manipulation 
        public int Add(UserParam param)
        {
            int id;
            Validate.NotNull(param.UserName, "Username wajib diisi.");

            isExistOnAdding(param.UserName);
            using (_unitOfWork)
            {
                var dt = DateTime.Now;
                User model = new User(param.UserName, param.Email, param.RoleId, param.Status, dt, param.Password);
                _userRepository.Insert(model);
                _unitOfWork.Commit();
                id = model.Id;

                //Audit Log ADD 
                //int audit = _auditLogService.AddAssetDataAudit(param, id);
            }

            return id;
        }

        public int Update(int id, UserParam param)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Asset Class tidak ditemukan.");

            IsExistOnEditing(id, param.UserName, param.Password, param.Email, param.RoleId, param.Status);
            using (_unitOfWork)
            {
                //Audit Log UPDATE 
                //int audit = _auditLogService.UpdateAssetDataAudit(param, id);
                var dt = DateTime.Now;
                model.Update(param.UserName, param.Password, param.Email, param.RoleId, param.Status, dt);
                _userRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;
            }
            return id;
        }
        #endregion Manipulation

        public void Delete(int id)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "User tidak ditemukan.");

            using (_unitOfWork)
            {
                _userRepository.Delete(id);
                _unitOfWork.Commit();
                
            }
        }

        public void Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "User tidak ditemukan.");

            using (_unitOfWork)
            {
                _userRepository.Delete(id, deleteBy, deleteDate);
                _unitOfWork.Commit();

            }
        }
    }
}
