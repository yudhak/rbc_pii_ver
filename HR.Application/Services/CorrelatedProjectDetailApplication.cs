﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;
using HR.Infrastructure;

namespace HR.Application
{
    public class CorrelatedProjectDetailService : ICorrelatedProjectDetailService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICorrelatedProjectDetailRepository _correlatedProjectDetailRepository;
        private readonly ICorrelatedProjectRepository _correlatedProjectRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly ICorrelationMatrixRepository _correlationMatrixRepository;
        private readonly ISektorRepository _sektorRepository;
        private readonly IAuditLogService _auditLogService;
        private readonly IUserRepository _userRepository;
        private readonly IStatusRepository _statusRepository;
        private readonly IApprovalRepository _approvalRepository;
        private readonly IMasterApprovalCorrelatedProjectRepository _masterApprovalCorrelatedProjectRepository;
        private readonly IScenarioRepository _scenarioRepository;
        private readonly IEmailService _emailService;
        private readonly IUserService _userService;

        private readonly IDatabaseContext _databaseContext;

        public CorrelatedProjectDetailService(IUnitOfWork unitOfWork, ICorrelatedProjectDetailRepository correlatedProjectDetailRepository, ICorrelatedProjectRepository correlatedProjectRepository, 
            IProjectRepository projectRepository, ICorrelationMatrixRepository correlationMatrixRepository, 

            ISektorRepository sektorRepository, IAuditLogService auditLogService, IUserRepository userRepository, IStatusRepository statusRepository, IApprovalRepository approvalRepository
            , IMasterApprovalCorrelatedProjectRepository masterApprovalCorrelatedProjectRepository, IScenarioRepository scenarioRepository, IDatabaseContext databaseContext,
            IEmailService emailService, IUserService userService
            )
        {
            _unitOfWork = unitOfWork;
            _correlatedProjectDetailRepository = correlatedProjectDetailRepository;
            _correlatedProjectRepository = correlatedProjectRepository;
            _projectRepository = projectRepository;
            _correlationMatrixRepository = correlationMatrixRepository;
            _sektorRepository = sektorRepository;
            _auditLogService = auditLogService;
            _userRepository = userRepository;
            _statusRepository = statusRepository;
            _approvalRepository = approvalRepository;
            _masterApprovalCorrelatedProjectRepository = masterApprovalCorrelatedProjectRepository;
            _scenarioRepository = scenarioRepository;
            _databaseContext = databaseContext;
            _emailService = emailService;
            _userService = userService;
        }

        public int Add(CorrelatedProjectDetailCollectionParam param)
        {

            int id = 0;

            CorrelatedProject correlatedProject = _correlatedProjectRepository.Get(param.CorrelatedProjectId);
            Validate.NotNull(correlatedProject, "CorrelatedProject tidak ditemukan.");

            //variabel untuk email
            int emailDari = 0;
            int emailKepada = 0;
            List<int> emailCC = new List<int>();
            string source = "CorrelatedProject";
            string typePesan = "";
            Status statusEmail = null;
            string templateNotif;
            int requestId = 0;
            string keterangan = "";
            var userCreateNew = _userService.Get(param.CreateBy.GetValueOrDefault());
            //id = correlatedProject.Id;
            if (correlatedProject.StatusId == 2)
            {
                var PerluExistingName = _correlatedProjectRepository.GetByScenarioProjectSektorId2(correlatedProject.ScenarioId, correlatedProject.ProjectId, correlatedProject.SektorId, param.CreateBy.GetValueOrDefault()).Where(x => x.StatusId !=2);
                int ValidateName = PerluExistingName.Count();
                if (ValidateName >= 1)
                {
                    string dodol = null;
                    Validate.NotNull(dodol, "User tidak boleh mempunyai 2 draft dengan korelasi Proyek yang sama.");
                    return 0;
                }
                else
                {
                    var checkMasterScenario = _masterApprovalCorrelatedProjectRepository.GetAllByProjectId(correlatedProject.Project.Id);
                    var checkoutMaster = checkMasterScenario.Count();
                    if (checkoutMaster == 0)
                    {
                        Validate.NotNull(null, "User harus setting Approval untuk Korelasi Proyek terlebih dahulu.");
                    }

                    foreach (var itemMain in param.CorrelatedProjectDetailCollection)
                    {
                        foreach (var itemDetail in itemMain.CorrelatedProjectMatrixValues)
                        {
                            CorrelationMatrix correlationMatrix = _correlationMatrixRepository.Get(itemDetail.CorrelationMatrixId);
                            Project projectIdRow = _projectRepository.Get(itemDetail.ProjectIdRow);
                            Project projectIdCol = _projectRepository.Get(itemDetail.ProjectIdCol);

                            var data = _correlatedProjectDetailRepository.IsExisitOnAdding(correlatedProject.Id, projectIdRow.Id, projectIdCol.Id);
                            //Audit Log Update 
                            int audit = _auditLogService.CorrelationProjectDetailAudit(data, correlationMatrix, Convert.ToInt32(param.CreateBy));
                            data.Update(correlationMatrix, param.CreateBy, param.CreateDate);
                            _correlatedProjectDetailRepository.Update(data);
                        }
                    }

                    int nomorUrutStatus = 0;
                    int dodol = Convert.ToInt32(param.CreateBy);
                    var masterApproval = _masterApprovalCorrelatedProjectRepository.GetByProjectIdUserId(correlatedProject.ProjectId, dodol);
    
                    if (masterApproval != null)
                    {
                        nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
                    }
                    if (masterApproval == null && (userCreateNew.RoleId == 3 || userCreateNew.RoleId == 4 || userCreateNew.RoleId == 5))
                    {
                        masterApproval = _masterApprovalCorrelatedProjectRepository.GetByProjectIdRoleId(correlatedProject.ProjectId, userCreateNew.RoleId);
                        nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
                    }
                    if (nomorUrutStatus != 0)
                    {
                        param.IsUpdate = true;
                        var duplicate = Duplicate(param);

                        var status = _statusRepository.GetStatus(nomorUrutStatus);
                        if (param.IsSend == true && status.Id != 2)
                        {
                            param.IsDraftApproval = true;
                            int idDuplicate = Duplicate(param);

                            AddApproval(idDuplicate, status, param.CreateBy, param.CreateDate, nomorUrutStatus);
                            _databaseContext.SaveChanges();

                            #region email
                            if (param.IsSend == true && param.IsStatusApproval == false || param.IsStatusApproval == null)
                            {
                                if (nomorUrutStatus != 3)
                                {
                                    emailDari = param.CreateBy.GetValueOrDefault();

                                    var master = _masterApprovalCorrelatedProjectRepository.GetByNomorUrutProjectId(nomorUrutStatus + 1, correlatedProject.ProjectId);
                                    Validate.NotNull(master, "Master Approval Korelasi Proyek tidak ditemukan.");
                                    emailKepada = master.UserId.GetValueOrDefault();

                                    typePesan = status.StatusDescription;

                                    requestId = correlatedProject.Id;

                                    if (nomorUrutStatus < 2)
                                    {
                                        master = _masterApprovalCorrelatedProjectRepository.GetByNomorUrutProjectId(nomorUrutStatus + 1, correlatedProject.ProjectId);
                                        Validate.NotNull(master, "Master Approval Korelasi Proyek tidak ditemukan.");

                                        emailCC.Add(master.UserId.GetValueOrDefault());
                                        //source
                                        _emailService.NotifyResetPassword(source, typePesan, requestId, emailKepada, emailCC, emailDari, param.TemplateNotif);
                                    }
                                    else
                                    {
                                        emailCC.Add(master.UserId.GetValueOrDefault());
                                        _emailService.NotifyResetPassword(source, typePesan, requestId, emailCC, emailDari, param.TemplateNotif);
                                    }


                                }
                            }
                            #endregion email
                        }

                        if (status.Id == 2)
                        {
                            if (param.IsSend == true)
                            {
   
                                var deleteCorrelatedProject = Update2(duplicate, param);
                                SetIsApproved(duplicate, param.CreateBy.GetValueOrDefault(), param.CreateDate.GetValueOrDefault());
                            }
                        }
                        return duplicate;
                    }
                    else
                    {
                        param.IsUpdate = true;
                        var duplicate = Duplicate(param);
                        var status = _statusRepository.GetStatus(nomorUrutStatus);
                        if (param.IsSend == true)
                        {
                            param.IsDraftApproval = true;
                            int idDuplicate = Duplicate(param);

                            AddApproval(idDuplicate, status, param.CreateBy, param.CreateDate, nomorUrutStatus);
                            _databaseContext.SaveChanges();

                            #region email
                            if (param.IsSend == true && param.IsStatusApproval == false || param.IsStatusApproval == null)
                            {
                                if (nomorUrutStatus != 3)
                                {
                                    emailDari = param.CreateBy.GetValueOrDefault();

                                    var master = _masterApprovalCorrelatedProjectRepository.GetByNomorUrutProjectId(nomorUrutStatus + 1, correlatedProject.ProjectId);
                                    Validate.NotNull(master, "Master Approval Korelasi Proyek tidak ditemukan.");
                                    emailKepada = master.UserId.GetValueOrDefault();

                                    typePesan = status.StatusDescription;

                                    requestId = correlatedProject.Id;

                                    if (nomorUrutStatus < 2)
                                    {
                                        master = _masterApprovalCorrelatedProjectRepository.GetByNomorUrutProjectId(nomorUrutStatus + 1, correlatedProject.ProjectId);
                                        Validate.NotNull(master, "Master Approval Korelasi Proyek tidak ditemukan.");

                                        emailCC.Add(master.UserId.GetValueOrDefault());
                                        //source
                                        _emailService.NotifyResetPassword(source, typePesan, requestId, emailKepada, emailCC, emailDari, param.TemplateNotif);
                                    }
                                    else
                                    {
                                        emailCC.Add(master.UserId.GetValueOrDefault());
                                        _emailService.NotifyResetPassword(source, typePesan, requestId, emailCC, emailDari, param.TemplateNotif);
                                    }


                                }
                            }
                            #endregion email
                        }

                        return duplicate;
                    }
                }
            }
            else
            {

                #region Approval
                Status status = null;
                //Get Status from role id
                int nomorUrutStatus = 0;

                bool isSend;
                bool IsStatusApproval;
                if (correlatedProject.StatusId != 2)
                {
                    //Jika Risk Matrix Project saat ini statusnya 1/submit, tidak bisa diedit
                    if (correlatedProject.StatusId == 1 && param.IsStatusApproval == null)
                    {
                        throw new ApplicationException(string.Format("Correlated Project tidak bisa diedit karena dalam proses approval"));
                    }

                    //Jika Risk Matrix Project saat ini statusnya 3/reject, maka akan kirim kembali ke Approval 
                    //atau jika user menekan tombol kirim, maka akan kirim ke tabel Approval
                    else if ((correlatedProject.StatusId == 3 && param.IsSend == true) && param.IsStatusApproval == null || param.IsSend == true && param.IsStatusApproval == null)
                    {
                        var checkMasterScenario = _masterApprovalCorrelatedProjectRepository.GetAllByProjectId(correlatedProject.Project.Id);
                        var checkoutMaster = checkMasterScenario.Count();
                        if (checkoutMaster == 0)
                        {
                            Validate.NotNull(null, "User harus setting Approval untuk Korelasi Proyek terlebih dahulu.");
                        }
                        var userCreate = _userRepository.Get(param.CreateBy.GetValueOrDefault());
                        Validate.NotNull(userCreate, "User tidak ditemukan.");

                        //get role level from user id
                        var masterApproval = _masterApprovalCorrelatedProjectRepository.GetByProjectIdUserId(correlatedProject.ProjectId, userCreate.Id);
                        if (masterApproval != null)
                        {
                            nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
                        }
                        if (masterApproval == null && (userCreate.RoleId == 3 || userCreate.RoleId == 4 || userCreate.RoleId == 5))
                        {
                            masterApproval = _masterApprovalCorrelatedProjectRepository.GetByProjectIdRoleId(correlatedProject.ProjectId, userCreate.RoleId);
                            nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
                        }
                        status = _statusRepository.GetStatus(nomorUrutStatus);
                        Validate.NotNull(status, "Status tidak ditemukan.");

                        if (status != null && status.Id != 2)
                        {
                            param.IsUpdate = true;
                            param.IsDraftApproval = true;
                            int idDuplicate = Duplicate(param);
                            
                            //insert into approval
                            AddApproval(idDuplicate, status, param.CreateBy, param.CreateDate, nomorUrutStatus);
                            _databaseContext.SaveChanges();
                        }
                        correlatedProject.UpdateStatus(status, param.CreateBy, param.CreateDate);
                    }
                }
                IsStatusApproval = param.IsStatusApproval.GetValueOrDefault();
                param.IsStatusApproval = null;
                isSend = param.IsSend.GetValueOrDefault();
                param.IsSend = null;
                if (param.StatusId != null)
                {
                    statusEmail = _statusRepository.Get(param.StatusId.GetValueOrDefault());
                    param.StatusId = null;
                }
                if (param.NomorUrutStatus.GetValueOrDefault() != 0)
                {
                    nomorUrutStatus = param.NomorUrutStatus.GetValueOrDefault();
                    param.NomorUrutStatus = null;
                }
                param.IsDraftApproval = null;
                param.IsUpdate = null;
                keterangan = param.Keterangan;
                param.Keterangan = null;
                templateNotif = param.TemplateNotif;
                param.TemplateNotif = null;
                #endregion Approval
                _databaseContext.SaveChanges();

                using (_unitOfWork)
                {
                    var a = 0;
                    foreach (var itemMain in param.CorrelatedProjectDetailCollection)
                    {
                        foreach (var itemDetail in itemMain.CorrelatedProjectMatrixValues)
                        {
                            CorrelationMatrix correlationMatrix = _correlationMatrixRepository.Get(itemDetail.CorrelationMatrixId);
                            Validate.NotNull(correlationMatrix, "Correlation Matrix tidak ditemukan.");

                            Project projectIdRow = _projectRepository.Get(itemDetail.ProjectIdRow);
                            Validate.NotNull(projectIdRow, "Project (row) tidak ditemukan.");

                            Project projectIdCol = _projectRepository.Get(itemDetail.ProjectIdCol);
                            Validate.NotNull(projectIdCol, "Project (col) tidak ditemukan.");

                            var data = _correlatedProjectDetailRepository.IsExisitOnAdding(correlatedProject.Id, projectIdRow.Id, projectIdCol.Id);
                            if (data == null)
                            {
                                CorrelatedProjectDetail model = new CorrelatedProjectDetail(correlatedProject, projectIdRow.Id, projectIdCol.Id, correlationMatrix, param.CreateBy, param.CreateDate);
                                _correlatedProjectDetailRepository.Insert(model);
                                a++;
                            }
                            else
                            {
                                //Audit Log Update 
                                int audit = _auditLogService.CorrelationProjectDetailAudit(data, correlationMatrix,Convert.ToInt32(param.CreateBy));

                                data.Update(correlationMatrix, param.CreateBy, param.CreateDate);
                                _correlatedProjectDetailRepository.Update(data);
                            }
                        }
                    }

                    if (a > 0)
                    {
                        int audit = _auditLogService.AddCorrelationProjectDetailAudit(correlatedProject.Id, Convert.ToInt32(param.CreateBy));
                    }

                    if (nomorUrutStatus == 3)
                    {
                        
                        var deleteCorrelatedProject = Update2(correlatedProject.Id, param);
                        SetIsApproved(correlatedProject.Id, param.CreateBy.GetValueOrDefault(), param.CreateDate.GetValueOrDefault());
                    }
                    _databaseContext.SaveChanges();
                    #region email notif
                    //Untuk notifikasi email submit
                    if (isSend == true && IsStatusApproval == false)
                    {
                        //AddApproval(id, status, param.UpdateBy, param.UpdateDate, nomorUrutStatus);

                        if (nomorUrutStatus != 3)
                        {
                            emailDari = param.CreateBy.GetValueOrDefault();

                            var master = _masterApprovalCorrelatedProjectRepository.GetByNomorUrutProjectId(nomorUrutStatus + 1, correlatedProject.ProjectId);
                            Validate.NotNull(master, "Master Approval Korelasi Proyek tidak ditemukan.");

                            //
                            emailKepada = master.UserId.GetValueOrDefault();

                            Validate.NotNull(status, "Status description submit Korelasi Proyek tidak ditemukan.");
                            //
                            typePesan = status.StatusDescription;

                            //
                            requestId = correlatedProject.Id;

                            if (nomorUrutStatus < 2)
                            {
                                var master2 = _masterApprovalCorrelatedProjectRepository.GetAllByProjectId(correlatedProject.ProjectId);
                                Validate.NotNull(master, "Master Approval Korelasi Proyek tidak ditemukan.");
                                foreach (var item in master2)
                                {
                                    emailCC.Add(item.UserId.GetValueOrDefault());

                                }

                                //source
                                _emailService.NotifyResetPassword(source, typePesan, requestId, emailKepada, emailCC, emailDari, templateNotif);
                            }
                            else
                            {
                                emailCC.Add(master.UserId.GetValueOrDefault());
                                _emailService.NotifyResetPassword(source, typePesan, requestId, emailCC, emailDari, templateNotif);
                            }



                        }
                    }

                    if (isSend == false && IsStatusApproval == true)
                    {
                        //AddApproval(id, status, param.UpdateBy, param.UpdateDate, nomorUrutStatus);

                        if (nomorUrutStatus != 3)
                        {
                            emailDari = param.CreateBy.GetValueOrDefault();

                            var master2 = _masterApprovalCorrelatedProjectRepository.GetByNomorUrutProjectId(nomorUrutStatus + 1, correlatedProject.ProjectId);
                            emailKepada = master2.UserId.GetValueOrDefault();

                            var master = _masterApprovalCorrelatedProjectRepository.GetAllByProjectId(correlatedProject.ProjectId);
                            Validate.NotNull(master, "Master Approval Correlated Project tidak ditemukan.");

                            foreach (var item in master)
                            {
                                emailCC.Add(item.UserId.GetValueOrDefault());
                            }
                            //
                            // emailCC.Add(emailKepada);

                            Validate.NotNull(statusEmail, "Status description approve Correlated Project tidak ditemukan.");
                            //
                            typePesan = statusEmail.StatusDescription;

                            //
                            requestId = correlatedProject.Id;

                            //source
                            _emailService.NotifyResetPassword(source, typePesan, requestId, emailKepada, emailCC, emailDari, templateNotif);

                        }
                    }
                    #endregion email notif
                    _unitOfWork.Commit();
                }
                return id;
            }


        }

        public CorrelatedProjectDetailCollectionParam GetByCorrelatedProjectId(int correlatedProjectId)
        {
            CorrelatedProjectDetailCollectionParam param = new CorrelatedProjectDetailCollectionParam();
            IList<CorrelatedProjectDetailCollection> dataCollection = new List<CorrelatedProjectDetailCollection>();

            var data = _correlatedProjectDetailRepository.GetByCorrelatedProjectId(correlatedProjectId).ToList();
            if (data.Count > 0)
            {
                foreach (var item in data)
                {
                    CorrelatedProjectDetailCollection detail = new CorrelatedProjectDetailCollection();
                    IList<CorrelatedProjectMatrixValues> detailList = new List<CorrelatedProjectMatrixValues>();

                    var dataDetail = data.Where(x => x.ProjectIdRow == item.ProjectIdRow).ToList();
                    foreach (var itemDetail in dataDetail)
                    {
                        CorrelatedProjectMatrixValues detailValue = new CorrelatedProjectMatrixValues();
                        detailValue.ProjectIdRow = item.ProjectIdRow;
                        detailValue.ProjectIdCol = item.ProjectIdCol;
                        detailValue.CorrelationMatrixId = item.CorrelationMatrixId;

                        detailList.Add(detailValue);
                    }

                    detail.ProjectId = item.ProjectIdRow;
                    detail.CorrelatedProjectMatrixValues = detailList.ToArray();

                    dataCollection.Add(detail);
                }

                var correlatedProject = _correlatedProjectRepository.Get(correlatedProjectId);
                var project = _projectRepository.Get(correlatedProject.ProjectId);
                var sektor = _sektorRepository.Get(correlatedProject.SektorId);

                param.CorrelatedProjectId = data[0].CorrelatedProjectId;
                param.ProjectId = project.Id;
                param.NamaProject = project.NamaProject;
                param.SektorId = sektor.Id;
                param.NamaSektor = sektor.NamaSektor;
                param.CorrelatedProjectDetailCollection  = dataCollection.ToArray();
                param.CreateBy = data[0].CreateBy;
                param.CreateDate = data[0].CreateDate;
                param.UpdateBy = data[0].UpdateBy;
                param.UpdateDate = data[0].UpdateDate;
                param.IsDelete = data[0].IsDelete;
            }
            return param;
        }


        private void AddApproval(int idParam, Status status, int? createBy, DateTime? createDate, int nomorUrutStatus)
        {
            var user = _userRepository.Get(createBy.GetValueOrDefault());
 
            Validate.NotNull(user, "User tidak ditemukan.");


            var userRole = _userRepository.GetRoleUser(user.Id);
            Validate.NotNull(userRole, "User Role tidak ditemukan.");


            string statusApproval = status.StatusDescription + " By " + user.UserName + " Role " + userRole.Name;
            string keterangan = "";
            string sourceApproval = "CorrelatedProject";
            Approval model = new Approval(idParam, statusApproval,

            sourceApproval, status, user.Id, createDate, keterangan, nomorUrutStatus);

            _approvalRepository.Insert(model);

        }

        //public int Add(CorrelatedProjectDetailCollectionParam param)
        //{

        //    int id = 0;

        //    CorrelatedProject correlatedProject = _correlatedProjectRepository.Get(param.CorrelatedProjectId);
        //    Validate.NotNull(correlatedProject, "CorrelatedProject tidak ditemukan.");

        //    #region Approval
        //    Status status = null;
        //    //get status from role id
        //    int nomorUrutStatus = 0;
        //    if (correlatedProject.StatusId != 2)
        //    {
        //        //Jika Risk Matrix Project saat ini statusnya 1/submit, tidak bisa diedit
        //        if (correlatedProject.StatusId == 1 && param.IsStatusApproval == null)
        //        {

        //            throw new ApplicationException(string.Format("Correlated Project tidak bisa diedit karena dalam proses approval"));
        //        }

        //        //Jika Risk Matrix Project saat ini statusnya 3/reject, maka akan kirim kembali ke Approval 
        //        //atau jika user menekan tombol kirim, maka akan kirim ke tabel Approval
        //        else if ((correlatedProject.StatusId == 3 && param.IsSend == true) && param.IsStatusApproval == null || param.IsSend == true && param.IsStatusApproval == null)
        //        {
        //            var checkMasterScenario = _masterApprovalCorrelatedProjectRepository.GetAllByProjectId(correlatedProject.Project.Id);
        //            var checkoutMaster = checkMasterScenario.Count();
        //            if (checkoutMaster == 0)
        //            {
        //                Validate.NotNull(null, "User must setting Approval for Correlation Project first.");
        //            }
        //            var userCreate = _userRepository.Get(param.CreateBy.GetValueOrDefault());
        //            Validate.NotNull(userCreate, "User is not found.");

        //            //get role level from user id
        //            var masterApproval = _masterApprovalCorrelatedProjectRepository.GetByProjectIdUserId(correlatedProject.ProjectId, userCreate.Id);
        //            if (masterApproval != null)
        //            {
        //                nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
        //            }
        //            status = _statusRepository.GetStatus(nomorUrutStatus);
        //            Validate.NotNull(status, "Status is not found.");

        //            //insert into approval
        //            AddApproval(correlatedProject.Id, status, param.CreateBy, param.CreateDate, nomorUrutStatus);
        //            //_unitOfWork.Commit();
        //            correlatedProject.UpdateStatus(status, param.CreateBy, param.CreateDate);
        //        }
        //        else if (correlatedProject.StatusId == 2 && param.IsSend == true)
        //        {
        //            var checkMasterScenario = _masterApprovalCorrelatedProjectRepository.GetAllByProjectId(correlatedProject.Project.Id);
        //            var checkoutMaster = checkMasterScenario.Count();
        //            if (checkoutMaster == 0)
        //            {
        //                Validate.NotNull(null, "User must setting Approval for Correlation Project first.");
        //            }
        //            var userUpdate = _userRepository.Get(param.CreateBy.GetValueOrDefault());
        //            Validate.NotNull(userUpdate, "User is not found.");

        //            //get role level from user id
        //            var masterApproval = _masterApprovalCorrelatedProjectRepository.GetByProjectIdUserId(correlatedProject.ProjectId, userUpdate.Id);
        //            if (masterApproval != null)
        //            {
        //                nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
        //            }
        //            status = _statusRepository.GetStatus(nomorUrutStatus);
        //            Validate.NotNull(status, "Status is not found.");

        //            //insert into approval
        //            AddApproval(correlatedProject.Id, status, param.CreateBy, param.CreateDate, nomorUrutStatus);
        //            //_unitOfWork.Commit();
        //            correlatedProject.UpdateStatus(status, param.CreateBy, param.CreateDate);
        //        }
        //        //jika user menekan tombol kirim, maka akan kirim ke tabel Approval
        //    }

        //    #endregion Approval

        //    param.IsStatusApproval = null;
        //    param.IsSend = null;
        //    using (_unitOfWork)
        //    {
        //        foreach (var itemMain in param.CorrelatedProjectDetailCollection)
        //        {
        //            foreach (var itemDetail in itemMain.CorrelatedProjectMatrixValues)
        //            {
        //                CorrelationMatrix correlationMatrix = _correlationMatrixRepository.Get(itemDetail.CorrelationMatrixId);
        //                Validate.NotNull(correlationMatrix, "Correlation Matrix tidak ditemukan.");

        //                Project projectIdRow = _projectRepository.Get(itemDetail.ProjectIdRow);
        //                Validate.NotNull(projectIdRow, "Project (row) tidak ditemukan.");

        //                Project projectIdCol = _projectRepository.Get(itemDetail.ProjectIdCol);
        //                Validate.NotNull(projectIdCol, "Project (col) tidak ditemukan.");

        //                var data = _correlatedProjectDetailRepository.IsExisitOnAdding(correlatedProject.Id, projectIdRow.Id, projectIdCol.Id);
        //                if (data == null)
        //                {
        //                    CorrelatedProjectDetail model = new CorrelatedProjectDetail(correlatedProject, projectIdRow.Id, projectIdCol.Id, correlationMatrix, param.CreateBy, param.CreateDate);
        //                    _correlatedProjectDetailRepository.Insert(model);
        //                }
        //                else
        //                {
        //                    //Audit Log Update 
        //                    int audit = _auditLogService.CorrelationProjectDetailAudit(data, correlationMatrix);

        //                    data.Update(correlationMatrix, param.CreateBy, param.CreateDate);
        //                    _correlatedProjectDetailRepository.Update(data);
        //                }
        //            }
        //        }
        //        //if (status != null)
        //        //{
        //        //    AddApproval(correlatedProject.Id, status, param.CreateBy, param.CreateDate, nomorUrutStatus);
        //        //}

        //        _unitOfWork.Commit();
        //    }

        //    return id;
        //}

        public int Duplicate(CorrelatedProjectDetailCollectionParam param)
        {

            int id;
            //Validate.NotNull(param.NamaScenario, "Nama Scenario is required.");

            CorrelatedProject correlatedProject = _correlatedProjectRepository.Get(param.CorrelatedProjectId);
            Validate.NotNull(correlatedProject, "Korelasi Proyek tidak ditemukan.");

            var scenario = _scenarioRepository.Get(correlatedProject.ScenarioId);
            Validate.NotNull(scenario, "Scenario tidak ditemukan.");
            //  correlatedProject.ScenarioId;


            #region Approval
            Status status = null;
            //Get Status from role id
            int nomorUrutStatus = 0;
            if (param.IsSend == true && param.IsUpdate == null)
            {
                status = _statusRepository.Get(4);
                Validate.NotNull(status, "Status tidak ditemukan.");
            }

            if (param.IsSend == true && param.IsUpdate == true && param.IsDraftApproval == true)
            {

                status = _statusRepository.Get(4);
                Validate.NotNull(status, "Status tidak ditemukan.");
            }
            if (param.IsSend == true && param.IsUpdate == true && param.IsDraftApproval == null)
            {
                var userCreate = _userRepository.Get(param.CreateBy.GetValueOrDefault());
                Validate.NotNull(userCreate, "User tidak ditemukan.");

                //get role level from user id
                var masterApproval = _masterApprovalCorrelatedProjectRepository.GetByProjectIdUserId(correlatedProject.ProjectId, userCreate.Id);
                if (masterApproval != null)
                {
                    nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
                }
                if (masterApproval == null && (userCreate.RoleId == 3 || userCreate.RoleId == 4 || userCreate.RoleId == 5))
                {
                    masterApproval = _masterApprovalCorrelatedProjectRepository.GetByProjectIdRoleId(correlatedProject.ProjectId, userCreate.RoleId);
                    nomorUrutStatus = masterApproval.NomorUrutStatus.GetValueOrDefault();
                }
                status = _statusRepository.GetStatus(nomorUrutStatus);

                //status = _statusRepository.Get(1);
                Validate.NotNull(status, "Status tidak ditemukan.");
            }
            #endregion Approval

            CorrelatedProject model = new CorrelatedProject(scenario, correlatedProject.ProjectId, correlatedProject.SektorId, param.CreateBy, param.CreateDate, status);
            _correlatedProjectRepository.Insert(model);
            //test get user for approval
            var user = _userRepository.GetAll();
            _databaseContext.SaveChanges();

            foreach (var itemMain in param.CorrelatedProjectDetailCollection)
            {
                foreach (var itemDetail in itemMain.CorrelatedProjectMatrixValues)
                {
                    CorrelationMatrix correlationMatrix = _correlationMatrixRepository.Get(itemDetail.CorrelationMatrixId);
                    Validate.NotNull(correlationMatrix, "Correlation Matrix tidak ditemukan.");

                    Project projectIdRow = _projectRepository.Get(itemDetail.ProjectIdRow);
                    Validate.NotNull(projectIdRow, "Project (row) tidak ditemukan.");

                    Project projectIdCol = _projectRepository.Get(itemDetail.ProjectIdCol);
                    Validate.NotNull(projectIdCol, "Project (col) tidak ditemukan.");

                    var data = _correlatedProjectDetailRepository.IsExisitOnAdding(model.Id, projectIdRow.Id, projectIdCol.Id);
                    if (data == null)
                    {
                        CorrelatedProjectDetail modelCorrelatedProjectDetail = new CorrelatedProjectDetail(model, projectIdRow.Id, projectIdCol.Id, correlationMatrix, param.CreateBy, param.CreateDate);
                        _correlatedProjectDetailRepository.Insert(modelCorrelatedProjectDetail);
                    }

                }
            }

            //insert into approval
            //if (status != null)
            //{
            //    AddApproval(model.Id, status, param.CreateBy, param.UpdateDate, nomorUrutStatus);
            //}
            _databaseContext.SaveChanges();
            return model.Id;
        }

        public int Update2(int id, CorrelatedProjectDetailCollectionParam param)
        {
            CorrelatedProject model = _correlatedProjectRepository.Get(id);
            Validate.NotNull(model, "Correlated Project tidak ditemukan.");

            var PerluExistingName = _correlatedProjectRepository.GetByScenarioProjectSektorId(model.ScenarioId, model.ProjectId, model.SektorId);
            int ValidateName = PerluExistingName.Count();


            #region Approval
            bool isSend = param.IsSend.GetValueOrDefault();
            param.IsSend = null;

            int nomorUrutStatus = 0;

            //variabel email
            Status statusEmail = null;
            if (param.StatusId != null)
            {
                statusEmail = _statusRepository.Get(param.StatusId.GetValueOrDefault());
                param.StatusId = null;
            }

            int emailDari = param.UpdateBy.GetValueOrDefault();
            int emailKepada = model.CreateBy.GetValueOrDefault();
            List<int> emailCC = new List<int>();

            string typePesan = "";
            string source = "CorrelatedProject";
            int requestId = 0;
            string templateNotif = param.TemplateNotif;
            param.TemplateNotif = null;
            string keterangan = param.Keterangan;
            param.Keterangan = null;
            bool isStatusApproval = param.IsStatusApproval.GetValueOrDefault();
            param.IsStatusApproval = null;

            if (param.NomorUrutStatus.GetValueOrDefault() != 0)
            {
                nomorUrutStatus = param.NomorUrutStatus.GetValueOrDefault();
                param.NomorUrutStatus = null;
            }
            #endregion Approval

            // int audit = _auditLogService.UpdateScenarioAudit(param, id);


            //foreach (var itemMain in param.CorrelatedProjectDetailCollection)
            //    {
            //        foreach (var itemDetail in itemMain.CorrelatedProjectMatrixValues)
            //        {
            //            CorrelationMatrix correlationMatrix = _correlationMatrixRepository.Get(itemDetail.CorrelationMatrixId);
            //            Validate.NotNull(correlationMatrix, "Correlation Matrix tidak ditemukan.");

            //            Project projectIdRow = _projectRepository.Get(itemDetail.ProjectIdRow);
            //            Validate.NotNull(projectIdRow, "Project (row) tidak ditemukan.");

            //            Project projectIdCol = _projectRepository.Get(itemDetail.ProjectIdCol);
            //            Validate.NotNull(projectIdCol, "Project (col) tidak ditemukan.");

            //            var data = _correlatedProjectDetailRepository.IsExisitOnAdding(model.Id, projectIdRow.Id, projectIdCol.Id);
            //            if (data == null)
            //            {
            //                CorrelatedProjectDetail modelCorrelatedProjectDetail = new CorrelatedProjectDetail(model, projectIdRow.Id, projectIdCol.Id, correlationMatrix, param.CreateBy, param.CreateDate);
            //                _correlatedProjectDetailRepository.Insert(modelCorrelatedProjectDetail);
            //            }
            //            else
            //            {
            //                //Audit Log Update 
            //                int audit = _auditLogService.CorrelationProjectDetailAudit(data, correlationMatrix);

            //                data.Update(correlationMatrix, param.CreateBy, param.CreateDate);
            //                _correlatedProjectDetailRepository.Update(data);
            //            }
            //        }
            //    }

                _databaseContext.SaveChanges();


            var correlatedProjectSameName = _correlatedProjectRepository.GetByScenarioProjectSektorId2(model.ScenarioId, model.ProjectId, model.SektorId, param.CreateBy.GetValueOrDefault());
            var otherCorrelatedProjectId = 0;
            bool IsFirst = true;
            foreach (var item in correlatedProjectSameName)
            {
                if (item.StatusId == 2 && item.Id != model.Id)
                {
                    IsFirst = false;
                }
            }

            if (IsFirst)
            {
                foreach (var item in correlatedProjectSameName)
                {
                    if (item.StatusId == 1)
                    {
                        Delete2(item.Id, Convert.ToInt32(param.CreateBy), Convert.ToDateTime(param.CreateDate));
                    }
                }
            }
            else
            {
                foreach (var item in correlatedProjectSameName)
                {
                    if (item.StatusId == 1)
                    {
                        //item.SetNullStatus(Convert.ToInt32(param.CreateBy), Convert.ToDateTime(param.CreateDate));
                        Delete2(item.Id, Convert.ToInt32(param.CreateBy), Convert.ToDateTime(param.CreateDate));
                    }
                    if (item.StatusId == 2 && item.Id != model.Id)
                    {
                        Delete2(item.Id, Convert.ToInt32(param.CreateBy), Convert.ToDateTime(param.CreateDate));
                    }

                }
            }
            _databaseContext.SaveChanges();
            var correlatedProjectSame = _correlatedProjectRepository.GetByScenarioProjectSektorId(model.ScenarioId, model.ProjectId, model.SektorId);

            foreach (var item in correlatedProjectSame)
            {
                if (item.StatusId == 2 && item.Id != model.Id)
                {
                    Delete2(item.Id, Convert.ToInt32(param.CreateBy), Convert.ToDateTime(param.CreateDate));
                }
                if (item.StatusId == 1 && item.Id != model.Id)
                {
                    Delete2(item.Id, Convert.ToInt32(param.CreateBy), Convert.ToDateTime(param.CreateDate));
                }
            }
            SetIsApproved(model.Id, param.UpdateBy.GetValueOrDefault(), param.UpdateDate.GetValueOrDefault());
            _databaseContext.SaveChanges();
            #region email
            //untuk notifikasi email approve
            if (isSend == false && isStatusApproval == true)
            {
                if (nomorUrutStatus == 3)
                {

                    var master = _masterApprovalCorrelatedProjectRepository.GetAllByProjectId(model.ProjectId);
                    Validate.NotNull(master, "Master Approval Correlated Project tidak ditemukan.");

                    foreach (var item in master)
                    {
                        emailCC.Add(item.UserId.GetValueOrDefault());
                    }
                    //
                    if (!emailCC.Contains(emailKepada))
                    {
                        emailCC.Add(emailKepada);
                    }

                    //
                    typePesan = statusEmail.StatusDescription; ;

                    //
                    requestId = model.Id;

                    //source
                    //code notifikasi email
                    //disini emailCC sebagai emailKepada
                    _emailService.NotifyResetPassword(source, typePesan, requestId, emailCC, emailDari, templateNotif);
                }
            }
            #endregion email

            _databaseContext.SaveChanges();
            id = model.Id;
            
            return id;

        }

        public int Delete2(int id, int deleteBy, DateTime deleteDate)
        {

            CorrelatedProject correlatedProject = _correlatedProjectRepository.Get(id);
            Validate.NotNull(correlatedProject, "Correlated Project is not found.");

            correlatedProject.Delete(deleteBy, deleteDate);
            _correlatedProjectRepository.Update(correlatedProject);


            _databaseContext.SaveChanges();
            return id;
        }

        public int SetIsApproved(int id, int updateBy, DateTime updateDate)
        {

            CorrelatedProject correlatedProject = _correlatedProjectRepository.Get(id);
            Validate.NotNull(correlatedProject, "Correlated Project tidak ditemukan.");

            var scenario = _scenarioRepository.Get(correlatedProject.ScenarioId);
            Validate.NotNull(scenario, "Scenario Correlated Project tidak ditemukan.");

            var correlatedProjectOld = _correlatedProjectRepository.GetIsApprovedList(scenario.Id);
            //Validate.NotNull(correlatedProject, "Correlated Project lama tidak ditemukan.");

            correlatedProject.SetIsApproved(updateBy, updateDate);

            if (correlatedProjectOld != null )
            {
                foreach (var item in correlatedProjectOld )
                {
                    if (item.Id != correlatedProject.Id)
                    {
                        item.RemoveIsApproved(updateBy, updateDate);

                        item.SetNullStatus(updateBy, updateDate);
                    }
                    
                }
                
            }

            var correlatedProjectScenario = _correlatedProjectRepository.GetByScenarioIdIsDeleteFalse(scenario.Id).Where(x => x.StatusId == 2);

            if (correlatedProjectScenario != null)
            {
                foreach (var item in correlatedProjectScenario)
                {
                    if (item.StatusId == 2 && item.Id != correlatedProject.Id)
                    {
                        item.RemoveIsApproved(updateBy, updateDate);

                        item.SetNullStatus(updateBy, updateDate);
                    }
                }
            }
            

            _databaseContext.SaveChanges();
            return id;
        }

        public int AddMurni(CorrelatedProjectDetailCollectionParam param) {
            int id = 0;

            CorrelatedProject correlatedProject = _correlatedProjectRepository.Get(param.CorrelatedProjectId);
            Validate.NotNull(correlatedProject, "CorrelatedProject tidak ditemukan.");

                foreach (var itemMain in param.CorrelatedProjectDetailCollection)
                {
                    foreach (var itemDetail in itemMain.CorrelatedProjectMatrixValues)
                    {
                        CorrelationMatrix correlationMatrix = _correlationMatrixRepository.Get(itemDetail.CorrelationMatrixId);
                        Validate.NotNull(correlationMatrix, "Correlation Matrix tidak ditemukan.");

                        Project projectIdRow = _projectRepository.Get(itemDetail.ProjectIdRow);
                        Validate.NotNull(projectIdRow, "Project (row) tidak ditemukan.");

                        Project projectIdCol = _projectRepository.Get(itemDetail.ProjectIdCol);
                        Validate.NotNull(projectIdCol, "Project (col) tidak ditemukan.");

                        var data = _correlatedProjectDetailRepository.IsExisitOnAdding(correlatedProject.Id, projectIdRow.Id, projectIdCol.Id);
                        if (data == null)
                        {
                            CorrelatedProjectDetail model = new CorrelatedProjectDetail(correlatedProject, projectIdRow.Id, projectIdCol.Id, correlationMatrix, param.CreateBy, param.CreateDate);
                            _correlatedProjectDetailRepository.Insert(model);
                        }
                        else
                        {
                            //Audit Log Update 
                            int audit = _auditLogService.CorrelationProjectDetailAudit(data, correlationMatrix, Convert.ToInt32(param.UpdateBy));

                            data.Update(correlationMatrix, param.CreateBy, param.CreateDate);
                            _correlatedProjectDetailRepository.Update(data);
                        }
                    }
                }

            _databaseContext.SaveChanges();


            return id;
        }
    }
}
