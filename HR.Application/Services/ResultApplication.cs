﻿using System;
using HR.Domain;
using System.Collections.Generic;
using System.Linq;
using HR.Common;
using HR.Application.Params;
using HR.Common.Validation;
using HR.Infrastructure;
using ClosedXML.Excel;
using System.Web;
using System.IO;

namespace HR.Application
{
    public class ResultService : IResultService
    {

        private readonly IDatabaseContext _iDatabaseContext;
        private readonly IRiskMatrixProjectRepository _riskMatrixProjectRepository;
        private readonly IStageTahunRiskMatrixRepository _stageTahunRiskMatrixRepository;
        private readonly IStageTahunRiskMatrixDetailRepository _stageTahunRiskMatrixDetailRepository;
        private readonly IRiskRegistrasiRepository _riskRegistrasiRepository;
        private readonly ILikehoodDetailRepository _likehoodDetailRepository;
        private readonly IScenarioRepository _scenarioRepository;
        private readonly ICorrelatedSektorRepository _correlatedSektorRepository;
        private readonly ICorrelatedProjectRepository _correlatedProjectRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly IScenarioDetailRepository _scenarioDetailRepository;
        private readonly ICorrelatedSektorDetailRepository _correlatedSektorDetailRepository;
        private readonly ICorrelatedProjectDetailRepository _correlatedProjectDetailRepository;
        private readonly ISektorRepository _sektorRepository;
        private readonly IAssetDataRepository _assetDataRepository;
        private readonly IPMNRepository _pmnRepository;
        private readonly IScenarioCalculationRepository _scenarioCalculationRepository;
        private readonly IAvailableCapitalProjectedRepository _availableCapitalProjectedRepository;
        private readonly IProjectCalculationRepository _projectCalculationRepository;
        private readonly ICalculationService _calculationService;


        public ResultService(IDatabaseContext iDatabaseContext, IRiskMatrixProjectRepository riskMatrixProjectRepository, IStageTahunRiskMatrixRepository stageTahunRiskMatrixRepository, IStageTahunRiskMatrixDetailRepository stageTahunRiskMatrixDetailRepository,
            IRiskRegistrasiRepository riskRegistrasiRepository, ILikehoodDetailRepository likehoodDetailRepository, IScenarioRepository scenarioRepository, ICorrelatedSektorRepository correlatedSektorRepository, ICorrelatedProjectRepository correlatedProjectRepository,
            IProjectRepository projectRepository, IScenarioDetailRepository scenarioDetailRepository, ICorrelatedSektorDetailRepository correlatedSektorDetailRepository, ICorrelatedProjectDetailRepository correlatedProjectDetailRepository, ISektorRepository sektorRepository,
            IAssetDataRepository assetDataRepository, IPMNRepository pmnRepository, IScenarioCalculationRepository scenarioCalculationRepository, IAvailableCapitalProjectedRepository availableCapitalProjectedRepository, IProjectCalculationRepository projectCalculationRepository,
            ICalculationService calculationService)
        {
            _iDatabaseContext = iDatabaseContext;
            _riskMatrixProjectRepository = riskMatrixProjectRepository;
            _stageTahunRiskMatrixRepository = stageTahunRiskMatrixRepository;
            _stageTahunRiskMatrixDetailRepository = stageTahunRiskMatrixDetailRepository;
            _riskRegistrasiRepository = riskRegistrasiRepository;
            _likehoodDetailRepository = likehoodDetailRepository;
            _scenarioRepository = scenarioRepository;
            _correlatedSektorRepository = correlatedSektorRepository;
            _correlatedProjectRepository = correlatedProjectRepository;
            _projectRepository = projectRepository;
            _scenarioDetailRepository = scenarioDetailRepository;
            _correlatedSektorDetailRepository = correlatedSektorDetailRepository;
            _correlatedProjectDetailRepository = correlatedProjectDetailRepository;
            _sektorRepository = sektorRepository;
            _assetDataRepository = assetDataRepository;
            _pmnRepository = pmnRepository;
            _scenarioCalculationRepository = scenarioCalculationRepository;
            _availableCapitalProjectedRepository = availableCapitalProjectedRepository;
            _projectCalculationRepository = projectCalculationRepository;
            _calculationService = calculationService;
        }
        
        public Result GetAllDataResult(int id, int projectId, int riskRegistrasiId, int sektorId)
        {
            Result result = new Result();
            var scenario = _scenarioRepository.GetDefault();
            result.ScenarioId = scenario.Id;
            result.NamaScenario = scenario.NamaScenario;

            var calculation = _calculationService.GetAllDataCalculationByScenarioId();

            #region declarationIds
            int projId = 0;
            int riskId = 0;
            int sektId = 0;
            if (projectId != 0)
            {
                var project = _projectRepository.Get(projectId);
                projId = project.Id;
            }
            else
            {
                var projectList = _projectRepository.GetAllActive().ToList();
                if (projectList.Count > 0)
                {
                    projId = projectList[0].Id;
                }
                else
                {
                    throw new ApplicationException(string.Format("Daftar proyek yang aktif tidak ditemukan."));
                }
            }
            if (riskRegistrasiId != 0)
            {
                var risk = _riskRegistrasiRepository.Get(riskRegistrasiId);
                riskId = risk.Id;
            }
            else
            {
                var riskList = _riskRegistrasiRepository.GetAll().ToList();
                riskId = riskList[0].Id;
            }
            if (sektorId != 0)
            {
                var sektor = _sektorRepository.Get(sektorId);
                sektId = sektor.Id;
            }
            else
            {
                var sektorList = _sektorRepository.GetAll().ToList();
                sektId = sektorList[0].Id;
            }
            #endregion declarationIds

            //var scenarioDefault = _scenarioRepository.GetDefault();
            //var calculationResult = GetAllDataCalculationByScenarioId(scenarioDefault.Id);

            //DiversifiedBenefitAchieved
            Validate.NotNull(calculation.CalculationResult, "Data kalkulasi tidak ditemukan.");
            DiversifiedBenefitAchieved diversifiedBenefitAchieved = new DiversifiedBenefitAchieved();
            var diversifiedBenefitAchievedBySektor = GetDiversifiedBenefitAchievedBySektor(calculation.CalculationResult.AggregationSektor.Sektor, calculation.CalculationResult.AggregationSektor);
            var diversifiedBenefitAchievedByProject = GetDiversifiedBenefitAchievedByProject(calculation.CalculationResult.AggregationOfProject.AggregationOfProjectCollection, calculation.CalculationResult.AggregationOfProject);
            var diversifiedBenefitAchievedCollection = GetDiversifiedBenefitAchievedCollection(calculation.CalculationResult.AggregationOfProject.AggregationOfProjectCollection, calculation.CalculationResult.AggregationOfProject);
            diversifiedBenefitAchieved.DiversifiedBenefitAchievedBySektor = diversifiedBenefitAchievedBySektor;
            diversifiedBenefitAchieved.DiversifiedBenefitAchievedByProject = diversifiedBenefitAchievedByProject;
            diversifiedBenefitAchieved.DiversifiedBenefitAchievedCollection = diversifiedBenefitAchievedCollection;
            result.DiversifiedBenefitAchieved = diversifiedBenefitAchieved;

            //LeverageItem
            var leverageItem = GenerateLevaregeRation(calculation.CalculationResult);
            result.LeverageRatio = leverageItem;

            //AvailableVSRiskCapital
            var availableVS = GenerateAvailableVSRiskCapital(calculation.CalculationResult);
            result.AvailableVSRiskCapital = availableVS;

            //Liquidity
            var liquidty = GenerateLiquidity(calculation.CalculationResult);
            result.Liquidity = liquidty;

            //Risk Budget
            var riskBudget = GenerateRiskBudget(calculation.CalculationResult, projId, riskId, sektId);
            result.RiskBudget = riskBudget;

            //SensitivityStressTesting
            SensitivityStressTesting sensitivityStressTesting = new SensitivityStressTesting();
            var sensitivityResult = GetSensitivityResult(calculation, result, calculation.CalculationResult);
            var stressTestingResult = GetStressTestingResult(calculation, result, calculation.CalculationResult);
            sensitivityStressTesting.SensitivityResult = sensitivityResult;
            sensitivityStressTesting.StressTestingResult = stressTestingResult;
            result.SensitivityStressTesting = sensitivityStressTesting;

            result.ProjectMonitoring = calculation.CalculationResult.ProjectMonitoring;
            result.SektorMonitoring = calculation.CalculationResult.SektorMonitoring;

            return result;
        }

        #region 1. Diversified Benefit Achieved

        public DiversifiedBenefitAchievedBySektor GetDiversifiedBenefitAchievedBySektor(IList<SektorLite> sektors, AggregationSektor aggregationSektor)
        {
            DiversifiedBenefitAchievedBySektor diversifiedBenefitAchievedBySektor = new DiversifiedBenefitAchievedBySektor();
            if (sektors.Count() > 0)
            {
                IList<YearSektorCollection> yearSektorCollections = new List<YearSektorCollection>();

                for (int i = 0; i < aggregationSektor.UndiversifiedAggregationSektor.YearCollection.Count(); i++)
                {
                    IList<YearSektorValue> yearSektorValues = new List<YearSektorValue>();
                    YearSektorCollection yearSektorCollection = new YearSektorCollection();
                    yearSektorCollection.Year = aggregationSektor.UndiversifiedAggregationSektor.YearCollection[i].Year;
                    yearSektorCollection.Total = aggregationSektor.UndiversifiedAggregationSektor.YearCollection[i].Total - aggregationSektor.InterProjectDiversifiedAggregationSektor.YearCollection[i].Total;

                    for (int j = 0; j < aggregationSektor.UndiversifiedAggregationSektor.YearCollection[i].YearValues.Count(); j++)
                    {
                        var sektor = sektors.Where(x => x.SektorId == aggregationSektor.UndiversifiedAggregationSektor.YearCollection[i].YearValues[j].SektorId).FirstOrDefault();
                        var sektorName = sektor.NamaSektor;
                        YearSektorValue yearSektorValue = new YearSektorValue();
                        yearSektorValue.SektorId = aggregationSektor.UndiversifiedAggregationSektor.YearCollection[i].YearValues[j].SektorId;
                        yearSektorValue.NamaSektor = sektorName;
                        yearSektorValue.Value = aggregationSektor.UndiversifiedAggregationSektor.YearCollection[i].YearValues[j].Value - aggregationSektor.InterProjectDiversifiedAggregationSektor.YearCollection[i].YearValues[j].Value;
                        yearSektorValues.Add(yearSektorValue);
                    }
                    yearSektorCollection.YearSektorValue = yearSektorValues.ToArray();
                    yearSektorCollections.Add(yearSektorCollection);
                }
                diversifiedBenefitAchievedBySektor.SektorDiversifiedBenefitAchieved = sektors.ToArray();
                diversifiedBenefitAchievedBySektor.YearSektorCollection = yearSektorCollections.ToArray();
            }
            return diversifiedBenefitAchievedBySektor;
        }

        public DiversifiedBenefitAchievedByProject GetDiversifiedBenefitAchievedByProject(IList<AggregationOfProjectCollection> projects, AggregationOfProject aggregationOfProject)
        {
            DiversifiedBenefitAchievedByProject diversifiedBenefitAchievedByProject = new DiversifiedBenefitAchievedByProject();
            if (projects.Count() > 0)
            {
                IList<YearProjectCollection> yearProjectCollections = new List<YearProjectCollection>();

                for (int i = 0; i < aggregationOfProject.AggregationUndiversifiedProjectCapital.Count(); i++)
                {
                    IList<YearProjectValue> yearProjectValues = new List<YearProjectValue>();
                    YearProjectCollection yearProjectCollection = new YearProjectCollection();
                    yearProjectCollection.Year = aggregationOfProject.AggregationUndiversifiedProjectCapital[i].aggregationYears;
                    yearProjectCollection.Total = aggregationOfProject.AggregationUndiversifiedProjectCapital[i].TotalPerYear - aggregationOfProject.AggregationInterDiversifiedProjectCapital[i].TotalPerYear;

                    for (int j = 0; j < aggregationOfProject.AggregationUndiversifiedProjectCapital[i].AggregationUndiversifiedProjectCollection.Count(); j++)
                    {
                        YearProjectValue yearProjectValue = new YearProjectValue();
                        yearProjectValue.ProjectId = aggregationOfProject.AggregationUndiversifiedProjectCapital[i].AggregationUndiversifiedProjectCollection[j].ProjectId;
                        yearProjectValue.Value = aggregationOfProject.AggregationUndiversifiedProjectCapital[i].AggregationUndiversifiedProjectCollection[j].Total - aggregationOfProject.AggregationInterDiversifiedProjectCapital[i].AggregationInterDiversifiedProjectCollection[j].Total;
                        yearProjectValues.Add(yearProjectValue);
                    }
                    yearProjectCollection.YearProjectValue = yearProjectValues.ToArray();
                    yearProjectCollections.Add(yearProjectCollection);
                }
                diversifiedBenefitAchievedByProject.ProjectDiversifiedBenefitAchieved = projects.ToArray();
                diversifiedBenefitAchievedByProject.YearProjectCollection = yearProjectCollections.ToArray();
            }
            return diversifiedBenefitAchievedByProject;
        }

        public DiversifiedBenefitAchievedCollection GetDiversifiedBenefitAchievedCollection(IList<AggregationOfProjectCollection> projects, AggregationOfProject aggregationOfProject)
        {
            DiversifiedBenefitAchievedCollection diversifiedBenefitAchievedCollection = new DiversifiedBenefitAchievedCollection();
            if (projects.Count() > 0)
            {
                IList<UndiversifiedBenefit> undiversifiedBenefits = new List<UndiversifiedBenefit>();
                foreach (var undiversified in aggregationOfProject.AggregationUndiversifiedProjectCapital)
                {
                    UndiversifiedBenefit undiversifiedBenefit = new UndiversifiedBenefit();
                    undiversifiedBenefit.Year = undiversified.aggregationYears;
                    undiversifiedBenefit.Total = undiversified.TotalPerYear;
                    undiversifiedBenefits.Add(undiversifiedBenefit);
                }
                IList<DiversifiedBenefit> diversifiedBenefits = new List<DiversifiedBenefit>();
                foreach (var diversified in aggregationOfProject.AggregationInterDiversifiedProjectCapital)
                {
                    DiversifiedBenefit diversifiedBenefit = new DiversifiedBenefit();
                    diversifiedBenefit.Year = diversified.aggregationYears;
                    diversifiedBenefit.Total = diversified.TotalPerYear;
                    diversifiedBenefits.Add(diversifiedBenefit);
                }
                IList<InterProjectBenefit> interProjectBenefits = new List<InterProjectBenefit>();
                for (int i = 0; i < undiversifiedBenefits.Count; i++)
                {
                    InterProjectBenefit interProjectBenefit = new InterProjectBenefit();
                    interProjectBenefit.Year = undiversifiedBenefits[i].Year;
                    interProjectBenefit.Total = undiversifiedBenefits[i].Total - diversifiedBenefits[i].Total;
                    interProjectBenefits.Add(interProjectBenefit);
                }
                diversifiedBenefitAchievedCollection.UndiversifiedBenefitAchieved = undiversifiedBenefits.ToArray();
                diversifiedBenefitAchievedCollection.DiversifiedBenefitAchieved = diversifiedBenefits.ToArray();
                diversifiedBenefitAchievedCollection.InterProjectBenefit = interProjectBenefits.ToArray();
            }
            return diversifiedBenefitAchievedCollection;
        }

        #endregion 1. Diversified Benefit Achieved

        #region 2. Leverage Ratios
        public LeverageRatio GenerateLevaregeRation(CalculationResult calculationResult)
        {
            LeverageRatio result = new LeverageRatio();
            string[] listName = { "Total Undiversified Risk Capital", "Total Diversified Risk Capital", "Available Capital", "Total Maximum Exposure", "Diversification Ratio", "Mitigation Effectiveness", "Leveratio Ratio" };
            #region LeverageRatio
            #region leverageItem
            IList<LeverageItem> nameList = new List<LeverageItem>();
            foreach (var item in listName)
            {
                LeverageItem name = new LeverageItem();
                switch (item)
                {
                    //for Total Undiversified Risk Capital only
                    case "Total Undiversified Risk Capital":
                        IList<ItemValue> undiValueList = new List<ItemValue>();
                        //var year = calculationResult.AssetProjection.SummaryAssetProjection.FirstOrDefault(x => x.SummaryName == "Total Undiversified Capital");
                        //Validate.NotNull(year, "Data undiversified risk capital tidak ditemukan.");
                        foreach (var itemYear in calculationResult.AssetProjectionLiquidity.AssetSummaryCollection)
                        {
                            ItemValue undiValue = new ItemValue();
                            undiValue.Name = item;
                            undiValue.Year = itemYear.Year;
                            undiValue.Value = itemYear.TotalUndiversifiedCapital;
                            undiValueList.Add(undiValue);
                        }
                        name.Name = item;
                        name.Values = undiValueList.ToArray();
                        nameList.Add(name);
                        break;

                    // for Total Diversified Risk Capital only
                    case "Total Diversified Risk Capital":
                        IList<ItemValue> diValueList = new List<ItemValue>();
                        //var yearDiv = calculationResult.AssetProjection.SummaryAssetProjection.FirstOrDefault(x => x.SummaryName == "Total Diversified Capital (Risk Capital)");
                        //Validate.NotNull(yearDiv, "Data diversified risk capital tidak ditemukan.");
                        foreach (var itemDiv in calculationResult.AssetProjectionLiquidity.AssetSummaryCollection)
                        {
                            ItemValue diValue = new ItemValue();
                            diValue.Name = item;
                            diValue.Year = itemDiv.Year;
                            diValue.Value = itemDiv.TotalDiversifiedCapital;
                            diValueList.Add(diValue);
                        }
                        name.Name = item;
                        name.Values = diValueList.ToArray();
                        nameList.Add(name);
                        break;

                    //for Available Capital
                    case "Available Capital":
                        IList<ItemValue> availValueList = new List<ItemValue>();
                        var dataAvailable = calculationResult.AssetProjectionLiquidity.AssetProjectionIlustration;
                        Validate.NotNull(dataAvailable, "Data available capital after recourse tidak ditemukan.");

                        foreach (var itemAva in dataAvailable)
                        {
                            Validate.NotNull(itemAva, "Data available capital after recourse tahun {0} tidak ditemukan.", itemAva.Year);

                            ItemValue itVal = new ItemValue();
                            itVal.Name = item;
                            itVal.Value = itemAva.AvailableCapitalRecourse;
                            itVal.Year = itemAva.Year;
                            availValueList.Add(itVal);
                        }
                        name.Name = item;
                        name.Values = availValueList.ToArray();
                        nameList.Add(name);
                        break;

                    //for Total Maximum Exposure
                    case "Total Maximum Exposure":
                        //kasih nilai 0 dulu karena gak tau kenapa di excel cuma ngambil riskmatrix utk proyek Toll Balikpapan - Samarinda
                        var scenario = _scenarioRepository.GetDefault();
                        var riskMatrixProject = _riskMatrixProjectRepository.GetByScenarioId(scenario.Id);
                        //var stageTahunRiskMatrix = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectId(riskMatrixProject.Id).ToList();
                        IList<ItemValue> expValueList = new List<ItemValue>();
                        Validate.NotNull(riskMatrixProject, "Risk Matrix Project tidak ditemukan.");
                        foreach (var year in calculationResult.CollectionYears)
                        {
                            ItemValue diValue = new ItemValue();
                            decimal totalMaxExpose = 0;
                            foreach (var project in riskMatrixProject)
                            {
                                var stageTahunRiskMatrix = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectId(project.Id).ToList();
                                foreach (var riskMatrix in stageTahunRiskMatrix)
                                {
                                    if (riskMatrix.Tahun == year)
                                    {
                                        if (riskMatrix.MaximumNilaiExpose == null)
                                        {
                                            totalMaxExpose += 0;
                                        }
                                        if (riskMatrix.MaximumNilaiExpose != null)
                                        {
                                            totalMaxExpose += riskMatrix.MaximumNilaiExpose.Value;
                                        }
                                    }
                                }
                            }
                            diValue.Name = item;
                            diValue.Year = year;
                            diValue.Value = totalMaxExpose;
                            expValueList.Add(diValue);
                        }
                        name.Name = item;
                        name.Values = expValueList.ToArray();
                        nameList.Add(name);
                        break;

                    //for Diversification Ratio only
                    case "Diversification Ratio":
                        var undiv = nameList.FirstOrDefault(x => x.Name == "Total Undiversified Risk Capital");
                        Validate.NotNull(undiv, "Data undiversified risk capital tidak ditemukan.");
                        var diver = nameList.FirstOrDefault(x => x.Name == "Total Diversified Risk Capital");
                        Validate.NotNull(diver, "Data diversified risk capital tidak ditemukan.");
                        IList<ItemValue> ratioList = new List<ItemValue>();
                        foreach (var index in undiv.Values)
                        {
                            var divByYear = diver.Values.FirstOrDefault(x => x.Year == index.Year);
                            ItemValue rat = new ItemValue();
                            rat.Name = item;
                            rat.Year = index.Year;
                            if (divByYear.Value == 0)
                            {
                                rat.Value = 0;
                            }
                            else
                            {
                                var temp = index.Value / divByYear.Value;
                                rat.Value = temp;
                            }
                            ratioList.Add(rat);
                        }
                        name.Name = item;
                        name.Values = ratioList.ToArray();
                        nameList.Add(name);
                        break;

                    //for Mitigation Effectiveness only
                    case "Mitigation Effectiveness":
                        var maxExp = nameList.FirstOrDefault(x => x.Name == "Total Maximum Exposure");
                        Validate.NotNull(maxExp, "Data total maximum exposure tidak ditemukan.");
                        var diverM = nameList.FirstOrDefault(x => x.Name == "Total Diversified Risk Capital");
                        Validate.NotNull(diverM, "Data diversified risk capital tidak ditemukan.");
                        IList<ItemValue> mitigationList = new List<ItemValue>();
                        foreach (var index in maxExp.Values)
                        {
                            var divByYear = diverM.Values.FirstOrDefault(x => x.Year == index.Year);
                            Validate.NotNull(divByYear, "Data diversified risk capital untuk tahun {0} tidak ditemukan.", index.Year);
                            ItemValue mit = new ItemValue();
                            mit.Name = item;
                            mit.Year = index.Year;
                            if (divByYear.Value == 0)
                            {
                                mit.Value = 0;
                            }
                            else
                            {
                                var temp = index.Value / divByYear.Value;
                                mit.Value = temp;
                            }
                            mitigationList.Add(mit);
                        }
                        name.Name = item;
                        name.Values = mitigationList.ToArray();
                        nameList.Add(name);
                        break;

                    //for Leveratio Ratio only
                    case "Leveratio Ratio":
                        var avaiCap = nameList.FirstOrDefault(x => x.Name == "Available Capital");
                        Validate.NotNull(avaiCap, "Data available capital tidak ditemukan.");
                        var diversified = nameList.FirstOrDefault(x => x.Name == "Total Diversified Risk Capital");
                        Validate.NotNull(diversified, "Data total diversified risk capital tidak ditemukan.");
                        IList<ItemValue> leveratioist = new List<ItemValue>();
                        foreach (var itemLever in avaiCap.Values)
                        {
                            var leverPerYear = diversified.Values.FirstOrDefault(x => x.Year == itemLever.Year);
                            Validate.NotNull(leverPerYear, "Data total diversified risk capital untuk tahun {0} tidak ditemukan.", itemLever.Year);
                            ItemValue rat = new ItemValue();
                            rat.Name = item;
                            rat.Year = itemLever.Year;
                            if (leverPerYear.Value == 0)
                            {
                                rat.Value = 0;
                            }
                            else
                            {
                                var temp = itemLever.Value / leverPerYear.Value;
                                rat.Value = temp;
                            }
                            leveratioist.Add(rat);
                        }
                        name.Name = item;
                        name.Values = leveratioist.ToArray();
                        nameList.Add(name);
                        break;
                }
            }
            #endregion leverageItem
            #region yearCollection
            IList<YearCollectionLeverage> yearList = new List<YearCollectionLeverage>();
            if (calculationResult.CollectionYears.Length > 0)
            {
                foreach (var item in calculationResult.CollectionYears)
                {
                    YearCollectionLeverage year = new YearCollectionLeverage();
                    year.Year = item;
                    yearList.Add(year);
                }
            }
            else
            {
                throw new ApplicationException(string.Format("Generate tahun gagal / data tidak ditemukan."));
            }
            #endregion yearCollection
            #endregion LeverageRatio;

            result.LeverageItem = nameList.ToArray();
            result.YearCollectionLeverage = yearList.ToArray();
            return result;
        }
        #endregion 2. Leverage Ratios

        #region 3. AvailableVSRiskCapital
        public AvailableVSRiskCapital GenerateAvailableVSRiskCapital(CalculationResult calculationResult)
        {
            AvailableVSRiskCapital result = new AvailableVSRiskCapital();
            string[] listName = { "Available Capital", "Undiversified Risk Capital", "Diversified Risk Capital", "Total Maximum Exposure" };
            #region itemCollection
            IList<AvailableVSRiskCapitalCollection> itemCollection = new List<AvailableVSRiskCapitalCollection>();
            foreach (var itemName in listName)
            {
                AvailableVSRiskCapitalCollection itemAvailable = new AvailableVSRiskCapitalCollection();
                switch (itemName)
                {
                    case "Available Capital":
                        IList<AvailableVSRiskCapitalItem> itemVS = new List<AvailableVSRiskCapitalItem>();
                        //var year = calculationResult.AssetProjection.SummaryAssetProjection.FirstOrDefault(x => x.SummaryName == "Available capital Projected (allowing for reinvestment)");
                        //Validate.NotNull(year, "Data available capital project tidak ditemukan.");
                        foreach (var itemYear in calculationResult.AssetProjectionLiquidity.AssetSummaryCollection)
                        {
                            AvailableVSRiskCapitalItem avaiValue = new AvailableVSRiskCapitalItem();
                            avaiValue.Name = itemName;
                            avaiValue.Year = itemYear.Year;
                            avaiValue.Value = itemYear.AvailableCapitalProjected;
                            itemVS.Add(avaiValue);
                        }
                        itemAvailable.Name = itemName;
                        itemAvailable.Values = itemVS.ToArray();
                        itemCollection.Add(itemAvailable);
                        break;
                    case "Undiversified Risk Capital":
                        IList<AvailableVSRiskCapitalItem> undiValueList = new List<AvailableVSRiskCapitalItem>();
                        //var yearUndiv = calculationResult.AssetProjection.SummaryAssetProjection.FirstOrDefault(x => x.SummaryName == "Total Undiversified Capital");
                        //Validate.NotNull(yearUndiv, "Data undiversified risk capital tidak ditemukan.");
                        foreach (var itemYear in calculationResult.AssetProjectionLiquidity.AssetSummaryCollection)
                        {
                            AvailableVSRiskCapitalItem undiValue = new AvailableVSRiskCapitalItem();
                            undiValue.Name = itemName;
                            undiValue.Year = itemYear.Year;
                            undiValue.Value = itemYear.TotalUndiversifiedCapital;
                            undiValueList.Add(undiValue);
                        }
                        itemAvailable.Name = itemName;
                        itemAvailable.Values = undiValueList.ToArray();
                        itemCollection.Add(itemAvailable);
                        break;
                    case "Diversified Risk Capital":
                        IList<AvailableVSRiskCapitalItem> diValueList = new List<AvailableVSRiskCapitalItem>();
                        //var yearDiv = calculationResult.AssetProjection.SummaryAssetProjection.FirstOrDefault(x => x.SummaryName == "Total Diversified Capital (Risk Capital)");
                        //Validate.NotNull(yearDiv, "Data diversified risk capital tidak ditemukan.");
                        foreach (var itemDiv in calculationResult.AssetProjectionLiquidity.AssetSummaryCollection)
                        {
                            AvailableVSRiskCapitalItem diValue = new AvailableVSRiskCapitalItem();
                            diValue.Name = itemName;
                            diValue.Year = itemDiv.Year;
                            diValue.Value = itemDiv.TotalDiversifiedCapital;
                            diValueList.Add(diValue);
                        }
                        itemAvailable.Name = itemName;
                        itemAvailable.Values = diValueList.ToArray();
                        itemCollection.Add(itemAvailable);
                        break;

                    case "Total Maximum Exposure":
                        //kasih nilai 0 dulu karena gak tau kenapa di excel cuma ngambil riskmatrix utk proyek Toll Balikpapan - Samarinda
                        var scenario = _scenarioRepository.GetDefault();
                        var riskMatrixProject = _riskMatrixProjectRepository.GetByScenarioId(scenario.Id);
                        //var stageTahunRiskMatrix = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectId(riskMatrixProject.Id).ToList();
                        IList<AvailableVSRiskCapitalItem> expValueList = new List<AvailableVSRiskCapitalItem>();
                        Validate.NotNull(riskMatrixProject, "Risk Matrix Project tidak ditemukan.");
                        foreach (var year in calculationResult.CollectionYears)
                        {
                            AvailableVSRiskCapitalItem diValue = new AvailableVSRiskCapitalItem();
                            decimal totalMaxExpose = 0;
                            foreach (var project in riskMatrixProject)
                            {
                                var stageTahunRiskMatrix = _stageTahunRiskMatrixRepository.GetByRiskMatrixProjectId(project.Id).ToList();
                                foreach (var riskMatrix in stageTahunRiskMatrix)
                                {
                                    if (riskMatrix.Tahun == year)
                                    {
                                        if (riskMatrix.MaximumNilaiExpose == null)
                                        {
                                            totalMaxExpose += 0;
                                        }
                                        if (riskMatrix.MaximumNilaiExpose != null)
                                        {
                                            totalMaxExpose += riskMatrix.MaximumNilaiExpose.Value;
                                        }
                                    }
                                }
                            }
                            diValue.Name = itemName;
                            diValue.Year = year;
                            diValue.Value = totalMaxExpose;
                            expValueList.Add(diValue);
                        }
                        itemAvailable.Name = itemName;
                        itemAvailable.Values = expValueList.ToArray();
                        itemCollection.Add(itemAvailable);
                        break;
                }
            }
            #endregion itemCollection
            #region yearCollection
            IList<AvailableVSRiskCapitalYearCollection> yearList = new List<AvailableVSRiskCapitalYearCollection>();
            if (calculationResult.CollectionYears.Length > 0)
            {
                foreach (var item in calculationResult.CollectionYears)
                {
                    AvailableVSRiskCapitalYearCollection year = new AvailableVSRiskCapitalYearCollection();
                    year.Year = item;
                    yearList.Add(year);
                }
            }
            else
            {
                throw new ApplicationException(string.Format("Generate tahun gagal / data tidak ditemukan."));
            }
            #endregion yearCollection

            result.AvailableVSRiskCapitalCollection = itemCollection.ToArray();
            result.AvailableVSRiskCapitalYearCollection = yearList.ToArray();

            return result;
        }
        #endregion 3. AvailableVSRiskCapital

        #region 4. Liquidity
        public Liquidity GenerateLiquidity(CalculationResult calculationResult)
        {
            Liquidity result = new Liquidity();
            string[] listName = { "Risk Capital", "Liquidity" };
            #region itemCollection
            IList<LiquidityCollection> itemCollection = new List<LiquidityCollection>();
            foreach (var itemName in listName)
            {
                LiquidityCollection itemAvailable = new LiquidityCollection();
                switch (itemName)
                {
                    case "Risk Capital":
                        IList<LiquidityItem> itemVS = new List<LiquidityItem>();
                        //var year = calculationResult.AssetProjection.SummaryAssetProjection.FirstOrDefault(x => x.SummaryName == "Total Diversified Capital (Risk Capital)");
                        //Validate.NotNull(year, "Total Diversified Capital(Risk Capital) tidak ditemukan.");
                        foreach (var itemYear in calculationResult.AssetProjectionLiquidity.AssetSummaryCollection)
                        {
                            LiquidityItem avaiValue = new LiquidityItem();
                            avaiValue.Name = itemName;
                            avaiValue.Year = itemYear.Year;
                            avaiValue.Value = itemYear.TotalDiversifiedCapital;
                            itemVS.Add(avaiValue);
                        }
                        itemAvailable.Name = itemName;
                        itemAvailable.Values = itemVS.ToArray();
                        itemCollection.Add(itemAvailable);
                        break;
                    case "Liquidity":
                        IList<LiquidityItem> undiValueList = new List<LiquidityItem>();
                        var yearUndiv = calculationResult.AssetProjectionLiquidity;
                        Validate.NotNull(yearUndiv, "Data undiversified risk capital tidak ditemukan.");
                        foreach (var itemYear in yearUndiv.LiquidAsset)
                        {
                            LiquidityItem undiValue = new LiquidityItem();
                            undiValue.Name = itemName;
                            undiValue.Year = itemYear.Year;
                            undiValue.Value = itemYear.Value;
                            undiValueList.Add(undiValue);
                        }
                        itemAvailable.Name = itemName;
                        itemAvailable.Values = undiValueList.ToArray();
                        itemCollection.Add(itemAvailable);
                        break;
                }
            }
            #endregion itemCollection
            #region yearCollection
            IList<LiquidityYearCollection> yearList = new List<LiquidityYearCollection>();
            if (calculationResult.CollectionYears.Length > 0)
            {
                foreach (var item in calculationResult.CollectionYears)
                {
                    LiquidityYearCollection year = new LiquidityYearCollection();
                    year.Year = item;
                    yearList.Add(year);
                }
            }
            else
            {
                throw new ApplicationException(string.Format("Generate tahun gagal / data tidak ditemukan."));
            }
            #endregion yearCollection

            result.LiquidityCollection = itemCollection.ToArray();
            result.LiquidityYearCollection = yearList.ToArray();
            return result;
        }
        #endregion 4. Liquidity

        #region 5. Sensitivity & Stress Testing

        public SensitivityStress GetSensitivityResult(Calculation calculation, Result result, CalculationResult calculationResult)
        {
            SensitivityStress sensitivityResult = new SensitivityStress();
            string[] listName = { "Risk Capital", "Available Capital", "Liquidity" };

            #region itemCollection
            IList<SensitivityStressCollection> itemCollection = new List<SensitivityStressCollection>();
            IList<SensitivityStressScenarioCollection> itemCollectionScenario = new List<SensitivityStressScenarioCollection>();
            foreach (var itemName in listName)
            {
                SensitivityStressCollection itemAvailable = new SensitivityStressCollection();
                switch (itemName)
                {
                    case "Risk Capital":
                        var year = calculation.ScenarioTesting.Sensitivity.Diversified;
                        foreach (var scenario in year)
                        {
                            SensitivityStressScenarioCollection itemAvailableScenario = new SensitivityStressScenarioCollection();
                            IList<SensitivityStressItem> itemVS = new List<SensitivityStressItem>();
                            foreach (var itemYear in scenario.MidValue)
                            {
                                SensitivityStressItem avaiValue = new SensitivityStressItem();
                                avaiValue.Name = itemName;
                                avaiValue.Year = itemYear.Year;
                                avaiValue.Value = itemYear.Value.Value;
                                itemVS.Add(avaiValue);
                            }
                            itemAvailableScenario.Name = itemName;
                            itemAvailableScenario.NamaScenario = scenario.NamaScenario;
                            itemAvailableScenario.ScenarioId = scenario.ScenarioId;
                            itemAvailableScenario.Values = itemVS.ToArray();
                            itemCollectionScenario.Add(itemAvailableScenario);
                        }
                        break;
                    case "Available Capital":
                        IList<SensitivityStressItem> itemAC = new List<SensitivityStressItem>();
                        //var yearAC = calculationResult.AssetProjection.SummaryAssetProjection.FirstOrDefault(x => x.SummaryName == "Available capital Projected (allowing for reinvestment)");
                        //Validate.NotNull(yearAC, "Data available capital project tidak ditemukan.");
                        foreach (var itemYear in calculationResult.AssetProjectionLiquidity.AssetSummaryCollection)
                        {
                            SensitivityStressItem avaiValue = new SensitivityStressItem();
                            avaiValue.Name = itemName;
                            avaiValue.Year = itemYear.Year;
                            avaiValue.Value = itemYear.AvailableCapitalProjected;
                            itemAC.Add(avaiValue);
                        }
                        itemAvailable.Name = itemName;
                        itemAvailable.Values = itemAC.ToArray();
                        itemCollection.Add(itemAvailable);
                        break;
                    case "Liquidity":
                        IList<SensitivityStressItem> undiValueList = new List<SensitivityStressItem>();
                        var yearUndiv = calculationResult.AssetProjectionLiquidity;
                        Validate.NotNull(yearUndiv, "Data undiversified risk capital tidak ditemukan.");
                        foreach (var itemYear in yearUndiv.LiquidAsset)
                        {
                            SensitivityStressItem undiValue = new SensitivityStressItem();
                            undiValue.Name = itemName;
                            undiValue.Year = itemYear.Year;
                            undiValue.Value = itemYear.Value;
                            undiValueList.Add(undiValue);
                        }
                        itemAvailable.Name = itemName;
                        itemAvailable.Values = undiValueList.ToArray();
                        itemCollection.Add(itemAvailable);
                        break;
                }
            }
            #endregion itemCollection

            #region yearCollection
            IList<SensitivityStressYearCollection> yearList = new List<SensitivityStressYearCollection>();
            if (calculationResult.CollectionYears.Length > 0)
            {
                foreach (var item in calculationResult.CollectionYears)
                {
                    SensitivityStressYearCollection year = new SensitivityStressYearCollection();
                    year.Year = item;
                    yearList.Add(year);
                }
            }
            else
            {
                throw new ApplicationException(string.Format("Generate tahun gagal / data tidak ditemukan."));
            }
            #endregion yearCollection

            sensitivityResult.SensitivityStressCollection = itemCollection.ToArray();
            sensitivityResult.SensitivityStressScenarioCollection = itemCollectionScenario.ToArray();
            sensitivityResult.SensitivityStressYearCollection = yearList.ToArray();

            return sensitivityResult;
        }

        public StressTestingResult GetStressTestingResult(Calculation calculation, Result result, CalculationResult calculationResult)
        {
            StressTestingResult stressTestingResult = new StressTestingResult();
            string[] listName = { "Risk Capital", "Available Capital", "Liquidity" };

            #region itemCollection
            IList<SensitivityStressCollection> itemCollection = new List<SensitivityStressCollection>();
            IList<SensitivityStressScenarioCollection> itemCollectionRC = new List<SensitivityStressScenarioCollection>();
            IList<SensitivityStressScenarioCollection> itemCollectionAC = new List<SensitivityStressScenarioCollection>();
            foreach (var itemName in listName)
            {
                SensitivityStressCollection itemAvailable = new SensitivityStressCollection();
                switch (itemName)
                {
                    case "Risk Capital":
                        var year = calculation.ScenarioTesting.StressTesting.RiskCapital;
                        foreach (var scenario in year)
                        {
                            SensitivityStressScenarioCollection itemAvailableScenario = new SensitivityStressScenarioCollection();
                            IList<SensitivityStressItem> itemVS = new List<SensitivityStressItem>();
                            foreach (var itemYear in scenario.ValuePerYear)
                            {
                                SensitivityStressItem avaiValue = new SensitivityStressItem();
                                avaiValue.Name = itemName;
                                avaiValue.Year = itemYear.Year;
                                avaiValue.Value = itemYear.Value.Value;
                                itemVS.Add(avaiValue);
                            }
                            itemAvailableScenario.Name = itemName;
                            itemAvailableScenario.NamaScenario = scenario.NamaScenario;
                            itemAvailableScenario.ScenarioId = scenario.ScenarioId;
                            itemAvailableScenario.Values = itemVS.ToArray();
                            itemCollectionRC.Add(itemAvailableScenario);
                        }
                        break;
                    case "Available Capital":
                        var yearAC = calculation.ScenarioTesting.StressTesting.AvailableCapital;
                        foreach (var scenario in yearAC)
                        {
                            SensitivityStressScenarioCollection itemAvailableScenario = new SensitivityStressScenarioCollection();
                            IList<SensitivityStressItem> itemVS = new List<SensitivityStressItem>();
                            foreach (var itemYear in scenario.ValuePerYear)
                            {
                                SensitivityStressItem avaiValue = new SensitivityStressItem();
                                avaiValue.Name = itemName;
                                avaiValue.Year = itemYear.Year;
                                avaiValue.Value = itemYear.Value.Value;
                                itemVS.Add(avaiValue);
                            }
                            itemAvailableScenario.Name = itemName;
                            itemAvailableScenario.NamaScenario = scenario.NamaScenario;
                            itemAvailableScenario.ScenarioId = scenario.ScenarioId;
                            itemAvailableScenario.Values = itemVS.ToArray();
                            itemCollectionAC.Add(itemAvailableScenario);
                        }
                        break;
                    case "Liquidity":
                        IList<SensitivityStressItem> undiValueList = new List<SensitivityStressItem>();
                        var yearUndiv = calculationResult.AssetProjectionLiquidity;
                        Validate.NotNull(yearUndiv, "Data undiversified risk capital tidak ditemukan.");
                        foreach (var itemYear in yearUndiv.LiquidAsset)
                        {
                            SensitivityStressItem undiValue = new SensitivityStressItem();
                            undiValue.Name = itemName;
                            undiValue.Year = itemYear.Year;
                            undiValue.Value = itemYear.Value;
                            undiValueList.Add(undiValue);
                        }
                        itemAvailable.Name = itemName;
                        itemAvailable.Values = undiValueList.ToArray();
                        itemCollection.Add(itemAvailable);
                        break;
                }
            }
            #endregion itemCollection

            #region yearCollection
            IList<SensitivityStressYearCollection> yearList = new List<SensitivityStressYearCollection>();
            if (calculationResult.CollectionYears.Length > 0)
            {
                foreach (var item in calculationResult.CollectionYears)
                {
                    SensitivityStressYearCollection year = new SensitivityStressYearCollection();
                    year.Year = item;
                    yearList.Add(year);
                }
            }
            else
            {
                throw new ApplicationException(string.Format("Generate tahun gagal / data tidak ditemukan."));
            }
            #endregion yearCollection

            stressTestingResult.SensitivityStressCollection = itemCollection.ToArray();
            stressTestingResult.RiskCapitalCollection = itemCollectionRC.ToArray();
            stressTestingResult.AvailableCapitalCollection = itemCollectionAC.ToArray();
            stressTestingResult.SensitivityStressYearCollection = yearList.ToArray();
            return stressTestingResult;
        }

        #endregion 5. Sensitivity & Stress Testing

        #region 6. Risk Budget
        public RiskBudget GenerateRiskBudget(CalculationResult calculationResult, int projectId, int riskRegistrasiId, int sektorId)
        {
            RiskBudget result = new RiskBudget();
            RiskBudgetByProject byProject = new RiskBudgetByProject();
            RiskBudgetByRisk byRisk = new RiskBudgetByRisk();
            RiskBudgetBySektor bySektor = new RiskBudgetBySektor();
            IList<RiskBudgetYearCollection> yearList = new List<RiskBudgetYearCollection>();

            string[] listName = { "Entity", "Minimum Risk Percentage", "Maximum Risk Percentage" };

            //by Project
            #region byProject
            #region itemCollection
            var projects = _projectRepository.GetAllActive().ToList();
            //var projectId = projects[0].Id;
            var projectSelected = projects.Where(x => x.Id == projectId).FirstOrDefault();
            IList<RiskBudgetCollection> itemCollection = new List<RiskBudgetCollection>();
            foreach (var item in listName)
            {
                RiskBudgetCollection itemRS = new RiskBudgetCollection();
                var years = calculationResult.AggregationOfProject.AggregationInterDiversifiedProjectCapital;
                Validate.NotNull(years, "Aggregation Inter Project Diversified tidak ditemukan.");
                switch (item)
                {
                    case "Entity":
                        IList<RiskBudgetItem> riskBugetCollection = new List<RiskBudgetItem>();
                        foreach (var itemYear in years)
                        {
                            RiskBudgetItem riskBugetItem = new RiskBudgetItem();
                            var data = itemYear.AggregationInterDiversifiedProjectCollection.Where(x => x.ProjectId == projectId).FirstOrDefault();
                            decimal? firstStep = 0;
                            if (itemYear.TotalPerYear != 0 && data != null)
                            {
                                firstStep = data.Total / itemYear.TotalPerYear;
                            }
                            var lastStep = firstStep * 100;

                            //fomating value
                            var finalValue = decimal.Round(lastStep.Value, 28);

                            riskBugetItem.ProjectId = projectId;
                            riskBugetItem.Name = projectSelected.NamaProject;
                            riskBugetItem.Year = itemYear.aggregationYears;
                            riskBugetItem.Value = finalValue;
                            riskBugetCollection.Add(riskBugetItem);
                        }
                        itemRS.Name = projectSelected.NamaProject;
                        itemRS.Values = riskBugetCollection.ToArray();
                        itemCollection.Add(itemRS);
                        break;
                    case "Minimum Risk Percentage":
                        IList<RiskBudgetItem> riskBugetMinimum = new List<RiskBudgetItem>();
                        foreach (var itemYear in years)
                        {
                            RiskBudgetItem riskBugetItemMinimum = new RiskBudgetItem();
                            var data = itemYear.AggregationInterDiversifiedProjectCollection.Where(x => x.ProjectId == projectId).FirstOrDefault();
                            riskBugetItemMinimum.ProjectId = projectId;
                            riskBugetItemMinimum.Name = projectSelected.NamaProject;
                            riskBugetItemMinimum.Year = itemYear.aggregationYears;
                            riskBugetItemMinimum.Value = projectSelected.Minimum;
                            riskBugetMinimum.Add(riskBugetItemMinimum);
                        }
                        itemRS.Name = item;
                        itemRS.Values = riskBugetMinimum.ToArray();
                        itemCollection.Add(itemRS);
                        break;
                    case "Maximum Risk Percentage":
                        IList<RiskBudgetItem> riskBugetMaximum = new List<RiskBudgetItem>();
                        foreach (var itemYear in years)
                        {
                            RiskBudgetItem riskBugetItemMax = new RiskBudgetItem();
                            var data = itemYear.AggregationInterDiversifiedProjectCollection.Where(x => x.ProjectId == projectId).FirstOrDefault();
                            riskBugetItemMax.ProjectId = projectId;
                            riskBugetItemMax.Name = projectSelected.NamaProject;
                            riskBugetItemMax.Year = itemYear.aggregationYears;
                            riskBugetItemMax.Value = projectSelected.Maximum;
                            riskBugetMaximum.Add(riskBugetItemMax);
                        }
                        itemRS.Name = item;
                        itemRS.Values = riskBugetMaximum.ToArray();
                        itemCollection.Add(itemRS);
                        break;
                }
            }
            #endregion itemCollection
            #region yearCollection
            if (calculationResult.CollectionYears.Length > 0)
            {
                foreach (var item in calculationResult.CollectionYears)
                {
                    RiskBudgetYearCollection year = new RiskBudgetYearCollection();
                    year.Year = item;
                    yearList.Add(year);
                }
            }
            else
            {
                throw new ApplicationException(string.Format("Generate tahun gagal / data tidak ditemukan."));
            }
            #endregion yearCollection
            #region projectCollection
            IList<ProjectLite> projectList = new List<ProjectLite>();
            foreach (var item in projects)
            {
                ProjectLite proj = new ProjectLite();
                proj.ProjectId = item.Id;
                proj.NamaProject = item.NamaProject;
                projectList.Add(proj);
            }
            #endregion projectCollection

            byProject.RiskBudgetCollection = itemCollection.ToArray();
            byProject.RiskBudgetYearCollection = yearList.ToArray();
            byProject.ProjectCollection = projectList.ToArray();
            #endregion byProject

            #region byRisk
            var riskRegistrasis = _riskRegistrasiRepository.GetAll().ToList();
            var riskRegistrasiSelected = riskRegistrasis.Where(x => x.Id == riskRegistrasiId).FirstOrDefault();
            IList<RiskBudgetCollectionByRisk> itemRiskCollection = new List<RiskBudgetCollectionByRisk>();
            foreach (var item in listName)
            {
                RiskBudgetCollectionByRisk itemRisk = new RiskBudgetCollectionByRisk();
                var years = calculationResult.AggregationRisk.InterProjectDiversifiedAggregationRisk.RiskYearCollection;
                Validate.NotNull(years, "Aggregation Sektor Inter Project Diversified tidak ditemukan.");
                switch (item)
                {
                    case "Entity":
                        IList<RiskBudgetItemByRisk> riskBugetCollection = new List<RiskBudgetItemByRisk>();
                        foreach (var itemYear in years)
                        {
                            RiskBudgetItemByRisk riskBugetItem = new RiskBudgetItemByRisk();
                            var data = itemYear.RiskYearValue.Where(x => x.RiskRegistrasiId == riskRegistrasiId).FirstOrDefault();

                            decimal? firstStep = 0;
                            if (itemYear.Total != 0)
                            {
                                firstStep = data.Value / itemYear.Total;
                            }
                            var lastStep = firstStep * 100;

                            //fomating value
                            var finalValue = decimal.Round(lastStep.Value, 28);

                            riskBugetItem.RiskRegistrasiId = riskRegistrasiId;
                            riskBugetItem.KodeRisk = riskRegistrasiSelected.KodeMRisk;
                            riskBugetItem.Year = itemYear.Year;
                            riskBugetItem.Value = finalValue;
                            riskBugetCollection.Add(riskBugetItem);
                        }
                        itemRisk.Name = riskRegistrasiSelected.KodeMRisk;
                        itemRisk.Values = riskBugetCollection.ToArray();
                        itemRiskCollection.Add(itemRisk);
                        break;
                    case "Minimum Risk Percentage":
                        IList<RiskBudgetItemByRisk> riskBugetMinimum = new List<RiskBudgetItemByRisk>();
                        foreach (var itemYear in years)
                        {
                            RiskBudgetItemByRisk riskBugetItemMinimum = new RiskBudgetItemByRisk();
                            var data = itemYear.RiskYearValue.Where(x => x.RiskRegistrasiId == riskRegistrasiId).FirstOrDefault();
                            riskBugetItemMinimum.RiskRegistrasiId = riskRegistrasiId;
                            riskBugetItemMinimum.KodeRisk = riskRegistrasiSelected.KodeMRisk;
                            riskBugetItemMinimum.Year = itemYear.Year;
                            riskBugetItemMinimum.Value = Convert.ToDecimal(riskRegistrasiSelected.Minimum);
                            riskBugetMinimum.Add(riskBugetItemMinimum);
                        }
                        itemRisk.Name = item;
                        itemRisk.Values = riskBugetMinimum.ToArray();
                        itemRiskCollection.Add(itemRisk);
                        break;
                    case "Maximum Risk Percentage":
                        IList<RiskBudgetItemByRisk> riskBugetMaximum = new List<RiskBudgetItemByRisk>();
                        foreach (var itemYear in years)
                        {
                            RiskBudgetItemByRisk riskBugetItemMax = new RiskBudgetItemByRisk();
                            var data = itemYear.RiskYearValue.Where(x => x.RiskRegistrasiId == riskRegistrasiId).FirstOrDefault();
                            riskBugetItemMax.RiskRegistrasiId = riskRegistrasiId;
                            riskBugetItemMax.KodeRisk = riskRegistrasiSelected.KodeMRisk;
                            riskBugetItemMax.Year = itemYear.Year;
                            riskBugetItemMax.Value = Convert.ToDecimal(riskRegistrasiSelected.Maximum);
                            riskBugetMaximum.Add(riskBugetItemMax);
                        }
                        itemRisk.Name = item;
                        itemRisk.Values = riskBugetMaximum.ToArray();
                        itemRiskCollection.Add(itemRisk);
                        break;
                }
            }

            IList<RiskRegistrasiLite> risks = new List<RiskRegistrasiLite>();
            foreach (var item in riskRegistrasis)
            {
                RiskRegistrasiLite risk = new RiskRegistrasiLite();
                risk.RiskRegistrasiId = item.Id;
                risk.KodeMRisk = item.KodeMRisk;
                risk.NamaCategoryRisk = item.NamaCategoryRisk;
                risk.Definisi = item.Definisi;
                risk.Minimum = item.Minimum;
                risk.Maximum = item.Maximum;
                risks.Add(risk);
            }

            byRisk.RiskBudgetCollectionByRisk = itemRiskCollection.ToArray();
            byRisk.YearCollection = yearList.ToArray();
            byRisk.RiskRegistrasiCollection = risks.ToArray();
            #endregion byRisk

            #region bySektor
            #region itemCollection
            var sektors = _sektorRepository.GetAll().ToList();
            var sektorSelected = sektors.Where(x => x.Id == sektorId).FirstOrDefault();
            IList<RiskBudgetCollectionBySektor> itemSektorCollection = new List<RiskBudgetCollectionBySektor>();
            foreach (var item in listName)
            {
                RiskBudgetCollectionBySektor itemRS = new RiskBudgetCollectionBySektor();
                var years = calculationResult.AggregationOfProject.AggregationInterDiversifiedProjectCapital;
                Validate.NotNull(years, "Aggregation Inter Project Diversified tidak ditemukan.");
                switch (item)
                {
                    case "Entity":
                        IList<RiskBudgetItemBySektor> riskBugetCollection = new List<RiskBudgetItemBySektor>();
                        foreach (var itemYear in years)
                        {
                            RiskBudgetItemBySektor riskBugetItem = new RiskBudgetItemBySektor();
                            var data = itemYear.AggregationInterDiversifiedProjectCollection.Where(x => x.SektorId == sektorId).ToList();
                            decimal? firstStep = 0;
                            if (itemYear.TotalPerYear != 0)
                            {
                                firstStep = data.Sum(x => x.Total) / itemYear.TotalPerYear;
                            }
                            var lastStep = firstStep * 100;

                            //fomating value
                            var getTwoDigitValue = CurrencyHelper.toDouble(lastStep);
                            var finalValue = decimal.Round(lastStep.Value, 28);

                            riskBugetItem.SektorId = sektorId;
                            riskBugetItem.NamaSektor = sektorSelected.NamaSektor;
                            riskBugetItem.Year = itemYear.aggregationYears;
                            riskBugetItem.Value = finalValue;
                            riskBugetCollection.Add(riskBugetItem);
                        }
                        itemRS.Name = sektorSelected.NamaSektor;
                        itemRS.Values = riskBugetCollection.ToArray();
                        itemSektorCollection.Add(itemRS);
                        break;
                    case "Minimum Risk Percentage":
                        IList<RiskBudgetItemBySektor> riskBugetMinimum = new List<RiskBudgetItemBySektor>();
                        foreach (var itemYear in years)
                        {
                            RiskBudgetItemBySektor riskBugetItemMinimum = new RiskBudgetItemBySektor();
                            var data = itemYear.AggregationInterDiversifiedProjectCollection.Where(x => x.SektorId == sektorId).ToList();
                            riskBugetItemMinimum.SektorId = sektorId;
                            riskBugetItemMinimum.NamaSektor = sektorSelected.NamaSektor;
                            riskBugetItemMinimum.Year = itemYear.aggregationYears;
                            riskBugetItemMinimum.Value = sektorSelected.Minimum;
                            riskBugetMinimum.Add(riskBugetItemMinimum);
                        }
                        itemRS.Name = item;
                        itemRS.Values = riskBugetMinimum.ToArray();
                        itemSektorCollection.Add(itemRS);
                        break;
                    case "Maximum Risk Percentage":
                        IList<RiskBudgetItemBySektor> riskBugetMaximum = new List<RiskBudgetItemBySektor>();
                        foreach (var itemYear in years)
                        {
                            RiskBudgetItemBySektor riskBugetItemMax = new RiskBudgetItemBySektor();
                            var data = itemYear.AggregationInterDiversifiedProjectCollection.Where(x => x.SektorId == sektorId).ToList();
                            riskBugetItemMax.SektorId = sektorId;
                            riskBugetItemMax.NamaSektor = sektorSelected.NamaSektor;
                            riskBugetItemMax.Year = itemYear.aggregationYears;
                            riskBugetItemMax.Value = sektorSelected.Maximum;
                            riskBugetMaximum.Add(riskBugetItemMax);
                        }
                        itemRS.Name = item;
                        itemRS.Values = riskBugetMaximum.ToArray();
                        itemSektorCollection.Add(itemRS);
                        break;
                }
            }
            #endregion itemCollection

            #region sektorCollection
            IList<SektorLite> sektrorList = new List<SektorLite>();
            foreach (var item in sektors)
            {
                SektorLite proj = new SektorLite();
                proj.SektorId = item.Id;
                proj.NamaSektor = item.NamaSektor;
                sektrorList.Add(proj);
            }
            #endregion sektorCollection

            bySektor.RiskBudgetCollectionBySektor = itemSektorCollection.ToArray();
            bySektor.YearCollection = yearList.ToArray();
            bySektor.SektorCollection = sektrorList.ToArray();
            #endregion bySektor

            result.RiskBudgetByProject = byProject;
            result.RiskBudgetBySektor = bySektor;
            result.RiskBudgetByRisk = byRisk;

            return result;
        }
        #endregion 6. Risk Budget
                
    }
}
