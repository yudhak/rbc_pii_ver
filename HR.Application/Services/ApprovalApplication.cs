﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;
using HR.Infrastructure;
using System.Net;
using System.Dynamic;
//using System.Web.Http;


namespace HR.Application
{
    public class ApprovalService : IApprovalService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IApprovalRepository _approvalRepository;
        private readonly IDatabaseContext _databaseContext;
        private readonly IUserRepository _userRepository;
        private readonly IStatusRepository _statusRepository;
        private readonly IScenarioRepository _scenarioRepository;
        private readonly IScenarioDetailRepository _scenarioDetailRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly ICorrelatedSektorRepository _correlatedSectorRepository;
        private readonly IRiskMatrixProjectRepository _riskMatrixProjectRepository;
        private readonly ICorrelatedProjectRepository _correlatedProjectRepository;
        private readonly ICorrelationRiskAntarSektorRepository _correlationRiskAntarSektorRepository;
        private readonly IScenarioService _scenarioService;
        private readonly IStageTahunRiskMatrixService _stageTahunRiskMatrixService;
        private readonly IStageTahunRiskMatrixDetailService _stageTahunRiskMatrixDetailService;
        private readonly ICorrelatedProjectDetailService _correlatedProjectDetailService;
        private readonly ICorrelatedSektorDetailService _correlatedSektorDetailService;
        private readonly IMasterApprovalScenarioRepository _masterApprovalScenarioRepository;
        private readonly IMasterApprovalRiskMatrixProjectRepository _masterApprovalRiskMatrixProjectRepository;
        private readonly IMasterApprovalCorrelatedSektorRepository _masterApprovalCorrelatedSektorRepository;
        private readonly IMasterApprovalCorrelatedProjectRepository _masterApprovalCorrelatedProjectRepository;
        private readonly IRiskMatrixRepository _riskMatrixRepository;
        private readonly IEmailService _emailService;
        private readonly IStageTahunRiskMatrixRepository _stageTahunRiskMatrixRepository;
        private readonly IRiskMatriksTemporerService _riskMatriksTemporerService;
        private readonly IRiskMatriksTemporerRepository _riskMatriksTemporerRepository;
        private readonly ISektorRepository _sektorRepository;

        public ApprovalService(IUnitOfWork uow, IApprovalRepository approvalRepository, IDatabaseContext databaseContext
            , IUserRepository userRepository, IStatusRepository statusRepository, IScenarioRepository scenarioRepository,
            IScenarioDetailRepository scenarioDetailRepository, IProjectRepository projectRepository,
            ICorrelatedSektorRepository correlatedSectorRepository, IRiskMatrixProjectRepository riskMatrixProjectRepository
            , ICorrelatedProjectRepository correlatedProjectRepository, IScenarioService scenarioService,
            IStageTahunRiskMatrixService stageTahunRiskMatrixService, IStageTahunRiskMatrixDetailService stageTahunRiskMatrixDetailService
            , IRiskMatrixRepository riskMatrixRepository, ICorrelationRiskAntarSektorRepository correlationRiskAntarSektorRepository,
            ICorrelatedProjectDetailService correlatedProjectDetailService, ICorrelatedSektorDetailService correlatedSektorDetailService,
            IMasterApprovalScenarioRepository masterApprovalScenarioRepository, IMasterApprovalRiskMatrixProjectRepository masterApprovalRiskMatrixProjectRepository
            , IMasterApprovalCorrelatedSektorRepository masterApprovalCorrelatedSektorRepository, IMasterApprovalCorrelatedProjectRepository masterApprovalCorrelatedProjectRepository, IEmailService emailService
            , IStageTahunRiskMatrixRepository stageTahunRiskMatrixRepository, IRiskMatriksTemporerService riskMatriksTemporerService, IRiskMatriksTemporerRepository riskMatriksTemporerRepository,
            ISektorRepository sektorRepository)
        {
            _unitOfWork = uow;
            _approvalRepository = approvalRepository;
            _databaseContext = databaseContext;
            _userRepository = userRepository;
            _statusRepository = statusRepository;
            _scenarioRepository = scenarioRepository;
            _scenarioDetailRepository = scenarioDetailRepository;
            _projectRepository = projectRepository;
            _correlatedSectorRepository = correlatedSectorRepository;
            _riskMatrixProjectRepository = riskMatrixProjectRepository;
            _correlatedProjectRepository = correlatedProjectRepository;
            _scenarioService = scenarioService;
            _stageTahunRiskMatrixService = stageTahunRiskMatrixService;
            _stageTahunRiskMatrixDetailService = stageTahunRiskMatrixDetailService;
            _riskMatrixRepository = riskMatrixRepository;
            _correlationRiskAntarSektorRepository = correlationRiskAntarSektorRepository;
            _correlatedProjectDetailService = correlatedProjectDetailService;
            _correlatedSektorDetailService = correlatedSektorDetailService;
            _masterApprovalScenarioRepository = masterApprovalScenarioRepository;
            _masterApprovalRiskMatrixProjectRepository = masterApprovalRiskMatrixProjectRepository;
            _masterApprovalCorrelatedSektorRepository = masterApprovalCorrelatedSektorRepository;
            _masterApprovalCorrelatedProjectRepository = masterApprovalCorrelatedProjectRepository;
            _emailService = emailService;
            _stageTahunRiskMatrixRepository = stageTahunRiskMatrixRepository;
            _riskMatriksTemporerService = riskMatriksTemporerService;
            _riskMatriksTemporerRepository = riskMatriksTemporerRepository;
            _sektorRepository = sektorRepository;
        }
        //tessting
        #region Query
        public IEnumerable<Approval> GetAll()
        {
            return _approvalRepository.GetAll();
        }

        public Approval Get(int id)
        {
            return _approvalRepository.Get(id);
        }

        public string GetByActive(int id, int idUser, string source)
        {
            var approvalActive = _approvalRepository.GetByActive(id, idUser, source);

            if (approvalActive != null)
            {
                return approvalActive;
            }
            else
            {
                return null;
            }
        }

        public int Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Approval tidak ditemukan.");

            using (_unitOfWork)
            {
                model.Delete(deleteBy, deleteDate);
                _approvalRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;
            }
            return id;
        }

        public IEnumerable<ApprovalExtend> GetSentItem(string keyword, int id, bool sentItem, int userId)
        {
            var result = _approvalRepository.GetSentItem(sentItem, userId, keyword, id).ToList();

            //var scenarioSent = _scenarioRepository.GetAll().Where(x => x.StatusId == 1 && x.CreateBy == userId);
            

            #region asign entity into object
            IList<ApprovalExtend> approvalList = new List<ApprovalExtend>();

            for (int i = 0; i < result.Count; i++)
            {
                var result2 = _approvalRepository.GetByRequestId(result[i].RequestId).ToList();
                foreach (var getSameApproval in result2)
                {
                    if (getSameApproval.StatusId == 3)
                    {
                        result[i].StatusId = 3;
                    }
                }
                switch (result[i].SourceApproval)
                {
                    #region scenario
                    case "Scenario":
                        
                        ApprovalExtend obj = new ApprovalExtend();
                        ScenarioExtend scenarioExt = new ScenarioExtend();
                        var scenario = _scenarioRepository.Get(result[i].RequestId);
                        if (scenario != null)
                        {
                            scenarioExt.NamaScenario = scenario.NamaScenario;
                            scenarioExt.IsDefault = scenario.IsDefault;
                            scenarioExt.CreateBy = scenario.CreateBy;
                            scenarioExt.UpdateBy = scenario.UpdateBy;
                            scenarioExt.UpdateDate = scenario.UpdateDate;
                            scenarioExt.LikehoodId = scenario.LikehoodId;
                            scenarioExt.StatusId = scenario.StatusId;

                            StatusExtend statusExtend = new StatusExtend();
                            var status = _statusRepository.Get(result[i].StatusId);
                            statusExtend.StatusDescription = status.StatusDescription;

                            UserExtend userExtend = new UserExtend();
                            var user = _userRepository.Get(result[i].CreateBy.GetValueOrDefault());
                            userExtend.UserName = user.UserName;
                            userExtend.Email = user.Email;
                            userExtend.Id = user.Id;

                            obj.Scenario = scenarioExt;
                            obj.Id = result[i].Id;
                            obj.NamaScenario = scenario.NamaScenario;
                            obj.StatusApproval = result[i].StatusApproval;
                            obj.SourceApproval = result[i].SourceApproval;
                            obj.CreateBy = result[i].CreateBy;
                            obj.CreateDate = result[i].CreateDate;
                            obj.UpdateBy = result[i].UpdateBy;
                            obj.UpdateDate = result[i].UpdateDate;
                            obj.IsDelete = result[i].IsDelete;
                            obj.DeleteDate = result[i].DeleteDate;
                            obj.Keterangan = result[i].Keterangan;
                            obj.IsActive = result[i].IsActive;
                            if (scenarioExt.StatusId == 2 )
                            {
                                obj.StatusId = scenarioExt.StatusId.GetValueOrDefault();
                            }
                            else
                            {
                                obj.StatusId = result[i].StatusId;
                            }
                            obj.RequestId = result[i].RequestId;
                            obj.NomorUrutStatus = result[i].NomorUrutStatus;
                            obj.Status = statusExtend;
                            obj.User = userExtend;
                            obj.NamaEntitas = scenario.NamaScenario;
                            approvalList.Add(obj);
                        }
                        break;
                    #endregion scenario

                    #region riskmatrixproject
                    case "RiskMatrixProject":
                        ApprovalExtend objR = new ApprovalExtend();
                        RiskMatrixProjectExtend rm = new RiskMatrixProjectExtend();
                        var riskMatrix = _riskMatrixProjectRepository.Get(result[i].RequestId);
                        Validate.NotNull(riskMatrix, "Risk Matrix Proyek tidak ditemukan.");

                        rm.ProjectId = riskMatrix.ProjectId;
                        rm.RiskMatrixId = riskMatrix.RiskMatrixId;
                        rm.ScenarioId = riskMatrix.ScenarioId;
                        rm.StatusId = riskMatrix.StatusId;
                        rm.CreateBy = riskMatrix.CreateBy;
                        rm.CreateDate = riskMatrix.CreateDate;
                        rm.UpdateBy = riskMatrix.UpdateBy;
                        rm.UpdateDate = riskMatrix.UpdateDate;

                        StatusExtend statusExtendR = new StatusExtend();
                        var statusR = _statusRepository.Get(result[i].StatusId);
                        statusExtendR.StatusDescription = statusR.StatusDescription;

                        UserExtend userExtendR = new UserExtend();
                        var userR = _userRepository.Get(result[i].CreateBy.GetValueOrDefault());
                        userExtendR.UserName = userR.UserName;
                        userExtendR.Email = userR.Email;
                        userExtendR.Id = userR.Id;

                        ProjectExtend pR = new ProjectExtend();
                        var project = _projectRepository.Get(rm.ProjectId);
                        pR.NamaProject = project.NamaProject;

                        objR.RiskMatrixProject = rm;
                        objR.Id = result[i].Id;
                        objR.NamaScenario = riskMatrix.Scenario.NamaScenario;
                        objR.StatusApproval = result[i].StatusApproval;
                        objR.SourceApproval = result[i].SourceApproval;
                        objR.CreateBy = result[i].CreateBy;
                        objR.CreateDate = result[i].CreateDate;
                        objR.UpdateBy = result[i].UpdateBy;
                        objR.UpdateDate = result[i].UpdateDate;
                        objR.IsDelete = result[i].IsDelete;
                        objR.DeleteDate = result[i].DeleteDate;
                        objR.Keterangan = result[i].Keterangan;
                        objR.IsActive = result[i].IsActive;
                        if (rm.StatusId == 2)
                        {
                            objR.StatusId = rm.StatusId.GetValueOrDefault();
                        }
                        else
                        {
                            objR.StatusId = result[i].StatusId;
                        }
                        objR.RequestId = result[i].RequestId;
                        objR.NomorUrutStatus = result[i].NomorUrutStatus;
                        objR.Status = statusExtendR;
                        objR.User = userExtendR;
                        objR.NamaEntitas = pR.NamaProject;
                        approvalList.Add(objR);
                        break;
                    #endregion riskmatrixproject

                    #region correlationrisk
                    case "CorrelatedSektor":
                        ApprovalExtend objCS = new ApprovalExtend();
                        CorrelatedSektorExtend cs = new CorrelatedSektorExtend();
                        var correlatedSektor = _correlatedSectorRepository.Get(result[i].RequestId);

                        var scenarioS = _scenarioRepository.Get(correlatedSektor.ScenarioId);
                        Validate.NotNull(correlatedSektor, "Korelasi Sektor tidak ditemukan.");

                        cs.NamaSektor = correlatedSektor.NamaSektor;
                        cs.ScenarioId = correlatedSektor.ScenarioId;
                        cs.SektorId = correlatedSektor.SektorId;
                        cs.StatusId = correlatedSektor.StatusId;
                        cs.CreateBy = correlatedSektor.CreateBy;
                        cs.CreateDate = correlatedSektor.CreateDate;
                        cs.UpdateBy = correlatedSektor.UpdateBy;
                        cs.UpdateDate = correlatedSektor.UpdateDate;

                        StatusExtend statusExtendS = new StatusExtend();
                        var statusS = _statusRepository.Get(result[i].StatusId);
                        statusExtendS.StatusDescription = statusS.StatusDescription;

                        UserExtend userExtendS = new UserExtend();
                        var userS = _userRepository.Get(result[i].CreateBy.GetValueOrDefault());
                        userExtendS.UserName = userS.UserName;
                        userExtendS.Email = userS.Email;
                        userExtendS.Id = userS.Id;

                        SektorExtend sektorExtendS = new SektorExtend();
                        var sektor = _sektorRepository.Get(cs.SektorId);
                        sektorExtendS.NamaSektor = sektor.NamaSektor;

                        objCS.CorrelatedSektor = cs;
                        objCS.Id = result[i].Id;
                        objCS.NamaScenario = scenarioS.NamaScenario;
                        objCS.StatusApproval = result[i].StatusApproval;
                        objCS.SourceApproval = result[i].SourceApproval;
                        objCS.CreateBy = result[i].CreateBy;
                        objCS.CreateDate = result[i].CreateDate;
                        objCS.UpdateBy = result[i].UpdateBy;
                        objCS.UpdateDate = result[i].UpdateDate;
                        objCS.IsDelete = result[i].IsDelete;
                        objCS.DeleteDate = result[i].DeleteDate;
                        objCS.Keterangan = result[i].Keterangan;
                        objCS.IsActive = result[i].IsActive;
                        if (cs.StatusId == 2)
                        {
                            objCS.StatusId = cs.StatusId.GetValueOrDefault();
                        }
                        else
                        {
                            objCS.StatusId = result[i].StatusId;
                        }
                        objCS.RequestId = result[i].RequestId;
                        objCS.NomorUrutStatus = result[i].NomorUrutStatus;
                        objCS.Status = statusExtendS;
                        objCS.User = userExtendS;
                        objCS.NamaEntitas = sektorExtendS.NamaSektor;
                        approvalList.Add(objCS);
                        break;
                    #endregion correlationrisk

                    #region correlationproject
                    case "CorrelatedProject":
                        ApprovalExtend objCP = new ApprovalExtend();
                        CorrelatedProjectExtend cp = new CorrelatedProjectExtend();
                        var correlatedProject = _correlatedProjectRepository.Get(result[i].RequestId);
                        Validate.NotNull(correlatedProject, "Korelasi Proyek tidak ditemukan.");
                        var scenarioP = _scenarioRepository.Get(correlatedProject.ScenarioId);
                        cp.ProjectId = correlatedProject.ProjectId;
                        cp.ProjectName = correlatedProject.Project.NamaProject;
                        cp.ScenarioId = correlatedProject.ScenarioId;
                        cp.SektorId = correlatedProject.SektorId;
                        cp.StatusId = correlatedProject.StatusId;
                        cp.CreateBy = result[i].CreateBy;
                        cp.CreateDate = result[i].CreateDate;
                        cp.UpdateBy = result[i].UpdateBy;
                        cp.UpdateDate = result[i].UpdateDate;
                        cp.IsDelete = result[i].IsDelete;
                        cp.DeleteDate = result[i].DeleteDate;
                        cp.IsApproved = correlatedProject.IsApproved;

                        StatusExtend statusExtendP = new StatusExtend();
                        var statusP = _statusRepository.Get(result[i].StatusId);
                        statusExtendP.StatusDescription = statusP.StatusDescription;

                        UserExtend userExtendP = new UserExtend();
                        var userP = _userRepository.Get(result[i].CreateBy.GetValueOrDefault());
                        userExtendP.UserName = userP.UserName;
                        userExtendP.Email = userP.Email;
                        userExtendP.Id = userP.Id;

                        ProjectExtend projectExtendP = new ProjectExtend();
                        var projectP = _projectRepository.Get(cp.ProjectId);

                        projectExtendP.NamaProject = projectP.NamaProject;

                        objCP.CorrelatedProject = cp;
                        objCP.Id = result[i].Id;
                        objCP.NamaScenario = scenarioP.NamaScenario;
                        objCP.StatusApproval = result[i].StatusApproval;
                        objCP.SourceApproval = result[i].SourceApproval;
                        objCP.CreateBy = result[i].CreateBy;
                        objCP.CreateDate = result[i].CreateDate;
                        objCP.UpdateBy = result[i].UpdateBy;
                        objCP.UpdateDate = result[i].UpdateDate;
                        objCP.IsDelete = result[i].IsDelete;
                        objCP.DeleteDate = result[i].DeleteDate;
                        objCP.Keterangan = result[i].Keterangan;
                        objCP.IsActive = result[i].IsActive;
                        if (cp.StatusId == 2 )
                        {
                            objCP.StatusId = cp.StatusId.GetValueOrDefault();
                        }
                        else
                        {
                            objCP.StatusId = result[i].StatusId;
                        }
                        objCP.RequestId = result[i].RequestId;
                        objCP.NomorUrutStatus = result[i].NomorUrutStatus;
                        objCP.Status = statusExtendP;
                        objCP.User = userExtendP;
                        objCP.NamaEntitas = projectExtendP.NamaProject;
                        approvalList.Add(objCP);
                        break;
                        #endregion correlationproject
                }
            }
            #endregion asign entity into object

            return approvalList.OrderByDescending(x => x.Id).ToList();
        }


        public IEnumerable<ApprovalExtend> GetSentItem(string keyword, int id, int id2, bool sentItem, int userId)
        {
            var result = _approvalRepository.GetSentItem(sentItem, userId, keyword, id).ToList();

            //var scenarioSent = _scenarioRepository.GetAll().Where(x => x.StatusId == 1 && x.CreateBy == userId);


            #region asign entity into object
            IList<ApprovalExtend> approvalList = new List<ApprovalExtend>();
            IList<ApprovalExtend> results = new List<ApprovalExtend>();
            for (int i = 0; i < result.Count; i++)
            {
                var result2 = _approvalRepository.GetByRequestId(result[i].RequestId).ToList();
                foreach (var getSameApproval in result2)
                {
                    if (getSameApproval.StatusId == 3)
                    {
                        result[i].StatusId = 3;
                    }
                }
                switch (result[i].SourceApproval)
                {
                    #region scenario
                    case "Scenario":

                        ApprovalExtend obj = new ApprovalExtend();
                        ScenarioExtend scenarioExt = new ScenarioExtend();
                        var scenario = _scenarioRepository.Get(result[i].RequestId);
                        if (scenario != null)
                        {
                            scenarioExt.NamaScenario = scenario.NamaScenario;
                            scenarioExt.IsDefault = scenario.IsDefault;
                            scenarioExt.CreateBy = scenario.CreateBy;
                            scenarioExt.UpdateBy = scenario.UpdateBy;
                            scenarioExt.UpdateDate = scenario.UpdateDate;
                            scenarioExt.LikehoodId = scenario.LikehoodId;
                            scenarioExt.StatusId = scenario.StatusId;

                            StatusExtend statusExtend = new StatusExtend();
                            var status = _statusRepository.Get(result[i].StatusId);
                            statusExtend.StatusDescription = status.StatusDescription;

                            UserExtend userExtend = new UserExtend();
                            var user = _userRepository.Get(result[i].CreateBy.GetValueOrDefault());
                            userExtend.UserName = user.UserName;
                            userExtend.Email = user.Email;
                            userExtend.Id = user.Id;

                            obj.Scenario = scenarioExt;
                            obj.Id = result[i].Id;
                            obj.NamaScenario = scenario.NamaScenario;
                            obj.StatusApproval = result[i].StatusApproval;
                            obj.SourceApproval = result[i].SourceApproval;
                            obj.CreateBy = result[i].CreateBy;
                            obj.CreateDate = result[i].CreateDate;
                            obj.UpdateBy = result[i].UpdateBy;
                            obj.UpdateDate = result[i].UpdateDate;
                            obj.IsDelete = result[i].IsDelete;
                            obj.DeleteDate = result[i].DeleteDate;
                            obj.Keterangan = result[i].Keterangan;
                            obj.IsActive = result[i].IsActive;
                            if (scenarioExt.StatusId == 2)
                            {
                                obj.StatusId = scenarioExt.StatusId.GetValueOrDefault();
                            }
                            else
                            {
                                obj.StatusId = result[i].StatusId;
                            }
                            obj.RequestId = result[i].RequestId;
                            obj.NomorUrutStatus = result[i].NomorUrutStatus;
                            obj.Status = statusExtend;
                            obj.User = userExtend;
                            obj.NamaEntitas = scenario.NamaScenario;
                            approvalList.Add(obj);
                        }
                        break;
                    #endregion scenario

                    #region riskmatrixproject
                    case "RiskMatrixProject":
                        ApprovalExtend objR = new ApprovalExtend();
                        RiskMatrixProjectExtend rm = new RiskMatrixProjectExtend();
                        var riskMatrix = _riskMatrixProjectRepository.Get(result[i].RequestId);
                        Validate.NotNull(riskMatrix, "Risk Matrix Proyek tidak ditemukan.");

                        rm.ProjectId = riskMatrix.ProjectId;
                        rm.RiskMatrixId = riskMatrix.RiskMatrixId;
                        rm.ScenarioId = riskMatrix.ScenarioId;
                        rm.StatusId = riskMatrix.StatusId;
                        rm.CreateBy = riskMatrix.CreateBy;
                        rm.CreateDate = riskMatrix.CreateDate;
                        rm.UpdateBy = riskMatrix.UpdateBy;
                        rm.UpdateDate = riskMatrix.UpdateDate;

                        StatusExtend statusExtendR = new StatusExtend();
                        var statusR = _statusRepository.Get(result[i].StatusId);
                        statusExtendR.StatusDescription = statusR.StatusDescription;

                        UserExtend userExtendR = new UserExtend();
                        var userR = _userRepository.Get(result[i].CreateBy.GetValueOrDefault());
                        userExtendR.UserName = userR.UserName;
                        userExtendR.Email = userR.Email;
                        userExtendR.Id = userR.Id;

                        ProjectExtend pR = new ProjectExtend();
                        var project = _projectRepository.Get(rm.ProjectId);
                        pR.NamaProject = project.NamaProject;

                        objR.RiskMatrixProject = rm;
                        objR.Id = result[i].Id;
                        objR.NamaScenario = riskMatrix.Scenario.NamaScenario;
                        objR.StatusApproval = result[i].StatusApproval;
                        objR.SourceApproval = result[i].SourceApproval;
                        objR.CreateBy = result[i].CreateBy;
                        objR.CreateDate = result[i].CreateDate;
                        objR.UpdateBy = result[i].UpdateBy;
                        objR.UpdateDate = result[i].UpdateDate;
                        objR.IsDelete = result[i].IsDelete;
                        objR.DeleteDate = result[i].DeleteDate;
                        objR.Keterangan = result[i].Keterangan;
                        objR.IsActive = result[i].IsActive;
                        if (rm.StatusId == 2)
                        {
                            objR.StatusId = rm.StatusId.GetValueOrDefault();
                        }
                        else
                        {
                            objR.StatusId = result[i].StatusId;
                        }
                        objR.RequestId = result[i].RequestId;
                        objR.NomorUrutStatus = result[i].NomorUrutStatus;
                        objR.Status = statusExtendR;
                        objR.User = userExtendR;
                        objR.NamaEntitas = pR.NamaProject;
                        approvalList.Add(objR);
                        break;
                    #endregion riskmatrixproject

                    #region correlationrisk
                    case "CorrelatedSektor":
                        ApprovalExtend objCS = new ApprovalExtend();
                        CorrelatedSektorExtend cs = new CorrelatedSektorExtend();
                        var correlatedSektor = _correlatedSectorRepository.Get(result[i].RequestId);

                        var scenarioS = _scenarioRepository.Get(correlatedSektor.ScenarioId);
                        Validate.NotNull(correlatedSektor, "Korelasi Sektor tidak ditemukan.");

                        cs.NamaSektor = correlatedSektor.NamaSektor;
                        cs.ScenarioId = correlatedSektor.ScenarioId;
                        cs.SektorId = correlatedSektor.SektorId;
                        cs.StatusId = correlatedSektor.StatusId;
                        cs.CreateBy = correlatedSektor.CreateBy;
                        cs.CreateDate = correlatedSektor.CreateDate;
                        cs.UpdateBy = correlatedSektor.UpdateBy;
                        cs.UpdateDate = correlatedSektor.UpdateDate;

                        StatusExtend statusExtendS = new StatusExtend();
                        var statusS = _statusRepository.Get(result[i].StatusId);
                        statusExtendS.StatusDescription = statusS.StatusDescription;

                        UserExtend userExtendS = new UserExtend();
                        var userS = _userRepository.Get(result[i].CreateBy.GetValueOrDefault());
                        userExtendS.UserName = userS.UserName;
                        userExtendS.Email = userS.Email;
                        userExtendS.Id = userS.Id;

                        SektorExtend sektorExtendS = new SektorExtend();
                        var sektor = _sektorRepository.Get(cs.SektorId);
                        sektorExtendS.NamaSektor = sektor.NamaSektor;

                        objCS.CorrelatedSektor = cs;
                        objCS.Id = result[i].Id;
                        objCS.NamaScenario = scenarioS.NamaScenario;
                        objCS.StatusApproval = result[i].StatusApproval;
                        objCS.SourceApproval = result[i].SourceApproval;
                        objCS.CreateBy = result[i].CreateBy;
                        objCS.CreateDate = result[i].CreateDate;
                        objCS.UpdateBy = result[i].UpdateBy;
                        objCS.UpdateDate = result[i].UpdateDate;
                        objCS.IsDelete = result[i].IsDelete;
                        objCS.DeleteDate = result[i].DeleteDate;
                        objCS.Keterangan = result[i].Keterangan;
                        objCS.IsActive = result[i].IsActive;
                        if (cs.StatusId == 2)
                        {
                            objCS.StatusId = cs.StatusId.GetValueOrDefault();
                        }
                        else
                        {
                            objCS.StatusId = result[i].StatusId;
                        }
                        objCS.RequestId = result[i].RequestId;
                        objCS.NomorUrutStatus = result[i].NomorUrutStatus;
                        objCS.Status = statusExtendS;
                        objCS.User = userExtendS;
                        objCS.NamaEntitas = sektorExtendS.NamaSektor;
                        approvalList.Add(objCS);
                        break;
                    #endregion correlationrisk

                    #region correlationproject
                    case "CorrelatedProject":
                        ApprovalExtend objCP = new ApprovalExtend();
                        CorrelatedProjectExtend cp = new CorrelatedProjectExtend();
                        var correlatedProject = _correlatedProjectRepository.Get(result[i].RequestId);
                        Validate.NotNull(correlatedProject, "Korelasi Proyek tidak ditemukan.");
                        var scenarioP = _scenarioRepository.Get(correlatedProject.ScenarioId);
                        cp.ProjectId = correlatedProject.ProjectId;
                        cp.ProjectName = correlatedProject.Project.NamaProject;
                        cp.ScenarioId = correlatedProject.ScenarioId;
                        cp.SektorId = correlatedProject.SektorId;
                        cp.StatusId = correlatedProject.StatusId;
                        cp.CreateBy = result[i].CreateBy;
                        cp.CreateDate = result[i].CreateDate;
                        cp.UpdateBy = result[i].UpdateBy;
                        cp.UpdateDate = result[i].UpdateDate;
                        cp.IsDelete = result[i].IsDelete;
                        cp.DeleteDate = result[i].DeleteDate;
                        cp.IsApproved = correlatedProject.IsApproved;

                        StatusExtend statusExtendP = new StatusExtend();
                        var statusP = _statusRepository.Get(result[i].StatusId);
                        statusExtendP.StatusDescription = statusP.StatusDescription;

                        UserExtend userExtendP = new UserExtend();
                        var userP = _userRepository.Get(result[i].CreateBy.GetValueOrDefault());
                        userExtendP.UserName = userP.UserName;
                        userExtendP.Email = userP.Email;
                        userExtendP.Id = userP.Id;

                        ProjectExtend projectExtendP = new ProjectExtend();
                        var projectP = _projectRepository.Get(cp.ProjectId);

                        projectExtendP.NamaProject = projectP.NamaProject;

                        objCP.CorrelatedProject = cp;
                        objCP.Id = result[i].Id;
                        objCP.NamaScenario = scenarioP.NamaScenario;
                        objCP.StatusApproval = result[i].StatusApproval;
                        objCP.SourceApproval = result[i].SourceApproval;
                        objCP.CreateBy = result[i].CreateBy;
                        objCP.CreateDate = result[i].CreateDate;
                        objCP.UpdateBy = result[i].UpdateBy;
                        objCP.UpdateDate = result[i].UpdateDate;
                        objCP.IsDelete = result[i].IsDelete;
                        objCP.DeleteDate = result[i].DeleteDate;
                        objCP.Keterangan = result[i].Keterangan;
                        objCP.IsActive = result[i].IsActive;
                        if (cp.StatusId == 2)
                        {
                            objCP.StatusId = cp.StatusId.GetValueOrDefault();
                        }
                        else
                        {
                            objCP.StatusId = result[i].StatusId;
                        }
                        objCP.RequestId = result[i].RequestId;
                        objCP.NomorUrutStatus = result[i].NomorUrutStatus;
                        objCP.Status = statusExtendP;
                        objCP.User = userExtendP;
                        objCP.NamaEntitas = projectExtendP.NamaProject;
                        approvalList.Add(objCP);
                        break;
                        #endregion correlationproject
                }
            }
            #endregion asign entity into object
            if (id2 == 0)
            {
                results = approvalList.ToList();
            }
            else
            {
                foreach (var item in approvalList)
                {
                    if (id2 == 5 || (item.Status == null && id2 == 4))
                    {
                        results.Add(item);
                    }
                    if (item.Status != null && id2 != 5)
                    {
                        if (item.StatusId == id2)
                            results.Add(item);
                        //if (item.Status.StatusDescription.ToLower().Contains(keyword.ToLower()))
                        //    result.Add(item);
                    }
                }
            }
            
            

            return results.OrderByDescending(x => x.Id).ToList();
        }
        public IEnumerable<ApprovalExtend> GetByRequestId(int requestId)
        {
            var result = _approvalRepository.GetByRequestId(requestId).ToList();

            IList<ApprovalExtend> approvalList = new List<ApprovalExtend>();
            if (result.Count > 0)
            {
                for (int i = 0; i < result.Count; i++)
                {
                    switch (result[i].SourceApproval)
                    {
                        #region scenario
                        case "Scenario":
                            ApprovalExtend obj = new ApprovalExtend();
                            ScenarioExtend scenarioExt = new ScenarioExtend();
                            var scenario = _scenarioRepository.Get(result[i].RequestId);
                            if (scenario != null)
                            {
                                scenarioExt.NamaScenario = scenario.NamaScenario;
                                scenarioExt.IsDefault = scenario.IsDefault;
                                scenarioExt.CreateBy = scenario.CreateBy;
                                scenarioExt.UpdateBy = scenario.UpdateBy;
                                scenarioExt.UpdateDate = scenario.UpdateDate;
                                scenarioExt.LikehoodId = scenario.LikehoodId;
                                scenarioExt.StatusId = scenario.StatusId;

                                StatusExtend statusExtend = new StatusExtend();
                                var status = _statusRepository.Get(result[i].StatusId);
                                statusExtend.StatusDescription = status.StatusDescription;

                                UserExtend userExtend = new UserExtend();
                                var user = _userRepository.Get(result[i].CreateBy.GetValueOrDefault());
                                userExtend.UserName = user.UserName;
                                userExtend.Email = user.Email;
                                userExtend.Id = user.Id;

                                UserExtend userApprove = new UserExtend();
                                var userA = _userRepository.Get(result[i].UpdateBy.GetValueOrDefault());
                                if (userA != null)
                                {
                                    userApprove.UserName = userA.UserName;
                                    userApprove.Email = userA.Email;
                                    userApprove.Id = userA.Id;
                                }
                                else
                                {
                                    userApprove.UserName = "";
                                    userApprove.Email = "";
                                    userApprove.Id = 0;
                                }
                                

                                obj.Scenario = scenarioExt;
                                obj.Id = result[i].Id;
                                obj.NamaScenario = scenario.NamaScenario;
                                obj.StatusApproval = result[i].StatusApproval;
                                obj.SourceApproval = result[i].SourceApproval;
                                obj.CreateBy = result[i].CreateBy;
                                obj.CreateDate = result[i].CreateDate;
                                obj.UpdateBy = result[i].UpdateBy;
                                obj.UpdateDate = result[i].UpdateDate;
                                obj.IsDelete = result[i].IsDelete;
                                obj.DeleteDate = result[i].DeleteDate;
                                obj.Keterangan = result[i].Keterangan;
                                obj.IsActive = result[i].IsActive;
                                obj.StatusId = result[i].StatusId;
                                obj.RequestId = result[i].RequestId;
                                obj.NomorUrutStatus = result[i].NomorUrutStatus;
                                obj.Status = statusExtend;
                                obj.User = userExtend;
                                obj.NamaEntitas = scenario.NamaScenario;
                                obj.UserApprove = userApprove;
                                approvalList.Add(obj);
                            }
                            break;
                        #endregion scenario

                        #region riskmatrixproject
                        case "RiskMatrixProject":
                            ApprovalExtend objR = new ApprovalExtend();
                            RiskMatrixProjectExtend rm = new RiskMatrixProjectExtend();
                            var riskMatrix = _riskMatrixProjectRepository.Get(result[i].RequestId);
                            Validate.NotNull(riskMatrix, "Risk Matrix Proyek tidak ditemukan.");

                            rm.ProjectId = riskMatrix.ProjectId;
                            rm.RiskMatrixId = riskMatrix.RiskMatrixId;
                            rm.ScenarioId = riskMatrix.ScenarioId;
                            rm.StatusId = riskMatrix.StatusId;
                            rm.CreateBy = riskMatrix.CreateBy;
                            rm.CreateDate = riskMatrix.CreateDate;
                            rm.UpdateBy = riskMatrix.UpdateBy;
                            rm.UpdateDate = riskMatrix.UpdateDate;

                            StatusExtend statusExtendR = new StatusExtend();
                            var statusR = _statusRepository.Get(result[i].StatusId);
                            statusExtendR.StatusDescription = statusR.StatusDescription;

                            UserExtend userExtendR = new UserExtend();
                            var userR = _userRepository.Get(result[i].CreateBy.GetValueOrDefault());
                            userExtendR.UserName = userR.UserName;
                            userExtendR.Email = userR.Email;
                            userExtendR.Id = userR.Id;

                            UserExtend userApproveR = new UserExtend();
                            var userAR = _userRepository.Get(result[i].UpdateBy.GetValueOrDefault());
                            if (userAR != null)
                            {
                                userApproveR.UserName = userAR.UserName;
                                userApproveR.Email = userAR.Email;
                                userApproveR.Id = userAR.Id;
                            }
                            else
                            {
                                userApproveR.UserName = "";
                                userApproveR.Email = "";
                                userApproveR.Id = 0;
                            }


                            ProjectExtend pR = new ProjectExtend();
                            var project = _projectRepository.Get(rm.ProjectId);
                            pR.NamaProject = project.NamaProject;

                            objR.RiskMatrixProject = rm;
                            objR.Id = result[i].Id;
                            objR.NamaScenario = riskMatrix.Scenario.NamaScenario;
                            objR.StatusApproval = result[i].StatusApproval;
                            objR.SourceApproval = result[i].SourceApproval;
                            objR.CreateBy = result[i].CreateBy;
                            objR.CreateDate = result[i].CreateDate;
                            objR.UpdateBy = result[i].UpdateBy;
                            objR.UpdateDate = result[i].UpdateDate;
                            objR.IsDelete = result[i].IsDelete;
                            objR.DeleteDate = result[i].DeleteDate;
                            objR.Keterangan = result[i].Keterangan;
                            objR.IsActive = result[i].IsActive;
                            objR.StatusId = result[i].StatusId;
                            objR.RequestId = result[i].RequestId;
                            objR.NomorUrutStatus = result[i].NomorUrutStatus;
                            objR.Status = statusExtendR;
                            objR.User = userExtendR;
                            objR.NamaEntitas = pR.NamaProject;
                            objR.UserApprove = userApproveR;
                            approvalList.Add(objR);
                            break;
                        #endregion riskmatrixproject

                        #region correlationrisk
                        case "CorrelatedSektor":
                            ApprovalExtend objCS = new ApprovalExtend();
                            CorrelatedSektorExtend cs = new CorrelatedSektorExtend();
                            var correlatedSektor = _correlatedSectorRepository.Get(result[i].RequestId);
                            Validate.NotNull(correlatedSektor, "Korelasi Sektor tidak ditemukan.");
                            var scenarioS = _scenarioRepository.Get(correlatedSektor.ScenarioId);
                            cs.NamaSektor = correlatedSektor.NamaSektor;
                            cs.ScenarioId = correlatedSektor.ScenarioId;
                            cs.SektorId = correlatedSektor.SektorId;
                            cs.StatusId = correlatedSektor.StatusId;
                            cs.CreateBy = correlatedSektor.CreateBy;
                            cs.CreateDate = correlatedSektor.CreateDate;
                            cs.UpdateBy = correlatedSektor.UpdateBy;
                            cs.UpdateDate = correlatedSektor.UpdateDate;

                            StatusExtend statusExtendS = new StatusExtend();
                            var statusS = _statusRepository.Get(result[i].StatusId);
                            statusExtendS.StatusDescription = statusS.StatusDescription;

                            UserExtend userExtendS = new UserExtend();
                            var userS = _userRepository.Get(result[i].CreateBy.GetValueOrDefault());
                            userExtendS.UserName = userS.UserName;
                            userExtendS.Email = userS.Email;
                            userExtendS.Id = userS.Id;

                            UserExtend userApproveS = new UserExtend();
                            var userAS = _userRepository.Get(result[i].UpdateBy.GetValueOrDefault());
                            if (userAS != null)
                            {
                                userApproveS.UserName = userAS.UserName;
                                userApproveS.Email = userAS.Email;
                                userApproveS.Id = userAS.Id;
                            }
                            else
                            {
                                userApproveS.UserName = "";
                                userApproveS.Email = "";
                                userApproveS.Id = 0;
                            }


                            SektorExtend sektorExtendS = new SektorExtend();
                            var sektor = _sektorRepository.Get(cs.SektorId);
                            sektorExtendS.NamaSektor = sektor.NamaSektor;

                            objCS.CorrelatedSektor = cs;
                            objCS.Id = result[i].Id;
                            objCS.NamaScenario = scenarioS.NamaScenario;
                            objCS.StatusApproval = result[i].StatusApproval;
                            objCS.SourceApproval = result[i].SourceApproval;
                            objCS.CreateBy = result[i].CreateBy;
                            objCS.CreateDate = result[i].CreateDate;
                            objCS.UpdateBy = result[i].UpdateBy;
                            objCS.UpdateDate = result[i].UpdateDate;
                            objCS.IsDelete = result[i].IsDelete;
                            objCS.DeleteDate = result[i].DeleteDate;
                            objCS.Keterangan = result[i].Keterangan;
                            objCS.IsActive = result[i].IsActive;
                            objCS.StatusId = result[i].StatusId;
                            objCS.RequestId = result[i].RequestId;
                            objCS.NomorUrutStatus = result[i].NomorUrutStatus;
                            objCS.Status = statusExtendS;
                            objCS.User = userExtendS;
                            objCS.NamaEntitas = sektorExtendS.NamaSektor;
                            objCS.UserApprove = userApproveS;
                            approvalList.Add(objCS);
                            break;
                        #endregion correlationrisk

                        #region correlationproject
                        case "CorrelatedProject":
                            ApprovalExtend objCP = new ApprovalExtend();
                            CorrelatedProjectExtend cp = new CorrelatedProjectExtend();
                            var correlatedProject = _correlatedProjectRepository.Get(result[i].RequestId);
                            Validate.NotNull(correlatedProject, "Korelasi Proyek tidak ditemukan.");
                            var scenarioP = _scenarioRepository.Get(correlatedProject.ScenarioId);
                            cp.ProjectId = correlatedProject.ProjectId;
                            cp.ProjectName = correlatedProject.Project.NamaProject;
                            cp.ScenarioId = correlatedProject.ScenarioId;
                            cp.SektorId = correlatedProject.SektorId;
                            cp.StatusId = correlatedProject.StatusId;
                            cp.CreateBy = result[i].CreateBy;
                            cp.CreateDate = result[i].CreateDate;
                            cp.UpdateBy = result[i].UpdateBy;
                            cp.UpdateDate = result[i].UpdateDate;
                            cp.IsDelete = result[i].IsDelete;
                            cp.DeleteDate = result[i].DeleteDate;
                            cp.IsApproved = correlatedProject.IsApproved;

                            StatusExtend statusExtendP = new StatusExtend();
                            var statusP = _statusRepository.Get(result[i].StatusId);
                            statusExtendP.StatusDescription = statusP.StatusDescription;

                            UserExtend userExtendP = new UserExtend();
                            var userP = _userRepository.Get(result[i].CreateBy.GetValueOrDefault());
                            userExtendP.UserName = userP.UserName;
                            userExtendP.Email = userP.Email;
                            userExtendP.Id = userP.Id;

                            UserExtend userApproveP = new UserExtend();
                            var userAP = _userRepository.Get(result[i].UpdateBy.GetValueOrDefault());
                            if (userAP != null)
                            {
                                userApproveP.UserName = userAP.UserName;
                                userApproveP.Email = userAP.Email;
                                userApproveP.Id = userAP.Id;
                            }
                            else
                            {
                                userApproveP.UserName = "";
                                userApproveP.Email = "";
                                userApproveP.Id = 0;
                            }


                            ProjectExtend projectExtendP = new ProjectExtend();
                            var projectP = _projectRepository.Get(cp.ProjectId);

                            projectExtendP.NamaProject = projectP.NamaProject;

                            objCP.CorrelatedProject = cp;
                            objCP.Id = result[i].Id;
                            objCP.NamaScenario = scenarioP.NamaScenario;
                            objCP.StatusApproval = result[i].StatusApproval;
                            objCP.SourceApproval = result[i].SourceApproval;
                            objCP.CreateBy = result[i].CreateBy;
                            objCP.CreateDate = result[i].CreateDate;
                            objCP.UpdateBy = result[i].UpdateBy;
                            objCP.UpdateDate = result[i].UpdateDate;
                            objCP.IsDelete = result[i].IsDelete;
                            objCP.DeleteDate = result[i].DeleteDate;
                            objCP.Keterangan = result[i].Keterangan;
                            objCP.IsActive = result[i].IsActive;
                            objCP.StatusId = result[i].StatusId;
                            objCP.RequestId = result[i].RequestId;
                            objCP.NomorUrutStatus = result[i].NomorUrutStatus;
                            objCP.Status = statusExtendP;
                            objCP.User = userExtendP;
                            objCP.NamaEntitas = projectExtendP.NamaProject;
                            objCP.UserApprove = userApproveP;
                            approvalList.Add(objCP);
                            break;
                            #endregion correlationproject
                    }
                }
            }

            return approvalList;
        }


        //public IEnumerable<ApprovalExtend> GetItem(string keyword, int id, bool sentItem, int userId)
        //{
        //    //var result = _approvalRepository.GetItem();

        //    //var scenarioSent = _scenarioRepository.GetAll().Where(x => x.StatusId == 1 && x.CreateBy == userId);

        //    #region asign entity into object
        //    IList<ApprovalExtend> approvalList = new List<ApprovalExtend>();

        //    //get role user from master scenario
        //    var masterApprovalScenario = _masterApprovalScenarioRepository.GetByUserId(userId);

        //    //get role user from master scenario
        //    var masterApprovalCorrelatedSektor = _masterApprovalCorrelatedSektorRepository.GetByUserId(userId);

        //    for (int i = 0; i < result.Count; i++)
        //    {
        //        switch (result[i].SourceApproval)
        //        {
        //            #region scenario
        //            case "Scenario":
        //                if (masterApprovalScenario.NomorUrutStatus - 1 == result[i].NomorUrutStatus)
        //                {
        //                    ApprovalExtend obj = new ApprovalExtend();

        //                    var scenario = _scenarioRepository.Get(result[i].RequestId);
        //                    Validate.NotNull(scenario, "Skenario tidak ditemukan.");

        //                    scenarioExt.NamaScenario = scenario.NamaScenario;
        //                    scenarioExt.IsDefault = scenario.IsDefault;
        //                    scenarioExt.CreateBy = scenario.CreateBy;
        //                    scenarioExt.UpdateBy = scenario.UpdateBy;
        //                    scenarioExt.UpdateDate = scenario.UpdateDate;
        //                    scenarioExt.LikehoodId = scenario.LikehoodId;
        //                    scenarioExt.StatusId = scenario.StatusId;

        //                    StatusExtend statusExtend = new StatusExtend();
        //                    var status = _statusRepository.Get(result[i].StatusId);
        //                    statusExtend.StatusDescription = status.StatusDescription;

        //                    UserExtend userExtend = new UserExtend();
        //                    var user = _userRepository.Get(result[i].CreateBy.GetValueOrDefault());
        //                    userExtend.UserName = user.UserName;
        //                    userExtend.Email = user.Email;
        //                    userExtend.Id = user.Id;


        //                    obj.Scenario = scenarioExt;
        //                    obj.Id = result[i].Id;
        //                    obj.NamaScenario = scenario.NamaScenario;
        //                    obj.StatusApproval = result[i].StatusApproval;
        //                    obj.SourceApproval = result[i].SourceApproval;
        //                    obj.CreateBy = result[i].CreateBy;
        //                    obj.CreateDate = result[i].CreateDate;
        //                    obj.UpdateBy = result[i].UpdateBy;
        //                    obj.UpdateDate = result[i].UpdateDate;
        //                    obj.IsDelete = result[i].IsDelete;
        //                    obj.DeleteDate = result[i].DeleteDate;
        //                    obj.Keterangan = result[i].Keterangan;
        //                    obj.IsActive = result[i].IsActive;
        //                    obj.StatusId = result[i].StatusId;
        //                    obj.RequestId = result[i].RequestId;
        //                    obj.NomorUrutStatus = result[i].NomorUrutStatus;
        //                    obj.Status = statusExtend;
        //                    obj.User = userExtend;
        //                    obj.NamaEntitas = scenario.NamaScenario;
        //                    approvalList.Add(obj);
        //                }
                        
        //                break;
        //            #endregion scenario

        //            #region riskmatrixproject
        //            case "RiskMatrixProject":
        //                //get role user from master scenario
        //                var masterApprovalRiskMatrixProject = _masterApprovalRiskMatrixProjectRepository.GetByUserId(userId);

        //                ApprovalExtend objR = new ApprovalExtend();
        //                RiskMatrixProjectExtend rm = new RiskMatrixProjectExtend();
        //                var riskMatrix = _riskMatrixProjectRepository.Get(result[i].RequestId);
        //                Validate.NotNull(riskMatrix, "Risk Matrix Proyek tidak ditemukan.");

        //                rm.ProjectId = riskMatrix.ProjectId;
        //                rm.RiskMatrixId = riskMatrix.RiskMatrixId;
        //                rm.ScenarioId = riskMatrix.ScenarioId;
        //                rm.StatusId = riskMatrix.StatusId;
        //                rm.CreateBy = riskMatrix.CreateBy;
        //                rm.CreateDate = riskMatrix.CreateDate;
        //                rm.UpdateBy = riskMatrix.UpdateBy;
        //                rm.UpdateDate = riskMatrix.UpdateDate;

        //                StatusExtend statusExtendR = new StatusExtend();
        //                var statusR = _statusRepository.Get(result[i].StatusId);
        //                statusExtendR.StatusDescription = statusR.StatusDescription;

        //                UserExtend userExtendR = new UserExtend();
        //                var userR = _userRepository.Get(result[i].CreateBy.GetValueOrDefault());
        //                userExtendR.UserName = userR.UserName;
        //                userExtendR.Email = userR.Email;
        //                userExtendR.Id = userR.Id;

        //                ProjectExtend pR = new ProjectExtend();
        //                var project = _projectRepository.Get(rm.ProjectId);
        //                pR.NamaProject = project.NamaProject;

        //                objR.RiskMatrixProject = rm;
        //                objR.Id = result[i].Id;
        //                objR.NamaScenario = riskMatrix.Scenario.NamaScenario;
        //                objR.StatusApproval = result[i].StatusApproval;
        //                objR.SourceApproval = result[i].SourceApproval;
        //                objR.CreateBy = result[i].CreateBy;
        //                objR.CreateDate = result[i].CreateDate;
        //                objR.UpdateBy = result[i].UpdateBy;
        //                objR.UpdateDate = result[i].UpdateDate;
        //                objR.IsDelete = result[i].IsDelete;
        //                objR.DeleteDate = result[i].DeleteDate;
        //                objR.Keterangan = result[i].Keterangan;
        //                objR.IsActive = result[i].IsActive;
        //                objR.StatusId = result[i].StatusId;
        //                objR.RequestId = result[i].RequestId;
        //                objR.NomorUrutStatus = result[i].NomorUrutStatus;
        //                objR.Status = statusExtendR;
        //                objR.User = userExtendR;
        //                objR.NamaEntitas = pR.NamaProject;
        //                approvalList.Add(objR);
        //                break;
        //            #endregion riskmatrixproject

        //            #region correlationrisk
        //            case "CorrelatedSektor":
        //                ApprovalExtend objCS = new ApprovalExtend();
        //                CorrelatedSektorExtend cs = new CorrelatedSektorExtend();
        //                var correlatedSektor = _correlatedSectorRepository.Get(result[i].RequestId);
        //                Validate.NotNull(correlatedSektor, "Korelasi Sektor tidak ditemukan.");

        //                cs.NamaSektor = correlatedSektor.NamaSektor;
        //                cs.ScenarioId = correlatedSektor.ScenarioId;
        //                cs.SektorId = correlatedSektor.SektorId;
        //                cs.StatusId = correlatedSektor.StatusId;
        //                cs.CreateBy = correlatedSektor.CreateBy;
        //                cs.CreateDate = correlatedSektor.CreateDate;
        //                cs.UpdateBy = correlatedSektor.UpdateBy;
        //                cs.UpdateDate = correlatedSektor.UpdateDate;

        //                StatusExtend statusExtendS = new StatusExtend();
        //                var statusS = _statusRepository.Get(result[i].StatusId);
        //                statusExtendS.StatusDescription = statusS.StatusDescription;

        //                UserExtend userExtendS = new UserExtend();
        //                var userS = _userRepository.Get(result[i].CreateBy.GetValueOrDefault());
        //                userExtendS.UserName = userS.UserName;
        //                userExtendS.Email = userS.Email;
        //                userExtendS.Id = userS.Id;

        //                SektorExtend sektorExtendS = new SektorExtend();
        //                var sektor = _sektorRepository.Get(cs.SektorId);
        //                sektorExtendS.NamaSektor = sektor.NamaSektor;

        //                objCS.CorrelatedSektor = cs;
        //                objCS.Id = result[i].Id;
        //                objCS.NamaScenario = "asasa";
        //                objCS.StatusApproval = result[i].StatusApproval;
        //                objCS.SourceApproval = result[i].SourceApproval;
        //                objCS.CreateBy = result[i].CreateBy;
        //                objCS.CreateDate = result[i].CreateDate;
        //                objCS.UpdateBy = result[i].UpdateBy;
        //                objCS.UpdateDate = result[i].UpdateDate;
        //                objCS.IsDelete = result[i].IsDelete;
        //                objCS.DeleteDate = result[i].DeleteDate;
        //                objCS.Keterangan = result[i].Keterangan;
        //                objCS.IsActive = result[i].IsActive;
        //                objCS.StatusId = result[i].StatusId;
        //                objCS.RequestId = result[i].RequestId;
        //                objCS.NomorUrutStatus = result[i].NomorUrutStatus;
        //                objCS.Status = statusExtendS;
        //                objCS.User = userExtendS;
        //                objCS.NamaEntitas = sektorExtendS.NamaSektor;
        //                approvalList.Add(objCS);
        //                break;
        //            #endregion correlationrisk

        //            #region correlationproject
        //            case "CorrelatedProject":
        //                //get role user from master scenario
        //                var masterApprovalCorrelatedProject = _masterApprovalCorrelatedProjectRepository.GetByUserId(userId);


        //                ApprovalExtend objCP = new ApprovalExtend();
        //                CorrelatedProjectExtend cp = new CorrelatedProjectExtend();
        //                var correlatedProject = _correlatedProjectRepository.Get(result[i].RequestId);
        //                Validate.NotNull(correlatedProject, "Korelasi Proyek tidak ditemukan.");

        //                cp.ProjectId = correlatedProject.ProjectId;
        //                cp.ProjectName = correlatedProject.Project.NamaProject;
        //                cp.ScenarioId = correlatedProject.ScenarioId;
        //                cp.SektorId = correlatedProject.SektorId;
        //                cp.StatusId = correlatedProject.StatusId;
        //                cp.CreateBy = result[i].CreateBy;
        //                cp.CreateDate = result[i].CreateDate;
        //                cp.UpdateBy = result[i].UpdateBy;
        //                cp.UpdateDate = result[i].UpdateDate;
        //                cp.IsDelete = result[i].IsDelete;
        //                cp.DeleteDate = result[i].DeleteDate;
        //                cp.IsApproved = correlatedProject.IsApproved;

        //                StatusExtend statusExtendP = new StatusExtend();
        //                var statusP = _statusRepository.Get(result[i].StatusId);
        //                statusExtendP.StatusDescription = statusP.StatusDescription;

        //                UserExtend userExtendP = new UserExtend();
        //                var userP = _userRepository.Get(result[i].CreateBy.GetValueOrDefault());
        //                userExtendP.UserName = userP.UserName;
        //                userExtendP.Email = userP.Email;
        //                userExtendP.Id = userP.Id;

        //                ProjectExtend projectExtendP = new ProjectExtend();
        //                var projectP = _projectRepository.Get(cp.ProjectId);

        //                projectExtendP.NamaProject = projectP.NamaProject;

        //                objCP.CorrelatedProject = cp;
        //                objCP.Id = result[i].Id;
        //                objCP.NamaScenario = "asasasa";
        //                objCP.StatusApproval = result[i].StatusApproval;
        //                objCP.SourceApproval = result[i].SourceApproval;
        //                objCP.CreateBy = result[i].CreateBy;
        //                objCP.CreateDate = result[i].CreateDate;
        //                objCP.UpdateBy = result[i].UpdateBy;
        //                objCP.UpdateDate = result[i].UpdateDate;
        //                objCP.IsDelete = result[i].IsDelete;
        //                objCP.DeleteDate = result[i].DeleteDate;
        //                objCP.Keterangan = result[i].Keterangan;
        //                objCP.IsActive = result[i].IsActive;
        //                objCP.StatusId = result[i].StatusId;
        //                objCP.RequestId = result[i].RequestId;
        //                objCP.NomorUrutStatus = result[i].NomorUrutStatus;
        //                objCP.Status = statusExtendP;
        //                objCP.User = userExtendP;
        //                objCP.NamaEntitas = projectExtendP.NamaProject;
        //                approvalList.Add(objCP);
        //                break;
        //                #endregion correlationproject
        //        }
        //    }
        //    #endregion asign entity into object

        //    return approvalList;
        //}
        #endregion Query

        #region Manipulation 
        public int Add(ApprovalParam param)
        {
            int id;
            if (param.NomorUrutStatus != 3 && param.StatusId == 2)
            {
                param.StatusId = 1;
            }
            var status = _statusRepository.Get(param.StatusId.GetValueOrDefault());
            Validate.NotNull(status, "Status tidak ditemukan.");

            Approval model = new Approval(param.RequestId.GetValueOrDefault(), param.StatusApproval, param.SourceApproval,
                status, param.CreateBy, param.CreateDate,  param.Keterangan, param.NomorUrutStatus.GetValueOrDefault());
            _approvalRepository.Insert(model);
            _databaseContext.SaveChanges();

            id = model.Id;

            if (model.NomorUrutStatus == 3 && model.StatusId == 2)
            {
                model.RemoveActive(param.CreateBy, param.CreateDate);
            }
            else if (model.StatusId == 3)
            {
                var modelDeleteList = _approvalRepository.GetAllBaseOnSource(model.RequestId, model.SourceApproval);

                foreach (var item in modelDeleteList)
                {
                    item.RemoveActive(param.CreateBy, param.CreateDate);
                    item.Delete(param.CreateBy.GetValueOrDefault(), param.CreateDate.GetValueOrDefault());
                }
            }
            _databaseContext.SaveChanges();
            return id;
        }        

        public int UpdateStatus(int id, ApprovalParam param)
        {
            //Get Approval dari ID
            var modelOld = this.Get(id);
            param.Id = modelOld.Id;
            Validate.NotNull(modelOld, "Approval tidak ditemukan.");
            
            //Get user yang melakukan Approval
            var userUpdate = _databaseContext.Users.SingleOrDefault(x => x.Id == param.UpdateBy);
            Validate.NotNull(userUpdate, "User tidak ditemukan.");

            //Get status dari user yang membuat data tersebut menggunakan roleId
            var statusUserUpdate = _statusRepository.Get(param.StatusId.GetValueOrDefault());
            Validate.NotNull(statusUserUpdate, "Status tidak ditemukan.");

            Role roleUserUpdate = _userRepository.GetRoleUser(userUpdate.RoleId);
            Validate.NotNull(roleUserUpdate, "Role User tidak ditemukan.");

            param.SourceApproval = modelOld.SourceApproval;

            //Get request Id dari model lama ke parameter
            param.RequestId = modelOld.RequestId;
            //get role dari user yang melakukan update
            int? nomorUrutStatus = SourceMasterApproval(param);
            Validate.NotNull(nomorUrutStatus, "Nomor Urut Status tidak ditemukan.");
            param.NomorUrutStatus = nomorUrutStatus.GetValueOrDefault();

            //mencek apakah statusnya reject
            if (modelOld.StatusId == 3)
            {
                //lemparkan validasi bahwa approvalnya sudah di reject
                throw new ApplicationException(string.Format("Approval dengan Id ini telah direject."));
            }
            else
            {
                if (modelOld != null)
                {
                    //Remove current default
                    modelOld.RemoveActive(param.UpdateBy, param.UpdateDate);
                    _approvalRepository.Update(modelOld);
                    _databaseContext.SaveChanges();
                }

                switch (modelOld.SourceApproval)
                {
                    //Jika pilih 
                    case "Scenario":
                        SetDefaultScenario(param, userUpdate, statusUserUpdate, roleUserUpdate);
                        break;
                    case "RiskMatrixProject":
                        SetDefaultRiskMatrixProject(param, userUpdate, statusUserUpdate, roleUserUpdate);
                        break;
                    case "CorrelatedProject":
                        SetDefaultCorrelatedProject(param, userUpdate, statusUserUpdate, roleUserUpdate);
                        break;
                    case "CorrelatedSektor":
                        SetDefaultCorrelatedSektor(param, userUpdate, statusUserUpdate, roleUserUpdate);
                        break;
                    default:
                        throw new ApplicationException(string.Format("Sumber Approval tidak bisa ditemukan"));
                        break;
                }
            }
            return id;
        }
        
        #endregion Manipulation

        #region Private
        //Memilih update untuk ke tabel yang mana
        private void SourceApproval(ApprovalParam param, Status status)
        {
            switch (param.SourceApproval)
            {
                //Jika pilih 
                case "Scenario":
                    UpdateStatusScenario(param, status);
                    break;
                case "CorrelatedSektor":
                    UpdateStatusCorrelatedSektor(param, status);
                    break;
                case "RiskMatrixProject":
                    UpdateStatusRiskMatrixProjects(param, status);
                    break;
                case "CorrelatedProject":
                    UpdateStatusCorrelatedProject(param, status);
                    break;
                default:
                    break;
            }
        }

        //Memilih update untuk ke tabel yang mana
        private int? SourceMasterApproval(ApprovalParam param)
        {
            switch (param.SourceApproval)
            {
                //Jika pilih 
                case "Scenario":
                    return MasterApprovalScenario(param);
                case "RiskMatrixProject":
                    return MasterApprovalRiskMatrixProject(param);
                case "CorrelatedSektor":
                    return MasterApprovalCorrelatedSektor(param);
                case "CorrelatedProject":
                    return MasterApprovalCorrelatedProject(param); 
                default:                  
                    return null;
            }
        }

        private int? MasterApprovalScenario(ApprovalParam param)
        {
            //get role level from user id
            var masterApproval = _masterApprovalScenarioRepository.GetByUserId(param.UpdateBy.GetValueOrDefault());
            Validate.NotNull(masterApproval, "Master Approval Scenario untuk User {} belum dibuat.");
            
            return masterApproval.NomorUrutStatus;
        }

        private int? MasterApprovalRiskMatrixProject(ApprovalParam param)
        {
            var projectRiskMatrixProject = _riskMatrixProjectRepository.Get(param.RequestId.GetValueOrDefault());
            //get role level from user id
            var masterApproval = _masterApprovalRiskMatrixProjectRepository.GetByProjectIdUserId(projectRiskMatrixProject.ProjectId,param.UpdateBy.GetValueOrDefault());
            Validate.NotNull(masterApproval, "Master Approval Risk Matrix Project untuk User {} belum dibuat.");
 
            return masterApproval.NomorUrutStatus;
        }

        private int? MasterApprovalCorrelatedSektor(ApprovalParam param)
        {
            var masterApproval = _masterApprovalCorrelatedSektorRepository.GetByUserId(param.UpdateBy.GetValueOrDefault());
            Validate.NotNull(masterApproval, "Master Approval Correlated Sektor untuk User {} belum dibuat.");

            return masterApproval.NomorUrutStatus;
        }

        private int? MasterApprovalCorrelatedProject(ApprovalParam param)
        {
            var projectRiskMatrixProject = _correlatedProjectRepository.Get(param.RequestId.GetValueOrDefault());
            //get role level from user id
            var masterApproval = _masterApprovalCorrelatedProjectRepository.GetByProjectIdUserId(projectRiskMatrixProject.ProjectId, param.UpdateBy.GetValueOrDefault());
            Validate.NotNull(masterApproval, "Master Approval Correlated Project untuk User {} belum dibuat.");

            return masterApproval.NomorUrutStatus;
        }

        private void UpdateScenario(ApprovalParam param)
        {
            Scenario oldScenario = _scenarioRepository.Get(param.RequestId);

            var scenarioParam = new ScenarioParam
            {
                CreateBy = oldScenario.CreateBy,
                CreateDate = oldScenario.CreateDate,
                IsDefault = oldScenario.IsDefault,
                IsDelete = oldScenario.IsDelete,
                DeleteDate = oldScenario.DeleteDate,
                IsUpdate = true,
                LikehoodId = param.LikehoodId.GetValueOrDefault(),
                NamaScenario = oldScenario.NamaScenario,
                ProjectId = param.ProjectId,
                UpdateBy = param.UpdateBy,
                UpdateDate = param.UpdateDate,
                IsStatusApproval = true,
                StatusId = param.StatusId,
                NomorUrutStatus = param.NomorUrutStatus,
                TypePesan = param.TypePesan,
                Keterangan = param.Keterangan,
                TemplateNotif = param.TemplateNotif
            };

            if (param.NomorUrutStatus == 3)
            {
                _scenarioService.Update2(param.RequestId.GetValueOrDefault(), scenarioParam);
            }
            else
            {
                _scenarioService.Update(param.RequestId.GetValueOrDefault(), scenarioParam);
            }

        }

        private void UpdateScenarioMurni(ApprovalParam param)
        {
            Scenario oldScenario = _scenarioRepository.Get(param.RequestId);

            var scenarioParam = new ScenarioParam
            {
                CreateBy = oldScenario.CreateBy,
                CreateDate = oldScenario.CreateDate,
                IsDefault = oldScenario.IsDefault,
                IsDelete = oldScenario.IsDelete,
                DeleteDate = oldScenario.DeleteDate,
                IsUpdate = true,
                LikehoodId = param.LikehoodId.GetValueOrDefault(),
                NamaScenario = oldScenario.NamaScenario,
                ProjectId = param.ProjectId,
                UpdateBy = param.UpdateBy,
                UpdateDate = param.UpdateDate,
                IsStatusApproval = true,
                StatusId = param.StatusId,
                NomorUrutStatus = param.NomorUrutStatus,
                TypePesan = param.TypePesan
            };

            _scenarioService.UpdateScenarioMurni(oldScenario.Id, scenarioParam);

            var current = _approvalRepository.Get(param.Id.GetValueOrDefault());
            if (current != null)
            {
                current.SetIsActive();
                current.UpdateKeterangan(param.Keterangan);
            }
            _databaseContext.SaveChanges();
        }

        private int DraftScenario(ApprovalParam param)
        {
            int id = 0;

            Scenario oldScenario = _scenarioRepository.Get(param.RequestId);

            var scenarioParam = new ScenarioParam
            {
                CreateBy = oldScenario.CreateBy,
                CreateDate = param.UpdateDate,
                IsDefault = oldScenario.IsDefault,
                IsDelete = oldScenario.IsDelete,
                DeleteDate = oldScenario.DeleteDate,
                IsUpdate = true,
                LikehoodId = param.LikehoodId.GetValueOrDefault(),
                NamaScenario = oldScenario.NamaScenario,
                ProjectId = param.ProjectId,
                UpdateBy = param.UpdateBy,
                UpdateDate = param.UpdateDate,
                IsStatusApproval = true,
                StatusId = oldScenario.StatusId,
                NomorUrutStatus = param.NomorUrutStatus,
                TypePesan = param.TypePesan
            };
            id = _scenarioService.Duplicate(scenarioParam);

            _databaseContext.SaveChanges();
            if (id == 0)
            {
                throw new ApplicationException(string.Format("Draft Approval Scenario belum terbuat."));
            }
            return id;
        }

        private void DeleteOtherScenarioDraft(ApprovalParam param)
        {
            Scenario oldScenario = _scenarioRepository.Get(param.RequestId);

            var scenarioList = _scenarioRepository.GetAllByName(oldScenario.Id).Where(x => x.StatusId == 4 && x.IsDelete == false);

            Status status = null;

            if (scenarioList.Count() > 0)
            {
                foreach (var item in scenarioList)
                {
                    item.Delete(param.UpdateBy.GetValueOrDefault(), param.UpdateDate.GetValueOrDefault());
                    _scenarioRepository.Update(item);
                    var approval = _approvalRepository.GetAllBaseOnSource(item.Id,  "Scenario");
                    foreach (var item2 in approval)
                    {
                        item2.RemoveActive(param.UpdateBy.GetValueOrDefault(), param.UpdateDate.GetValueOrDefault());
                        _approvalRepository.Update(item2);
                    }
                }
            }
            _databaseContext.SaveChanges();

        }

        private void UpdateCorrelatedSektorDetail(ApprovalParam param)
        {
            CorrelatedSektor correlatedSektor = _correlatedSectorRepository.Get(param.RequestId.GetValueOrDefault());

            var correlatedSektorDetailParam = new CorrelatedSektorDetailCollectionParam
            {
                CorrelatedSektorId = param.CorrelatedSektorId.GetValueOrDefault(),
                CorrelatedSektorDetailCollection = param.CorrelatedSektorDetailCollection,
                CreateBy = correlatedSektor.CreateBy,
                CreateDate = correlatedSektor.CreateDate,
                DeleteDate = correlatedSektor.DeleteDate,
                IsDelete = correlatedSektor.IsDelete,
                UpdateBy = param.UpdateBy,
                IsStatusApproval = true,
                UpdateDate = param.UpdateDate,
                Keterangan = param.Keterangan,
                StatusId = param.StatusId,
                TemplateNotif = param.TemplateNotif,
                NomorUrutStatus = param.NomorUrutStatus,
            };
            if (param.NomorUrutStatus == 3)
            {
                _correlatedSektorDetailService.AddMurni(correlatedSektorDetailParam);
                int id = _correlatedSektorDetailService.Update2(param.RequestId.GetValueOrDefault(), correlatedSektorDetailParam);
            }
            else
            {
                int id = _correlatedSektorDetailService.Add( correlatedSektorDetailParam);
            }


        }

        private void UpdateCorrelatedSektorDetailMurni(ApprovalParam param)
        {
            CorrelatedSektor correlatedSektor = _correlatedSectorRepository.Get(param.RequestId.GetValueOrDefault());

            var correlatedSektorDetailParam = new CorrelatedSektorDetailCollectionParam
            {
                CorrelatedSektorId = param.CorrelatedSektorId.GetValueOrDefault(),
                CorrelatedSektorDetailCollection = param.CorrelatedSektorDetailCollection,
                CreateBy = correlatedSektor.CreateBy,
                CreateDate = correlatedSektor.CreateDate,
                DeleteDate = correlatedSektor.DeleteDate,
                IsDelete = correlatedSektor.IsDelete,
                UpdateBy = param.UpdateBy,
                IsStatusApproval = true,
                UpdateDate = param.UpdateDate
            };
            _correlatedSektorDetailService.AddMurni(correlatedSektorDetailParam);

            var current = _approvalRepository.Get(param.Id.GetValueOrDefault());
            if (current != null)
            {
                current.SetIsActive();
                current.UpdateKeterangan(param.Keterangan);
            }
            _databaseContext.SaveChanges();

        }

        private int DraftCorrelatedSektor(ApprovalParam param)
        {
            int id = 0;

            CorrelatedSektor correlatedSektor = _correlatedSectorRepository.Get(param.RequestId.GetValueOrDefault());

            var correlatedSektorDetailParam = new CorrelatedSektorDetailCollectionParam
            {
                CorrelatedSektorId = param.CorrelatedSektorId.GetValueOrDefault(),
                CorrelatedSektorDetailCollection = param.CorrelatedSektorDetailCollection,
                CreateBy = correlatedSektor.CreateBy,
                CreateDate = correlatedSektor.CreateDate,
                DeleteDate = correlatedSektor.DeleteDate,
                IsDelete = correlatedSektor.IsDelete,
                UpdateBy = param.UpdateBy,
                IsStatusApproval = true,
                UpdateDate = param.UpdateDate
            };

            id = _correlatedSektorDetailService.Duplicate(correlatedSektorDetailParam);

            _databaseContext.SaveChanges();
            if (id == 0)
            {
                throw new ApplicationException(string.Format("Draft Approval Correlated Sector belum terbuat."));
            }

            return id;

        }

        private void DeleteOtherCorrelatedSektorDraft(ApprovalParam param)
        {
            CorrelatedSektor oldCorrelatedSektor = _correlatedSectorRepository.Get(param.RequestId.GetValueOrDefault());

            var correlatedSektorList = _correlatedSectorRepository.GetByScenarioIdSektorIdList(oldCorrelatedSektor.ScenarioId, oldCorrelatedSektor.SektorId).Where(x => x.StatusId == 4 && x.IsDelete == false);

            Status status = null;

            if (correlatedSektorList.Count() > 0)
            {
                foreach (var item in correlatedSektorList)
                {
                    item.Delete(param.UpdateBy.GetValueOrDefault(), param.UpdateDate.GetValueOrDefault());
                    _correlatedSectorRepository.Update(item);
                    var approval = _approvalRepository.GetAllBaseOnSource(item.Id, "CorrelatedSektor");
                    foreach (var item2 in approval)
                    {
                        item2.RemoveActive(param.UpdateBy.GetValueOrDefault(), param.UpdateDate.GetValueOrDefault());
                        _approvalRepository.Update(item2);
                    }
                }
            }
            _databaseContext.SaveChanges();

        }

        private void UpdateCorrelatedProjectDetail(ApprovalParam param)
        {
            CorrelatedProject correlatedProject = _correlatedProjectRepository.Get(param.RequestId.GetValueOrDefault());

            var correlatedProjectDetailParam = new CorrelatedProjectDetailCollectionParam
            {
                CorrelatedProjectDetailCollection = param.CorrelatedProjectDetailCollection,
                CorrelatedProjectId = correlatedProject.Id,
                CreateBy = correlatedProject.CreateBy,
                CreateDate = correlatedProject.CreateDate,
                DeleteDate = correlatedProject.DeleteDate,
                IsDelete = correlatedProject.IsDelete,
                NamaProject = param.NamaProject,
                NamaSektor = param.NamaSektor,
                ProjectId = param.ProjectIdCP.GetValueOrDefault(),
                SektorId = param.SektorId.GetValueOrDefault(),
                UpdateBy = param.UpdateBy,
                IsStatusApproval = true,
                StatusId = param.StatusId,
                UpdateDate = param.UpdateDate,
                Keterangan = param.Keterangan,
                TemplateNotif = param.TemplateNotif,
                NomorUrutStatus = param.NomorUrutStatus,
            };
            if (param.NomorUrutStatus == 3)
            {
                _correlatedProjectDetailService.AddMurni(correlatedProjectDetailParam);
                int id = _correlatedProjectDetailService.Update2(param.RequestId.GetValueOrDefault(), correlatedProjectDetailParam);
            }
            else
            {
                int id = _correlatedProjectDetailService.Add(correlatedProjectDetailParam);
            }
           

        }

        private void UpdateCorrelatedProjectDetailMurni(ApprovalParam param)
        {
            CorrelatedProject correlatedProject = _correlatedProjectRepository.Get(param.RequestId.GetValueOrDefault());

            var correlatedProjectDetailParam = new CorrelatedProjectDetailCollectionParam
            {
                CorrelatedProjectDetailCollection = param.CorrelatedProjectDetailCollection,
                CorrelatedProjectId = correlatedProject.Id,
                CreateBy = correlatedProject.CreateBy,
                CreateDate = correlatedProject.CreateDate,
                DeleteDate = correlatedProject.DeleteDate,
                IsDelete = correlatedProject.IsDelete,
                NamaProject = param.NamaProject,
                NamaSektor = param.NamaSektor,
                ProjectId = param.ProjectIdCP.GetValueOrDefault(),
                SektorId = param.SektorId.GetValueOrDefault(),
                UpdateBy = param.UpdateBy,
                IsStatusApproval = true,
                UpdateDate = param.UpdateDate
            };

            _correlatedProjectDetailService.AddMurni(correlatedProjectDetailParam);

            var current = _approvalRepository.Get(param.Id.GetValueOrDefault());
            if (current != null)
            {
                current.SetIsActive();
                current.UpdateKeterangan(param.Keterangan);
            }
            _databaseContext.SaveChanges();

        }

        private int DraftCorrelatedProject(ApprovalParam param)
        {
            int id = 0;

            CorrelatedProject oldCorrelatedProject = _correlatedProjectRepository.Get(param.RequestId.GetValueOrDefault());
            Validate.NotNull(oldCorrelatedProject, "Correlated Project tidak ditemukan.");

            var correlatedProjectDetailParam = new CorrelatedProjectDetailCollectionParam
            {
                CorrelatedProjectDetailCollection = param.CorrelatedProjectDetailCollection,
                CorrelatedProjectId = oldCorrelatedProject.Id,
                CreateBy = oldCorrelatedProject.CreateBy,
                CreateDate = oldCorrelatedProject.CreateDate,
                DeleteDate = oldCorrelatedProject.DeleteDate,
                IsDelete = oldCorrelatedProject.IsDelete,
                NamaProject = param.NamaProject,
                NamaSektor = param.NamaSektor,
                ProjectId = param.ProjectIdCP.GetValueOrDefault(),
                SektorId = param.SektorId.GetValueOrDefault(),
                UpdateBy = param.UpdateBy,
                IsStatusApproval = true,
                UpdateDate = param.UpdateDate,
                StatusId = param.StatusId
            };

            id = _correlatedProjectDetailService.Duplicate(correlatedProjectDetailParam);

            _databaseContext.SaveChanges();
            if (id == 0)
            {
                throw new ApplicationException(string.Format("Draft Approval Correlated Project belum terbuat."));
            }

            return id;

        }

        private void DeleteOtherCorrelatedProjectDraft(ApprovalParam param)
        {
            CorrelatedProject oldCorrelatedProject = _correlatedProjectRepository.Get(param.RequestId.GetValueOrDefault());

            var correlatedProjectList = _correlatedProjectRepository.GetByScenarioProjectSektorId(oldCorrelatedProject.ScenarioId, oldCorrelatedProject.ProjectId, oldCorrelatedProject.SektorId).Where(x => x.StatusId == 4 && x.IsDelete == false);

            Status status = null;

            if (correlatedProjectList.Count() > 0)
            {
                foreach (var item in correlatedProjectList)
                {
                    item.Delete(param.UpdateBy.GetValueOrDefault(), param.UpdateDate.GetValueOrDefault());
                    _correlatedProjectRepository.Update(item);
                    var approval = _approvalRepository.GetAllBaseOnSource(item.Id, "CorrelatedProject");
                    foreach (var item2 in approval)
                    {
                        item2.RemoveActive(param.UpdateBy.GetValueOrDefault(), param.UpdateDate.GetValueOrDefault());
                        _approvalRepository.Update(item2);
                    }
                }
            }
            _databaseContext.SaveChanges();

        }

        private void UpdateRiskMatrixProjects(ApprovalParam param)
        {
            var stageTahunRiskMatrixDetailParam = new RiskMatrixCollectionParameter
            {
                RiskMatrixCollection = param.RiskMatrixCollection,
                UpdateBy = param.UpdateBy,
                UpdateDate = param.UpdateDate,
                IsStatusApproval = true,
                NomorUrutStatus = param.NomorUrutStatus,
                Keterangan = param.Keterangan,
                StatusId = param.StatusId,
                TemplateNotif = param.TemplateNotif
            };
            if (param.NomorUrutStatus == 3)
            {
                _stageTahunRiskMatrixDetailService.Update2(param.RequestId.GetValueOrDefault(), stageTahunRiskMatrixDetailParam);
            }
            else
            {
                _stageTahunRiskMatrixDetailService.Update(param.RequestId.GetValueOrDefault(), stageTahunRiskMatrixDetailParam);
            }
            
        }

        private void UpdateRiskMatrixProjectsMurni(ApprovalParam param)
        {
            var stageTahunRiskMatrixDetailParam = new RiskMatrixCollectionParameter
            {
                RiskMatrixCollection = param.RiskMatrixCollection,
                UpdateBy = param.UpdateBy,
                UpdateDate = param.UpdateDate,
                IsStatusApproval = true
            };

            _stageTahunRiskMatrixDetailService.UpdateMurni(param.RequestId.GetValueOrDefault(), stageTahunRiskMatrixDetailParam);

            var current = _approvalRepository.Get(param.Id.GetValueOrDefault());
            if (current != null)
            {
                current.SetIsActive();
                current.UpdateKeterangan(param.Keterangan);
            }

            _databaseContext.SaveChanges();

        }

        private int DraftRiskMatrixProjects(ApprovalParam param)
        {
            int id = 0;

            var stageTahunRiskMatrixDetailParam = new RiskMatrixCollectionParameter
            {
                RiskMatrixCollection = param.RiskMatrixCollection,
                UpdateBy = param.UpdateBy,
                UpdateDate = param.UpdateDate,
                IsStatusApproval = true
            };
            var stageTahunRiskMatrixWithId = _stageTahunRiskMatrixRepository.Get(param.RiskMatrixCollection[0].StageTahunRiskMatrixId);
            var riskMatrixProject = _riskMatrixProjectRepository.Get(stageTahunRiskMatrixWithId.RiskMatrixProjectId);

            id = _stageTahunRiskMatrixDetailService.Duplicate(stageTahunRiskMatrixDetailParam, riskMatrixProject);

            _databaseContext.SaveChanges();
            if (id == 0)
            {
                throw new ApplicationException(string.Format("Draft Approval Risk Matrix Project belum terbuat."));
            }
            return id;
        }

        private void DeleteOtherRiskMatrixProjectsDraft(ApprovalParam param)
        {
            RiskMatrixProject riskMatirxProject = _riskMatrixProjectRepository.Get(param.RequestId.GetValueOrDefault());

            var riskMatirxProjectList = _riskMatrixProjectRepository.GetByProjectRiskMatrixScenarioId(riskMatirxProject.ProjectId, riskMatirxProject.RiskMatrixId, riskMatirxProject.ScenarioId).Where(x => x.StatusId == 4 && x.IsDelete == false);

            Status status = null;

            if (riskMatirxProjectList.Count() > 0)
            {
                foreach (var item in riskMatirxProjectList)
                {
                    item.Delete(param.UpdateBy.GetValueOrDefault(), param.UpdateDate.GetValueOrDefault());
                    var approval = _approvalRepository.GetAllBaseOnSource(item.Id, "RiskMatrixProject");
                    foreach (var item2 in approval)
                    {
                        item2.RemoveActive(param.UpdateBy.GetValueOrDefault(), param.UpdateDate.GetValueOrDefault());
                    }
                }
            }
            _databaseContext.SaveChanges();
        }

        private void UpdateStatusScenario(ApprovalParam param, Status status)
        {
            //Get Scenario
            Scenario scenario = _scenarioRepository.Get(param.RequestId);

            //Set status Scenario
            scenario.UpdateStatus(status, param.UpdateBy, param.UpdateDate);
            _scenarioRepository.Update(scenario);
            _databaseContext.SaveChanges();
        }

        private void UpdateStatusRiskMatrixProjects(ApprovalParam param, Status status)
        {
            //Get Risk Matrix Project
            RiskMatrixProject riskMatrixProjects = _riskMatrixProjectRepository.Get(param.RequestId.GetValueOrDefault());
            riskMatrixProjects.UpdateStatus(status, param.UpdateBy, param.UpdateDate);
            _riskMatrixProjectRepository.Update(riskMatrixProjects);
            _databaseContext.SaveChanges();
        }

        private void UpdateStatusCorrelatedProject(ApprovalParam param, Status status)
        {
            //Get Correlated Project
            CorrelatedProject correlatedProject = _correlatedProjectRepository.Get(param.RequestId.GetValueOrDefault());
            correlatedProject.UpdateStatus(status, param.UpdateBy, param.UpdateDate);
            _correlatedProjectRepository.Update(correlatedProject);
            _databaseContext.SaveChanges();
        }

        private void UpdateStatusCorrelatedSektor(ApprovalParam param, Status status)
        {
            //Get Correlated Sektor
            CorrelatedSektor correlatedSektor = _correlatedSectorRepository.Get(param.RequestId.GetValueOrDefault());
            correlatedSektor.UpdateStatus(status, param.UpdateBy, param.UpdateDate);
            _correlatedSectorRepository.Update(correlatedSektor);
            _databaseContext.SaveChanges();
        }
        
        public void SetDefaultScenario(ApprovalParam approvalParam, User userUpdate, Status statusUserUpdate,
            Role roleUserUpdate)
        {
            //Get Scenario
            Scenario scenario = _scenarioRepository.Get(approvalParam.RequestId);

            int scenarioDraft = 0;
            //var scenarioDraftDatabase;
            //cek apakah di mau kirim ke draft atau tidak
            if (approvalParam.IsSend == false)
            {
                if (scenario.StatusId == 4)
                {
                    UpdateScenarioMurni(approvalParam);
                }
                
            }
            //cek apakah di mau kirim ke draft atau tidak
            else
            {
                //Notes
                string sourceApproval = "Scenario";

                //Generate status Approval
                string statusApproval = statusUserUpdate.StatusDescription + " By " + userUpdate.UserName + " Role " + roleUserUpdate.Name;

                int nomorUrutStatus = 0;

                //variabel untuk email
                string source = "Scenario";
                string typePesan = "";
                List<int> emailCC = new List<int>();
                int emailKepada = 0;
                int emailDari = 0;
                int requestId = 0;

                ApprovalParam approvalParamNew = new ApprovalParam
                {
                    RequestId = scenario.Id,
                    SourceApproval = sourceApproval,
                    StatusId = statusUserUpdate.Id,
                    // CreateBy = scenario.CreateBy,
                    CreateBy = approvalParam.UpdateBy,
                    CreateDate = approvalParam.UpdateDate,
                    Keterangan = approvalParam.Keterangan,
                    NomorUrutStatus = approvalParam.NomorUrutStatus,
                    StatusApproval = statusApproval
                };
                Add(approvalParamNew);

                if (statusUserUpdate.Id == 3)
                {
                   // UpdateStatusScenario(approvalParam, statusUserUpdate);

                    var otherScenarioId = 0;
                    var scenarioSameName2 = _scenarioRepository.GetAllByName2(scenario.Id, scenario.CreateBy.GetValueOrDefault());

                    foreach (var item in scenarioSameName2)
                    {
                        if (item.StatusId == 1 && item.Id != scenario.Id)
                        {
                            otherScenarioId = item.Id;
                        }
                    }

                    Validate.NotNull(otherScenarioId, "Scenario tidak ditemukan.");

                    Scenario scenarioDelete = _scenarioRepository.Get(otherScenarioId);
                    scenarioDelete.UpdateStatus(null, approvalParam.UpdateBy, approvalParam.UpdateDate);
                    scenario.Delete(approvalParam.UpdateBy.GetValueOrDefault(), approvalParam.UpdateDate.GetValueOrDefault());
                    _databaseContext.SaveChanges();
                    nomorUrutStatus = approvalParam.NomorUrutStatus.GetValueOrDefault();

                    #region email
                    emailDari = userUpdate.Id;

                    // emailKepada = scenario.UpdateBy.GetValueOrDefault();
                    if (emailKepada == 0)
                    {
                        emailKepada = scenario.CreateBy.GetValueOrDefault();
                    }
                    var masterApprovalScenario = _masterApprovalScenarioRepository.GetAll().ToList();
                    foreach (var item in masterApprovalScenario)
                    {
                        emailCC.Add(item.UserId.GetValueOrDefault());
                    }

                    source = sourceApproval;

                    typePesan = statusUserUpdate.StatusDescription;

                    requestId = scenario.Id;

                    // source code notifikasi email
                    _emailService.NotifyResetPassword(source, typePesan, requestId, emailKepada, emailCC, emailDari, approvalParam.TemplateNotif);
                    #endregion email
                }
                if (statusUserUpdate.Id == 2 && approvalParam.NomorUrutStatus == 3)
                {
                    //Set status scenario dan update data scenario
                    UpdateStatusScenario(approvalParam, statusUserUpdate);
                    //merubah ke tabel scenario ketika sudah di approve oleh user level 3 
                    DeleteOtherScenarioDraft(approvalParam);

                    UpdateScenario(approvalParam);


                }

                if (statusUserUpdate.Id == 2 && approvalParam.NomorUrutStatus != 3)
                {


                    UpdateScenario(approvalParam);


                }
            }                      
        }

        public void SetDefaultRiskMatrixProject(ApprovalParam approvalParam, User userUpdate, Status statusUserUpdate,
            Role roleUserUpdate)
        {
            //Get Risk Matrix Project
            RiskMatrixProject riskMatrixProject = _riskMatrixProjectRepository.Get(approvalParam.RequestId.GetValueOrDefault());
            int id;
            int riskMatrixProjectDraft = 0;

            //mencari data risk matrix project aslinya
            var otherRiskMatrixProjectId = 0;
            var riskMatrixProjectSameName2 = _riskMatrixProjectRepository.GetByProjectRiskMatrixScenarioId2(riskMatrixProject.ProjectId, riskMatrixProject.RiskMatrixId, riskMatrixProject.ScenarioId, riskMatrixProject.CreateBy.GetValueOrDefault()).Where( x => x.Id != riskMatrixProject.Id);

            if (riskMatrixProjectSameName2.Count() == 0)
            {
                riskMatrixProjectSameName2 = _riskMatrixProjectRepository.GetByProjectRiskMatrixScenarioId(riskMatrixProject.ProjectId, riskMatrixProject.RiskMatrixId, riskMatrixProject.ScenarioId).Where(x => x.Id != riskMatrixProject.Id);
            }

            foreach (var item in riskMatrixProjectSameName2)
            {
                if (item.StatusId == 1 && item.Id != riskMatrixProject.Id)
                {
                    otherRiskMatrixProjectId = item.Id;
                }
            }
            Validate.NotNull(otherRiskMatrixProjectId, "Risk Matrix Project tidak ditemukan.");

            if (approvalParam.IsSend == false)
            {
                if (riskMatrixProject.StatusId == 4)
                {
                    UpdateRiskMatrixProjectsMurni(approvalParam);
                }
            }
            else
            {
                string sourceApproval = "RiskMatrixProject";

                int nomorUrutStatus = 0;

                string statusApproval = statusUserUpdate.StatusDescription + " By " + userUpdate.UserName + " Role " + roleUserUpdate.Name;

                //variabel untuk email
                string typePesan = "";
                List<int> emailCC = new List<int>();
                int emailKepada = 0;
                int emailDari = 0;
                int requestId = 0;

                ApprovalParam approvalParamNew = new ApprovalParam
                {
                    RequestId = riskMatrixProject.Id,
                    SourceApproval = sourceApproval,
                    StatusId = statusUserUpdate.Id,
                    //CreateBy = riskMatrixProject.CreateBy,
                    CreateBy = approvalParam.UpdateBy,
                    CreateDate = approvalParam.UpdateDate,
                    Keterangan = approvalParam.Keterangan,
                    NomorUrutStatus = approvalParam.NomorUrutStatus,
                    StatusApproval = statusApproval
                };
                id = Add(approvalParamNew);
                //_riskMatriksTemporerService.ApproveRiskMatrixProjectAudit(approvalParamNew, otherRiskMatrixProjectId, id);
                if (statusUserUpdate.Id == 3)
                {
                    //Set status Risk Matrix Project
                    //UpdateStatusRiskMatrixProjects(approvalParam, statusUserUpdate);
                    // UpdateStatusCorrelatedSektor(approvalParam, statusUserUpdate);
                    

                    RiskMatrixProject riskMatrixProjectDelete = _riskMatrixProjectRepository.Get(otherRiskMatrixProjectId);
                    if (riskMatrixProjectDelete != null)
                    {
                        riskMatrixProjectDelete.SetNullStatus(approvalParam.UpdateBy, approvalParam.UpdateDate);
                    }
                   
                    //_riskMatriksTemporerService.RejectRiskMatrixProjectAudit(riskMatrixProjectDelete.Id, userUpdate.Id);
                    riskMatrixProject.Delete(approvalParam.UpdateBy.GetValueOrDefault(), approvalParam.UpdateDate.GetValueOrDefault());
                    _databaseContext.SaveChanges();
                    nomorUrutStatus = approvalParam.NomorUrutStatus.GetValueOrDefault();

                    #region email
                    emailDari = userUpdate.Id;

                    //emailKepada = riskMatrixProject.UpdateBy.GetValueOrDefault();
                    if (emailKepada == 0)
                    {
                        emailKepada = riskMatrixProject.CreateBy.GetValueOrDefault();
                    }
                    var masterApprovalRiskMatrixProject = _masterApprovalRiskMatrixProjectRepository.GetAllByProjectId(riskMatrixProject.ProjectId).ToList();
                    foreach (var item in masterApprovalRiskMatrixProject)
                    {
                        emailCC.Add(item.UserId.GetValueOrDefault());
                    }

                    typePesan = statusUserUpdate.StatusDescription;

                    requestId = riskMatrixProject.Id;

                    // source code notifikasi email
                    _emailService.NotifyResetPassword(sourceApproval, typePesan, requestId, emailKepada, emailCC, emailDari, approvalParam.TemplateNotif);
                    #endregion email
                }
                if (statusUserUpdate.Id == 2 && approvalParam.NomorUrutStatus == 3)
                {
                    //Set status Risk Matrix Project
                    UpdateStatusRiskMatrixProjects(approvalParam, statusUserUpdate);

                    UpdateRiskMatrixProjects(approvalParam);
                }
                if (statusUserUpdate.Id == 2 && approvalParam.NomorUrutStatus != 3)
                {
                    UpdateRiskMatrixProjects(approvalParam);
                }                
            }          
        }

        public void SetDefaultCorrelatedProject(ApprovalParam approvalParam, User userUpdate, Status statusUserUpdate,
            Role roleUserUpdate)
        {
            //Get Scenario
            CorrelatedProject correlatedProject = _correlatedProjectRepository.Get(approvalParam.RequestId.GetValueOrDefault());

            int correlatedSektorDraft = 0;

            if (approvalParam.IsSend == false)
            {
                if (correlatedProject.StatusId == 4)
                {
                    UpdateCorrelatedProjectDetailMurni(approvalParam);
                }
            }
            else
            {
                string sourceApproval = "CorrelatedProject";

                int nomorUrutStatus = 0;
                //emailDari tipenya int
                int emailDari = 0;
                //emailKepada tipe int
                int emailKepada = 0;

                //dalam bentuk array cc
                List<int> emailCC = new List<int>();
                //emailCC[0] = 0;
                //emailCC[1] = 0;
                //emailCC[2] = 0;

                //tipe pesan ada 2 string Skenario dan Submit
                string typePesan = "";

                //requestId tipe int IDScenario
                int requestId = 0;
                //Generate status Approval
                string statusApproval = statusUserUpdate.StatusDescription + " By " + userUpdate.UserName + " Role " + roleUserUpdate.Name;

                ApprovalParam approvalParamNew = new ApprovalParam
                {
                    RequestId = correlatedProject.Id,
                    SourceApproval = sourceApproval,
                    StatusId = statusUserUpdate.Id,
                    // CreateBy = correlatedProject.CreateBy,
                    CreateBy = approvalParam.UpdateBy,
                    CreateDate = approvalParam.UpdateDate,
                    Keterangan = approvalParam.Keterangan,
                    NomorUrutStatus = approvalParam.NomorUrutStatus,
                    StatusApproval = statusApproval
                };
                Add(approvalParamNew);

                if (statusUserUpdate.Id == 3)
                {
                    //Set status Risk Matrix Project
                    //UpdateStatusCorrelatedProject(approvalParam, statusUserUpdate);
                    var otherCorrelatedProjectId = 0;
                    var correlatedProjectscenarioSameName2 = _correlatedProjectRepository.GetByScenarioProjectSektorId2(correlatedProject.ScenarioId, correlatedProject.ProjectId, correlatedProject.SektorId, correlatedProject.CreateBy.GetValueOrDefault()).Where( x=> x.Id != correlatedProject.Id);

                    if (correlatedProjectscenarioSameName2.Count() == 0)
                    {
                        correlatedProjectscenarioSameName2 = _correlatedProjectRepository.GetByScenarioProjectSektorId(correlatedProject.ScenarioId, correlatedProject.ProjectId, correlatedProject.SektorId).Where(x => x.Id != correlatedProject.Id);
                    }

                    foreach (var item in correlatedProjectscenarioSameName2)
                    {
                        if (item.StatusId == 1 && item.Id != correlatedProject.Id)
                        {
                            otherCorrelatedProjectId = item.Id;
                        }
                    }

                    Validate.NotNull(otherCorrelatedProjectId, "Correlated Project tidak ditemukan.");

                    CorrelatedProject correlatedProjectDelete = _correlatedProjectRepository.Get(otherCorrelatedProjectId);
                    if (correlatedProjectDelete != null)
                    {
                        correlatedProjectDelete.UpdateStatus(null, approvalParam.UpdateBy, approvalParam.UpdateDate);
                    }
                    
                    correlatedProject.Delete(approvalParam.UpdateBy.GetValueOrDefault(), approvalParam.UpdateDate.GetValueOrDefault());
                    _databaseContext.SaveChanges();
                    nomorUrutStatus = approvalParam.NomorUrutStatus.GetValueOrDefault();

                    #region email
                    emailDari = userUpdate.Id;

                    //emailKepada = correlatedProject.UpdateBy.GetValueOrDefault();
                    if (emailKepada == 0)
                    {
                        emailKepada = correlatedProject.CreateBy.GetValueOrDefault();
                    }
                    var masterApprovalCorrelatedProject = _masterApprovalCorrelatedProjectRepository.GetAllByProjectId(correlatedProject.ProjectId).ToList();
                    foreach (var item in masterApprovalCorrelatedProject)
                    {
                        emailCC.Add(item.UserId.GetValueOrDefault());
                    }

                    typePesan = statusUserUpdate.StatusDescription;

                    requestId = correlatedProject.Id;

                    // source code notifikasi email
                    _emailService.NotifyResetPassword(sourceApproval, typePesan, requestId, emailKepada, emailCC, emailDari, approvalParam.TemplateNotif);
                    #endregion email
                }

                if (statusUserUpdate.Id == 2 && approvalParam.NomorUrutStatus == 3)
                {
                    //Set status Risk Matrix Project
                    UpdateStatusCorrelatedProject(approvalParam, statusUserUpdate);

                    DeleteOtherCorrelatedProjectDraft(approvalParam);

                    UpdateCorrelatedProjectDetail(approvalParam);

                    _correlatedProjectDetailService.SetIsApproved(approvalParam.RequestId.GetValueOrDefault(), approvalParam.UpdateBy.GetValueOrDefault(), approvalParam.UpdateDate.GetValueOrDefault());
                }
                if (statusUserUpdate.Id == 2 && approvalParam.NomorUrutStatus != 3)
                {

                    UpdateCorrelatedProjectDetail(approvalParam);
                }
            }
           

            //}

        }

        public void SetDefaultCorrelatedSektor(ApprovalParam approvalParam, User userUpdate, Status statusUserUpdate,
            Role roleUserUpdate)
        {
            //Get Scenario
            CorrelatedSektor correlatedSektor = _correlatedSectorRepository.Get(approvalParam.RequestId.GetValueOrDefault());

            int correlatedSektorDraft = 0;

            if (approvalParam.IsSend == false)
            {
                if (correlatedSektor.StatusId == 4)
                {
                    UpdateCorrelatedSektorDetailMurni(approvalParam);
                }
                //jika belum ada, segera buat draft baru
                //else
                //{
                //    correlatedSektorDraft = DraftCorrelatedSektor(approvalParam);
                //}
            }
            else
            {
                string sourceApproval = "CorrelatedSektor";

                int nomorUrutStatus = 0;
                //emailDari tipenya int
                int emailDari = 0;
                //emailKepada tipe int
                int emailKepada = 0;

                //dalam bentuk array cc
                List<int> emailCC = new List<int>();

                //tipe pesan ada 2 string Skenario dan Submit
                string typePesan = "";

                //requestId tipe int IDScenario
                int requestId = 0;
                //Generate status Approval
                string statusApproval = statusUserUpdate.StatusDescription + " By " + userUpdate.UserName + " Role " + roleUserUpdate.Name;


                ApprovalParam approvalParamNew = new ApprovalParam
                {
                    RequestId = correlatedSektor.Id,
                    SourceApproval = sourceApproval,
                    StatusId = statusUserUpdate.Id,
                    //CreateBy = correlatedSektor.CreateBy,
                    CreateBy = approvalParam.UpdateBy,
                    CreateDate = approvalParam.UpdateDate,
                    Keterangan = approvalParam.Keterangan,
                    NomorUrutStatus = approvalParam.NomorUrutStatus,
                    StatusApproval = statusApproval
                };
                Add(approvalParamNew);

                if (statusUserUpdate.Id == 3)
                {                    
                    var otherCorrelatedSektorId = 0;
                    var correlatedSektorSameName2 = _correlatedSectorRepository.GetByScenarioIdSektorIdList2(correlatedSektor.ScenarioId, correlatedSektor.SektorId, correlatedSektor.CreateBy.GetValueOrDefault()).Where( x => x.Id != correlatedSektor.Id);

                    if (correlatedSektorSameName2.Count() == 0)
                    {
                        correlatedSektorSameName2 = _correlatedSectorRepository.GetByScenarioIdSektorIdList(correlatedSektor.ScenarioId, correlatedSektor.SektorId).Where(x => x.Id != correlatedSektor.Id); ;
                    }

                    foreach (var item in correlatedSektorSameName2)
                    {
                        if (item.StatusId == 1 && item.Id != correlatedSektor.Id)
                        {
                            otherCorrelatedSektorId = item.Id;
                        }
                    }
                    Validate.NotNull(otherCorrelatedSektorId, "Correlated Sector tidak ditemukan.");

                    CorrelatedSektor correlatedSectorDelete = _correlatedSectorRepository.Get(otherCorrelatedSektorId);
                    if (correlatedSectorDelete != null)
                    {
                        correlatedSectorDelete.UpdateStatus(null, approvalParam.UpdateBy, approvalParam.UpdateDate);
                    }
                    
                    correlatedSektor.Delete(approvalParam.UpdateBy.GetValueOrDefault(), approvalParam.UpdateDate.GetValueOrDefault());
                    _databaseContext.SaveChanges();
                    nomorUrutStatus = approvalParam.NomorUrutStatus.GetValueOrDefault();

                    #region email
                    emailDari = userUpdate.Id;

                    // emailKepada = correlatedSektor.UpdateBy.GetValueOrDefault();
                    if (emailKepada == 0)
                    {
                        emailKepada = correlatedSektor.CreateBy.GetValueOrDefault();
                    }
                    var masterApprovalCorrelatedSektor = _masterApprovalCorrelatedSektorRepository.GetAll().ToList();
                    foreach (var item in masterApprovalCorrelatedSektor)
                    {
                        emailCC.Add(item.UserId.GetValueOrDefault());
                    }

                    typePesan = statusUserUpdate.StatusDescription;

                    requestId = correlatedSektor.Id;

                    // source code notifikasi email
                     _emailService.NotifyResetPassword(sourceApproval, typePesan, requestId, emailKepada, emailCC, emailDari, approvalParam.TemplateNotif);
                    #endregion email
                }
                if (statusUserUpdate.Id == 2 && approvalParam.NomorUrutStatus == 3)
                {
                    //Set status Risk Matrix Project
                    UpdateStatusCorrelatedSektor(approvalParam, statusUserUpdate);

                    DeleteOtherCorrelatedSektorDraft(approvalParam);

                    UpdateCorrelatedSektorDetail(approvalParam);
                }
                if (statusUserUpdate.Id == 2 && approvalParam.NomorUrutStatus != 3)
                {

                    UpdateCorrelatedSektorDetail(approvalParam);
                }
            }
            
        }
        #endregion Private
    }
}
