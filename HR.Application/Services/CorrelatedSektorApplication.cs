﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;

namespace HR.Application
{
    public class CorrelatedSektorService : ICorrelatedSektorService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICorrelatedSektorRepository _correlatedSektorRepository;
        private readonly IScenarioRepository _scenarioRepository;

        public CorrelatedSektorService(IUnitOfWork unitOfWork, ICorrelatedSektorRepository correlatedSektorRepository, IScenarioRepository scenarioRepository)
        {
            _unitOfWork = unitOfWork;
            _correlatedSektorRepository = correlatedSektorRepository;
            _scenarioRepository = scenarioRepository;
        }

        #region Query
        public IEnumerable<CorrelatedSektor> GetAll()
        {
            return _correlatedSektorRepository.GetAll();
        }

        public IEnumerable<CorrelatedSektor> GetByScenarioDefaultId(string keyword)
        {
            var scenarioDefault = _scenarioRepository.GetDefault();
            Validate.NotNull(scenarioDefault, "Scenario default tidak ditemukan.");

            return _correlatedSektorRepository.GetByScenarioDefaultId(scenarioDefault.Id, keyword);
        }

        public IEnumerable<CorrelatedSektor> GetByScenarioDefaultId(string keyword, int id)
        {
            int idScenarioDefault = 0;
            var scenarioDefault = _scenarioRepository.GetDefault();
            if (scenarioDefault == null)
            {
                idScenarioDefault = 0;
            }
            else
            {
                idScenarioDefault = scenarioDefault.Id;
            }
            //Validate.NotNull(scenarioDefault, "Scenario default tidak ditemukan.");

            return _correlatedSektorRepository.GetByScenarioDefaultId(idScenarioDefault, keyword, id);
        }

        public IEnumerable<CorrelatedSektor> GetByScenarioDefaultId(string keyword, int id, int user)
        {
            int idScenarioDefault = 0;
            var scenarioDefault = _scenarioRepository.GetDefault();
            if (scenarioDefault == null)
            {
                idScenarioDefault = 0;
            }
            else
            {
                idScenarioDefault = scenarioDefault.Id;
            }
            //Validate.NotNull(scenarioDefault, "Scenario default tidak ditemukan.");

            return _correlatedSektorRepository.GetByScenarioDefaultId(idScenarioDefault, keyword, id, user);
        }

        public IEnumerable<CorrelatedSektor> GetByScenarioDefaultId(string keyword, int id,int id2, int user)
        {
            int idScenarioDefault = 0;
            var scenarioDefault = _scenarioRepository.GetDefault();
            if (scenarioDefault == null)
            {
                idScenarioDefault = 0;
            }
            else
            {
                idScenarioDefault = scenarioDefault.Id;
            }
            //Validate.NotNull(scenarioDefault, "Scenario default tidak ditemukan.");

            return _correlatedSektorRepository.GetByScenarioDefaultId(idScenarioDefault, keyword, id,id2, user);
        }

        public CorrelatedSektor Get(int id)
        {
            return _correlatedSektorRepository.Get(id);
        }
        #endregion Query

        public int Add(CorrelatedSektorParam param)
        {
            throw new NotImplementedException();
        }

        public int Update(int id, CorrelatedSektorParam param)
        {
            throw new NotImplementedException();
        }

        public int Delete(int id, int deleteBy, DateTime deleteDate)
        {
            throw new NotImplementedException();
        }


    }
}
