﻿using System.Collections.Generic;
using HR.Core;
using HR.Domain;

namespace HR.Application
{
    public class CorrelatedProjectService : ICorrelatedProjectService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICorrelatedProjectRepository _correlatedProjectRepository;
        private readonly IScenarioRepository _scenarioRepository;

        public CorrelatedProjectService(IUnitOfWork unitOfWork, ICorrelatedProjectRepository correlatedProjectRepository, IScenarioRepository scenarioRepository)
        {
            _unitOfWork = unitOfWork;
            _correlatedProjectRepository = correlatedProjectRepository;
            _scenarioRepository = scenarioRepository;
        }

        public IEnumerable<CorrelatedProject> GetAll()
        {
            return _correlatedProjectRepository.GetAll();
        }

        public IEnumerable<CorrelatedProject> GetByScenarioDefaultId(string keyword, int id)
        {
            var scenarioDefault = _scenarioRepository.GetDefault();
            return _correlatedProjectRepository.GetByScenarioId(scenarioDefault.Id, keyword, id);
            
        }

        public IEnumerable<CorrelatedProject> GetByScenarioDefaultId(string keyword, int id, int user)
        {
            int idScenarioDefault;
            var scenarioDefault = _scenarioRepository.GetDefault();
            if (scenarioDefault == null)
            {
                idScenarioDefault = 0;
            }
            else
            {
                idScenarioDefault = scenarioDefault.Id;
            }
           // return _correlatedProjectRepository.GetByScenarioId(scenarioDefault.Id, keyword, id);
            return _correlatedProjectRepository.GetByScenarioId(idScenarioDefault, keyword, id, user);
        }

        public IEnumerable<CorrelatedProject> GetByScenarioDefaultId(string keyword, int id, int id2, int user)
        {
            int idScenarioDefault;
            var scenarioDefault = _scenarioRepository.GetDefault();
            if (scenarioDefault == null)
            {
                idScenarioDefault = 0;
            }
            else
            {
                idScenarioDefault = scenarioDefault.Id;
            }
            // return _correlatedProjectRepository.GetByScenarioId(scenarioDefault.Id, keyword, id);
            return _correlatedProjectRepository.GetByScenarioId(idScenarioDefault, keyword, id,id2, user);
        }

        public CorrelatedProject Get(int id)
        {
            return _correlatedProjectRepository.Get(id);
        }

        public IEnumerable<Project> GetProjectByScenarioDefault(int scenarioId)
        {
            var data = _correlatedProjectRepository.GetByScenarioId(scenarioId, null, 0);
            
            IList<Project> projects = new List<Project>();

            //if (data.Count() > 0)
            //{
                foreach (var item in data)
                {
                    var project = item.Project;
                    projects.Add(project);
                }
            //}

            return projects;
        }

        public IEnumerable<Project> GetProjectByScenarioDefault(int scenarioId,int user)
        {
            var data = _correlatedProjectRepository.GetByScenarioId(scenarioId, null, 0, user);

            IList<Project> projects = new List<Project>();

            //if (data.Count() > 0)
            //{
            foreach (var item in data)
            {
                var project = item.Project;
                projects.Add(project);
            }
            //}

            return projects;
        }

    }
}
