﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;

namespace HR.Application
{
    public class TahapanService : ITahapanService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITahapanRepository _tahapanRepository;
        private readonly IAuditLogService _auditLogService;
        private readonly IProjectRepository _projectRepository;

        public TahapanService(IUnitOfWork uow, ITahapanRepository tahapanRepository, IAuditLogService auditLogService, IProjectRepository projectRepository)
        {
            _unitOfWork = uow;
            _tahapanRepository = tahapanRepository;
            _auditLogService = auditLogService;
            _projectRepository = projectRepository;
        }

        #region Query
        public IEnumerable<Tahapan> GetAll()
        {
            return _tahapanRepository.GetAll();
        }
        public IEnumerable<Tahapan> GetAll(string keyword, int id)
        {
            return _tahapanRepository.GetAll(keyword, id);
        }

        public Tahapan Get(int id)
        {
            return _tahapanRepository.Get(id);
        }

        public void IsExistOnEditing(int id, string namaTahapan, string keterangan)
        {
            if(_tahapanRepository.IsExist(id, namaTahapan))
            {
                throw new ApplicationException(string.Format("Nama Tahapan {0} sudah ada.", namaTahapan));
            }
        }

        public void isAlreadyUsed(string nama)
        {
            bool alreadyUsed = false;
            var projects = _projectRepository.GetAllActive().ToList();
            if (projects.Count > 0)
            {
                var project = projects.Where(x => x.Tahapan.NamaTahapan == nama).ToList();
                if (project.Count > 1)
                {
                    alreadyUsed = true;
                }
            }

            if (alreadyUsed)
                throw new ApplicationException(string.Format("Data tidak bisa dihapus / diubah karena sudah digunakan oleh Proyek dan Kalkulasi."));
        }

        public void isExistOnAdding(string namaTahapan)
        {
            if(_tahapanRepository.IsExist(namaTahapan))
            {
                throw new ApplicationException(string.Format("Nama Tahapan {0} sudah ada.", namaTahapan));
            }
        }
        #endregion Query

        #region Manipulation 
        public int Add(TahapanParam param)
        {
            int id;
            Validate.NotNull(param.NamaTahapan, "Nama Tahapan wajib diisi.");

            isExistOnAdding(param.NamaTahapan);
            using (_unitOfWork)
            {
                Tahapan model = new Tahapan(param.NamaTahapan, param.Keterangan, param.CreateBy, param.CreateDate);
                _tahapanRepository.Insert(model);
                _unitOfWork.Commit();
                id = model.Id;

                //Audit Log ADD 
                int audit = _auditLogService.AddTahapanAudit(param, id);
            }

            return id;
        }

        public int Update(int id, TahapanParam param)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Nama Tahapan tidak ditemukan.");

            IsExistOnEditing(id, param.NamaTahapan, param.Keterangan);
            isAlreadyUsed(model.NamaTahapan);
            using (_unitOfWork)
            {
                //Audit Log UPDATE 
                int audit = _auditLogService.UpdateTahapanAudit(param, id);

                model.Update(param.NamaTahapan, param.Keterangan, param.UpdateBy, param.UpdateDate);
                _tahapanRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;
            }
            return id;
        }

        public int Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Nama Tahapan tidak ditemukan.");

            isAlreadyUsed(model.NamaTahapan);
            using (_unitOfWork)
            {
                model.Delete(deleteBy, deleteDate);
                _tahapanRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;

                //Audit Log Delete 
                int audit = _auditLogService.DeleteTahapanDataAudit(id, deleteBy);
            }
            return id;
        }
        #endregion Manipulation
    }
}
