﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;
using HR.Infrastructure;

namespace HR.Application
{
    public class FunctionalRiskService : IFunctionalRiskService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IFunctionalRiskRepository _functionalRiskRepository;
        private readonly IMatrixRepository _matrixRepository;
        private readonly IColorCommentRepository _colorCommentRepository;
        private readonly IScenarioRepository _scenarioRepository;
        private readonly IDatabaseContext _iDatabaseContext;
        private readonly IAuditLogService _auditLogService;

        public FunctionalRiskService(IUnitOfWork uow, IDatabaseContext iDatabaseContext, IFunctionalRiskRepository functionalRiskRepository, IMatrixRepository matrixRepository, IColorCommentRepository colorCommentRepository, IScenarioRepository scenarioRepository, IAuditLogService auditLogService)
        {
            _unitOfWork = uow;
            _iDatabaseContext = iDatabaseContext;
            _functionalRiskRepository = functionalRiskRepository;
            _matrixRepository = matrixRepository;
            _colorCommentRepository = colorCommentRepository;
            _scenarioRepository = scenarioRepository;
            _auditLogService = auditLogService;
        }

        #region Query
        public IEnumerable<FunctionalRisk> GetAll(string keyword, int id)
        {
            return _functionalRiskRepository.GetAll(keyword, id);
        }

        public FunctionalRisk Get(int id)
        {
            return _functionalRiskRepository.Get(id);
        }
        public IEnumerable<FunctionalRisk> GetByScenarioId(int scenarioId)
        {
            return _functionalRiskRepository.GetByScenarioId(scenarioId);
        }
        //public void IsExistOnEditing(int id, string definisi)
        //{
        //    if (_functionalRiskRepository.IsExist(id, definisi))
        //    {
        //        throw new ApplicationException(string.Format("Functional Risk {0} sudah ada.", definisi));
        //    }
        //}

        //public void isExistOnAdding(string definisi)
        //{
        //    if (_functionalRiskRepository.IsExist(definisi))
        //    {
        //        throw new ApplicationException(string.Format("Functional Risk {0} sudah ada.", definisi));
        //    }
        //}
        #endregion Query

        #region Manipulation 
        public int Add(FunctionalRiskAddParam param)
        {
            int id = 0;
            var scenarioIdDefault = _scenarioRepository.GetDefault().Id;
            int[] x = new int[3];
            x[0] = scenarioIdDefault;
            x[1] = param.Scenarios[0];
            x[2] = param.Scenarios[1];
            int[] scenarios = x.Distinct().ToArray();
            var matrixAll = _matrixRepository.GetAll().ToList();
            var colorCommentAll = _colorCommentRepository.GetAll().ToList();
            var getAll = _functionalRiskRepository.GetAll(null,0).ToList();
            var getAllFalse = _functionalRiskRepository.GetAllFalse().ToList();

            if (getAllFalse != null)
            {
                foreach (var item in getAllFalse)
                {
                    var model = this.Get(item.Id);
                    Validate.NotNull(model, "Functional Risk tidak ditemukan.");
                    model.Delete(param.CreateBy, param.CreateDate);
                    _functionalRiskRepository.Update(model);
                    _iDatabaseContext.SaveChanges();
                }
            }

            foreach (var scenarioId in scenarios)
            {
                var scenario = _scenarioRepository.Get(scenarioId);
                var getByScenarioId = _functionalRiskRepository.GetByScenarioId(scenario.Id).ToList();
                if (getByScenarioId.Count == 0)
                {
                    for (int i = 0; i < matrixAll.Count; i++)
                    {
                        for (int j = 0; j < colorCommentAll.Count; j++)
                        {
                            var matrix = _matrixRepository.Get(matrixAll[i].Id);
                            var colorComment = _colorCommentRepository.Get(colorCommentAll[j].Id);
                            //var scenario = _scenarioRepository.Get(scenarioId);
                            FunctionalRisk model = new FunctionalRisk(matrix, colorComment, scenario, null, param.CreateBy, param.CreateDate, 0, 0, null);
                            _functionalRiskRepository.Insert(model);
                            _iDatabaseContext.SaveChanges();
                        }
                    }
                }
                else
                {
                    foreach (var item in getByScenarioId)
                    {
                        var model = this.Get(item.Id);
                        model.UnDelete();
                        _functionalRiskRepository.Update(model);
                        _iDatabaseContext.SaveChanges();
                    }
                }
                //Audit Log ADD 
                //int audit = _auditLogService.AddFunctionalRiskAudit(param, id);
            }
            return id;
        }

        public int Update(int id, FunctionalRiskParam param)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Functional Risk tidak ditemukan.");

            var matrix = _matrixRepository.Get(param.MatrixId);
            var colorComment = _colorCommentRepository.Get(param.ColorCommentId);
            var scenario = _scenarioRepository.Get(param.ScenarioId);

            //IsExistOnEditing(id, param.Definisi);
            using (_unitOfWork)
            {
                //Audit Log UPDATE 
                int audit = _auditLogService.UpdateFunctionalRiskAudit(param, id);

                model.Update(matrix, colorComment, scenario, param.Definisi, param.UpdateBy, param.UpdateDate, param.NilaiMaksimum, param.NilaiMinimum, param.Komentar);
                _functionalRiskRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;
            }
            return id;
        }

        public int Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Functional Risk tidak ditemukan.");

            using (_unitOfWork)
            {
                model.Delete(deleteBy, deleteDate);
                _functionalRiskRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;
            }
            return id;
        }
        #endregion Manipulation
    }
}
