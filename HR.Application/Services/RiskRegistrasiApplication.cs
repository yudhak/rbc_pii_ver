﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;
using HR.Infrastructure;

namespace HR.Application
{
    public class RiskRegistrasiService : IRiskRegistrasiService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRiskRegistrasiRepository _riskRegistrasiRepository;
        private readonly IProjectRiskStatusRepository _projectRiskStatusRepository;
        private readonly IAuditLogService _auditLogService;
        private readonly IDatabaseContext _databaseContext;

        public RiskRegistrasiService(IUnitOfWork uow, IRiskRegistrasiRepository riskRegistrasiRepository, IProjectRiskStatusRepository projectRiskStatusRepository, IAuditLogService auditLogService, IDatabaseContext databaseContext)
        {
            _unitOfWork = uow;
            _riskRegistrasiRepository = riskRegistrasiRepository;
            _projectRiskStatusRepository = projectRiskStatusRepository;
            _auditLogService = auditLogService;
            _databaseContext = databaseContext;
        }

        #region Query
        public IEnumerable<RiskRegistrasi> GetAll()
        {
            return _riskRegistrasiRepository.GetAll();
        }
        public IEnumerable<RiskRegistrasi> GetAll(string keyword, int id)
        {
            return _riskRegistrasiRepository.GetAll(keyword, id);
        }

        public RiskRegistrasi Get(int id)
        {
            return _riskRegistrasiRepository.Get(id);
        }

        public void IsExistOnEditing(int id, string kodeMRisk, string namaCategoryRisk, string definisi)
        {
            if (_riskRegistrasiRepository.IsExist(id, namaCategoryRisk))
            {
                throw new ApplicationException(string.Format("Kode Risk {0} already exist.", namaCategoryRisk));
            }
        }

        public void isExistOnAdding(string kodeMRisk)
        {
            if (_riskRegistrasiRepository.IsExist(kodeMRisk))
            {
                throw new ApplicationException(string.Format("Kode Risk {0} already exist.", kodeMRisk));
            }
        }
        #endregion Query

        #region Manipulation 
        public int Add(RiskRegistrasiParam param)
        {
            int id;
            Validate.NotNull(param.NamaCategoryRisk, "Nama Categori Risk wajib diisi.");

            isExistOnAdding(param.KodeMRisk);
            using (_unitOfWork)
            {
                RiskRegistrasi model = new RiskRegistrasi(param.KodeMRisk, param.NamaCategoryRisk, param.Definisi, param.Maximum, param.Minimum, param.CreateBy, param.CreateDate);
                _riskRegistrasiRepository.Insert(model);
                _unitOfWork.Commit();
                id = model.Id;

                //Audit Log ADD 
                int audit = _auditLogService.AddRiskRegistrasiAudit(param, id);

            }

            return id;
        }

        public int Update(int id, RiskRegistrasiParam param)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Nama RiskRegistrasi tidak ditemukan.");

            IsExistOnEditing(id, param.KodeMRisk, param.NamaCategoryRisk, param.Definisi);
            using (_unitOfWork)
            {
                //Audit Log Update 
                int audit = _auditLogService.UpdateRiskRegistrasiAudit(param, id);

                //model.Update(param.KodeMRisk, param.NamaCategoryRisk, param.Definisi, param.UpdateBy, param.UpdateDate);
                model.Update(param.KodeMRisk, param.NamaCategoryRisk, param.Definisi, param.Maximum, param.Minimum, param.UpdateBy, param.UpdateDate);
                              
                _riskRegistrasiRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;
            }
            return id;
        }
        
        public int Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Nama Category Risk tidak ditemukan.");

            using (_unitOfWork)
            {
                //Audit Log Delete 
                int audit = _auditLogService.DeleteRiskRegistrasiAudit(id, deleteBy);

                model.Delete(deleteBy, deleteDate);
                _riskRegistrasiRepository.Update(model);
                var subRiskRegistrasi = _databaseContext.SubRiskRegistrasis.Where(x => x.RiskRegistrasiId == id).ToList();
                foreach (var item in subRiskRegistrasi.ToList())
                {
                    _databaseContext.SubRiskRegistrasis.Remove(item);
                }
                var projectRiskRegistrasi = _databaseContext.ProjectRiskRegistrasis.Where(x => x.RiskRegistrasiId == id).ToList();
                foreach (var item in projectRiskRegistrasi.ToList())
                {
                    _databaseContext.ProjectRiskRegistrasis.Remove(item);
                }
                var projectRiskStatus = _databaseContext.ProjectRiskStatus.Where(x => x.RiskRegistrasiId == id).ToList();
                foreach (var item in projectRiskStatus.ToList())
                {
                    _databaseContext.ProjectRiskStatus.Remove(item);
                }
                var allInDetail = _databaseContext.StageTahunRiskMatrixDetails.Where(x => x.RiskRegistrasiId == id).ToList();
                foreach(var item in allInDetail.ToList())
                {
                    _databaseContext.StageTahunRiskMatrixDetails.Remove(item);
                }
                _unitOfWork.Commit();
                id = model.Id;
            }
            return id;
        }
        #endregion Manipulation

        private void UpdateProjectRiskStatus(RiskRegistrasi model)
        {
            var currentProjectRiskStatusList = _projectRiskStatusRepository.GetAll();
            if(currentProjectRiskStatusList != null)
            {
                var currentProject = currentProjectRiskStatusList.GroupBy(x => x.ProjectId).ToList();
                if(currentProject.Count > 0)
                {
                    foreach (var item in currentProject)
                    {
                    }
                }
            }
        }
    }
}
