﻿using HR.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Application.Params;
using HR.Core;
using HR.Domain;

namespace HR.Application
{
    public class OverAllCommentsService : IOverAllCommentsService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IOverAllCommentsRepository _overAllCommentsRepository;
        private readonly IColorCommentRepository _colorCommentRepository;
        private readonly IMatrixRepository _matrixRepository;
        private readonly IAuditLogService _auditLogService;

        public OverAllCommentsService(IUnitOfWork uow, IOverAllCommentsRepository overAllCommentsRepository, IColorCommentRepository colorCommentRepository, IAuditLogService auditLogService)
        {
            _unitOfWork = uow;
            _overAllCommentsRepository = overAllCommentsRepository;
            _colorCommentRepository = colorCommentRepository;
            _auditLogService = auditLogService;

        }

        #region Query
        public IEnumerable<OverAllComments> GetAll()
        {
            return _overAllCommentsRepository.GetAll();
        }
        public IEnumerable<OverAllComments> GetByColorId(int colorId)
        {
            return _overAllCommentsRepository.GetByColorId(colorId);
        }
        public OverAllComments Get(int id)
        {
            return _overAllCommentsRepository.Get(id);
        }

        public void IsExistOnEditing(int id, string overAllComment)
        {
            if (_overAllCommentsRepository.IsExist(id, overAllComment))
            {
                throw new ApplicationException(string.Format("OverAllComment {0} sudah ada.", overAllComment));
            }
        }

        public void isExistOnAdding(string overAllComment)
        {
            if (_overAllCommentsRepository.IsExist(overAllComment))
            {
                throw new ApplicationException(string.Format("OverAllComment {0} sudah ada.", overAllComment));
            }
        }
        #endregion Query

        #region Manipulation 
        public int Add(OverAllCommentsParam param)
        {
            int id;
            Validate.NotNull(param.OverAllComment, "OverAllComment wajib diisi.");

            var colorComment = _colorCommentRepository.Get(param.ColorCommentId);

            //count average here 
            isExistOnAdding(param.OverAllComment);
            using (_unitOfWork)
            {
                OverAllComments model = new OverAllComments(colorComment, param.OverAllComment, param.CreateBy, param.CreateDate);
                _overAllCommentsRepository.Insert(model);
                _unitOfWork.Commit();
                id = model.Id;

                //Audit Log ADD 
                int audit = _auditLogService.AddOverAllCommentsAudit(param, id);
            }

            return id;
        }

        public int Update(int id, OverAllCommentsParam param)
        {
            var model = this.GetByColorId(id).FirstOrDefault();
            Validate.NotNull(model, "OverAllComment tidak ditemukan.");

            var colorComment = _colorCommentRepository.Get(param.ColorCommentId);

            IsExistOnEditing(id, param.OverAllComment);
            using (_unitOfWork)
            {
                //Audit Log UPDATE 
                int audit = _auditLogService.UpdateOverAllCommentsAudit(param, id);

                model.Update(colorComment, param.OverAllComment, param.UpdateBy, param.UpdateDate);
                _overAllCommentsRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;
            }
            return id;
        }

        public int Delete(int id, int deleteBy, DateTime deleteDate)
        {
            var model = this.Get(id);
            Validate.NotNull(model, "Comment tidak ditemukan.");

            using (_unitOfWork)
            {
                model.Delete(deleteBy, deleteDate);
                _overAllCommentsRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;
            }
            return id;
        }

        public int SetDefault(int id, int updateBy, DateTime updateDate)
        {
            var model = _colorCommentRepository.Get(id);
            Validate.NotNull(model, "Comment tidak ditemukan.");

            var oldModel = _colorCommentRepository.GetDefault();

            if(oldModel != null)
            {
                oldModel.UnsetDefault(updateBy, updateDate);
                _colorCommentRepository.Update(oldModel);
            }

            using (_unitOfWork)
            {
                model.SetDefault(updateBy, updateDate);
                _colorCommentRepository.Update(model);
                _unitOfWork.Commit();
                id = model.Id;
            }
            return id;
        }
        #endregion Manipulation
    }
}
