﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HR.Application.DTO
{    
    public class AuditLogDTO
    {
        public int Id { get; private set; }
        public int? MenuId { get; private set; }
        public string TableModified { get; private set; }
        public int? DataObjekId { get; private set; }
        public string ColumnModified { get; private set; }
        public string DataAwal { get; private set; }
        public string DataAkhir { get; private set; }
        public DateTime LogTimestamp { get; private set; }
        public string ModifiedBy { get; private set; }


        public AuditLogDTO(AuditLog model)
        {
            if (model == null) return;
           
            this.Id = model.Id;
            this.MenuId = model.MenuId;
            this.TableModified = model.TableModified;
            this.DataObjekId = model.DataObjekId;
            this.ColumnModified = model.ColumnModified;
            this.DataAwal = model.DataAwal;
            this.DataAkhir = model.DataAkhir;
            this.LogTimestamp = model.LogTimestamp;                  
            //this.ModifiedBy = model.User.UserName;            
        }

        public static AuditLogDTO From(AuditLog model)
        {
            return new AuditLogDTO(model);
        }

        public static IList<AuditLogDTO> From(IList<AuditLog> collection)
        {
            IList<AuditLogDTO> colls = new List<AuditLogDTO>();
            foreach (var item in collection)
            {
                colls.Add(new AuditLogDTO(item));
            }
            return colls;
        }
    }
}
