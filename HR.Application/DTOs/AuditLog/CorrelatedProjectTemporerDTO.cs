﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HR.Application.DTO
{
    public class CorrelatedProjectTemporerDTO
    {
        public int Id { get; private set; }
        public int? MenuId { get; private set; }
        public string TableModified { get; private set; }
        public int? DataObjekId { get; private set; }
        public string ColumnModified { get; private set; }
        public string DataAwal { get; private set; }
        public string DataAkhir { get; private set; }
        public DateTime LogTimestamp { get; private set; }
        public int? ModifiedBy { get; private set; }
        public DateTime DeleteDate { get; private set; }
        public int? DeleteBy { get; private set; }

        public CorrelatedProjectTemporerDTO(CorrelatedProjectTemporer model)
        {
            if (model == null) return;
            this.Id = model.Id;
            this.MenuId = model.MenuId;
            this.TableModified = model.TableModified;
            this.DataObjekId = model.DataObjekId;
            this.ColumnModified = model.ColumnModified;
            this.DataAwal = model.DataAwal;
            this.DataAkhir = model.DataAkhir;
            this.LogTimestamp = model.LogTimestamp;
            this.ModifiedBy = model.ModifiedBy;
            this.DeleteDate = model.DeleteDate;
            this.DeleteBy = model.DeleteBy;
        }

        public static CorrelatedProjectTemporerDTO From(CorrelatedProjectTemporer model)
        {
            return new CorrelatedProjectTemporerDTO(model);
        }

        public static IList<CorrelatedProjectTemporerDTO> From(IList<CorrelatedProjectTemporer> collection)
        {
            IList<CorrelatedProjectTemporerDTO> colls = new List<CorrelatedProjectTemporerDTO>();
            foreach (var item in collection)
            {
                colls.Add(new CorrelatedProjectTemporerDTO(item));
            }
            return colls;
        }
    }
}
