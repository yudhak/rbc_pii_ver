﻿using System;
using System.Collections.Generic;
using HR.Domain;


namespace HR.Application.DTO
{
    public class MasterMenuApprovalDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int MenuId { get; set; }

        public MasterMenuApprovalDTO(MasterMenuApproval model)
        {
            if (model == null) return;
            this.Id = model.Id;
            this.Name = model.Name;
            this.Description = model.Description;
            this.MenuId = model.MenuId;
        }

        public static MasterMenuApprovalDTO From(MasterMenuApproval model)
        {
            return new MasterMenuApprovalDTO(model);
        }

        public static IList<MasterMenuApprovalDTO> From(IList<MasterMenuApproval> collection)
        {
            IList<MasterMenuApprovalDTO> colls = new List<MasterMenuApprovalDTO>();
            foreach (var item in collection)
            {
                colls.Add(new MasterMenuApprovalDTO(item));
            }
            return colls;
        }
    }
}
