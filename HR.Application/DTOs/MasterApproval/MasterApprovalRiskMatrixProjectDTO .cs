﻿using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application.DTO
{
    public class MasterApprovalRiskMatrixProjectDTO
    {
        public int Id { get; set; }
        public int MenuId { get; set; }
        public int ProjectId { get; set; }
        public int UserId { get; set; }
        public int? NomorUrutStatus { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DeleteDate { get; set; }
        public MenuDTO Menu { get; set; }
        public ProjectDTO Project { get; set; }
        public UserDTO User { get; set; }

        public MasterApprovalRiskMatrixProjectDTO(MasterApprovalRiskMatrixProject model)
        {
            if (model == null) return;

            this.Id = model.Id;
            this.CreateBy = model.CreateBy;
            this.CreateDate = model.CreateDate;
            this.UpdateBy = model.UpdateBy;
            this.UpdateDate = model.UpdateDate;
            this.IsDelete = model.IsDelete;
            this.DeleteDate = model.DeleteDate;
            this.NomorUrutStatus = model.NomorUrutStatus;

            if(model.Menu != null)
            {
                MenuDTO menuDTO = MenuDTO.From(model.Menu);
                this.Menu = menuDTO;
                this.MenuId = menuDTO.Id;

            }

            if (model.Project != null)
            {
                ProjectDTO projectDTO = ProjectDTO.From(model.Project);
                this.Project = projectDTO;
                this.ProjectId = projectDTO.Id;
            }

            if (model.User != null)
            {
                UserDTO userDTO = UserDTO.From(model.User);
                this.User = userDTO;
                this.UserId = userDTO.Id;
            }

        }

        public static MasterApprovalRiskMatrixProjectDTO From(MasterApprovalRiskMatrixProject model)
        {
            return new MasterApprovalRiskMatrixProjectDTO(model);
        }

        public static IList<MasterApprovalRiskMatrixProjectDTO> From(IList<MasterApprovalRiskMatrixProject> collection)
        {
            IList<MasterApprovalRiskMatrixProjectDTO> colls = new List<MasterApprovalRiskMatrixProjectDTO>();
            foreach (var item in collection)
            {
                colls.Add(new MasterApprovalRiskMatrixProjectDTO(item));
            }
            return colls;
        }
    }
}
