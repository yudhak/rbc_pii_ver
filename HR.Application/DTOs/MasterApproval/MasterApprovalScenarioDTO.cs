﻿using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application.DTO
{
    public class MasterApprovalScenarioDTO
    {
        public int Id { get; set; }
        public int MenuId { get; set; }
        public int UserId { get; set; }
        public int? NomorUrutStatus { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DeleteDate { get; set; }
        public MenuDTO Menu { get; set; }
        public UserDTO User { get; set; }

        public MasterApprovalScenarioDTO(MasterApprovalScenario model)
        {
            if (model == null) return;

            this.Id = model.Id;
            
            this.CreateBy = model.CreateBy;
            this.CreateDate = model.CreateDate;
            this.UpdateBy = model.UpdateBy;
            this.UpdateDate = model.UpdateDate;
            this.IsDelete = model.IsDelete;
            this.DeleteDate = model.DeleteDate;
            this.NomorUrutStatus = model.NomorUrutStatus;

            if(model.Menu != null)
            {
                MenuDTO menuDTO = MenuDTO.From(model.Menu);
                this.Menu = menuDTO;
                this.MenuId = menuDTO.Id;

            }
            
            if(model.User != null)
            {
                UserDTO userDTO = UserDTO.From(model.User);
                this.User = userDTO;
                this.UserId = userDTO.Id;
            }

        }

        public static MasterApprovalScenarioDTO From(MasterApprovalScenario model)
        {
            return new MasterApprovalScenarioDTO(model);
        }

        public static IList<MasterApprovalScenarioDTO> From(IList<MasterApprovalScenario> collection)
        {
            IList<MasterApprovalScenarioDTO> colls = new List<MasterApprovalScenarioDTO>();
            foreach (var item in collection)
            {
                colls.Add(new MasterApprovalScenarioDTO(item));
            }
            return colls;
        }
    }
}
