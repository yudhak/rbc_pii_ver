﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HR.Application.DTO
{
    public class CorrelatedSektorDTO
    {
        public int Id { get; set; }
        public int SektorId { get; set; }
        public string NamaSektor { get; set; }
        public int ScenarioId { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DeleteDate { get; set; }
        public int? StatusId { get; set; }
        public string StatusUserApproval { get; set; }
        public IList<RiskRegistrasiDTO> RiskRegistrasi { get; set; }
        public IList<CorrelationMatrixDTO> CorrelationMatrix { get; set; }
        public StatusDTO Status { get; set; }
        //public ScenarioDTO Scenario { get; set; }
        public ScenarioLightDTO Scenario { get; set; }
        public string NamaScenario { get; set; }
        public CorrelatedSektorDTO(CorrelatedSektor model, IList<RiskRegistrasi> riskRegistrasi, IList<CorrelationMatrix> correlationMatrix)
        {
            if (model == null) return;

            this.Id = model.Id;
            this.SektorId = model.SektorId;
            this.NamaSektor = model.NamaSektor;
            this.ScenarioId = model.ScenarioId;
            this.CreateBy = model.CreateBy;
            this.CreateDate = model.CreateDate;
            this.UpdateBy = model.UpdateBy;
            this.UpdateDate = model.UpdateDate;
            this.IsDelete = model.IsDelete;
            this.DeleteDate = model.DeleteDate;

            if(riskRegistrasi.Count > 0)
            {
                IList<RiskRegistrasiDTO> riskDTO = RiskRegistrasiDTO.From(riskRegistrasi);
                this.RiskRegistrasi = riskDTO;
            }

            if(correlationMatrix.Count > 0)
            {
                IList<CorrelationMatrixDTO> corMatDTO = CorrelationMatrixDTO.From(correlationMatrix);
                this.CorrelationMatrix = corMatDTO;
            }
            if (model.Status != null)
            {
                StatusDTO statusDTO = StatusDTO.From(model.Status);
                this.Status = statusDTO;
                this.StatusId = statusDTO.Id;
            }
            //if (model.Scenario != null)
            //{
            //    this.NamaScenario = model.Scenario.NamaScenario;
            //}
        }

        public CorrelatedSektorDTO(CorrelatedSektor model)
        {
            if (model == null) return;

            this.Id = model.Id;
            this.SektorId = model.SektorId;
            this.NamaSektor = model.NamaSektor;
            this.ScenarioId = model.ScenarioId;
            this.CreateBy = model.CreateBy;
            this.CreateDate = model.CreateDate;
            this.UpdateBy = model.UpdateBy;
            this.UpdateDate = model.UpdateDate;
            this.IsDelete = model.IsDelete;
            this.DeleteDate = model.DeleteDate;

        }

        public CorrelatedSektorDTO(CorrelatedSektor model, IList<RiskRegistrasi> riskRegistrasi, IList<CorrelationMatrix> correlationMatrix, IList<Scenario> scenario)
        {
            var dataScenario = scenario.Where(x => x.Id == model.ScenarioId).FirstOrDefault();
            if (model == null) return;
            this.NamaScenario = dataScenario.NamaScenario;
            this.Id = model.Id;
            this.SektorId = model.SektorId;
            this.NamaSektor = model.NamaSektor;
            this.ScenarioId = model.ScenarioId;
            this.CreateBy = model.CreateBy;
            this.CreateDate = model.CreateDate;
            this.UpdateBy = model.UpdateBy;
            this.UpdateDate = model.UpdateDate;
            this.IsDelete = model.IsDelete;
            this.DeleteDate = model.DeleteDate;

            if (riskRegistrasi.Count > 0)
            {
                IList<RiskRegistrasiDTO> riskDTO = RiskRegistrasiDTO.From(riskRegistrasi);
                this.RiskRegistrasi = riskDTO;
            }

            if (correlationMatrix.Count > 0)
            {
                IList<CorrelationMatrixDTO> corMatDTO = CorrelationMatrixDTO.From(correlationMatrix);
                this.CorrelationMatrix = corMatDTO;
            }
            if (model.Status != null)
            {
                StatusDTO statusDTO = StatusDTO.From(model.Status);
                this.Status = statusDTO;
                this.StatusId = statusDTO.Id;
            }
            if (scenario != null)
            {
                ScenarioLightDTO scenarios = ScenarioLightDTO.From(dataScenario);
                this.Scenario = scenarios;
            }
            //if (model.Scenario != null)
            //{
            //    this.NamaScenario = model.Scenario.NamaScenario;
            //}
        }


        public static CorrelatedSektorDTO From(CorrelatedSektor model, IList<RiskRegistrasi> riskRegistrasi, IList<CorrelationMatrix> correlationMatrix)
        {
            return new CorrelatedSektorDTO(model, riskRegistrasi, correlationMatrix);
        }

        public static IList<CorrelatedSektorDTO> From(IList<CorrelatedSektor> collection, IList<RiskRegistrasi> riskRegistrasi, IList<CorrelationMatrix> correlationMatrix)
        {
            IList<CorrelatedSektorDTO> colls = new List<CorrelatedSektorDTO>();
            foreach (var item in collection)
            {
                colls.Add(new CorrelatedSektorDTO(item, riskRegistrasi, correlationMatrix));
            }
            return colls;
        }

        public static IList<CorrelatedSektorDTO> From(IList<CorrelatedSektor> collection, IList<RiskRegistrasi> riskRegistrasi, IList<CorrelationMatrix> correlationMatrix, IList<Scenario> scenario)
        {
            IList<CorrelatedSektorDTO> colls = new List<CorrelatedSektorDTO>();
            foreach (var item in collection)
            {
                colls.Add(new CorrelatedSektorDTO(item, riskRegistrasi, correlationMatrix, scenario));
            }
            return colls;
        }


        public static CorrelatedSektorDTO From(CorrelatedSektor model)
        {
            return new CorrelatedSektorDTO(model);
        }

        public static IList<CorrelatedSektorDTO> From(IList<CorrelatedSektor> collection)
        {
            IList<CorrelatedSektorDTO> colls = new List<CorrelatedSektorDTO>();
            foreach (var item in collection)
            {
                colls.Add(new CorrelatedSektorDTO(item));
            }
            return colls;
        }
    }
}
