﻿using HR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HR.Application.DTO
{
    public class CorrelatedProjectDTO
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public string NamaProject { get; set; }
        public int SektorId { get; set; }
        public string NamaSektor { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DeleteDate { get; set; }
        public int? StatusId { get; set; }
        public string StatusUserApproval { get; set; }
        public IList<CorrelationMatrixDTO> CorrelationMatrix { get; set; }
        public IList<ProjectDTO> Project { get; set; }
        //public ProjectDTO ProjectD { get; set; }
        public StatusDTO Status { get; set; }
        public string NamaScenario { get; set; }
        public int IdScenario { get; set; }
        //public ScenarioDTO Scenario { get; set; }
        public ScenarioLightDTO Scenario { get; set; }
        public CorrelatedProjectDTO(CorrelatedProject model, IList<CorrelationMatrix> correlationMatrix, IList<Project> project)
        {
            if (model == null) return;
            var dataProject = project.Where(x => x.Id == model.ProjectId).FirstOrDefault();

            this.Id = model.Id;
            this.ProjectId = model.ProjectId;
            this.NamaProject = dataProject.NamaProject;
            this.SektorId = model.SektorId;
            if (dataProject.Sektor != null)
            {
                this.NamaSektor = dataProject.Sektor.NamaSektor;
            }
            this.CreateBy = model.CreateBy;
            this.CreateDate = model.CreateDate;
            this.UpdateBy = model.UpdateBy;
            this.UpdateDate = model.UpdateDate;
            this.IsDelete = model.IsDelete;
            this.DeleteDate = model.DeleteDate;

            if (correlationMatrix.Count > 0)
            {
                IList<CorrelationMatrixDTO> corMatDTO = CorrelationMatrixDTO.From(correlationMatrix);
                this.CorrelationMatrix = corMatDTO;
            }

            if (project.Count > 0)
            {
                IList<ProjectDTO> projectDTO = ProjectDTO.From(project);
                this.Project = projectDTO;
            }
            if (model.Status != null)
            {
                StatusDTO statusDTO = StatusDTO.From(model.Status);
                this.Status = statusDTO;
                this.StatusId = statusDTO.Id;
            }
            //if (model.ScenarioId != 0)
            //{
            //    this.IdScenario = model.ScenarioId;
            //}
        }

        public CorrelatedProjectDTO(CorrelatedProject model, IList<CorrelationMatrix> correlationMatrix)
        {
            if (model == null) return;
            this.Id = model.Id;
            this.ProjectId = model.ProjectId;
            this.NamaProject = model.Project.NamaProject;
            this.SektorId = model.SektorId;
            if (model.Sektor != null)
            {
                this.NamaSektor = model.Sektor.NamaSektor;
            }
            this.CreateBy = model.CreateBy;
            this.CreateDate = model.CreateDate;
            this.UpdateBy = model.UpdateBy;
            this.UpdateDate = model.UpdateDate;
            this.IsDelete = model.IsDelete;
            this.DeleteDate = model.DeleteDate;

            if (correlationMatrix.Count > 0)
            {
                IList<CorrelationMatrixDTO> corMatDTO = CorrelationMatrixDTO.From(correlationMatrix);
                this.CorrelationMatrix = corMatDTO;
            }

            if (model.Status != null)
            {
                StatusDTO statusDTO = StatusDTO.From(model.Status);
                this.Status = statusDTO;
                this.StatusId = statusDTO.Id;
            }

            //if (model.ScenarioId != null)
            //{
            //    this.NamaScenario = model.Scenario.NamaScenario;
            //}
        }

        public CorrelatedProjectDTO(CorrelatedProject model, IList<CorrelationMatrix> correlationMatrix, IList<Scenario> scenario)
        {
            var dataScenario = scenario.Where(x => x.Id == model.ScenarioId).FirstOrDefault();
            this.NamaScenario = dataScenario.NamaScenario;
            if (model == null) return;
            this.Id = model.Id;
            this.ProjectId = model.ProjectId;
            this.NamaProject = model.Project.NamaProject;
            this.SektorId = model.SektorId;
            if (model.Sektor != null)
            {
                this.NamaSektor = model.Sektor.NamaSektor;
            }
            this.CreateBy = model.CreateBy;
            this.CreateDate = model.CreateDate;
            this.UpdateBy = model.UpdateBy;
            this.UpdateDate = model.UpdateDate;
            this.IsDelete = model.IsDelete;
            this.DeleteDate = model.DeleteDate;

            if (correlationMatrix.Count > 0)
            {
                IList<CorrelationMatrixDTO> corMatDTO = CorrelationMatrixDTO.From(correlationMatrix);
                this.CorrelationMatrix = corMatDTO;
            }

            if (model.Status != null)
            {
                StatusDTO statusDTO = StatusDTO.From(model.Status);
                this.Status = statusDTO;
                this.StatusId = statusDTO.Id;
            }
            //if (namaScenario != null)
            //{
            //    this.NamaScenario = namaScenario;
            //}
            if (scenario != null)
            {
                ScenarioLightDTO scenarioDTO = ScenarioLightDTO.From(dataScenario);
                this.Scenario = scenarioDTO;
            }
        }

        //public CorrelatedProjectDTO(CorrelatedProject model, IList<CorrelationMatrix> correlationMatrix, Project project)
        //{
        //    if (model == null) return;
        //    var dataProject = project;

        //    this.Id = model.Id;
        //    this.ProjectId = model.ProjectId;
        //    this.NamaProject = dataProject.NamaProject;
        //    this.SektorId = model.SektorId;
        //    if (dataProject.Sektor != null)
        //    {
        //        this.NamaSektor = dataProject.Sektor.NamaSektor;
        //    }
        //    this.CreateBy = model.CreateBy;
        //    this.CreateDate = model.CreateDate;
        //    this.UpdateBy = model.UpdateBy;
        //    this.UpdateDate = model.UpdateDate;
        //    this.IsDelete = model.IsDelete;
        //    this.DeleteDate = model.DeleteDate;

        //    if (correlationMatrix.Count > 0)
        //    {
        //        IList<CorrelationMatrixDTO> corMatDTO = CorrelationMatrixDTO.From(correlationMatrix);
        //        this.CorrelationMatrix = corMatDTO;
        //    }

        //    if (project != null)
        //    {
        //        ProjectDTO projectDTO = ProjectDTO.From(project);
        //        this.ProjectD = projectDTO;
        //    }
        //    if (model.Status != null)
        //    {
        //        StatusDTO statusDTO = StatusDTO.From(model.Status);
        //        this.Status = statusDTO;
        //        this.StatusId = statusDTO.Id;
        //    }
        //}


        public static CorrelatedProjectDTO From(CorrelatedProject model, IList<CorrelationMatrix> correlationMatrix, IList<Project> project)
        {
            return new CorrelatedProjectDTO(model, correlationMatrix, project);
        }

        //public static CorrelatedProjectDTO From(CorrelatedProject model, IList<CorrelationMatrix> correlationMatrix, Project project)
        //{
        //    return new CorrelatedProjectDTO(model, correlationMatrix, project);
        //}

        public static IList<CorrelatedProjectDTO> From(IList<CorrelatedProject> collection, IList<CorrelationMatrix> correlationMatrix, IList<Project> project)
        {
            IList<CorrelatedProjectDTO> colls = new List<CorrelatedProjectDTO>();
            foreach (var item in collection)
            {
                colls.Add(new CorrelatedProjectDTO(item, correlationMatrix, project));
            }
            return colls;
        }

        public static IList<CorrelatedProjectDTO> From(IList<CorrelatedProject> collection, IList<CorrelationMatrix> correlationMatrix)
        {
            IList<CorrelatedProjectDTO> colls = new List<CorrelatedProjectDTO>();
            foreach (var item in collection)
            {
                colls.Add(new CorrelatedProjectDTO(item, correlationMatrix));
            }
            return colls;
        }

        public static IList<CorrelatedProjectDTO> From(IList<CorrelatedProject> collection, IList<CorrelationMatrix> correlationMatrix, IList<Scenario> scenario, int angka)
        {
            IList<CorrelatedProjectDTO> colls = new List<CorrelatedProjectDTO>();
            foreach (var item in collection)
            {
                colls.Add(new CorrelatedProjectDTO(item, correlationMatrix, scenario));
            }
            return colls;
        }

        //public static IList<CorrelatedProjectDTO> From(IList<CorrelatedProject> collection, IList<CorrelationMatrix> correlationMatrix, Project project)
        //{
        //    IList<CorrelatedProjectDTO> colls = new List<CorrelatedProjectDTO>();
        //    foreach (var item in collection)
        //    {
        //        colls.Add(new CorrelatedProjectDTO(item, correlationMatrix, project));
        //    }
        //    return colls;
        //}
    }
}
