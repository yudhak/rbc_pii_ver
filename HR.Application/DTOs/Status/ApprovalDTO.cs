﻿using HR.Application.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.Domain;
using HR.Common;
using HR.Application.Params;

namespace HR.Application.DTO
{
    public class ApprovalDTO
    {
        public int Id { get; set; }
        public string StatusApproval { get; set; }
        public string SourceApproval { get; set; }
        public string Keterangan { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DeleteDate { get; set; }
        public bool? IsActive { get; set; }

        //foreign key
        public int RequestId { get; set; }
        public int NomorUrutStatus { get; set; }
        public int StatusId { get; set; }
        //public int ScenarioId { get; set; }
        //navigation properties
        public ScenarioDTO Scenario { get; set; }
        public StatusDTO Status { get; set; }
       // public RoleDTO Role { get; set; }
        public UserDTO UserCreate { get; set; }
        public RiskMatrixProjectDTO riskMatrixProject { get; set; }
        public CorrelatedProjectDTO correlatedProject { get; set; }
        public IList<CorrelatedProjectApprovalParam> correlatedProjectList { get; set; }
        public CorrelatedProjectDetailCollectionParam correlatedProjectDetailCollection { get; set; }
        public CorrelatedSektorDTO correlatedSektor { get; set; }
        public CorrelatedSektorDetailCollectionParam correlatedSektorDetailCollection { get; set; }
        public IList<RiskRegistrasi> RiskRegistrasi { get; set; }
        public IList<CorrelationMatrix> CorrelationMatrix { get; set; }
        public IList<StageTahunRiskMatrixLightDTO> StageTahunRiskMatrix { get; set; }
        public RiskMatrixCollectionParameter RiskMatrixCollectionParameter { get; set; }
        public IList<RiskMatrixCollectionParam> riskMatrixCollectionParamForMasterAproval { get; set; }

        //properties tambahan
        public string NamaApproval { get; set; }
        public string NamaPembuat { get; set; }
        //public int PembuatId { get; set; }
        public string NamaScenario { get; set; }
        public string NamaEntitas { get; set; }

        public ApprovalDTO()
        {

        }

        public ApprovalDTO(Approval model)
        {
            if (model == null) return;

            this.Id = model.Id;
            this.RequestId = model.RequestId;
            this.StatusApproval = model.StatusApproval;
            this.SourceApproval = model.SourceApproval;
            this.Keterangan = model.Keterangan;
            this.StatusId = model.StatusId;
            this.CreateDate = model.CreateDate;
            this.CreateBy = model.CreateBy;
            this.IsDelete = model.IsDelete;
            this.DeleteDate = model.DeleteDate;
            this.NomorUrutStatus = model.NomorUrutStatus;
            if (model.Status != null)
            {
                StatusDTO statusDTO = StatusDTO.From(model.Status);
                this.Status = statusDTO;
                this.StatusId = statusDTO.Id;
                this.NamaApproval = statusDTO.StatusDescription;
            }
        }

        //Untuk master Approval Scenario
        public ApprovalDTO(Status status, Scenario scenario, User userCreate)
        {
            if (status != null)
            {
                StatusDTO statusDTO = StatusDTO.From(status);
                this.Status = statusDTO;
                this.StatusId = statusDTO.Id;
            }

            if (scenario != null)
            {
                ScenarioDTO scenarioDTO = ScenarioDTO.From(scenario);
                this.Scenario = scenarioDTO;
            }

            if (userCreate != null)
            {
                UserDTO userCreateDTO = UserDTO.From(userCreate);
                this.UserCreate = userCreateDTO;
            }
        }

        //Untuk master Approval risk matrix project
        public ApprovalDTO(Status status, RiskMatrixProject riskMatrixProject, List<RiskRegistrasi> riskRegistrasi, User userCreate, IList<StageTahunRiskMatrix> stageTahunRiskMatrix, IList<RiskMatrixCollectionParam> riskMatrixCollectionParam)
        {
            if (status != null)
            {
                StatusDTO statusDTO = StatusDTO.From(status);
                this.Status = statusDTO;
                this.StatusId = statusDTO.Id;
            }


            if (riskMatrixProject != null)
            {
                RiskMatrixProjectDTO riskMatrixProjectDTO = RiskMatrixProjectDTO.From(riskMatrixProject, riskRegistrasi);

                this.riskMatrixProject = riskMatrixProjectDTO;
            }

            if (userCreate != null)
            {
                UserDTO userCreateDTO = UserDTO.From(userCreate);
                this.UserCreate = userCreateDTO;
            }

            if (stageTahunRiskMatrix != null)
            {
                IList<StageTahunRiskMatrixLightDTO> stageTahunRiskMatrixLightDTO = StageTahunRiskMatrixLightDTO.From(stageTahunRiskMatrix);
                this.StageTahunRiskMatrix = stageTahunRiskMatrixLightDTO;
            }

            if (riskMatrixCollectionParam != null)
            {
                this.riskMatrixCollectionParamForMasterAproval = riskMatrixCollectionParam;
            }
            
        }
        //Untuk master Approval correlated sektor
        public ApprovalDTO(Status status, CorrelatedSektor correlatedSektor, User userCreate, MasterApprovalCorrelatedSektor masterApprovalCorrelatedSektor)
        {
            if (status != null)
            {
                StatusDTO statusDTO = StatusDTO.From(status);
                this.Status = statusDTO;
                this.StatusId = statusDTO.Id;
            }


            if (correlatedSektor != null)
            {
                CorrelatedSektorDTO correlatedSektorDTO = CorrelatedSektorDTO.From(correlatedSektor);
                this.correlatedSektor = correlatedSektorDTO;
            }

            if (userCreate != null)
            {
                UserDTO userCreateDTO = UserDTO.From(userCreate);
                this.UserCreate = userCreateDTO;
            }
        }
        //Untuk master Approval correlated project
        public ApprovalDTO(Status status, User userCreate)
        {
            if (status != null)
            {
                StatusDTO statusDTO = StatusDTO.From(status);
                this.Status = statusDTO;
                this.StatusId = statusDTO.Id;
            }

            if (userCreate != null)
            {
                UserDTO userCreateDTO = UserDTO.From(userCreate);
                this.UserCreate = userCreateDTO;
            }
        }


        public static ApprovalDTO From(Approval model)
        {
            return new ApprovalDTO(model);
        }

        public static IList<ApprovalDTO> From(IList<Approval> collection)
        {
            IList<ApprovalDTO> colls = new List<ApprovalDTO>();
            foreach (var item in collection)
            {
                colls.Add(new ApprovalDTO(item));
            }
            return colls;
        }

    }

    public class ApprovalExtendDTO
    {
        public int Id { get; set; }
        public string NamaScenario { get; set; }
        public string StatusApproval { get; set; }
        public string SourceApproval { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DeleteDate { get; set; }
        public string Keterangan { get; set; }
        public bool IsActive { get; set; }
        public int StatusId { get; set; }
        public int RequestId { get; set; }
        public int NomorUrutStatus { get; set; }

        public StatusExtend Status { get; set; }
        public ScenarioDTO Scenario { get; set; }
        public RiskMatrixProjectExtend RiskMatrixProject { get; set; }
        public CorrelatedSektorExtend CorrelatedSektor { get; set; }
        public CorrelatedProjectExtend CorrelatedProject { get; set; }

        public ApprovalExtendDTO() { }

        public ApprovalExtendDTO(ApprovalExtend model)
        {
            if (model == null) return;

            this.Id = model.Id;
            this.NamaScenario = model.NamaScenario;
            this.StatusApproval = model.StatusApproval;
            this.SourceApproval = model.SourceApproval;
            this.CreateBy = model.CreateBy;
            this.CreateDate = model.CreateDate;
            this.UpdateBy = model.UpdateBy;
            this.UpdateDate = model.UpdateDate;
            this.IsDelete = model.IsDelete;
            this.Keterangan = model.Keterangan;
            this.IsActive = model.IsActive;
            this.StatusId = model.StatusId;
            this.RequestId = model.RequestId;
            this.NomorUrutStatus = model.NomorUrutStatus;
        }

        public static IList<ApprovalExtendDTO> From(IList<ApprovalExtend> collection)
        {
            IList<ApprovalExtendDTO> colls = new List<ApprovalExtendDTO>();
            foreach (var item in collection)
            {
                colls.Add(new ApprovalExtendDTO(item));
            }
            return colls;
        }
    }
}
