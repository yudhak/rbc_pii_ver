﻿using System;
using System.Collections.Generic;
using HR.Domain;


namespace HR.Application.DTO
{
    public class StatusDTO
    {
        public int Id { get; set; }
        public string StatusDescription { get; set; }

        public StatusDTO(Status model)
        {
            if (model == null) return;
            this.Id = model.Id;
            this.StatusDescription = model.StatusDescription;
        }

        public static StatusDTO From(Status model)
        {
            return new StatusDTO(model);
        }

        public static IList<StatusDTO> From(IList<Status> collection)
        {
            IList<StatusDTO> colls = new List<StatusDTO>();
            foreach (var item in collection)
            {
                colls.Add(new StatusDTO(item));
            }
            return colls;
        }
    }
}
