﻿using HR.Domain;
using System;
using System.Collections.Generic;

namespace HR.Application.DTO
{
    public class ScenarioDTO
    {
        public int Id { get; set; }
        public string NamaScenario { get; set; }
        public int LikehoodId { get; set; }
        public string NamaLikehood { get; set; }
        public bool IsDefault { get; set; }
        //public int? Status { get; set; }
        public int? StatusId { get; set; }
        public string StatusUserApproval { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DeleteDate { get; set; }
        public LikehoodDTO Likehood { get; set; }
        public IList<ScenarioDetailDTO> ScenarioDetail { get; set; }
        public IList<ProjectDTO> Project { get; set; }
        public StatusDTO Status { get; set; }

        public ScenarioDTO(Scenario model)
        {
            if (model == null) return;

            this.Id = model.Id;
            this.NamaScenario = model.NamaScenario;
            this.CreateBy = model.CreateBy;
            this.CreateDate = model.CreateDate;
            this.UpdateBy = model.UpdateBy;
            this.UpdateDate = model.UpdateDate;
            this.IsDelete = model.IsDelete;
            this.DeleteDate = model.DeleteDate;
            //this.Status = model.Status;
            this.IsDefault = model.IsDefault;

            if(model.Likehood != null)
            {
                LikehoodDTO likehoodDTO = LikehoodDTO.From(model.Likehood);
                this.Likehood = likehoodDTO;
                this.LikehoodId = likehoodDTO.Id;
                this.NamaLikehood = likehoodDTO.NamaLikehood;
            }
            
            if(model.ScenarioDetail != null)
            {
                IList<ScenarioDetailDTO> scenarioDetailDTO = ScenarioDetailDTO.From(model.ScenarioDetail);
                this.ScenarioDetail = scenarioDetailDTO;
            }

            this.Project = new List<ProjectDTO>();
            if (model.ScenarioDetail != null)
            {
                foreach (var item in this.ScenarioDetail)
                {
                    this.Project.Add(item.Project);
                }
            }
            if (model.Status != null)
            {
                StatusDTO statusDTO = StatusDTO.From(model.Status);
                this.Status = statusDTO;
                this.StatusId = statusDTO.Id;
            }
        }

        public static ScenarioDTO From(Scenario model)
        {
            return new ScenarioDTO(model);
        }

        public static IList<ScenarioDTO> From(IList<Scenario> collection)
        {
            IList<ScenarioDTO> colls = new List<ScenarioDTO>();
            foreach (var item in collection)
            {
                colls.Add(new ScenarioDTO(item));
            }
            return colls;
        }
    }

    public class ScenarioLightDTO
    {
        public int Id { get; set; }
        public string NamaScenario { get; set; }
        public int LikehoodId { get; set; }
        public string NamaLikehood { get; set; }
        public bool IsDefault { get; set; }
        //public int? Status { get; set; }
        public int? StatusId { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DeleteDate { get; set; }
        public string StatusUserApproval { get; set; }
        public StatusDTO Status { get; set; }

        public ScenarioLightDTO(Scenario model)
        {
            if (model == null) return;

            this.Id = model.Id;
            this.NamaScenario = model.NamaScenario;
            this.CreateBy = model.CreateBy;
            this.CreateDate = model.CreateDate;
            this.UpdateBy = model.UpdateBy;
            this.UpdateDate = model.UpdateDate;
            this.IsDelete = model.IsDelete;
            this.DeleteDate = model.DeleteDate;
            this.IsDefault = model.IsDefault;
           // this.StatusUserApproval = model.
            if (model.Likehood != null)
            {
                this.LikehoodId = model.Likehood.Id;
                this.NamaLikehood = model.Likehood.NamaLikehood;
            }

            
            if (model.Status != null)
            {
                StatusDTO statusDTO = StatusDTO.From(model.Status);
                this.Status = statusDTO;
                this.StatusId = statusDTO.Id;
            }
        }

        public static ScenarioLightDTO From(Scenario model)
        {
            return new ScenarioLightDTO(model);
        }

        public static IList<ScenarioLightDTO> From(IList<Scenario> collection)
        {
            IList<ScenarioLightDTO> colls = new List<ScenarioLightDTO>();
            foreach (var item in collection)
            {
                colls.Add(new ScenarioLightDTO(item));
            }
            return colls;
        }
    }
}
