﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using HR.Domain;
using System.Collections.Generic;
using System;

namespace HR.Application.DTO
{
    public class RoleAccessFrontDTO 
    {
        public int Id { get; private set; }
        public int RoleId { get; set; }       
        public int MenuId { get; set; }
        public bool? Tambah { get; private set; }
        public bool? Ubah { get; private set; }
        public bool? Hapus { get; private set; }
        public bool? Lihat { get; private set; }
        public bool? Detail { get; private set; }
        public int? CreateBy { get; private set; }
        public DateTime? CreateDate { get; private set; }
        public int? UpdateBy { get; private set; }
        public DateTime? UpdateDate { get; private set; }
        public bool? IsDelete { get; private set; }
        public bool? IsActive { get; private set; }

        public RoleListDTO Role { get; set; }
        public MenuAccessLiteWithChildDTO Menu { get; set; }

        public RoleAccessFrontDTO(RoleAccessFront model)
        {
            if (model == null) return;

            this.Id = model.Id;
            this.RoleId = model.RoleId;
            this.MenuId = model.MenuId;
            this.Tambah = model.Tambah;
            this.Ubah = model.Ubah;
            this.Hapus = model.Hapus;
            this.Lihat = model.Lihat;
            this.Detail = model.Detail;
            this.CreateBy = model.CreateBy;
            this.CreateDate = model.CreateDate;
            this.UpdateBy = model.UpdateBy;
            this.UpdateDate = model.UpdateDate;
            this.IsDelete = model.IsDelete;
            this.IsActive = model.IsActive;

            if (model.Role != null)
            {
                RoleListDTO roleDTO = RoleListDTO.From(model.Role);
                this.Role = roleDTO;
                this.RoleId = roleDTO.Id;
            }

            if (model.Menu != null)
            {
                MenuAccessLiteWithChildDTO menuDTO = MenuAccessLiteWithChildDTO.From(model.Menu);
                this.Menu = menuDTO;
                this.MenuId = menuDTO.Id;
            }
        }

        public static RoleAccessFrontDTO From(RoleAccessFront model)
        {
            return new RoleAccessFrontDTO(model);
        }

        public static IList<RoleAccessFrontDTO> From(IList<RoleAccessFront> collection)
        {
            IList<RoleAccessFrontDTO> colls = new List<RoleAccessFrontDTO>();
            foreach (var item in collection)
            {
                colls.Add(new RoleAccessFrontDTO(item));
            }
            return colls;
        }
    }
}
