﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using HR.Domain;
using System.Collections.Generic;
using System;

namespace HR.Application.DTO
{
    public class RoleAccessDTO 
    {
        public int Id { get; private set; }
        public int RoleId { get; set; }       
        public int MenuId { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool? IsActive { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }

        public RoleListDTO Role { get; set; }
        public MenuAccessLiteWithChildDTO Menu { get; set; }

        public RoleAccessDTO(RoleAccess model)
        {
            if (model == null) return;

            this.Id = model.Id;
            this.MenuId = model.MenuId;
            this.RoleId = model.RoleId;
            this.CreateBy = model.CreateBy;
            this.CreateDate = model.CreateDate;
            this.IsActive = model.IsActive;
            this.UpdateBy = model.UpdateBy;
            this.UpdateDate = model.UpdateDate;
            this.IsDelete = model.IsDelete;

            if (model.Role != null)
            {
                RoleListDTO roleDTO = RoleListDTO.From(model.Role);
                this.Role = roleDTO;
                this.RoleId = roleDTO.Id;
            }

            if (model.Menu != null)
            {
                MenuAccessLiteWithChildDTO menuDTO = MenuAccessLiteWithChildDTO.From(model.Menu);
                this.Menu = menuDTO;
                this.MenuId = menuDTO.Id;
            }
        }

        public static RoleAccessDTO From(RoleAccess model)
        {
            return new RoleAccessDTO(model);
        }

        public static IList<RoleAccessDTO> From(IList<RoleAccess> collection)
        {
            IList<RoleAccessDTO> colls = new List<RoleAccessDTO>();
            foreach (var item in collection)
            {
                colls.Add(new RoleAccessDTO(item));
            }
            return colls;
        }
    }
}
